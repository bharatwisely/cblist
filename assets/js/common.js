var growth_flag = '';
var expense_flag = '';
jQuery(document).ready(function($) {
	"use strict";
	
	var btn_clicked = false;
	var select_imgs = false;
	
	var num = 0;
	if ( $('.number-field').attr('id') == 'FirstMortgageRate' || $('.mumber-field').attr('id') == 'SecondMortgageRate' ) {
		$('.number-field').attr('placeholder', num.toFixed(3));
	} else {
		$('.number-field').attr('placeholder', num.toFixed(2));
	}
	
	$('.number-field, .MonthlyRent_input, .NoPreZeroWith2Decimal, .NoPreZeroWithoutDecimal, .Bid1InputBox, .Bid2InputBox, .Bid3InputBox, .BudgetInputBox').change(function(){
		try {
			if ( $(this).val().length == 0 ) {
				return false;
			}
			var avrVal = parseFloat($(this).val().replace("$", ""));
			avrVal = avrVal.replace(/\b0+/g, '');
		} catch(err) {
			//
		}
		if ( $(this).attr('id') == 'FirstMortgageRate' || $(this).attr('id') == 'SecondMortgageRate' || $(this).attr('id') == 'Rate_1' || $(this).attr('id') == 'Rate_2' ) {
			$(this).val(avrVal.toFixed(3));
		} else {
			if ( $(this).hasClass('percentageField') ) {
				$(this).val(avrVal.toFixed(0));
			} else if ( $(this).hasClass('NoPreZeroWithoutDecimal') ) {
				$(this).val(avrVal.toFixed(0));
			} else {
				$(this).val(avrVal.toFixed(2));
			}
		}		
	});
	
	$('.left-panel a, footer a').on('click', function(e) {
		var ProposalStatus = $('#ProposalStatus').val();
		if ( btn_clicked && ProposalStatus == 'draft' ) {
			e.preventDefault();
			
			$('#ProposalStatus').val('draft');
			$('#ExitProposal').data('rel', $(this).attr('href'));
			$('#confirm_modal').modal('show');
		}
	});
	$('#SaveDraft, #SaveAsDraft').on('click', function() {
		select_imgs = true;
		$('.draft-loading').removeClass('hide');
		$('.on_save_event').click();
	});
	$('#ExitProposal').on('click', function() {
		goto($(this).data('rel'));
	});
	
	// progressbar
	var $pb = $('.progress .progress-bar');
	$('.trigger-bar').click(function() {
		var $value = $(this).attr('data-rel');
		$pb.attr('data-transitiongoal', $value).progressbar({
			display_text: 'center'
		});
	});
	
	// script for offer screen ui	
	$('.offer-calc-page').css('min-height', $(window).height() - $('.page-title').outerHeight() - 30);
	
	$('#RehubValuator .Step1Button').click(function(e) {
		e.preventDefault();
        $('.quick-address').html('<p>Street Address: <b>' + $('#PropertyStreetAddress').val() + '</b></p><p>City/Town: <b>' + $('#PropertyCityTown').val() + '</b></p><p>State/Province: <b>' + $('#PropertyStateProvince').val() + '</b></p><p>ZIP Code: <b>' + $('#PropertyZipCode').val() + '</b></p><p>Country: <b>' + $('#PropertyCountry option:selected').text() + '</b></p>');
    });
	$('#RehubValuator .Step2Button').click(function(e) {
		e.preventDefault();
    });
	$('#RehubValuator .Step3Button').click(function(e) {
		e.preventDefault();
		if ( $('#PurchasePrice').val() != '' ) {
			if ( $('#PurchasePriceEM').length > 0 ) {
				$('#PurchasePriceEM').text($('#PurchasePrice').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">PP: <em id="PurchasePriceEM">' + $('#PurchasePrice').val() + '</em></span>');
				$('.quick-tags-nano').nanoScroller();
			}
		}
    });
	$('#RehubValuator .Step4Button').click(function(e) {
		e.preventDefault();
    });
	$('#RehubValuator .Step5Button').click(function(e) {
		e.preventDefault();
		if ( $('#MonthsFlipRehab').val() != '' ) {
			if ( $('#MonthsFlipRehabEM').length > 0 ) {
				$('#MonthsFlipRehabEM').text($('#MonthsFlipRehab').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">FRP(M): <em id="MonthsFlipRehabEM">' + $('#MonthsFlipRehab').val() + '</em></span> ');
				$('.quick-tags-nano').nanoScroller();
			}
		}
		if ( $('#ActualFlipBudget').val() != '' ) {
			if ( $('#ActualFlipBudgetEM').length > 0 ) {
				$('#ActualFlipBudgetEM').text($('#ActualFlipBudget').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">FB: <em id="ActualFlipBudgetEM">' + $('#ActualFlipBudget').val() + '</em></span> ');
				$('.quick-tags-nano').nanoScroller();
			}
		}
		if ( $('#ARVFlip').val() != '' ) {
			if ( $('#ARVFlipEM').length > 0 ) {
				$('#ARVFlipEM').text($('#ARVFlip').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">ARVF: <em id="ARVFlipEM">' + $('#ARVFlip').val() + '</em></span> ');
				$('.quick-tags-nano').nanoScroller();
			}
		}
		if ( $('#MonthsToSell').val() != '' ) {
			if ( $('#MonthsToSellEM').length > 0 ) {
				$('#MonthsToSellEM').text($('#MonthsToSell').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">MSF: <em id="MonthsToSellEM">' + $('#MonthsToSell').val() + '</em></span> ');
				$('.quick-tags-nano').nanoScroller();
			}
		}
    });
	$('#RehubValuator .Step6Button').click(function(e) {
		e.preventDefault();
		if ( $('#MonthsRefiRehab').val() != '' ) {
			if ( $('#MonthsRefiRehabEM').length > 0 ) {
				$('#MonthsRefiRehabEM').text($('#MonthsRefiRehab').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">RRP: <em id="MonthsRefiRehabEM">' + $('#MonthsRefiRehab').val() + '</em></span> ');
				$('.quick-tags-nano').nanoScroller();
			}
		}
		if ( $('#ActualRefiBudget').val() != '' ) {
			if ( $('#ActualRefiBudgetEM').length > 0 ) {
				$('#ActualRefiBudgetEM').text($('#ActualRefiBudget').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">RB: <em id="ActualRefiBudgetEM">' + $('#ActualRefiBudget').val() + '</em></span> ');
				$('.quick-tags-nano').nanoScroller();
			}
		}
		if ( $('#ARVRent').val() != '' ) {
			if ( $('#ARVRentEM').length > 0 ) {
				$('#ARVRentEM').text($('#ARVRent').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">ARVR: <em id="ARVRentEM">' + $('#ARVRent').val() + '</em></span> ');
				$('.quick-tags-nano').nanoScroller();
			}
		}
		if ( $('#MonthsToRefi').val() != '' ) {
			if ( $('#MonthsToRefiEM').length > 0 ) {
				$('#MonthsToRefiEM').text($('#MonthsToRefi').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">MSR: <em id="MonthsToRefiEM">' + $('#MonthsToRefi').val() + '</em></span> ');
				$('.quick-tags-nano').nanoScroller();
			}
		}
		if ( $('#CashOutRefi').val() != '' ) {
			if ( $('#CashOutRefiEM').length > 0 ) {
				$('#CashOutRefiEM').text($('#CashOutRefi').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">COR: <em id="CashOutRefiEM">' + $('#CashOutRefi').val() + '</em></span> ');
				$('.quick-tags-nano').nanoScroller();
			}
		}
		if ( $('#CashflowMonthly').val() != '' ) {
			if ( $('#CashflowMonthlyEM').length > 0 ) {
				$('#CashflowMonthlyEM').text($('#CashflowMonthly').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">CM: <em id="CashflowMonthlyEM">' + $('#CashflowMonthly').val() + '</em></span> ');
				$('.quick-tags-nano').nanoScroller();
			}
		}
    });
	$('#RehubValuator .Step7Button').click(function(e) {
		e.preventDefault();
    });
	$('#RehubValuator .Step8Button').click(function(e) {
		e.preventDefault();
    });
	
	$('#PropertyStreetAddress').on('change blur', function(e) {
        $('#PropertyNameTitle').text(' ' + $(this).val());
    });
	
	$('#PropertyStreetAddress').on('change blur focus', function(e) {
        $('#FlipTitleNew').text($('#PropertyStreetAddress').val());
		$('#FlipTitleEdit').text($('#PropertyStreetAddress').val());
		$('#RefiTitleNew').text($('#PropertyStreetAddress').val());
		$('#RefiTitleEdit').text($('#PropertyStreetAddress').val());
    });
	
	$('.offer-calculator-screen .on_next_section').on('click', function() {
		var form = $(this).parents('form');
		var error = [];
		$(form).find('.Required').each(function(index) {
			if ( $(this).val() == false ) {
				$(this).css({
					'border' : '1px solid #a94442',
					'background' : '#f2dede',
				});
				error.push('error');
			} else {
				var background = $(this).css('background-color'); var border = '';
				var prev = $(this).parents('.panel-field').prev('.panel-field').find('.form-control');
				if ( background == 'rgb(242, 222, 222)' ) {
					if ( prev.css('background-color') == 'rgb(206, 234, 217)' ) {
						background = 'rgb(198, 238, 253)';
						border = '1px solid rgb(42, 108, 132)';
					} else {
						background = 'rgb(206, 234, 217)';
						border = '1px solid rgb(0, 124, 48)';
					}
				}
				$(this).css({
					'border' : border,
					'background' : background,
				});
			}
		});
		
		var page = $('#page_type').val();
		if ( page == 'PROPOSAL' ) {
			if ( $(this).hasClass('Step5Button') ) {
				if ( (parseInt($('#MonthsFlipRehab').val()) + parseInt($('#MonthsToSell').val())) > 24 ) {
					$('#MonthsFlipRehab, #MonthsToSell').css({
						'border' : '1px solid #a94442',
						'background' : '#f2dede',
					});
					$('#MonthsModal').modal('show');
					return false;
				} else {
					var background1 = $('#MonthsFlipRehab').css('background-color'); var border1 = '';
					var background2 = $('#MonthsToSell').css('background-color'); var border2 = '';
					var prev1 = $('#MonthsFlipRehab').parents('.panel-field').prev('.panel-field').find('.form-control');
					var prev2 = $('#MonthsToSell').parents('.panel-field').prev('.panel-field').find('.form-control');
					if ( background1 == 'rgb(242, 222, 222)' ) {
						if ( prev1.css('background-color') == 'rgb(206, 234, 217)' ) {
							background1 = 'rgb(198, 238, 253)';
							border1 = '1px solid rgb(42, 108, 132)';
						} else {
							background1 = 'rgb(206, 234, 217)';
							border1 = '1px solid rgb(0, 124, 48)';
						}
					}
					$('#MonthsFlipRehab').css({
						'border' : border1,
						'background' : background1,
					});
					if ( background2 == 'rgb(242, 222, 222)' ) {
						if ( prev2.css('background-color') == 'rgb(206, 234, 217)' ) {
							background2 = 'rgb(198, 238, 253)';
							border2 = '1px solid rgb(42, 108, 132)';
						} else {
							background2 = 'rgb(206, 234, 217)';
							border2 = '1px solid rgb(0, 124, 48)';
						}
					}
					$('#MonthsToSell').css({
						'border' : border2,
						'background' : background2,
					});
				}
			}
			if ( $(this).hasClass('Step6Button') ) {
				if ( (parseInt($('#MonthsRefiRehab').val()) + parseInt($('#MonthsToRefi').val())) > 24 ) {
					$('#MonthsRefiRehab, #MonthsToRefi').css({
						'border' : '1px solid #a94442',
						'background' : '#f2dede',
					});
					$('#MonthsModal').modal('show');
					return false;
				} else {
					var background1 = $('#MonthsRefiRehab').css('background-color'); var border1 = '';
					var background2 = $('#MonthsToRefi').css('background-color'); var border2 = '';
					var prev1 = $('#MonthsRefiRehab').parents('.panel-field').prev('.panel-field').find('.form-control');
					var prev2 = $('#MonthsToRefi').parents('.panel-field').prev('.panel-field').find('.form-control');
					if ( background1 == 'rgb(242, 222, 222)' ) {
						if ( prev1.css('background-color') == 'rgb(206, 234, 217)' ) {
							background1 = 'rgb(198, 238, 253)';
							border1 = '1px solid rgb(42, 108, 132)';
						} else {
							background1 = 'rgb(206, 234, 217)';
							border1 = '1px solid rgb(0, 124, 48)';
						}
					}
					$('#MonthsRefiRehab').css({
						'border' : border1,
						'background' : background1,
					});
					if ( background2 == 'rgb(242, 222, 222)' ) {
						if ( prev2.css('background-color') == 'rgb(206, 234, 217)' ) {
							background2 = 'rgb(198, 238, 253)';
							border2 = '1px solid rgb(42, 108, 132)';
						} else {
							background2 = 'rgb(206, 234, 217)';
							border2 = '1px solid rgb(0, 124, 48)';
						}
					}
					$('#MonthsToRefi').css({
						'border' : border2,
						'background' : background2,
					});
				}
			}
		}
		if ( page == 'COMPARISON' ) {
			if ( $(this).hasClass('analyser1') ) {
				if ( $('#Used_1').val() == 'Yes' && $('#Used_2').val() == 'Yes' ) {
			if ( (parseFloat($('#DownPayment').val()) + parseFloat($('#LTV_1').val()) + parseFloat($('#LTV_2').val())) != 100 ) {
					$('#DownPayment, #LTV_1, #LTV_2').css({
						'border' : '1px solid #a94442',
						'background' : '#f2dede',
					});
					$('#LTVModal').modal('show');
						return false;
					} else {
						var background1 = $('#DownPayment').css('background-color'); var border1 = '';
						var background2 = $('#LTV_1').css('background-color'); var border2 = '';
						var background3 = $('#LTV_2').css('background-color'); var border3 = '';
						var prev1 = $('#DownPayment').parents('.panel-field').prev('.panel-field').find('.form-control');
						var prev2 = $('#LTV_1').parents('.panel-field').prev('.panel-field').find('.form-control');
						var prev3 = $('#LTV_2').parents('.panel-field').prev('.panel-field').find('.form-control');
						if ( background1 == 'rgb(242, 222, 222)' ) {
							if ( prev1.css('background-color') == 'rgb(206, 234, 217)' ) {
								background1 = 'rgb(198, 238, 253)';
								border1 = '1px solid rgb(42, 108, 132)';
							} else {
								background1 = 'rgb(206, 234, 217)';
								border1 = '1px solid rgb(0, 124, 48)';
							}
						}
						$('#DownPayment').css({
							'border' : border1,
							'background' : background1,
						});
						if ( background2 == 'rgb(242, 222, 222)' ) {
							if ( prev2.css('background-color') == 'rgb(206, 234, 217)' ) {
								background2 = 'rgb(198, 238, 253)';
								border2 = '1px solid rgb(42, 108, 132)';
							} else {
								background2 = 'rgb(206, 234, 217)';
								border2 = '1px solid rgb(0, 124, 48)';
							}
						}
						$('#LTV_1').css({
							'border' : border2,
							'background' : background2,
						});
						if ( background3 == 'rgb(242, 222, 222)' ) {
							if ( prev3.css('background-color') == 'rgb(206, 234, 217)' ) {
								background3 = 'rgb(198, 238, 253)';
								border3 = '1px solid rgb(42, 108, 132)';
							} else {
								background3 = 'rgb(206, 234, 217)';
								border3 = '1px solid rgb(0, 124, 48)';
							}
						}
						$('#LTV_2').css({
							'border' : border3,
							'background' : background3,
						});
					}
		} else if( $('#Used_1').val() == 'Yes' && $('#Used_2').val() == 'No' ) {
			if ( (parseFloat($('#DownPayment').val()) + parseFloat($('#LTV_1').val())) != 100 ) {
					$('#DownPayment, #LTV_1').css({
					    'border' : '1px solid #a94442',
						'background' : '#f2dede',
					});
					$('#LTVModal').modal('show');
					return false;
			} else {
				var background1 = $('#DownPayment').css('background-color'); var border1 = '';
				var background2 = $('#LTV_1').css('background-color'); var border2 = '';
				var prev1 = $('#DownPayment').parents('.panel-field').prev('.panel-field').find('.form-control');
				var prev2 = $('#LTV_1').parents('.panel-field').prev('.panel-field').find('.form-control');
				if ( background1 == 'rgb(242, 222, 222)' ) {
					if ( prev1.css('background-color') == 'rgb(206, 234, 217)' ) {
						background1 = 'rgb(198, 238, 253)';
						border1 = '1px solid rgb(42, 108, 132)';
					} else {
						background1 = 'rgb(206, 234, 217)';
						border1 = '1px solid rgb(0, 124, 48)';
					}
				}
				$('#DownPayment').css({
					'border' : border1,
					'background' : background1,
				});
				if ( background2 == 'rgb(242, 222, 222)' ) {
					if ( prev2.css('background-color') == 'rgb(206, 234, 217)' ) {
						background2 = 'rgb(198, 238, 253)';
						border2 = '1px solid rgb(42, 108, 132)';
					} else {
						background2 = 'rgb(206, 234, 217)';
						border2 = '1px solid rgb(0, 124, 48)';
					}
				}
				$('#LTV_1').css({
					'border' : border2,
					'background' : background2,
				});
			}
		} else if ( $('#Used_1').val() == 'No' && $('#Used_2').val() == 'Yes' ) {
			if ( (parseFloat($('#DownPayment').val()) + parseFloat($('#LTV_2').val())) != 100 ) {
				$('#DownPayment, #LTV_2').css({
					'border' : '1px solid #a94442',
					'background' : '#f2dede',
				});
				$('#LTVModal').modal('show');
				return false;
			} else {
				var background1 = $('#DownPayment').css('background-color'); var border1 = '';
				var background3 = $('#LTV_2').css('background-color'); var border3 = '';
				var prev1 = $('#DownPayment').parents('.panel-field').prev('.panel-field').find('.form-control');
				var prev3 = $('#LTV_2').parents('.panel-field').prev('.panel-field').find('.form-control');
				if ( background1 == 'rgb(242, 222, 222)' ) {
					if ( prev1.css('background-color') == 'rgb(206, 234, 217)' ) {
						background1 = 'rgb(198, 238, 253)';
						border1 = '1px solid rgb(42, 108, 132)';
					} else {
						background1 = 'rgb(206, 234, 217)';
						border1 = '1px solid rgb(0, 124, 48)';
					}
				}
				$('#DownPayment').css({
					'border' : border1,
					'background' : background1,
				});
				if ( background3 == 'rgb(242, 222, 222)' ) {
					if ( prev3.css('background-color') == 'rgb(206, 234, 217)' ) {
						background3 = 'rgb(198, 238, 253)';
						border3 = '1px solid rgb(42, 108, 132)';
					} else {
						background3 = 'rgb(206, 234, 217)';
						border3 = '1px solid rgb(0, 124, 48)';
					}
				}
				$('#LTV_2').css({
					'border' : border3,
					'background' : background3,
				});
			}
		} else {
			if ( parseFloat($('#DownPayment').val()) != 100 ) {
				$('#DownPayment').css({
					'border' : '1px solid #a94442',
					'background' : '#f2dede',
				});
				$('#LTVModal').modal('show');
				return false;
			} else {
				var background1 = $('#DownPayment').css('background-color'); var border1 = '';
				var prev1 = $('#DownPayment').parents('.panel-field').prev('.panel-field').find('.form-control');
				if ( background1 == 'rgb(242, 222, 222)' ) {
					if ( prev1.css('background-color') == 'rgb(206, 234, 217)' ) {
						background1 = 'rgb(198, 238, 253)';
						border1 = '1px solid rgb(42, 108, 132)';
					} else {
						background1 = 'rgb(206, 234, 217)';
						border1 = '1px solid rgb(0, 124, 48)';
					}
				}
				$('#DownPayment').css({
					'border' : border1,
					'background' : background1,
				});
			}
		}
			}
		}
		if ( page == 'RENTAL' ) {
			if ( $(this).hasClass('RentalStep7') ) {
				if ( $('#UseFinancing').val() == 'Yes' ) {
					if ( $('#FirstMortgageUsed').val() == 'Yes' && $('#SecondMortgageUsed').val() == 'Yes' ) {
						if ( (parseFloat($('#DownPaymentPercent').val()) + parseFloat($('#FirstMortgageLTVPercent').val()) + parseFloat($('#SecondMortgageLTVPercent').val())) != 100 ) {
							$('#DownPaymentPercent, #FirstMortgageLTVPercent, #SecondMortgageLTVPercent').css({
								'border' : '1px solid #a94442',
								'background' : '#f2dede',
							});
							$('#LTVModal').modal('show');
							return false;
						} else {
							var background1 = $('#DownPaymentPercent').css('background-color'); var border1 = '';
							var background2 = $('#FirstMortgageLTVPercent').css('background-color'); var border2 = '';
							var background3 = $('#SecondMortgageLTVPercent').css('background-color'); var border3 = '';
							var prev1 = $('#DownPaymentPercent').parents('.panel-field').prev('.panel-field').find('.form-control');
							var prev2 = $('#FirstMortgageLTVPercent').parents('.panel-field').prev('.panel-field').find('.form-control');
							var prev3 = $('#SecondMortgageLTVPercent').parents('.panel-field').prev('.panel-field').find('.form-control');
							if ( background1 == 'rgb(242, 222, 222)' ) {
								if ( prev1.css('background-color') == 'rgb(206, 234, 217)' ) {
									background1 = 'rgb(198, 238, 253)';
									border1 = '1px solid rgb(42, 108, 132)';
								} else {
									background1 = 'rgb(206, 234, 217)';
									border1 = '1px solid rgb(0, 124, 48)';
								}
							}
							$('#DownPaymentPercent').css({
								'border' : border1,
								'background' : background1,
							});
							if ( background2 == 'rgb(242, 222, 222)' ) {
								if ( prev2.css('background-color') == 'rgb(206, 234, 217)' ) {
									background2 = 'rgb(198, 238, 253)';
									border2 = '1px solid rgb(42, 108, 132)';
								} else {
									background2 = 'rgb(206, 234, 217)';
									border2 = '1px solid rgb(0, 124, 48)';
								}
							}
							$('#FirstMortgageLTVPercent').css({
								'border' : border2,
								'background' : background2,
							});
							if ( background3 == 'rgb(242, 222, 222)' ) {
								if ( prev3.css('background-color') == 'rgb(206, 234, 217)' ) {
									background3 = 'rgb(198, 238, 253)';
									border3 = '1px solid rgb(42, 108, 132)';
								} else {
									background3 = 'rgb(206, 234, 217)';
									border3 = '1px solid rgb(0, 124, 48)';
								}
							}
							$('#SecondMortgageLTVPercent').css({
								'border' : border3,
								'background' : background3,
							});
						}
					} else if ( $('#FirstMortgageUsed').val() == 'Yes' && $('#SecondMortgageUsed').val() == 'No' ) {
						if ( (parseFloat($('#DownPaymentPercent').val()) + parseFloat($('#FirstMortgageLTVPercent').val())) != 100 ) {
							$('#DownPaymentPercent, #FirstMortgageLTVPercent').css({
								'border' : '1px solid #a94442',
								'background' : '#f2dede',
							});
							$('#LTVModal').modal('show');
							return false;
						} else {
							var background1 = $('#DownPaymentPercent').css('background-color'); var border1 = '';
							var background2 = $('#FirstMortgageLTVPercent').css('background-color'); var border2 = '';
							var prev1 = $('#DownPaymentPercent').parents('.panel-field').prev('.panel-field').find('.form-control');
							var prev2 = $('#FirstMortgageLTVPercent').parents('.panel-field').prev('.panel-field').find('.form-control');
							if ( background1 == 'rgb(242, 222, 222)' ) {
								if ( prev1.css('background-color') == 'rgb(206, 234, 217)' ) {
									background1 = 'rgb(198, 238, 253)';
									border1 = '1px solid rgb(42, 108, 132)';
								} else {
									background1 = 'rgb(206, 234, 217)';
									border1 = '1px solid rgb(0, 124, 48)';
								}
							}
							$('#DownPaymentPercent').css({
								'border' : border1,
								'background' : background1,
							});
							if ( background2 == 'rgb(242, 222, 222)' ) {
								if ( prev2.css('background-color') == 'rgb(206, 234, 217)' ) {
									background2 = 'rgb(198, 238, 253)';
									border2 = '1px solid rgb(42, 108, 132)';
								} else {
									background2 = 'rgb(206, 234, 217)';
									border2 = '1px solid rgb(0, 124, 48)';
								}
							}
							$('#FirstMortgageLTVPercent').css({
								'border' : border2,
								'background' : background2,
							});
						}
					} else if ( $('#FirstMortgageUsed').val() == 'No' && $('#SecondMortgageUsed').val() == 'Yes' ) {
						if ( (parseFloat($('#DownPaymentPercent').val()) + parseFloat($('#SecondMortgageLTVPercent').val())) != 100 ) {
							$('#DownPaymentPercent, #SecondMortgageLTVPercent').css({
								'border' : '1px solid #a94442',
								'background' : '#f2dede',
							});
							$('#LTVModal').modal('show');
							return false;
						} else {
							var background1 = $('#DownPaymentPercent').css('background-color'); var border1 = '';
							var background3 = $('#SecondMortgageLTVPercent').css('background-color'); var border3 = '';
							var prev1 = $('#DownPaymentPercent').parents('.panel-field').prev('.panel-field').find('.form-control');
							var prev3 = $('#SecondMortgageLTVPercent').parents('.panel-field').prev('.panel-field').find('.form-control');
							if ( background1 == 'rgb(242, 222, 222)' ) {
								if ( prev1.css('background-color') == 'rgb(206, 234, 217)' ) {
									background1 = 'rgb(198, 238, 253)';
									border1 = '1px solid rgb(42, 108, 132)';
								} else {
									background1 = 'rgb(206, 234, 217)';
									border1 = '1px solid rgb(0, 124, 48)';
								}
							}
							$('#DownPaymentPercent').css({
								'border' : border1,
								'background' : background1,
							});
							if ( background3 == 'rgb(242, 222, 222)' ) {
								if ( prev3.css('background-color') == 'rgb(206, 234, 217)' ) {
									background3 = 'rgb(198, 238, 253)';
									border3 = '1px solid rgb(42, 108, 132)';
								} else {
									background3 = 'rgb(206, 234, 217)';
									border3 = '1px solid rgb(0, 124, 48)';
								}
							}
							$('#SecondMortgageLTVPercent').css({
								'border' : border3,
								'background' : background3,
							});
						}
					}
				} else {
					$('#DownPaymentPercent').val(100);
					/*if ( parseFloat($('#DownPaymentPercent').val()) != 100 ) {
						$('#DownPaymentPercent').css({
							'border' : '1px solid #a94442',
							'background' : '#f2dede',
						});
						$('#LTVModal').modal('show');
						return false;
					} else {
						var background1 = $('#DownPaymentPercent').css('background-color'); var border1 = '';
						var prev1 = $('#DownPaymentPercent').parents('.panel-field').prev('.panel-field').find('.form-control');
						if ( background1 == 'rgb(242, 222, 222)' ) {
							if ( prev1.css('background-color') == 'rgb(206, 234, 217)' ) {
								background1 = 'rgb(198, 238, 253)';
								border1 = '1px solid rgb(42, 108, 132)';
							} else {
								background1 = 'rgb(206, 234, 217)';
								border1 = '1px solid rgb(0, 124, 48)';
							}
						}
						$('#DownPaymentPercent').css({
							'border' : border1,
							'background' : background1,
						});
					}*/
				}
			}
		}
		
		if ( error.indexOf('error') == -1 ) {
			$(this).parent().parent().parent().parent().slideUp(500);
			$(this).parent().parent().parent().parent().next('.full-section-box').slideDown(500).find('.assump-box-child:eq(0)').slideDown(500).find('.form-control:eq(0)').focus();
			$("html, body").animate({scrollTop: 0}, 500);
			btn_clicked = true;
			var $pb = $('.progress .progress-bar');
			var $value = $(this).attr('data-rel');
			$pb.attr('data-transitiongoal', $value).progressbar({
				display_text: 'center'
			});
		} else {
			show_error_message('Fields are required!');
			return false;
		}
	});
	$('.offer-calculator-screen .on_prev_section').on('click', function() {
		$(this).parent().parent().parent().parent().slideUp(500);
		$(this).parent().parent().parent().parent().prev('.full-section-box').slideDown(500).find('.assump-box-child:last').slideDown(500).find('.form-control:first').focus();
		$("html, body").animate({scrollTop: 0}, 500);
		btn_clicked = true;
	});	
	
	$('.offer-calculator-screen .Required').on('change blur focus', function() {
		if ( $(this).val() != false ) {
			var background = $(this).css('background-color'); var border = '';
			var prev = $(this).parents('.panel-field').prev('.panel-field').find('.form-control');
			if ( background == 'rgb(242, 222, 222)' ) {
				if ( prev.css('background-color') == 'rgb(206, 234, 217)' ) {
					background = 'rgb(198, 238, 253)';
					border = '1px solid rgb(42, 108, 132)';
				} else {
					background = 'rgb(206, 234, 217)';
					border = '1px solid rgb(0, 124, 48)';
				}
			}
			if ( $(this).is('[readonly]') ) {
				if ( $(this).attr('id') == 'PropertyClosingDate' ) {
					$(this).css({
						'border' : border,
						'background' : background,
					});
				} else {
					$(this).css({
						'border' : border,
						'background' : background,
					});
				}
			} else {
				$(this).css({
					'border' : border,
					'background' : background,
				});
			}
		}
	});
	
	$('.offer-calculator-screen .on_save_event').on('click', function() {
		var form = $(this).parents('form');
		var error = [];
		$(form).find('.Required').each(function(index) {
			if ( $(this).val() == false ) {
				$(this).css({
					'border' : '1px solid #a94442',
					'background' : '#f2dede',
				});
				error.push('error');
			} else {
				var background = $(this).css('background-color'); var border = '';
				var prev = $(this).parents('.panel-field').prev('.panel-field').find('.form-control');
				if ( background == 'rgb(242, 222, 222)' ) {
					if ( prev.css('background-color') == 'rgb(206, 234, 217)' ) {
						background = 'rgb(198, 238, 253)';
						border = '1px solid rgb(42, 108, 132)';
					} else {
						background = 'rgb(206, 234, 217)';
						border = '1px solid rgb(0, 124, 48)';
					}
				}
				$(this).css({
					'border' : border,
					'background' : background,
				});
			}
		});
		
		if ( error.indexOf('error') != -1 ) {
			show_error_message('Fields are required!');
			return false;
		}
		
		var count = 0;
		var imgError = new Array();
		var page = $('#page_type').val();
		var flag = new Array;
		var PageStatus = $('#PageStatus').val();
		var Images = $.trim( $('.images').val() );
		
		if ( select_imgs == false ) {
			$('#ProposalStatus').val('publish');
		} else {
			$('#ProposalStatus').val('draft');
		}
		
		if ( PageStatus == 'INSERT' ) {
			if(page!='COMPARISON'){
				if ( select_imgs == false ) {
					if ( !Images ) {
						var con = confirm('You are going to submit your proposal without photos. Don\'t worry you can upload photos later. Do you wish to continue?');
					} else {
						var con = true;
					}
				} else {
					var con = true;
				}
			}
			/*if ( con == true ) {
				if ( Images ) {
					for ( var i = 0; i < $('.images').get(0).files.length; i++ ) {
						var img = $('.images').get(0).files[i].name;
						var size = $('.images').get(0).files[i].size;
						var extension = img.split('.').pop().toUpperCase();
						if ( extension != "PNG" && extension != "JPG" && extension != "GIF" && extension != "JPEG" && extension != "BMP" ) {
							imgError.push('ERROR: Invalid image selected in field ' + (i+1) + '. Valid image types: JPG, PNG, GIF & BMP');
							count = count + 1;
						}
						if ( size > 1048576 ) {
							imgError.push('ERROR: Allowed file size exceeded in field ' + (i+1) + '. Max. 1MB');
							count = count + 1;
						}
					}
				}
			} else {
				return false;
			}
			
			if ( count > 0 ) {
				btn_clicked = true;
				alert(imgError);
				return false;
			}*/
		} else {
			/*if ( Images ) {
				for ( var i = 0; i < $('.images').get(0).files.length; i++ ) {
					var img = $('.images').get(0).files[i].name;
					var size = $('.images').get(0).files[i].size;
					var extension = img.split('.').pop().toUpperCase();
					if ( extension != "PNG" && extension != "JPG" && extension != "GIF" && extension != "JPEG" && extension != "BMP" ) {
						imgError.push('ERROR: Invalid image selected in field ' + (i+1) + '. Valid image types: JPG, PNG, GIF & BMP');
						count = count + 1;
					}
					if ( size > 1048576 ) {
						imgError.push('ERROR: Allowed file size exceeded in field ' + (i+1) + '. Max. 1MB');
						count = count + 1;
					}
				}
			}
			
			if ( count > 0 ) {
				btn_clicked = true;
				alert(imgError);
				return false;
			}*/
		}
		
		var $pb = $('.progress .progress-bar');
		var $value = $(this).attr('data-rel');
		$pb.attr('data-transitiongoal', $value).progressbar({
			display_text: 'center'
		});
		
		$('.loading').removeClass('hide');
		var elem = $(this);
		$('.on_save_event').attr('disabled', 'disabled');
		
		$('form[name="PictureForm"]').ajaxForm().submit();

		$('form[name="FlipBudgetForm"]').each(function (index) {
            var $form = $(this);
			var dataString = $form.serialize();
			$.ajax({
				type: "POST",
				url: base_url + "pages/flip-budget-ajax/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					// 
				}
			});
		});

		$('form[name="RefiBudgetForm"]').each(function (index) {
            var $form = $(this);
			var dataString = $form.serialize();
			$.ajax({
				type: "POST",
				url: base_url + "pages/refi-budget-ajax/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					// 
				}
			});
		});
		
		$('form[name="PropertyForm"]').each(function (index) {
            var $form = $(this);
			var dataString = $form.serialize();
			$.ajax({
				type: "POST",
				url: base_url + "pages/ajax-store/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					flag[index] = data;
					if ( page == 'PROPOSAL' ) {
						if ( index == 1 ) {
							var error = flag.indexOf('error');
							if ( error == -1 ) {
								btn_clicked = false;
								$('.on_save_event').text('SAVING');
								if ( !btn_clicked ) {
									setTimeout(function() {
										window.location.href = base_url + 'pages/proposals/';
										return false;
									}, 2000);
								}
							} else {
								alert('Something went wrong, please try again!');
								btn_clicked = true;
								return false;
							}
						}
					} else if ( page == 'COMPARISON' ) {
						if ( index == 1 ) {
							var error = flag.indexOf('error');
							if ( error == -1 ) {
								btn_clicked = false;
								$('.on_save_event').attr('disabled', 'disabled');
								$('.on_save_event').text('SAVING');
								if ( !btn_clicked ) {
									setTimeout(function() {
										window.location.href = base_url + 'pages/comparisons/';
										return false;
									}, 2000);
								}
							} else {
								alert('Something went wrong, please try again!');
								btn_clicked = true;
								return false;
							}
						}
					} else if ( page == 'RENTAL' ) {
						if ( index == 1 ) {
							var error = flag.indexOf('error');
							if ( error == -1 ) {
								btn_clicked = false;
								$('.on_save_event').attr('disabled', 'disabled');
								$('.on_save_event').text('SAVING');
								if ( !btn_clicked ) {
									setTimeout(function() {
										window.location.href = base_url + 'pages/rental-valuator/';
										return false;
									}, 2000);
								}
							} else {
								alert('Something went wrong, please try again!');
								btn_clicked = true;
								return false;
							}
						}
					} else if ( page == 'MAX OFFER' ) {
						if ( index == 1 ) {
							var error = flag.indexOf('error');
							if ( error == -1 ) {
								btn_clicked = false;
								$('.on_save_event').attr('disabled', 'disabled');
								$('.on_save_event').text('SAVING');
								if ( !btn_clicked ) {
									setTimeout(function() {
										window.location.href = base_url + 'pages/max-offers/';
										return false;
									}, 2000);
								}
							} else {
								alert('Something went wrong, please try again!');
								btn_clicked = true;
								return false;
							}
						}
					}
				}
			});
		});
	});
	
	// Rental Modal Close
	$('#HelpfullCalcReturn').click(function() {
		$('#HelpfulCalcModal').modal('hide');
    });
	
	// Rental Modal Close
	$('#HelpfullCalc1Return').click(function() {
		$('#HelpfulCalcModal1').modal('hide');
    });
	
	// Rental Modal Close
	$('#AdvaceModalReturn').click(function() {
		$('#MoreOptionModal').modal('hide');
    });
	
	// Rental Modal Close
	$('#GrowthModalReturn').click(function() {
		$('#ManualGrowthRateModal').modal('hide');
    });
	
	
	// Closing Cost Modal Close
	$('#ClosingCostsReturn').click(function() {
		var TotalPurchaseClosingCosts = $('#TotalPurchaseClosingCosts').val() ? parseFloat($('#TotalPurchaseClosingCosts').val()) : 0;
		TotalPurchaseClosingCosts = TotalPurchaseClosingCosts ? parseFloat(TotalPurchaseClosingCosts) : 0;
		if ( $('#ClosingCostsOption').val() == 'Quick Lump Sum' ) {
			$('#ClosingCosts').val(TotalPurchaseClosingCosts.toFixed(2));
		} else if ( $('#ClosingCostsOption').val() == 'Detailed Input' ) {
			$('#ClosingCosts').val(TotalPurchaseClosingCosts.toFixed(2));
		}
		$('#closing-tags').html($('#ClosingCostsOption').val());
		$('#ClosingCostsModal').modal('hide');
    });
	
	// Holding Cost Modal Close
	$('#HoldingCostsReturn').click(function() {
		var TotalPurchaseHoldingCosts = $('#TotalPurchaseHoldingCosts').val() ? parseFloat($('#TotalPurchaseHoldingCosts').val()) : 0;
		TotalPurchaseHoldingCosts = TotalPurchaseHoldingCosts ? parseFloat(TotalPurchaseHoldingCosts) : 0;
		if ( $('#HoldingCostsOption').val() == 'Quick Lump Sum' ) {
			$('#HoldingCosts').val(TotalPurchaseHoldingCosts.toFixed(2));
		} else if ( $('#HoldingCostsOption').val() == 'Detailed Input' ) {
			$('#HoldingCosts').val(TotalPurchaseHoldingCosts.toFixed(2) + '/month');
		}
		$('#holding-tags').html($('#HoldingCostsOption').val());
		$('#HoldingCostsModal').modal('hide');
    });
	
	// Flip Analysis Modal Close
	$('#FlipRehubReturn').click(function() {
		if ( $('#FlipRehabBudgetMethod').val() == 'Detailed Input' ) {
			var DetailedFlipBudget = $('#DetailedFlipBudget').val() ? parseFloat($('#DetailedFlipBudget').val()) : 0;
			$('#ActualFlipBudget').val(DetailedFlipBudget.toFixed(2));
		} else {
			var LumpSumFlipBudget = $('#LumpSumFlipBudget').val() ? parseFloat($('#LumpSumFlipBudget').val()) : 0;
			$('#ActualFlipBudget').val(LumpSumFlipBudget.toFixed(2));
		}
		$('#FlipRehubModal').modal('hide');
		return false;
    });
	
	// Refi Analysis Modal Close
	$('#RefiRehubReturn').click(function() {
		if ( $('#RefiRehabBudgetMethod').val() == 'Detailed Input' ) {
			var DetailedRefiBudget = $('#DetailedRefiBudget').val() ? parseFloat($('#DetailedRefiBudget').val()) : 0;
			$('#ActualRefiBudget').val(DetailedRefiBudget.toFixed(2));
		} else {
			var LumpSumRefiBudget = $('#LumpSumRefiBudget').val() ? parseFloat($('#LumpSumRefiBudget').val()) : 0;
			$('#ActualRefiBudget').val(LumpSumRefiBudget.toFixed(2));
		}
		$('#RefiRehubModal').modal('hide');
		return false;
    });
	
	// Operating Income Modal Close
	$('#OperatingIncomeReturn').click(function() {
		var MonthlyGrossOperatingIncome = $('#MonthlyGrossOperatingIncome').val() ? parseFloat($('#MonthlyGrossOperatingIncome').val()) : 0;
		$('#OperatingIncome').val(MonthlyGrossOperatingIncome.toFixed(2));
		$('#OperatingIncomeModal').modal('hide');
		$('#OperatingExpensesModal').modal('show');
		return false;
    });
	
	// Operating Expenses Modal Close
	$('#OperatingExpensesReturn').click(function() {
		var OperatingExpensesMonthlyTotal = $('#OperatingExpensesMonthlyTotal').val() ? parseFloat($('#OperatingExpensesMonthlyTotal').val()) : 0;
		$('#OperatingExpenses').val(OperatingExpensesMonthlyTotal.toFixed(2));
		$('#OperatingExpensesModal').modal('hide');
		return false;
    });
	
	// FlipRehubModal Auto Margin
	$("#FlipRehubModal").css({
		'margin-left': function () {
			return -($(this).width() / 3);
		}
	});
	// RefiRehubModal Auto Margin
	$("#RefiRehubModal").css({
		'margin-left': function () {
			return -($(this).width() / 3);
		}
	});
	
	// MoreOptionModal Auto Margin
	$("#MoreOptionModal").css({
		'margin-left': function () {
			return -($(this).width() / 4);
		}
	});
	
	// ManualGrowthRateModal Auto Margin
	$("#ManualGrowthRateModal").css({
		'margin-left': function () {
			return -($(this).width() / 4);
		}
	});
	
	// Print Modal
	$('#FullFlip').click(function() {
		$('.print-option').removeAttr('checked');
        $('input[name="CoverPage"]').prop('checked', 'checked');
		$('input[name="FlipMarketingSheet"]').prop('checked', 'checked');
		$('input[name="FlipCashFlow"]').prop('checked', 'checked');
		$('input[name="CompsReport"]').prop('checked', 'checked');
		$('input[name="PicsPage"]').prop('checked', 'checked');
    });
	$('#FullRefi').click(function() {
		$('.print-option').removeAttr('checked');
        $('input[name="CoverPage"]').prop('checked', 'checked');
		$('input[name="RefiMarketingSheet"]').prop('checked', 'checked');
		$('input[name="RefiCashFlow"]').prop('checked', 'checked');
		$('input[name="CompsReport"]').prop('checked', 'checked');
		$('input[name="PicsPage"]').prop('checked', 'checked');
    });
	$('#FullFlipLender').click(function() {
		$('.print-option').removeAttr('checked');
        $('input[name="CoverPage"]').prop('checked', 'checked');
		$('input[name="FlipFundingRequest"]').prop('checked', 'checked');
		$('input[name="FlipCashFlowLender"]').prop('checked', 'checked');
		$('input[name="CompsReport"]').prop('checked', 'checked');
		$('input[name="PicsPage"]').prop('checked', 'checked');
    });
	$('#FullRefiLender').click(function() {
		$('.print-option').removeAttr('checked');
        $('input[name="CoverPage"]').prop('checked', 'checked');
		$('input[name="RefiFundingRequest"]').prop('checked', 'checked');
		$('input[name="RefiCashFlowLender"]').prop('checked', 'checked');
		$('input[name="CompsReport"]').prop('checked', 'checked');
		$('input[name="PicsPage"]').prop('checked', 'checked');
    });
	
	//	print proposal 
	$('.btn_proposal_print').click(function(){
		var user_id = $(this).attr('data-id');
		var propertyId = $(this).attr('data-propertyId');
		var type = $(this).attr('data-type');
		var print_url = base_url + "pages/print-report/"+ propertyId + "/";
		if ( $('.print-feature:checked').length == 0 ) {
			$('.print-error').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Please select atleast one report before clicking the button.</span>');
			return false;
		}
		var feature = '';
		$('.print-feature').each(function(index){
			if ( $(this).prop("checked") ) {
				feature = (feature ? feature + '-' : '') + $(this).attr("name");
			}
		});
		$.ajax({
			type: 'POST',
			url: base_url + 'pages/checkuserpaid/' + user_id + '/',
			data: {},
			success: function(data) {
				if ( data == 0 ) {
					if ( type == 'proposal' ) {
						$('#reports').modal('hide');
					} else if ( type == 'rental' ) {
						$('#rental_reports').modal('hide');
					}
					$('#upgrade_modal').modal('show');
					$('.print-feature').removeAttr('checked');
				} else {
					OpenInNewTab(print_url + feature + '/');
				}
			},
		});
	});
	
	//	print proposal 
	$('.btn_proposal_download').click(function(){
		var user_id = $(this).attr('data-id');
		var propertyId = $(this).attr('data-propertyId');
		var type = $(this).attr('data-type');
		var download_url = base_url + "pages/download-report/"+ propertyId + "/";
		if ( $('.print-feature:checked').length == 0 ) {
			$('.print-error').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Please select reports before clicking the download button.</span>');
			return false;
		}
		var feature = '';
		$('.print-feature').each(function(index){
			if ( $(this).prop("checked") ) {
				feature = (feature ? feature + '-' : '') + $(this).attr("name");
			}
		});
		$.ajax({
			type: 'POST',
			url: base_url + 'pages/checkuserpaid/' + user_id + '/',
			data: {},
			success: function(data) {
				if ( data == 0 ) {
					if ( type == 'proposal' ) {
						$('#reports').modal('hide');
					} else if ( type == 'rental' ) {
						$('#rental_reports').modal('hide');
					}
					$('#upgrade_modal').modal('show');
					$('.print-feature').removeAttr('checked');
				} else {
					OpenInNewTab(download_url + feature + '/');
				}
			},
		});
	});
	
	//	print proposal 
	$('.btn_proposal_email').click(function(){
		$(this).attr('disabled', 'disabled');
		var ajax_request = [];
		var user_id = $(this).attr('data-id');
		var propertyId = $(this).attr('data-propertyId');
		var type = $(this).attr('data-type');
		if ( $('.print-feature:checked').length == 0 ) {
			$('.print-error').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Please select atleast one report before clicking the email button.</span>');
			return false;
		}
		if ( type == 'proposal' ) {
			$('#reports').modal('hide');
		} else if ( type == 'rental' ) {
			$('#rental_reports').modal('hide');
		}
		$('.print-feature').each(function(index){
			if ( $(this).prop("checked") ) {
				var feature = $(this).attr("name");
				$.ajax({
					type: 'POST',
					url: base_url + 'pages/checkuserpaid/' + user_id + '/',
					data: {},
					success: function(data) {
						if ( data == 0 ) {
							if ( type == 'proposal' ) {
								$('#reports').modal('hide');
							} else if ( type == 'rental' ) {
								$('#rental_reports').modal('hide');
							}
							$('#upgrade_modal').modal('show');
							$('.print-feature').removeAttr('checked');
						} else {
							var ajax_req = $.ajax({
								type: 'POST',
								dataType: "json",
								url: base_url + "pages/download-report/"+ propertyId + "/"+ feature + "/save/" + type + "/",
								data: {},
								success: function(data) {
									$('#modalEmail').modal('show');
									$('.modal-loading').html('<span class="text-danger"><img src="' + base_url + 'assets/images/loading.gif" alt="loading" /> Please wait, attaching the reports to your email.</span>');
									$('#attchments_files').append('<input type="hidden" name="attachments[]" value="' + data.pdf_path + '" />');
									$('#attchments_views').append('<div class="col-lg-3 pdf-attach"><a target="_blank" href="' + data.pdf_url + '"><i class="fa fa-file-pdf-o"></i> ' + data.pdf_name + '</a></div>');
									$('#inputSubject').val(data.propertyName + ' - CBList Reports');
								},
							});
							ajax_request.push(ajax_req);
						}
					},
				});				
			}
			var process_interval = setInterval(function() {
				var process_complete_flag = true;
				$.each(ajax_request, function(index, value) {
					if ( value.state() == "pending" ) {
						process_complete_flag = false;
					}
				});
				if ( process_complete_flag ) {
					$('.modal-loading').html('<span class="text-success"><i class="fa fa-check-square-o"></i> Reports are successfully attached.</span>');
					$(this).removeAttr('disabled');
					setTimeout(function() {
						$('.modal-loading').html('');
						clearInterval(process_interval);
					}, 10000);
				}
			}, 1000);
		});
	});
	
	$('#btn_email_send').click(function() {
		var inputTo = $('#inputTo').val();
		if ( inputTo == '' || inputTo == false ) {
			$('.modal-loading').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Please specify at least one recipient.</span>');
			return false;
		}
		
		$('.modal-loading').html('<span class="text-danger"><img src="' + base_url + 'assets/images/loading.gif" alt="loading" /> Please wait, sending email.</span>');
		var dataString = $('#email_form').serialize();
		$.ajax({
			type: 'POST',
			url: base_url + "pages/email-proposal/",
			data: dataString,
			success: function(data) {
				if ( data == 'sent' ) {
					$('#email_form')[0].reset();
					$('#attchments_views').html('');
					$('#attchments_files').html('');
					$('.modal-loading').html('<span class="text-success"><i class="fa fa-check-square-o"></i> Email successfully sent.</span>');
					setTimeout(function() {
						$('.modal-loading').html('');
					}, 10000);
				} else {
					$('.modal-loading').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Something went wrong, please try again.</span>');
				}
			},
		});
    });
	
	$('#btn_email_cancel').click(function() {
        $('#modalEmail').modal('hide');
		var type = $(this).attr('data-type');
		if ( type == 'proposal' ) {
			$('#reports').modal('show');
		} else if ( type == 'rental' ) {
			$('#rental_reports').modal('show');
		}
		$('.print-feature').removeAttr('checked');
    });
	
	$('.btn_proposal_cancel').click(function() {
        $('.print-feature').removeAttr('checked');
		$('.print-error').html('');
    });
	
	$('.print-feature').click(function(e) {
        $('.print-error').html('');
    });
	
	
	// tooltip functions
	$('body').popover({
		selector	: '[data-popover]', 
		trigger		: 'onfocus onclick hover', 
		placement	: 'right', 
		html		: true, 
		delay		: {
			show	: 1, 
			hide	: 1
		}
	});
	
	/*$('.percentageField').on('change', function() {
		$(this).val($(this).val().toFixed(0));
	});*/
	
	$('.percentageField').on('change blur', function(){
		if ( $(this).val() < 0 || $(this).val() > 100 ) {
			$(this).val(0);
			show_error_message('Percentage should be 0 to 100');
			return false;
		}
	});
	
	$('#Designation').change(function(){
		if($("#Designation").val() != "Active Participant"){
			$('#GAIRow').addClass('hide');
		} else {
			if($('#GAIRow').hasClass('hide')){
				$('#GAIRow').removeClass('hide');
			}
		}		
	});
	
	$('#Used_1').on('change',function(){
		if($('#Used_1').val() == "Yes"){
			if(!$('#LTV_1').hasClass('Required')){
				$('#LTV_1').addClass('Required');
			}
			if(!$('#Rate_1').hasClass('Required')){
				$('#Rate_1').addClass('Required');
			}
			if(!$('#OriginationFee_1').hasClass('Required')){
				$('#OriginationFee_1').addClass('Required');
			}
			if(!$('#DiscountFee_1').hasClass('Required')){
				$('#DiscountFee_1').addClass('Required');
			}	    
		} else {
			if($('#LTV_1').hasClass('Required')){
				$('#LTV_1').removeClass('Required');
			}
			if($('#Rate_1').hasClass('Required')){
				$('#Rate_1').removeClass('Required');
			}
			if($('#OriginationFee_1').hasClass('Required')){
				$('#OriginationFee_1').removeClass('Required');
			}
			if($('#DiscountFee_1').hasClass('Required')){
				$('#DiscountFee_1').removeClass('Required');
			}	
		}
	});
	$('#UseFinancing').on('change',function(){
		if ( $('#UseFinancing').val() == 'No' ) {
			$('#DownPaymentPercent').val(100);
		}
	});
	$('#Used_2').on('change',function(){
		if($('#Used_2').val() == "Yes"){
			if($('#LoanType_2').hasClass('hide')){
				$('#LoanType_2').removeClass('hide');
			}
			if($('#TermYears_2').hasClass('hide')){
				$('#TermYears_2').removeClass('hide');
			}
			if($('#LTV_2').parent().hasClass('hide')){
				$('#LTV_2').parent().removeClass('hide');
			}
			if(!$('#LTV_2').hasClass('Required')){
				$('#LTV_2').addClass('Required');
			}
			if($('#Rate_2').parent().hasClass('hide')){
				$('#Rate_2').parent().removeClass('hide');
			}
			if(!$('#Rate_2').hasClass('Required')){
				$('#Rate_2').addClass('Required');
			}
			if($('#OriginationFee_2').parent().hasClass('hide')){
				$('#OriginationFee_2').parent().removeClass('hide');
			}
			if(!$('#OriginationFee_2').hasClass('Required')){
				$('#OriginationFee_2').addClass('Required');
			}
			if($('#DiscountFee_2').parent().hasClass('hide')){
				$('#DiscountFee_2').parent().removeClass('hide');
			}
			if(!$('#DiscountFee_2').hasClass('Required')){
				$('#DiscountFee_2').addClass('Required');
			}
		} else {
			$('#LoanType_2').addClass('hide');
			$('#TermYears_2').addClass('hide');
			$('#LTV_2').parent().addClass('hide');
			if($('#LTV_2').hasClass('Required')){
				$('#LTV_2').removeClass('Required');
			}
			$('#Rate_2').parent().addClass('hide');
			if($('#Rate_2').hasClass('Required')){
				$('#Rate_2').removeClass('Required');
			}
			$('#OriginationFee_2').parent().addClass('hide');
			if($('#OriginationFee_2').hasClass('Required')){
				$('#OriginationFee_2').removeClass('Required');
			}
			$('#DiscountFee_2').parent().addClass('hide');
			if($('#DiscountFee_2').hasClass('Required')){
				$('#DiscountFee_2').removeClass('Required');
			}
		}
	});
	
	$('.SetCover').on('click', function() {
		$('.CoverImages').val('off');
		$(this).parent().find('.CoverImages').val('on');
	});
});


function ValidateFileUpload(elem) {
	var fuData = document.getElementById(elem);
	var FileUploadPath = fuData.value;
	
	if ( FileUploadPath == '' ) {
		alert("Please upload an image");
	} else {
		var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
		if ( Extension == "gif" || Extension == "png" || Extension == "bmp" || Extension == "jpeg" || Extension == "jpg" ) {
			if ( fuData.files && fuData.files[0] ) {
				var reader = new FileReader();
				reader.onload = function(e) {
					$('#Preview_' + elem).attr('src', e.target.result);
					$('#Preview_' + elem).next('textarea').removeAttr('disabled');
					$('#Preview_' + elem).prev('.Check_Cover').find('input[type="radio"]').removeAttr('disabled');
					$('#Preview_' + elem).prev('.Check_Cover').find('input[type="hidden"]').removeAttr('disabled');
				}
				reader.readAsDataURL(fuData.files[0]);
			}
		} else {
			fuData.value = '';
			$('#ImageErrorModal').modal('show');
			return false;
		}
	}
}