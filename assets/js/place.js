// Start of Google Map Address search scripts
function addressAutoComplete() {
	if ( $( '#map-canvas' ).length ) {
		var mapOptions = {
			center: new google.maps.LatLng(40.0583238, -74.4056612),
			zoom: 8,
		};
	
		var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		if ( $( '#property_add' ).length ) {
			var input = (document.getElementById('property_add'));
		}
		if ( $( '#property_address' ).length ) {
			var input = (document.getElementById('property_address'));
		}
		
		map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.bindTo('bounds', map);
		
		var infowindow = new google.maps.InfoWindow();
		
		var marker = new google.maps.Marker({
			map: map,
			anchorPoint: new google.maps.Point(0, -29)
		});
		
		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			infowindow.close();
			marker.setVisible(false);
			
			var place = autocomplete.getPlace();
			if (!place.geometry) {
				return;
			}
			
			if (place.geometry.viewport) {
				map.fitBounds(place.geometry.viewport);
			} else {
				map.setCenter(place.geometry.location);
				map.setZoom(12);
			}
			
			marker.setPosition(place.geometry.location);
			marker.setVisible(true);
			
			var address = '';
			if (place.address_components) {
				address = [
					(place.address_components[0] && place.address_components[0].short_name || ''),
					(place.address_components[1] && place.address_components[1].short_name || ''),
					(place.address_components[2] && place.address_components[2].short_name || '')
				].join(' ');
			}
			
			infowindow.setContent('<strong>' + input.value + '</strong>');
			infowindow.open(map, marker);
		});
	}
}
google.maps.event.addDomListener(window, 'load', addressAutoComplete);
// End of function

// Start of default address with page load
function addressDefaultSelect() {
	if ( $( '#property_add' ).length ) {
		// Start of Public Profile Page Property Address
		var myOptions = {
			center: new google.maps.LatLng(40.0583238, -74.4056612),
			zoom: 8,
		};
		
		var map = new google.maps.Map($('#map-canvas')[0], myOptions);
		
		var addresses = [];
		$('input[name="property_add"]').each(function(index, element) {
			addresses.push($(element).val());
		});
		
		var infowindow = new google.maps.InfoWindow();
		var bounds = new google.maps.LatLngBounds();
		
		for ( var x = 0; x < addresses.length; x++ ) {
			$.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+addresses[x]+'&sensor=false', null, function (data) {
				var p = data.results[0].geometry.location;
				var latlng = new google.maps.LatLng(p.lat, p.lng);
				var marker = new google.maps.Marker({
					position: latlng,
					map: map,
					title: data.results[0].formatted_address,
				});
				
				//extend the bounds to include each marker's position
				bounds.extend(marker.position);
				
				google.maps.event.addListener(marker, 'click', (function (marker, i) {
					return function () {
						infowindow.setContent('<strong>' + data.results[0].formatted_address + '</strong>');
						infowindow.open(map, marker);
					}
				})(marker, i));
			});
		}
		// End of Public Profile Page Property Address
		
		//now fit the map to the newly inclusive bounds
		map.fitBounds(bounds);
		
		//(optional) restore the zoom level after the map is done scaling
		var listener = google.maps.event.addListener(map, "idle", function () {
			map.setZoom(8);
			google.maps.event.removeListener(listener);
		});
	}
}
google.maps.event.addDomListener(window, 'load', addressDefaultSelect);
// End of function

// Start of address search for filter 
/*function filterAddressSearch() {
	if ( $( '#InvestorLocation' ).length ) {
		autocomplete = new google.maps.places.Autocomplete(document.getElementById('InvestorLocation'), { types: [ 'geocode' ] });
		google.maps.event.addListener(autocomplete, 'place_changed', function() {
			// Nothing to do
		});
	}
}
google.maps.event.addDomListener(window, 'load', filterAddressSearch);*/
// End of function

// Start of multiple address on google map
$(document).ready(function(e) {
	if ( $( '#map_canvas' ).length ) {
		// Start of Public Profile Page Property Address
		var myOptions = {
			center: new google.maps.LatLng(40.0583238, -74.4056612),
			zoom: 8,
		};
		
		var map = new google.maps.Map($('#map_canvas')[0], myOptions);
		
		var addresses = [];
		$('input[name="property_add"]').each(function(index, element) {
			addresses.push($(element).val());
		});
		
		var infowindow = new google.maps.InfoWindow();
		var bounds = new google.maps.LatLngBounds();
		
		for ( var x = 0; x < addresses.length; x++ ) {
			$.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address='+addresses[x]+'&sensor=false', null, function (data) {
				var p = data.results[0].geometry.location;
				var latlng = new google.maps.LatLng(p.lat, p.lng);
				var marker = new google.maps.Marker({
					position: latlng,
					map: map,
					title: data.results[0].formatted_address,
				});
				
				//extend the bounds to include each marker's position
				bounds.extend(marker.position);
				
				google.maps.event.addListener(marker, 'click', (function (marker, i) {
					return function () {
						infowindow.setContent('<strong>' + data.results[0].formatted_address + '</strong>');
						infowindow.open(map, marker);
					}
				})(marker, i));
			});
		}
		// End of Public Profile Page Property Address
		
		//now fit the map to the newly inclusive bounds
		map.fitBounds(bounds);
		
		//(optional) restore the zoom level after the map is done scaling
		var listener = google.maps.event.addListener(map, "idle", function () {
			map.setZoom(8);
			google.maps.event.removeListener(listener);
		});
	}
});
// End of function