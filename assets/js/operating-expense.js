jQuery(document).ready(function($) {
    "use strict";
	
	$('.OperatingExpenses_input').on('change blur focus', function() {
		var OperatingExpensesMonthly_1  =  $('#OperatingExpensesMonthly_1').val()  ? parseFloat($('#OperatingExpensesMonthly_1').val())  : 0;
		var OperatingExpensesMonthly_2  =  $('#OperatingExpensesMonthly_2').val()  ? parseFloat($('#OperatingExpensesMonthly_2').val())  : 0;
		var OperatingExpensesMonthly_3  =  $('#OperatingExpensesMonthly_3').val()  ? parseFloat($('#OperatingExpensesMonthly_3').val())  : 0;
		var OperatingExpensesMonthly_4  =  $('#OperatingExpensesMonthly_4').val()  ? parseFloat($('#OperatingExpensesMonthly_4').val())  : 0;
		var OperatingExpensesMonthly_5  =  $('#OperatingExpensesMonthly_5').val()  ? parseFloat($('#OperatingExpensesMonthly_5').val())  : 0;
		var OperatingExpensesMonthly_6  =  $('#OperatingExpensesMonthly_6').val()  ? parseFloat($('#OperatingExpensesMonthly_6').val())  : 0;
		var OperatingExpensesMonthly_7  =  $('#OperatingExpensesMonthly_7').val()  ? parseFloat($('#OperatingExpensesMonthly_7').val())  : 0;
		var OperatingExpensesMonthly_8  =  $('#OperatingExpensesMonthly_8').val()  ? parseFloat($('#OperatingExpensesMonthly_8').val())  : 0;
		var OperatingExpensesMonthly_9  =  $('#OperatingExpensesMonthly_9').val()  ? parseFloat($('#OperatingExpensesMonthly_9').val())  : 0;
		var OperatingExpensesMonthly_10 =  $('#OperatingExpensesMonthly_10').val() ? parseFloat($('#OperatingExpensesMonthly_10').val()) : 0;
		
		var OperatingExpensesMonthlyOther_1 =  $('#OperatingExpensesMonthlyOther_1').val() ? parseFloat($('#OperatingExpensesMonthlyOther_1').val()) : 0;
		var OperatingExpensesMonthlyOther_2 =  $('#OperatingExpensesMonthlyOther_2').val() ? parseFloat($('#OperatingExpensesMonthlyOther_2').val()) : 0;
		var OperatingExpensesMonthlyOther_3 =  $('#OperatingExpensesMonthlyOther_3').val() ? parseFloat($('#OperatingExpensesMonthlyOther_3').val()) : 0;
		var OperatingExpensesMonthlyOther_4 =  $('#OperatingExpensesMonthlyOther_4').val() ? parseFloat($('#OperatingExpensesMonthlyOther_4').val()) : 0;
		
		var OperatingExpensesUtilitiesWaterSewerMonthly = $('#OperatingExpensesUtilitiesWaterSewerMonthly').val() ? parseFloat($('#OperatingExpensesUtilitiesWaterSewerMonthly').val()) : 0;
		var OperatingExpensesUtilitiesElectricityMonthly = $('#OperatingExpensesUtilitiesElectricityMonthly').val() ? parseFloat($('#OperatingExpensesUtilitiesElectricityMonthly').val()) : 0;
		var OperatingExpensesUtilitiesGasMonthly = $('#OperatingExpensesUtilitiesGasMonthly').val() ? parseFloat($('#OperatingExpensesUtilitiesGasMonthly').val()) : 0;
		var OperatingExpensesUtilitiesFuelOilMonthly = $('#OperatingExpensesUtilitiesFuelOilMonthly').val() ? parseFloat($('#OperatingExpensesUtilitiesFuelOilMonthly').val()) : 0;
		var OperatingExpensesUtilitiesOtherUtilitiesMonthly = $('#OperatingExpensesUtilitiesOtherUtilitiesMonthly').val() ? parseFloat($('#OperatingExpensesUtilitiesOtherUtilitiesMonthly').val()) : 0;
		
		var OperatingExpensesMonthlyTotal = OperatingExpensesMonthly_1 + OperatingExpensesMonthly_2 + OperatingExpensesMonthly_3 + OperatingExpensesMonthly_4 + OperatingExpensesMonthly_5 + OperatingExpensesMonthly_6 + OperatingExpensesMonthly_7 + OperatingExpensesMonthly_8 + OperatingExpensesMonthly_9 + OperatingExpensesMonthly_10 + OperatingExpensesMonthlyOther_1 + OperatingExpensesMonthlyOther_2 + OperatingExpensesMonthlyOther_3 + OperatingExpensesMonthlyOther_4 + OperatingExpensesUtilitiesWaterSewerMonthly + OperatingExpensesUtilitiesElectricityMonthly + OperatingExpensesUtilitiesGasMonthly + OperatingExpensesUtilitiesFuelOilMonthly + OperatingExpensesUtilitiesOtherUtilitiesMonthly;
		OperatingExpensesMonthlyTotal = OperatingExpensesMonthlyTotal ? parseFloat(OperatingExpensesMonthlyTotal) : 0;
		$('#OperatingExpensesMonthlyTotal').val(OperatingExpensesMonthlyTotal.toFixed(2));
		
		var OperatingExpensesAnnualTotal = OperatingExpensesMonthlyTotal * 12;
		OperatingExpensesAnnualTotal = OperatingExpensesAnnualTotal ? parseFloat(OperatingExpensesAnnualTotal) : 0;
		$('#OperatingExpensesAnnualTotal').val(OperatingExpensesAnnualTotal.toFixed(2));
		
		var OperatingExpensesAnnual_1 = OperatingExpensesMonthly_1 * 12;
		$('#OperatingExpensesAnnual_1').val(OperatingExpensesAnnual_1.toFixed(2));
		var OperatingExpensesPercentage_1 = (OperatingExpensesAnnual_1 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentage_1 = (OperatingExpensesPercentage_1) ? parseFloat(OperatingExpensesPercentage_1) : 0;
		$('#OperatingExpensesPercentage_1').val(Math.round(OperatingExpensesPercentage_1));
		
		var OperatingExpensesAnnual_2 = OperatingExpensesMonthly_2 * 12;
		$('#OperatingExpensesAnnual_2').val(OperatingExpensesAnnual_2.toFixed(2));
		var OperatingExpensesPercentage_2 = (OperatingExpensesAnnual_2 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentage_2 = (OperatingExpensesPercentage_2) ? parseFloat(OperatingExpensesPercentage_2) : 0;
		$('#OperatingExpensesPercentage_2').val(Math.round(OperatingExpensesPercentage_2));
		
		var OperatingExpensesAnnual_3 = OperatingExpensesMonthly_3 * 12;
		$('#OperatingExpensesAnnual_3').val(OperatingExpensesAnnual_3.toFixed(2));
		var OperatingExpensesPercentage_3 = (OperatingExpensesAnnual_3 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentage_3 = (OperatingExpensesPercentage_3) ? parseFloat(OperatingExpensesPercentage_3) : 0;
		$('#OperatingExpensesPercentage_3').val(Math.round(OperatingExpensesPercentage_3));
		
		var OperatingExpensesAnnual_4 = OperatingExpensesMonthly_4 * 12;
		$('#OperatingExpensesAnnual_4').val(OperatingExpensesAnnual_4.toFixed(2));
		var OperatingExpensesPercentage_4 = (OperatingExpensesAnnual_4 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentage_4 = (OperatingExpensesPercentage_4) ? parseFloat(OperatingExpensesPercentage_4) : 0;
		$('#OperatingExpensesPercentage_4').val(Math.round(OperatingExpensesPercentage_4));
		
		var OperatingExpensesAnnual_5 = OperatingExpensesMonthly_5 * 12;
		$('#OperatingExpensesAnnual_5').val(OperatingExpensesAnnual_5.toFixed(2));
		var OperatingExpensesPercentage_5 = (OperatingExpensesAnnual_5 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentage_5 = (OperatingExpensesPercentage_5) ? parseFloat(OperatingExpensesPercentage_5) : 0;
		$('#OperatingExpensesPercentage_5').val(Math.round(OperatingExpensesPercentage_5));
		
		var OperatingExpensesAnnual_6 = OperatingExpensesMonthly_6 * 12;
		$('#OperatingExpensesAnnual_6').val(OperatingExpensesAnnual_6.toFixed(2));
		var OperatingExpensesPercentage_6 = (OperatingExpensesAnnual_6 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentage_6 = (OperatingExpensesPercentage_6) ? parseFloat(OperatingExpensesPercentage_6) : 0;
		$('#OperatingExpensesPercentage_6').val(Math.round(OperatingExpensesPercentage_6));
		
		var OperatingExpensesAnnual_7 = OperatingExpensesMonthly_7 * 12;
		$('#OperatingExpensesAnnual_7').val(OperatingExpensesAnnual_7.toFixed(2));
		var OperatingExpensesPercentage_7 = (OperatingExpensesAnnual_7 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentage_7 = (OperatingExpensesPercentage_7) ? parseFloat(OperatingExpensesPercentage_7) : 0;
		$('#OperatingExpensesPercentage_7').val(Math.round(OperatingExpensesPercentage_7));
		
		var OperatingExpensesAnnual_8 = OperatingExpensesMonthly_8 * 12;
		$('#OperatingExpensesAnnual_8').val(OperatingExpensesAnnual_8.toFixed(2));
		var OperatingExpensesPercentage_8 = (OperatingExpensesAnnual_8 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentage_8 = (OperatingExpensesPercentage_8) ? parseFloat(OperatingExpensesPercentage_8) : 0;
		$('#OperatingExpensesPercentage_8').val(Math.round(OperatingExpensesPercentage_8));
		
		var OperatingExpensesAnnual_9 = OperatingExpensesMonthly_9 * 12;
		$('#OperatingExpensesAnnual_9').val(OperatingExpensesAnnual_9.toFixed(2));
		var OperatingExpensesPercentage_9 = (OperatingExpensesAnnual_9 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentage_9 = (OperatingExpensesPercentage_9) ? parseFloat(OperatingExpensesPercentage_9) : 0;
		$('#OperatingExpensesPercentage_9').val(Math.round(OperatingExpensesPercentage_9));
		
		var OperatingExpensesAnnual_10 = OperatingExpensesMonthly_10 * 12;
		$('#OperatingExpensesAnnual_10').val(OperatingExpensesAnnual_10.toFixed(2));
		var OperatingExpensesPercentage_10 = (OperatingExpensesAnnual_10 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentage_10 = (OperatingExpensesPercentage_10) ? parseFloat(OperatingExpensesPercentage_10) : 0;
		$('#OperatingExpensesPercentage_10').val(Math.round(OperatingExpensesPercentage_10));
		
		var OperatingExpensesAnnualOther_1 = OperatingExpensesMonthlyOther_1 * 12;
		$('#OperatingExpensesAnnualOther_1').val(OperatingExpensesAnnualOther_1.toFixed(2));
		var OperatingExpensesPercentageOther_1 = (OperatingExpensesAnnualOther_1 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentageOther_1 = (OperatingExpensesPercentageOther_1) ? parseFloat(OperatingExpensesPercentageOther_1) : 0;
		$('#OperatingExpensesPercentageOther_1').val(Math.round(OperatingExpensesPercentageOther_1));
		
		var OperatingExpensesAnnualOther_2 = OperatingExpensesMonthlyOther_2 * 12;
		$('#OperatingExpensesAnnualOther_2').val(OperatingExpensesAnnualOther_2.toFixed(2));
		var OperatingExpensesPercentageOther_2 = (OperatingExpensesAnnualOther_2 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentageOther_2 = (OperatingExpensesPercentageOther_2) ? parseFloat(OperatingExpensesPercentageOther_2) : 0;
		$('#OperatingExpensesPercentageOther_2').val(Math.round(OperatingExpensesPercentageOther_2));
		
		var OperatingExpensesAnnualOther_3 = OperatingExpensesMonthlyOther_3 * 12;
		$('#OperatingExpensesAnnualOther_3').val(OperatingExpensesAnnualOther_3.toFixed(2));
		var OperatingExpensesPercentageOther_3 = (OperatingExpensesAnnualOther_3 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentageOther_3 = (OperatingExpensesPercentageOther_3) ? parseFloat(OperatingExpensesPercentageOther_3) : 0;
		$('#OperatingExpensesPercentageOther_3').val(Math.round(OperatingExpensesPercentageOther_3));
		
		var OperatingExpensesAnnualOther_4 = OperatingExpensesMonthlyOther_4 * 12;
		$('#OperatingExpensesAnnualOther_4').val(OperatingExpensesAnnualOther_4.toFixed(2));
		var OperatingExpensesPercentageOther_4 = (OperatingExpensesAnnualOther_4 / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesPercentageOther_4 = (OperatingExpensesPercentageOther_4) ? parseFloat(OperatingExpensesPercentageOther_4) : 0;
		$('#OperatingExpensesPercentageOther_4').val(Math.round(OperatingExpensesPercentageOther_4));
		
		var OperatingExpensesUtilitiesWaterSewerAnnual = OperatingExpensesUtilitiesWaterSewerMonthly * 12;
		$('#OperatingExpensesUtilitiesWaterSewerAnnual').val(OperatingExpensesUtilitiesWaterSewerAnnual.toFixed(2));
		var OperatingExpensesUtilitiesWaterSewerPercentage = (OperatingExpensesUtilitiesWaterSewerAnnual / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesUtilitiesWaterSewerPercentage = (OperatingExpensesUtilitiesWaterSewerPercentage) ? parseFloat(OperatingExpensesUtilitiesWaterSewerPercentage) : 0;
		$('#OperatingExpensesUtilitiesWaterSewerPercentage').val(Math.round(OperatingExpensesUtilitiesWaterSewerPercentage));

		var OperatingExpensesUtilitiesElectricityAnnual = OperatingExpensesUtilitiesElectricityMonthly * 12;
		$('#OperatingExpensesUtilitiesElectricityAnnual').val(OperatingExpensesUtilitiesElectricityAnnual.toFixed(2));
		var OperatingExpensesUtilitiesElectricityPercentage = (OperatingExpensesUtilitiesElectricityAnnual / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesUtilitiesElectricityPercentage = (OperatingExpensesUtilitiesElectricityPercentage) ? parseFloat(OperatingExpensesUtilitiesElectricityPercentage) : 0;
		$('#OperatingExpensesUtilitiesElectricityPercentage').val(Math.round(OperatingExpensesUtilitiesElectricityPercentage));

		var OperatingExpensesUtilitiesGasAnnual = OperatingExpensesUtilitiesGasMonthly * 12;
		$('#OperatingExpensesUtilitiesGasAnnual').val(OperatingExpensesUtilitiesGasAnnual.toFixed(2));
		var OperatingExpensesUtilitiesGasPercentage = (OperatingExpensesUtilitiesGasAnnual / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesUtilitiesGasPercentage = (OperatingExpensesUtilitiesGasPercentage) ? parseFloat(OperatingExpensesUtilitiesGasPercentage) : 0;
		$('#OperatingExpensesUtilitiesGasPercentage').val(Math.round(OperatingExpensesUtilitiesGasPercentage));

		var OperatingExpensesUtilitiesFuelOilAnnual = OperatingExpensesUtilitiesFuelOilMonthly * 12;
		$('#OperatingExpensesUtilitiesFuelOilAnnual').val(OperatingExpensesUtilitiesFuelOilAnnual.toFixed(2));
		var OperatingExpensesUtilitiesFuelOilPercentage = (OperatingExpensesUtilitiesFuelOilAnnual / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesUtilitiesFuelOilPercentage = (OperatingExpensesUtilitiesFuelOilPercentage) ? parseFloat(OperatingExpensesUtilitiesFuelOilPercentage) : 0;
		$('#OperatingExpensesUtilitiesFuelOilPercentage').val(Math.round(OperatingExpensesUtilitiesFuelOilPercentage));

		var OperatingExpensesUtilitiesOtherUtilitiesAnnual = OperatingExpensesUtilitiesOtherUtilitiesMonthly * 12;
		$('#OperatingExpensesUtilitiesOtherUtilitiesAnnual').val(OperatingExpensesUtilitiesOtherUtilitiesAnnual.toFixed(2));
		var OperatingExpensesUtilitiesOtherUtilitiesPercentage = (OperatingExpensesUtilitiesOtherUtilitiesAnnual / OperatingExpensesAnnualTotal) * 100;
		OperatingExpensesUtilitiesOtherUtilitiesPercentage = (OperatingExpensesUtilitiesOtherUtilitiesPercentage) ? parseFloat(OperatingExpensesUtilitiesOtherUtilitiesPercentage) : 0;
		$('#OperatingExpensesUtilitiesOtherUtilitiesPercentage').val(Math.round(OperatingExpensesUtilitiesOtherUtilitiesPercentage));

		var OperatingExpensesPercentageTotal = OperatingExpensesPercentage_1 + OperatingExpensesPercentage_2 + OperatingExpensesPercentage_3 + OperatingExpensesPercentage_4 + OperatingExpensesPercentage_5 + OperatingExpensesPercentage_6 + OperatingExpensesPercentage_7 + OperatingExpensesPercentage_8 + OperatingExpensesPercentage_9 + OperatingExpensesPercentage_10 + OperatingExpensesPercentageOther_1 + OperatingExpensesPercentageOther_2 + OperatingExpensesPercentageOther_3 + OperatingExpensesPercentageOther_4 + OperatingExpensesUtilitiesWaterSewerPercentage + OperatingExpensesUtilitiesElectricityPercentage + OperatingExpensesUtilitiesGasPercentage + OperatingExpensesUtilitiesFuelOilPercentage + OperatingExpensesUtilitiesOtherUtilitiesPercentage;
		OperatingExpensesPercentageTotal = OperatingExpensesPercentageTotal ? parseFloat(OperatingExpensesPercentageTotal) : 0;
		$('#OperatingExpensesPercentageTotal').val(Math.round(OperatingExpensesPercentageTotal));
		
		var OperatingExpensesMonthlyTotal = $('#OperatingExpensesMonthlyTotal').val() ? parseFloat($('#OperatingExpensesMonthlyTotal').val()) : 0;
		$('#OperatingExpenses').val(OperatingExpensesMonthlyTotal.toFixed(2));
	});	
	
});