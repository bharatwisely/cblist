jQuery(document).ready(function($) {
    "use strict";
	
	// Bid 1 Calculation Event
	$('#RefiTable .Bid1InputBox').on('change blur focus', function() {
        RefiTotalBid1TotalCalculation();
    });
	// Bid 2 Calculation Event
	$('#RefiTable .Bid2InputBox').on('change blur focus', function() {
        RefiTotalBid2TotalCalculation();
    });
	// Bid 3 Calculation Event
	$('#RefiTable .Bid3InputBox').on('change blur focus', function() {
        RefiTotalBid3TotalCalculation();
    });
	// Total Budget Calculation Event
	$('#RefiTable .BudgetInputBox').on('change blur focus', function() {
		var Parent = $(this).parents('tr');
		var Budget = parseFloat($(this).val());
		var Month = parseFloat(Parent.children('td:eq(9)').find('select').val());
		if ( Budget > 0 ) {
			if ( Month == 0 ) {
				Parent.children('td:eq(10)').text('Enter Moth to Pay!');
			} else {
				Parent.children('td:eq(10)').text('');
			}
		} else {
			Parent.children('td:eq(10)').text('');
		}
		
		// Total Budget Calculation
		var BudgetDemo 						= $('#RefiTable #BudgetDemo').val() 					? parseFloat($('#RefiTable #BudgetDemo').val()) 					: 0;
		var BudgetArchitectural				= $('#RefiTable #BudgetArchitectural').val() 			? parseFloat($('#RefiTable #BudgetArchitectural').val()) 			: 0;
		var BudgetMasterBuildingPermit		= $('#RefiTable #BudgetMasterBuildingPermit').val() 	? parseFloat($('#RefiTable #BudgetMasterBuildingPermit').val()) 	: 0;
		var BudgetPlumbing					= $('#RefiTable #BudgetPlumbing').val() 				? parseFloat($('#RefiTable #BudgetPlumbing').val()) 				: 0;
		var BudgetElectrical				= $('#RefiTable #BudgetElectrical').val() 				? parseFloat($('#RefiTable #BudgetElectrical').val()) 				: 0;
		var BudgetHVAC						= $('#RefiTable #BudgetHVAC').val() 					? parseFloat($('#RefiTable #BudgetHVAC').val()) 					: 0;
		var BudgetDumpster					= $('#RefiTable #BudgetDumpster').val() 				? parseFloat($('#RefiTable #BudgetDumpster').val()) 				: 0;
		var BudgetOther						= $('#RefiTable #BudgetOther').val() 					? parseFloat($('#RefiTable #BudgetOther').val()) 					: 0;
		
		var BudgetWallFraming				= $('#RefiTable #BudgetWallFraming').val() 				? parseFloat($('#RefiTable #BudgetWallFraming').val()) 			: 0;
		var BudgetFloorFraming				= $('#RefiTable #BudgetFloorFraming').val() 			? parseFloat($('#RefiTable #BudgetFloorFraming').val()) 			: 0;
		var BudgetCeilingFraming			= $('#RefiTable #BudgetCeilingFraming').val() 			? parseFloat($('#RefiTable #BudgetCeilingFraming').val()) 			: 0;
		var BudgetInteriorElectrical		= $('#RefiTable #BudgetInteriorElectrical').val() 		? parseFloat($('#RefiTable #BudgetInteriorElectrical').val()) 		: 0;
		var BudgetInteriorPlumbing			= $('#RefiTable #BudgetInteriorPlumbing').val() 		? parseFloat($('#RefiTable #BudgetInteriorPlumbing').val()) 		: 0;
		var BudgetInteriorHVAC				= $('#RefiTable #BudgetInteriorHVAC').val() 			? parseFloat($('#RefiTable #BudgetInteriorHVAC').val()) 			: 0;
		var BudgetFlooring					= $('#RefiTable #BudgetFlooring').val() 				? parseFloat($('#RefiTable #BudgetFlooring').val()) 				: 0;
		var BudgetSheetrock					= $('#RefiTable #BudgetSheetrock').val() 				? parseFloat($('#RefiTable #BudgetSheetrock').val()) 				: 0;
		var BudgetWindows					= $('#RefiTable #BudgetWindows').val() 					? parseFloat($('#RefiTable #BudgetWindows').val()) 				: 0;
		var BudgetInteriorDoors				= $('#RefiTable #BudgetInteriorDoors').val() 			? parseFloat($('#RefiTable #BudgetInteriorDoors').val()) 			: 0;
		var BudgetTrim						= $('#RefiTable #BudgetTrim').val() 					? parseFloat($('#RefiTable #BudgetTrim').val()) 					: 0;
		var BudgetBathroomVanities			= $('#RefiTable #BudgetBathroomVanities').val() 		? parseFloat($('#RefiTable #BudgetBathroomVanities').val()) 		: 0;
		var BudgetBathroomFixtures			= $('#RefiTable #BudgetBathroomFixtures').val() 		? parseFloat($('#RefiTable #BudgetBathroomFixtures').val()) 		: 0;
		var BudgetKitchenCabinets			= $('#RefiTable #BudgetKitchenCabinets').val() 			? parseFloat($('#RefiTable #BudgetKitchenCabinets').val()) 		: 0;
		var BudgetLaborInstallKitchen		= $('#RefiTable #BudgetLaborInstallKitchen').val() 		? parseFloat($('#RefiTable #BudgetLaborInstallKitchen').val()) 	: 0;
		var BudgetFloorCoverings1			= $('#RefiTable #BudgetFloorCoverings1').val() 			? parseFloat($('#RefiTable #BudgetFloorCoverings1').val()) 		: 0;
		var BudgetFloorCoverings2			= $('#RefiTable #BudgetFloorCoverings2').val() 			? parseFloat($('#RefiTable #BudgetFloorCoverings2').val()) 		: 0;
		var BudgetFloorCoverings3			= $('#RefiTable #BudgetFloorCoverings3').val() 			? parseFloat($('#RefiTable #BudgetFloorCoverings3').val()) 		: 0;
		var BudgetInteriorPainting			= $('#RefiTable #BudgetInteriorPainting').val() 		? parseFloat($('#RefiTable #BudgetInteriorPainting').val()) 		: 0;
		var BudgetLightFixtures				= $('#RefiTable #BudgetLightFixtures').val() 			? parseFloat($('#RefiTable #BudgetLightFixtures').val()) 			: 0;
		var BudgetOtherFixtures				= $('#RefiTable #BudgetOtherFixtures').val() 			? parseFloat($('#RefiTable #BudgetOtherFixtures').val()) 			: 0;
		var BudgetAppliances				= $('#RefiTable #BudgetAppliances').val() 				? parseFloat($('#RefiTable #BudgetAppliances').val()) 				: 0;
		var BudgetOtherInterior				= $('#RefiTable #BudgetOtherInterior').val() 			? parseFloat($('#RefiTable #BudgetOtherInterior').val()) 			: 0;
		
		var BudgetExteriorTrim				= $('#RefiTable #BudgetExteriorTrim').val() 			? parseFloat($('#RefiTable #BudgetExteriorTrim').val()) 			: 0;
		var BudgetExteriorDoors				= $('#RefiTable #BudgetExteriorDoors').val()			? parseFloat($('#RefiTable #BudgetExteriorDoors').val()) 			: 0;
		var BudgetPorches					= $('#RefiTable #BudgetPorches').val() 					? parseFloat($('#RefiTable #BudgetPorches').val()) 				: 0;
		var BudgetSiding					= $('#RefiTable #BudgetSiding').val() 					? parseFloat($('#RefiTable #BudgetSiding').val()) 					: 0;
		var BudgetExteriorPainting			= $('#RefiTable #BudgetExteriorPainting').val() 		? parseFloat($('#RefiTable #BudgetExteriorPainting').val()) 		: 0;
		var BudgetRoof						= $('#RefiTable #BudgetRoof').val() 					? parseFloat($('#RefiTable #BudgetRoof').val()) 					: 0;
		var BudgetGuttersDownspouts			= $('#RefiTable #BudgetGuttersDownspouts').val() 		? parseFloat($('#RefiTable #BudgetGuttersDownspouts').val()) 		: 0;
		var BudgetFencing					= $('#RefiTable #BudgetFencing').val() 					? parseFloat($('#RefiTable #BudgetFencing').val()) 				: 0;
		var BudgetLandscaping				= $('#RefiTable #BudgetLandscaping').val() 				? parseFloat($('#RefiTable #BudgetLandscaping').val()) 			: 0;
		var BudgetDrivewayConcrete			= $('#RefiTable #BudgetDrivewayConcrete').val() 		? parseFloat($('#RefiTable #BudgetDrivewayConcrete').val()) 		: 0;
		var BudgetFoudationWork				= $('#RefiTable #BudgetFoudationWork').val() 			? parseFloat($('#RefiTable #BudgetFoudationWork').val()) 			: 0;
		var BudgetBrickPointingReplacement	= $('#RefiTable #BudgetBrickPointingReplacement').val()	? parseFloat($('#RefiTable #BudgetBrickPointingReplacement').val()): 0;
		var BudgetOtherExterior				= $('#RefiTable #BudgetOtherExterior').val() 			? parseFloat($('#RefiTable #BudgetOtherExterior').val()) 			: 0;
		
		var BudgetContingency				= $('#RefiTable #BudgetContingency').val() 				? parseFloat($('#RefiTable #BudgetContingency').val()) 			: 0;
		var BudgetGCFee						= $('#RefiTable #BudgetGCFee').val() 					? parseFloat($('#RefiTable #BudgetGCFee').val()) 					: 0;
		var BudgetCleanup					= $('#RefiTable #BudgetCleanup').val() 					? parseFloat($('#RefiTable #BudgetCleanup').val()) 				: 0;
		var BudgetOtherOther				= $('#RefiTable #BudgetOtherOther').val() 				? parseFloat($('#RefiTable #BudgetOtherOther').val()) 				: 0;
		
		var TotalBudgetTotal = BudgetDemo + BudgetArchitectural + BudgetMasterBuildingPermit + BudgetPlumbing + BudgetElectrical + BudgetHVAC + BudgetDumpster + BudgetOther + BudgetWallFraming + BudgetFloorFraming + BudgetCeilingFraming + BudgetInteriorElectrical + BudgetInteriorPlumbing + BudgetInteriorHVAC + BudgetFlooring + BudgetSheetrock + BudgetWindows + BudgetInteriorDoors + BudgetTrim + BudgetBathroomVanities + BudgetBathroomFixtures + BudgetKitchenCabinets + BudgetLaborInstallKitchen + BudgetFloorCoverings1 + BudgetFloorCoverings2 + BudgetFloorCoverings3 + BudgetInteriorPainting + BudgetLightFixtures + BudgetOtherFixtures + BudgetAppliances + BudgetOtherInterior + BudgetExteriorTrim + BudgetExteriorDoors + BudgetPorches + BudgetSiding + BudgetExteriorPainting + BudgetRoof + BudgetGuttersDownspouts + BudgetFencing + BudgetLandscaping + BudgetDrivewayConcrete + BudgetFoudationWork + BudgetBrickPointingReplacement + BudgetOtherExterior + BudgetContingency + BudgetGCFee + BudgetCleanup + BudgetOtherOther;
		
		TotalBudgetTotal = TotalBudgetTotal ? parseFloat(TotalBudgetTotal) : 0;
		$('#RefiTable #DetailedRefiBudget').val(TotalBudgetTotal.toFixed(2));

	});
		
});

// Bid 1 Calculation
function RefiTotalBid1TotalCalculation() {
	var Bid1Demo 						= $('#RefiTable #Bid1Demo').val() 						? parseFloat($('#RefiTable #Bid1Demo').val()) 						: 0;
	var Bid1Architectural				= $('#RefiTable #Bid1Architectural').val() 			? parseFloat($('#RefiTable #Bid1Architectural').val()) 			: 0;
	var Bid1MasterBuildingPermit		= $('#RefiTable #Bid1MasterBuildingPermit').val() 		? parseFloat($('#RefiTable #Bid1MasterBuildingPermit').val()) 		: 0;
	var Bid1Plumbing					= $('#RefiTable #Bid1Plumbing').val() 					? parseFloat($('#RefiTable #Bid1Plumbing').val()) 					: 0;
	var Bid1Electrical					= $('#RefiTable #Bid1Electrical').val() 				? parseFloat($('#RefiTable #Bid1Electrical').val()) 				: 0;
	var Bid1HVAC						= $('#RefiTable #Bid1HVAC').val() 						? parseFloat($('#RefiTable #Bid1HVAC').val()) 						: 0;
	var Bid1Dumpster					= $('#RefiTable #Bid1Dumpster').val() 					? parseFloat($('#RefiTable #Bid1Dumpster').val()) 					: 0;
	var Bid1Other						= $('#RefiTable #Bid1Other').val() 					? parseFloat($('#RefiTable #Bid1Other').val()) 					: 0;
	
	var Bid1WallFraming					= $('#RefiTable #Bid1WallFraming').val() 				? parseFloat($('#RefiTable #Bid1WallFraming').val()) 				: 0;
	var Bid1FloorFraming				= $('#RefiTable #Bid1FloorFraming').val() 				? parseFloat($('#RefiTable #Bid1FloorFraming').val()) 				: 0;
	var Bid1CeilingFraming				= $('#RefiTable #Bid1CeilingFraming').val() 			? parseFloat($('#RefiTable #Bid1CeilingFraming').val()) 			: 0;
	var Bid1InteriorElectrical			= $('#RefiTable #Bid1InteriorElectrical').val() 		? parseFloat($('#RefiTable #Bid1InteriorElectrical').val()) 		: 0;
	var Bid1InteriorPlumbing			= $('#RefiTable #Bid1InteriorPlumbing').val() 			? parseFloat($('#RefiTable #Bid1InteriorPlumbing').val()) 			: 0;
	var Bid1InteriorHVAC				= $('#RefiTable #Bid1InteriorHVAC').val() 				? parseFloat($('#RefiTable #Bid1InteriorHVAC').val()) 				: 0;
	var Bid1Flooring					= $('#RefiTable #Bid1Flooring').val() 					? parseFloat($('#RefiTable #Bid1Flooring').val()) 					: 0;
	var Bid1Sheetrock					= $('#RefiTable #Bid1Sheetrock').val() 				? parseFloat($('#RefiTable #Bid1Sheetrock').val()) 				: 0;
	var Bid1Windows						= $('#RefiTable #Bid1Windows').val() 					? parseFloat($('#RefiTable #Bid1Windows').val()) 					: 0;
	var Bid1InteriorDoors				= $('#RefiTable #Bid1InteriorDoors').val() 			? parseFloat($('#RefiTable #Bid1InteriorDoors').val()) 			: 0;
	var Bid1Trim						= $('#RefiTable #Bid1Trim').val() 						? parseFloat($('#RefiTable #Bid1Trim').val()) 						: 0;
	var Bid1BathroomVanities			= $('#RefiTable #Bid1BathroomVanities').val() 			? parseFloat($('#RefiTable #Bid1BathroomVanities').val()) 			: 0;
	var Bid1BathroomFixtures			= $('#RefiTable #Bid1BathroomFixtures').val() 			? parseFloat($('#RefiTable #Bid1BathroomFixtures').val()) 			: 0;
	var Bid1KitchenCabinets				= $('#RefiTable #Bid1KitchenCabinets').val() 			? parseFloat($('#RefiTable #Bid1KitchenCabinets').val()) 			: 0;
	var Bid1LaborInstallKitchen			= $('#RefiTable #Bid1LaborInstallKitchen').val() 		? parseFloat($('#RefiTable #Bid1LaborInstallKitchen').val()) 		: 0;
	var Bid1FloorCoverings1				= $('#RefiTable #Bid1FloorCoverings1').val() 			? parseFloat($('#RefiTable #Bid1FloorCoverings1').val()) 			: 0;
	var Bid1FloorCoverings2				= $('#RefiTable #Bid1FloorCoverings2').val() 			? parseFloat($('#RefiTable #Bid1FloorCoverings2').val()) 			: 0;
	var Bid1FloorCoverings3				= $('#RefiTable #Bid1FloorCoverings3').val() 			? parseFloat($('#RefiTable #Bid1FloorCoverings3').val()) 			: 0;
	var Bid1InteriorPainting			= $('#RefiTable #Bid1InteriorPainting').val() 			? parseFloat($('#RefiTable #Bid1InteriorPainting').val()) 			: 0;
	var Bid1LightFixtures				= $('#RefiTable #Bid1LightFixtures').val() 			? parseFloat($('#RefiTable #Bid1LightFixtures').val()) 			: 0;
	var Bid1OtherFixtures				= $('#RefiTable #Bid1OtherFixtures').val() 			? parseFloat($('#RefiTable #Bid1OtherFixtures').val()) 			: 0;
	var Bid1Appliances					= $('#RefiTable #Bid1Appliances').val() 				? parseFloat($('#RefiTable #Bid1Appliances').val()) 				: 0;
	var Bid1OtherInterior				= $('#RefiTable #Bid1OtherInterior').val() 			? parseFloat($('#RefiTable #Bid1OtherInterior').val()) 			: 0;
	
	var Bid1ExteriorTrim				= $('#RefiTable #Bid1ExteriorTrim').val() 				? parseFloat($('#RefiTable #Bid1ExteriorTrim').val()) 				: 0;
	var Bid1ExteriorDoors				= $('#RefiTable #Bid1ExteriorDoors').val()			 	? parseFloat($('#RefiTable #Bid1ExteriorDoors').val()) 			: 0;
	var Bid1Porches						= $('#RefiTable #Bid1Porches').val() 					? parseFloat($('#RefiTable #Bid1Porches').val()) 					: 0;
	var Bid1Siding						= $('#RefiTable #Bid1Siding').val() 					? parseFloat($('#RefiTable #Bid1Siding').val()) 					: 0;
	var Bid1ExteriorPainting			= $('#RefiTable #Bid1ExteriorPainting').val() 			? parseFloat($('#RefiTable #Bid1ExteriorPainting').val()) 			: 0;
	var Bid1Roof						= $('#RefiTable #Bid1Roof').val() 						? parseFloat($('#RefiTable #Bid1Roof').val()) 						: 0;
	var Bid1GuttersDownspouts			= $('#RefiTable #Bid1GuttersDownspouts').val() 		? parseFloat($('#RefiTable #Bid1GuttersDownspouts').val()) 		: 0;
	var Bid1Fencing						= $('#RefiTable #Bid1Fencing').val() 					? parseFloat($('#RefiTable #Bid1Fencing').val()) 					: 0;
	var Bid1Landscaping					= $('#RefiTable #Bid1Landscaping').val() 				? parseFloat($('#RefiTable #Bid1Landscaping').val()) 				: 0;
	var Bid1DrivewayConcrete			= $('#RefiTable #Bid1DrivewayConcrete').val() 			? parseFloat($('#RefiTable #Bid1DrivewayConcrete').val()) 			: 0;
	var Bid1FoudationWork				= $('#RefiTable #Bid1FoudationWork').val() 			? parseFloat($('#RefiTable #Bid1FoudationWork').val()) 			: 0;
	var Bid1BrickPointingReplacement	= $('#RefiTable #Bid1BrickPointingReplacement').val() 	? parseFloat($('#RefiTable #Bid1BrickPointingReplacement').val()) 	: 0;
	var Bid1OtherExterior				= $('#RefiTable #Bid1OtherExterior').val() 			? parseFloat($('#RefiTable #Bid1OtherExterior').val()) 			: 0;
	
	var Bid1Contingency					= $('#RefiTable #Bid1Contingency').val() 				? parseFloat($('#RefiTable #Bid1Contingency').val()) 				: 0;
	var Bid1GCFee						= $('#RefiTable #Bid1GCFee').val() 					? parseFloat($('#RefiTable #Bid1GCFee').val()) 					: 0;
	var Bid1Cleanup						= $('#RefiTable #Bid1Cleanup').val() 					? parseFloat($('#RefiTable #Bid1Cleanup').val()) 					: 0;
	var Bid1OtherOther					= $('#RefiTable #Bid1OtherOther').val() 				? parseFloat($('#RefiTable #Bid1OtherOther').val()) 				: 0;
	
	var TotalBid1Total = Bid1Demo + Bid1Architectural + Bid1MasterBuildingPermit + Bid1Plumbing + Bid1Electrical + Bid1HVAC + Bid1Dumpster + Bid1Other + Bid1WallFraming + Bid1FloorFraming + Bid1CeilingFraming + Bid1InteriorElectrical + Bid1InteriorPlumbing + Bid1InteriorHVAC + Bid1Flooring + Bid1Sheetrock + Bid1Windows + Bid1InteriorDoors + Bid1Trim + Bid1BathroomVanities + Bid1BathroomFixtures + Bid1KitchenCabinets + Bid1LaborInstallKitchen + Bid1FloorCoverings1 + Bid1FloorCoverings2 + Bid1FloorCoverings3 + Bid1InteriorPainting + Bid1LightFixtures + Bid1OtherFixtures + Bid1Appliances + Bid1OtherInterior + Bid1ExteriorTrim + Bid1ExteriorDoors + Bid1Porches + Bid1Siding + Bid1ExteriorPainting + Bid1Roof + Bid1GuttersDownspouts + Bid1Fencing + Bid1Landscaping + Bid1DrivewayConcrete + Bid1FoudationWork + Bid1BrickPointingReplacement + Bid1OtherExterior + Bid1Contingency + Bid1GCFee + Bid1Cleanup + Bid1OtherOther;
	
	TotalBid1Total = TotalBid1Total ? TotalBid1Total : 0;
	$('#RefiTable #TotalBid1Total').val(TotalBid1Total.toFixed(2));
}

// Bid 2 Calculation
function RefiTotalBid2TotalCalculation() {
	var Bid2Demo 						= $('#RefiTable #Bid2Demo').val() 						? parseFloat($('#RefiTable #Bid2Demo').val()) 						: 0;
	var Bid2Architectural				= $('#RefiTable #Bid2Architectural').val() 			? parseFloat($('#RefiTable #Bid2Architectural').val()) 			: 0;
	var Bid2MasterBuildingPermit		= $('#RefiTable #Bid2MasterBuildingPermit').val() 		? parseFloat($('#RefiTable #Bid2MasterBuildingPermit').val()) 		: 0;
	var Bid2Plumbing					= $('#RefiTable #Bid2Plumbing').val() 					? parseFloat($('#RefiTable #Bid2Plumbing').val()) 					: 0;
	var Bid2Electrical					= $('#RefiTable #Bid2Electrical').val() 				? parseFloat($('#RefiTable #Bid2Electrical').val()) 				: 0;
	var Bid2HVAC						= $('#RefiTable #Bid2HVAC').val() 						? parseFloat($('#RefiTable #Bid2HVAC').val()) 						: 0;
	var Bid2Dumpster					= $('#RefiTable #Bid2Dumpster').val() 					? parseFloat($('#RefiTable #Bid2Dumpster').val()) 					: 0;
	var Bid2Other						= $('#RefiTable #Bid2Other').val() 					? parseFloat($('#RefiTable #Bid2Other').val()) 					: 0;
	
	var Bid2WallFraming					= $('#RefiTable #Bid2WallFraming').val() 				? parseFloat($('#RefiTable #Bid2WallFraming').val()) 				: 0;
	var Bid2FloorFraming				= $('#RefiTable #Bid2FloorFraming').val() 				? parseFloat($('#RefiTable #Bid2FloorFraming').val()) 				: 0;
	var Bid2CeilingFraming				= $('#RefiTable #Bid2CeilingFraming').val() 			? parseFloat($('#RefiTable #Bid2CeilingFraming').val()) 			: 0;
	var Bid2InteriorElectrical			= $('#RefiTable #Bid2InteriorElectrical').val() 		? parseFloat($('#RefiTable #Bid2InteriorElectrical').val()) 		: 0;
	var Bid2InteriorPlumbing			= $('#RefiTable #Bid2InteriorPlumbing').val() 			? parseFloat($('#RefiTable #Bid2InteriorPlumbing').val()) 			: 0;
	var Bid2InteriorHVAC				= $('#RefiTable #Bid2InteriorHVAC').val() 				? parseFloat($('#RefiTable #Bid2InteriorHVAC').val()) 				: 0;
	var Bid2Flooring					= $('#RefiTable #Bid2Flooring').val() 					? parseFloat($('#RefiTable #Bid2Flooring').val()) 					: 0;
	var Bid2Sheetrock					= $('#RefiTable #Bid2Sheetrock').val() 				? parseFloat($('#RefiTable #Bid2Sheetrock').val()) 				: 0;
	var Bid2Windows						= $('#RefiTable #Bid2Windows').val() 					? parseFloat($('#RefiTable #Bid2Windows').val()) 					: 0;
	var Bid2InteriorDoors				= $('#RefiTable #Bid2InteriorDoors').val() 			? parseFloat($('#RefiTable #Bid2InteriorDoors').val()) 			: 0;
	var Bid2Trim						= $('#RefiTable #Bid2Trim').val() 						? parseFloat($('#RefiTable #Bid2Trim').val()) 						: 0;
	var Bid2BathroomVanities			= $('#RefiTable #Bid2BathroomVanities').val() 			? parseFloat($('#RefiTable #Bid2BathroomVanities').val()) 			: 0;
	var Bid2BathroomFixtures			= $('#RefiTable #Bid2BathroomFixtures').val() 			? parseFloat($('#RefiTable #Bid2BathroomFixtures').val()) 			: 0;
	var Bid2KitchenCabinets				= $('#RefiTable #Bid2KitchenCabinets').val() 			? parseFloat($('#RefiTable #Bid2KitchenCabinets').val()) 			: 0;
	var Bid2LaborInstallKitchen			= $('#RefiTable #Bid2LaborInstallKitchen').val() 		? parseFloat($('#RefiTable #Bid2LaborInstallKitchen').val()) 		: 0;
	var Bid2FloorCoverings1				= $('#RefiTable #Bid2FloorCoverings1').val() 			? parseFloat($('#RefiTable #Bid2FloorCoverings1').val()) 			: 0;
	var Bid2FloorCoverings2				= $('#RefiTable #Bid2FloorCoverings2').val() 			? parseFloat($('#RefiTable #Bid2FloorCoverings2').val()) 			: 0;
	var Bid2FloorCoverings3				= $('#RefiTable #Bid2FloorCoverings3').val() 			? parseFloat($('#RefiTable #Bid2FloorCoverings3').val()) 			: 0;
	var Bid2InteriorPainting			= $('#RefiTable #Bid2InteriorPainting').val() 			? parseFloat($('#RefiTable #Bid2InteriorPainting').val()) 			: 0;
	var Bid2LightFixtures				= $('#RefiTable #Bid2LightFixtures').val() 			? parseFloat($('#RefiTable #Bid2LightFixtures').val()) 			: 0;
	var Bid2OtherFixtures				= $('#RefiTable #Bid2OtherFixtures').val() 			? parseFloat($('#RefiTable #Bid2OtherFixtures').val()) 			: 0;
	var Bid2Appliances					= $('#RefiTable #Bid2Appliances').val() 				? parseFloat($('#RefiTable #Bid2Appliances').val()) 				: 0;
	var Bid2OtherInterior				= $('#RefiTable #Bid2OtherInterior').val() 			? parseFloat($('#RefiTable #Bid2OtherInterior').val()) 			: 0;
	
	var Bid2ExteriorTrim				= $('#RefiTable #Bid2ExteriorTrim').val() 				? parseFloat($('#RefiTable #Bid2ExteriorTrim').val()) 				: 0;
	var Bid2ExteriorDoors				= $('#RefiTable #Bid2ExteriorDoors').val()			 	? parseFloat($('#RefiTable #Bid2ExteriorDoors').val()) 			: 0;
	var Bid2Porches						= $('#RefiTable #Bid2Porches').val() 					? parseFloat($('#RefiTable #Bid2Porches').val()) 					: 0;
	var Bid2Siding						= $('#RefiTable #Bid2Siding').val() 					? parseFloat($('#RefiTable #Bid2Siding').val()) 					: 0;
	var Bid2ExteriorPainting			= $('#RefiTable #Bid2ExteriorPainting').val() 			? parseFloat($('#RefiTable #Bid2ExteriorPainting').val()) 			: 0;
	var Bid2Roof						= $('#RefiTable #Bid2Roof').val() 						? parseFloat($('#RefiTable #Bid2Roof').val()) 						: 0;
	var Bid2GuttersDownspouts			= $('#RefiTable #Bid2GuttersDownspouts').val() 		? parseFloat($('#RefiTable #Bid2GuttersDownspouts').val()) 		: 0;
	var Bid2Fencing						= $('#RefiTable #Bid2Fencing').val() 					? parseFloat($('#RefiTable #Bid2Fencing').val()) 					: 0;
	var Bid2Landscaping					= $('#RefiTable #Bid2Landscaping').val() 				? parseFloat($('#RefiTable #Bid2Landscaping').val()) 				: 0;
	var Bid2DrivewayConcrete			= $('#RefiTable #Bid2DrivewayConcrete').val() 			? parseFloat($('#RefiTable #Bid2DrivewayConcrete').val()) 			: 0;
	var Bid2FoudationWork				= $('#RefiTable #Bid2FoudationWork').val() 			? parseFloat($('#RefiTable #Bid2FoudationWork').val()) 			: 0;
	var Bid2BrickPointingReplacement	= $('#RefiTable #Bid2BrickPointingReplacement').val() 	? parseFloat($('#RefiTable #Bid2BrickPointingReplacement').val()) 	: 0;
	var Bid2OtherExterior				= $('#RefiTable #Bid2OtherExterior').val() 			? parseFloat($('#RefiTable #Bid2OtherExterior').val()) 			: 0;
	
	var Bid2Contingency					= $('#RefiTable #Bid2Contingency').val() 				? parseFloat($('#RefiTable #Bid2Contingency').val()) 				: 0;
	var Bid2GCFee						= $('#RefiTable #Bid2GCFee').val() 					? parseFloat($('#RefiTable #Bid2GCFee').val()) 					: 0;
	var Bid2Cleanup						= $('#RefiTable #Bid2Cleanup').val() 					? parseFloat($('#RefiTable #Bid2Cleanup').val()) 					: 0;
	var Bid2OtherOther					= $('#RefiTable #Bid2OtherOther').val() 				? parseFloat($('#RefiTable #Bid2OtherOther').val()) 				: 0;
	
	var TotalBid2Total = Bid2Demo + Bid2Architectural + Bid2MasterBuildingPermit + Bid2Plumbing + Bid2Electrical + Bid2HVAC + Bid2Dumpster + Bid2Other + Bid2WallFraming + Bid2FloorFraming + Bid2CeilingFraming + Bid2InteriorElectrical + Bid2InteriorPlumbing + Bid2InteriorHVAC + Bid2Flooring + Bid2Sheetrock + Bid2Windows + Bid2InteriorDoors + Bid2Trim + Bid2BathroomVanities + Bid2BathroomFixtures + Bid2KitchenCabinets + Bid2LaborInstallKitchen + Bid2FloorCoverings1 + Bid2FloorCoverings2 + Bid2FloorCoverings3 + Bid2InteriorPainting + Bid2LightFixtures + Bid2OtherFixtures + Bid2Appliances + Bid2OtherInterior + Bid2ExteriorTrim + Bid2ExteriorDoors + Bid2Porches + Bid2Siding + Bid2ExteriorPainting + Bid2Roof + Bid2GuttersDownspouts + Bid2Fencing + Bid2Landscaping + Bid2DrivewayConcrete + Bid2FoudationWork + Bid2BrickPointingReplacement + Bid2OtherExterior + Bid2Contingency + Bid2GCFee + Bid2Cleanup + Bid2OtherOther;
	
	TotalBid2Total = TotalBid2Total ? parseFloat(TotalBid2Total) : 0;
	$('#RefiTable #TotalBid2Total').val(TotalBid2Total.toFixed(2));
}

// Bid 3 Calculation
function RefiTotalBid3TotalCalculation() {
	var Bid3Demo 						= $('#RefiTable #Bid3Demo').val() 						? parseFloat($('#RefiTable #Bid3Demo').val()) 						: 0;
	var Bid3Architectural				= $('#RefiTable #Bid3Architectural').val() 			? parseFloat($('#RefiTable #Bid3Architectural').val()) 			: 0;
	var Bid3MasterBuildingPermit		= $('#RefiTable #Bid3MasterBuildingPermit').val() 		? parseFloat($('#RefiTable #Bid3MasterBuildingPermit').val()) 		: 0;
	var Bid3Plumbing					= $('#RefiTable #Bid3Plumbing').val() 					? parseFloat($('#RefiTable #Bid3Plumbing').val()) 					: 0;
	var Bid3Electrical					= $('#RefiTable #Bid3Electrical').val() 				? parseFloat($('#RefiTable #Bid3Electrical').val()) 				: 0;
	var Bid3HVAC						= $('#RefiTable #Bid3HVAC').val() 						? parseFloat($('#RefiTable #Bid3HVAC').val()) 						: 0;
	var Bid3Dumpster					= $('#RefiTable #Bid3Dumpster').val() 					? parseFloat($('#RefiTable #Bid3Dumpster').val()) 					: 0;
	var Bid3Other						= $('#RefiTable #Bid3Other').val() 					? parseFloat($('#RefiTable #Bid3Other').val()) 					: 0;
	
	var Bid3WallFraming					= $('#RefiTable #Bid3WallFraming').val() 				? parseFloat($('#RefiTable #Bid3WallFraming').val()) 				: 0;
	var Bid3FloorFraming				= $('#RefiTable #Bid3FloorFraming').val() 				? parseFloat($('#RefiTable #Bid3FloorFraming').val()) 				: 0;
	var Bid3CeilingFraming				= $('#RefiTable #Bid3CeilingFraming').val() 			? parseFloat($('#RefiTable #Bid3CeilingFraming').val()) 			: 0;
	var Bid3InteriorElectrical			= $('#RefiTable #Bid3InteriorElectrical').val() 		? parseFloat($('#RefiTable #Bid3InteriorElectrical').val()) 		: 0;
	var Bid3InteriorPlumbing			= $('#RefiTable #Bid3InteriorPlumbing').val() 			? parseFloat($('#RefiTable #Bid3InteriorPlumbing').val()) 			: 0;
	var Bid3InteriorHVAC				= $('#RefiTable #Bid3InteriorHVAC').val() 				? parseFloat($('#RefiTable #Bid3InteriorHVAC').val()) 				: 0;
	var Bid3Flooring					= $('#RefiTable #Bid3Flooring').val() 					? parseFloat($('#RefiTable #Bid3Flooring').val()) 					: 0;
	var Bid3Sheetrock					= $('#RefiTable #Bid3Sheetrock').val() 				? parseFloat($('#RefiTable #Bid3Sheetrock').val()) 				: 0;
	var Bid3Windows						= $('#RefiTable #Bid3Windows').val() 					? parseFloat($('#RefiTable #Bid3Windows').val()) 					: 0;
	var Bid3InteriorDoors				= $('#RefiTable #Bid3InteriorDoors').val() 			? parseFloat($('#RefiTable #Bid3InteriorDoors').val()) 			: 0;
	var Bid3Trim						= $('#RefiTable #Bid3Trim').val() 						? parseFloat($('#RefiTable #Bid3Trim').val()) 						: 0;
	var Bid3BathroomVanities			= $('#RefiTable #Bid3BathroomVanities').val() 			? parseFloat($('#RefiTable #Bid3BathroomVanities').val()) 			: 0;
	var Bid3BathroomFixtures			= $('#RefiTable #Bid3BathroomFixtures').val() 			? parseFloat($('#RefiTable #Bid3BathroomFixtures').val()) 			: 0;
	var Bid3KitchenCabinets				= $('#RefiTable #Bid3KitchenCabinets').val() 			? parseFloat($('#RefiTable #Bid3KitchenCabinets').val()) 			: 0;
	var Bid3LaborInstallKitchen			= $('#RefiTable #Bid3LaborInstallKitchen').val() 		? parseFloat($('#RefiTable #Bid3LaborInstallKitchen').val()) 		: 0;
	var Bid3FloorCoverings1				= $('#RefiTable #Bid3FloorCoverings1').val() 			? parseFloat($('#RefiTable #Bid3FloorCoverings1').val()) 			: 0;
	var Bid3FloorCoverings2				= $('#RefiTable #Bid3FloorCoverings2').val() 			? parseFloat($('#RefiTable #Bid3FloorCoverings2').val()) 			: 0;
	var Bid3FloorCoverings3				= $('#RefiTable #Bid3FloorCoverings3').val() 			? parseFloat($('#RefiTable #Bid3FloorCoverings3').val()) 			: 0;
	var Bid3InteriorPainting			= $('#RefiTable #Bid3InteriorPainting').val() 			? parseFloat($('#RefiTable #Bid3InteriorPainting').val()) 			: 0;
	var Bid3LightFixtures				= $('#RefiTable #Bid3LightFixtures').val() 			? parseFloat($('#RefiTable #Bid3LightFixtures').val()) 			: 0;
	var Bid3OtherFixtures				= $('#RefiTable #Bid3OtherFixtures').val() 			? parseFloat($('#RefiTable #Bid3OtherFixtures').val()) 			: 0;
	var Bid3Appliances					= $('#RefiTable #Bid3Appliances').val() 				? parseFloat($('#RefiTable #Bid3Appliances').val()) 				: 0;
	var Bid3OtherInterior				= $('#RefiTable #Bid3OtherInterior').val() 			? parseFloat($('#RefiTable #Bid3OtherInterior').val()) 			: 0;
	
	var Bid3ExteriorTrim				= $('#RefiTable #Bid3ExteriorTrim').val() 				? parseFloat($('#RefiTable #Bid3ExteriorTrim').val()) 				: 0;
	var Bid3ExteriorDoors				= $('#RefiTable #Bid3ExteriorDoors').val()			 	? parseFloat($('#RefiTable #Bid3ExteriorDoors').val()) 			: 0;
	var Bid3Porches						= $('#RefiTable #Bid3Porches').val() 					? parseFloat($('#RefiTable #Bid3Porches').val()) 					: 0;
	var Bid3Siding						= $('#RefiTable #Bid3Siding').val() 					? parseFloat($('#RefiTable #Bid3Siding').val()) 					: 0;
	var Bid3ExteriorPainting			= $('#RefiTable #Bid3ExteriorPainting').val() 			? parseFloat($('#RefiTable #Bid3ExteriorPainting').val()) 			: 0;
	var Bid3Roof						= $('#RefiTable #Bid3Roof').val() 						? parseFloat($('#RefiTable #Bid3Roof').val()) 						: 0;
	var Bid3GuttersDownspouts			= $('#RefiTable #Bid3GuttersDownspouts').val() 		? parseFloat($('#RefiTable #Bid3GuttersDownspouts').val()) 		: 0;
	var Bid3Fencing						= $('#RefiTable #Bid3Fencing').val() 					? parseFloat($('#RefiTable #Bid3Fencing').val()) 					: 0;
	var Bid3Landscaping					= $('#RefiTable #Bid3Landscaping').val() 				? parseFloat($('#RefiTable #Bid3Landscaping').val()) 				: 0;
	var Bid3DrivewayConcrete			= $('#RefiTable #Bid3DrivewayConcrete').val() 			? parseFloat($('#RefiTable #Bid3DrivewayConcrete').val()) 			: 0;
	var Bid3FoudationWork				= $('#RefiTable #Bid3FoudationWork').val() 			? parseFloat($('#RefiTable #Bid3FoudationWork').val()) 			: 0;
	var Bid3BrickPointingReplacement	= $('#RefiTable #Bid3BrickPointingReplacement').val() 	? parseFloat($('#RefiTable #Bid3BrickPointingReplacement').val()) 	: 0;
	var Bid3OtherExterior				= $('#RefiTable #Bid3OtherExterior').val() 			? parseFloat($('#RefiTable #Bid3OtherExterior').val()) 			: 0;
	
	var Bid3Contingency					= $('#RefiTable #Bid3Contingency').val() 				? parseFloat($('#RefiTable #Bid3Contingency').val()) 				: 0;
	var Bid3GCFee						= $('#RefiTable #Bid3GCFee').val() 					? parseFloat($('#RefiTable #Bid3GCFee').val()) 					: 0;
	var Bid3Cleanup						= $('#RefiTable #Bid3Cleanup').val() 					? parseFloat($('#RefiTable #Bid3Cleanup').val()) 					: 0;
	var Bid3OtherOther					= $('#RefiTable #Bid3OtherOther').val() 				? parseFloat($('#RefiTable #Bid3OtherOther').val()) 				: 0;
	
	var TotalBid3Total = Bid3Demo + Bid3Architectural + Bid3MasterBuildingPermit + Bid3Plumbing + Bid3Electrical + Bid3HVAC + Bid3Dumpster + Bid3Other + Bid3WallFraming + Bid3FloorFraming + Bid3CeilingFraming + Bid3InteriorElectrical + Bid3InteriorPlumbing + Bid3InteriorHVAC + Bid3Flooring + Bid3Sheetrock + Bid3Windows + Bid3InteriorDoors + Bid3Trim + Bid3BathroomVanities + Bid3BathroomFixtures + Bid3KitchenCabinets + Bid3LaborInstallKitchen + Bid3FloorCoverings1 + Bid3FloorCoverings2 + Bid3FloorCoverings3 + Bid3InteriorPainting + Bid3LightFixtures + Bid3OtherFixtures + Bid3Appliances + Bid3OtherInterior + Bid3ExteriorTrim + Bid3ExteriorDoors + Bid3Porches + Bid3Siding + Bid3ExteriorPainting + Bid3Roof + Bid3GuttersDownspouts + Bid3Fencing + Bid3Landscaping + Bid3DrivewayConcrete + Bid3FoudationWork + Bid3BrickPointingReplacement + Bid3OtherExterior + Bid3Contingency + Bid3GCFee + Bid3Cleanup + Bid3OtherOther;
	
	TotalBid3Total = TotalBid3Total ? parseFloat(TotalBid3Total) : 0;
	$('#RefiTable #TotalBid3Total').val(TotalBid3Total.toFixed(2));
}


// Check Month Exceed
function CheckRefiMonthRehab(SelfElem, CheckElem) {
	var SelfVal = parseFloat($(SelfElem).val()), CheckVal = parseFloat($('#' + CheckElem).val());
	if ( SelfVal > CheckVal ) {
		$(SelfElem).parents('td').next('td').text('Month Exceeds Rehab Timeline');
	} else {
		$(SelfElem).parents('td').next('td').text('');
	}
}

// Refi Rehab Draw Selection Change
function RefiRehabDrawSelectionChange() {
	if ( $('#RefiRehabDrawSelection').val() == 'Fund Rehab at Closing' ) {
		if ( $('#RefiRehabBudgetMethod').val() == 'Quick Lump Sum' ) {
			var LumpSumRefiBudget = parseFloat($('#LumpSumRefiBudget').val());
			$('#MonthTotal_0').text(LumpSumRefiBudget);
		} else {
			var DetailedRefiBudget = parseFloat($('#DetailedRefiBudget').val())
			$('#MonthTotal_0').text(DetailedRefiBudget);
		}
	} else {
		$('#MonthTotal_0').text(0);
	}
}