jQuery(document).ready(function($) {
	"use strict";
	
	var base_url = $('#base_url').val();
	
	var FormData = $('form[name="RentalRefiForm"]').serialize();
	$.ajax({
		type: "POST",
		url: base_url + "pages/report-refi-calc/",
		data: FormData,
		success: function(data) {
			$('#UpdatedRows').html(data);
		}
	});
	
	$('.form-control').on('change blur focus', function() {
		var FormData = $('form[name="RentalRefiForm"]').serialize();
		$.ajax({
			type: "POST",
			url: base_url + "pages/report-refi-calc/",
			data: FormData,
			success: function(data) {
				$('#UpdatedRows').html(data);
			}
		});
	}); 
	
});