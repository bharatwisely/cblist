<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cblistblog');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'wisely@123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         ':^-Tk=/*W%DvFpI#U?8Q0[gxXvhQ 7HNR2rqjt_g#_d+zs@XO83q][)A1?H5FI+^');
define('SECURE_AUTH_KEY',  'Zk&i7}+VmtUs1w(-.LX|*]*s}AAaC1]Pxw2+s?R-+N8mQ,%8m.PKkn|u}%Upt}T!');
define('LOGGED_IN_KEY',    '0lQdgD/`N<Ey1R;-d}.4.K[O=eUBY5{_b:^iq(#}WjY:Vy-La~(VB5C<!2Xu|d|V');
define('NONCE_KEY',        't|)1nRm?`62:|OS)V>+:?j[!HrJQV0i>CMgM<;vP{~aI^>N;fg^$h0trg&Ue2:k%');
define('AUTH_SALT',        'h>s1hSVHWD?sDL{<&)UD>&`- i][ED}w0C2H/}<=c+n|0#%X@nMGqW}u_y.qxyqM');
define('SECURE_AUTH_SALT', 'F9`)|d%P]?9+B[*xh`Z;Xwu/XS={B`:fi3Z`U9!3WT{-]orwp:gBS$ucP1WbJC|+');
define('LOGGED_IN_SALT',   'yeY)!+33$R&ZY-a_M+(z%N!JL)V,LwPRSLk^HDHZG@@uFs)/2ncuvyS=gM]Rh~2]');
define('NONCE_SALT',       'dSy0jVR-|iZDl]O[&<You<k5K]|??iqDDFTr~#sS-qd~YP7zArV/f/=g<F2O7u#|');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
