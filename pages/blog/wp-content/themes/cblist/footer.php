<footer>
	<div class="container">
        <div class="row">
            <div class="col-lg-6">
                <p class="text-left">
                    <a href="<?php bloginfo("url"); ?>">Blog</a> | 
                    <a href="<?php echo HOME_URL; ?>pages/our-offer/">Our Offer</a> | 
                    <a href="<?php echo HOME_URL; ?>pages/features/">Features</a>
                </p>
            </div>
            <div class="col-lg-6">
                <p class="text-right">&copy; Copyright <?php echo date('Y'); ?>. Realty Fund Connect.</p>
            </div>
        </div>
	</div>
</footer>

<!-- Bootstrap API Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.min.js"></script>
<!-- Custom function js -->
<script src="<?php echo get_template_directory_uri(); ?>/js/functions.js"></script>
<!-- Bootstrap Progress Bar Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-progressbar.js"></script>
<!-- Bootstrap Datepicker Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-datepicker.js"></script>
<!-- Bootstrap Datepicker Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-slider.js"></script>
<!-- Bootstrap Star Rating Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap-star-rating.min.js"></script>
<!-- DataTablses jQuery Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.dataTables.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/dataTables.bootstrap.js"></script>
<!-- jQuery Character Counter Scripts -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.char-count.js"></script>
<!-- Flex Slider Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.flexslider-min.js"></script>
<!-- Mousewheel Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.mousewheel-3.0.6.pack.js"></script>
<!-- Fancybox Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox.pack.js?v=2.1.5"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox-media.js?v=1.0.6"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<!-- Morris.js charts -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.nanoscroller.min.js"></script>
<!-- Morris.js charts -->
<script src="<?php echo get_template_directory_uri(); ?>/js/raphael-min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/morris.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/chart.js"></script>
<!-- Responside Sidebar menu API -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.sidr.min.js"></script>
<!-- Ajax file upload API -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.form.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/common.js"></script>
<!-- Place script file -->
<script src="<?php echo get_template_directory_uri(); ?>/js/place.js"></script>
<!-- Custom script file -->
<script src="<?php echo get_template_directory_uri(); ?>/js/scripts.js"></script>
<?php wp_footer(); ?>
</body>
</html>