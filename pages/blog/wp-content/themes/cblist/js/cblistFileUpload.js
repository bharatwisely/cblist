jQuery(function() {
	jQuery(function() {
		jQuery.fn.wptheme = function(options) {
			var selector = jQuery(this).selector; // Get the selector
			// Set default options
			var defaults = {
				'preview' : '.preview-upload',
				'text' : '.text-upload',
				'button' : '.button-upload',
			};
			var options = jQuery.extend(defaults, options);
			
			// When the Button is clicked...
			jQuery(options.button).click(function() { 
				// Get the Text element.
				var text = jQuery(this).siblings(options.text);
				
				// Show WP Media Uploader popup
				tb_show('Upload', 'media-upload.php?referer=wptheme&type=image&TB_iframe=true&post_id=0', false);
				
				// Re-define the global function 'send_to_editor'
				// Define where the new value will be sent to
				window.send_to_editor = function(html) {
					// Get the URL of new image
					var src = jQuery('img', html).attr('src');
					// Send this value to the Text field.
					text.attr('value', src).trigger('change'); 
					tb_remove(); // Then close the popup window
				}
				return false;
			});
		
			jQuery(options.text).bind('change', function() {
				// Get the value of current object
				var url = this.value;
				// Determine the Preview field
				var preview = jQuery(this).siblings(options.preview);
				// Bind the value to Preview field
				jQuery(preview).attr('src', url);
			});
		}
		
		// Usage
		jQuery('.upload-form').wptheme(); // Use as default option.
	});
});