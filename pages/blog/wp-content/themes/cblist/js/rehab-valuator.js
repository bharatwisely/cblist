jQuery(document).ready(function($) {
    "use strict";
	
	$('.form-control').on('change blur focus', function() {
        if ( $('#HoldingCostsOption').val() == 'Quick Lump Sum' ) {
			
			$('#TotalHoldingCostsRow').show();
			$('#TotalHoldingCosts').show();
			$('#TotalHoldingCostsRow').find('label').text('Enter Total Holding Costs Here:');
			$('#InsurancePolicyPremiumRow').hide();
			$('#ElectricalBillsRow').hide();
			$('#GasBillsRow').hide();
			$('#OilBillsRow').hide();
			$('#WaterSewerBillsRow').hide();
			$('#AlarmSystemBillsRow').hide();
			$('#MiscellaniousRow').hide();
			$('#LawncareRow').hide();
			$('#HoldingOther1Row').hide();
			$('#HoldingOther2Row').hide();
			
			var HoldingTotal = $('#TotalHoldingCosts').val() ? parseFloat($('#TotalHoldingCosts').val()) : 0;
			$('#TotalPurchaseHoldingCosts').val(HoldingTotal.toFixed(2));
			
			var TotalHoldingCosts = $('#TotalPurchaseHoldingCosts').val() ? parseFloat($('#TotalPurchaseHoldingCosts').val()) : 0;
			$('#FlipHoldingCosts').val(TotalHoldingCosts.toFixed(2));
			$('#RefiHoldingCosts').val(TotalHoldingCosts.toFixed(2));
			
		} else if ( $('#HoldingCostsOption').val() == 'Detailed Input' ) {
			
			$('#TotalHoldingCostsRow').hide();
			$('#TotalHoldingCosts').hide();
			$('#TotalHoldingCostsRow').find('label').text('Enter Itemized Forecast Below');			
			$('#InsurancePolicyPremiumRow').show().find('label').text('Insurance Policy Premium');
			$('#ElectricalBillsRow').show().find('label').text('Electrical Bills');
			$('#GasBillsRow').show().find('label').text('Gas Bills');
			$('#OilBillsRow').show().find('label').text('Oil Bills');
			$('#WaterSewerBillsRow').show().find('label').text('Water/Sewer Bills');
			$('#AlarmSystemBillsRow').show().find('label').text('Alarm System Bills');
			$('#MiscellaniousRow').show().find('label').text('Miscellanious');
			$('#LawncareRow').show().find('label').text('Lawncare');
			$('#HoldingOther1Row').show().find('label').text('Other');
			$('#HoldingOther2Row').show().find('label').text('Other');	
			
			var InsurancePolicyPremium 	= $('#InsurancePolicyPremium').val()	? parseFloat($('#InsurancePolicyPremium').val())	: 0;
			var ElectricalBills 		= $('#ElectricalBills').val()			? parseFloat($('#ElectricalBills').val())			: 0;
			var GasBills				= $('#GasBills').val()					? parseFloat($('#GasBills').val())					: 0;
			var OilBills 				= $('#OilBills').val()					? parseFloat($('#OilBills').val())					: 0;
			var WaterSewerBills			= $('#WaterSewerBills').val()			? parseFloat($('#WaterSewerBills').val())			: 0;
			var AlarmSystemBills		= $('#AlarmSystemBills').val()			? parseFloat($('#AlarmSystemBills').val())			: 0;
			var Miscellanious 			= $('#Miscellanious').val()				? parseFloat($('#Miscellanious').val())				: 0;
			var Lawncare				= $('#Lawncare').val()					? parseFloat($('#Lawncare').val())					: 0;
			var HoldingOther1			= $('#HoldingOther1').val()				? parseFloat($('#HoldingOther1').val())				: 0;
			var HoldingOther2 			= $('#HoldingOther2').val()				? parseFloat($('#HoldingOther2').val())				: 0;
			var HoldingTotal = parseFloat(InsurancePolicyPremium + ElectricalBills + GasBills + OilBills + WaterSewerBills + AlarmSystemBills + Miscellanious + Lawncare + HoldingOther1 + HoldingOther2);
			$('#TotalPurchaseHoldingCosts').val(HoldingTotal.toFixed(2));			
			
			var TotalHoldingCosts = $('#TotalPurchaseHoldingCosts').val() ? parseFloat($('#TotalPurchaseHoldingCosts').val()) : 0;
			
			var MonthsFlipRehab = $('#MonthsFlipRehab').val() ? parseInt($('#MonthsFlipRehab').val()) : 0;
			var MonthsToSell = $('#MonthsToSell').val() ? parseInt($('#MonthsToSell').val()) : 0;
			var FlipHoldingCosts = parseFloat(TotalHoldingCosts * (MonthsFlipRehab + MonthsToSell));
			$('#FlipHoldingCosts').val(FlipHoldingCosts.toFixed(2));
			
			var MonthsRefiRehab = $('#MonthsRefiRehab').val() ? parseInt($('#MonthsRefiRehab').val()) : 0;
			var MonthsToRefi = $('#MonthsToRefi').val() ? parseInt($('#MonthsToRefi').val()) : 0;
			var RefiHoldingCosts = parseFloat(TotalHoldingCosts * (MonthsRefiRehab + MonthsToRefi));
			$('#RefiHoldingCosts').val(RefiHoldingCosts.toFixed(2));
			
		}
		
		if ( $('#ClosingCostsOption').val() == 'Quick Lump Sum' ) {
		
			$('#ItemizedForecastRow').show();
			$('#TotalClosingCosts').show();
			$('#TotalClosingCostsRow').find('label').text('Enter Total Closing Costs Here:');
			$('#AttorneyClosingFeesRow').hide();
			$('#TitleSearchRow').hide();
			$('#TitleInsuranceRow').hide();
			$('#RecordingFeesRow').hide();
			$('#AppraisalRow').hide();
			$('#InspectionRow').hide();
			$('#RealEstateTaxesDueRow').hide();
			$('#ClosingOther1Row').hide();
			$('#ClosingOther2Row').hide();
			$('#ClosingOther3Row').hide();
			
			var ClosingTotal = $('#TotalClosingCosts').val() ? parseFloat($('#TotalClosingCosts').val()) : 0;
			ClosingTotal = ClosingTotal ? parseFloat(ClosingTotal) : 0;
			$('#TotalPurchaseClosingCosts').val(ClosingTotal.toFixed(2));
			
		} else if ( $('#ClosingCostsOption').val() == 'Detailed Input' ) {
			
			$('#ItemizedForecastRow').hide();
			$('#TotalClosingCosts').hide();
			$('#TotalClosingCostsRow').find('label').text('Enter Itemized Forecast Below');
			$('#AttorneyClosingFeesRow').show().find('label').text('Attorney\'s/Closing Fees');
			$('#TitleSearchRow').show().find('label').text('Title Search');
			$('#TitleInsuranceRow').show().find('label').text('Title Insurance');
			$('#RecordingFeesRow').show().find('label').text('Recording Fees');
			$('#AppraisalRow').show().find('label').text('Appraisal');
			$('#InspectionRow').show().find('label').text('Inspection');
			$('#RealEstateTaxesDueRow').show().find('label').text('Real Estate Taxes Due');
			$('#ClosingOther1Row').show().find('label').text('Other');
			$('#ClosingOther2Row').show().find('label').text('Other');
			$('#ClosingOther3Row').show().find('label').text('Other');
			
			var AttorneyClosingFees	= $('#AttorneyClosingFees').val()	? parseFloat($('#AttorneyClosingFees').val())	: 0;
			var TitleSearch 		= $('#TitleSearch').val()			? parseFloat($('#TitleSearch').val())			: 0;
			var TitleInsurance		= $('#TitleInsurance').val()		? parseFloat($('#TitleInsurance').val())		: 0;
			var RecordingFees 		= $('#RecordingFees').val()			? parseFloat($('#RecordingFees').val())			: 0;
			var Appraisal			= $('#Appraisal').val()				? parseFloat($('#Appraisal').val())				: 0;
			var Inspection			= $('#Inspection').val()			? parseFloat($('#Inspection').val())			: 0;
			var RealEstateTaxesDue	= $('#RealEstateTaxesDue').val()	? parseFloat($('#RealEstateTaxesDue').val())	: 0;
			var ClosingOther1		= $('#ClosingOther1').val()			? parseFloat($('#ClosingOther1').val())			: 0;
			var ClosingOther2		= $('#ClosingOther2').val()			? parseFloat($('#ClosingOther2').val())			: 0;
			var ClosingOther3 		= $('#ClosingOther3').val()			? parseFloat($('#ClosingOther3').val())			: 0;		
			var ClosingTotal = parseFloat(AttorneyClosingFees + TitleSearch + TitleInsurance + RecordingFees + Appraisal + Inspection + RealEstateTaxesDue + ClosingOther1 + ClosingOther2 + ClosingOther3);
			$('#TotalPurchaseClosingCosts').val(ClosingTotal.toFixed(2));
			
		}
		
		
		if ( $('#CapSelection').val() == 'ARV' ) {
			$('#FinancingCapRow').show().find('label').text('Max % of ARV to be financed');			
		} else if ( $('#CapSelection').val() == 'Cost' ) {
			$('#FinancingCapRow').show().find('label').text('Max % of Cost to be financed');			
		}
		
		
		if ( $('#SplitWithLender').val() == 'Yes' ) {
			$('#LenderSplitRow').show().find('label').text('What % of Pre-Tax Profits Does Lender Get?');			
		} else if ( $('#SplitWithLender').val() == 'No' ) {
			$('#LenderSplitRow').hide();
		}
		
		
		if ( $('#FinancingUsed').val() == 'Financing' ) {
			
			$('#FinanceClosingHoldingRow').show().find('label').text('Include Closings/Holding Costs in the loan?');
			$('#CapSelectionRow').show().find('label').text('Lender caps ARV or Cost of Project?');		
			if ( $('#CapSelection').val() == 'ARV' ) {
				$('#FinancingCapRow').show().find('label').text('Max % of ARV to be financed');
			} else if ( $('#CapSelection').val() == 'Cost' ) {
				$('#FinancingCapRow').show().find('label').text('Max % of Cost to be financed');
			}
			$('#RehabDiscountPointsRow').show().find('label').text('Origination/Discount Points');
			$('#OtherPointsRow').show().find('label').text('Other Closing Costs Paid to Lender');
			$('#PointsClosingUpfrontRow').show().find('label').text('Points and Closings Costs Upfront or Back-End?');
			$('#RehabInterestRateRow').show().find('label').text('Interest Rate');
			$('#InterestPaymentsMadeRow').show().find('label').text('Interest Payments During Rehab?');
			$('#SplitWithLenderRow').show().find('label').text('Split Back-End Profits with Lender?');		
			if ( $('#SplitWithLender').val() == 'Yes' ) {
				$('#LenderSplitRow').show().find('label').text('What % of Pre-Tax Profits Does Lender Get?');
			} else if ( $('#SplitWithLender').val() == 'No' ) {
				$('#LenderSplitRow').hide();
			}
			
			$('#MaxFinancedFlipRow').show();
			$('#ActualFinancedFlipRow').show();
			$('#PointInterestRolledUpFlipRow').show();
			$('#TotalLoanFlipRow').show();
			
			$('#MaxFinancedRefiRow').show();
			$('#ActualFinancedRentRow').show();
			$('#PointsInterestRolledupRentRow').show();
			$('#TotalLoanRentRow').show();
			
			$('#FinancingUsed').parents('.assump-box').find('.AssumpHR').show();		
			if ( $('#SplitWithLender').val() == 'Yes' ) {
				$('#FlipProfitRow').find('label').html('<strong>Projected Profit (after any lender split)</strong>');
				$('#RefiProfitRow').find('label').html('<strong>Projected Profit (after any lender split)</strong>');
			} else {
				$('#FlipProfitRow').find('label').html('<strong>Projected Profit</strong>');
				$('#RefiProfitRow').find('label').html('<strong>Projected Profit</strong>');
			}
			$('#MaxFinancedRefiRow').show().find('label').text('Max $ that can be financed');
			$('#ActualFinancedRentRow').show().find('label').text('Actual to be financed (not inc. closing/holding)');
			
		} else if ( $('#FinancingUsed').val() == 'All Cash' ) {
			
			$('#FinanceClosingHoldingRow').hide();
			$('#CapSelectionRow').hide();
			$('#FinancingCapRow').hide();
			$('#RehabDiscountPointsRow').hide();
			$('#OtherPointsRow').hide();
			$('#RehabDiscountPointsRow').hide();
			$('#OtherPointsRow').hide();
			$('#PointsClosingUpfrontRow').hide();
			$('#RehabInterestRateRow').hide();
			$('#InterestPaymentsMadeRow').hide();
			$('#SplitWithLenderRow').hide();
			$('#LenderSplitRow').hide();
			$('#FinancingUsed').parents('.assump-box').find('.AssumpHR').hide();
			$('#MaxFinancedFlipRow').hide();
			$('#ActualFinancedFlipRow').hide();
			$('#PointInterestRolledUpFlipRow').hide();
			$('#TotalLoanFlipRow').hide();
			$('#FlipProfitRow').find('label').text('Projected Profit');
			$('#MaxFinancedRefiRow').hide();
			$('#ActualFinancedRentRow').hide();
			$('#PointsInterestRolledupRentRow').hide();
			$('#TotalLoanRentRow').hide();
			
		}
		
		
		// TotalCapitalNeededFlip Calculation 
        var PurchasePrice = $('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0;
		var ActualFlipBudget = $('#ActualFlipBudget').val() ? parseFloat($('#ActualFlipBudget').val()) : 0;
		var ClosingCosts = $('#ClosingCosts').val() ? parseFloat($('#ClosingCosts').val()) : 0;
		var FlipHoldingCosts = $('#FlipHoldingCosts').val() ? parseInt($('#FlipHoldingCosts').val()) : 0;
		var TotalCapitalNeededFlip = parseFloat(PurchasePrice + ActualFlipBudget + ClosingCosts + FlipHoldingCosts);
		$('#TotalCapitalNeededFlip').val(TotalCapitalNeededFlip.toFixed(2));
		
		
		// MaxFinancedFlip Calculation 
		if ( $('#FinancingUsed').val() == 'All Cash' ) {
			$('#MaxFinancedFlip').val('');
		} else {
			var FinancingCap = $('#FinancingCap').val() ? parseFloat($('#FinancingCap').val()) : 0;
			if ( $('#CapSelection').val() == 'ARV' ) {
				var ARVFlip = $('#ARVFlip').val() ? parseFloat($('#ARVFlip').val()) : 0;
				var MaxFinancedFlip = parseFloat(ARVFlip * (FinancingCap / 100));
				$('#MaxFinancedFlip').val(MaxFinancedFlip.toFixed(2));
			} else {
				var PurchasePrice = $('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0;
				var ActualFlipBudget = $('#ActualFlipBudget').val() ? parseFloat($('#ActualFlipBudget').val()) : 0;
				var MaxFinancedFlip = parseFloat((PurchasePrice + ActualFlipBudget) * (FinancingCap / 100));
				$('#MaxFinancedFlip').val(MaxFinancedFlip.toFixed(2));
			}
		}
		
		
		// Start Flip Analysis Column
		// ActualFinancedFlip Calculation
		if ( $('#FinancingUsed').val() == 'All Cash' ) {
			$('#ActualFinancedFlip').val(0);
		} else {
			var MaxFinancedFlip = $('#MaxFinancedFlip').val() ? parseFloat($('#MaxFinancedFlip').val()) : 0;
			var TotalCapitalNeededFlip = $('#TotalCapitalNeededFlip').val() ? parseFloat($('#TotalCapitalNeededFlip').val()) : 0;
			var ClosingCosts = $('#ClosingCosts').val() ? parseFloat($('#ClosingCosts').val()) : 0;
			var FlipHoldingCosts = $('#FlipHoldingCosts').val() ? parseFloat($('#FlipHoldingCosts').val()) : 0;
			var ActualFinancedFlip = parseFloat(Math.min(MaxFinancedFlip, TotalCapitalNeededFlip - (ClosingCosts + FlipHoldingCosts)));
			$('#ActualFinancedFlip').val(ActualFinancedFlip.toFixed(2));
		}
		
		
		// PointInterestRolledUpFlip Calculation  (Calc Sheet)
		var Row19Total = 0, Row22Total = 0, Row29Total = 0, Row32Total = 0;
		var PurchaseClosingCostsFinanced = 0, HoldingCostsFinanced = 0, FlipPoints = 0, FlipOtherFinancingCosts = 0, FlipLoanInterest = 0, FlipRehabInterest = 0;
		
		var MonthTotal = new Array();
		var MonthMatchArray = new Array();
		var FlipBudgetColumn = new Array();
		$("form[name='FlipBudgetForm'] .FlipBudgetColumn").each(function(index, value) {
			FlipBudgetColumn[index] = parseFloat($(this).val());
		});
		var MonthPaidColumn = new Array();
		$("form[name='FlipBudgetForm'] .FlipMonthPaidColumn").each(function(index, value) {
			MonthPaidColumn[index] = (FlipBudgetColumn[index] ? parseInt($(this).val()) : 0);
		});		
		
		var MonthsFlipRehab = $('#MonthsFlipRehab').val() ? parseInt($('#MonthsFlipRehab').val()) : 0;
		var MonthsToSell = $('#MonthsToSell').val() ? parseInt($('#MonthsToSell').val()) : 0;
		var RehabDiscountPoints = $('#RehabDiscountPoints').val() ? parseFloat($('#RehabDiscountPoints').val()/100) : 0;
		var ActualFinancedFlip = $('#ActualFinancedFlip').val() ? parseFloat($('#ActualFinancedFlip').val()) : 0;
		var OtherPoints = $('#OtherPoints').val() ? parseFloat($('#OtherPoints').val()/100) : 0;
		var FlipHoldingCosts = $('#FlipHoldingCosts').val() ? parseFloat($('#FlipHoldingCosts').val()) : 0;
		var RehabInterestRate = $('#RehabInterestRate').val() ? parseFloat($('#RehabInterestRate').val()/100) : 0;
		var LumpSumFlipBudget = $('#LumpSumFlipBudget').val() ? parseFloat($('#LumpSumFlipBudget').val()) : 0;
		var DetailedFlipBudget = $('#DetailedFlipBudget').val() ? parseFloat($('#DetailedFlipBudget').val()) : 0;
		
		var B2 = ($('#FinancingUsed').val()=="Financing" ? "Yes" : "No");
		var B3 = $('#CapSelection').val();
		var B4 = $('#FinancingCap').val() ? parseFloat($('#FinancingCap').val()/100) : 0;
		
		var F2 = $('#ARVFlip').val() ? parseFloat($('#ARVFlip').val()) : 0;
		var F3 = ($('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0) + ($('#ActualFlipBudget').val() ? parseFloat($('#ActualFlipBudget').val()) : 0);
		var F4 = ($('#CapSelection').val()=="ARV" ? (B4*F2) : (B4*F3));
		
		var I2 = $('#ARVRent').val() ? parseFloat($('#ARVRent').val()) : 0;
		var I3 = ($('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0) + ($('#ActualRefiBudget').val() ? parseFloat($('#ActualRefiBudget').val()) : 0);
		var I4 = ($('#CapSelection').val()=="ARV" ? (B4*I2) : (B4*I3));
		
		var B10 = $('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0;
		var B11 = (B2=="Yes" ? (B3=="Cost" ? (B4*B10) : Math.min(B10, F4)) : 0);
		var PurchaseFinanced = B11;
		var B12 = B10-B11;
		var B13 = $('#ClosingCosts').val() ? parseFloat($('#ClosingCosts').val()) : 0;
		
		var B7 = new Array(); var B8 = new Array(); var B9 = new Array(); var B14 = new Array(); var B15 = new Array(); var B16 = new Array(); 
		var B17 = new Array(); var B18 = new Array(); var B19 = new Array(); var B20 = new Array(); var B21 = new Array(); var B22 = new Array(); 
		var B23 = new Array(); var B24 = new Array(); var B25 = new Array(); var B26 = new Array(); var B27 = new Array(); var B28 = new Array(); 
		var B29 = new Array(); var B30 = new Array(); var B31 = new Array(); var B32 = new Array(); var B34 = new Array();
		for ( var i=0; i<=24; i++ ) {
			if ( i == 0 ) {
				B7[i] = 1; <!-- B7 -->
				B8[i] = (i>(MonthsFlipRehab+MonthsToSell) ? 0 : 1); <!-- B8 -->
				B9[i] = ($('#FinancingUsed').val()=="Financing" ? 1 : 0); <!-- B9 -->
				B14[i] = parseFloat($('#PointsClosingUpfront').val()=="Paid Upfront" ? (i==0 ? (RehabDiscountPoints)*ActualFinancedFlip : 0) : (i==(MonthsFlipRehab+MonthsToSell) ? (RehabDiscountPoints)*ActualFinancedFlip : 0)); <!-- B14 -->
				B15[i] = parseFloat($('#PointsClosingUpfront').val()=="Paid Upfront" ? (i==0 ? (OtherPoints)*ActualFinancedFlip : 0) : (i==(MonthsFlipRehab+MonthsToSell) ? (OtherPoints)*ActualFinancedFlip : 0)); <!-- B15 -->
				B16[i] = 0; <!-- B16 -->
				B17[i] = parseFloat(PurchaseFinanced + (B13 + B16[i]) * ($('#FinanceClosingHolding').val()=="Yes" ? 1 : 0)); <!-- B17 -->
				B18[i] = 0; <!-- B18 -->
				B19[i] = 0; <!-- B19 -->
				B20[i] = 0; <!-- B20 -->
				B21[i] = 0; <!-- B21 -->
				B22[i] = 0; <!-- B22 -->
				MonthTotal[i] = ($('#FlipRehabDrawSelection').val()=="Fund Rehab at Closing" ? ($('#FlipRehabBudgetMethod').val()=="Quick Lump Sum" ? parseFloat(LumpSumFlipBudget) : parseFloat(DetailedFlipBudget)) : 0);
				B23[i] = MonthTotal[i]; <!-- B23 -->				
				B24[i] = parseFloat((B3=="Cost" ? MonthTotal[i] * B4 : Math.max(0, Math.min(F4, MonthTotal[i]))) * B9[i]); <!-- B24 -->
				B25[i] = parseFloat(B23[i] - B24[i]); <!-- B25 -->
				B26[i] = parseFloat((B23[i]==0 ? 0 : B23[i]) * B8[i]); <!-- B26 -->
				B27[i] = B24[i]; <!-- B27 -->
				B28[i] = 0; <!-- B28 -->
				B29[i] = 0; <!-- B29 -->
				B30[i] = 0; <!-- B30 -->
				B31[i] = 0; <!-- B31 -->
				B32[i] = 0; <!-- B32 -->
				B34[i] = parseFloat((PurchaseFinanced + B27[i]) * B8[i]); <!-- B34 -->
			} else {
				B7[i] = (i>MonthsFlipRehab ? 0 : 1); <!-- B7 -->
				B8[i] = (i>(MonthsFlipRehab+MonthsToSell) ? 0 : 1); <!-- B8 -->
				B9[i] = ($('#FinancingUsed').val()=="Financing" ? 1 : 0); <!-- B9 -->
				B14[i] = parseFloat($('#PointsClosingUpfront').val()=="Paid Upfront" ? (i==0 ? (RehabDiscountPoints)*ActualFinancedFlip : 0) : (i==(MonthsFlipRehab+MonthsToSell) ? (RehabDiscountPoints)*ActualFinancedFlip : 0)); <!-- B14 -->
				B15[i] = parseFloat($('#PointsClosingUpfront').val()=="Paid Upfront" ? (i==0 ? (OtherPoints)*ActualFinancedFlip : 0) : (i==(MonthsFlipRehab+MonthsToSell) ? (OtherPoints)*ActualFinancedFlip : 0)); <!-- B15 -->
				B16[i] = parseFloat((FlipHoldingCosts/(MonthsFlipRehab+MonthsToSell))*B8[i]); <!-- B16 -->
				B17[i] = parseFloat((B17[i-1] + B16[i] * ($('#FinanceClosingHolding').val()=="Yes" ? 1 : 0))*B8[i]); <!-- B17 -->
				B18[i] = parseFloat(RehabInterestRate*B17[i-1] / 12 * (i>(MonthsFlipRehab+MonthsToSell) ? 0 : 1)); <!-- B18 -->
				B19[i] = parseFloat($('#InterestPaymentsMade').val()=="Yes" ? B18[i] : 0); <!-- B19 -->
				B20[i] = parseFloat(B18[i] - B19[i]); <!-- B20 -->
				if ( i == 1 ) {
					B21[i] = parseFloat(B20[i] + B21[i-1]); <!-- B21 -->
				} else {
					B21[i] = parseFloat((B20[i]+B21[i-1]+B21[i-1]*(RehabInterestRate/12))*B8[i]); <!-- B21 -->
				}
				B22[i] = parseFloat(B21[i] * (i == (MonthsFlipRehab + MonthsToSell) ? 1 : 0)); <!-- B22 -->
				var ColU = (i<=MonthsFlipRehab ? 1 : 0);
				var MonthMatch = 0;
				for ( var j = 0; j < MonthPaidColumn.length; j++ ) {
					if ( MonthPaidColumn[j] == i ) {
						MonthMatch += parseFloat(FlipBudgetColumn[j]);
					}
				}
				MonthMatchArray[i] = parseFloat(MonthMatch);
				MonthTotal[i] = ($('#FlipRehabDrawSelection').val()=="Fund Rehab at Closing" ? 0 : ($('#FlipRehabBudgetMethod').val()=="Detailed Input" ? parseFloat(MonthMatchArray[i] * ColU) : parseFloat(LumpSumFlipBudget / MonthsFlipRehab * ColU)));
				B23[i] = MonthTotal[i]; <!-- B23 -->				
				B24[i] = parseFloat((B3=="Cost" ? MonthTotal[i] * B4 : Math.max(0, Math.min(F4-(B34[i-1]), B23[i]))) * B9[i]); <!-- B24 -->
				B25[i] = parseFloat(B23[i] - B24[i]); <!-- B25 -->
				B26[i] = parseFloat((B23[i]==0 ? B26[i-1] : B23[i] + B26[i-1]) * B8[i]); <!-- B26 -->
				B27[i] = parseFloat((B24[i]==0 ? B27[i-1] : B24[i] + B27[i-1]) * B8[i]); <!-- B27 -->
				B28[i] = parseFloat(B27[i-1] * RehabInterestRate/12 * B8[i] * B9[i]); <!-- B28 -->
				B29[i] = parseFloat($('#InterestPaymentsMade').val()=="Yes" ? B28[i] : 0); <!-- B29 -->
				B30[i] = parseFloat(B28[i] - B29[i]); <!-- B30 -->
				if ( i == 1 ) {
					B31[i] = parseFloat(B30[i] + B31[i-1]); <!-- B31 -->
				} else {
					B31[i] = parseFloat((B30[i] + B31[i-1] + B31[i-1] * (RehabInterestRate/12)) * B8[i]); <!-- B31 -->
				}
				B32[i] = parseFloat(B31[i] * (i==(MonthsFlipRehab+MonthsToSell) ? 1 : 0)); <!-- B32 -->
				B34[i] = parseFloat((PurchaseFinanced + B27[i]) * B8[i]); <!-- B34 -->
			}
			
			Row19Total = parseFloat(Row19Total + B19[i]);
			Row22Total = parseFloat(Row22Total + B22[i]);
			
			Row29Total = parseFloat(Row29Total + B29[i]);
			//alert()
			Row32Total = parseFloat(Row32Total + B32[i]);
			
			HoldingCostsFinanced  = parseFloat(HoldingCostsFinanced + B16[i]);
			FlipPoints = parseFloat(FlipPoints + B14[i]);
			FlipOtherFinancingCosts = parseFloat(FlipOtherFinancingCosts + B15[i]);
		}
		PurchaseClosingCostsFinanced = parseFloat($('#FinanceClosingHolding').val() == "Yes" ? ClosingCosts : 0);
		FlipLoanInterest = parseFloat(Row19Total + Row22Total);
		FlipRehabInterest = parseFloat(Row29Total + Row32Total);
		HoldingCostsFinanced = ($('#FinanceClosingHolding').val()=="Yes" ? HoldingCostsFinanced : 0);
		
		var PointInterestRolledUpFlip = parseFloat($('#FinancingUsed').val() == "All Cash" ? 0 : PurchaseClosingCostsFinanced + HoldingCostsFinanced + ($('#PointsClosingUpfront').val()=="Paid Upfront" ? 0 : (FlipPoints + FlipOtherFinancingCosts)) + ($('#InterestPaymentsMade').val()=="Yes" ? 0 : (FlipLoanInterest + FlipRehabInterest)));
		$('#PointInterestRolledUpFlip').val(PointInterestRolledUpFlip.toFixed(2));
		
		
		// TotalLoanFlip Calculation
		var PointInterestRolledUpFlip = $('#PointInterestRolledUpFlip').val() ? parseFloat($('#PointInterestRolledUpFlip').val()) : 0;
		var ActualFinancedFlip = $('#ActualFinancedFlip').val() ? parseFloat($('#ActualFinancedFlip').val()) : 0;
		var TotalLoanFlip = parseFloat(PointInterestRolledUpFlip + ActualFinancedFlip);
		$('#TotalLoanFlip').val(TotalLoanFlip.toFixed(2));
		
		
		// CashRequiredFlip Calculation
		var TotalCapitalNeededFlip = $('#TotalCapitalNeededFlip').val() ? parseFloat($('#TotalCapitalNeededFlip').val()) : 0;
		var FinancingCostFlip = parseFloat(FlipPoints + FlipOtherFinancingCosts + FlipLoanInterest + FlipRehabInterest);
		//alert(FlipRehabInterest);
		var ActualFinancedFlip = $('#ActualFinancedFlip').val() ? parseFloat($('#ActualFinancedFlip').val()) : 0;
		var PointInterestRolledUpFlip = $('#PointInterestRolledUpFlip').val() ? parseFloat($('#PointInterestRolledUpFlip').val()) : 0;
		var CashRequiredFlip = parseFloat(Math.max(0, (TotalCapitalNeededFlip + FinancingCostFlip - ActualFinancedFlip - PointInterestRolledUpFlip)));
		$('#CashRequiredFlip').val(CashRequiredFlip.toFixed(2));
		
		
		// TotalCostBasisFlip Calculation
		var PurchasePrice = $('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0;
		var ClosingCosts = $('#ClosingCosts').val() ? parseFloat($('#ClosingCosts').val()) : 0;
		var FlipHoldingCosts = $('#FlipHoldingCosts').val() ? parseFloat($('#FlipHoldingCosts').val()) : 0;
		var ActualFlipBudget = $('#ActualFlipBudget').val() ? parseFloat($('#ActualFlipBudget').val()) : 0;
		var FinancingCostFlip = parseFloat(FlipPoints + FlipOtherFinancingCosts + FlipLoanInterest + FlipRehabInterest);
		var TotalCostBasisFlip = parseFloat(PurchasePrice + ClosingCosts + FlipHoldingCosts + ActualFlipBudget + FinancingCostFlip);
		$('#TotalCostBasisFlip').val(TotalCostBasisFlip.toFixed(2));
		
		// AVRPercentage Calculation
		var TotalCostBasisFlip = $('#TotalCostBasisFlip').val() ? parseFloat($('#TotalCostBasisFlip').val()) : 0;
		var ARVFlip = $('#ARVFlip').val() ? parseFloat($('#ARVFlip').val()) : 0;
		var AVRPercentage = parseFloat((TotalCostBasisFlip / ARVFlip) * 100);
		AVRPercentage = AVRPercentage ? parseFloat(AVRPercentage) : 0;
		$('#AVRPercentage').val(Math.round(AVRPercentage));
	
		// FlipProfit Calculation
		var ResalePrice = $('#ResalePrice').val() ? parseFloat($('#ResalePrice').val()) : 0;
		var TotalCostBasisFlip = $('#TotalCostBasisFlip').val() ? parseFloat($('#TotalCostBasisFlip').val()) : 0;
		var CostOfSale = $('#CostOfSale').val() ? parseFloat($('#CostOfSale').val()/100) : 0;
		var LenderSplit = $('#LenderSplit').val() ? parseFloat($('#LenderSplit').val()/100) : 0;
		var FlipProfit = parseFloat((ResalePrice - TotalCostBasisFlip - CostOfSale * ResalePrice) * (1 - LenderSplit * ($('#FinancingUsed').val()=="Financing" ? 1 : 0) * ($('#SplitWithLender').val()=="Yes" ? 1 : 0)));
		$('#FlipProfit').val(FlipProfit.toFixed(2));
		
		// FlipROI Calculation
		if ( $('#CashRequiredFlip').val() == 0 ) {
			$('#FlipROI').val('Infinite');
		} else {
			var FlipProfit = $('#FlipProfit').val() ? parseFloat($('#FlipProfit').val()) : 0;
			var CashRequiredFlip = $('#CashRequiredFlip').val() ? parseFloat($('#CashRequiredFlip').val()) : 0;
			var FlipROI = parseFloat((FlipProfit / CashRequiredFlip) * 100);
			$('#FlipROI').val(FlipROI.toFixed(2));
		}
		
		// FlipROIAnnualized Calculation
		if ( $('#FlipROI').val() == 'Infinite' ) {
			$('#FlipROIAnnualized').val('Infinite');
		} else {
			var FlipProfit = $('#FlipProfit').val() ? parseFloat($('#FlipProfit').val()) : 0;
			var CashRequiredFlip = $('#CashRequiredFlip').val() ? parseFloat($('#CashRequiredFlip').val()) : 0;
			var FlipROI = parseFloat((FlipProfit / CashRequiredFlip) * 100);
			var MonthsFlipRehab = $('#MonthsFlipRehab').val() ? parseFloat($('#MonthsFlipRehab').val()) : 0;
			var MonthsToSell = $('#MonthsToSell').val() ? parseFloat($('#MonthsToSell').val()) : 0;
			var FlipROIAnnualized = parseFloat((FlipROI / (MonthsFlipRehab + MonthsToSell) * 12));
			$('#FlipROIAnnualized').val(FlipROIAnnualized.toFixed(2));
		}
		
		// FlipRehabBudgetMethod option change
		if ( $('#FlipRehabBudgetMethod').val() == 'Quick Lump Sum' ) {
			$('#LumpSumFlipBudgetRow').show();
			$('#DetailedInputFlipBudgetRow').hide();
			
			var LumpSumFlipBudget = $('#LumpSumFlipBudget').val() ? parseFloat($('#LumpSumFlipBudget').val()) : 0;
			$('#ActualFlipBudget').val(LumpSumFlipBudget.toFixed(2));
			
		} else if ( $('#FlipRehabBudgetMethod').val() == 'Detailed Input' ) {
			$('#DetailedInputFlipBudgetRow').show();
			$('#LumpSumFlipBudgetRow').hide();
			
			var DetailedFlipBudget = $('#DetailedFlipBudget').val() ? parseFloat($('#DetailedFlipBudget').val()) : 0;
			$('#ActualFlipBudget').val(DetailedFlipBudget.toFixed(2));
		}
		// End Flip Analysis Column
		
		
	
		
		// Start Refi and Hold Analysis Column
		// TotalCapitalNeededRent Calculation 
		var PurchasePrice = $('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0;
		var ActualRefiBudget = $('#ActualRefiBudget').val() ? parseFloat($('#ActualRefiBudget').val()) : 0;
		var ClosingCosts = $('#ClosingCosts').val() ? parseFloat($('#ClosingCosts').val()) : 0;
		var RefiHoldingCosts = $('#RefiHoldingCosts').val() ? parseInt($('#RefiHoldingCosts').val()) : 0;
		var TotalCapitalNeededRent = parseFloat(PurchasePrice + ActualRefiBudget + ClosingCosts + RefiHoldingCosts);
		$('#TotalCapitalNeededRent').val(TotalCapitalNeededRent.toFixed(2));
		
		
		// MaxFinancedRefi Calculation 
		if ( $('#FinancingUsed').val() == 'All Cash' ) {
			$('#MaxFinancedRefi').val('');
		} else {
			var FinancingCap = $('#FinancingCap').val() ? parseFloat($('#FinancingCap').val()) : 0;
			if ( $('#CapSelection').val() == 'ARV' ) {
				var ARVRent = $('#ARVRent').val() ? parseFloat($('#ARVRent').val()) : 0;
				var MaxFinancedRefi = parseFloat(ARVRent * (FinancingCap / 100));
				$('#MaxFinancedRefi').val(MaxFinancedRefi.toFixed(2));
			} else {
				var PurchasePrice = $('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0;
				var ActualRefiBudget = $('#ActualRefiBudget').val() ? parseFloat($('#ActualRefiBudget').val()) : 0;
				var MaxFinancedRefi = parseFloat((PurchasePrice + ActualRefiBudget) * (FinancingCap / 100));
				$('#MaxFinancedRefi').val(MaxFinancedRefi.toFixed(2));
			}
		}
		
		
		// ActualFinancedRent Calculation
		if ( $('#FinancingUsed').val() == 'All Cash' ) {
			$('#ActualFinancedRent').val(0);
		} else {
			var MaxFinancedRefi = $('#MaxFinancedRefi').val() ? parseFloat($('#MaxFinancedRefi').val()) : 0;
			var TotalCapitalNeededRent = $('#TotalCapitalNeededRent').val() ? parseFloat($('#TotalCapitalNeededRent').val()) : 0;
			var ClosingCosts = $('#ClosingCosts').val() ? parseFloat($('#ClosingCosts').val()) : 0;
			var RefiHoldingCosts = $('#RefiHoldingCosts').val() ? parseFloat($('#RefiHoldingCosts').val()) : 0;
			var ActualFinancedFlip = parseFloat(Math.min(MaxFinancedRefi, TotalCapitalNeededRent - (ClosingCosts + RefiHoldingCosts)));
			$('#ActualFinancedRent').val(ActualFinancedFlip.toFixed(2));
		}
		
		
		// PointInterestRolledUpRent Calculation  (Calc Sheet) 
		var Row118Total = 0, Row121Total = 0, Row124Total = 0, Row126Total = 0, Row131Total = 0, Row134Total = 0;
		var RefiClosingCostsFinanced = 0, RefiHoldingCostsFinanced = 0, RefiPoints = 0, RefiOtherFinancingCosts = 0, RentLoanInterest = 0, RentRehabInterest = 0;
		
		var MonthTotal = new Array();
		var MonthMatchArray = new Array();
		var RefiBudgetColumn = new Array();
		$("form[name='RefiBudgetForm'] .RefiBudgetColumn").each(function(index, value) {
			RefiBudgetColumn[index] = parseFloat($(this).val());
		});
		var MonthPaidColumn = new Array();
		$("form[name='RefiBudgetForm'] .RefiMonthPaidColumn").each(function(index, value) {
			MonthPaidColumn[index] = (RefiBudgetColumn[index] ? parseInt($(this).val()) : 0 );
		});		
		
		var MonthsRefiRehab = $('#MonthsRefiRehab').val() ? parseInt($('#MonthsRefiRehab').val()) : 0;
		var MonthsToRefi = $('#MonthsToRefi').val() ? parseInt($('#MonthsToRefi').val()) : 0;
		var RehabDiscountPoints = $('#RehabDiscountPoints').val() ? parseFloat($('#RehabDiscountPoints').val()/100) : 0;
		var ActualFinancedRent = $('#ActualFinancedRent').val() ? parseFloat($('#ActualFinancedRent').val()) : 0;
		var OtherPoints = $('#OtherPoints').val() ? parseFloat($('#OtherPoints').val()/100) : 0;
		var RefiHoldingCosts = $('#RefiHoldingCosts').val() ? parseFloat($('#RefiHoldingCosts').val()) : 0;
		var RehabInterestRate = $('#RehabInterestRate').val() ? parseFloat($('#RehabInterestRate').val()/100) : 0;
		var LumpSumRefiBudget = $('#LumpSumRefiBudget').val() ? parseFloat($('#LumpSumRefiBudget').val()) : 0;
		
		var DetailedRefiBudget = $('#DetailedRefiBudget').val() ? parseFloat($('#DetailedRefiBudget').val()) : 0;
		
		var B2 = ($('#FinancingUsed').val()=="Financing" ? "Yes" : "No");
		var B3 = $('#CapSelection').val();
		var B4 = $('#FinancingCap').val() ? parseFloat($('#FinancingCap').val()/100) : 0;
		
		var F2 = $('#ARVFlip').val() ? parseFloat($('#ARVFlip').val()) : 0;
		var F3 = ($('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0) + ($('#ActualFlipBudget').val() ? parseFloat($('#ActualFlipBudget').val()) : 0);
		var F4 = ($('#CapSelection').val()=="ARV" ? B4 * F2 : B4 * F3);
		
		var I2 = $('#ARVRent').val() ? parseFloat($('#ARVRent').val()) : 0;
		var I3 = ($('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0) + ($('#ActualRefiBudget').val() ? parseFloat($('#ActualRefiBudget').val()) : 0);
		var I4 = ($('#CapSelection').val()=="ARV" ? B4 * I2 : B4 * I3);
		
		var B112 = $('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0;
		var B113 = (B2=="Yes" ? (B3=="Cost" ? B4*B112 : Math.min(B112, I4)) : 0);
		var PurchaseFinancedRefi = B113;
		var B114 = B112-B113;
		var B115 = $('#ClosingCosts').val() ? parseFloat($('#ClosingCosts').val()) : 0;
		
		var B109 = new Array(); var B110 = new Array(); var B111 = new Array(); var B116 = new Array(); var B117 = new Array(); var B118 = new Array(); 
		var B119 = new Array(); var B120 = new Array(); var B121 = new Array(); var B122 = new Array(); var B123 = new Array(); var B124 = new Array(); 
		var B125 = new Array(); var B126 = new Array(); var B127 = new Array(); var B128 = new Array(); var B129 = new Array(); var B130 = new Array(); 
		var B131 = new Array(); var B132 = new Array(); var B133 = new Array(); var B134 = new Array(); var B136 = new Array(); var B137 = new Array();
		
		for ( var i=0; i<=24; i++ ) {
			if ( i == 0 ) {
				B109[i] = 1; <!-- B109 -->
				B110[i] = (i>(MonthsRefiRehab+MonthsToRefi) ? 0 : 1); <!-- B110 -->
				B111[i] = ($('#FinancingUsed').val()=="Financing" ? 1 : 0); <!-- B111 -->
				B116[i] = parseFloat($('#PointsClosingUpfront').val()=="Paid Upfront" ? (i==0 ? (RehabDiscountPoints)*ActualFinancedRent : 0) : (i==(MonthsRefiRehab+MonthsToRefi) ? (RehabDiscountPoints)*ActualFinancedRent : 0)); <!-- B116 -->
				B117[i] = parseFloat($('#PointsClosingUpfront').val()=="Paid Upfront" ? (i==0 ? (OtherPoints)*ActualFinancedRent : 0) : (i==(MonthsRefiRehab+MonthsToRefi) ? (OtherPoints)*ActualFinancedRent : 0)); <!-- B117 -->
				B118[i] = 0; <!-- B118 -->
				Row118Total = parseFloat(Row118Total + B118[i]);
				B119[i] = parseFloat(PurchaseFinanced + (B115 + B118[i]) * ($('#FinanceClosingHolding').val()=="Yes" ? 1 : 0)); <!-- B119 -->
				B120[i] = 0; <!-- B120 -->
				B121[i] = 0; <!-- B121 -->
				B122[i] = 0; <!-- B122 -->
				B123[i] = 0; <!-- B123 -->
				B124[i] = 0; <!-- B124 -->
				MonthTotal[i] = ($('#RefiRehabDrawSelection').val()=="Fund Rehab at Closing" ? ($('#RefiRehabBudgetMethod').val()=="Quick Lump Sum" ? parseFloat(LumpSumRefiBudget) : parseFloat(DetailedRefiBudget)) : 0);
				B125[i] = MonthTotal[i]; <!-- B125 -->
				B126[i] = parseFloat((B3=="Cost" ? MonthTotal[i] * B4 : Math.max(0, Math.min(I4, MonthTotal[i]))) * B111[i]); <!-- B126 -->
				Row126Total = parseFloat(Row126Total + B126[i]);
				B127[i] = parseFloat(B125[i] - B126[i]); <!-- B127 -->
				B128[i] = parseFloat((B125[i]==0 ? 0 : B125[i]) * B110[i]); <!-- B128 -->
				B129[i] = B126[i]; <!-- B129 -->
				B130[i] = 0; <!-- B130 -->
				B131[i] = 0; <!-- B131 -->
				B132[i] = 0; <!-- B132 -->
				B133[i] = 0; <!-- B133 -->
				B134[i] = 0; <!-- B134 -->
				B136[i] = parseFloat(PurchaseFinancedRefi+(Row126Total)+($('#PointsClosingUpfront').val()=="Paid Upfront" ? 0 : (B116[i]+B117[i]))); <!-- B136 -->
				B137[i] = PurchaseFinancedRefi+ClosingCosts*($('#FinancingUsed').val()=="All Cash" ? 0 : ($('#FinanceClosingHolding').val()=="Yes" ? 1 : 0))+(Row126Total)+($('#PointsClosingUpfront').val()=="Paid Upfront" ? 0 : (B116[i]+B117[i])); <!-- B137 -->
			} else {
				B109[i] = (i>MonthsRefiRehab ? 0 : 1); <!-- B109 -->
				B110[i] = (i>(MonthsRefiRehab+MonthsToRefi) ? 0 : 1); <!-- B110 -->
				B111[i] = ($('#FinancingUsed').val()=="Financing" ? 1 : 0); <!-- B111 -->
				B116[i] = parseFloat($('#PointsClosingUpfront').val()=="Paid Upfront" ? (i==0 ? (RehabDiscountPoints)*ActualFinancedRent : 0) : (i==(MonthsRefiRehab+MonthsToRefi) ? (RehabDiscountPoints)*ActualFinancedRent : 0)); <!-- B116 -->
				B117[i] = parseFloat($('#PointsClosingUpfront').val()=="Paid Upfront" ? (i==0 ? (OtherPoints)*ActualFinancedRent : 0) : (i==(MonthsRefiRehab+MonthsToRefi) ? (OtherPoints)*ActualFinancedRent : 0)); <!-- B117 -->	
				B118[i] = parseFloat(RefiHoldingCosts/(MonthsRefiRehab+MonthsToRefi)*B110[i]); <!-- B118 -->
				Row118Total = parseFloat(Row118Total + B118[i]);
				B119[i] = parseFloat((B119[i-1] + B118[i] * ($('#FinanceClosingHolding').val()=="Yes" ? 1 : 0))*B110[i]); <!-- B119 -->
				B120[i] = parseFloat(RehabInterestRate*B119[i-1] / 12 * (i>(MonthsRefiRehab+MonthsToRefi) ? 0 : 1)); <!-- B120 -->
				B121[i] = parseFloat($('#InterestPaymentsMade').val()=="Yes" ? B120[i] : 0); <!-- B121 -->
				B122[i] = parseFloat(B120[i] - B121[i]); <!-- B122 -->
				if ( i == 1 ) {
					B123[i] = parseFloat(B122[i] + B123[i-1]); <!-- B123 -->
				} else {
					B123[i] = parseFloat((B122[i]+B123[i-1]+B123[i-1]*(RehabInterestRate/12))*B110[i]); <!-- B123 -->
				}
				B124[i] = parseFloat(B123[i] * (i == (MonthsRefiRehab + MonthsToRefi) ? 1 : 0)); <!-- B124 -->
				
				var ColU = (i<=MonthsRefiRehab ? 1 : 0);
				var MonthMatch = 0;
				for ( var j = 0; j < MonthPaidColumn.length; j++ ) {
					if ( MonthPaidColumn[j] == i ) {
						MonthMatch += parseFloat(RefiBudgetColumn[j]);
					}
				}
				MonthMatchArray[i] = parseFloat(MonthMatch);
				MonthTotal[i] = ($('#RefiRehabDrawSelection').val()=="Fund Rehab at Closing" ? 0 : ($('#RefiRehabBudgetMethod').val()=="Detailed Input" ? parseFloat(MonthMatchArray[i] * ColU) : parseFloat(LumpSumFlipBudget / MonthsRefiRehab * ColU)));
				B125[i] = MonthTotal[i]; <!-- B125 -->
				B126[i] = parseFloat((B3=="Cost" ? MonthTotal[i] * B4 : Math.max(0, Math.min(I4-(B137[i-1]), B125[i]))) * B111[i]); <!-- B126 -->
				Row126Total = parseFloat(Row126Total + B126[i]);
				B127[i] = parseFloat(B125[i] - B126[i]); <!-- B127 -->
				B128[i] = parseFloat((B125[i]==0 ? B128[i-1] : B125[i] + B128[i-1]) * B110[i]); <!-- B128 -->
				B129[i] = parseFloat((B126[i]==0 ? B129[i-1] : B126[i] + B129[i-1]) * B110[i]); <!-- B129 -->
				B130[i] = parseFloat(B129[i-1] * RehabInterestRate/12 * B110[i] * B111[i]); <!-- B130 -->
				B131[i] = parseFloat($('#InterestPaymentsMade').val()=="Yes" ? B130[i] : 0); <!-- B131 -->
				B132[i] = parseFloat(B130[i] - B131[i]); <!-- B132 -->
				if ( i == 1 ) {
					B133[i] = parseFloat(B132[i] + B133[i-1]); <!-- B133 -->
				} else {
					B133[i] = parseFloat((B132[i] + B133[i-1] + B133[i-1] * (RehabInterestRate/12)) * B110[i]); <!-- B133 -->
				}
				B134[i] = parseFloat(B133[i] * (i==(MonthsRefiRehab+MonthsToRefi) ? 1 : 0)); <!-- B134 -->
				B136[i] = parseFloat((PurchaseFinancedRefi+B129[i]+($('#PointsClosingUpfront').val()=="Paid Upfront" ? 0 : (B116[i]+B117[i])))*B110[i]); <!-- B136 -->
				B137[i] = (PurchaseFinancedRefi+(ClosingCosts+(Row118Total))*($('#FinancingUsed').val()=="All Cash" ? 0 : ($('#FinanceClosingHolding').val()=="Yes" ? 1 : 0))+B129[i]+($('#PointsClosingUpfront').val()=="Paid Upfront" ? 0 : (B116[i]+B117[i]))+B123[i]+B133[i])*B110[i];
			}			
			Row121Total = parseFloat(Row121Total + B121[i]);
			Row124Total = parseFloat(Row124Total + B124[i]);	
			
			Row131Total = parseFloat(Row131Total + B131[i]);
			Row134Total = parseFloat(Row134Total + B134[i]);
			
			RefiHoldingCostsFinanced  = parseFloat(RefiHoldingCostsFinanced + B118[i]);
			RefiPoints = parseFloat(RefiPoints + B116[i]);
			RefiOtherFinancingCosts = parseFloat(RefiOtherFinancingCosts + B117[i]);
		}		
		RefiClosingCostsFinanced = parseFloat($('#FinanceClosingHolding').val() == "Yes" ? ClosingCosts : 0);
		RentLoanInterest = parseFloat(Row121Total + Row124Total);
		RentRehabInterest = parseFloat(Row131Total + Row134Total);
		RefiHoldingCostsFinanced = ($('#FinanceClosingHolding').val()=="Yes" ? RefiHoldingCostsFinanced : 0);
		
		var PointsInterestRolledupRent = parseFloat($('#FinancingUsed').val() == "All Cash" ? 0 : RefiClosingCostsFinanced + RefiHoldingCostsFinanced + ($('#PointsClosingUpfront').val()=="Paid Upfront" ? 0 : (RefiPoints + RefiOtherFinancingCosts)) + ($('#InterestPaymentsMade').val()=="Yes" ? 0 : (RentLoanInterest + RentRehabInterest)));
		$('#PointsInterestRolledupRent').val(PointsInterestRolledupRent.toFixed(2));
		
		
		// TotalLoanRent Calculation
		var PointsInterestRolledupRent = $('#PointsInterestRolledupRent').val() ? parseFloat($('#PointsInterestRolledupRent').val()) : 0;
		var ActualFinancedRent = $('#ActualFinancedRent').val() ? parseFloat($('#ActualFinancedRent').val()) : 0;
		var TotalLoanRent = parseFloat(PointsInterestRolledupRent + ActualFinancedRent);
		$('#TotalLoanRent').val(TotalLoanRent.toFixed(2));
		
		
		// CashRequiredRent Calculation
		var TotalCapitalNeededRent = $('#TotalCapitalNeededRent').val() ? parseFloat($('#TotalCapitalNeededRent').val()) : 0;
		var FinancingCostRent = parseFloat(RefiPoints + RefiOtherFinancingCosts + RentLoanInterest + RentRehabInterest);
		var ActualFinancedRent = $('#ActualFinancedRent').val() ? parseFloat($('#ActualFinancedRent').val()) : 0;
		var PointsInterestRolledupRent = $('#PointsInterestRolledupRent').val() ? parseFloat($('#PointsInterestRolledupRent').val()) : 0;
		var CashRequiredRent = parseFloat(Math.max(0, (TotalCapitalNeededRent + FinancingCostRent - ActualFinancedRent - PointsInterestRolledupRent)));
		$('#CashRequiredRent').val(CashRequiredRent.toFixed(2));

		
		// TotalCostBasisRent Calculation
		var PurchasePrice = $('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0;
		var ClosingCosts = $('#ClosingCosts').val() ? parseFloat($('#ClosingCosts').val()) : 0;
		var RefiHoldingCosts = $('#RefiHoldingCosts').val() ? parseFloat($('#RefiHoldingCosts').val()) : 0;
		var ActualRefiBudget = $('#ActualRefiBudget').val() ? parseFloat($('#ActualRefiBudget').val()) : 0;
		var FinancingCostRent = parseFloat(RefiPoints + RefiOtherFinancingCosts + RentLoanInterest + RentRehabInterest);
		var TotalCostBasisRent = parseFloat(PurchasePrice + ClosingCosts + RefiHoldingCosts + ActualRefiBudget + FinancingCostRent);
		$('#TotalCostBasisRent').val(TotalCostBasisRent.toFixed(2));

		
		// NOIMonthly Calculation
		var OperatingIncome = $('#OperatingIncome').val() ? parseFloat($('#OperatingIncome').val()) : 0;
		var OperatingExpenses = $('#OperatingExpenses').val() ? parseFloat($('#OperatingExpenses').val()) : 0;
		var NOIMonthly = parseFloat(OperatingIncome - OperatingExpenses);
		$('#NOIMonthly').val(NOIMonthly.toFixed(2));
		
		
		// NewPayment Calculation  =IF(Amortization="Interest Only",-(K23*K16/12),PMT(K16/12,Amortization*12,K23,0))
		var RefiAmount = $('#RefiAmount').val() ? parseFloat($('#RefiAmount').val()) : 0;
		var PermanentRate = $('#PermanentRate').val() ? parseFloat($('#PermanentRate').val()/100) : 0;
		var Amortization = $('#Amortization').val() ? parseFloat($('#Amortization').val()) : 0;
		var NewPayment = parseFloat($('#Amortization').val() == 'Interest Only' ? -(RefiAmount * PermanentRate / 12) : PMT(PermanentRate / 12, Amortization * 12, RefiAmount, 0, 0));
		$('#NewPayment').val(NewPayment.toFixed(2));
		
		
		// RefiAmount Calculation
		var ARVRent = $('#ARVRent').val() ? parseFloat($('#ARVRent').val()) : 0;
		var RefiPercent = $('#RefiPercent').val() ? parseFloat($('#RefiPercent').val()) : 0;
		var RefiAmount = parseFloat(ARVRent * (RefiPercent / 100));
		$('#RefiAmount').val(RefiAmount.toFixed(2));
		
		
		// CashOutRefi Calculation
		var RefiAmount = $('#RefiAmount').val() ? parseFloat($('#RefiAmount').val()) : 0;
		var TotalLoanRent = $('#TotalLoanRent').val() ? parseFloat($('#TotalLoanRent').val()) : 0;
		var RefiDiscountPoints = $('#RefiDiscountPoints').val() ? parseFloat($('#RefiDiscountPoints').val()) : 0;
		var CashOutRefi = parseFloat(RefiAmount - (TotalLoanRent + (RefiDiscountPoints / 100) * RefiAmount));
		$('#CashOutRefi').val(CashOutRefi.toFixed(2));
		
		
		// RefiProfit Calculation
		var CashOutRefi = $('#CashOutRefi').val() ? parseFloat($('#CashOutRefi').val()) : 0;
		var CashRequiredRent = $('#CashRequiredRent').val() ? parseFloat($('#CashRequiredRent').val()) : 0;
		var LenderSplit = $('#LenderSplit').val() ? parseFloat($('#LenderSplit').val()) : 0;
		var FinancingUsed = $('#FinancingUsed').val();
		var SplitWithLender = $('#SplitWithLender').val();
		var RefiProfit = parseFloat(Math.max(0, (CashOutRefi-CashRequiredRent)) * (1 - (LenderSplit/100) * (FinancingUsed == 'Financing' ? 1 : 0) * (SplitWithLender=='Yes' ? 1 : 0)));
		$('#RefiProfit').val(RefiProfit.toFixed(2));
		
		
		var RefiProfit = $('#RefiProfit').val() ? parseFloat($('#RefiProfit').val()) : 0;
		var CashRequiredRent = $('#CashRequiredRent').val() ? parseFloat($('#CashRequiredRent').val()) : 0;
		var MonthsRefiRehab = $('#MonthsRefiRehab').val() ? parseFloat($('#MonthsRefiRehab').val()) : 0;
		var MonthsToRefi = $('#MonthsToRefi').val() ? parseFloat($('#MonthsToRefi').val()) : 0;
		var RefiROI = $('#CashRequiredRent').val() == 0 ? 'N/A' : parseFloat(((RefiProfit / CashRequiredRent) / (MonthsRefiRehab + MonthsToRefi) * 12) * 100).toFixed(2);
		$('#RefiROI').val(RefiROI);
		
		
		// CashTiedUP Calculation
		var CashRequiredRent = $('#CashRequiredRent').val() ? parseFloat($('#CashRequiredRent').val()) : 0;
		var CashOutRefi = $('#CashOutRefi').val() ? parseFloat($('#CashOutRefi').val()) : 0;
		var CashTiedUP = parseFloat(Math.max(0, CashRequiredRent - CashOutRefi));
		$('#CashTiedUP').val(CashTiedUP.toFixed(2));
		
		
		// EquityLeft Calculation
		var RefiPercent = $('#RefiPercent').val() ? parseFloat($('#RefiPercent').val()/100) : 0;
		var ARVRent = $('#ARVRent').val() ? parseFloat($('#ARVRent').val()) : 0;
		var EquityLeft = parseFloat((1 - RefiPercent) * ARVRent);
		$('#EquityLeft').val(EquityLeft.toFixed(2));
		
		
		// CashflowMonthly Calculation
		var OperatingIncome = $('#OperatingIncome').val() ? parseFloat($('#OperatingIncome').val()) : 0;
		var OperatingExpenses = $('#OperatingExpenses').val() ? parseFloat($('#OperatingExpenses').val()) : 0;
		var NewPayment = $('#NewPayment').val() ? parseFloat($('#NewPayment').val()) : 0;
		var CashflowMonthly = parseFloat(OperatingIncome - OperatingExpenses + NewPayment);
		$('#CashflowMonthly').val(CashflowMonthly.toFixed(2));
		
		
		// RefiROIAnnual Calculation
		var CashTiedUP = $('#CashTiedUP').val() ? parseFloat($('#CashTiedUP').val()) : 0;
		var CashflowMonthly = $('#CashflowMonthly').val() ? parseFloat($('#CashflowMonthly').val()) : 0;
		var RefiROIAnnual = CashTiedUP > 0 ? parseFloat((CashflowMonthly * 12 / CashTiedUP)*100).toFixed(2) : 'Infinite';
		$('#RefiROIAnnual').val(RefiROIAnnual);
		
		
		// DCR Calculation
		var NOIMonthly = $('#NOIMonthly').val() ? parseFloat($('#NOIMonthly').val()) : 0;
		var NewPayment = $('#NewPayment').val() ? parseFloat($('#NewPayment').val()) : 0;
		var DCR = parseFloat(-NOIMonthly / (NewPayment));
		DCR = DCR ? parseFloat(DCR) : 0;
		$('#DCR').val(DCR.toFixed(2));
		
		
		// PaybackPeriod Calculation
		var CashTiedUP = $('#CashTiedUP').val() ? parseFloat($('#CashTiedUP').val()) : 0;
		var CashflowMonthly = $('#CashflowMonthly').val() ? parseFloat($('#CashflowMonthly').val()) : 0;
		var PaybackPeriod = CashTiedUP > 0 ? CashTiedUP / (CashflowMonthly * 12) : 'No Cash Tied Up to Pay Back!';
		PaybackPeriod = PaybackPeriod=='No Cash Tied Up to Pay Back!' ? 'No Cash Tied Up to Pay Back!' : parseFloat(PaybackPeriod).toFixed(2);
		$('#PaybackPeriod').val(PaybackPeriod);
		
		
		// CapRateARV Calculation
		var NOIMonthly = $('#NOIMonthly').val() ? parseFloat($('#NOIMonthly').val()) : 0;
		var ARVRent = $('#ARVRent').val() ? parseFloat($('#ARVRent').val()) : 0;
		var CapRateARV = parseFloat(((NOIMonthly * 12) / ARVRent)*100);
		CapRateARV = CapRateARV ? parseFloat(CapRateARV) : 0;
		$('#CapRateARV').val(CapRateARV.toFixed(2));
		
		
		if ( $('#RefiRehabBudgetMethod').val() == 'Quick Lump Sum' ) {
			$('#LumpSumRefiBudgetRow').show();
			$('#DetailedInputRefiBudgetRow').hide();
		} else if ( $('#RefiRehabBudgetMethod').val() == 'Detailed Input' ) {
			$('#DetailedInputRefiBudgetRow').show();
			$('#LumpSumRefiBudgetRow').hide();
		}
		
		if ( $('#RefiRehabBudgetMethod').val() == 'Detailed Input' ) {
			var DetailedRefiBudget = $('#DetailedRefiBudget').val() ? parseFloat($('#DetailedRefiBudget').val()) : 0;
			$('#ActualRefiBudget').val(DetailedRefiBudget.toFixed(2));
		} else {
			var LumpSumRefiBudget = $('#LumpSumRefiBudget').val() ? parseFloat($('#LumpSumRefiBudget').val()) : 0;
			$('#ActualRefiBudget').val(LumpSumRefiBudget.toFixed(2));
		}
		// End Refi and Hold Analysis Column
    });	
	
});	