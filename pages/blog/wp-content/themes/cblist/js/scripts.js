jQuery(document).ready(function($) {
	"use strict";
	
	$('#responsive-menu').sidr();
	
    $('.left-panel').height($(window).height());
	var menuHeight = ($(window).height() - ($('.left-panel .site-title').height() + $('.left-panel .account-pic').height() + ($('.left-panel .upgrade-btn').length > 0 ? $('.left-panel .upgrade-btn').height() : 0) + 100));
	$('.panel-menu .nano').height(menuHeight);
	
    // Preview profile picture
    $("#picture").change(function(){
        var filename = $(this).val();
        var extension = filename.replace(/^.*\./, '');
        if ( extension == filename ) {
            extension = '';
        } else {
            extension = extension.toLowerCase();
        }
        if ( extension == 'gif' || extension == 'jpg' || extension == 'jpeg' || extension == 'png' || extension == 'bmp' ) {
            readURL(this, '#profile_pic', false);
        }
    });
    // end of function
	
    // Add more portfolio image
    $('.add_more').click(function() {
        $('.portfolio-pic').append('<div class="image-upload"><i title="Remove this image" class="fa fa-close portfolio-close"></i><img class="portfolio-img thumbnail" alt="pic" src="' + base_url + '/assets/images/uploadimg.jpg"><input type="file" title="Select portfolio images" class="form-control file-portfolio" id="portfolio_pic" name="portfolio_pic[]"><hr /></div>');
        // Preview portfolio image after adding extra field
        $(".file-portfolio").change(function(){
            var filename = $(this).val();
            var extension = filename.replace(/^.*\./, '');
            if ( extension == filename ) {
                extension = '';
            } else {
                extension = extension.toLowerCase();
            }
            if ( extension == 'gif' || extension == 'jpg' || extension == 'jpeg' || extension == 'png' || extension == 'bmp' ) {
                readURL(this, '.portfolio-img', true);
            }
        });
        // Remove portfolio image before uploading
        $('.portfolio-close').click(function(e) {
            $(this).parents('.image-upload').remove();
        });
        return false;
    });
    // end of function
	
    // Preview portfolio image for editing portfolio
    $(".file-portfolio").change(function(){
        var old_img = $(this).attr('id'), old_del_img = $('input[name="del_pic"]').val();
        var filename = $(this).val();
        var extension = filename.replace(/^.*\./, '');
        if ( extension == filename ) {
            extension = '';
        } else {
            extension = extension.toLowerCase();
        }
        if ( extension == 'gif' || extension == 'jpg' || extension == 'jpeg' || extension == 'png' || extension == 'bmp' ) {
            readURL(this, '.portfolio-img', true);
            var all_img = $('input[name="all_pic"]').val();
            if ( all_img.indexOf(old_img) != -1 ) {
                all_img = all_img.replace(old_img, '');
                all_img = all_img.replace('||', '|');
            }
            $('input[name="all_pic"]').val(all_img);
            $('input[name="del_pic"]').val(old_img + '|' + old_del_img);
        }
    });
	// end of function
	
    // Remove portfolio image for editing portfolio
    $('.portfolio-close').click(function(e) {
        var all_img = $('input[name="all_pic"]').val(), img = $(this).attr('id'), del_img = $('input[name="del_pic"]').val();
        if ( all_img.indexOf(img) != -1 ) {
            all_img = all_img.replace(img, '');
            all_img = all_img.replace('||', '|');
        }
        $('input[name="all_pic"]').val(all_img);
        $('input[name="del_pic"]').val(img + '|' + del_img);
        $(this).parents('.image-upload').remove();
    });
	// end of function

	// Preview proposal slider images
	if ( $( '#public_slider_pic' ).length ) {
		if ( window.File && window.FileList && window.FileReader ) {
			var filesInput = document.getElementById('public_slider_pic');
			filesInput.addEventListener("change", function(event){
				var files = event.target.files; //FileList object
				var output = document.getElementById("result");
				for (var i = 0; i< files.length; i++) {
					var file = files[i];
					//Only pics
					if(!file.type.match('image')) continue;
					
					var picReader = new FileReader();
					picReader.addEventListener("load",function(event){
						var picFile = event.target;
						var div = document.createElement("div");
						div.className = 'col-sm-2 image-upload';
						div.innerHTML = '<img alt="pic" class="slider-proposal-img thumbnail" src="' + picFile.result + '" width="100%" height="83" />';
						output.insertBefore(div, null);
					});
					
					//Read the image
					picReader.readAsDataURL(file);
				}
			});
		} else {
			alert('Your browser does not support File API');
		}
	}
    // end of function	
	
    // Remove proposal slider image for proposal editing
    $('.proposal-slider-close').click(function(e) {
        var all_img = $('input[name="all_slider_pic"]').val(), img = $(this).attr('id'), del_img = $('input[name="del_slider_pic"]').val();
        if ( all_img.indexOf(img) != -1 ) {
            all_img = all_img.replace(img, '');
            all_img = all_img.replace('||', '|');
        }
        $('input[name="all_slider_pic"]').val(all_img);
        $('input[name="del_slider_pic"]').val(img + '|' + del_img);
        $(this).parent('.image-upload').remove();
    });
	// end of function
	
	// Preview proposal screenshot images
	if ( $( '#public_pic' ).length ) {
		if ( window.File && window.FileList && window.FileReader ) {
			var filesInput = document.getElementById('public_pic');
			filesInput.addEventListener("change", function(event){
				var files = event.target.files; //FileList object
				var output = document.getElementById("result_pic");
				for (var i = 0; i< files.length; i++) {
					var file = files[i];
					//Only pics
					if(!file.type.match('image')) continue;
					
					var picReader = new FileReader();
					picReader.addEventListener("load",function(event){
						var picFile = event.target;
						var div = document.createElement("div");
						div.className = 'col-sm-2 image-upload';
						div.innerHTML = '<img alt="pic" class="proposal-img thumbnail" src="' + picFile.result + '" width="100%" height="100%" />';
						output.insertBefore(div, null);
					});
					
					//Read the image
					picReader.readAsDataURL(file);
				}
			});
		} else {
			alert('Your browser does not support File API');
		}
	}
    // end of function
	
	// Remove proposal slider image for editing proposal public
    $('.proposal-close').click(function(e) {
        var all_img = $('input[name="all_public_pic"]').val(), img = $(this).attr('id'), del_img = $('input[name="del_public_pic"]').val();
        if ( all_img.indexOf(img) != -1 ) {
            all_img = all_img.replace(img, '');
            all_img = all_img.replace('||', '|');
        }
        $('input[name="all_public_pic"]').val(all_img);
        $('input[name="del_public_pic"]').val(img + '|' + del_img);
        $(this).parent('.image-upload').remove();
    });
	// end of function
	
    // Tooltips and alertbox
    $('.add-proposal').popover();
    $('.pop-over').popover();
    $('[data-toggle="tooltip"]').tooltip();
    $(".alert").alert();
    $('#dob').datepicker({
		endDate: '+0d',
        autoclose : true,
    });
	$('.DateSoldField').datepicker({
		endDate: '+0d',
        autoclose : true,
    });
	$('.DateClosing').datepicker({
        autoclose : true,
    });
	$(".nano").nanoScroller();
    $('.datatables').DataTable();
	// end of function
    
    // Edit Profile/Account Toggle
    $('#edit_profile').click(function() {
        $('.edit-field').show();
        $('.profile-data').hide();
        $(this).addClass('hide');
        $('#cancel_profile').removeClass('hide');
        return false;
    });
	// end of function
	
    // Cancel Profile/Account Toggle
    $('#cancel_profile').click(function() {
        $('.edit-field').hide();
        $('.profile-data').show();
        $(this).addClass('hide');
        $('#edit_profile').removeClass('hide');
        return false;
    });
    // end of function
	
    // Fancybox for Portfolio Gallery
    $('.fancybox').fancybox();
    // end of function
	
	// Character Limit for textarea
    $("#sales_pitch").characterCounter({
        limit: 500,
        counterSelector: '#pitch_counter',
    });
	$("#about_you").characterCounter({
        limit: 500,
        counterSelector: '#about_counter',
    });
	$("#about_investment").characterCounter({
        limit: 500,
        counterSelector: '#investment_counter',
    });
    // end of function
	
	// Add feature in public proposal
    $('.add-feature').click(function(e) {
		if ( parseInt(document.getElementById('last_feature').value) >= 1 ) {
			$('.delete-feature').css('display', 'inline');
		}
		var last_feature_value = parseInt(document.getElementById('last_feature').value);
		var last_feature_name = parseInt(document.getElementById('last_feature').value) - parseInt(1);
	    $('.added_features').append('<input type="text" value="" name="added_feature[' + last_feature_name + ']" class="form-control" id="added_feature_' + last_feature_value + '" />');
		var last_feature_value_incremented = parseInt(document.getElementById('last_feature').value) + parseInt(1);
		$('#last_feature').val(last_feature_value_incremented);
	});   
	// end of function
	
	// Delete feature from public proposal
	$('.delete-feature').click(function(e) {
		var last_feature_value = parseInt(document.getElementById('last_feature').value) - parseInt(1);
		document.getElementById('added_feature_' + last_feature_value).remove();
		$('#last_feature').val(last_feature_value);
		if ( last_feature_value == 1 ) {
			$('.delete-feature').css('display','none');
		}
    });
	// end of function	
	
	// Add feature in public proposal
    $('#add_new_add').click(function(e) {
		if ( parseInt(document.getElementById("last_address").value) >= 1 ) {
			$('#remove_add').css('display', 'inline');
		}
		var last_value = parseInt(document.getElementById("last_address").value);
	    $('#comparable_sales_table tbody').append('<tr id="added_new_add_' + last_value + '"><td><input type="text" class="form-control" id="address_' + last_value + '" value="" name="address[' + last_value + ']" /></td><td><input type="text" class="form-control" id="beds_' + last_value + '" value="" name="beds[' + last_value + ']"></td><td><input type="text" class="form-control" id="baths_' + last_value + '" value="" name="baths[' + last_value + ']"></td><td><input type="text" class="form-control" id="size_' + last_value + '" value="" name="size[' + last_value + ']"></td><td><input type="text" class="form-control" id="date_sold_' + last_value + '" value="" name="date_sold[' + last_value + ']"></td><td><input type="text" class="form-control" id="sales_price_' + last_value + '" value="" name="sales_price[' + last_value + ']"></td><td><input type="text" class="form-control" id="notes_' + last_value + '" value="" name="notes[' + last_value + ']"></td></tr>');
		var last_value_incremented = parseInt(document.getElementById("last_address").value) + parseInt(1);
		$('#last_address').val(last_value_incremented);
	});
	// end of function
	
	// Delete feature from public proposal
	$('#remove_add').click(function(e) {
		var last_value = parseInt(document.getElementById("last_address").value) - parseInt(1);
		document.getElementById('added_new_add_' + last_value).remove();
		$('#last_address').val(last_value);
		if ( last_value == 1 ) {
			$('#remove_add').css('display','none');
		}
    });
	// end of function	
	
	
	// Bootstrap modal resize
	if ($(window).height() >= 320){
		$(window).resize(adjustModalMaxHeightAndPosition).trigger("resize");
	}
	// end of function
	
	// .CSV file download event
	$( ".csv-downloader" ).click(function() {
		var elem = $(this);
		var url = $(elem).attr('href');
		$(elem).text('Downloading');
		$.ajax({
			type:'POST',
			url:base_url + 'pages/update-downloaded-csv/',
			data:{
				'url' : url
			},
		});
		setTimeout(function() {
			$(elem).text('Downloaded');
		}, 2000);
	});
	// end of function
	
	// Proposal List filter
	$( "#proposal_filter_form" ).submit(function() {
		setCookie('__pfsrch', 1, 1);
		$('.filter-loader').show();
	});
	// end of function
	
	// Proposal Filter show by default
	if ( getCookie('__pfsrch') ) {
		$('#filter_body').show();
	}
	// end of function
	
	// Filter for searching the investor's proposal
	$('#search_filter').click(function(e) {
		if ( $('#filter_body').css('display') == 'block' ) {
			setCookie('__pfsrch', 1, -1);
		}
        $('#filter_body').slideToggle();
		return false;
    });
	// end of function
	
	// Price range slider
	$(".filter-price-range").slider();
	// end of function
	
	// Star rating 
	$('.filter-star-rating').rating({
		defaultCaption: '{rating}',
        starCaptions: {
            0.5: '',
            1: '',
            1.5: '',
            2: '',
            2.5: '',
            3: '',
            3.5: '',
            4: '',
            4.5: '',
            5: ''
        },
        starCaptionClasses: {
            0.5: '',
            1: '',
            1.5: '',
            2: '',
            2.5: '',
            3: '',
            3.5: '',
            4: '',
            4.5: '',
            5: ''
        },
		clearButton: '',
	});
	// end of function
	
	// Yes/No radio button
	$('.btn-toggle').click(function() {
		$(this).find('.btn').toggleClass('active');
		if ($(this).find('.btn-primary').size() > 0) {
			$(this).find('.btn').toggleClass('btn-primary');
		}
		if ($(this).find('.btn-danger').size() > 0) {
			$(this).find('.btn').toggleClass('btn-danger');
		}
		if ($(this).find('.btn-success').size() > 0) {
			$(this).find('.btn').toggleClass('btn-success');
		}
		if ($(this).find('.btn-info').size() > 0) {
			$(this).find('.btn').toggleClass('btn-info');
		}
		$(this).find('.btn').toggleClass('btn-default');
	});
	// end of function
	
	// Flexslider API 
	$('.flexslider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	});
	// end of function
	
	// Add feature in public proposal
    $('#add_new_sales').click(function(e) {
		if ( parseInt($("#last_sales").attr('rel')) >= 1 ) {
			$('#remove_sales').css('display', 'inline');
		}
		var last = parseInt($("#last_sales").attr('rel'));
	    $('#more_sales').append('<div id="added_sales_' + last + '"><hr /><div class="panel-field" id="SalesAddress' + last + 'Row"><div class="row"><label class="col-lg-3">Address</label><div class="col-lg-9"><input type="text" name="SalesAddress[' + last + ']" id="SalesAddress_' + last + '" class="form-control Required" /></div></div></div><div class="panel-field" id="BedsBaths' + last + 'Row"><div class="row"><div class="col-lg-6"><div id="Beds' + last + 'Row"><div class="row"><label class="col-lg-6">Beds</label><div class="col-lg-6"><input type="text" name="Beds[' + last + ']" id="Beds_' + last + '" class="form-control NumberOnly Required" /></div></div></div></div><div class="col-lg-6"><div id="Baths' + last + 'Row"><div class="row"><label class="col-lg-6">Baths</label><div class="col-lg-6"><input type="text" name="Baths[' + last + ']" id="Baths_' + last + '" class="form-control NumberOnly Required" /></div></div></div></div></div></div><div class="panel-field" id="SqFtDateSold' + last + 'Row"><div class="row"><div class="col-lg-6"><div id="SqFt' + last + 'Row"><div class="row"><label class="col-lg-6">Sq Ft</label><div class="col-lg-6"><input type="text" name="SqFt[' + last + ']" id="SqFt_' + last + '" class="form-control NumberOnly Required" /></div></div></div></div><div class="col-lg-6"><div id="DateSold' + last + 'Row"><div class="row"><label class="col-lg-6">Date Sold</label><div class="col-lg-6"><input type="text" name="DateSold[' + last + ']" id="DateSold_' + last + '" class="form-control DateSoldField Required" readonly="readonly" data-provide="datepicker" data-date-format="mm-dd-yyyy" /></div></div></div></div></div></div><div class="panel-field" id="SalesPriceNotes' + last + 'Row"><div class="row"><div class="col-lg-6"><div id="SalesPrice' + last + 'Row"><div class="row"><label class="col-lg-6">Sales Price</label><div class="col-lg-6"><div class="input-group"><div class="input-group-addon">$</div><input type="text" name="SalesPrice[' + last + ']" id="SalesPrice_' + last + '" class="form-control number-field NumberOnly Required" /></div></div></div></div></div><div class="col-lg-6"><div id="Notes' + last + 'Row"><div class="row"><label class="col-lg-6">Notes</label><div class="col-lg-6"><input type="text" name="Notes[' + last + ']" id="Notes_' + last + '" class="form-control Required" /></div></div></div></div></div></div></div>');
		var last_incremented = parseInt($("#last_sales").attr('rel')) + parseInt(1);
		$('#last_sales').attr('rel', last_incremented);
		
		var num = 0;
		$('.number-field').attr('placeholder', num.toFixed(2));
		$('.number-field').change(function(){
			try {
				if ( $(this).val().length == 0 ) {
					return false;
				}
				var avrVal = parseFloat($(this).val().replace("$", ""));
			} catch(err) {
				//
			}
			$(this).val(avrVal.toFixed(2));
		});
		
		$('.DateSoldField').datepicker({
			endDate: '+0d',
			autoclose : true,
		});
	});
	// end of function
	
	// Delete feature from public proposal
	$('#remove_sales').click(function(e) {
		var last = parseInt($("#last_sales").attr('rel')) - parseInt(1);
		document.getElementById('added_sales_' + last).remove();
		$("#last_sales").attr('rel', last);
		if ( last == 1 ) {
			$('#remove_sales').css('display', 'none');
		}
    });
	// end of function
	
	
	// Add image in public proposal
    $('#add_new_image').click(function(e) {
		if ( parseInt($("#last_image").attr('rel')) >= 1 ) {
			$('#remove_image').css('display', 'inline');
		}
		var last = parseInt($("#last_image").attr('rel'));
	    $('#more_image').append('<div id="added_image_' + last + '"><hr /><div class="panel-field" id="Images' + last + 'Row"><div class="row"><label class="col-lg-6">Pictures</label><div class="col-lg-6"><input type="file" name="Images[' + last + ']" id="Images_' + last + '" class="form-control images" /><font size="1">Max Size 5MB with GIF, JPG, PNG, BMP</font></div></div></div></div>');
		var last_incremented = parseInt($("#last_image").attr('rel')) + parseInt(1);
		$('#last_image').attr('rel', last_incremented);
		
		var num = 0;
		$('.number-field').attr('placeholder', num.toFixed(2));
		$('.number-field').change(function(){
			try {
				if ( $(this).val().length == 0 ) {
					return false;
				}
				var avrVal = parseFloat($(this).val().replace("$", ""));
			} catch(err) {
				//
			}
			$(this).val(avrVal.toFixed(2));
		});
	});
	// end of function
	
	// Delete image from public proposal
	$('#remove_image').click(function(e) {
		var last = parseInt($("#last_image").attr('rel')) - parseInt(1);
		document.getElementById('added_image_' + last).remove();
		$("#last_image").attr('rel', last);
		if ( last == 1 ) {
			$('#remove_image').css('display', 'none');
		}
    });
	// end of function
	
	$('.portfolio_img_upload').on('click', function() {
		$(this).next('input[type="file"]').click();
	});
	
	$(".NumberOnly").on('keydown', function (e) {
		if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || (e.keyCode == 65 && ( e.ctrlKey === true || e.metaKey === true ) ) || (e.keyCode >= 35 && e.keyCode <= 40)) {
			return;
		}
		if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
			e.preventDefault();
			show_error_message('Please input numbers only!');
		}
	});
	
	$(".YearRange").on("change", function(e) {
		var val = Math.abs(parseInt($(this).val(), 10) || 1);
		if ( val > 2100 || val < 1900 ) {
			$(this).val('');
			$(this).focus();
			show_error_message('Built Year must be between 1900 - 2100!');
		} else {
			this.value = val;
		}
	});
	
	$(".WebsiteOnly").on("change", function(e) {
		if ( /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test($(this).val()) ) {
			return;
		} else {
			$(this).val('');
			$(this).focus();
			show_error_message('Please enter a valid URL!');
		}
	});
	
	$(".EmailOnly").on("change", function(e) {
		if ( /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/.test($(this).val()) ) {
			return;
		} else {
			$(this).val('');
			$(this).focus();
			show_error_message('Please enter a valid Email!');
		}
	});
});
/*
 * document ready end
 */