	jQuery(document).ready(function($) {
    "use strict";
	
	$('#OperatingIncomeModal .form-control').on('change blur focus', function() {
		var NoOfUnits_1  =  $('#NoOfUnits_1').val()  ? parseFloat($('#NoOfUnits_1').val())  : 0;
		var NoOfUnits_2  =  $('#NoOfUnits_2').val()  ? parseFloat($('#NoOfUnits_2').val())  : 0;
		var NoOfUnits_3  =  $('#NoOfUnits_3').val()  ? parseFloat($('#NoOfUnits_3').val())  : 0;
		var NoOfUnits_4  =  $('#NoOfUnits_4').val()  ? parseFloat($('#NoOfUnits_4').val())  : 0;
		var NoOfUnits_5  =  $('#NoOfUnits_5').val()  ? parseFloat($('#NoOfUnits_5').val())  : 0;
		var NoOfUnits_6  =  $('#NoOfUnits_6').val()  ? parseFloat($('#NoOfUnits_6').val())  : 0;
		var NoOfUnits_7  =  $('#NoOfUnits_7').val()  ? parseFloat($('#NoOfUnits_7').val())  : 0;
		var NoOfUnits_8  =  $('#NoOfUnits_8').val()  ? parseFloat($('#NoOfUnits_8').val())  : 0;
		var NoOfUnits_9  =  $('#NoOfUnits_9').val()  ? parseFloat($('#NoOfUnits_9').val())  : 0;
		var NoOfUnits_10 =  $('#NoOfUnits_10').val() ? parseFloat($('#NoOfUnits_10').val()) : 0;
		var NoOfUnits_11 =  $('#NoOfUnits_11').val() ? parseFloat($('#NoOfUnits_11').val()) : 0;
		var NoOfUnits_12 =  $('#NoOfUnits_12').val() ? parseFloat($('#NoOfUnits_12').val()) : 0;
		
		var SquareFt_1  =  $('#SquareFt_1').val()  ? parseFloat($('#SquareFt_1').val())  : 0;
		var SquareFt_2  =  $('#SquareFt_2').val()  ? parseFloat($('#SquareFt_2').val())  : 0;
		var SquareFt_3  =  $('#SquareFt_3').val()  ? parseFloat($('#SquareFt_3').val())  : 0;
		var SquareFt_4  =  $('#SquareFt_4').val()  ? parseFloat($('#SquareFt_4').val())  : 0;
		var SquareFt_5  =  $('#SquareFt_5').val()  ? parseFloat($('#SquareFt_5').val())  : 0;
		var SquareFt_6  =  $('#SquareFt_6').val()  ? parseFloat($('#SquareFt_6').val())  : 0;
		var SquareFt_7  =  $('#SquareFt_7').val()  ? parseFloat($('#SquareFt_7').val())  : 0;
		var SquareFt_8  =  $('#SquareFt_8').val()  ? parseFloat($('#SquareFt_8').val())  : 0;
		var SquareFt_9  =  $('#SquareFt_9').val()  ? parseFloat($('#SquareFt_9').val())  : 0;
		var SquareFt_10 =  $('#SquareFt_10').val() ? parseFloat($('#SquareFt_10').val()) : 0;
		var SquareFt_11 =  $('#SquareFt_11').val() ? parseFloat($('#SquareFt_11').val()) : 0;
		var SquareFt_12 =  $('#SquareFt_12').val() ? parseFloat($('#SquareFt_12').val()) : 0;
		
		var MonthlyRent_1  =  $('#MonthlyRent_1').val()  ? parseFloat($('#MonthlyRent_1').val())  : 0;
		var MonthlyRent_2  =  $('#MonthlyRent_2').val()  ? parseFloat($('#MonthlyRent_2').val())  : 0;
		var MonthlyRent_3  =  $('#MonthlyRent_3').val()  ? parseFloat($('#MonthlyRent_3').val())  : 0;
		var MonthlyRent_4  =  $('#MonthlyRent_4').val()  ? parseFloat($('#MonthlyRent_4').val())  : 0;
		var MonthlyRent_5  =  $('#MonthlyRent_5').val()  ? parseFloat($('#MonthlyRent_5').val())  : 0;
		var MonthlyRent_6  =  $('#MonthlyRent_6').val()  ? parseFloat($('#MonthlyRent_6').val())  : 0;
		var MonthlyRent_7  =  $('#MonthlyRent_7').val()  ? parseFloat($('#MonthlyRent_7').val())  : 0;
		var MonthlyRent_8  =  $('#MonthlyRent_8').val()  ? parseFloat($('#MonthlyRent_8').val())  : 0;
		var MonthlyRent_9  =  $('#MonthlyRent_9').val()  ? parseFloat($('#MonthlyRent_9').val())  : 0;
		var MonthlyRent_10 =  $('#MonthlyRent_10').val() ? parseFloat($('#MonthlyRent_10').val()) : 0;
		var MonthlyRent_11 =  $('#MonthlyRent_11').val() ? parseFloat($('#MonthlyRent_11').val()) : 0;
		var MonthlyRent_12 =  $('#MonthlyRent_12').val() ? parseFloat($('#MonthlyRent_12').val()) : 0;
		
		var AnnualRent_1 = parseFloat(MonthlyRent_1 * 12 * NoOfUnits_1); $('#AnnualRent_1').val(AnnualRent_1);
		var AnnualRent_2 = parseFloat(MonthlyRent_2 * 12 * NoOfUnits_2); $('#AnnualRent_2').val(AnnualRent_2);
		var AnnualRent_3 = parseFloat(MonthlyRent_3 * 12 * NoOfUnits_3); $('#AnnualRent_3').val(AnnualRent_3);
		var AnnualRent_4 = parseFloat(MonthlyRent_4 * 12 * NoOfUnits_4); $('#AnnualRent_4').val(AnnualRent_4);
		var AnnualRent_5 = parseFloat(MonthlyRent_5 * 12 * NoOfUnits_5); $('#AnnualRent_5').val(AnnualRent_5);
		var AnnualRent_6 = parseFloat(MonthlyRent_6 * 12 * NoOfUnits_6); $('#AnnualRent_6').val(AnnualRent_6);
		var AnnualRent_7 = parseFloat(MonthlyRent_7 * 12 * NoOfUnits_7); $('#AnnualRent_7').val(AnnualRent_7);
		var AnnualRent_8 = parseFloat(MonthlyRent_8 * 12 * NoOfUnits_8); $('#AnnualRent_8').val(AnnualRent_8);
		var AnnualRent_9 = parseFloat(MonthlyRent_9 * 12 * NoOfUnits_9); $('#AnnualRent_9').val(AnnualRent_9);
		var AnnualRent_10 = parseFloat(MonthlyRent_10 * 12 * NoOfUnits_10); $('#AnnualRent_10').val(AnnualRent_10);
		var AnnualRent_11 = parseFloat(MonthlyRent_11 * 12 * NoOfUnits_11); $('#AnnualRent_11').val(AnnualRent_11);
		var AnnualRent_12 = parseFloat(MonthlyRent_12 * 12 * NoOfUnits_12); $('#AnnualRent_12').val(AnnualRent_12);
		
		var NoOfUnit_Total = NoOfUnits_1 + NoOfUnits_2 + NoOfUnits_3 + NoOfUnits_4 + NoOfUnits_5 + NoOfUnits_6 + NoOfUnits_7 + NoOfUnits_8 + NoOfUnits_9 + NoOfUnits_10 + NoOfUnits_11 + NoOfUnits_12;
		NoOfUnit_Total = NoOfUnit_Total ? parseFloat(NoOfUnit_Total) : 0;
		$('#NoOfUnit_Total').val(NoOfUnit_Total);
		
		var SquareFt_Total = (NoOfUnits_1 * SquareFt_1) + (NoOfUnits_2 * SquareFt_2) + (NoOfUnits_3 * SquareFt_3) + (NoOfUnits_4 * SquareFt_4) + (NoOfUnits_5 * SquareFt_5) + (NoOfUnits_6 * SquareFt_6) + (NoOfUnits_7 * SquareFt_7) + (NoOfUnits_8 * SquareFt_8) + (NoOfUnits_9 * SquareFt_9) + (NoOfUnits_10 * SquareFt_10) + (NoOfUnits_11 * SquareFt_11) + (NoOfUnits_12 * SquareFt_12);
		SquareFt_Total = SquareFt_Total ? parseFloat(SquareFt_Total) : 0;
		$('#SquareFt_Total').val(SquareFt_Total.toFixed(0));
		
		var MonthlyRent_Gross = (NoOfUnits_1 * MonthlyRent_1) + (NoOfUnits_2 * MonthlyRent_2) + (NoOfUnits_3 * MonthlyRent_3) + (NoOfUnits_4 * MonthlyRent_4) + (NoOfUnits_5 * MonthlyRent_5) + (NoOfUnits_6 * MonthlyRent_6) + (NoOfUnits_7 * MonthlyRent_7) + (NoOfUnits_8 * MonthlyRent_8) + (NoOfUnits_9 * MonthlyRent_9) + (NoOfUnits_10 * MonthlyRent_10) + (NoOfUnits_11 * MonthlyRent_11) + (NoOfUnits_12 * MonthlyRent_12);
		MonthlyRent_Gross = MonthlyRent_Gross ? parseFloat(MonthlyRent_Gross) : 0;
		$('#MonthlyRent_Gross').val(MonthlyRent_Gross);
		
		var AnnualRent_Gross = MonthlyRent_Gross * 12;
		AnnualRent_Gross = AnnualRent_Gross ? parseFloat(AnnualRent_Gross) : 0;
		$('#AnnualRent_Gross').val(AnnualRent_Gross);
		
		var VacancyLossPercent = $('#VacancyLossPercent').val() ? parseFloat($('#VacancyLossPercent').val()) : 0;
		var VacancyLossMonthly = (MonthlyRent_Gross * VacancyLossPercent) / 100;
		VacancyLossMonthly = VacancyLossMonthly ? parseFloat(VacancyLossMonthly) : 0;
		$('#VacancyLossMonthly').val(VacancyLossMonthly);
		
		var VacancyLossAnnual = VacancyLossMonthly * 12;
		VacancyLossAnnual = VacancyLossAnnual ? parseFloat(VacancyLossAnnual) : 0;
		$('#VacancyLossAnnual').val(VacancyLossAnnual);
		
		var ManagementPercent = $('#ManagementPercent').val() ? parseFloat($('#ManagementPercent').val()) : 0;
		var ManagementMonthly = (MonthlyRent_Gross * ManagementPercent) / 100;
		ManagementMonthly = ManagementMonthly ? parseFloat(ManagementMonthly) : 0;
		$('#ManagementMonthly').val(ManagementMonthly);
		
		var ManagementAnnual = ManagementMonthly * 12;
		ManagementAnnual = ManagementAnnual ? parseFloat(ManagementAnnual) : 0;
		$('#ManagementAnnual').val(ManagementAnnual);
		
		var OtherIncomeMonthly = $('#OtherIncomeMonthly').val() ? parseFloat($('#OtherIncomeMonthly').val()) : 0;
		var OtherIncomeAnnual = OtherIncomeMonthly * 12;
		OtherIncomeAnnual = OtherIncomeAnnual ? parseFloat(OtherIncomeAnnual) : 0;
		$('#OtherIncomeAnnual').val(OtherIncomeAnnual);
		
		var MonthlyGrossOperatingIncome = MonthlyRent_Gross - (VacancyLossMonthly + ManagementMonthly) + OtherIncomeMonthly;
		MonthlyGrossOperatingIncome = MonthlyGrossOperatingIncome ? parseFloat(MonthlyGrossOperatingIncome) : 0;
		$('#MonthlyGrossOperatingIncome').val(MonthlyGrossOperatingIncome);
		
		var AnnualGrossOperatingIncome = MonthlyGrossOperatingIncome * 12;
		AnnualGrossOperatingIncome = AnnualGrossOperatingIncome ? parseFloat(AnnualGrossOperatingIncome) : 0;
		$('#AnnualGrossOperatingIncome').val(AnnualGrossOperatingIncome);
		
		var AnnualPercent_1 =  parseFloat((AnnualRent_1 / AnnualRent_Gross) * 100); $('#AnnualPercent_1').val((AnnualPercent_1 ? AnnualPercent_1 : 0));
		var AnnualPercent_2 =  parseFloat((AnnualRent_2 / AnnualRent_Gross) * 100); $('#AnnualPercent_2').val((AnnualPercent_2 ? AnnualPercent_2 : 0));
		var AnnualPercent_3 =  parseFloat((AnnualRent_3 / AnnualRent_Gross) * 100); $('#AnnualPercent_3').val((AnnualPercent_3 ? AnnualPercent_3 : 0));
		var AnnualPercent_4 =  parseFloat((AnnualRent_4 / AnnualRent_Gross) * 100); $('#AnnualPercent_4').val((AnnualPercent_4 ? AnnualPercent_4 : 0));
		var AnnualPercent_5 =  parseFloat((AnnualRent_5 / AnnualRent_Gross) * 100); $('#AnnualPercent_5').val((AnnualPercent_5 ? AnnualPercent_5 : 0));
		var AnnualPercent_6 =  parseFloat((AnnualRent_6 / AnnualRent_Gross) * 100); $('#AnnualPercent_6').val((AnnualPercent_6 ? AnnualPercent_6 : 0));
		var AnnualPercent_7 =  parseFloat((AnnualRent_7 / AnnualRent_Gross) * 100); $('#AnnualPercent_7').val((AnnualPercent_7 ? AnnualPercent_7 : 0));
		var AnnualPercent_8 =  parseFloat((AnnualRent_8 / AnnualRent_Gross) * 100); $('#AnnualPercent_8').val((AnnualPercent_8 ? AnnualPercent_8 : 0));
		var AnnualPercent_9 =  parseFloat((AnnualRent_9 / AnnualRent_Gross) * 100); $('#AnnualPercent_9').val((AnnualPercent_9 ? AnnualPercent_9 : 0));
		var AnnualPercent_10 =  parseFloat((AnnualRent_10 / AnnualRent_Gross) * 100); $('#AnnualPercent_10').val((AnnualPercent_10 ? AnnualPercent_10 : 0));
		var AnnualPercent_11 =  parseFloat((AnnualRent_11 / AnnualRent_Gross) * 100); $('#AnnualPercent_11').val((AnnualPercent_11 ? AnnualPercent_11 : 0));
		var AnnualPercent_12 =  parseFloat((AnnualRent_12 / AnnualRent_Gross) * 100); $('#AnnualPercent_12').val((AnnualPercent_12 ? AnnualPercent_12 : 0));

		var AnnualPercent_Gross = AnnualPercent_1 + AnnualPercent_2 + AnnualPercent_3 + AnnualPercent_4 + AnnualPercent_5 + AnnualPercent_6 + AnnualPercent_7 + AnnualPercent_8 + AnnualPercent_9 + AnnualPercent_10 + AnnualPercent_11 + AnnualPercent_12;
		AnnualPercent_Gross = AnnualPercent_Gross ? parseFloat(AnnualPercent_Gross) : 0;
		$('#AnnualPercent_Gross').val(Math.round(AnnualPercent_Gross));
		
		var MonthlyGrossOperatingIncome = $('#MonthlyGrossOperatingIncome').val() ? parseFloat($('#MonthlyGrossOperatingIncome').val()) : 0;
		$('#OperatingIncome').val(MonthlyGrossOperatingIncome);
    });
	
});