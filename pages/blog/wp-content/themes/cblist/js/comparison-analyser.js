jQuery(document).ready(function($) {
    "use strict";
	
	$('.form-control').on('change blur focus', function() {
		// FINANCING ASSUMPTIONS
		var DownPayment 		= $('#DownPayment').val() 		? parseFloat($('#DownPayment').val()/100)		: 0;
		
		var Used_1 				= $('#Used_1').val();
		var LoanType_1 			= $('#LoanType_1').val();
		var TermYears_1 		= $('#TermYears_1').val() 		? parseFloat($('#TermYears_1').val()) 			: 0;
		var LTV_1 				= $('#LTV_1').val() 			? parseFloat($('#LTV_1').val()/100)				: 0;
		var Rate_1 				= $('#Rate_1').val() 			? parseFloat($('#Rate_1').val()/100)			: 0;
		var OriginationFee_1	= $('#OriginationFee_1').val() 	? parseFloat($('#OriginationFee_1').val()/100) 	: 0;
		var DiscountFee_1 		= $('#DiscountFee_1').val() 	? parseFloat($('#DiscountFee_1').val()/100) 	: 0;
		
		var InterestOnly_1 		= (LoanType_1 == "Fixed" ? "No" : "Yes");
		$('#InterestOnly_1').val(InterestOnly_1);
		
		var Used_2 				= $('#Used_2').val();
		var LoanType_2 			= $('#LoanType_2').val();
		var TermYears_2 		= $('#TermYears_2').val() 		? parseFloat($('#TermYears_2').val()) 			: 0;
		var LTV_2 				= $('#LTV_2').val() 			? parseFloat($('#LTV_2').val()/100)				: 0;
		var Rate_2 				= $('#Rate_2').val() 			? parseFloat($('#Rate_2').val()/100)			: 0;
		var OriginationFee_2	= $('#OriginationFee_2').val() 	? parseFloat($('#OriginationFee_2').val()/100) 	: 0;
		var DiscountFee_2 		= $('#DiscountFee_2').val() 	? parseFloat($('#DiscountFee_2').val()/100) 	: 0;
		
		var InterestOnly_2 		= (LoanType_2 == "Fixed" ? "No" : "Yes");
		$('#InterestOnly_2').val(InterestOnly_2);
		
		var ClosingMiscFees 	= $('#ClosingMiscFees').val() 	? parseFloat($('#ClosingMiscFees').val()/100) 	: 0;
		var CashbackSeller 		= $('#CashbackSeller').val() 	? parseFloat($('#CashbackSeller').val()/100) 	: 0;
		
		// PROPERTY 1
		var P1Price 	= $('#P1Price').val() 	? parseFloat($('#P1Price').val())  : 0;
		var P1SqFeet 	= $('#P1SqFeet').val() 	? parseFloat($('#P1SqFeet').val()) : 0;
		var P1CapImp 	= $('#P1CapImp').val() 	? parseFloat($('#P1CapImp').val()) : 0;
		
		var P1Rent 			= $('#P1Rent').val() 		? parseFloat($('#P1Rent').val()) 		: 0;
		var P1Income		= $('#P1Income').val() 		? parseFloat($('#P1Income').val()) 		: 0;
		var P1Vacancy		= $('#P1Vacancy').val() 	? parseFloat($('#P1Vacancy').val()/100)		: 0;
		var P1Management	= $('#P1Management').val() 	? parseFloat($('#P1Management').val()/100) 	: 0;
		var P1Taxes			= $('#P1Taxes').val() 		? parseFloat($('#P1Taxes').val()) 		: 0;
		var P1Ins			= $('#P1Ins').val() 		? parseFloat($('#P1Ins').val()) 		: 0;
		var P1Maint			= $('#P1Maint').val() 		? parseFloat($('#P1Maint').val()) 		: 0;
		var P1OpEx			= $('#P1OpEx').val() 		? parseFloat($('#P1OpEx').val()) 		: 0;
		
		var P1CapOut = DownPayment * P1Price + P1CapImp + ((OriginationFee_1 + DiscountFee_1 + ClosingMiscFees) * LTV_1 * P1Price) * (Used_1 == 'Yes' ? 1 : 0) + ((OriginationFee_2 + DiscountFee_2) * LTV_2 * P1Price) * (Used_2 == 'Yes' ? 1 : 0) - CashbackSeller * P1Price;
		$('#P1CapOut').val(P1CapOut.toFixed(2));
		
		var P1TaxesRent		= (P1Rent > 0 ? P1Taxes / 12 / P1Rent : 0);
		$('#P1TaxesRent').val((P1TaxesRent*100).toFixed(2));
		var P1InsRent		= (P1Rent > 0 ? P1Ins / 12 / P1Rent : 0);
		$('#P1InsRent').val((P1InsRent*100).toFixed(2));
		var P1MaintRent		= (P1Rent > 0 ? P1Maint / 12 / P1Rent : 0);
		$('#P1MaintRent').val((P1MaintRent*100).toFixed(2));
		var P1OpExRent		= (P1Rent > 0 ? P1OpEx / 12 / P1Rent : 0);
		$('#P1OpExRent').val((P1OpExRent*100).toFixed(2));
		var P1RentPercent	= (P1TaxesRent + P1InsRent + P1MaintRent + P1OpExRent);
		$('#P1RentPercent').val((P1RentPercent*100).toFixed(2));
		
		var P1NOI = P1Rent - (P1Vacancy + P1Management) * P1Rent + P1Income - P1Taxes / 12 - P1Ins / 12 - P1Maint / 12 - P1OpEx / 12;
		$('#P1NOI').val(P1NOI.toFixed(2));
		
		var P1MortPymt = ((InterestOnly_1 == "No" ? PMT(Rate_1 / 12, TermYears_1 * 12, LTV_1 * P1Price, 0, 0) : -(Rate_1 / 12) * LTV_1 * P1Price)) * (Used_1 == "Yes" ? 1 : 0) + ((InterestOnly_2 == "No" ? PMT(Rate_2 / 12, TermYears_2 * 12, LTV_2 * P1Price, 0, 0) : -(Rate_2 / 12) * LTV_2 * P1Price)) * (Used_2 == "Yes" ? 1 : 0);
		$('#P1MortPymt').val(P1MortPymt.toFixed(2));
		
		
		// PROPERTY 2
		var P2Price 	= $('#P2Price').val() 	? parseFloat($('#P2Price').val())  : 0;
		var P2SqFeet 	= $('#P2SqFeet').val() 	? parseFloat($('#P2SqFeet').val()) : 0;
		var P2CapImp 	= $('#P2CapImp').val() 	? parseFloat($('#P2CapImp').val()) : 0;
		
		var P2Rent 			= $('#P2Rent').val() 		? parseFloat($('#P2Rent').val()) 		: 0;
		var P2Income		= $('#P2Income').val() 		? parseFloat($('#P2Income').val()) 		: 0;
		var P2Vacancy		= $('#P2Vacancy').val() 	? parseFloat($('#P2Vacancy').val()/100) 	: 0;
		var P2Management	= $('#P2Management').val() 	? parseFloat($('#P2Management').val()/100) 	: 0;
		var P2Taxes			= $('#P2Taxes').val() 		? parseFloat($('#P2Taxes').val()) 		: 0;
		var P2Ins			= $('#P2Ins').val() 		? parseFloat($('#P2Ins').val()) 		: 0;
		var P2Maint			= $('#P2Maint').val() 		? parseFloat($('#P2Maint').val()) 		: 0;
		var P2OpEx			= $('#P2OpEx').val() 		? parseFloat($('#P2OpEx').val()) 		: 0;
		
		var P2CapOut = DownPayment * P2Price + P2CapImp + ((OriginationFee_1 + DiscountFee_1 + ClosingMiscFees) * LTV_1 * P2Price) * (Used_1 == 'Yes' ? 1 : 0) + ((OriginationFee_2 + DiscountFee_2) * LTV_2 * P2Price) * (Used_2 == 'Yes' ? 1 : 0) - CashbackSeller * P2Price;
		$('#P2CapOut').val(P2CapOut.toFixed(2));
		
		var P2TaxesRent		= (P2Rent > 0 ? P2Taxes / 12 / P2Rent : 0);
		$('#P2TaxesRent').val((P2TaxesRent*100).toFixed(2));
		var P2InsRent		= (P2Rent > 0 ? P2Ins / 12 / P2Rent : 0);
		$('#P2InsRent').val((P2InsRent*100).toFixed(2));
		var P2MaintRent		= (P2Rent > 0 ? P2Maint / 12 / P2Rent : 0);
		$('#P2MaintRent').val((P2MaintRent*100).toFixed(2));
		var P2OpExRent		= (P2Rent > 0 ? P2OpEx / 12 / P2Rent : 0);
		$('#P2OpExRent').val((P2OpExRent*100).toFixed(2));
		var P2RentPercent	= (P2TaxesRent + P2InsRent + P2MaintRent + P2OpExRent);
		$('#P2RentPercent').val((P2RentPercent*100).toFixed(2));
		
		var P2NOI = P2Rent - (P2Vacancy + P2Management) * P2Rent + P2Income - P2Taxes / 12 - P2Ins / 12 - P2Maint / 12 - P2OpEx / 12;
		$('#P2NOI').val(P2NOI.toFixed(2));
		
		var P2MortPymt = ((InterestOnly_1 == "No" ? PMT(Rate_1 / 12, TermYears_1 * 12, LTV_1 * P2Price, 0, 0) : -(Rate_1 / 12) * LTV_1 * P2Price)) * (Used_1 == "Yes" ? 1 : 0) + ((InterestOnly_2 == "No" ? PMT(Rate_2 / 12, TermYears_2 * 12, LTV_2 * P2Price, 0, 0) : -(Rate_2 / 12) * LTV_2 * P2Price)) * (Used_2 == "Yes" ? 1 : 0);
		$('#P2MortPymt').val(P2MortPymt.toFixed(2));
		
		
		// PEOPERTY 3
		var P3Price 	= $('#P3Price').val() 	? parseFloat($('#P3Price').val())  : 0;
		var P3SqFeet 	= $('#P3SqFeet').val() 	? parseFloat($('#P3SqFeet').val()) : 0;
		var P3CapImp 	= $('#P3CapImp').val() 	? parseFloat($('#P3CapImp').val()) : 0;
		
		var P3Rent 			= $('#P3Rent').val() 		? parseFloat($('#P3Rent').val()) 		: 0;
		var P3Income		= $('#P3Income').val() 		? parseFloat($('#P3Income').val()) 		: 0;
		var P3Vacancy		= $('#P3Vacancy').val() 	? parseFloat($('#P3Vacancy').val()/100) 	: 0;
		var P3Management	= $('#P3Management').val() 	? parseFloat($('#P3Management').val()/100) 	: 0;
		var P3Taxes			= $('#P3Taxes').val() 		? parseFloat($('#P3Taxes').val()) 		: 0;
		var P3Ins			= $('#P3Ins').val() 		? parseFloat($('#P3Ins').val()) 		: 0;
		var P3Maint			= $('#P3Maint').val() 		? parseFloat($('#P3Maint').val()) 		: 0;
		var P3OpEx			= $('#P3OpEx').val() 		? parseFloat($('#P3OpEx').val()) 		: 0;
		
		var P3CapOut = DownPayment * P3Price + P3CapImp + ((OriginationFee_1 + DiscountFee_1 + ClosingMiscFees) * LTV_1 * P3Price) * (Used_1 == 'Yes' ? 1 : 0) + ((OriginationFee_2 + DiscountFee_2) * LTV_2 * P3Price) * (Used_2 == 'Yes' ? 1 : 0) - CashbackSeller * P3Price;
		$('#P3CapOut').val(P3CapOut.toFixed(2));
		
		var P3TaxesRent		= (P3Rent > 0 ? P3Taxes / 12 / P3Rent : 0);
		$('#P3TaxesRent').val((P3TaxesRent*100).toFixed(2));
		var P3InsRent		= (P3Rent > 0 ? P3Ins / 12 / P3Rent : 0);
		$('#P3InsRent').val((P3InsRent*100).toFixed(2));
		var P3MaintRent		= (P3Rent > 0 ? P3Maint / 12 / P3Rent : 0);
		$('#P3MaintRent').val((P3MaintRent*100).toFixed(2));
		var P3OpExRent		= (P3Rent > 0 ? P3OpEx / 12 / P3Rent : 0);
		$('#P3OpExRent').val((P3OpExRent*100).toFixed(2));
		var P3RentPercent	= (P3TaxesRent + P3InsRent + P3MaintRent + P3OpExRent);
		$('#P3RentPercent').val((P3RentPercent*100).toFixed(2));
		
		var P3NOI = P3Rent - (P3Vacancy + P3Management) * P3Rent + P3Income - P3Taxes / 12 - P3Ins / 12 - P3Maint / 12 - P3OpEx / 12;
		$('#P3NOI').val(P3NOI.toFixed(2));
		
		var P3MortPymt = ((InterestOnly_1 == "No" ? PMT(Rate_1 / 12, TermYears_1 * 12, LTV_1 * P3Price, 0, 0) : -(Rate_1 / 12) * LTV_1 * P3Price)) * (Used_1 == "Yes" ? 1 : 0) + ((InterestOnly_2 == "No" ? PMT(Rate_2 / 12, TermYears_2 * 12, LTV_2 * P3Price, 0, 0) : -(Rate_2 / 12) * LTV_2 * P3Price)) * (Used_2 == "Yes" ? 1 : 0);
		$('#P3MortPymt').val(P3MortPymt.toFixed(2));
		
		
		// PROPERTY 4
		var P4Price 	= $('#P4Price').val() 	? parseFloat($('#P4Price').val())  : 0;
		var P4SqFeet 	= $('#P4SqFeet').val() 	? parseFloat($('#P4SqFeet').val()) : 0;
		var P4CapImp 	= $('#P4CapImp').val() 	? parseFloat($('#P4CapImp').val()) : 0;
		
		var P4Rent 			= $('#P4Rent').val() 		? parseFloat($('#P4Rent').val()) 		: 0;
		var P4Income		= $('#P4Income').val() 		? parseFloat($('#P4Income').val()) 		: 0;
		var P4Vacancy		= $('#P4Vacancy').val() 	? parseFloat($('#P4Vacancy').val()/100) 	: 0;
		var P4Management	= $('#P4Management').val() 	? parseFloat($('#P4Management').val()/100) 	: 0;
		var P4Taxes			= $('#P4Taxes').val() 		? parseFloat($('#P4Taxes').val()) 		: 0;
		var P4Ins			= $('#P4Ins').val() 		? parseFloat($('#P4Ins').val()) 		: 0;
		var P4Maint			= $('#P4Maint').val() 		? parseFloat($('#P4Maint').val()) 		: 0;
		var P4OpEx			= $('#P4OpEx').val() 		? parseFloat($('#P4OpEx').val()) 		: 0;
		
		var P4CapOut = DownPayment * P4Price + P4CapImp + ((OriginationFee_1 + DiscountFee_1 + ClosingMiscFees) * LTV_1 * P4Price) * (Used_1 == 'Yes' ? 1 : 0) + ((OriginationFee_2 + DiscountFee_2) * LTV_2 * P4Price) * (Used_2 == 'Yes' ? 1 : 0) - CashbackSeller * P4Price;
		$('#P4CapOut').val(P4CapOut.toFixed(2));
		
		var P4TaxesRent		= (P4Rent > 0 ? P4Taxes / 12 / P4Rent : 0);
		$('#P4TaxesRent').val((P4TaxesRent*100).toFixed(2));
		var P4InsRent		= (P4Rent > 0 ? P4Ins / 12 / P4Rent : 0);
		$('#P4InsRent').val((P4InsRent*100).toFixed(2));
		var P4MaintRent		= (P4Rent > 0 ? P4Maint / 12 / P4Rent : 0);
		$('#P4MaintRent').val((P4MaintRent*100).toFixed(2));
		var P4OpExRent		= (P4Rent > 0 ? P4OpEx / 12 / P4Rent : 0);
		$('#P4OpExRent').val((P4OpExRent*100).toFixed(2));
		var P4RentPercent	= (P4TaxesRent + P4InsRent + P4MaintRent + P4OpExRent);
		$('#P4RentPercent').val((P4RentPercent*100).toFixed(2));
		
		var P4NOI = P4Rent - (P4Vacancy + P4Management) * P4Rent + P4Income - P4Taxes / 12 - P4Ins / 12 - P4Maint / 12 - P4OpEx / 12;
		$('#P4NOI').val(P4NOI.toFixed(2));
		
		var P4MortPymt = ((InterestOnly_1 == "No" ? PMT(Rate_1 / 12, TermYears_1 * 12, LTV_1 * P4Price, 0, 0) : -(Rate_1 / 12) * LTV_1 * P4Price)) * (Used_1 == "Yes" ? 1 : 0) + ((InterestOnly_2 == "No" ? PMT(Rate_2 / 12, TermYears_2 * 12, LTV_2 * P4Price, 0, 0) : -(Rate_2 / 12) * LTV_2 * P4Price)) * (Used_2 == "Yes" ? 1 : 0);
		$('#P4MortPymt').val(P4MortPymt.toFixed(2));
		
		
		// PROPERTY 5
		var P5Price 	= $('#P5Price').val() 	? parseFloat($('#P5Price').val())  : 0;
		var P5SqFeet 	= $('#P5SqFeet').val() 	? parseFloat($('#P5SqFeet').val()) : 0;
		var P5CapImp 	= $('#P5CapImp').val() 	? parseFloat($('#P5CapImp').val()) : 0;
		
		var P5Rent 			= $('#P5Rent').val() 		? parseFloat($('#P5Rent').val()) 		: 0;
		var P5Income		= $('#P5Income').val() 		? parseFloat($('#P5Income').val()) 		: 0;
		var P5Vacancy		= $('#P5Vacancy').val() 	? parseFloat($('#P5Vacancy').val()/100) 	: 0;
		var P5Management	= $('#P5Management').val() 	? parseFloat($('#P5Management').val()/100) 	: 0;
		var P5Taxes			= $('#P5Taxes').val() 		? parseFloat($('#P5Taxes').val()) 		: 0;
		var P5Ins			= $('#P5Ins').val() 		? parseFloat($('#P5Ins').val()) 		: 0;
		var P5Maint			= $('#P5Maint').val() 		? parseFloat($('#P5Maint').val()) 		: 0;
		var P5OpEx			= $('#P5OpEx').val() 		? parseFloat($('#P5OpEx').val()) 		: 0;
		
		var P5CapOut = DownPayment * P5Price + P5CapImp + ((OriginationFee_1 + DiscountFee_1 + ClosingMiscFees) * LTV_1 * P5Price) * (Used_1 == 'Yes' ? 1 : 0) + ((OriginationFee_2 + DiscountFee_2) * LTV_2 * P5Price) * (Used_2 == 'Yes' ? 1 : 0) - CashbackSeller * P5Price;
		$('#P5CapOut').val(P5CapOut.toFixed(2));
		
		var P5TaxesRent		= (P5Rent > 0 ? P5Taxes / 12 / P5Rent : 0);
		$('#P5TaxesRent').val((P5TaxesRent*100).toFixed(2));
		var P5InsRent		= (P5Rent > 0 ? P5Ins / 12 / P5Rent : 0);
		$('#P5InsRent').val((P5InsRent*100).toFixed(2));
		var P5MaintRent		= (P5Rent > 0 ? P5Maint / 12 / P5Rent : 0);
		$('#P5MaintRent').val((P5MaintRent*100).toFixed(2));
		var P5OpExRent		= (P5Rent > 0 ? P5OpEx / 12 / P5Rent : 0);
		$('#P5OpExRent').val((P5OpExRent*100).toFixed(2));
		var P5RentPercent	= (P5TaxesRent + P5InsRent + P5MaintRent + P5OpExRent);
		$('#P5RentPercent').val((P5RentPercent*100).toFixed(2));
		
		var P5NOI = P5Rent - (P5Vacancy + P5Management) * P5Rent + P5Income - P5Taxes / 12 - P5Ins / 12 - P5Maint / 12 - P5OpEx / 12;
		$('#P5NOI').val(P5NOI.toFixed(2));
		
		var P5MortPymt = ((InterestOnly_1 == "No" ? PMT(Rate_1 / 12, TermYears_1 * 12, LTV_1 * P5Price, 0, 0) : -(Rate_1 / 12) * LTV_1 * P5Price)) * (Used_1 == "Yes" ? 1 : 0) + ((InterestOnly_2 == "No" ? PMT(Rate_2 / 12, TermYears_2 * 12, LTV_2 * P5Price, 0, 0) : -(Rate_2 / 12) * LTV_2 * P5Price)) * (Used_2 == "Yes" ? 1 : 0);
		$('#P5MortPymt').val(P5MortPymt.toFixed(2));
		
	});
});