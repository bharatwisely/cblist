// variables
var base_url = $('#base_url').val();

// function for redirect page
var goto = function(url) {
	if ( url ) {
		window.location.href = url;
	}
	return false;
};
// end of function

// function for read the upload images path
var readURL = function(input, img, prev) {
	if ( input.files && input.files[0] ) {
		var reader = new FileReader();
		reader.onload = function (e) {
			if ( prev == true ) {
				$(input).prev(img).attr('src', e.target.result);
			} else {
				$(img).attr('src', e.target.result);
			}
		}
		reader.readAsDataURL(input.files[0]);
	}
};
// end of function

// function for set cookie
var setCookie = function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}
// end of function 

// function for get cookie
var getCookie = function(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
    }
    return "";
}
// end of function

// Bootstrap Modal Top Position adjustment
function adjustModalMaxHeightAndPosition(){
	$('.modal').each(function(){
		if($(this).hasClass('in') === false){
			$(this).show();
		}
		var contentHeight = $(window).height() - 60;
		var headerHeight = $(this).find('.modal-header').outerHeight() || 2;
		var footerHeight = $(this).find('.modal-footer').outerHeight() || 2;
		
		$(this).find('.modal-content').css({
			'max-height': function () {
				return contentHeight;
			}
		});
		
		$(this).find('.modal-body').css({
			'max-height': function () {
				return contentHeight - (headerHeight + footerHeight);
			}
		});
		
		$(this).find('.modal-dialog').addClass('modal-dialog-center').css({
			'margin-top': function () {
				return -($(this).outerHeight() / 2);
			},
			'margin-left': function () {
				return -($(this).outerWidth() / 2);
			}
		});
		if($(this).hasClass('in') === false){
			$(this).hide();
		}
	});
}
// end of function


function PMT(rate_per_period, number_of_payments, present_value, future_value, type) {
    if ( rate_per_period != 0.0 ) {
        var q = Math.pow(1 + rate_per_period, number_of_payments);
        return -(rate_per_period * (future_value + (q * present_value))) / ((-1 + q) * (1 + rate_per_period * (type)));
    } else if(number_of_payments != 0.0){
        return -(future_value + present_value) / number_of_payments;
    }
    return 0;
}

var originalLeave = $.fn.popover.Constructor.prototype.leave;
$.fn.popover.Constructor.prototype.leave = function(obj){
	var self = obj instanceof this.constructor ? obj : $(obj.currentTarget)[this.type](this.getDelegateOptions()).data('bs.' + this.type);
	var container, timeout;
	
	originalLeave.call(this, obj);
	
	if(obj.currentTarget) {
		container = $(obj.currentTarget).siblings('.popover')
		timeout = self.timeout;
		container.one('mouseenter', function(){
			clearTimeout(timeout);
			container.one('mouseleave', function(){
				$.fn.popover.Constructor.prototype.leave.call(self, self);
			});
		});
	}
};


// function for open proposal print dialog
function open_print_proposal(propertyId, btnType){
	if ( btnType == 'print' ) {
		$('.btn_proposal_print').show().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_download').hide().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_email').hide().attr("data-propertyId", propertyId.trim());
	} else if ( btnType == 'download' ) {
		$('.btn_proposal_print').hide().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_download').show().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_email').hide().attr("data-propertyId", propertyId.trim());
	} else if ( btnType == 'email' ) {
		$('.btn_proposal_print').hide().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_download').hide().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_email').show().attr("data-propertyId", propertyId.trim());
	}
	$('#reports').modal('show');
}

function open_print_rental(propertyId, btnType){
	if ( btnType == 'print' ) {
		$('.btn_proposal_print').show().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_download').hide().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_email').hide().attr("data-propertyId", propertyId.trim());
	} else if ( btnType == 'download' ) {
		$('.btn_proposal_print').hide().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_download').show().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_email').hide().attr("data-propertyId", propertyId.trim());
	} else if ( btnType == 'email' ) {
		$('.btn_proposal_print').hide().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_download').hide().attr("data-propertyId", propertyId.trim());
		$('.btn_proposal_email').show().attr("data-propertyId", propertyId.trim());
	}
	$('#rental_reports').modal('show');
}

function OpenInNewTab(url) {
 	var url1 = window.open(url, '_blank');
}


// function for upgrade investor
function investor_upgrade(user_id) {
	$.ajax({
		type: 'POST',
		url: base_url + 'pages/checkuserpaid/' + user_id + '/',
		data: {},
		success: function(data) {
			if ( data == 1 || data == 0 ) {
				window.location.href = base_url + 'pages/payment/';
			}
		},
	});
}

// function paypal pay
function paypal_pay() {
	$(".choosePlan-form").submit(function(e) {
		var postData = $(this).serialize();
		var formURL = $(this).attr("action");
		$.ajax ({
			url : formURL,
			type: "POST",
			data : postData,
			success:function(data){
				$('#response_demo').html(data);
				$('.cblist97').submit();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//if fails     
			}
		});
		e.preventDefault(); 
		e.unbind(); 
	});
	$(".choosePlan-form").submit(); 
}

// function get transaction data
function get_transaction_data() {
	$.ajax({
		type: "GET",
		data: {},
		url: base_url + 'pages/payment_status/',
		success: function(data) {
			var json_data = JSON.parse(data);
			var status  = parseInt(json_data.status);
			if ( status == 1 ) {
				$.ajax({
					type:"GET",
					data:{},
					url: base_url + "pages/payment_details/",
					success:function(data){
						json_data = JSON.parse(data);
						$("#subscription_id").html(json_data.subscr_id);
						$("#subscription_date").html(json_data.payment_date);
						$("#subscription_email").html(json_data.payer_email);
						$("#subscription_amount").html('$'+json_data.amount);
						$(".transaction_data").css("display","block");
						$(".sucess_image").css("display","block");
						$(".pending_image").css("display","none");
						$(".success_header").css("display","block");
						$(".pending_header").css("display","none");
						$(".re_page").css("display","block");
						if ( json_data.amount == '297.00' ) {
							$('#upgrade-btn').remove();
						} else {
							$("#PaidText").text('Become a Premium Member');
							$("#buynow_btn").html('<i class="fa fa-credit-card"></i>&nbsp;&nbsp;&nbsp;UPGRADE NOW');
						}
					}
				});
			}
		},
		error: function(xhr,status,error) {
			console.log("error");
		}
	});
}


var sucmsgtime;
function hide_success_message() {
	sucmsgtime = setTimeout(function() {
		$('.success_message').slideToggle('slow').remove();
	}, 2000);
}
function show_success_message(msg) {
	clearTimeout(sucmsgtime);
	if ( $('.success_message').length > 0 ) {
		$('.success_message').remove();
	}
	$('body').append('<div class="success_message">' + msg + '</div>');
	hide_success_message();
}

var errmsgtime;
function hide_error_message() {
	errmsgtime = setTimeout(function() {
		$('.error_message').slideToggle('slow').remove();
	}, 2000);
}
function show_error_message(msg) {
	clearTimeout(errmsgtime);
	if ( $('.error_message').length > 0 ) {
		$('.error_message').remove();
	}
	$('body').append('<div class="error_message">' + msg + '</div>');
	hide_error_message();
	return false;
}