jQuery(document).ready(function($) {
    "use strict";
	
	$('.form-control').on('change blur focus', function() {
		var PropertyUnit1 = $('#PropertyNoOfUnit_1').val() ? parseFloat($('#PropertyNoOfUnit_1').val()) : 0;
		var PropertySquareFeet1 = $('#PropertySquareFeet_1').val() ? parseFloat($('#PropertySquareFeet_1').val()) : 0;
		var PropertyMonthlyRent1 = $('#PropertyMonthlyRent_1').val() ? parseFloat($('#PropertyMonthlyRent_1').val()) : 0;
		var PropertyAnnualRent1 = PropertyUnit1*12*PropertyMonthlyRent1;
		$('#PropertyAnnualRent_1').val(PropertyAnnualRent1.toFixed(2));
		
		var PropertyUnit2 = $('#PropertyNoOfUnit_2').val() ? parseFloat($('#PropertyNoOfUnit_2').val()) : 0;
		var PropertySquareFeet2 = $('#PropertySquareFeet_2').val() ? parseFloat($('#PropertySquareFeet_2').val()) : 0;
		var PropertyMonthlyRent2 = $('#PropertyMonthlyRent_2').val() ? parseFloat($('#PropertyMonthlyRent_2').val()) : 0;
		var PropertyAnnualRent2 = PropertyUnit2*12*PropertyMonthlyRent2;
		$('#PropertyAnnualRent_2').val(PropertyAnnualRent2.toFixed(2));
		
		var PropertyUnit3 = $('#PropertyNoOfUnit_3').val() ? parseFloat($('#PropertyNoOfUnit_3').val()) : 0;
		var PropertySquareFeet3 = $('#PropertySquareFeet_3').val() ? parseFloat($('#PropertySquareFeet_3').val()) : 0;
		var PropertyMonthlyRent3 = $('#PropertyMonthlyRent_3').val() ? parseFloat($('#PropertyMonthlyRent_3').val()) : 0;
		var PropertyAnnualRent3 = PropertyUnit3*12*PropertyMonthlyRent3;
		$('#PropertyAnnualRent_3').val(PropertyAnnualRent3.toFixed(2));
		
		var PropertyUnit4 = $('#PropertyNoOfUnit_4').val() ? parseFloat($('#PropertyNoOfUnit_4').val()) : 0;
		var PropertySquareFeet4 = $('#PropertySquareFeet_4').val() ? parseFloat($('#PropertySquareFeet_4').val()) : 0;
		var PropertyMonthlyRent4 = $('#PropertyMonthlyRent_4').val() ? parseFloat($('#PropertyMonthlyRent_4').val()) : 0;
		var PropertyAnnualRent4 = PropertyUnit4*12*PropertyMonthlyRent4;
		$('#PropertyAnnualRent_4').val(PropertyAnnualRent4.toFixed(2));
		
		var PropertyUnit5 = $('#PropertyNoOfUnit_5').val() ? parseFloat($('#PropertyNoOfUnit_5').val()) : 0;
		var PropertySquareFeet5 = $('#PropertySquareFeet_5').val() ? parseFloat($('#PropertySquareFeet_5').val()) : 0;
		var PropertyMonthlyRent5 = $('#PropertyMonthlyRent_5').val() ? parseFloat($('#PropertyMonthlyRent_5').val()) : 0;
		var PropertyAnnualRent5 = PropertyUnit5*12*PropertyMonthlyRent5;
		$('#PropertyAnnualRent_5').val(PropertyAnnualRent5.toFixed(2));
		
		var PropertyUnit6 = $('#PropertyNoOfUnit_6').val() ? parseFloat($('#PropertyNoOfUnit_6').val()) : 0;
		var PropertySquareFeet6 = $('#PropertySquareFeet_6').val() ? parseFloat($('#PropertySquareFeet_6').val()) : 0;
		var PropertyMonthlyRent6 = $('#PropertyMonthlyRent_6').val() ? parseFloat($('#PropertyMonthlyRent_6').val()) : 0;
		var PropertyAnnualRent6 = PropertyUnit6*12*PropertyMonthlyRent6;
		$('#PropertyAnnualRent_6').val(PropertyAnnualRent6.toFixed(2));
		
		var PropertyUnit7 = $('#PropertyNoOfUnit_7').val() ? parseFloat($('#PropertyNoOfUnit_7').val()) : 0;
		var PropertySquareFeet7 = $('#PropertySquareFeet_7').val() ? parseFloat($('#PropertySquareFeet_7').val()) : 0;
		var PropertyMonthlyRent7 = $('#PropertyMonthlyRent_7').val() ? parseFloat($('#PropertyMonthlyRent_7').val()) : 0;
		var PropertyAnnualRent7 = PropertyUnit7*12*PropertyMonthlyRent7;
		$('#PropertyAnnualRent_7').val(PropertyAnnualRent7.toFixed(2));
		
		var PropertyUnit8 = $('#PropertyNoOfUnit_8').val() ? parseFloat($('#PropertyNoOfUnit_8').val()) : 0;
		var PropertySquareFeet8 = $('#PropertySquareFeet_8').val() ? parseFloat($('#PropertySquareFeet_8').val()) : 0;
		var PropertyMonthlyRent8 = $('#PropertyMonthlyRent_8').val() ? parseFloat($('#PropertyMonthlyRent_8').val()) : 0;
		var PropertyAnnualRent8 = PropertyUnit8*12*PropertyMonthlyRent8;
		$('#PropertyAnnualRent_8').val(PropertyAnnualRent8.toFixed(2));
		
		var PropertyUnit9 = $('#PropertyNoOfUnit_9').val() ? parseFloat($('#PropertyNoOfUnit_9').val()) : 0;
		var PropertySquareFeet9 = $('#PropertySquareFeet_9').val() ? parseFloat($('#PropertySquareFeet_9').val()) : 0;
		var PropertyMonthlyRent9 = $('#PropertyMonthlyRent_9').val() ? parseFloat($('#PropertyMonthlyRent_9').val()) : 0;
		var PropertyAnnualRent9 = PropertyUnit9*12*PropertyMonthlyRent9;
		$('#PropertyAnnualRent_9').val(PropertyAnnualRent9.toFixed(2));
		
		var PropertyUnit10 = $('#PropertyNoOfUnit_10').val() ? parseFloat($('#PropertyNoOfUnit_10').val()) : 0;
		var PropertySquareFeet10 = $('#PropertySquareFeet_10').val() ? parseFloat($('#PropertySquareFeet_10').val()) : 0;
		var PropertyMonthlyRent10 = $('#PropertyMonthlyRent_10').val() ? parseFloat($('#PropertyMonthlyRent_10').val()) : 0;
		var PropertyAnnualRent10 = PropertyUnit10*12*PropertyMonthlyRent10;
		$('#PropertyAnnualRent_10').val(PropertyAnnualRent10.toFixed(2));
		
		var PropertyUnit11 = $('#PropertyNoOfUnit_11').val() ? parseFloat($('#PropertyNoOfUnit_11').val()) : 0;
		var PropertySquareFeet11 = $('#PropertySquareFeet_11').val() ? parseFloat($('#PropertySquareFeet_11').val()) : 0;
		var PropertyMonthlyRent11 = $('#PropertyMonthlyRent_11').val() ? parseFloat($('#PropertyMonthlyRent_11').val()) : 0;
		var PropertyAnnualRent11 = PropertyUnit11*12*PropertyMonthlyRent11;
		$('#PropertyAnnualRent_11').val(PropertyAnnualRent11.toFixed(2));
		
		var PropertyUnit12 = $('#PropertyNoOfUnit_12').val() ? parseFloat($('#PropertyNoOfUnit_12').val()) : 0;
		var PropertySquareFeet12 = $('#PropertySquareFeet_12').val() ? parseFloat($('#PropertySquareFeet_12').val()) : 0;
		var PropertyMonthlyRent12 = $('#PropertyMonthlyRent_12').val() ? parseFloat($('#PropertyMonthlyRent_12').val()) : 0;
		var PropertyAnnualRent12 = PropertyUnit12*12*PropertyMonthlyRent12;
		$('#PropertyAnnualRent_12').val(PropertyAnnualRent12.toFixed(2));
		
		var PropertyNoOfUnit_Total = PropertyUnit1+PropertyUnit2+PropertyUnit3+PropertyUnit4+PropertyUnit5+PropertyUnit6+PropertyUnit7+PropertyUnit8+PropertyUnit9+PropertyUnit10+PropertyUnit11+PropertyUnit12;
		$('#PropertyNoOfUnit_Total').val(PropertyNoOfUnit_Total);
		
		var  GrossScheduledIncomeMonthly = (PropertyUnit1*PropertyMonthlyRent1)+(PropertyUnit2*PropertyMonthlyRent2)+(PropertyUnit3*PropertyMonthlyRent3)+(PropertyUnit4*PropertyMonthlyRent4)+(PropertyUnit5*PropertyMonthlyRent5)+(PropertyUnit6*PropertyMonthlyRent6)+(PropertyUnit7*PropertyMonthlyRent7)+(PropertyUnit8*PropertyMonthlyRent8)+(PropertyUnit9*PropertyMonthlyRent9)+(PropertyUnit10*PropertyMonthlyRent10)+(PropertyUnit11*PropertyMonthlyRent11)+(PropertyUnit12*PropertyMonthlyRent12);
		
		var PropertySquareFeet_Total = (PropertyUnit1*PropertySquareFeet1)+(PropertyUnit2*PropertySquareFeet2)+(PropertyUnit3*PropertySquareFeet3)+(PropertyUnit4*PropertySquareFeet4)+(PropertyUnit5*PropertySquareFeet5)+(PropertyUnit6*PropertySquareFeet6)+(PropertyUnit7*PropertySquareFeet7)+(PropertyUnit8*PropertySquareFeet8)+(PropertyUnit9*PropertySquareFeet9)+(PropertyUnit10*PropertySquareFeet10)+(PropertyUnit11*PropertySquareFeet11)+(PropertyUnit12*PropertySquareFeet12);
		$('#GrossScheduledIncomeMonthly').val(GrossScheduledIncomeMonthly.toFixed(2));
		$('#PropertySquareFeet_Total').val(PropertySquareFeet_Total);
		
		var GrossScheduledIncomeAnnual = GrossScheduledIncomeMonthly*12;
		$('#GrossScheduledIncomeAnnual').val(GrossScheduledIncomeAnnual.toFixed(2));
		
		
		var PropertyRentSqFtPercentage_1 = ((PropertyMonthlyRent1 && PropertySquareFeet1) ? (PropertyMonthlyRent1/PropertySquareFeet1) : 0);
		$('#PropertyRentSqFtPercentage_1').val(PropertyRentSqFtPercentage_1.toFixed(2));
		
		var PropertyRentSqFtPercentage_2 = ((PropertyMonthlyRent2 && PropertySquareFeet2) ? (PropertyMonthlyRent2/PropertySquareFeet2) : 0);
		$('#PropertyRentSqFtPercentage_2').val(PropertyRentSqFtPercentage_2.toFixed(2));
		
		var PropertyRentSqFtPercentage_3 = ((PropertyMonthlyRent3 && PropertySquareFeet3) ? (PropertyMonthlyRent3/PropertySquareFeet3) : 0);
		$('#PropertyRentSqFtPercentage_3').val(PropertyRentSqFtPercentage_3.toFixed(2));
		
		var PropertyRentSqFtPercentage_4 = ((PropertyMonthlyRent4 && PropertySquareFeet4) ? (PropertyMonthlyRent4/PropertySquareFeet4) : 0);
		$('#PropertyRentSqFtPercentage_4').val(PropertyRentSqFtPercentage_4.toFixed(2));
		
		var PropertyRentSqFtPercentage_5 = ((PropertyMonthlyRent5 && PropertySquareFeet5) ? (PropertyMonthlyRent5/PropertySquareFeet5) : 0);
		$('#PropertyRentSqFtPercentage_5').val(PropertyRentSqFtPercentage_5.toFixed(2));
		
		var PropertyRentSqFtPercentage_6 = ((PropertyMonthlyRent6 && PropertySquareFeet6) ? (PropertyMonthlyRent6/PropertySquareFeet6) : 0);
		$('#PropertyRentSqFtPercentage_6').val(PropertyRentSqFtPercentage_6.toFixed(2));
		
		var PropertyRentSqFtPercentage_7 = ((PropertyMonthlyRent7 && PropertySquareFeet7) ? (PropertyMonthlyRent7/PropertySquareFeet7) : 0);
		$('#PropertyRentSqFtPercentage_7').val(PropertyRentSqFtPercentage_7.toFixed(2));
		
		var PropertyRentSqFtPercentage_8 = ((PropertyMonthlyRent8 && PropertySquareFeet8) ? (PropertyMonthlyRent8/PropertySquareFeet8) : 0);
		$('#PropertyRentSqFtPercentage_8').val(PropertyRentSqFtPercentage_8.toFixed(2));
		
		var PropertyRentSqFtPercentage_9 = ((PropertyMonthlyRent9 && PropertySquareFeet9) ? (PropertyMonthlyRent9/PropertySquareFeet9) : 0);
		$('#PropertyRentSqFtPercentage_9').val(PropertyRentSqFtPercentage_9.toFixed(2));
		
		var PropertyRentSqFtPercentage_10 = ((PropertyMonthlyRent10 && PropertySquareFeet10) ? (PropertyMonthlyRent10/PropertySquareFeet10) : 0);
		$('#PropertyRentSqFtPercentage_10').val(PropertyRentSqFtPercentage_10.toFixed(2));
		
		var PropertyRentSqFtPercentage_11 = ((PropertyMonthlyRent11 && PropertySquareFeet11) ? (PropertyMonthlyRent11/PropertySquareFeet11) : 0);
		$('#PropertyRentSqFtPercentage_11').val(PropertyRentSqFtPercentage_11.toFixed(2));
		
		var PropertyRentSqFtPercentage_12 = ((PropertyMonthlyRent12 && PropertySquareFeet12) ? (PropertyMonthlyRent12/PropertySquareFeet12) : 0);
		$('#PropertyRentSqFtPercentage_12').val(PropertyRentSqFtPercentage_12.toFixed(2));
		
		
		if(PropertyAnnualRent1!=0){
			var PropertyPercentage_1 = (PropertyAnnualRent1/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_1').val(PropertyPercentage_1.toFixed(2));
		}else{
			var PropertyPercentage_1 = 0;
			$('#PropertyPercentage_1').val(PropertyPercentage_1.toFixed(2));
		}
		if(PropertyAnnualRent2!=0){
			var PropertyPercentage_2 = (PropertyAnnualRent2/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_2').val(PropertyPercentage_2.toFixed(2));
		}else{
			var PropertyPercentage_2 = 0;
			$('#PropertyPercentage_2').val(PropertyPercentage_2.toFixed(2));
		}
		if(PropertyAnnualRent3!=0){
			var PropertyPercentage_3 = (PropertyAnnualRent3/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_3').val(PropertyPercentage_3.toFixed(2));
		}else{
			var PropertyPercentage_3 = 0;
			$('#PropertyPercentage_3').val(PropertyPercentage_3.toFixed(2));
		}
		if(PropertyAnnualRent4!=0){
			var PropertyPercentage_4 = (PropertyAnnualRent4/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_4').val(PropertyPercentage_4.toFixed(2));
		}else{
			var PropertyPercentage_4 = 0;
			$('#PropertyPercentage_4').val(PropertyPercentage_4.toFixed(2));
		}
		if(PropertyAnnualRent5!=0){
			var PropertyPercentage_5 = (PropertyAnnualRent5/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_5').val(PropertyPercentage_5.toFixed(2));
		}else{
			var PropertyPercentage_5 = 0;
			$('#PropertyPercentage_5').val(PropertyPercentage_5.toFixed(2));
		}
		if(PropertyAnnualRent6!=0){
			var PropertyPercentage_6 = (PropertyAnnualRent6/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_6').val(PropertyPercentage_6.toFixed(2));
		}else{
			var PropertyPercentage_6 = 0;
			$('#PropertyPercentage_6').val(PropertyPercentage_6.toFixed(2));
		}
		if(PropertyAnnualRent7!=0){
			var PropertyPercentage_7 = (PropertyAnnualRent7/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_7').val(PropertyPercentage_7.toFixed(2));
		}else{
			var PropertyPercentage_7 = 0;
			$('#PropertyPercentage_7').val(PropertyPercentage_7.toFixed(2));
		}
		if(PropertyAnnualRent8!=0){
			var PropertyPercentage_8 = (PropertyAnnualRent8/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_8').val(PropertyPercentage_8.toFixed(2));
		}else{
			var PropertyPercentage_8 = 0;
			$('#PropertyPercentage_8').val(PropertyPercentage_8.toFixed(2));
		}
		if(PropertyAnnualRent9!=0){
			var PropertyPercentage_9 = (PropertyAnnualRent9/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_9').val(PropertyPercentage_9.toFixed(2));
		}else{
			var PropertyPercentage_9 = 0;
			$('#PropertyPercentage_9').val(PropertyPercentage_9.toFixed(2));
		}
		if(PropertyAnnualRent10!=0){
			var PropertyPercentage_10 = (PropertyAnnualRent10/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_10').val(PropertyPercentage_10.toFixed(2));
		}else{
			var PropertyPercentage_10 = 0;
			$('#PropertyPercentage_10').val(PropertyPercentage_10.toFixed(2));
		}
		if(PropertyAnnualRent11!=0){
			var PropertyPercentage_11 = (PropertyAnnualRent11/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_11').val(PropertyPercentage_11.toFixed(2));
		}else{
			var PropertyPercentage_11 = 0;
			$('#PropertyPercentage_11').val(PropertyPercentage_11.toFixed(2));
		}
		if(PropertyAnnualRent12!=0){
			var PropertyPercentage_12 = (PropertyAnnualRent12/GrossScheduledIncomeAnnual)*100;
			$('#PropertyPercentage_12').val(PropertyPercentage_12.toFixed(2));
		}else{
			var PropertyPercentage_12 = 0;
			$('#PropertyPercentage_12').val(PropertyPercentage_12.toFixed(2));
		}
		var GrossScheduledIncomePercentage = PropertyPercentage_1+PropertyPercentage_2+PropertyPercentage_3+PropertyPercentage_4+PropertyPercentage_5+PropertyPercentage_6+PropertyPercentage_7+PropertyPercentage_8+PropertyPercentage_9+PropertyPercentage_10+PropertyPercentage_11+PropertyPercentage_12;
		$('#GrossScheduledIncomePercentage').val(GrossScheduledIncomePercentage.toFixed(2));
		
		
		var VacancyLossPercentage = $('#VacancyLossPercentage').val() ? parseFloat($('#VacancyLossPercentage').val()) : 0;
		var VacancyLossMonthly = (GrossScheduledIncomeMonthly*VacancyLossPercentage)/100;											
		var VacancyLossAnnual = VacancyLossMonthly*12;
		$('#VacancyLossMonthly').val(VacancyLossMonthly.toFixed(2));
		$('#VacancyLossAnnual').val(VacancyLossAnnual.toFixed(2));
		
		var ConcessionsParcentage = $('#ConcessionsParcentage').val() ? parseFloat($('#ConcessionsParcentage').val()) : 0;
		var ConcessionsMonthly = (GrossScheduledIncomeMonthly*ConcessionsParcentage)/100;
		var ConcessionsAnnual = ConcessionsMonthly*12;
		$('#ConcessionsMonthly').val(ConcessionsMonthly.toFixed(2));
		$('#ConcessionsAnnual').val(ConcessionsAnnual.toFixed(2));
		
		var ManagementFeePercentage = $('#ManagementFeePercentage').val() ? parseFloat($('#ManagementFeePercentage').val()) : 0;
		var ManagementFeeMonthly = (GrossScheduledIncomeMonthly*ManagementFeePercentage)/100;
		var ManagementFeeAnnual = ManagementFeeMonthly*12;
		$('#ManagementFeeMonthly').val(ManagementFeeMonthly.toFixed(2));
		$('#ManagementFeeAnnual').val(ManagementFeeAnnual.toFixed(2));
		
		var OtherIncomeMonthly = $('#OtherIncomeMonthly').val() ? parseFloat($('#OtherIncomeMonthly').val()) : 0;
		var OtherIncomeAnnual = OtherIncomeMonthly*12;
		$('#OtherIncomeAnnual').val(OtherIncomeAnnual.toFixed(2));
		
		var OperatingIncomeMonthlyTotal = GrossScheduledIncomeMonthly-(VacancyLossMonthly+ConcessionsMonthly+ManagementFeeMonthly)+OtherIncomeMonthly;
		var OperatingIncomeAnnualTotal = OperatingIncomeMonthlyTotal*12;
		$('#OperatingIncomeMonthlyTotal').val(OperatingIncomeMonthlyTotal.toFixed(2));
		$('#OperatingIncomeAnnualTotal').val(OperatingIncomeAnnualTotal.toFixed(2));
		
		var OperatingExpenseMonthly_1 = $('#OperatingExpenseMonthly_1').val() ? parseFloat($('#OperatingExpenseMonthly_1').val()) : 0;
		var OperatingExpenseAnnual_1 = OperatingExpenseMonthly_1*12;
		$('#OperatingExpenseAnnual_1').val(OperatingExpenseAnnual_1.toFixed(2));
		var OperatingExpensePercentage_1 = (OperatingExpenseAnnual_1/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_1 = OperatingExpensePercentage_1 ? parseFloat(OperatingExpensePercentage_1) : 0;
		$('#OperatingExpensePercentage_1').val(OperatingExpensePercentage_1.toFixed(2));
		
		var OperatingExpenseMonthly_2 = $('#OperatingExpenseMonthly_2').val() ? parseFloat($('#OperatingExpenseMonthly_2').val()) : 0;
		var OperatingExpenseAnnual_2 = OperatingExpenseMonthly_2*12;
		$('#OperatingExpenseAnnual_2').val(OperatingExpenseAnnual_2.toFixed(2));
		var OperatingExpensePercentage_2 = (OperatingExpenseAnnual_2/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_2 = OperatingExpensePercentage_2 ? parseFloat(OperatingExpensePercentage_2) : 0;
		$('#OperatingExpensePercentage_2').val(OperatingExpensePercentage_2.toFixed(2));
		
		var OperatingExpenseMonthly_3 = $('#OperatingExpenseMonthly_3').val() ? parseFloat($('#OperatingExpenseMonthly_3').val()) : 0;
		var OperatingExpenseAnnual_3 = OperatingExpenseMonthly_3*12;
		$('#OperatingExpenseAnnual_3').val(OperatingExpenseAnnual_3.toFixed(2));
		var OperatingExpensePercentage_3 = (OperatingExpenseAnnual_3/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_3 = OperatingExpensePercentage_3 ? parseFloat(OperatingExpensePercentage_3) : 0;
		$('#OperatingExpensePercentage_3').val(OperatingExpensePercentage_3.toFixed(2));
		
		var OperatingExpenseMonthly_4 = $('#OperatingExpenseMonthly_4').val() ? parseFloat($('#OperatingExpenseMonthly_4').val()) : 0;
		var OperatingExpenseAnnual_4 = OperatingExpenseMonthly_4*12;
		$('#OperatingExpenseAnnual_4').val(OperatingExpenseAnnual_4.toFixed(2));
		var OperatingExpensePercentage_4 = (OperatingExpenseAnnual_4/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_4 = OperatingExpensePercentage_4 ? parseFloat(OperatingExpensePercentage_4) : 0;
		$('#OperatingExpensePercentage_4').val(OperatingExpensePercentage_4.toFixed(2));
		
		var OperatingExpenseMonthly_5 = $('#OperatingExpenseMonthly_5').val() ? parseFloat($('#OperatingExpenseMonthly_5').val()) : 0;
		var OperatingExpenseAnnual_5 = OperatingExpenseMonthly_5*12;
		$('#OperatingExpenseAnnual_5').val(OperatingExpenseAnnual_5.toFixed(2));
		var OperatingExpensePercentage_5 = (OperatingExpenseAnnual_5/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_5 = OperatingExpensePercentage_5 ? parseFloat(OperatingExpensePercentage_5) : 0;
		$('#OperatingExpensePercentage_5').val(OperatingExpensePercentage_5.toFixed(2));
		
		var OperatingExpenseMonthly_6 = $('#OperatingExpenseMonthly_6').val() ? parseFloat($('#OperatingExpenseMonthly_6').val()) : 0;
		var OperatingExpenseAnnual_6 = OperatingExpenseMonthly_6*12;
		$('#OperatingExpenseAnnual_6').val(OperatingExpenseAnnual_6.toFixed(2));
		var OperatingExpensePercentage_6 = (OperatingExpenseAnnual_6/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_6 = OperatingExpensePercentage_6 ? parseFloat(OperatingExpensePercentage_6) : 0;
		$('#OperatingExpensePercentage_6').val(OperatingExpensePercentage_6.toFixed(2));
		
		var OperatingExpenseMonthly_7 = $('#OperatingExpenseMonthly_7').val() ? parseFloat($('#OperatingExpenseMonthly_7').val()) : 0;
		var OperatingExpenseAnnual_7 = OperatingExpenseMonthly_7*12;
		$('#OperatingExpenseAnnual_7').val(OperatingExpenseAnnual_7.toFixed(2));
		var OperatingExpensePercentage_7 = (OperatingExpenseAnnual_7/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_7 = OperatingExpensePercentage_7 ? parseFloat(OperatingExpensePercentage_7) : 0;
		$('#OperatingExpensePercentage_7').val(OperatingExpensePercentage_7.toFixed(2));
		
		var OperatingExpenseMonthly_8 = $('#OperatingExpenseMonthly_8').val() ? parseFloat($('#OperatingExpenseMonthly_8').val()) : 0;
		var OperatingExpenseAnnual_8 = OperatingExpenseMonthly_8*12;
		$('#OperatingExpenseAnnual_8').val(OperatingExpenseAnnual_8.toFixed(2));
		var OperatingExpensePercentage_8 = (OperatingExpenseAnnual_8/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_8 = OperatingExpensePercentage_8 ? parseFloat(OperatingExpensePercentage_8) : 0;
		$('#OperatingExpensePercentage_8').val(OperatingExpensePercentage_8.toFixed(2));
		
		var OperatingExpenseMonthly_9 = $('#OperatingExpenseMonthly_9').val() ? parseFloat($('#OperatingExpenseMonthly_9').val()) : 0;
		var OperatingExpenseAnnual_9 = OperatingExpenseMonthly_9*12;
		$('#OperatingExpenseAnnual_9').val(OperatingExpenseAnnual_9.toFixed(2));
		var OperatingExpensePercentage_9 = (OperatingExpenseAnnual_9/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_9 = OperatingExpensePercentage_9 ? parseFloat(OperatingExpensePercentage_9) : 0;
		$('#OperatingExpensePercentage_9').val(OperatingExpensePercentage_9.toFixed(2));
		
		var OperatingExpenseMonthly_10 = $('#OperatingExpenseMonthly_10').val() ? parseFloat($('#OperatingExpenseMonthly_10').val()) : 0;
		var OperatingExpenseAnnual_10 = OperatingExpenseMonthly_10*12;
		$('#OperatingExpenseAnnual_10').val(OperatingExpenseAnnual_10.toFixed(2));
		var OperatingExpensePercentage_10 = (OperatingExpenseAnnual_10/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_10 = OperatingExpensePercentage_10 ? parseFloat(OperatingExpensePercentage_10) : 0;
		$('#OperatingExpensePercentage_10').val(OperatingExpensePercentage_10.toFixed(2));
		
		var OperatingExpenseMonthly_11 = $('#OperatingExpenseMonthly_11').val() ? parseFloat($('#OperatingExpenseMonthly_11').val()) : 0;
		var OperatingExpenseAnnual_11 = OperatingExpenseMonthly_11*12;
		$('#OperatingExpenseAnnual_11').val(OperatingExpenseAnnual_11.toFixed(2));
		var OperatingExpensePercentage_11 = (OperatingExpenseAnnual_11/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_11 = OperatingExpensePercentage_11 ? parseFloat(OperatingExpensePercentage_11) : 0;
		$('#OperatingExpensePercentage_11').val(OperatingExpensePercentage_11.toFixed(2));
		
		var OperatingExpenseMonthly_12 = $('#OperatingExpenseMonthly_12').val() ? parseFloat($('#OperatingExpenseMonthly_12').val()) : 0;
		var OperatingExpenseAnnual_12 = OperatingExpenseMonthly_12*12;
		$('#OperatingExpenseAnnual_12').val(OperatingExpenseAnnual_12.toFixed(2));
		var OperatingExpensePercentage_12 = (OperatingExpenseAnnual_12/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_12 = OperatingExpensePercentage_12 ? parseFloat(OperatingExpensePercentage_12) : 0;
		$('#OperatingExpensePercentage_12').val(OperatingExpensePercentage_12.toFixed(2));
		
		var OperatingExpenseMonthly_13 = $('#OperatingExpenseMonthly_13').val() ? parseFloat($('#OperatingExpenseMonthly_13').val()) : 0;
		var OperatingExpenseAnnual_13 = OperatingExpenseMonthly_13*12;
		$('#OperatingExpenseAnnual_13').val(OperatingExpenseAnnual_13.toFixed(2));
		var OperatingExpensePercentage_13 = (OperatingExpenseAnnual_13/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_13 = OperatingExpensePercentage_13 ? parseFloat(OperatingExpensePercentage_13) : 0;
		$('#OperatingExpensePercentage_13').val(OperatingExpensePercentage_13.toFixed(2));
		
		var OperatingExpenseMonthly_14 = $('#OperatingExpenseMonthly_14').val() ? parseFloat($('#OperatingExpenseMonthly_14').val()) : 0;
		var OperatingExpenseAnnual_14 = OperatingExpenseMonthly_14*12;
		$('#OperatingExpenseAnnual_14').val(OperatingExpenseAnnual_14.toFixed(2));
		var OperatingExpensePercentage_14 = (OperatingExpenseAnnual_14/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_14 = OperatingExpensePercentage_14 ? parseFloat(OperatingExpensePercentage_14) : 0;
		$('#OperatingExpensePercentage_14').val(OperatingExpensePercentage_14.toFixed(2));
		
		var OperatingExpenseMonthly_15 = $('#OperatingExpenseMonthly_15').val() ? parseFloat($('#OperatingExpenseMonthly_15').val()) : 0;
		var OperatingExpenseAnnual_15 = OperatingExpenseMonthly_15*12;
		$('#OperatingExpenseAnnual_15').val(OperatingExpenseAnnual_15.toFixed(2));
		var OperatingExpensePercentage_15 = (OperatingExpenseAnnual_15/OperatingIncomeAnnualTotal)*100;
		OperatingExpensePercentage_15 = OperatingExpensePercentage_15 ? parseFloat(OperatingExpensePercentage_15) : 0;
		$('#OperatingExpensePercentage_15').val(OperatingExpensePercentage_15.toFixed(2));
		
		var OperatingExpenseWaterSewerMonthly = $('#OperatingExpenseWaterSewerMonthly').val() ? parseFloat($('#OperatingExpenseWaterSewerMonthly').val()) : 0;
		var OperatingExpenseWaterSewerAnnual = OperatingExpenseWaterSewerMonthly*12;
		$('#OperatingExpenseWaterSewerAnnual').val(OperatingExpenseWaterSewerAnnual.toFixed(2));
		var OperatingExpenseWaterSewerPercentage = (OperatingExpenseWaterSewerAnnual/OperatingIncomeAnnualTotal)*100;
		OperatingExpenseWaterSewerPercentage = OperatingExpenseWaterSewerPercentage ? parseFloat(OperatingExpenseWaterSewerPercentage) : 0;
		$('#OperatingExpenseWaterSewerPercentage').val(OperatingExpenseWaterSewerPercentage.toFixed(2));
		
		
		var OperatingExpenseElectricityMonthly = $('#OperatingExpenseElectricityMonthly').val() ? parseFloat($('#OperatingExpenseElectricityMonthly').val()) : 0;
		var OperatingExpenseElectricityAnnual = OperatingExpenseElectricityMonthly*12;
		$('#OperatingExpenseElectricityAnnual').val(OperatingExpenseElectricityAnnual.toFixed(2));
		var OperatingExpenseElectricityPercentage = (OperatingExpenseElectricityAnnual/OperatingIncomeAnnualTotal)*100;
		OperatingExpenseElectricityPercentage = OperatingExpenseElectricityPercentage ? parseFloat(OperatingExpenseElectricityPercentage) : 0;
		$('#OperatingExpenseElectricityPercentage').val(OperatingExpenseElectricityPercentage.toFixed(2));
		
		var OperatingExpenseGasMonthly = $('#OperatingExpenseGasMonthly').val() ? parseFloat($('#OperatingExpenseGasMonthly').val()) : 0;
		var OperatingExpenseGasAnnual = OperatingExpenseGasMonthly*12;
		$('#OperatingExpenseGasAnnual').val(OperatingExpenseGasAnnual.toFixed(2));
		var OperatingExpenseGasPercentage = (OperatingExpenseGasAnnual/OperatingIncomeAnnualTotal)*100;
		OperatingExpenseGasPercentage = OperatingExpenseGasPercentage ? parseFloat(OperatingExpenseGasPercentage) : 0;
		$('#OperatingExpenseGasPercentage').val(OperatingExpenseGasPercentage.toFixed(2));
		
		var OperatingExpenseFuelOilMonthly = $('#OperatingExpenseFuelOilMonthly').val() ? parseFloat($('#OperatingExpenseFuelOilMonthly').val()) : 0;
		var OperatingExpenseFuelOilAnnual = OperatingExpenseFuelOilMonthly*12;
		$('#OperatingExpenseFuelOilAnnual').val(OperatingExpenseFuelOilAnnual.toFixed(2));
		var OperatingExpenseFuelOilPercentage = (OperatingExpenseFuelOilAnnual/OperatingIncomeAnnualTotal)*100;
		OperatingExpenseFuelOilPercentage = OperatingExpenseFuelOilPercentage ? parseFloat(OperatingExpenseFuelOilPercentage) : 0;
		$('#OperatingExpenseFuelOilPercentage').val(OperatingExpenseFuelOilPercentage.toFixed(2));
		
		var OperatingExpenseOtherUtilitiesMonthly = $('#OperatingExpenseOtherUtilitiesMonthly').val() ? parseFloat($('#OperatingExpenseOtherUtilitiesMonthly').val()) : 0;
		var OperatingExpenseOtherUtilitiesAnnual = OperatingExpenseOtherUtilitiesMonthly*12;
		$('#OperatingExpenseOtherUtilitiesAnnual').val(OperatingExpenseOtherUtilitiesAnnual.toFixed(2));
		var OperatingExpenseOtherUtilitiesPercentage = (OperatingExpenseOtherUtilitiesAnnual/OperatingIncomeAnnualTotal)*100;
		OperatingExpenseOtherUtilitiesPercentage = OperatingExpenseOtherUtilitiesPercentage ? parseFloat(OperatingExpenseOtherUtilitiesPercentage) : 0;
		$('#OperatingExpenseOtherUtilitiesPercentage').val(OperatingExpenseOtherUtilitiesPercentage.toFixed(2));
		
		var OperatingExpenseReservesMonthly = $('#OperatingExpenseReservesMonthly').val() ? parseFloat($('#OperatingExpenseReservesMonthly').val()) : 0;
		var OperatingExpenseReservesAnnual = OperatingExpenseReservesMonthly*12;
		$('#OperatingExpenseReservesAnnual').val(OperatingExpenseReservesAnnual.toFixed(2));
		var OperatingExpenseReservesPercentage = (OperatingExpenseReservesAnnual/OperatingIncomeAnnualTotal)*100;
		OperatingExpenseReservesPercentage = OperatingExpenseReservesPercentage ? parseFloat(OperatingExpenseReservesPercentage) : 0;
		$('#OperatingExpenseReservesPercentage').val(OperatingExpenseReservesPercentage.toFixed(2));
		
		var OperatingExpenseTotalMonthly = OperatingExpenseMonthly_1+OperatingExpenseMonthly_2+OperatingExpenseMonthly_3+OperatingExpenseMonthly_4+OperatingExpenseMonthly_5+OperatingExpenseMonthly_6+OperatingExpenseMonthly_7+OperatingExpenseMonthly_8+OperatingExpenseMonthly_9+OperatingExpenseMonthly_10+OperatingExpenseMonthly_11+OperatingExpenseMonthly_12+OperatingExpenseMonthly_13+OperatingExpenseMonthly_14+OperatingExpenseMonthly_15+OperatingExpenseWaterSewerMonthly+OperatingExpenseElectricityMonthly+OperatingExpenseGasMonthly+OperatingExpenseFuelOilMonthly+OperatingExpenseOtherUtilitiesMonthly+OperatingExpenseReservesMonthly;
		$('#OperatingExpenseTotalMonthly').val(OperatingExpenseTotalMonthly.toFixed(2));
		
		var OperatingExpenseTotalAnnual = OperatingExpenseTotalMonthly*12;
		$('#OperatingExpenseTotalAnnual').val(OperatingExpenseTotalAnnual.toFixed(2));
		
		var OperatingExpenseTotalPercentage = (OperatingExpenseTotalAnnual/OperatingIncomeAnnualTotal)*100;
		OperatingExpenseTotalPercentage = OperatingExpenseTotalPercentage ? parseFloat(OperatingExpenseTotalPercentage) : 0;
		$('#OperatingExpenseTotalPercentage').val(OperatingExpenseTotalPercentage.toFixed(2));
		
		var OperatingExpensesIncomeMonthly = (OperatingExpenseTotalMonthly/OperatingIncomeMonthlyTotal)*100;
		OperatingExpensesIncomeMonthly = OperatingExpensesIncomeMonthly ? parseFloat(OperatingExpensesIncomeMonthly) : 0;
		$('#OperatingExpensesIncomeMonthly').val(OperatingExpensesIncomeMonthly.toFixed(2));
		
		var NetOperatingIncomeMonthly = OperatingIncomeMonthlyTotal-OperatingExpenseTotalMonthly;
		$('#NetOperatingIncomeMonthly').val(NetOperatingIncomeMonthly.toFixed(2));
		
		var NetOperatingIncomeAnnual = NetOperatingIncomeMonthly*12;
		$('#NetOperatingIncomeAnnual').val(NetOperatingIncomeAnnual.toFixed(2));
		
		var NetOperatingIncomePercentage = (NetOperatingIncomeAnnual/OperatingIncomeAnnualTotal)*100;
		NetOperatingIncomePercentage = NetOperatingIncomePercentage ? parseFloat(NetOperatingIncomePercentage) : 0;
		$('#NetOperatingIncomePercentage').val(NetOperatingIncomePercentage.toFixed(2));
		
		if ( $('#OverrideInitialValue').val() == 'Yes' ) {
			//$('#InitialMarketValue').val('');
			$('#InitialMarketValue').val() ? parseFloat($('#InitialMarketValue').val()) : 0;
			$('#InitialMarketValue').removeAttr('readonly');
		} else {
			$('#InitialMarketValue').val('');
			$('#InitialMarketValue').attr('readonly', 'readonly');
		}
		 
		
		//.................................................Extra......................................................//
		var PurchasePrice = $('#PurchasePrice').val() ? parseFloat($('#PurchasePrice').val()) : 0;
		var PropertyClosingDate = $('#PropertyClosingDate').val();
		var PurchaseDate = $('#PurchaseDate').val(PropertyClosingDate);
		
		//.................................................Financial Analysis........................................//
		var UseFinancing = $('#UseFinancing').val();
		var DownPaymentPercent = $('#DownPaymentPercent').val() ? parseFloat($('#DownPaymentPercent').val()) : 0;
		if(UseFinancing == "Yes"){
			var DownPayment = (PurchasePrice*DownPaymentPercent)/100;
		}
		else{
			var DownPayment = PurchasePrice;
		}
		$('#DownPayment').val(DownPayment.toFixed(2));
		
		var FirstMortgageUsed = $('#FirstMortgageUsed').val();
		var SecondMortgageUsed = $('#SecondMortgageUsed').val();
		
		
		var FirstMortgageTerm = $('#FirstMortgageTerm').val() ? parseFloat($('#FirstMortgageTerm').val()) : 0;
		var SecondMortgageTerm = $('#SecondMortgageTerm').val() ? parseFloat($('#SecondMortgageTerm').val()) : 0;
		
		var FirstMortgageLTVPercent = $('#FirstMortgageLTVPercent').val() ? parseFloat($('#FirstMortgageLTVPercent').val()/100) : 0;
		var SecondMortgageLTVPercent = $('#SecondMortgageLTVPercent').val() ? parseFloat($('#SecondMortgageLTVPercent').val()/100) : 0;
		
		var FirstMortgageLoanType = $('#FirstMortgageLoanType').val();
		var SecondMortgageLoanType = $('#SecondMortgageLoanType').val();
		
		var FirstMortgageRate = $('#FirstMortgageRate').val() ? parseFloat($('#FirstMortgageRate').val()/100) : 0;
		var SecondMortgageRate = $('#SecondMortgageRate').val() ? parseFloat($('#SecondMortgageRate').val()/100) : 0;
		
		//..................................................Cash Layout..............................................//
		var RollupFirst = $('#RollupFirst').val();
		var RollUpSecond = $('#RollUpSecond').val();
		var InspectionAppraisal = $('#InspectionAppraisal').val() ? parseFloat($('#InspectionAppraisal').val()) : 0;
		
		var MortgageOneOriginationFeePercent = $('#MortgageOneOriginationFeePercent').val() ? parseFloat($('#MortgageOneOriginationFeePercent').val()/100) : 0;
		var MortgageTwoOriginationFeePercent = $('#MortgageTwoOriginationFeePercent').val() ? parseFloat($('#MortgageTwoOriginationFeePercent').val()/100) : 0;
		
		var MortgageOneOriginationFee = (MortgageOneOriginationFeePercent*FirstMortgageLTVPercent*PurchasePrice);
		var MortgageTwoOriginationFee = MortgageTwoOriginationFeePercent*SecondMortgageLTVPercent*PurchasePrice;
		if(FirstMortgageUsed == "Yes"){
			MortgageOneOriginationFee = MortgageOneOriginationFee*1;
		}else{
			MortgageOneOriginationFee = MortgageOneOriginationFee*0;
		}
		if(SecondMortgageUsed == "Yes"){
			MortgageTwoOriginationFee = MortgageTwoOriginationFee*1;
		}else{
			MortgageTwoOriginationFee = MortgageTwoOriginationFee*0;
		}
		if(UseFinancing == "Yes"){
			MortgageOneOriginationFee = MortgageOneOriginationFee*1;
			MortgageTwoOriginationFee = MortgageTwoOriginationFee*1;
		}else{
			MortgageOneOriginationFee = MortgageOneOriginationFee*0;
			MortgageTwoOriginationFee = MortgageTwoOriginationFee*0;
		}
		$('#MortgageOneOriginationFee').val(MortgageOneOriginationFee.toFixed(2));
		$('#MortgageTwoOriginationFee').val(MortgageTwoOriginationFee.toFixed(2));
		
		
		var MortgageOneDiscountFeePercent = $('#MortgageOneDiscountFeePercent').val() ? parseFloat($('#MortgageOneDiscountFeePercent').val()/100) : 0;
		var MortgageTwoDiscountFeePercent = $('#MortgageTwoDiscountFeePercent').val() ? parseFloat($('#MortgageTwoDiscountFeePercent').val()/100) : 0;
		var MortgageOneDiscountFee = MortgageOneDiscountFeePercent*FirstMortgageLTVPercent*PurchasePrice;
		var MortgageTwoDiscountFee = MortgageTwoDiscountFeePercent*SecondMortgageLTVPercent*PurchasePrice;
		if(FirstMortgageUsed == "Yes"){
			MortgageOneDiscountFee = MortgageOneDiscountFee*1;
		}else{
			MortgageOneDiscountFee = MortgageOneDiscountFee*0;
		}
		if(SecondMortgageUsed == "Yes"){
			MortgageTwoDiscountFee = MortgageTwoDiscountFee*1;
		}else{
			MortgageTwoDiscountFee = MortgageTwoDiscountFee*0;
		}
		if(UseFinancing == "Yes"){
			MortgageOneDiscountFee = MortgageOneDiscountFee*1;
			MortgageTwoDiscountFee = MortgageTwoDiscountFee*1;
		}else{
			MortgageOneDiscountFee = MortgageOneDiscountFee*0;
			MortgageTwoDiscountFee = MortgageTwoDiscountFee*0;
		}
		$('#MortgageOneDiscountFee').val(MortgageOneDiscountFee.toFixed(2));
		$('#MortgageTwoDiscountFee').val(MortgageTwoDiscountFee.toFixed(2));
		
		var ClosingCostsMiscFeePercent = $('#ClosingCostsMiscFeePercent').val() ? parseFloat($('#ClosingCostsMiscFeePercent').val()/100) : 0;
		var ClosingCostsMiscFee = ClosingCostsMiscFeePercent*PurchasePrice;
		$('#ClosingCostsMiscFee').val(ClosingCostsMiscFee.toFixed(2));
		var TotalSettlement = parseFloat(InspectionAppraisal+MortgageOneOriginationFee+MortgageOneDiscountFee+ClosingCostsMiscFee);
		$('#TotalSettlement').val(TotalSettlement.toFixed(2));
		
		var CashbackFromSellerPercent = $('#CashbackFromSellerPercent').val() ? parseFloat($('#CashbackFromSellerPercent').val()/100) : 0;
		var CashbackFromSeller = CashbackFromSellerPercent*PurchasePrice;
		$('#CashbackFromSeller').val(CashbackFromSeller.toFixed(2));
		if(RollupFirst == "No"){
			var hiddencalc1 = 1;
		} else {
			var hiddencalc1 = 0;
		}
		if(RollUpSecond == "No"){
			var hiddencalc2 = 1;
		} else {
			var hiddencalc2 = 0;
		}
		var InitialCapitalReserves_1 = $('#InitialCapitalReserves_1').val() ? parseFloat($('#InitialCapitalReserves_1').val()) : 0;
		var InitialCapitalImprovements = $('#InitialCapitalImprovements').val() ? parseFloat($('#InitialCapitalImprovements').val()) : 0;
		var FeeName = $('#FeeName').val() ? parseFloat($('#FeeName').val()) : 0;
		
		//...................................................Financial Analysis Amount Calculation..........................//
		if(RollupFirst == "No"){
			var FirstMortgageAmount = FirstMortgageLTVPercent*PurchasePrice;
		}else{
			var FirstMortgageAmount = (FirstMortgageLTVPercent*PurchasePrice)+MortgageOneOriginationFee+MortgageOneDiscountFee;
		}
		if(FirstMortgageUsed == "Yes"){
			FirstMortgageAmount = FirstMortgageAmount*1;
		}else{
			FirstMortgageAmount = FirstMortgageAmount*0;
		}
		if(RollUpSecond == "No"){
			var SecondMortgageAmount = SecondMortgageLTVPercent*PurchasePrice;
		}else{
			var SecondMortgageAmount = (SecondMortgageLTVPercent*PurchasePrice)+MortgageTwoOriginationFee+MortgageTwoDiscountFee;
		}
		if(SecondMortgageUsed == "Yes"){
			SecondMortgageAmount = SecondMortgageAmount*1;
		}else{
			SecondMortgageAmount = SecondMortgageAmount*0;
		}
		if(UseFinancing == "Yes"){
			FirstMortgageAmount = FirstMortgageAmount*1;
			SecondMortgageAmount = SecondMortgageAmount*1;
		}else{
			FirstMortgageAmount = FirstMortgageAmount*0;
			SecondMortgageAmount = SecondMortgageAmount*0;
		}

		$('#FirstMortgageAmount').val(FirstMortgageAmount.toFixed(2));
		$('#SecondMortgageAmount').val(SecondMortgageAmount.toFixed(2));
		
		if(FirstMortgageLoanType == "Fixed"){
			var FirstMortgagePayment = PMT(FirstMortgageRate/12,FirstMortgageTerm*12,FirstMortgageAmount, 0, 0);
		} else {
			var FirstMortgagePayment = -FirstMortgageAmount*(FirstMortgageRate/12);
		}
		FirstMortgagePayment = FirstMortgagePayment ? parseFloat(FirstMortgagePayment) : 0;
		$('#FirstMortgagePayment').val(FirstMortgagePayment.toFixed(2));
		
		if(SecondMortgageLoanType == "Fixed"){
			var SecondMortgagePayment = PMT(SecondMortgageRate/12,SecondMortgageTerm*12,SecondMortgageAmount, 0, 0);
		} else {
			var SecondMortgagePayment = -SecondMortgageAmount*(SecondMortgageRate/12);
		}
		SecondMortgagePayment = SecondMortgagePayment ? parseFloat(SecondMortgagePayment) : 0;
		$('#SecondMortgagePayment').val(SecondMortgagePayment.toFixed(2));
		
		var FirstMortgagePaymentTotal = FirstMortgagePayment+SecondMortgagePayment;
		FirstMortgagePaymentTotal = FirstMortgagePaymentTotal ? parseFloat(FirstMortgagePaymentTotal) : 0;
		$('#FirstMortgagePaymentTotal').val(FirstMortgagePaymentTotal.toFixed(2));
		
		
		
		
		var TotalLoanAmount = FirstMortgageAmount+SecondMortgageAmount;
		$('#TotalLoanAmount').val(TotalLoanAmount.toFixed(2));
		
		var InterestRate = (DownPaymentPercent == 100 ? 0 : ( FirstMortgageLTVPercent * FirstMortgageRate * (FirstMortgageUsed == "Yes" ? 1 : 0) +(SecondMortgageUsed == "Yes" ? 1 : 0) * SecondMortgageRate*SecondMortgageLTVPercent )/(FirstMortgageLTVPercent*(FirstMortgageUsed == "Yes" ? 1 : 0)+SecondMortgageLTVPercent*(SecondMortgageUsed == "Yes" ? 1 : 0)));
		 
		 InterestRate = InterestRate ? InterestRate*100 : 0;
		$('#InterestRate').val(InterestRate.toFixed(2));
		
		
		 
		
		
		//---------------------------------------------------------outlay calculation------------------------------------//
		 var TotalCashOutlay = DownPayment+InspectionAppraisal+ClosingCostsMiscFee+(MortgageOneOriginationFee+MortgageOneDiscountFee)*hiddencalc1+(MortgageTwoOriginationFee+MortgageTwoDiscountFee)*hiddencalc2-CashbackFromSeller+InitialCapitalReserves_1+InitialCapitalImprovements+FeeName;
		$('#TotalCashOutlay').val(TotalCashOutlay.toFixed(2));
		
		var FirstYearMonthlyCashflow = NetOperatingIncomeMonthly + FirstMortgagePaymentTotal;
		var AnnualCashflow = FirstYearMonthlyCashflow * 12;
		var AnnualCashOnCash = AnnualCashflow/TotalCashOutlay
		var FirstYearAnnualCOCReturn = (TotalCashOutlay == 0 ? "N/A" : parseFloat(AnnualCashOnCash*100));
		
		FirstYearAnnualCOCReturn = (FirstYearAnnualCOCReturn != 'N/A' ? FirstYearAnnualCOCReturn.toFixed(2) : 'N/A');
		$('#FirstYearMonthlyCashflow').val(FirstYearMonthlyCashflow.toFixed(2));
		$('#FirstYearAnnualCOCReturn').val(FirstYearAnnualCOCReturn);
		
		
		//********************************************************Helpful Calculator************************************//
		
		var MortgagePayments = FirstMortgagePaymentTotal ? parseFloat( FirstMortgagePaymentTotal) : 0;
		MortgagePayments = Math.abs(MortgagePayments);
		$('#MortgagePayments').val(MortgagePayments.toFixed(2));
		
		var Insurance = parseFloat(OperatingExpenseMonthly_3);
		$('#Insurance').val(Insurance.toFixed(2));
		
		var RealEstateTaxes = parseFloat(OperatingExpenseMonthly_10);
		$('#RealEstateTaxes').val(RealEstateTaxes.toFixed(2));
		
		var HelpfulTotal = MortgagePayments+Insurance+RealEstateTaxes;
		$("#HelpfulTotal").val(HelpfulTotal.toFixed(2));

		var MonthsReserve = $('#MonthsReserve').val() ? parseFloat($('#MonthsReserve').val()) : 0;
		
		var AmountHold = HelpfulTotal*MonthsReserve;
		$('#AmountHold').val(AmountHold.toFixed(2));
		
		var ExpectedHoldingPeriod = $('#ExpectedHoldingPeriod').val() ? parseFloat($('#ExpectedHoldingPeriod').val()) : 0;
		var OptionOneRate = $('#OptionOneRate').val() ? parseFloat($('#OptionOneRate').val()/100) : 0;
		var OptionTwoRate = $('#OptionTwoRate').val() ? parseFloat($('#OptionTwoRate').val()/100) : 0;
		var OptionThreeRate = $('#OptionThreeRate').val() ? parseFloat($('#OptionThreeRate').val()/100) : 0;
		
		var OptionOnePoints = $('#OptionOnePoints').val() ? parseFloat($('#OptionOnePoints').val()) : 0;
		var OptionTwoPoints = $('#OptionTwoPoints').val() ? parseFloat($('#OptionTwoPoints').val()) : 0;
		var OptionThreePoints = $('#OptionThreePoints').val() ? parseFloat($('#OptionThreePoints').val()) : 0;
		
		var OptionOneFactor = OptionOneRate*ExpectedHoldingPeriod+OptionOnePoints/100;
		var OptionTwoFactor = OptionTwoRate*ExpectedHoldingPeriod+OptionTwoPoints/100;
		var OptionThreeFactor = OptionThreeRate*ExpectedHoldingPeriod+OptionThreePoints/100;
		
		$('#OptionOneFactor').val(OptionOneFactor.toFixed(2));
		$('#OptionTwoFactor').val(OptionTwoFactor.toFixed(2));
		$('#OptionThreeFactor').val(OptionThreeFactor.toFixed(2));
		
		var arr = [OptionOneFactor, OptionTwoFactor, OptionThreeFactor];
		var sorted = arr.slice().sort(function(a,b){return b-a})
		var ranks = arr.slice().map(function(v){ return sorted.indexOf(v)+1 });
		 
		var org_val = ranks.reverse();
		
		
		var OptionOneRank = org_val[0];
		var OptionTwoRank = org_val[1];
		var OptionThreeRank = org_val[2];
		
		$('#OptionOneRank').val(OptionOneRank);
		$('#OptionTwoRank').val(OptionTwoRank);
		$('#OptionThreeRank').val(OptionThreeRank);
		
		//*************************************Rental Calculation Modal***********************************************//
		$('#IndividualRent_Year_1_1').val(PropertyMonthlyRent1.toFixed(2));
		$('#IndividualRent_Year_2_1').val(PropertyMonthlyRent2.toFixed(2));
		$('#IndividualRent_Year_3_1').val(PropertyMonthlyRent3.toFixed(2));
		$('#IndividualRent_Year_4_1').val(PropertyMonthlyRent4.toFixed(2));
		$('#IndividualRent_Year_5_1').val(PropertyMonthlyRent5.toFixed(2));
		$('#IndividualRent_Year_6_1').val(PropertyMonthlyRent6.toFixed(2));
		$('#IndividualRent_Year_7_1').val(PropertyMonthlyRent7.toFixed(2));
		$('#IndividualRent_Year_8_1').val(PropertyMonthlyRent8.toFixed(2));
		$('#IndividualRent_Year_9_1').val(PropertyMonthlyRent9.toFixed(2));
		$('#IndividualRent_Year_10_1').val(PropertyMonthlyRent10.toFixed(2));
		$('#IndividualRent_Year_11_1').val(PropertyMonthlyRent11.toFixed(2));
		$('#IndividualRent_Year_12_1').val(PropertyMonthlyRent12.toFixed(2));
		for(var j=1; j<41; j++){	
			var IndividualRent_Total_j = 0;
			for(var i=1;i<13;i++){
					var IndividualRent_Year_i_j = $('#IndividualRent_Year_'+i+'_'+j+'').val() ? parseFloat($('#IndividualRent_Year_'+i+'_'+j+'').val()) : 0;
					if(IndividualRent_Year_i_j != 0){
					var IndividualRent_Total_j = IndividualRent_Total_j+IndividualRent_Year_i_j;
					$('#IndividualRent_Total_'+j+'').val(IndividualRent_Total_j.toFixed(2));
				}	
			}
		}
		$('#OverrideVacancyRates_Year_1').val(VacancyLossPercentage.toFixed(2));
		$('#OverrideConcessions_Year_1').val(ConcessionsParcentage.toFixed(2));
		

		$("#TotalCashOutlay_0").val((-TotalCashOutlay).toFixed(2));
		for ( var i=1; i<31; i++) {
			var CapitalImprovementSchedule_i = $('#CapitalImprovementSchedule_' + i).val() ? parseFloat($('#CapitalImprovementSchedule_' + i).val()) : 0;
			if ( i == 1 ) {
				var TotalCashOutlay_i = (-TotalCashOutlay) - CapitalImprovementSchedule_i;
			} else {
				var TotalCashOutlayAdvResult = $('#TotalCashOutlay_' + (i-1)).val() ? parseFloat($('#TotalCashOutlay_' + (i-1)).val()) : 0;
				var TotalCashOutlay_i = TotalCashOutlayAdvResult - CapitalImprovementSchedule_i;
				if ( i == 30 ) {
					var TotalCashOutlay_30 = TotalCashOutlayAdvResult - CapitalImprovementSchedule_i;
				}
			}
			$('#TotalCashOutlay_' + i).val(TotalCashOutlay_i.toFixed(2));
		}
		
		for ( var i=31; i<41; i++) {
			$('#TotalCashOutlay_' + i).val(TotalCashOutlay_30.toFixed(2));
		}
		
		var RentGrowthOveride = $('#RentGrowthOveride').val(); 
		var RentGrowthRate = $('#RentGrowthRate').val() ? parseFloat($('#RentGrowthRate').val()/100) : 0; 
		for ( var j=1; j<13; j++ ) {
			var PropertyAnnualGrowthRate_j = $('#PropertyAnnualGrowthRate_'+j).val() ? parseFloat($('#PropertyAnnualGrowthRate_'+j).val()/100) : 0; 
			for ( var i=13; i<41; i++ ) {
				var result = $('#IndividualRent_Year_' + j + '_' + (i-1)).val() ? parseFloat($('#IndividualRent_Year_' + j + '_' + (i-1)).val()) : 0;
				var IndividualRent_Year_Result = (RentGrowthOveride == "Yes" ? result*(1+RentGrowthRate) : result*(1+PropertyAnnualGrowthRate_j));
				$('#IndividualRent_Year_' + j + '_' + i).val(IndividualRent_Year_Result.toFixed(2));
			}
		}
		
	});
});