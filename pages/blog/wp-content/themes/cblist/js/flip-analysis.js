jQuery(document).ready(function($) {
    "use strict";
	
	// Bid 1 Calculation Event
	$('#FlipTable .Bid1InputBox').on('change blur focus', function() {
        FlipTotalBid1TotalCalculation();
    });
	// Bid 2 Calculation Event
	$('#FlipTable .Bid2InputBox').on('change blur focus', function() {
        FlipTotalBid2TotalCalculation();
    });
	// Bid 3 Calculation Event
	$('#FlipTable .Bid3InputBox').on('change blur focus', function() {
        FlipTotalBid3TotalCalculation();
    });
	// Total Budget Calculation Event
	$('#FlipTable .BudgetInputBox').on('change blur focus', function() {
		var Parent = $(this).parents('tr');
		var Budget = parseFloat($(this).val());
		var Month = parseFloat(Parent.children('td:eq(9)').find('select').val());
		if ( Budget > 0 ) {
			if ( Month == 0 ) {
				Parent.children('td:eq(10)').text('Enter Moth to Pay!');
			} else {
				Parent.children('td:eq(10)').text('');
			}
		} else {
			Parent.children('td:eq(10)').text('');
		}
		
		// Total Budget Calculation
		var BudgetDemo 						= $('#FlipTable #BudgetDemo').val() 					? parseFloat($('#FlipTable #BudgetDemo').val()) 					: 0;
		var BudgetArchitectural				= $('#FlipTable #BudgetArchitectural').val() 			? parseFloat($('#FlipTable #BudgetArchitectural').val()) 			: 0;
		var BudgetMasterBuildingPermit		= $('#FlipTable #BudgetMasterBuildingPermit').val() 	? parseFloat($('#FlipTable #BudgetMasterBuildingPermit').val()) 	: 0;
		var BudgetPlumbing					= $('#FlipTable #BudgetPlumbing').val() 				? parseFloat($('#FlipTable #BudgetPlumbing').val()) 				: 0;
		var BudgetElectrical				= $('#FlipTable #BudgetElectrical').val() 				? parseFloat($('#FlipTable #BudgetElectrical').val()) 				: 0;
		var BudgetHVAC						= $('#FlipTable #BudgetHVAC').val() 					? parseFloat($('#FlipTable #BudgetHVAC').val()) 					: 0;
		var BudgetDumpster					= $('#FlipTable #BudgetDumpster').val() 				? parseFloat($('#FlipTable #BudgetDumpster').val()) 				: 0;
		var BudgetOther						= $('#FlipTable #BudgetOther').val() 					? parseFloat($('#FlipTable #BudgetOther').val()) 					: 0;
		
		var BudgetWallFraming				= $('#FlipTable #BudgetWallFraming').val() 				? parseFloat($('#FlipTable #BudgetWallFraming').val()) 			: 0;
		var BudgetFloorFraming				= $('#FlipTable #BudgetFloorFraming').val() 			? parseFloat($('#FlipTable #BudgetFloorFraming').val()) 			: 0;
		var BudgetCeilingFraming			= $('#FlipTable #BudgetCeilingFraming').val() 			? parseFloat($('#FlipTable #BudgetCeilingFraming').val()) 			: 0;
		var BudgetInteriorElectrical		= $('#FlipTable #BudgetInteriorElectrical').val() 		? parseFloat($('#FlipTable #BudgetInteriorElectrical').val()) 		: 0;
		var BudgetInteriorPlumbing			= $('#FlipTable #BudgetInteriorPlumbing').val() 		? parseFloat($('#FlipTable #BudgetInteriorPlumbing').val()) 		: 0;
		var BudgetInteriorHVAC				= $('#FlipTable #BudgetInteriorHVAC').val() 			? parseFloat($('#FlipTable #BudgetInteriorHVAC').val()) 			: 0;
		var BudgetFlooring					= $('#FlipTable #BudgetFlooring').val() 				? parseFloat($('#FlipTable #BudgetFlooring').val()) 				: 0;
		var BudgetSheetrock					= $('#FlipTable #BudgetSheetrock').val() 				? parseFloat($('#FlipTable #BudgetSheetrock').val()) 				: 0;
		var BudgetWindows					= $('#FlipTable #BudgetWindows').val() 					? parseFloat($('#FlipTable #BudgetWindows').val()) 				: 0;
		var BudgetInteriorDoors				= $('#FlipTable #BudgetInteriorDoors').val() 			? parseFloat($('#FlipTable #BudgetInteriorDoors').val()) 			: 0;
		var BudgetTrim						= $('#FlipTable #BudgetTrim').val() 					? parseFloat($('#FlipTable #BudgetTrim').val()) 					: 0;
		var BudgetBathroomVanities			= $('#FlipTable #BudgetBathroomVanities').val() 		? parseFloat($('#FlipTable #BudgetBathroomVanities').val()) 		: 0;
		var BudgetBathroomFixtures			= $('#FlipTable #BudgetBathroomFixtures').val() 		? parseFloat($('#FlipTable #BudgetBathroomFixtures').val()) 		: 0;
		var BudgetKitchenCabinets			= $('#FlipTable #BudgetKitchenCabinets').val() 			? parseFloat($('#FlipTable #BudgetKitchenCabinets').val()) 		: 0;
		var BudgetLaborInstallKitchen		= $('#FlipTable #BudgetLaborInstallKitchen').val() 		? parseFloat($('#FlipTable #BudgetLaborInstallKitchen').val()) 	: 0;
		var BudgetFloorCoverings1			= $('#FlipTable #BudgetFloorCoverings1').val() 			? parseFloat($('#FlipTable #BudgetFloorCoverings1').val()) 		: 0;
		var BudgetFloorCoverings2			= $('#FlipTable #BudgetFloorCoverings2').val() 			? parseFloat($('#FlipTable #BudgetFloorCoverings2').val()) 		: 0;
		var BudgetFloorCoverings3			= $('#FlipTable #BudgetFloorCoverings3').val() 			? parseFloat($('#FlipTable #BudgetFloorCoverings3').val()) 		: 0;
		var BudgetInteriorPainting			= $('#FlipTable #BudgetInteriorPainting').val() 		? parseFloat($('#FlipTable #BudgetInteriorPainting').val()) 		: 0;
		var BudgetLightFixtures				= $('#FlipTable #BudgetLightFixtures').val() 			? parseFloat($('#FlipTable #BudgetLightFixtures').val()) 			: 0;
		var BudgetOtherFixtures				= $('#FlipTable #BudgetOtherFixtures').val() 			? parseFloat($('#FlipTable #BudgetOtherFixtures').val()) 			: 0;
		var BudgetAppliances				= $('#FlipTable #BudgetAppliances').val() 				? parseFloat($('#FlipTable #BudgetAppliances').val()) 				: 0;
		var BudgetOtherInterior				= $('#FlipTable #BudgetOtherInterior').val() 			? parseFloat($('#FlipTable #BudgetOtherInterior').val()) 			: 0;
		
		var BudgetExteriorTrim				= $('#FlipTable #BudgetExteriorTrim').val() 			? parseFloat($('#FlipTable #BudgetExteriorTrim').val()) 			: 0;
		var BudgetExteriorDoors				= $('#FlipTable #BudgetExteriorDoors').val()			? parseFloat($('#FlipTable #BudgetExteriorDoors').val()) 			: 0;
		var BudgetPorches					= $('#FlipTable #BudgetPorches').val() 					? parseFloat($('#FlipTable #BudgetPorches').val()) 				: 0;
		var BudgetSiding					= $('#FlipTable #BudgetSiding').val() 					? parseFloat($('#FlipTable #BudgetSiding').val()) 					: 0;
		var BudgetExteriorPainting			= $('#FlipTable #BudgetExteriorPainting').val() 		? parseFloat($('#FlipTable #BudgetExteriorPainting').val()) 		: 0;
		var BudgetRoof						= $('#FlipTable #BudgetRoof').val() 					? parseFloat($('#FlipTable #BudgetRoof').val()) 					: 0;
		var BudgetGuttersDownspouts			= $('#FlipTable #BudgetGuttersDownspouts').val() 		? parseFloat($('#FlipTable #BudgetGuttersDownspouts').val()) 		: 0;
		var BudgetFencing					= $('#FlipTable #BudgetFencing').val() 					? parseFloat($('#FlipTable #BudgetFencing').val()) 				: 0;
		var BudgetLandscaping				= $('#FlipTable #BudgetLandscaping').val() 				? parseFloat($('#FlipTable #BudgetLandscaping').val()) 			: 0;
		var BudgetDrivewayConcrete			= $('#FlipTable #BudgetDrivewayConcrete').val() 		? parseFloat($('#FlipTable #BudgetDrivewayConcrete').val()) 		: 0;
		var BudgetFoudationWork				= $('#FlipTable #BudgetFoudationWork').val() 			? parseFloat($('#FlipTable #BudgetFoudationWork').val()) 			: 0;
		var BudgetBrickPointingReplacement	= $('#FlipTable #BudgetBrickPointingReplacement').val()	? parseFloat($('#FlipTable #BudgetBrickPointingReplacement').val()): 0;
		var BudgetOtherExterior				= $('#FlipTable #BudgetOtherExterior').val() 			? parseFloat($('#FlipTable #BudgetOtherExterior').val()) 			: 0;
		
		var BudgetContingency				= $('#FlipTable #BudgetContingency').val() 				? parseFloat($('#FlipTable #BudgetContingency').val()) 			: 0;
		var BudgetGCFee						= $('#FlipTable #BudgetGCFee').val() 					? parseFloat($('#FlipTable #BudgetGCFee').val()) 					: 0;
		var BudgetCleanup					= $('#FlipTable #BudgetCleanup').val() 					? parseFloat($('#FlipTable #BudgetCleanup').val()) 				: 0;
		var BudgetOtherOther				= $('#FlipTable #BudgetOtherOther').val() 				? parseFloat($('#FlipTable #BudgetOtherOther').val()) 				: 0;
		
		var TotalBudgetTotal = BudgetDemo + BudgetArchitectural + BudgetMasterBuildingPermit + BudgetPlumbing + BudgetElectrical + BudgetHVAC + BudgetDumpster + BudgetOther + BudgetWallFraming + BudgetFloorFraming + BudgetCeilingFraming + BudgetInteriorElectrical + BudgetInteriorPlumbing + BudgetInteriorHVAC + BudgetFlooring + BudgetSheetrock + BudgetWindows + BudgetInteriorDoors + BudgetTrim + BudgetBathroomVanities + BudgetBathroomFixtures + BudgetKitchenCabinets + BudgetLaborInstallKitchen + BudgetFloorCoverings1 + BudgetFloorCoverings2 + BudgetFloorCoverings3 + BudgetInteriorPainting + BudgetLightFixtures + BudgetOtherFixtures + BudgetAppliances + BudgetOtherInterior + BudgetExteriorTrim + BudgetExteriorDoors + BudgetPorches + BudgetSiding + BudgetExteriorPainting + BudgetRoof + BudgetGuttersDownspouts + BudgetFencing + BudgetLandscaping + BudgetDrivewayConcrete + BudgetFoudationWork + BudgetBrickPointingReplacement + BudgetOtherExterior + BudgetContingency + BudgetGCFee + BudgetCleanup + BudgetOtherOther;
		
		TotalBudgetTotal = TotalBudgetTotal ? parseFloat(TotalBudgetTotal) : 0;
		$('#FlipTable #DetailedFlipBudget').val(TotalBudgetTotal.toFixed(2));

	});
		
});

// Bid 1 Calculation
function FlipTotalBid1TotalCalculation() {
	var Bid1Demo 						= $('#FlipTable #Bid1Demo').val() 						? parseFloat($('#FlipTable #Bid1Demo').val()) 						: 0;
	var Bid1Architectural				= $('#FlipTable #Bid1Architectural').val() 			? parseFloat($('#FlipTable #Bid1Architectural').val()) 			: 0;
	var Bid1MasterBuildingPermit		= $('#FlipTable #Bid1MasterBuildingPermit').val() 		? parseFloat($('#FlipTable #Bid1MasterBuildingPermit').val()) 		: 0;
	var Bid1Plumbing					= $('#FlipTable #Bid1Plumbing').val() 					? parseFloat($('#FlipTable #Bid1Plumbing').val()) 					: 0;
	var Bid1Electrical					= $('#FlipTable #Bid1Electrical').val() 				? parseFloat($('#FlipTable #Bid1Electrical').val()) 				: 0;
	var Bid1HVAC						= $('#FlipTable #Bid1HVAC').val() 						? parseFloat($('#FlipTable #Bid1HVAC').val()) 						: 0;
	var Bid1Dumpster					= $('#FlipTable #Bid1Dumpster').val() 					? parseFloat($('#FlipTable #Bid1Dumpster').val()) 					: 0;
	var Bid1Other						= $('#FlipTable #Bid1Other').val() 					? parseFloat($('#FlipTable #Bid1Other').val()) 					: 0;
	
	var Bid1WallFraming					= $('#FlipTable #Bid1WallFraming').val() 				? parseFloat($('#FlipTable #Bid1WallFraming').val()) 				: 0;
	var Bid1FloorFraming				= $('#FlipTable #Bid1FloorFraming').val() 				? parseFloat($('#FlipTable #Bid1FloorFraming').val()) 				: 0;
	var Bid1CeilingFraming				= $('#FlipTable #Bid1CeilingFraming').val() 			? parseFloat($('#FlipTable #Bid1CeilingFraming').val()) 			: 0;
	var Bid1InteriorElectrical			= $('#FlipTable #Bid1InteriorElectrical').val() 		? parseFloat($('#FlipTable #Bid1InteriorElectrical').val()) 		: 0;
	var Bid1InteriorPlumbing			= $('#FlipTable #Bid1InteriorPlumbing').val() 			? parseFloat($('#FlipTable #Bid1InteriorPlumbing').val()) 			: 0;
	var Bid1InteriorHVAC				= $('#FlipTable #Bid1InteriorHVAC').val() 				? parseFloat($('#FlipTable #Bid1InteriorHVAC').val()) 				: 0;
	var Bid1Flooring					= $('#FlipTable #Bid1Flooring').val() 					? parseFloat($('#FlipTable #Bid1Flooring').val()) 					: 0;
	var Bid1Sheetrock					= $('#FlipTable #Bid1Sheetrock').val() 				? parseFloat($('#FlipTable #Bid1Sheetrock').val()) 				: 0;
	var Bid1Windows						= $('#FlipTable #Bid1Windows').val() 					? parseFloat($('#FlipTable #Bid1Windows').val()) 					: 0;
	var Bid1InteriorDoors				= $('#FlipTable #Bid1InteriorDoors').val() 			? parseFloat($('#FlipTable #Bid1InteriorDoors').val()) 			: 0;
	var Bid1Trim						= $('#FlipTable #Bid1Trim').val() 						? parseFloat($('#FlipTable #Bid1Trim').val()) 						: 0;
	var Bid1BathroomVanities			= $('#FlipTable #Bid1BathroomVanities').val() 			? parseFloat($('#FlipTable #Bid1BathroomVanities').val()) 			: 0;
	var Bid1BathroomFixtures			= $('#FlipTable #Bid1BathroomFixtures').val() 			? parseFloat($('#FlipTable #Bid1BathroomFixtures').val()) 			: 0;
	var Bid1KitchenCabinets				= $('#FlipTable #Bid1KitchenCabinets').val() 			? parseFloat($('#FlipTable #Bid1KitchenCabinets').val()) 			: 0;
	var Bid1LaborInstallKitchen			= $('#FlipTable #Bid1LaborInstallKitchen').val() 		? parseFloat($('#FlipTable #Bid1LaborInstallKitchen').val()) 		: 0;
	var Bid1FloorCoverings1				= $('#FlipTable #Bid1FloorCoverings1').val() 			? parseFloat($('#FlipTable #Bid1FloorCoverings1').val()) 			: 0;
	var Bid1FloorCoverings2				= $('#FlipTable #Bid1FloorCoverings2').val() 			? parseFloat($('#FlipTable #Bid1FloorCoverings2').val()) 			: 0;
	var Bid1FloorCoverings3				= $('#FlipTable #Bid1FloorCoverings3').val() 			? parseFloat($('#FlipTable #Bid1FloorCoverings3').val()) 			: 0;
	var Bid1InteriorPainting			= $('#FlipTable #Bid1InteriorPainting').val() 			? parseFloat($('#FlipTable #Bid1InteriorPainting').val()) 			: 0;
	var Bid1LightFixtures				= $('#FlipTable #Bid1LightFixtures').val() 			? parseFloat($('#FlipTable #Bid1LightFixtures').val()) 			: 0;
	var Bid1OtherFixtures				= $('#FlipTable #Bid1OtherFixtures').val() 			? parseFloat($('#FlipTable #Bid1OtherFixtures').val()) 			: 0;
	var Bid1Appliances					= $('#FlipTable #Bid1Appliances').val() 				? parseFloat($('#FlipTable #Bid1Appliances').val()) 				: 0;
	var Bid1OtherInterior				= $('#FlipTable #Bid1OtherInterior').val() 			? parseFloat($('#FlipTable #Bid1OtherInterior').val()) 			: 0;
	
	var Bid1ExteriorTrim				= $('#FlipTable #Bid1ExteriorTrim').val() 				? parseFloat($('#FlipTable #Bid1ExteriorTrim').val()) 				: 0;
	var Bid1ExteriorDoors				= $('#FlipTable #Bid1ExteriorDoors').val()			 	? parseFloat($('#FlipTable #Bid1ExteriorDoors').val()) 			: 0;
	var Bid1Porches						= $('#FlipTable #Bid1Porches').val() 					? parseFloat($('#FlipTable #Bid1Porches').val()) 					: 0;
	var Bid1Siding						= $('#FlipTable #Bid1Siding').val() 					? parseFloat($('#FlipTable #Bid1Siding').val()) 					: 0;
	var Bid1ExteriorPainting			= $('#FlipTable #Bid1ExteriorPainting').val() 			? parseFloat($('#FlipTable #Bid1ExteriorPainting').val()) 			: 0;
	var Bid1Roof						= $('#FlipTable #Bid1Roof').val() 						? parseFloat($('#FlipTable #Bid1Roof').val()) 						: 0;
	var Bid1GuttersDownspouts			= $('#FlipTable #Bid1GuttersDownspouts').val() 		? parseFloat($('#FlipTable #Bid1GuttersDownspouts').val()) 		: 0;
	var Bid1Fencing						= $('#FlipTable #Bid1Fencing').val() 					? parseFloat($('#FlipTable #Bid1Fencing').val()) 					: 0;
	var Bid1Landscaping					= $('#FlipTable #Bid1Landscaping').val() 				? parseFloat($('#FlipTable #Bid1Landscaping').val()) 				: 0;
	var Bid1DrivewayConcrete			= $('#FlipTable #Bid1DrivewayConcrete').val() 			? parseFloat($('#FlipTable #Bid1DrivewayConcrete').val()) 			: 0;
	var Bid1FoudationWork				= $('#FlipTable #Bid1FoudationWork').val() 			? parseFloat($('#FlipTable #Bid1FoudationWork').val()) 			: 0;
	var Bid1BrickPointingReplacement	= $('#FlipTable #Bid1BrickPointingReplacement').val() 	? parseFloat($('#FlipTable #Bid1BrickPointingReplacement').val()) 	: 0;
	var Bid1OtherExterior				= $('#FlipTable #Bid1OtherExterior').val() 			? parseFloat($('#FlipTable #Bid1OtherExterior').val()) 			: 0;
	
	var Bid1Contingency					= $('#FlipTable #Bid1Contingency').val() 				? parseFloat($('#FlipTable #Bid1Contingency').val()) 				: 0;
	var Bid1GCFee						= $('#FlipTable #Bid1GCFee').val() 					? parseFloat($('#FlipTable #Bid1GCFee').val()) 					: 0;
	var Bid1Cleanup						= $('#FlipTable #Bid1Cleanup').val() 					? parseFloat($('#FlipTable #Bid1Cleanup').val()) 					: 0;
	var Bid1OtherOther					= $('#FlipTable #Bid1OtherOther').val() 				? parseFloat($('#FlipTable #Bid1OtherOther').val()) 				: 0;
	
	var TotalBid1Total = Bid1Demo + Bid1Architectural + Bid1MasterBuildingPermit + Bid1Plumbing + Bid1Electrical + Bid1HVAC + Bid1Dumpster + Bid1Other + Bid1WallFraming + Bid1FloorFraming + Bid1CeilingFraming + Bid1InteriorElectrical + Bid1InteriorPlumbing + Bid1InteriorHVAC + Bid1Flooring + Bid1Sheetrock + Bid1Windows + Bid1InteriorDoors + Bid1Trim + Bid1BathroomVanities + Bid1BathroomFixtures + Bid1KitchenCabinets + Bid1LaborInstallKitchen + Bid1FloorCoverings1 + Bid1FloorCoverings2 + Bid1FloorCoverings3 + Bid1InteriorPainting + Bid1LightFixtures + Bid1OtherFixtures + Bid1Appliances + Bid1OtherInterior + Bid1ExteriorTrim + Bid1ExteriorDoors + Bid1Porches + Bid1Siding + Bid1ExteriorPainting + Bid1Roof + Bid1GuttersDownspouts + Bid1Fencing + Bid1Landscaping + Bid1DrivewayConcrete + Bid1FoudationWork + Bid1BrickPointingReplacement + Bid1OtherExterior + Bid1Contingency + Bid1GCFee + Bid1Cleanup + Bid1OtherOther;
	
	TotalBid1Total = TotalBid1Total ? TotalBid1Total : 0;
	$('#FlipTable #TotalBid1Total').val(TotalBid1Total.toFixed(2));
}

// Bid 2 Calculation
function FlipTotalBid2TotalCalculation() {
	var Bid2Demo 						= $('#FlipTable #Bid2Demo').val() 						? parseFloat($('#FlipTable #Bid2Demo').val()) 						: 0;
	var Bid2Architectural				= $('#FlipTable #Bid2Architectural').val() 			? parseFloat($('#FlipTable #Bid2Architectural').val()) 			: 0;
	var Bid2MasterBuildingPermit		= $('#FlipTable #Bid2MasterBuildingPermit').val() 		? parseFloat($('#FlipTable #Bid2MasterBuildingPermit').val()) 		: 0;
	var Bid2Plumbing					= $('#FlipTable #Bid2Plumbing').val() 					? parseFloat($('#FlipTable #Bid2Plumbing').val()) 					: 0;
	var Bid2Electrical					= $('#FlipTable #Bid2Electrical').val() 				? parseFloat($('#FlipTable #Bid2Electrical').val()) 				: 0;
	var Bid2HVAC						= $('#FlipTable #Bid2HVAC').val() 						? parseFloat($('#FlipTable #Bid2HVAC').val()) 						: 0;
	var Bid2Dumpster					= $('#FlipTable #Bid2Dumpster').val() 					? parseFloat($('#FlipTable #Bid2Dumpster').val()) 					: 0;
	var Bid2Other						= $('#FlipTable #Bid2Other').val() 					? parseFloat($('#FlipTable #Bid2Other').val()) 					: 0;
	
	var Bid2WallFraming					= $('#FlipTable #Bid2WallFraming').val() 				? parseFloat($('#FlipTable #Bid2WallFraming').val()) 				: 0;
	var Bid2FloorFraming				= $('#FlipTable #Bid2FloorFraming').val() 				? parseFloat($('#FlipTable #Bid2FloorFraming').val()) 				: 0;
	var Bid2CeilingFraming				= $('#FlipTable #Bid2CeilingFraming').val() 			? parseFloat($('#FlipTable #Bid2CeilingFraming').val()) 			: 0;
	var Bid2InteriorElectrical			= $('#FlipTable #Bid2InteriorElectrical').val() 		? parseFloat($('#FlipTable #Bid2InteriorElectrical').val()) 		: 0;
	var Bid2InteriorPlumbing			= $('#FlipTable #Bid2InteriorPlumbing').val() 			? parseFloat($('#FlipTable #Bid2InteriorPlumbing').val()) 			: 0;
	var Bid2InteriorHVAC				= $('#FlipTable #Bid2InteriorHVAC').val() 				? parseFloat($('#FlipTable #Bid2InteriorHVAC').val()) 				: 0;
	var Bid2Flooring					= $('#FlipTable #Bid2Flooring').val() 					? parseFloat($('#FlipTable #Bid2Flooring').val()) 					: 0;
	var Bid2Sheetrock					= $('#FlipTable #Bid2Sheetrock').val() 				? parseFloat($('#FlipTable #Bid2Sheetrock').val()) 				: 0;
	var Bid2Windows						= $('#FlipTable #Bid2Windows').val() 					? parseFloat($('#FlipTable #Bid2Windows').val()) 					: 0;
	var Bid2InteriorDoors				= $('#FlipTable #Bid2InteriorDoors').val() 			? parseFloat($('#FlipTable #Bid2InteriorDoors').val()) 			: 0;
	var Bid2Trim						= $('#FlipTable #Bid2Trim').val() 						? parseFloat($('#FlipTable #Bid2Trim').val()) 						: 0;
	var Bid2BathroomVanities			= $('#FlipTable #Bid2BathroomVanities').val() 			? parseFloat($('#FlipTable #Bid2BathroomVanities').val()) 			: 0;
	var Bid2BathroomFixtures			= $('#FlipTable #Bid2BathroomFixtures').val() 			? parseFloat($('#FlipTable #Bid2BathroomFixtures').val()) 			: 0;
	var Bid2KitchenCabinets				= $('#FlipTable #Bid2KitchenCabinets').val() 			? parseFloat($('#FlipTable #Bid2KitchenCabinets').val()) 			: 0;
	var Bid2LaborInstallKitchen			= $('#FlipTable #Bid2LaborInstallKitchen').val() 		? parseFloat($('#FlipTable #Bid2LaborInstallKitchen').val()) 		: 0;
	var Bid2FloorCoverings1				= $('#FlipTable #Bid2FloorCoverings1').val() 			? parseFloat($('#FlipTable #Bid2FloorCoverings1').val()) 			: 0;
	var Bid2FloorCoverings2				= $('#FlipTable #Bid2FloorCoverings2').val() 			? parseFloat($('#FlipTable #Bid2FloorCoverings2').val()) 			: 0;
	var Bid2FloorCoverings3				= $('#FlipTable #Bid2FloorCoverings3').val() 			? parseFloat($('#FlipTable #Bid2FloorCoverings3').val()) 			: 0;
	var Bid2InteriorPainting			= $('#FlipTable #Bid2InteriorPainting').val() 			? parseFloat($('#FlipTable #Bid2InteriorPainting').val()) 			: 0;
	var Bid2LightFixtures				= $('#FlipTable #Bid2LightFixtures').val() 			? parseFloat($('#FlipTable #Bid2LightFixtures').val()) 			: 0;
	var Bid2OtherFixtures				= $('#FlipTable #Bid2OtherFixtures').val() 			? parseFloat($('#FlipTable #Bid2OtherFixtures').val()) 			: 0;
	var Bid2Appliances					= $('#FlipTable #Bid2Appliances').val() 				? parseFloat($('#FlipTable #Bid2Appliances').val()) 				: 0;
	var Bid2OtherInterior				= $('#FlipTable #Bid2OtherInterior').val() 			? parseFloat($('#FlipTable #Bid2OtherInterior').val()) 			: 0;
	
	var Bid2ExteriorTrim				= $('#FlipTable #Bid2ExteriorTrim').val() 				? parseFloat($('#FlipTable #Bid2ExteriorTrim').val()) 				: 0;
	var Bid2ExteriorDoors				= $('#FlipTable #Bid2ExteriorDoors').val()			 	? parseFloat($('#FlipTable #Bid2ExteriorDoors').val()) 			: 0;
	var Bid2Porches						= $('#FlipTable #Bid2Porches').val() 					? parseFloat($('#FlipTable #Bid2Porches').val()) 					: 0;
	var Bid2Siding						= $('#FlipTable #Bid2Siding').val() 					? parseFloat($('#FlipTable #Bid2Siding').val()) 					: 0;
	var Bid2ExteriorPainting			= $('#FlipTable #Bid2ExteriorPainting').val() 			? parseFloat($('#FlipTable #Bid2ExteriorPainting').val()) 			: 0;
	var Bid2Roof						= $('#FlipTable #Bid2Roof').val() 						? parseFloat($('#FlipTable #Bid2Roof').val()) 						: 0;
	var Bid2GuttersDownspouts			= $('#FlipTable #Bid2GuttersDownspouts').val() 		? parseFloat($('#FlipTable #Bid2GuttersDownspouts').val()) 		: 0;
	var Bid2Fencing						= $('#FlipTable #Bid2Fencing').val() 					? parseFloat($('#FlipTable #Bid2Fencing').val()) 					: 0;
	var Bid2Landscaping					= $('#FlipTable #Bid2Landscaping').val() 				? parseFloat($('#FlipTable #Bid2Landscaping').val()) 				: 0;
	var Bid2DrivewayConcrete			= $('#FlipTable #Bid2DrivewayConcrete').val() 			? parseFloat($('#FlipTable #Bid2DrivewayConcrete').val()) 			: 0;
	var Bid2FoudationWork				= $('#FlipTable #Bid2FoudationWork').val() 			? parseFloat($('#FlipTable #Bid2FoudationWork').val()) 			: 0;
	var Bid2BrickPointingReplacement	= $('#FlipTable #Bid2BrickPointingReplacement').val() 	? parseFloat($('#FlipTable #Bid2BrickPointingReplacement').val()) 	: 0;
	var Bid2OtherExterior				= $('#FlipTable #Bid2OtherExterior').val() 			? parseFloat($('#FlipTable #Bid2OtherExterior').val()) 			: 0;
	
	var Bid2Contingency					= $('#FlipTable #Bid2Contingency').val() 				? parseFloat($('#FlipTable #Bid2Contingency').val()) 				: 0;
	var Bid2GCFee						= $('#FlipTable #Bid2GCFee').val() 					? parseFloat($('#FlipTable #Bid2GCFee').val()) 					: 0;
	var Bid2Cleanup						= $('#FlipTable #Bid2Cleanup').val() 					? parseFloat($('#FlipTable #Bid2Cleanup').val()) 					: 0;
	var Bid2OtherOther					= $('#FlipTable #Bid2OtherOther').val() 				? parseFloat($('#FlipTable #Bid2OtherOther').val()) 				: 0;
	
	var TotalBid2Total = Bid2Demo + Bid2Architectural + Bid2MasterBuildingPermit + Bid2Plumbing + Bid2Electrical + Bid2HVAC + Bid2Dumpster + Bid2Other + Bid2WallFraming + Bid2FloorFraming + Bid2CeilingFraming + Bid2InteriorElectrical + Bid2InteriorPlumbing + Bid2InteriorHVAC + Bid2Flooring + Bid2Sheetrock + Bid2Windows + Bid2InteriorDoors + Bid2Trim + Bid2BathroomVanities + Bid2BathroomFixtures + Bid2KitchenCabinets + Bid2LaborInstallKitchen + Bid2FloorCoverings1 + Bid2FloorCoverings2 + Bid2FloorCoverings3 + Bid2InteriorPainting + Bid2LightFixtures + Bid2OtherFixtures + Bid2Appliances + Bid2OtherInterior + Bid2ExteriorTrim + Bid2ExteriorDoors + Bid2Porches + Bid2Siding + Bid2ExteriorPainting + Bid2Roof + Bid2GuttersDownspouts + Bid2Fencing + Bid2Landscaping + Bid2DrivewayConcrete + Bid2FoudationWork + Bid2BrickPointingReplacement + Bid2OtherExterior + Bid2Contingency + Bid2GCFee + Bid2Cleanup + Bid2OtherOther;
	
	TotalBid2Total = TotalBid2Total ? parseFloat(TotalBid2Total) : 0;
	$('#FlipTable #TotalBid2Total').val(TotalBid2Total.toFixed(2));
}

// Bid 3 Calculation
function FlipTotalBid3TotalCalculation() {
	var Bid3Demo 						= $('#FlipTable #Bid3Demo').val() 						? parseFloat($('#FlipTable #Bid3Demo').val()) 						: 0;
	var Bid3Architectural				= $('#FlipTable #Bid3Architectural').val() 			? parseFloat($('#FlipTable #Bid3Architectural').val()) 			: 0;
	var Bid3MasterBuildingPermit		= $('#FlipTable #Bid3MasterBuildingPermit').val() 		? parseFloat($('#FlipTable #Bid3MasterBuildingPermit').val()) 		: 0;
	var Bid3Plumbing					= $('#FlipTable #Bid3Plumbing').val() 					? parseFloat($('#FlipTable #Bid3Plumbing').val()) 					: 0;
	var Bid3Electrical					= $('#FlipTable #Bid3Electrical').val() 				? parseFloat($('#FlipTable #Bid3Electrical').val()) 				: 0;
	var Bid3HVAC						= $('#FlipTable #Bid3HVAC').val() 						? parseFloat($('#FlipTable #Bid3HVAC').val()) 						: 0;
	var Bid3Dumpster					= $('#FlipTable #Bid3Dumpster').val() 					? parseFloat($('#FlipTable #Bid3Dumpster').val()) 					: 0;
	var Bid3Other						= $('#FlipTable #Bid3Other').val() 					? parseFloat($('#FlipTable #Bid3Other').val()) 					: 0;
	
	var Bid3WallFraming					= $('#FlipTable #Bid3WallFraming').val() 				? parseFloat($('#FlipTable #Bid3WallFraming').val()) 				: 0;
	var Bid3FloorFraming				= $('#FlipTable #Bid3FloorFraming').val() 				? parseFloat($('#FlipTable #Bid3FloorFraming').val()) 				: 0;
	var Bid3CeilingFraming				= $('#FlipTable #Bid3CeilingFraming').val() 			? parseFloat($('#FlipTable #Bid3CeilingFraming').val()) 			: 0;
	var Bid3InteriorElectrical			= $('#FlipTable #Bid3InteriorElectrical').val() 		? parseFloat($('#FlipTable #Bid3InteriorElectrical').val()) 		: 0;
	var Bid3InteriorPlumbing			= $('#FlipTable #Bid3InteriorPlumbing').val() 			? parseFloat($('#FlipTable #Bid3InteriorPlumbing').val()) 			: 0;
	var Bid3InteriorHVAC				= $('#FlipTable #Bid3InteriorHVAC').val() 				? parseFloat($('#FlipTable #Bid3InteriorHVAC').val()) 				: 0;
	var Bid3Flooring					= $('#FlipTable #Bid3Flooring').val() 					? parseFloat($('#FlipTable #Bid3Flooring').val()) 					: 0;
	var Bid3Sheetrock					= $('#FlipTable #Bid3Sheetrock').val() 				? parseFloat($('#FlipTable #Bid3Sheetrock').val()) 				: 0;
	var Bid3Windows						= $('#FlipTable #Bid3Windows').val() 					? parseFloat($('#FlipTable #Bid3Windows').val()) 					: 0;
	var Bid3InteriorDoors				= $('#FlipTable #Bid3InteriorDoors').val() 			? parseFloat($('#FlipTable #Bid3InteriorDoors').val()) 			: 0;
	var Bid3Trim						= $('#FlipTable #Bid3Trim').val() 						? parseFloat($('#FlipTable #Bid3Trim').val()) 						: 0;
	var Bid3BathroomVanities			= $('#FlipTable #Bid3BathroomVanities').val() 			? parseFloat($('#FlipTable #Bid3BathroomVanities').val()) 			: 0;
	var Bid3BathroomFixtures			= $('#FlipTable #Bid3BathroomFixtures').val() 			? parseFloat($('#FlipTable #Bid3BathroomFixtures').val()) 			: 0;
	var Bid3KitchenCabinets				= $('#FlipTable #Bid3KitchenCabinets').val() 			? parseFloat($('#FlipTable #Bid3KitchenCabinets').val()) 			: 0;
	var Bid3LaborInstallKitchen			= $('#FlipTable #Bid3LaborInstallKitchen').val() 		? parseFloat($('#FlipTable #Bid3LaborInstallKitchen').val()) 		: 0;
	var Bid3FloorCoverings1				= $('#FlipTable #Bid3FloorCoverings1').val() 			? parseFloat($('#FlipTable #Bid3FloorCoverings1').val()) 			: 0;
	var Bid3FloorCoverings2				= $('#FlipTable #Bid3FloorCoverings2').val() 			? parseFloat($('#FlipTable #Bid3FloorCoverings2').val()) 			: 0;
	var Bid3FloorCoverings3				= $('#FlipTable #Bid3FloorCoverings3').val() 			? parseFloat($('#FlipTable #Bid3FloorCoverings3').val()) 			: 0;
	var Bid3InteriorPainting			= $('#FlipTable #Bid3InteriorPainting').val() 			? parseFloat($('#FlipTable #Bid3InteriorPainting').val()) 			: 0;
	var Bid3LightFixtures				= $('#FlipTable #Bid3LightFixtures').val() 			? parseFloat($('#FlipTable #Bid3LightFixtures').val()) 			: 0;
	var Bid3OtherFixtures				= $('#FlipTable #Bid3OtherFixtures').val() 			? parseFloat($('#FlipTable #Bid3OtherFixtures').val()) 			: 0;
	var Bid3Appliances					= $('#FlipTable #Bid3Appliances').val() 				? parseFloat($('#FlipTable #Bid3Appliances').val()) 				: 0;
	var Bid3OtherInterior				= $('#FlipTable #Bid3OtherInterior').val() 			? parseFloat($('#FlipTable #Bid3OtherInterior').val()) 			: 0;
	
	var Bid3ExteriorTrim				= $('#FlipTable #Bid3ExteriorTrim').val() 				? parseFloat($('#FlipTable #Bid3ExteriorTrim').val()) 				: 0;
	var Bid3ExteriorDoors				= $('#FlipTable #Bid3ExteriorDoors').val()			 	? parseFloat($('#FlipTable #Bid3ExteriorDoors').val()) 			: 0;
	var Bid3Porches						= $('#FlipTable #Bid3Porches').val() 					? parseFloat($('#FlipTable #Bid3Porches').val()) 					: 0;
	var Bid3Siding						= $('#FlipTable #Bid3Siding').val() 					? parseFloat($('#FlipTable #Bid3Siding').val()) 					: 0;
	var Bid3ExteriorPainting			= $('#FlipTable #Bid3ExteriorPainting').val() 			? parseFloat($('#FlipTable #Bid3ExteriorPainting').val()) 			: 0;
	var Bid3Roof						= $('#FlipTable #Bid3Roof').val() 						? parseFloat($('#FlipTable #Bid3Roof').val()) 						: 0;
	var Bid3GuttersDownspouts			= $('#FlipTable #Bid3GuttersDownspouts').val() 		? parseFloat($('#FlipTable #Bid3GuttersDownspouts').val()) 		: 0;
	var Bid3Fencing						= $('#FlipTable #Bid3Fencing').val() 					? parseFloat($('#FlipTable #Bid3Fencing').val()) 					: 0;
	var Bid3Landscaping					= $('#FlipTable #Bid3Landscaping').val() 				? parseFloat($('#FlipTable #Bid3Landscaping').val()) 				: 0;
	var Bid3DrivewayConcrete			= $('#FlipTable #Bid3DrivewayConcrete').val() 			? parseFloat($('#FlipTable #Bid3DrivewayConcrete').val()) 			: 0;
	var Bid3FoudationWork				= $('#FlipTable #Bid3FoudationWork').val() 			? parseFloat($('#FlipTable #Bid3FoudationWork').val()) 			: 0;
	var Bid3BrickPointingReplacement	= $('#FlipTable #Bid3BrickPointingReplacement').val() 	? parseFloat($('#FlipTable #Bid3BrickPointingReplacement').val()) 	: 0;
	var Bid3OtherExterior				= $('#FlipTable #Bid3OtherExterior').val() 			? parseFloat($('#FlipTable #Bid3OtherExterior').val()) 			: 0;
	
	var Bid3Contingency					= $('#FlipTable #Bid3Contingency').val() 				? parseFloat($('#FlipTable #Bid3Contingency').val()) 				: 0;
	var Bid3GCFee						= $('#FlipTable #Bid3GCFee').val() 					? parseFloat($('#FlipTable #Bid3GCFee').val()) 					: 0;
	var Bid3Cleanup						= $('#FlipTable #Bid3Cleanup').val() 					? parseFloat($('#FlipTable #Bid3Cleanup').val()) 					: 0;
	var Bid3OtherOther					= $('#FlipTable #Bid3OtherOther').val() 				? parseFloat($('#FlipTable #Bid3OtherOther').val()) 				: 0;
	
	var TotalBid3Total = Bid3Demo + Bid3Architectural + Bid3MasterBuildingPermit + Bid3Plumbing + Bid3Electrical + Bid3HVAC + Bid3Dumpster + Bid3Other + Bid3WallFraming + Bid3FloorFraming + Bid3CeilingFraming + Bid3InteriorElectrical + Bid3InteriorPlumbing + Bid3InteriorHVAC + Bid3Flooring + Bid3Sheetrock + Bid3Windows + Bid3InteriorDoors + Bid3Trim + Bid3BathroomVanities + Bid3BathroomFixtures + Bid3KitchenCabinets + Bid3LaborInstallKitchen + Bid3FloorCoverings1 + Bid3FloorCoverings2 + Bid3FloorCoverings3 + Bid3InteriorPainting + Bid3LightFixtures + Bid3OtherFixtures + Bid3Appliances + Bid3OtherInterior + Bid3ExteriorTrim + Bid3ExteriorDoors + Bid3Porches + Bid3Siding + Bid3ExteriorPainting + Bid3Roof + Bid3GuttersDownspouts + Bid3Fencing + Bid3Landscaping + Bid3DrivewayConcrete + Bid3FoudationWork + Bid3BrickPointingReplacement + Bid3OtherExterior + Bid3Contingency + Bid3GCFee + Bid3Cleanup + Bid3OtherOther;
	
	TotalBid3Total = TotalBid3Total ? parseFloat(TotalBid3Total) : 0;
	$('#FlipTable #TotalBid3Total').val(TotalBid3Total.toFixed(2));
}


// Check Month Exceed
function CheckFlipMonthRehab(SelfElem, CheckElem) {
	var SelfVal = parseFloat($(SelfElem).val()), CheckVal = parseFloat($('#' + CheckElem).val());
	if ( SelfVal > CheckVal ) {
		$(SelfElem).parents('td').next('td').text('Month Exceeds Rehab Timeline');
	} else {
		$(SelfElem).parents('td').next('td').text('');
	}
}

// Flip Rehab Draw Selection Change
function FlipRehabDrawSelectionChange() {
	if ( $('#FlipRehabDrawSelection').val() == 'Fund Rehab at Closing' ) {
		if ( $('#FlipRehabBudgetMethod').val() == 'Quick Lump Sum' ) {
			var LumpSumFlipBudget = parseFloat($('#LumpSumFlipBudget').val());
			$('#MonthTotal_0').text(LumpSumFlipBudget);
		} else {
			var DetailedFlipBudget = parseFloat($('#DetailedFlipBudget').val())
			$('#MonthTotal_0').text(DetailedFlipBudget);
		}
	} else {
		$('#MonthTotal_0').text(0);
	}
}