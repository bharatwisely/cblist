jQuery(document).ready(function($) {
    "use strict";
	
	$('#OfferCalculator .form-control').on('change blur focus', function() {
        var AVR 				= $('#avr').val() 				? parseFloat($('#avr').val()) 				: 0; // C5
		var MaxAVRPercent 		= $('#max_avr_percent').val() 	? parseFloat($('#max_avr_percent').val()) 	: 0; // C6
		var Repairs 			= $('#repairs').val()			? parseFloat($('#repairs').val())			: 0; // C8
		var ClosingCostsPercent = $('#closing_costs').val()		? parseFloat($('#closing_costs').val())		: 0; // C9
		var HoldingCosts 		= $('#holding_costs').val()		? parseFloat($('#holding_costs').val())		: 0; // C10
		var OtherExpenses 		= $('#other_expenses').val()	? parseFloat($('#other_expenses').val())	: 0; // C11
		var WholesaleProfit 	= $('#wholesale_profit').val()	? parseFloat($('#wholesale_profit').val())	: 0; // C13
		
		MaxAVRPercent = (MaxAVRPercent/100);
		ClosingCostsPercent = (ClosingCostsPercent/100);
		var MaxOffer = parseFloat(AVR*MaxAVRPercent-Repairs-(AVR*MaxAVRPercent-Repairs-HoldingCosts-OtherExpenses)*ClosingCostsPercent-HoldingCosts-OtherExpenses-WholesaleProfit);
		$('#max_offer').val(MaxOffer.toFixed(2));
		
		var EndBuyerCostBasis = MaxAVRPercent*AVR;
		$('#end_buyer_cost_basis').val(EndBuyerCostBasis.toFixed(2));
	
		var ProjectedMonthlyIncome 		= $('#projected_monthly_income').val() 	? parseFloat($('#projected_monthly_income').val())		: 0;
		var ProjectedMonthlyExpenses	= $('#projected_monthly_expenses').val()? parseFloat($('#projected_monthly_expenses').val())	: 0;
		var EndBuyerCostBasis			= $('#end_buyer_cost_basis').val()		? parseFloat($('#end_buyer_cost_basis').val()) 			: 0;
		var CapRate = (ProjectedMonthlyIncome - ProjectedMonthlyExpenses) * 12 / EndBuyerCostBasis;
		CapRate = CapRate ? parseFloat(CapRate * 100) : 0;
		$('#cap_rate').val(CapRate.toFixed(2));
	});

});