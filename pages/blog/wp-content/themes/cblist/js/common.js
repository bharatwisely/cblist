jQuery(document).ready(function($) {
	"use strict";
	
	var btn_clicked = false;
	
	var num = 0;
	$('.number-field').attr('placeholder', num.toFixed(2));
	
	$('.number-field').change(function(){
		try {
			if ( $(this).val().length == 0 ) {
				return false;
			}
			var avrVal = parseFloat($(this).val().replace("$", ""));
		} catch(err) {
			//
		}
		$(this).val(avrVal.toFixed(2));
	});
	
	// progressbar
	var $pb = $('.progress .progress-bar');
	$('.trigger-bar').click(function() {
		var $value = $(this).attr('data-rel');
		$pb.attr('data-transitiongoal', $value).progressbar({
			display_text: 'center'
		});
	});
	
	// script for offer screen ui	
	$('.offer-calc-page').css('min-height', $(window).height() - $('.page-title').outerHeight() - 30);
	
	$('#RehubValuator .Step1Button').click(function(e) {
		e.preventDefault();
    });
	$('#RehubValuator .Step2Button').click(function(e) {
		e.preventDefault();
    });
	$('#RehubValuator .Step3Button').click(function(e) {
		e.preventDefault();
		if ( $('#PurchasePrice').val() != '' ) {
			if ( $('#PurchasePriceEM').length > 0 ) {
				$('#PurchasePriceEM').text($('#PurchasePrice').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">Purchase Price: <em id="PurchasePriceEM">' + $('#PurchasePrice').val() + '</em></span>');
			}
		}
    });
	$('#RehubValuator .Step4Button').click(function(e) {
		e.preventDefault();
    });
	$('#RehubValuator .Step5Button').click(function(e) {
		e.preventDefault();
		if ( $('#MonthsFlipRehab').val() != '' ) {
			if ( $('#MonthsFlipRehabEM').length > 0 ) {
				$('#MonthsFlipRehabEM').text($('#MonthsFlipRehab').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">Flip Rehab Period (Months): <em id="MonthsFlipRehabEM">' + $('#MonthsFlipRehab').val() + '</em></span> ');
			}
		}
		if ( $('#ActualFlipBudget').val() != '' ) {
			if ( $('#ActualFlipBudgetEM').length > 0 ) {
				$('#ActualFlipBudgetEM').text($('#ActualFlipBudget').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">Flip Budget: <em id="ActualFlipBudgetEM">' + $('#ActualFlipBudget').val() + '</em></span> ');
			}
		}
		if ( $('#ARVFlip').val() != '' ) {
			if ( $('#ARVFlipEM').length > 0 ) {
				$('#ARVFlipEM').text($('#ARVFlip').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">ARV Flip: <em id="ARVFlipEM">' + $('#ARVFlip').val() + '</em></span> ');
			}
		}
		if ( $('#MonthsToSell').val() != '' ) {
			if ( $('#MonthsToSellEM').length > 0 ) {
				$('#MonthsToSellEM').text($('#MonthsToSell').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">Months to Sell for Flip: <em id="MonthsToSellEM">' + $('#MonthsToSell').val() + '</em></span> ');
			}
		}
    });
	$('#RehubValuator .Step6Button').click(function(e) {
		e.preventDefault();
		if ( $('#MonthsRefiRehab').val() != '' ) {
			if ( $('#MonthsRefiRehabEM').length > 0 ) {
				$('#MonthsRefiRehabEM').text($('#MonthsRefiRehab').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">Refi Rehab Period: <em id="MonthsRefiRehabEM">' + $('#MonthsRefiRehab').val() + '</em></span> ');
			}
		}
		if ( $('#ActualRefiBudget').val() != '' ) {
			if ( $('#ActualRefiBudgetEM').length > 0 ) {
				$('#ActualRefiBudgetEM').text($('#ActualRefiBudget').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">Refi Budget: <em id="ActualRefiBudgetEM">' + $('#ActualRefiBudget').val() + '</em></span> ');
			}
		}
		if ( $('#ARVRent').val() != '' ) {
			if ( $('#ARVRentEM').length > 0 ) {
				$('#ARVRentEM').text($('#ARVRent').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">ARV Rent: <em id="ARVRentEM">' + $('#ARVRent').val() + '</em></span> ');
			}
		}
		if ( $('#MonthsToRefi').val() != '' ) {
			if ( $('#MonthsToRefiEM').length > 0 ) {
				$('#MonthsToRefiEM').text($('#MonthsToRefi').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">Months to Sell for Rent: <em id="MonthsToRefiEM">' + $('#MonthsToRefi').val() + '</em></span> ');
			}
		}
		if ( $('#CashOutRefi').val() != '' ) {
			if ( $('#CashOutRefiEM').length > 0 ) {
				$('#CashOutRefiEM').text($('#CashOutRefi').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">Cash Out at Refi: <em id="CashOutRefiEM">' + $('#CashOutRefi').val() + '</em></span> ');
			}
		}
		if ( $('#CashflowMonthly').val() != '' ) {
			if ( $('#CashflowMonthlyEM').length > 0 ) {
				$('#CashflowMonthlyEM').text($('#CashflowMonthly').val());
			} else {
		        $('.quick-tags').append('<span class="quick-tag">Cashflow Monthly: <em id="CashflowMonthlyEM">' + $('#CashflowMonthly').val() + '</em></span> ');
			}
		}
    });
	$('#RehubValuator .Step7Button').click(function(e) {
		e.preventDefault();
    });
	$('#RehubValuator .Step8Button').click(function(e) {
		e.preventDefault();
    });
	
	$('.offer-calculator-screen .on_next_section').on('click', function() {
		var form = $(this).parents('form');
		var error = [];
		$(form).find('.Required').each(function(index) {
			if ( $(this).val() == false ) {
				$(this).css({
					'border' : '1px solid #a94442',
					'background' : '#f2dede',
				});
				error.push('error');
			} else {
				$(this).css({
					'border' : '1px solid #34a15e',
					'background' : '#ceead9',
				});
			}
		});
		
		var page = $('#page_type').val();
		if ( page == 'PROPOSAL' ) {
			if ( $(this).hasClass('Step5Button') ) {
				if ( (parseInt($('#MonthsFlipRehab').val()) + parseInt($('#MonthsToSell').val())) > 24 ) {
					$('#MonthsFlipRehab, #MonthsToSell').css({
						'border' : '1px solid #a94442',
						'background' : '#f2dede',
					});
					alert('ATTENTION: The total time period between rehabbing the property and selling it must be no more than 24 months. Please change your inputs to make sure that the total period assumed to rehab and sell the property is 24months or less!');
					return false;
				} else {
					$('#MonthsFlipRehab, #MonthsToSell').css({
						'border' : '1px solid #34a15e',
						'background' : '#ceead9',
					});
				}
			}
			if ( $(this).hasClass('Step6Button') ) {
				if ( (parseInt($('#MonthsRefiRehab').val()) + parseInt($('#MonthsToRefi').val())) > 24 ) {
					$('#MonthsRefiRehab, #MonthsToRefi').css({
						'border' : '1px solid #a94442',
						'background' : '#f2dede',
					});
					alert('ATTENTION: The total time period between rehabbing the property and selling it must be no more than 24 months. Please change your inputs to make sure that the total period assumed to rehab and sell the property is 24months or less!');
					return false;
				} else {
					$('#MonthsRefiRehab, #MonthsToRefi').css({
						'border' : '1px solid #34a15e',
						'background' : '#ceead9',
					});
				}
			}
		}
		
		if ( page == 'RENTAL' ) {
			if ( $(this).hasClass('RentalStep8') ) {
				if ( $('#FirstMortgageUsed').val() == 'Yes' && $('#SecondMortgageUsed').val() == 'Yes' ) {
					if ( (parseFloat($('#DownPaymentPercent').val()) + parseFloat($('#FirstMortgageLTVPercent').val()) + parseFloat($('#SecondMortgageLTVPercent').val())) > 100 ) {
						$('#DownPaymentPercent, #FirstMortgageLTVPercent, #SecondMortgageLTVPercent').css({
							'border' : '1px solid #a94442',
							'background' : '#f2dede',
						});
						alert('ATTENTION: Check LTV!');
						return false;
					} else {
						$('#DownPaymentPercent, #FirstMortgageLTVPercent, #SecondMortgageLTVPercent').css({
							'border' : '1px solid #34a15e',
							'background' : '#ceead9',
						});
					}
				} else if ( $('#FirstMortgageUsed').val() == 'Yes' && $('#SecondMortgageUsed').val() == 'No' ) {
					if ( (parseFloat($('#DownPaymentPercent').val()) + parseFloat($('#FirstMortgageLTVPercent').val())) > 100 ) {
						$('#DownPaymentPercent, #FirstMortgageLTVPercent').css({
							'border' : '1px solid #a94442',
							'background' : '#f2dede',
						});
						alert('ATTENTION: Check LTV!');
						return false;
					} else {
						$('#DownPaymentPercent, #FirstMortgageLTVPercent').css({
							'border' : '1px solid #34a15e',
							'background' : '#ceead9',
						});
					}
				} else if ( $('#FirstMortgageUsed').val() == 'No' && $('#SecondMortgageUsed').val() == 'Yes' ) {
					if ( (parseFloat($('#DownPaymentPercent').val()) + parseFloat($('#SecondMortgageLTVPercent').val())) > 100 ) {
						$('#DownPaymentPercent, #SecondMortgageLTVPercent').css({
							'border' : '1px solid #a94442',
							'background' : '#f2dede',
						});
						alert('ATTENTION: Check LTV!');
						return false;
					} else {
						$('#DownPaymentPercent, #SecondMortgageLTVPercent').css({
							'border' : '1px solid #34a15e',
							'background' : '#ceead9',
						});
					}
				}
			}
		}
		
		if ( error.indexOf('error') == -1 ) {
			$(this).parent().parent().parent().parent().slideUp(500);
			$(this).parent().parent().parent().parent().next('.full-section-box').slideDown(500).find('.assump-box-child:eq(0)').slideDown(500).find('.form-control:eq(0)').focus();
			$("html, body").animate({scrollTop: 0}, 500);
			btn_clicked = true;
			var $pb = $('.progress .progress-bar');
			var $value = $(this).attr('data-rel');
			$pb.attr('data-transitiongoal', $value).progressbar({
				display_text: 'center'
			});
		} else {
			show_error_message('Fields are required!');
			return false;
		}
	});
	$('.offer-calculator-screen .on_prev_section').on('click', function() {
		$(this).parent().parent().parent().parent().slideUp(500);
		$(this).parent().parent().parent().parent().prev('.full-section-box').slideDown(500).find('.assump-box-child:last').slideDown(500).find('.form-control:first').focus();
		$("html, body").animate({scrollTop: 0}, 500);
		btn_clicked = true;
	});	
	
	$('.offer-calculator-screen .Required').on('change blur focus', function() {
		if ( $(this).val() != false ) {
			if ( $(this).is('[readonly]') ) {
				$(this).css({
					'border' : '1px solid #34a15e',
					'background' : '#ffffff',
				});
			} else {
				$(this).css({
					'border' : '1px solid #34a15e',
					'background' : '#ceead9',
				});
			}
		}
	});
	
	$('.offer-calculator-screen .on_save_event').on('click', function() {
		var form = $(this).parents('form');
		var error = [];
		$(form).find('.Required').each(function(index) {
			if ( $(this).val() == false ) {
				$(this).css({
					'border' : '1px solid #a94442',
					'background' : '#f2dede',
				});
				error.push('error');
			} else {
				$(this).css({
					'border' : '1px solid #34a15e',
					'background' : '#ceead9',
				});
			}
		});
		
		if ( error.indexOf('error') != -1 ) {
			show_error_message('Fields are required!');
			return false;
		}
		
		var count = 0;
		var page = $('#page_type').val();
		var flag = new Array;
		var PageStatus = $('#PageStatus').val();
		var Images = $.trim( $('.images').val() );
		
		if ( PageStatus == 'INSERT' ) {
			if ( !Images ) {
				var con = confirm('You are going to submit your proposal without photos. Don\'t worry you can upload photos later. Do you wish to continue?');
			} else {
				var con = true;
			}
			if ( con == true ) {
				if ( Images ) {
					for ( var i = 0; i < $('.images').get(0).files.length; i++ ) {
						var img = $('.images').get(0).files[i].name;
						var extension = img.split('.').pop().toUpperCase();
						if ( extension != "PNG" && extension != "JPG" && extension != "GIF" && extension != "JPEG" && extension != "BMP" ) {
							count = count + 1;
						}
					}
				}
			} else {
				return false;
			}
			
			if ( count > 0 ) {
				btn_clicked = true;
				alert('Please select valid images. Valid image types: JPG, PNG, GIF & BMP');
				return false;
			}
		}
		
		var $pb = $('.progress .progress-bar');
		var $value = $(this).attr('data-rel');
		$pb.attr('data-transitiongoal', $value).progressbar({
			display_text: 'center'
		});
		
		$('.loading').removeClass('hide');
		var elem = $(this);
		$('.on_save_event').attr('disabled', 'disabled');
		
		$('form[name="PictureForm"]').ajaxForm({
			beforeSubmit:function(e){
				// 
			},
			success:function(data){
				// 
			},
			error:function(e){
				// error
			}
		}).submit();

		$('form[name="FlipBudgetForm"]').each(function (index) {
            var $form = $(this);
			var dataString = $form.serialize();
			$.ajax({
				type: "POST",
				url: base_url + "pages/flip-budget-ajax/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					// 
				}
			});
		});

		$('form[name="RefiBudgetForm"]').each(function (index) {
            var $form = $(this);
			var dataString = $form.serialize();
			$.ajax({
				type: "POST",
				url: base_url + "pages/refi-budget-ajax/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					// 
				}
			});
		});
		
		$('form[name="PropertyForm"]').each(function (index) {
            var $form = $(this);
			var dataString = $form.serialize();
			$.ajax({
				type: "POST",
				url: base_url + "pages/ajax-store/",
				async: dataString.async,
				data: dataString,
				success: function(data) {
					flag[index] = data;
					if ( page == 'PROPOSAL' ) {
						if ( index == 1 ) {
							var error = flag.indexOf('error');
							if ( error == -1 ) {
								btn_clicked = false;
								$('.on_save_event').text('SAVING');
								if ( !btn_clicked ) {
									setTimeout(function() {
										window.location.href = base_url + 'pages/proposals/';
										return false;
									}, 2000);
								}
							} else {
								alert('Something went wrong, please try again!');
								btn_clicked = true;
								return false;
							}
						}
					} else if ( page == 'COMPARISON' ) {
						if ( index == 1 ) {
							var error = flag.indexOf('error');
							if ( error == -1 ) {
								btn_clicked = false;
								$('.on_save_event').attr('disabled', 'disabled');
								$('.on_save_event').text('SAVING');
								if ( !btn_clicked ) {
									setTimeout(function() {
										window.location.href = base_url + 'pages/comparisons/';
										return false;
									}, 2000);
								}
							} else {
								alert('Something went wrong, please try again!');
								btn_clicked = true;
								return false;
							}
						}
					} else if ( page == 'RENTAL' ) {
						if ( index == 1 ) {
							var error = flag.indexOf('error');
							if ( error == -1 ) {
								btn_clicked = false;
								$('.on_save_event').attr('disabled', 'disabled');
								$('.on_save_event').text('SAVING');
								if ( !btn_clicked ) {
									setTimeout(function() {
										window.location.href = base_url + 'pages/rental-valuator/';
										return false;
									}, 2000);
								}
							} else {
								alert('Something went wrong, please try again!');
								btn_clicked = true;
								return false;
							}
						}
					} else if ( page == 'MAX OFFER' ) {
						if ( index == 1 ) {
							var error = flag.indexOf('error');
							if ( error == -1 ) {
								btn_clicked = false;
								$('.on_save_event').attr('disabled', 'disabled');
								$('.on_save_event').text('SAVING');
								if ( !btn_clicked ) {
									setTimeout(function() {
										window.location.href = base_url + 'pages/max-offers/';
										return false;
									}, 2000);
								}
							} else {
								alert('Something went wrong, please try again!');
								btn_clicked = true;
								return false;
							}
						}
					}
				}
			});
		});
	});
	

	$(window).bind('beforeunload', function() {
		if ( btn_clicked ) {
			return 'This page is asking you to confirm that you want to leave - data you have entered may not be saved.';
		}
	});
	
	// Rental Modal Close
	$('#AdvaceModalReturn').click(function() {
		$('#MoreOptionModal').modal('hide');
    });
	
	
	// Closing Cost Modal Close
	$('#ClosingCostsReturn').click(function() {
		var TotalPurchaseClosingCosts = $('#TotalPurchaseClosingCosts').val() ? parseFloat($('#TotalPurchaseClosingCosts').val()) : 0;
		TotalPurchaseClosingCosts = TotalPurchaseClosingCosts ? parseFloat(TotalPurchaseClosingCosts) : 0;
		if ( $('#ClosingCostsOption').val() == 'Quick Lump Sum' ) {
			$('#ClosingCosts').val(TotalPurchaseClosingCosts.toFixed(2));
		} else if ( $('#ClosingCostsOption').val() == 'Detailed Input' ) {
			$('#ClosingCosts').val(TotalPurchaseClosingCosts.toFixed(2));
		}
		$('#ClosingCostsModal').modal('hide');
    });
	
	// Holding Cost Modal Close
	$('#HoldingCostsReturn').click(function() {
		var TotalPurchaseHoldingCosts = $('#TotalPurchaseHoldingCosts').val() ? parseFloat($('#TotalPurchaseHoldingCosts').val()) : 0;
		TotalPurchaseHoldingCosts = TotalPurchaseHoldingCosts ? parseFloat(TotalPurchaseHoldingCosts) : 0;
		if ( $('#HoldingCostsOption').val() == 'Quick Lump Sum' ) {
			$('#HoldingCosts').val(TotalPurchaseHoldingCosts.toFixed(2));
		} else if ( $('#HoldingCostsOption').val() == 'Detailed Input' ) {
			$('#HoldingCosts').val(TotalPurchaseHoldingCosts.toFixed(2) + '/month');
		}
		$('#HoldingCostsModal').modal('hide');
    });
	
	// Flip Analysis Modal Close
	$('#FlipRehubReturn').click(function() {
		if ( $('#FlipRehabBudgetMethod').val() == 'Detailed Input' ) {
			var DetailedFlipBudget = $('#DetailedFlipBudget').val() ? parseFloat($('#DetailedFlipBudget').val()) : 0;
			$('#ActualFlipBudget').val(DetailedFlipBudget.toFixed(2));
		} else {
			var LumpSumFlipBudget = $('#LumpSumFlipBudget').val() ? parseFloat($('#LumpSumFlipBudget').val()) : 0;
			$('#ActualFlipBudget').val(LumpSumFlipBudget.toFixed(2));
		}
		$('#FlipRehubModal').modal('hide');
		return false;
    });
	
	// Refi Analysis Modal Close
	$('#RefiRehubReturn').click(function() {
		if ( $('#RefiRehabBudgetMethod').val() == 'Detailed Input' ) {
			var DetailedRefiBudget = $('#DetailedRefiBudget').val() ? parseFloat($('#DetailedRefiBudget').val()) : 0;
			$('#ActualRefiBudget').val(DetailedRefiBudget.toFixed(2));
		} else {
			var LumpSumRefiBudget = $('#LumpSumRefiBudget').val() ? parseFloat($('#LumpSumRefiBudget').val()) : 0;
			$('#ActualRefiBudget').val(LumpSumRefiBudget.toFixed(2));
		}
		$('#RefiRehubModal').modal('hide');
		return false;
    });
	
	// Operating Income Modal Close
	$('#OperatingIncomeReturn').click(function() {
		var MonthlyGrossOperatingIncome = $('#MonthlyGrossOperatingIncome').val() ? parseFloat($('#MonthlyGrossOperatingIncome').val()) : 0;
		$('#OperatingIncome').val(MonthlyGrossOperatingIncome.toFixed(2));
		$('#OperatingIncomeModal').modal('hide');
		return false;
    });
	
	// Operating Expenses Modal Close
	$('#OperatingExpensesReturn').click(function() {
		var OperatingExpensesMonthlyTotal = $('#OperatingExpensesMonthlyTotal').val() ? parseFloat($('#OperatingExpensesMonthlyTotal').val()) : 0;
		$('#OperatingExpenses').val(OperatingExpensesMonthlyTotal.toFixed(2));
		$('#OperatingExpensesModal').modal('hide');
		return false;
    });
	
	// FlipRehubModal Auto Margin
	$("#FlipRehubModal").css({
		'margin-left': function () {
			return -($(this).width() / 7);
		}
	});
	// RefiRehubModal Auto Margin
	$("#RefiRehubModal").css({
		'margin-left': function () {
			return -($(this).width() / 7);
		}
	});
	
	// MoreOptionModal Auto Margin
	$("#MoreOptionModal").css({
		'margin-left': function () {
			return -($(this).width() / 4);
		}
	});
	
	// Print Modal
	$('#FullFlip').click(function() {
		$('.print-option').removeAttr('checked');
        $('input[name="CoverPage"]').prop('checked', 'checked');
		$('input[name="FlipMarketingSheet"]').prop('checked', 'checked');
		$('input[name="FlipCashFlow"]').prop('checked', 'checked');
		$('input[name="CompsReport"]').prop('checked', 'checked');
		$('input[name="PicsPage"]').prop('checked', 'checked');
    });
	$('#FullRefi').click(function() {
		$('.print-option').removeAttr('checked');
        $('input[name="CoverPage"]').prop('checked', 'checked');
		$('input[name="RefiMarketingSheet"]').prop('checked', 'checked');
		$('input[name="RefiCashFlow"]').prop('checked', 'checked');
		$('input[name="CompsReport"]').prop('checked', 'checked');
		$('input[name="PicsPage"]').prop('checked', 'checked');
    });
	$('#FullFlipLender').click(function() {
		$('.print-option').removeAttr('checked');
        $('input[name="CoverPage"]').prop('checked', 'checked');
		$('input[name="FlipFundingRequest"]').prop('checked', 'checked');
		$('input[name="FlipCashFlowLender"]').prop('checked', 'checked');
		$('input[name="CompsReport"]').prop('checked', 'checked');
		$('input[name="PicsPage"]').prop('checked', 'checked');
    });
	$('#FullRefiLender').click(function() {
		$('.print-option').removeAttr('checked');
        $('input[name="CoverPage"]').prop('checked', 'checked');
		$('input[name="RefiFundingRequest"]').prop('checked', 'checked');
		$('input[name="RefiCashFlowLender"]').prop('checked', 'checked');
		$('input[name="CompsReport"]').prop('checked', 'checked');
		$('input[name="PicsPage"]').prop('checked', 'checked');
    });
	
	//	print proposal 
	$('.btn_proposal_print').click(function(){
		var user_id = $(this).attr('data-id');
		var propertyId = $(this).attr('data-propertyId');
		var type = $(this).attr('data-type');
		var print_url = base_url + "pages/print-report/"+ propertyId + "/";
		if ( $('.print-feature:checked').length == 0 ) {
			$('.print-error').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Please select atleast one report before clicking the print button.</span>');
			return false;
		}
		$('.print-feature').each(function(index){
			if ( $(this).prop("checked") ) {
				var feature = $(this).attr("name");
				$.ajax({
					type: 'POST',
					url: base_url + 'pages/checkuserpaid/' + user_id + '/',
					data: {},
					success: function(data) {
						if ( data == 0 ) {
							if ( type == 'proposal' ) {
								$('#reports').modal('hide');
							} else if ( type == 'rental' ) {
								$('#rental_reports').modal('hide');
							}
							$('#upgrade_modal').modal('show');
							$('.print-feature').removeAttr('checked');
						} else {
							OpenInNewTab(print_url + feature + '/');
						}
					},
				});
			}
		});
	});
	
	//	print proposal 
	$('.btn_proposal_download').click(function(){
		var user_id = $(this).attr('data-id');
		var propertyId = $(this).attr('data-propertyId');
		var type = $(this).attr('data-type');
		var download_url = base_url + "pages/download-report/"+ propertyId + "/";
		if ( $('.print-feature:checked').length == 0 ) {
			$('.print-error').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Please select reports before clicking the download button.</span>');
			return false;
		}
		var feature = '';
		$('.print-feature').each(function(index){
			if ( $(this).prop("checked") ) {
				feature = (feature ? feature + '-' : '') + $(this).attr("name");
			}
		});
		$.ajax({
			type: 'POST',
			url: base_url + 'pages/checkuserpaid/' + user_id + '/',
			data: {},
			success: function(data) {
				if ( data == 0 ) {
					if ( type == 'proposal' ) {
						$('#reports').modal('hide');
					} else if ( type == 'rental' ) {
						$('#rental_reports').modal('hide');
					}
					$('#upgrade_modal').modal('show');
					$('.print-feature').removeAttr('checked');
				} else {
					OpenInNewTab(download_url + feature + '/');
				}
			},
		});
	});
	
	//	print proposal 
	$('.btn_proposal_email').click(function(){
		$(this).attr('disabled', 'disabled');
		var ajax_request = [];
		var user_id = $(this).attr('data-id');
		var propertyId = $(this).attr('data-propertyId');
		var type = $(this).attr('data-type');
		if ( $('.print-feature:checked').length == 0 ) {
			$('.print-error').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Please select atleast one report before clicking the email button.</span>');
			return false;
		}
		if ( type == 'proposal' ) {
			$('#reports').modal('hide');
		} else if ( type == 'rental' ) {
			$('#rental_reports').modal('hide');
		}
		$('.print-feature').each(function(index){
			if ( $(this).prop("checked") ) {
				var feature = $(this).attr("name");
				$.ajax({
					type: 'POST',
					url: base_url + 'pages/checkuserpaid/' + user_id + '/',
					data: {},
					success: function(data) {
						if ( data == 0 ) {
							if ( type == 'proposal' ) {
								$('#reports').modal('hide');
							} else if ( type == 'rental' ) {
								$('#rental_reports').modal('hide');
							}
							$('#upgrade_modal').modal('show');
							$('.print-feature').removeAttr('checked');
						} else {
							var ajax_req = $.ajax({
								type: 'POST',
								dataType: "json",
								url: base_url + "pages/download-report/"+ propertyId + "/"+ feature + "/save/" + type + "/",
								data: {},
								success: function(data) {
									$('#modalEmail').modal('show');
									$('.modal-loading').html('<span class="text-danger"><img src="' + base_url + 'assets/images/loading.gif" alt="loading" /> Please wait, attaching the reports to your email.</span>');
									$('#attchments_files').append('<input type="hidden" name="attachments[]" value="' + data.pdf_path + '" />');
									$('#attchments_views').append('<div class="col-lg-3 pdf-attach"><a target="_blank" href="' + data.pdf_url + '"><i class="fa fa-file-pdf-o"></i> ' + data.pdf_name + '</a></div>');
									$('#inputSubject').val(data.propertyName + ' - CBList Reports');
								},
							});
							ajax_request.push(ajax_req);
						}
					},
				});				
			}
			var process_interval = setInterval(function() {
				var process_complete_flag = true;
				$.each(ajax_request, function(index, value) {
					if ( value.state() == "pending" ) {
						process_complete_flag = false;
					}
				});
				if ( process_complete_flag ) {
					$('.modal-loading').html('<span class="text-success"><i class="fa fa-check-square-o"></i> Reports are successfully attached.</span>');
					$(this).removeAttr('disabled');
					setTimeout(function() {
						$('.modal-loading').html('');
						clearInterval(process_interval);
					}, 10000);
				}
			}, 1000);
		});
	});
	
	$('#btn_email_send').click(function() {
		var inputTo = $('#inputTo').val();
		if ( inputTo == '' || inputTo == false ) {
			$('.modal-loading').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Please specify at least one recipient.</span>');
			return false;
		}
		
		$('.modal-loading').html('<span class="text-danger"><img src="' + base_url + 'assets/images/loading.gif" alt="loading" /> Please wait, sending email.</span>');
		var dataString = $('#email_form').serialize();
		$.ajax({
			type: 'POST',
			url: base_url + "pages/email-proposal/",
			data: dataString,
			success: function(data) {
				if ( data == 'sent' ) {
					$('#email_form')[0].reset();
					$('#attchments_views').html('');
					$('#attchments_files').html('');
					$('.modal-loading').html('<span class="text-success"><i class="fa fa-check-square-o"></i> Email successfully sent.</span>');
					setTimeout(function() {
						$('.modal-loading').html('');
					}, 10000);
				} else {
					$('.modal-loading').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Something went wrong, please try again.</span>');
				}
			},
		});
    });
	
	$('#btn_email_cancel').click(function() {
        $('#modalEmail').modal('hide');
		var type = $(this).attr('data-type');
		if ( type == 'proposal' ) {
			$('#reports').modal('show');
		} else if ( type == 'rental' ) {
			$('#rental_reports').modal('show');
		}
		$('.print-feature').removeAttr('checked');
    });
	
	$('.btn_proposal_cancel').click(function() {
        $('.print-feature').removeAttr('checked');
		$('.print-error').html('');
    });
	
	$('.print-feature').click(function(e) {
        $('.print-error').html('');
    });
	
	
	// tooltip functions
	$('body').popover({
		selector	: '[data-popover]', 
		trigger		: 'onfocus onclick hover', 
		placement	: 'right', 
		html		: true, 
		delay		: {
			show	: 1, 
			hide	: 1
		}
	});
	
	$('#rentalPopup').click(function(){
		if($('#PropertyClosingDate').val() !='' || $('#PropertyClosingDate').val() !='Undefined') {
			var PropertyClosingDate = $('#PropertyClosingDate').val();
			$.ajax({
				type: 'POST',
				url: base_url + 'pages/setpropertyClosingDate/',
				data: 'PropertyClosingDate=' + PropertyClosingDate,
				success: function(data) {
					$('#date_show').html(data);
				},
			});
		}
	});
	
});