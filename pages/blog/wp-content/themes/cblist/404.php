<?php get_header(); ?>

	<section>
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h1 style="margin-top:0;">404 Page Not Found</h1>
                    <h3>ooops..... You are lost</h3>
                    <p>The page you are looking for seems to be missing.Go back, or return to home page to choose a new direction.</p>
                    <a href="<?php bloginfo("url"); ?>">Back to Home Page</a>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>