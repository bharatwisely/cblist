<?php
add_filter('body_class', 'add_category_class_single');

define('HOME_URL', str_replace('pages/blog', '', get_bloginfo("url")));

function add_category_class_single($classes){
	if(is_home()) {
		$classes[] = 'home-page';
	}
	return $classes;
}

function cblist_wp_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	// Add the site name.
	$title .= get_bloginfo( 'name', 'display' );

	// Add the site description for the home/front page.
	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	// Add a page number if necessary.
	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'cblist' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'cblist_wp_title', 10, 2 );

// Enable support for Post Thumbnails, and declare two sizes.
add_theme_support( 'post-thumbnails' );

// File for Bootstrap Menu
require_once( "wp_bootstrap_navwalker.php" );

// Active Class
add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
function special_nav_class($classes, $item){
     if( in_array('current-menu-item', $classes) ){
             $classes[] = 'active ';
     }
     return $classes;
}

// This theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
	'header'	=> __( 'Header Menu', 'cblist' ),
	'footer'	=> __( 'Footer Menu', 'cblist' ),
) );

// Register Search
register_sidebar(array(
	'name'=> 'Search',
	'id' => 'search',
	'before_widget' => '<div id="%1$s" class="search-Block %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));

// Register Copy Right
register_sidebar(array(
	'name'=> 'Social Icon',
	'id' => 'social-icon',
	'before_widget' => '<div id="%1$s" class="social-Block %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<h2>',
	'after_title' => '</h2>',
));

// Register Footer #1
register_sidebar(array(
	'name'=> 'Footer #1',
	'id' => 'footer-1',
	'before_widget' => '<div id="%1$s" class="span6 widget widget-aboutus %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<header class="header-style"><h2 class="h-style">',
	'after_title' => '</h2></header>',
));

// Register Footer #2
register_sidebar(array(
	'name'=> 'Footer #2',
	'id' => 'footer-2',
	'before_widget' => '<div id="%1$s" class="span3 widget widget-aboutus %2$s">',
	'after_widget' => '</div>',
	'before_title' => '<header class="header-style"><h2 class="h-style">',
	'after_title' => '</h2></header>',
));

// Function for Wordpress File Upload Thickbox
function cblistFileUpload_admin_scripts() {
	//if (isset($_GET['page'])) {
		wp_enqueue_script('jquery');
		wp_enqueue_script('media-upload');
		wp_enqueue_script('thickbox');
		wp_register_script('my-upload', get_template_directory_uri() . '/js/cblistFileUpload.js', array('jquery', 'media-upload', 'thickbox'));
		wp_enqueue_script('my-upload');
	//}
}
 
function cblistFileUpload_admin_styles() {
	//if (isset($_GET['page'])) {
		wp_enqueue_style('thickbox');
	//}
}
add_action('admin_print_scripts', 'cblistFileUpload_admin_scripts');
add_action('admin_print_styles', 'cblistFileUpload_admin_styles');