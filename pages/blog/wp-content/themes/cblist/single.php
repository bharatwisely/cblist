<?php get_header(); ?>

    <section>
        <div class="container">            
            <div class="row">
            	<div class="breadcrumbs">
                	<span class="home" typeof="ListItem" property="itemListElement">
                		<a href="<?php echo HOME_URL; ?>">Home</a>&nbsp;>
                    </span>
					<?php if(function_exists('bcn_display')) { bcn_display(); } ?>
                </div>
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h1 style="margin-top:0;"><?php the_title(); ?></h1>
                        <div class="single_blog">
                            <div class="img_section">
	                            <?php $featuredImageThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full' ); ?>
								<img src="<?php echo $featuredImageThumbnail[0]; ?>" width="100%" />
                            </div>
                            <div class="single_title_section">
                                <div class="time"><?php the_time('l, F j, Y g:i A'); ?></div>
                                <p class="content_area"><?php the_content(); ?></p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <div class="row">
            	<?php comments_template(); ?>
            </div>
        </div>
    </section>

<?php get_footer(); ?>