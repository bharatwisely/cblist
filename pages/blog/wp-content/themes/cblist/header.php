<!DOCTYPE html>
<!--[if IE 8]><html lang="<?php language_attributes(); ?>" class="ie8"><![endif]-->
<!--[if IE 9]> <html lang="<?php language_attributes(); ?>" class="ie9"><![endif]-->
<!--[if !IE]><!--><html lang="<?php language_attributes(); ?>"><!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?php wp_title( '>>', true, 'right' ); ?></title>

<!-- Favicon -->
<link href="<?php echo get_template_directory_uri(); ?>/images/favicon.png" rel="shortcut icon" />

<!-- Bootstrap API Stylesheet -->
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Datepicker Stylesheet -->
<!-- Main Stylesheet -->
<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet" type="text/css" />
<!-- Bootstrap Progress Bar Stylesheet -->
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-progressbar.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-datepicker.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Slider Stylesheet -->
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-slider.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap Star Rating Stylesheet -->
<link href="<?php echo get_template_directory_uri(); ?>/css/bootstrap-star-rating.min.css" rel="stylesheet" type="text/css" />
<!-- Bootstrap DataTables Stylesheet -->
<link href="<?php echo get_template_directory_uri(); ?>/css/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- Font Awesome Stylesheet -->
<link href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Moris Chart Stylesheet -->
<link href="<?php echo get_template_directory_uri(); ?>/css/nanoscroller.css" rel="stylesheet" type="text/css" />
<!-- Moris Chart Stylesheet -->
<link href="<?php echo get_template_directory_uri(); ?>/css/morris.css" rel="stylesheet" type="text/css" />
<!-- Flex Slider Stylesheet -->
<link href="<?php echo get_template_directory_uri(); ?>/css/flexslider.css" rel="stylesheet" type="text/css" />
<!-- Fancybox Stylesheet -->
<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.fancybox.css?v=2.1.5" rel="stylesheet" type="text/css" />
<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.fancybox-buttons.css?v=1.0.5" rel="stylesheet" type="text/css" />
<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.fancybox-thumbs.css?v=1.0.7" rel="stylesheet" type="text/css" />
<!-- Responsive Sidebar menu API -->
<link href="<?php echo get_template_directory_uri(); ?>/css/jquery.sidr.light.css" rel="stylesheet" type="text/css" />

<!-- jQuery API Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.min.js"></script>
<!-- JWPlayer API Library -->
<script src="<?php echo get_template_directory_uri(); ?>/js/jwplayer.js"></script>
<!-- Google Place API Library -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;libraries=places"></script>
<!-- Google Place Custom Scripts -->

<script type="text/javascript">
jQuery(document).ready(function(e) {
    jQuery('.page-content .box-body').css('min-height', jQuery(window).height() - (jQuery('.page-title').height() + jQuery('.box-header').height() + 110));
});
</script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5shiv.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/respond.min.js"></script>
<![endif]-->
<?php wp_head(); ?>
</head>
<body>

<header id="header">
	<div class="container">
    	<div class="row">
        	<nav class="navbar">
            	<div class="container-fluid">
                	<div class="navbar-header">
                    	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        	<span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1 class="home-logo">
                        	<a class="navbar-brand" href="<?php echo HOME_URL; ?>">
                        		<img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="CBList" width="210" />
                        	</a>
                        </h1>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                        	<li>
                                <a href="<?php echo HOME_URL; ?>">Home</a>
                            </li>
                            <li>
                                <a href="<?php echo HOME_URL; ?>pages/aboutus/">About Us</a>
                            </li>
                            <li class="active">
                                <a href="<?php bloginfo("url"); ?>">Blog</a>
                            </li>
                            <li>
                                <a href="<?php echo HOME_URL; ?>pages/our-offer/">Our Offer</a>
                            </li>
                            <li>
                                <a href="<?php echo HOME_URL; ?>pages/features/">Features</a>
                            </li>
                            <li>
                                <a href="<?php echo HOME_URL; ?>pages/investors-login/">Investors Login</a>
                            </li>
                            <li>
                                <a href="<?php echo HOME_URL; ?>pages/cashlenders-login/">Cash Lenders Login</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>        	
    </div>
</header>