<?php get_header(); ?>

    <section>
        <div class="container">
            <div class="row">
                <?php while ( have_posts() ) : the_post(); ?>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h1 class="top_title"><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php the_title(); ?></a></h1>
                        <div class="single_blog">
                            <div class="img_section">
                                <?php $featuredImageThumbnail = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'medium' ); ?>
                                <a href="<?php echo get_permalink(get_the_ID()); ?>">
                                    <img src="<?php echo $featuredImageThumbnail[0]; ?>" width="<?php echo $featuredImageThumbnail[1]; ?>" height="<?php echo $featuredImageThumbnail[2]; ?>" />
                                </a>
                            </div>
                            <div class="title_section">
                                <div class="time"><?php the_time('l, F j, Y g:i A'); ?></div>
                                <p class="content_area">
                                    <?php echo implode(' ', array_slice(explode(' ', get_the_content()), 0, 100)); ?>...
                                    <a href="<?php echo get_permalink(get_the_ID()); ?>" class="read_more">Read More</a>
                                </p>
                            </div>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <?php wp_pagenavi(); ?>
        </div>
    </section>
    
<?php get_footer(); ?>