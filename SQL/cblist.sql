-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 23, 2015 at 08:48 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `cblist`
--

-- --------------------------------------------------------

--
-- Table structure for table `ci_analyser_extra_details`
--

CREATE TABLE IF NOT EXISTS `ci_analyser_extra_details` (
`ExtraID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `P1Rent` double NOT NULL,
  `P1Income` double NOT NULL,
  `P1Vacancy` double NOT NULL,
  `P1Management` double NOT NULL,
  `P1Taxes` double NOT NULL,
  `P1TaxesRent` double NOT NULL,
  `P1Ins` double NOT NULL,
  `P1InsRent` double NOT NULL,
  `P1Maint` double NOT NULL,
  `P1MaintRent` double NOT NULL,
  `P1OpEx` double NOT NULL,
  `P1OpExRent` double NOT NULL,
  `P1RentPercent` double NOT NULL,
  `P1NOI` double NOT NULL,
  `P1MortPymt` double NOT NULL,
  `P2Rent` double NOT NULL,
  `P2Income` double NOT NULL,
  `P2Vacancy` double NOT NULL,
  `P2Management` double NOT NULL,
  `P2Taxes` double NOT NULL,
  `P2TaxesRent` double NOT NULL,
  `P2Ins` double NOT NULL,
  `P2InsRent` double NOT NULL,
  `P2Maint` double NOT NULL,
  `P2MaintRent` double NOT NULL,
  `P2OpEx` double NOT NULL,
  `P2OpExRent` double NOT NULL,
  `P2RentPercent` double NOT NULL,
  `P2NOI` double NOT NULL,
  `P2MortPymt` double NOT NULL,
  `P3Rent` double NOT NULL,
  `P3Income` double NOT NULL,
  `P3Vacancy` double NOT NULL,
  `P3Management` double NOT NULL,
  `P3Taxes` double NOT NULL,
  `P3TaxesRent` double NOT NULL,
  `P3Ins` double NOT NULL,
  `P3InsRent` double NOT NULL,
  `P3Maint` double NOT NULL,
  `P3MaintRent` double NOT NULL,
  `P3OpEx` double NOT NULL,
  `P3OpExRent` double NOT NULL,
  `P3RentPercent` double NOT NULL,
  `P3NOI` double NOT NULL,
  `P3MortPymt` double NOT NULL,
  `P4Rent` double NOT NULL,
  `P4Income` double NOT NULL,
  `P4Vacancy` double NOT NULL,
  `P4Management` double NOT NULL,
  `P4Taxes` double NOT NULL,
  `P4TaxesRent` double NOT NULL,
  `P4Ins` double NOT NULL,
  `P4InsRent` double NOT NULL,
  `P4Maint` double NOT NULL,
  `P4MaintRent` double NOT NULL,
  `P4OpEx` double NOT NULL,
  `P4OpExRent` double NOT NULL,
  `P4RentPercent` double NOT NULL,
  `P4NOI` double NOT NULL,
  `P4MortPymt` double NOT NULL,
  `P5Rent` double NOT NULL,
  `P5Income` double NOT NULL,
  `P5Vacancy` double NOT NULL,
  `P5Management` double NOT NULL,
  `P5Taxes` double NOT NULL,
  `P5TaxesRent` double NOT NULL,
  `P5Ins` double NOT NULL,
  `P5InsRent` double NOT NULL,
  `P5Maint` double NOT NULL,
  `P5MaintRent` double NOT NULL,
  `P5OpEx` double NOT NULL,
  `P5OpExRent` double NOT NULL,
  `P5RentPercent` double NOT NULL,
  `P5NOI` double NOT NULL,
  `P5MortPymt` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_analyser_financing_assumption`
--

CREATE TABLE IF NOT EXISTS `ci_analyser_financing_assumption` (
`ComparisonID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `DownPayment` double NOT NULL,
  `Used_1` varchar(255) NOT NULL,
  `Used_2` varchar(255) NOT NULL,
  `LoanType_1` varchar(255) NOT NULL,
  `LoanType_2` varchar(255) NOT NULL,
  `TermYears_1` double NOT NULL,
  `TermYears_2` double NOT NULL,
  `LTV_1` double NOT NULL,
  `LTV_2` double NOT NULL,
  `Rate_1` double NOT NULL,
  `Rate_2` double NOT NULL,
  `InterestOnly_1` varchar(10) NOT NULL,
  `InterestOnly_2` varchar(10) NOT NULL,
  `OriginationFee_1` double NOT NULL,
  `OriginationFee_2` double NOT NULL,
  `DiscountFee_1` double NOT NULL,
  `DiscountFee_2` double NOT NULL,
  `ClosingMiscFees` double NOT NULL,
  `CashbackSeller` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_analyser_property_details`
--

CREATE TABLE IF NOT EXISTS `ci_analyser_property_details` (
`DetailedID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `P1Address` varchar(255) NOT NULL,
  `P1Price` double NOT NULL,
  `P1SqFeet` double NOT NULL,
  `P1CapImp` double NOT NULL,
  `P1CapOut` double NOT NULL,
  `P2Address` varchar(255) NOT NULL,
  `P2Price` double NOT NULL,
  `P2SqFeet` double NOT NULL,
  `P2CapImp` double NOT NULL,
  `P2CapOut` double NOT NULL,
  `P3Address` varchar(255) NOT NULL,
  `P3Price` double NOT NULL,
  `P3SqFeet` double NOT NULL,
  `P3CapImp` double NOT NULL,
  `P3CapOut` double NOT NULL,
  `P4Address` varchar(255) NOT NULL,
  `P4Price` double NOT NULL,
  `P4SqFeet` double NOT NULL,
  `P4CapImp` double NOT NULL,
  `P4CapOut` double NOT NULL,
  `P5Address` varchar(255) NOT NULL,
  `P5Price` double NOT NULL,
  `P5SqFeet` double NOT NULL,
  `P5CapImp` double NOT NULL,
  `P5CapOut` double NOT NULL,
  `CreatedOn` datetime NOT NULL,
  `ProposalStatus` enum('draft','publish') NOT NULL DEFAULT 'draft'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_closing_costs`
--

CREATE TABLE IF NOT EXISTS `ci_closing_costs` (
`ClosingID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ClosingCostsOption` varchar(255) NOT NULL,
  `TotalClosingCosts` double NOT NULL,
  `AttorneyClosingFees` double NOT NULL,
  `TitleSearch` double NOT NULL,
  `TitleInsurance` double NOT NULL,
  `RecordingFees` double NOT NULL,
  `Appraisal` double NOT NULL,
  `Inspection` double NOT NULL,
  `RealEstateTaxesDue` double NOT NULL,
  `ClosingOther1` double NOT NULL,
  `ClosingOther2` double NOT NULL,
  `ClosingOther3` double NOT NULL,
  `TotalPurchaseClosingCosts` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_closing_costs`
--

INSERT INTO `ci_closing_costs` (`ClosingID`, `PropertyID`, `UserID`, `ClosingCostsOption`, `TotalClosingCosts`, `AttorneyClosingFees`, `TitleSearch`, `TitleInsurance`, `RecordingFees`, `Appraisal`, `Inspection`, `RealEstateTaxesDue`, `ClosingOther1`, `ClosingOther2`, `ClosingOther3`, `TotalPurchaseClosingCosts`, `CreatedOn`) VALUES
(1, 1448009806, 3, 'Detailed Input', 0, 150, 1500, 250, 350, 2500, 0, 0, 0, 0, 0, 4750, '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_comparable_reports`
--

CREATE TABLE IF NOT EXISTS `ci_comparable_reports` (
`ReportID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `SalesAddress` varchar(255) NOT NULL,
  `Beds` double NOT NULL,
  `Baths` double NOT NULL,
  `SqFt` double NOT NULL,
  `DateSold` varchar(255) NOT NULL,
  `SalesPrice` decimal(10,0) NOT NULL,
  `Notes` varchar(255) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_comparable_reports`
--

INSERT INTO `ci_comparable_reports` (`ReportID`, `PropertyID`, `UserID`, `SalesAddress`, `Beds`, `Baths`, `SqFt`, `DateSold`, `SalesPrice`, `Notes`, `CreatedOn`) VALUES
(1, 1448009806, 3, 'Mumbai', 4, 3, 1500, '11-01-2015', '650000', 'Good', '2015-11-20 09:56:46'),
(2, 1448009806, 3, 'Bangalore', 4, 3, 1525, '11-02-2015', '655000', 'Good Property', '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_financing_assumption`
--

CREATE TABLE IF NOT EXISTS `ci_financing_assumption` (
`FinancingID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `FinancingUsed` varchar(255) NOT NULL,
  `CapSelection` varchar(255) NOT NULL,
  `FinancingCap` double NOT NULL,
  `RehabDiscountPoints` double NOT NULL,
  `OtherPoints` double NOT NULL,
  `PointsClosingUpfront` varchar(255) NOT NULL,
  `RehabInterestRate` double NOT NULL,
  `InterestPaymentsMade` varchar(255) NOT NULL,
  `SplitWithLender` varchar(255) NOT NULL,
  `LenderSplit` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_financing_assumption`
--

INSERT INTO `ci_financing_assumption` (`FinancingID`, `PropertyID`, `UserID`, `FinancingUsed`, `CapSelection`, `FinancingCap`, `RehabDiscountPoints`, `OtherPoints`, `PointsClosingUpfront`, `RehabInterestRate`, `InterestPaymentsMade`, `SplitWithLender`, `LenderSplit`, `CreatedOn`) VALUES
(1, 1448009806, 3, 'Financing', 'ARV', 70, 2, 1, 'Paid Upfront', 16, 'Yes', 'No', 0, '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_flip_analysis`
--

CREATE TABLE IF NOT EXISTS `ci_flip_analysis` (
`FlipID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `FlipHoldingCosts` double NOT NULL,
  `ActualFlipBudget` double NOT NULL,
  `MonthsFlipRehab` double NOT NULL,
  `ARVFlip` double NOT NULL,
  `MonthsToSell` double NOT NULL,
  `TotalCapitalNeededFlip` double NOT NULL,
  `MaxFinancedFlip` double NOT NULL,
  `ActualFinancedFlip` double NOT NULL,
  `PointInterestRolledUpFlip` double NOT NULL,
  `TotalLoanFlip` double NOT NULL,
  `CashRequiredFlip` double NOT NULL,
  `TotalCostBasisFlip` double NOT NULL,
  `AVRPercentage` double NOT NULL,
  `ResalePrice` double NOT NULL,
  `CostOfSale` double NOT NULL,
  `FlipProfit` double NOT NULL,
  `FlipROI` varchar(50) NOT NULL,
  `FlipROIAnnualized` varchar(50) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_flip_analysis`
--

INSERT INTO `ci_flip_analysis` (`FlipID`, `PropertyID`, `UserID`, `FlipHoldingCosts`, `ActualFlipBudget`, `MonthsFlipRehab`, `ARVFlip`, `MonthsToSell`, `TotalCapitalNeededFlip`, `MaxFinancedFlip`, `ActualFinancedFlip`, `PointInterestRolledUpFlip`, `TotalLoanFlip`, `CashRequiredFlip`, `TotalCostBasisFlip`, `AVRPercentage`, `ResalePrice`, `CostOfSale`, `FlipProfit`, `FlipROI`, `FlipROIAnnualized`, `CreatedOn`) VALUES
(1, 1448009806, 3, 800, 25000, 2, 525000, 2, 480550, 367500, 367500, 5550, 373050, 138394.33, 511444.33, 97, 525000, 6, -17944.33, '-12.97', '-38.90', '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_flip_budget`
--

CREATE TABLE IF NOT EXISTS `ci_flip_budget` (
`FlipBudgetID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `PreliminaryWork` varchar(255) NOT NULL,
  `FlipRehabBudgetMethod` varchar(255) NOT NULL,
  `FlipRehabDrawSelection` varchar(255) NOT NULL,
  `LumpSumFlipBudget` double NOT NULL,
  `DetailsNotes` varchar(255) NOT NULL,
  `SqFt` double NOT NULL,
  `Quantity` double NOT NULL,
  `Rate` double NOT NULL,
  `Bid1` double NOT NULL,
  `Bid2` double NOT NULL,
  `Bid3` double NOT NULL,
  `Budget` double NOT NULL,
  `MonthPaid` int(11) NOT NULL,
  `TotalBid1Total` double NOT NULL,
  `TotalBid2Total` double NOT NULL,
  `TotalBid3Total` double NOT NULL,
  `DetailedFlipBudget` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_flip_budget`
--

INSERT INTO `ci_flip_budget` (`FlipBudgetID`, `PropertyID`, `UserID`, `PreliminaryWork`, `FlipRehabBudgetMethod`, `FlipRehabDrawSelection`, `LumpSumFlipBudget`, `DetailsNotes`, `SqFt`, `Quantity`, `Rate`, `Bid1`, `Bid2`, `Bid3`, `Budget`, `MonthPaid`, `TotalBid1Total`, `TotalBid2Total`, `TotalBid3Total`, `DetailedFlipBudget`, `CreatedOn`) VALUES
(1, 1448009806, 3, 'Demo', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 15000, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(2, 1448009806, 3, 'Architectural fees', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 10000, 2, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(3, 1448009806, 3, 'Master Building Permit', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(4, 1448009806, 3, '&nbsp;&nbsp;&nbsp;Plumbing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(5, 1448009806, 3, '&nbsp;&nbsp;&nbsp;Electrical', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(6, 1448009806, 3, '&nbsp;&nbsp;&nbsp;HVAC', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(7, 1448009806, 3, 'Dumpster', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(8, 1448009806, 3, 'Other', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(9, 1448009806, 3, 'Wall Framing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(10, 1448009806, 3, 'Floor Framing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(11, 1448009806, 3, 'Ceiling Framing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(12, 1448009806, 3, 'Electrical', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(13, 1448009806, 3, 'Plumbing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(14, 1448009806, 3, 'HVAC', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(15, 1448009806, 3, 'Flooring', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(16, 1448009806, 3, 'Sheetrock', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(17, 1448009806, 3, 'Windows', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(18, 1448009806, 3, 'Interior Doors', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(19, 1448009806, 3, 'Trim', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(20, 1448009806, 3, 'Bathroom Vanities', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(21, 1448009806, 3, 'Bathroom Fixtures', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(22, 1448009806, 3, 'Kitchen Cabinets', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(23, 1448009806, 3, 'Labor to install Kitchen', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(24, 1448009806, 3, 'Floor Coverings 1', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(25, 1448009806, 3, 'Floor Coverings 2', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(26, 1448009806, 3, 'Floor Coverings 3', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(27, 1448009806, 3, 'Interior Painting', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(28, 1448009806, 3, 'Light Fixtures', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(29, 1448009806, 3, 'Other Fixtures', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(30, 1448009806, 3, 'Appliances', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(31, 1448009806, 3, 'Other Interior', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(32, 1448009806, 3, 'Exterior Trim', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(33, 1448009806, 3, 'Exterior Doors', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(34, 1448009806, 3, 'Porches', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(35, 1448009806, 3, 'Siding', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(36, 1448009806, 3, 'Exterior Painting', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(37, 1448009806, 3, 'Roof', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(38, 1448009806, 3, 'Gutters, Downspouts', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(39, 1448009806, 3, 'Fencing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(40, 1448009806, 3, 'Landscaping', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(41, 1448009806, 3, 'Driveway, concrete work', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(42, 1448009806, 3, 'Foudation Work', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(43, 1448009806, 3, 'Brick Pointing, replacement', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(44, 1448009806, 3, 'Other Exterior', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(45, 1448009806, 3, 'Contingency', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(46, 1448009806, 3, 'GCFee', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(47, 1448009806, 3, 'Cleanup', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46'),
(48, 1448009806, 3, 'Others', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 25000, '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_holding_costs`
--

CREATE TABLE IF NOT EXISTS `ci_holding_costs` (
`HoldingID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `HoldingCostsOption` varchar(255) NOT NULL,
  `TotalHoldingCosts` double NOT NULL,
  `InsurancePolicyPremium` double NOT NULL,
  `ElectricalBills` double NOT NULL,
  `GasBills` double NOT NULL,
  `OilBills` double NOT NULL,
  `WaterSewerBills` double NOT NULL,
  `AlarmSystemBills` double NOT NULL,
  `Miscellanious` double NOT NULL,
  `Lawncare` double NOT NULL,
  `HoldingOther1` double NOT NULL,
  `HoldingOther2` double NOT NULL,
  `TotalPurchaseHoldingCosts` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_holding_costs`
--

INSERT INTO `ci_holding_costs` (`HoldingID`, `PropertyID`, `UserID`, `HoldingCostsOption`, `TotalHoldingCosts`, `InsurancePolicyPremium`, `ElectricalBills`, `GasBills`, `OilBills`, `WaterSewerBills`, `AlarmSystemBills`, `Miscellanious`, `Lawncare`, `HoldingOther1`, `HoldingOther2`, `TotalPurchaseHoldingCosts`, `CreatedOn`) VALUES
(1, 1448009806, 3, 'Detailed Input', 0, 150, 25, 10, 15, 0, 0, 0, 0, 0, 0, 200, '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_max_offer`
--

CREATE TABLE IF NOT EXISTS `ci_max_offer` (
`OfferID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `address_name` varchar(255) NOT NULL,
  `avr` double NOT NULL,
  `max_avr_percent` double NOT NULL,
  `repairs` double NOT NULL,
  `closing_costs` double NOT NULL,
  `holding_costs` double NOT NULL,
  `other_expenses` double NOT NULL,
  `wholesale_profit` double NOT NULL,
  `max_offer` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_offer_rate`
--

CREATE TABLE IF NOT EXISTS `ci_offer_rate` (
`RentalID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `end_buyer_cost_basis` double NOT NULL,
  `projected_monthly_income` double NOT NULL,
  `projected_monthly_expenses` double NOT NULL,
  `cap_rate` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ci_operating_expenses`
--

CREATE TABLE IF NOT EXISTS `ci_operating_expenses` (
`ExpensesID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `OperatingExpensesOption_1` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthly_1` double NOT NULL,
  `OperatingExpensesAnnual_1` double NOT NULL,
  `OperatingExpensesPercentage_1` double NOT NULL,
  `OperatingExpensesOption_2` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthly_2` double NOT NULL,
  `OperatingExpensesAnnual_2` double NOT NULL,
  `OperatingExpensesPercentage_2` double NOT NULL,
  `OperatingExpensesOption_3` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthly_3` double NOT NULL,
  `OperatingExpensesAnnual_3` double NOT NULL,
  `OperatingExpensesPercentage_3` double NOT NULL,
  `OperatingExpensesOption_4` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthly_4` double NOT NULL,
  `OperatingExpensesAnnual_4` double NOT NULL,
  `OperatingExpensesPercentage_4` double NOT NULL,
  `OperatingExpensesOption_5` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthly_5` double NOT NULL,
  `OperatingExpensesAnnual_5` double NOT NULL,
  `OperatingExpensesPercentage_5` double NOT NULL,
  `OperatingExpensesOption_6` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthly_6` double NOT NULL,
  `OperatingExpensesAnnual_6` double NOT NULL,
  `OperatingExpensesPercentage_6` double NOT NULL,
  `OperatingExpensesOption_7` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthly_7` double NOT NULL,
  `OperatingExpensesAnnual_7` double NOT NULL,
  `OperatingExpensesPercentage_7` double NOT NULL,
  `OperatingExpensesOption_8` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthly_8` double NOT NULL,
  `OperatingExpensesAnnual_8` double NOT NULL,
  `OperatingExpensesPercentage_8` double NOT NULL,
  `OperatingExpensesOption_9` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthly_9` double NOT NULL,
  `OperatingExpensesAnnual_9` double NOT NULL,
  `OperatingExpensesPercentage_9` double NOT NULL,
  `OperatingExpensesOption_10` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthly_10` double NOT NULL,
  `OperatingExpensesAnnual_10` double NOT NULL,
  `OperatingExpensesPercentage_10` double NOT NULL,
  `OperatingExpensesOptionOther_1` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthlyOther_1` double NOT NULL,
  `OperatingExpensesAnnualOther_1` double NOT NULL,
  `OperatingExpensesPercentageOther_1` double NOT NULL,
  `OperatingExpensesOptionOther_2` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthlyOther_2` double NOT NULL,
  `OperatingExpensesAnnualOther_2` double NOT NULL,
  `OperatingExpensesPercentageOther_2` double NOT NULL,
  `OperatingExpensesOptionOther_3` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthlyOther_3` double NOT NULL,
  `OperatingExpensesAnnualOther_3` double NOT NULL,
  `OperatingExpensesPercentageOther_3` double NOT NULL,
  `OperatingExpensesOptionOther_4` varchar(255) DEFAULT NULL,
  `OperatingExpensesMonthlyOther_4` double NOT NULL,
  `OperatingExpensesAnnualOther_4` double NOT NULL,
  `OperatingExpensesPercentageOther_4` double NOT NULL,
  `OperatingExpensesUtilitiesWaterSewerMonthly` double NOT NULL,
  `OperatingExpensesUtilitiesWaterSewerAnnual` double NOT NULL,
  `OperatingExpensesUtilitiesWaterSewerPercentage` double NOT NULL,
  `OperatingExpensesUtilitiesElectricityMonthly` double NOT NULL,
  `OperatingExpensesUtilitiesElectricityAnnual` double NOT NULL,
  `OperatingExpensesUtilitiesElectricityPercentage` double NOT NULL,
  `OperatingExpensesUtilitiesGasMonthly` double NOT NULL,
  `OperatingExpensesUtilitiesGasAnnual` double NOT NULL,
  `OperatingExpensesUtilitiesGasPercentage` double NOT NULL,
  `OperatingExpensesUtilitiesFuelOilMonthly` double NOT NULL,
  `OperatingExpensesUtilitiesFuelOilAnnual` double NOT NULL,
  `OperatingExpensesUtilitiesFuelOilPercentage` double NOT NULL,
  `OperatingExpensesUtilitiesOtherUtilitiesMonthly` double NOT NULL,
  `OperatingExpensesUtilitiesOtherUtilitiesAnnual` double NOT NULL,
  `OperatingExpensesUtilitiesOtherUtilitiesPercentage` double NOT NULL,
  `OperatingExpensesMonthlyTotal` double NOT NULL,
  `OperatingExpensesAnnualTotal` double NOT NULL,
  `OperatingExpensesPercentageTotal` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_operating_expenses`
--

INSERT INTO `ci_operating_expenses` (`ExpensesID`, `PropertyID`, `UserID`, `OperatingExpensesOption_1`, `OperatingExpensesMonthly_1`, `OperatingExpensesAnnual_1`, `OperatingExpensesPercentage_1`, `OperatingExpensesOption_2`, `OperatingExpensesMonthly_2`, `OperatingExpensesAnnual_2`, `OperatingExpensesPercentage_2`, `OperatingExpensesOption_3`, `OperatingExpensesMonthly_3`, `OperatingExpensesAnnual_3`, `OperatingExpensesPercentage_3`, `OperatingExpensesOption_4`, `OperatingExpensesMonthly_4`, `OperatingExpensesAnnual_4`, `OperatingExpensesPercentage_4`, `OperatingExpensesOption_5`, `OperatingExpensesMonthly_5`, `OperatingExpensesAnnual_5`, `OperatingExpensesPercentage_5`, `OperatingExpensesOption_6`, `OperatingExpensesMonthly_6`, `OperatingExpensesAnnual_6`, `OperatingExpensesPercentage_6`, `OperatingExpensesOption_7`, `OperatingExpensesMonthly_7`, `OperatingExpensesAnnual_7`, `OperatingExpensesPercentage_7`, `OperatingExpensesOption_8`, `OperatingExpensesMonthly_8`, `OperatingExpensesAnnual_8`, `OperatingExpensesPercentage_8`, `OperatingExpensesOption_9`, `OperatingExpensesMonthly_9`, `OperatingExpensesAnnual_9`, `OperatingExpensesPercentage_9`, `OperatingExpensesOption_10`, `OperatingExpensesMonthly_10`, `OperatingExpensesAnnual_10`, `OperatingExpensesPercentage_10`, `OperatingExpensesOptionOther_1`, `OperatingExpensesMonthlyOther_1`, `OperatingExpensesAnnualOther_1`, `OperatingExpensesPercentageOther_1`, `OperatingExpensesOptionOther_2`, `OperatingExpensesMonthlyOther_2`, `OperatingExpensesAnnualOther_2`, `OperatingExpensesPercentageOther_2`, `OperatingExpensesOptionOther_3`, `OperatingExpensesMonthlyOther_3`, `OperatingExpensesAnnualOther_3`, `OperatingExpensesPercentageOther_3`, `OperatingExpensesOptionOther_4`, `OperatingExpensesMonthlyOther_4`, `OperatingExpensesAnnualOther_4`, `OperatingExpensesPercentageOther_4`, `OperatingExpensesUtilitiesWaterSewerMonthly`, `OperatingExpensesUtilitiesWaterSewerAnnual`, `OperatingExpensesUtilitiesWaterSewerPercentage`, `OperatingExpensesUtilitiesElectricityMonthly`, `OperatingExpensesUtilitiesElectricityAnnual`, `OperatingExpensesUtilitiesElectricityPercentage`, `OperatingExpensesUtilitiesGasMonthly`, `OperatingExpensesUtilitiesGasAnnual`, `OperatingExpensesUtilitiesGasPercentage`, `OperatingExpensesUtilitiesFuelOilMonthly`, `OperatingExpensesUtilitiesFuelOilAnnual`, `OperatingExpensesUtilitiesFuelOilPercentage`, `OperatingExpensesUtilitiesOtherUtilitiesMonthly`, `OperatingExpensesUtilitiesOtherUtilitiesAnnual`, `OperatingExpensesUtilitiesOtherUtilitiesPercentage`, `OperatingExpensesMonthlyTotal`, `OperatingExpensesAnnualTotal`, `OperatingExpensesPercentageTotal`, `CreatedOn`) VALUES
(1, 1448009806, 3, 'Taxes', 500, 6000, 55.56, 'Insurance', 250, 3000, 27.78, 'Landscaping', 150, 1800, 16.67, 'Super', 0, 0, 0, 'Garbage', 0, 0, 0, 'Exterminator', 0, 0, 0, 'Snow Removal', 0, 0, 0, 'Accounting', 0, 0, 0, 'Legal', 0, 0, 0, 'Management', 0, 0, 0, 'Advertising', 0, 0, 0, 'Repairs & Reserve', 0, 0, 0, 'Miscellaneous', 0, 0, 0, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 900, 10800, 100, '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_operating_income`
--

CREATE TABLE IF NOT EXISTS `ci_operating_income` (
`IncomeID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `NoOfUnits_1` int(11) NOT NULL,
  `UnitType_1` varchar(50) DEFAULT NULL,
  `SquareFt_1` int(11) NOT NULL,
  `MonthlyRent_1` double NOT NULL,
  `AnnualRent_1` double NOT NULL,
  `AnnualPercent_1` double NOT NULL,
  `NoOfUnits_2` int(11) NOT NULL,
  `UnitType_2` varchar(50) DEFAULT NULL,
  `SquareFt_2` int(11) NOT NULL,
  `MonthlyRent_2` double NOT NULL,
  `AnnualRent_2` double NOT NULL,
  `AnnualPercent_2` double NOT NULL,
  `NoOfUnits_3` int(11) NOT NULL,
  `UnitType_3` varchar(50) DEFAULT NULL,
  `SquareFt_3` int(11) NOT NULL,
  `MonthlyRent_3` double NOT NULL,
  `AnnualRent_3` double NOT NULL,
  `AnnualPercent_3` double NOT NULL,
  `NoOfUnits_4` int(11) NOT NULL,
  `UnitType_4` varchar(50) DEFAULT NULL,
  `SquareFt_4` int(11) NOT NULL,
  `MonthlyRent_4` double NOT NULL,
  `AnnualRent_4` double NOT NULL,
  `AnnualPercent_4` double NOT NULL,
  `NoOfUnits_5` int(11) NOT NULL,
  `UnitType_5` varchar(50) DEFAULT NULL,
  `SquareFt_5` int(11) NOT NULL,
  `MonthlyRent_5` double NOT NULL,
  `AnnualRent_5` double NOT NULL,
  `AnnualPercent_5` double NOT NULL,
  `NoOfUnits_6` int(11) NOT NULL,
  `UnitType_6` varchar(50) DEFAULT NULL,
  `SquareFt_6` int(11) NOT NULL,
  `MonthlyRent_6` double NOT NULL,
  `AnnualRent_6` double NOT NULL,
  `AnnualPercent_6` double NOT NULL,
  `NoOfUnits_7` int(11) NOT NULL,
  `UnitType_7` varchar(50) DEFAULT NULL,
  `SquareFt_7` int(11) NOT NULL,
  `MonthlyRent_7` double NOT NULL,
  `AnnualRent_7` double NOT NULL,
  `AnnualPercent_7` double NOT NULL,
  `NoOfUnits_8` int(11) NOT NULL,
  `UnitType_8` varchar(50) NOT NULL,
  `SquareFt_8` int(11) NOT NULL,
  `MonthlyRent_8` double NOT NULL,
  `AnnualRent_8` double NOT NULL,
  `AnnualPercent_8` double NOT NULL,
  `NoOfUnits_9` int(11) NOT NULL,
  `UnitType_9` varchar(50) DEFAULT NULL,
  `SquareFt_9` int(11) NOT NULL,
  `MonthlyRent_9` double NOT NULL,
  `AnnualRent_9` double NOT NULL,
  `AnnualPercent_9` double NOT NULL,
  `NoOfUnits_10` int(11) NOT NULL,
  `UnitType_10` varchar(50) DEFAULT NULL,
  `SquareFt_10` int(11) NOT NULL,
  `MonthlyRent_10` double NOT NULL,
  `AnnualRent_10` double NOT NULL,
  `AnnualPercent_10` double NOT NULL,
  `NoOfUnits_11` int(11) NOT NULL,
  `UnitType_11` varchar(50) DEFAULT NULL,
  `SquareFt_11` int(11) NOT NULL,
  `MonthlyRent_11` double NOT NULL,
  `AnnualRent_11` double NOT NULL,
  `AnnualPercent_11` double NOT NULL,
  `NoOfUnits_12` int(11) NOT NULL,
  `UnitType_12` varchar(50) DEFAULT NULL,
  `SquareFt_12` int(11) NOT NULL,
  `MonthlyRent_12` double NOT NULL,
  `AnnualRent_12` double NOT NULL,
  `AnnualPercent_12` double NOT NULL,
  `NoOfUnit_Total` int(11) NOT NULL,
  `SquareFt_Total` int(11) NOT NULL,
  `MonthlyRent_Gross` double NOT NULL,
  `AnnualRent_Gross` double NOT NULL,
  `AnnualPercent_Gross` double NOT NULL,
  `VacancyLossPercent` double NOT NULL,
  `VacancyLossMonthly` double NOT NULL,
  `VacancyLossAnnual` double NOT NULL,
  `ManagementPercent` double NOT NULL,
  `ManagementMonthly` double NOT NULL,
  `ManagementAnnual` double NOT NULL,
  `OtherIncomeMonthly` double NOT NULL,
  `OtherIncomeAnnual` double NOT NULL,
  `MonthlyGrossOperatingIncome` double NOT NULL,
  `AnnualGrossOperatingIncome` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_operating_income`
--

INSERT INTO `ci_operating_income` (`IncomeID`, `PropertyID`, `UserID`, `NoOfUnits_1`, `UnitType_1`, `SquareFt_1`, `MonthlyRent_1`, `AnnualRent_1`, `AnnualPercent_1`, `NoOfUnits_2`, `UnitType_2`, `SquareFt_2`, `MonthlyRent_2`, `AnnualRent_2`, `AnnualPercent_2`, `NoOfUnits_3`, `UnitType_3`, `SquareFt_3`, `MonthlyRent_3`, `AnnualRent_3`, `AnnualPercent_3`, `NoOfUnits_4`, `UnitType_4`, `SquareFt_4`, `MonthlyRent_4`, `AnnualRent_4`, `AnnualPercent_4`, `NoOfUnits_5`, `UnitType_5`, `SquareFt_5`, `MonthlyRent_5`, `AnnualRent_5`, `AnnualPercent_5`, `NoOfUnits_6`, `UnitType_6`, `SquareFt_6`, `MonthlyRent_6`, `AnnualRent_6`, `AnnualPercent_6`, `NoOfUnits_7`, `UnitType_7`, `SquareFt_7`, `MonthlyRent_7`, `AnnualRent_7`, `AnnualPercent_7`, `NoOfUnits_8`, `UnitType_8`, `SquareFt_8`, `MonthlyRent_8`, `AnnualRent_8`, `AnnualPercent_8`, `NoOfUnits_9`, `UnitType_9`, `SquareFt_9`, `MonthlyRent_9`, `AnnualRent_9`, `AnnualPercent_9`, `NoOfUnits_10`, `UnitType_10`, `SquareFt_10`, `MonthlyRent_10`, `AnnualRent_10`, `AnnualPercent_10`, `NoOfUnits_11`, `UnitType_11`, `SquareFt_11`, `MonthlyRent_11`, `AnnualRent_11`, `AnnualPercent_11`, `NoOfUnits_12`, `UnitType_12`, `SquareFt_12`, `MonthlyRent_12`, `AnnualRent_12`, `AnnualPercent_12`, `NoOfUnit_Total`, `SquareFt_Total`, `MonthlyRent_Gross`, `AnnualRent_Gross`, `AnnualPercent_Gross`, `VacancyLossPercent`, `VacancyLossMonthly`, `VacancyLossAnnual`, `ManagementPercent`, `ManagementMonthly`, `ManagementAnnual`, `OtherIncomeMonthly`, `OtherIncomeAnnual`, `MonthlyGrossOperatingIncome`, `AnnualGrossOperatingIncome`, `CreatedOn`) VALUES
(1, 1448009806, 3, 1, '2br', 1400, 4500, 54000, 100, 0, '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 0, '', 0, 0, 0, 0, 1, 1400, 4500, 54000, 100, 0, 0, 0, 0, 0, 0, 0, 0, 4500, 54000, '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_payment`
--

CREATE TABLE IF NOT EXISTS `ci_payment` (
`id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `user_type` varchar(500) NOT NULL,
  `firstname` varchar(500) NOT NULL,
  `lastname` varchar(500) NOT NULL,
  `payer_email` varchar(250) NOT NULL,
  `receiver_email` varchar(250) NOT NULL,
  `item_name` varchar(500) NOT NULL,
  `subscr_id` varchar(500) NOT NULL,
  `amount` varchar(500) NOT NULL,
  `payment_date` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_payment`
--

INSERT INTO `ci_payment` (`id`, `user_id`, `user_type`, `firstname`, `lastname`, `payer_email`, `receiver_email`, `item_name`, `subscr_id`, `amount`, `payment_date`) VALUES
(1, 1, 'investor', 'Test', 'Buyer', 'jitdxpert-buyer@gmail.com', 'sudip@wisely.co', 'CBList 297 Recurring Payment', 'I-P8KLYS51A1BG', '297.00', '01:28:50 Mar 18, 2015 PDT'),
(3, 4, 'investor', 'Test', 'Buyer', 'jitdxpert-buyer@gmail.com', 'sudip@wisely.co', 'CBList 97 Recurring Payment', 'I-FUK7JHDXHHWD', '97.00', '15:07:32 May 29, 2015 PDT'),
(4, 5, 'investor', 'Test', 'Buyer', 'jitdxpert-buyer@gmail.com', 'sudip@wisely.co', 'CBList 297 Recurring Payment', 'I-GKDR0XLRUMBE', '297.00', '15:18:29 May 29, 2015 PDT'),
(5, 3, 'investor', 'Test', 'Buyer', 'jitdxpert-buyer@gmail.com', 'sudip@wisely.co', 'CBList 297 Recurring Payment', 'I-HCWSLJVBS4A5', '297.00', '16:02:43 Jun 24, 2015 PDT'),
(6, 12, 'investor', 'Test', 'Buyer', 'jitdxpert-buyer@gmail.com', 'sudip@wisely.co', 'CBList 297 Recurring Payment', 'I-HCWSLJVBS4A5', '297.00', '16:02:43 Jun 24, 2015 PDT');

-- --------------------------------------------------------

--
-- Table structure for table `ci_personal_information`
--

CREATE TABLE IF NOT EXISTS `ci_personal_information` (
`PersonalID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `YourName` varchar(255) NOT NULL,
  `CompanyName` varchar(255) NOT NULL,
  `CompanyStreet` varchar(255) NOT NULL,
  `CompanyCity` varchar(255) NOT NULL,
  `CompanyState` varchar(255) NOT NULL,
  `CompanyZip` varchar(255) NOT NULL,
  `CompanyCountry` varchar(255) NOT NULL,
  `PhoneNumber` varchar(255) NOT NULL,
  `FacebookURL` varchar(255) DEFAULT NULL,
  `LinkedinURL` varchar(255) DEFAULT NULL,
  `TwitterURL` varchar(255) DEFAULT NULL,
  `Email` varchar(255) NOT NULL,
  `Website` varchar(255) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_personal_information`
--

INSERT INTO `ci_personal_information` (`PersonalID`, `PropertyID`, `UserID`, `YourName`, `CompanyName`, `CompanyStreet`, `CompanyCity`, `CompanyState`, `CompanyZip`, `CompanyCountry`, `PhoneNumber`, `FacebookURL`, `LinkedinURL`, `TwitterURL`, `Email`, `Website`, `CreatedOn`) VALUES
(1, 1448009806, 3, 'Tanushree Sen', 'Wisely', 'Guwahati', 'Guwahati', 'Assam', '568520', 'IN', '9706505789', 'facebook.com/tanushree.sen', 'linkedin.com/pub/tanushree-sen', 'twitter.com/TanushreeSen', 'sen.tanu2010@gmail.com', 'wisely.co', '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_portfolio`
--

CREATE TABLE IF NOT EXISTS `ci_portfolio` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `property_name` varchar(255) NOT NULL,
  `youtube_link` varchar(500) NOT NULL,
  `location` longtext NOT NULL,
  `info_one` varchar(1000) NOT NULL,
  `info_two` varchar(1000) NOT NULL,
  `images` longtext NOT NULL,
  `created_on` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_portfolio`
--

INSERT INTO `ci_portfolio` (`id`, `user_id`, `property_name`, `youtube_link`, `location`, `info_one`, `info_two`, `images`, `created_on`) VALUES
(1, 10, 'Palm Groves', 'https://www.youtube.com/watch?v=h2NcmF1xruc&spfreload=9', 'Fancy Bazaar, Guwahati, Assam, India', 'Property Located in prime location off Electronic city, on Chandapura-Anekal main road, close to Schools, Hospitals, Banks, Electronic City IT Hub, Jigini Industrial Area, Bommasandra Industrial area', 'Property Located in prime location off Electronic city, on Chandapura-Anekal main road, close to Schools, Hospitals, Banks, Electronic City IT Hub, Jigini Industrial Area, Bommasandra Industrial area', 'http://wisely.co/cblist/assets/portfolio/10_1437141114512.jpg|http://wisely.co/cblist/assets/portfolio/10_1437143554917.jpg', '2015-07-17 09:51:54'),
(2, 10, 'eric housing', 'https://www.youtube.com/watch?v=_NWaYjsz3qY', 'Latasil, Guwahati, Assam, India', 'Magnificia is a coveted address for the master class.\r\nA lifestyle of luxury and class.\r\nWith a classy elevation, Magnificia stands out as a landmark on Old Madras Road. The location is just 5 mins drive from Indiranagar, close to the elevated flyover and the Byapanahalli Metro Terminus,enroute to Whitefield and major IT offices and corporate landmarks. It is also connected conveniently to the International Airport through the elevated flyover. It is very close to major super markets, 5 star hotels, golf course and educational institutions. It is 7 km drive to the nerve centre of Bangalore - MG Road.', 'Magnificia is a coveted address for the master class.\r\nA lifestyle of luxury and class.\r\nWith a classy elevation, Magnificia stands out as a landmark on Old Madras Road. The location is just 5 mins drive from Indiranagar, close to the elevated flyover and the Byapanahalli Metro Terminus,enroute to Whitefield and major IT offices and corporate landmarks. It is also connected conveniently to the International Airport through the elevated flyover. It is very close to major super markets, 5 star hotels, golf course and educational institutions. It is 7 km drive to the nerve centre of Bangalore - MG Road.', 'http://wisely.co/cblist/assets/portfolio/10_143714324896.jpg', '2015-07-17 10:27:28'),
(3, 1, 'iSmart Portfolio', 'https://www.youtube.com/watch?v=82eM7QRtoRo', 'Kolkata, West Bengal, India', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://wisely.co/cblist/assets/portfolio/1_1437149905412.jpg|http://wisely.co/cblist/assets/portfolio/1_1437149905471.jpg|http://wisely.co/cblist/assets/portfolio/1_1437149905195.jpg|http://wisely.co/cblist/assets/portfolio/1_1437149905232.jpg', '2015-07-17 12:18:25'),
(4, 8, 'Portfolio Example in Summit NJ', 'https://www.youtube.com/watch?v=xoQvJbqLdV4', '123 Summit Street, Hartford, CT, United States', 'ddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd', 'ddddddddddddddddddddddd', 'http://52.4.194.25/assets/portfolio/8_1440203975666.jpg|http://52.4.194.25/assets/portfolio/8_1440203975151.jpg', '2015-07-17 23:20:47'),
(5, 11, 'aloha housing', 'https://www.youtube.com/watch?v=lv7sFVGbqYM', 'Dispur, Guwahati, Assam, India', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting', '0', '2015-07-18 08:20:11'),
(6, 12, 'Sample Property', 'https://www.youtube.com/watch?v=_CrbO82pb10', 'Kolkata, West Bengal, India', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://localhost/cblist/assets/portfolio/12_144695882456.jpg|http://localhost/cblist/assets/portfolio/12_1446958824704.jpg|http://localhost/cblist/assets/portfolio/12_1446958825498.jpg|http://localhost/cblist/assets/portfolio/12_1446958825139.jpg', '2015-07-27 07:35:57'),
(7, 3, 'Test Portfolio', 'https://www.youtube.com/watch?v=_CrbO82pb10', 'Kolkata, West Bengal, India', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 'http://localhost/cblist/assets/portfolio/3_1444901254946.jpg', '2015-07-27 09:43:44'),
(8, 13, 'vell tower', 'https://www.youtube.com/watch?v=pNL4xGTT0Mk&feature=youtu.be', 'Kolkata, West Bengal, India', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book', 'http://soumitrapaul.com/cblist/assets/portfolio/13_1438166095886.jpg', '2015-07-29 03:34:56'),
(9, 11, 'Sahara Housing', '', 'India', '5) That, if the parties of this agreement decide to leave vacant of the said rented premises, then they shall have to serve one/two/three English Calendar months Notice in writing to the other party, informing their intention and showing requirement cause to do so  and on vacation of the rented premises  this tenancy agreement shall stand dissolved automatically. \r\n6)  That, the Tenant shall pay the rented electric bill charges as consumed by the Tenant in the rented premises as per the meter reading of the concern Authority of ASEB and shall abide by all the electricity Acts and Rules. On default, the tenant shall be liable exclusively.', '', '0', '2015-09-17 13:22:07');

-- --------------------------------------------------------

--
-- Table structure for table `ci_previouspayment`
--

CREATE TABLE IF NOT EXISTS `ci_previouspayment` (
`old_payment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `subscription_id` varchar(255) NOT NULL,
  `subscription_amount` varchar(255) NOT NULL,
  `subscription_date` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_previouspayment`
--

INSERT INTO `ci_previouspayment` (`old_payment_id`, `user_id`, `firstname`, `lastname`, `subscription_id`, `subscription_amount`, `subscription_date`) VALUES
(1, 3, 'Test', 'Buyer', 'I-P8KLYS51A1BG', '97.00', '01:28:50 Mar 18, 2015 PDT'),
(2, 1, 'Test', 'Buyer', 'I-HUX42YWC5AEH', '97.00', '00:07:57 Jul 13, 2015 PDT');

-- --------------------------------------------------------

--
-- Table structure for table `ci_profile`
--

CREATE TABLE IF NOT EXISTS `ci_profile` (
`id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sales_pitch` longtext NOT NULL,
  `about_you` longtext NOT NULL,
  `about_investment` longtext NOT NULL,
  `country` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip_code` varchar(255) NOT NULL,
  `youtube_link` longtext NOT NULL,
  `updated_on` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_profile`
--

INSERT INTO `ci_profile` (`id`, `user_id`, `sales_pitch`, `about_you`, `about_investment`, `country`, `state`, `zip_code`, `youtube_link`, `updated_on`) VALUES
(1, 10, 'In publishing and graphic design, lorem ipsum (derived from Latin dolorem ipsum, translated as "pain itself") is a filler text commonly used to demonstrate the graphic elements of a document or visual presentation. Replacing meaningful content with placeholder text allows viewers to focus on graphic aspects such as font, typography, and page layout without being distracted by the content. It also reduces the need for the designer to come up with meaningful text, as they can instead use quickly', 'In publishing and graphic design, lorem ipsum (derived from Latin dolorem ipsum, translated as "pain itself") is a filler text commonly used to demonstrate the graphic elements of a document or visual presentation. Replacing meaningful content with placeholder text allows viewers to focus on graphic aspects such as font, typography, and page layout without being distracted by the content. It also reduces the need for the designer to come up with meaningful text, as they can instead use quickly-g', 'In publishing and graphic design, lorem ipsum (derived from Latin dolorem ipsum, translated as "pain itself") is a filler text commonly used to demonstrate the graphic elements of a document or visual presentation. Replacing meaningful content with placeholder text allows viewers to focus on graphic aspects such as font, typography, and page layout without being distracted by the content. It also reduces the need for the designer to come up with meaningful text, as they can instead use quick', '', '', '', 'https://www.youtube.com/watch?v=BIYybqS4LPg', '2015-07-17 10:07:39'),
(2, 1, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '', '', '', 'https://www.youtube.com/watch?v=VSImvK0De3I', '2015-07-17 12:15:06'),
(3, 8, 'rrrrFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF', 'rrrrrHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHHH', 'rrrrrrKKKKLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKKK', '', '', '', 'https://www.youtube.com/watch?v=Gzj7zP5BXdc', '2015-09-16 12:46:09'),
(4, 11, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting', '', '', '', 'https://www.youtube.com/watch?v=1qGgtmDXMaI', '2015-07-18 07:53:06'),
(5, 9, 'aaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa', '', '', '', '', '2015-07-18 15:06:08'),
(6, 12, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '', '', '', 'https://www.youtube.com/watch?v=_CrbO82pb10', '2015-07-27 07:22:46'),
(7, 3, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', '', '', '', 'https://www.youtube.com/watch?v=_CrbO82pb10', '2015-07-27 09:42:30'),
(8, 13, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book', '', '', '', 'https://www.youtube.com/watch?v=pNL4xGTT0Mk&feature=youtu.be', '2015-07-29 06:28:00'),
(9, 2, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.', '', '', '', '', '2015-07-30 05:32:56');

-- --------------------------------------------------------

--
-- Table structure for table `ci_property_headlines`
--

CREATE TABLE IF NOT EXISTS `ci_property_headlines` (
`HeadlineID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Headline` varchar(255) NOT NULL,
  `SubHeadline1` varchar(255) NOT NULL,
  `SubHeadline2` varchar(255) NOT NULL,
  `SubHeadline3` varchar(255) NOT NULL,
  `SubHeadline4` varchar(255) NOT NULL,
  `SubHeadline5` varchar(255) NOT NULL,
  `UnitMix` varchar(255) NOT NULL,
  `DescProp` longtext NOT NULL,
  `AreaNotes` longtext NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_property_headlines`
--

INSERT INTO `ci_property_headlines` (`HeadlineID`, `PropertyID`, `UserID`, `Headline`, `SubHeadline1`, `SubHeadline2`, `SubHeadline3`, `SubHeadline4`, `SubHeadline5`, `UnitMix`, `DescProp`, `AreaNotes`, `CreatedOn`) VALUES
(1, 1448009806, 3, 'Headline', 'Sub Headline 1', 'Sub Headline 2', 'Sub Headline 3', 'Sub Headline 4', 'Sub Headline 5', '5', 'Desciption of Property', 'Notes on Area', '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_property_information`
--

CREATE TABLE IF NOT EXISTS `ci_property_information` (
`InfoID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `PropertyName` varchar(255) NOT NULL,
  `PropertyStreetAddress` varchar(255) NOT NULL,
  `PropertyCityTown` varchar(255) NOT NULL,
  `PropertyStateProvince` varchar(255) NOT NULL,
  `PropertyZipCode` varchar(255) NOT NULL,
  `PropertyCountry` varchar(255) NOT NULL,
  `PropertyDescription` longtext NOT NULL,
  `WorkNeeded` longtext NOT NULL,
  `PropertyFeatures` longtext NOT NULL,
  `Bedrooms` varchar(255) NOT NULL,
  `Bathrooms` varchar(255) NOT NULL,
  `SquareFeet` varchar(255) NOT NULL,
  `YearBuilt` varchar(255) NOT NULL,
  `Submitted` enum('Yes','No') NOT NULL DEFAULT 'No',
  `CreatedOn` datetime NOT NULL,
  `ProposalStatus` enum('draft','publish') NOT NULL DEFAULT 'draft'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `ci_property_information`
--

INSERT INTO `ci_property_information` (`InfoID`, `PropertyID`, `UserID`, `PropertyName`, `PropertyStreetAddress`, `PropertyCityTown`, `PropertyStateProvince`, `PropertyZipCode`, `PropertyCountry`, `PropertyDescription`, `WorkNeeded`, `PropertyFeatures`, `Bedrooms`, `Bathrooms`, `SquareFeet`, `YearBuilt`, `Submitted`, `CreatedOn`, `ProposalStatus`) VALUES
(1, 1448009806, 3, 'VIP Tower', '225 VIP Road', 'Kolkata', 'WB', '700059', 'IN', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s.', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s.', '3', '3', '1520', '1980', 'No', '2015-11-20 09:56:46', 'publish');

-- --------------------------------------------------------

--
-- Table structure for table `ci_property_photos`
--

CREATE TABLE IF NOT EXISTS `ci_property_photos` (
`PhotoID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Photos` longtext NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_property_photos`
--

INSERT INTO `ci_property_photos` (`PhotoID`, `PropertyID`, `UserID`, `Photos`, `CreatedOn`) VALUES
(1, 1448009806, 3, 'http://localhost/cblist/assets/proposals/3_1448010262973.jpg|http://localhost/cblist/assets/proposals/3_2896020524666.jpg|http://localhost/cblist/assets/proposals/3_4344030786529.jpg', '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_proposal_view`
--

CREATE TABLE IF NOT EXISTS `ci_proposal_view` (
`id` int(11) NOT NULL,
  `investor_id` int(11) NOT NULL,
  `cashlender_id` int(11) NOT NULL,
  `proposal_id` int(11) NOT NULL,
  `download_link` longtext NOT NULL,
  `downloaded` varchar(16) NOT NULL,
  `download_count` int(11) NOT NULL,
  `visit_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ci_purchase_assumption`
--

CREATE TABLE IF NOT EXISTS `ci_purchase_assumption` (
`PurchaseID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `PurchasePrice` double NOT NULL,
  `ClosingCosts` double NOT NULL,
  `HoldingCosts` double NOT NULL,
  `FinanceClosingHolding` varchar(255) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_purchase_assumption`
--

INSERT INTO `ci_purchase_assumption` (`PurchaseID`, `PropertyID`, `UserID`, `PurchasePrice`, `ClosingCosts`, `HoldingCosts`, `FinanceClosingHolding`, `CreatedOn`) VALUES
(1, 1448009806, 3, 450000, 4750, 200, 'Yes', '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_refi_analysis`
--

CREATE TABLE IF NOT EXISTS `ci_refi_analysis` (
`RefiID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `RefiHoldingCosts` double NOT NULL,
  `ActualRefiBudget` double NOT NULL,
  `MonthsRefiRehab` double NOT NULL,
  `ARVRent` double NOT NULL,
  `MonthsToRefi` double NOT NULL,
  `TotalCapitalNeededRent` double NOT NULL,
  `MaxFinancedRefi` double NOT NULL,
  `ActualFinancedRent` double NOT NULL,
  `PointsInterestRolledupRent` double NOT NULL,
  `TotalLoanRent` double NOT NULL,
  `CashRequiredRent` double NOT NULL,
  `TotalCostBasisRent` double NOT NULL,
  `RefiPercent` double NOT NULL,
  `PermanentRate` double NOT NULL,
  `Amortization` double NOT NULL,
  `RefiDiscountPoints` double NOT NULL,
  `OperatingIncome` double NOT NULL,
  `OperatingExpenses` double NOT NULL,
  `NOIMonthly` double NOT NULL,
  `NewPayment` double NOT NULL,
  `RefiAmount` double NOT NULL,
  `CashOutRefi` double NOT NULL,
  `RefiProfit` double NOT NULL,
  `RefiROI` varchar(50) NOT NULL,
  `CashTiedUP` double NOT NULL,
  `EquityLeft` double NOT NULL,
  `CashflowMonthly` double NOT NULL,
  `RefiROIAnnual` varchar(50) NOT NULL,
  `DCR` double NOT NULL,
  `PaybackPeriod` double NOT NULL,
  `CapRateARV` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_refi_analysis`
--

INSERT INTO `ci_refi_analysis` (`RefiID`, `PropertyID`, `UserID`, `RefiHoldingCosts`, `ActualRefiBudget`, `MonthsRefiRehab`, `ARVRent`, `MonthsToRefi`, `TotalCapitalNeededRent`, `MaxFinancedRefi`, `ActualFinancedRent`, `PointsInterestRolledupRent`, `TotalLoanRent`, `CashRequiredRent`, `TotalCostBasisRent`, `RefiPercent`, `PermanentRate`, `Amortization`, `RefiDiscountPoints`, `OperatingIncome`, `OperatingExpenses`, `NOIMonthly`, `NewPayment`, `RefiAmount`, `CashOutRefi`, `RefiProfit`, `RefiROI`, `CashTiedUP`, `EquityLeft`, `CashflowMonthly`, `RefiROIAnnual`, `DCR`, `PaybackPeriod`, `CapRateARV`, `CreatedOn`) VALUES
(1, 1448009806, 3, 1200, 37000, 3, 500000, 3, 492950, 350000, 350000, 5950, 355950, 177320, 533270, 80, 6.5, 30, 3, 4500, 900, 3600, -2528.27, 400000, 32050, 0, '0.00', 145270, 100000, 1071.73, '8.85', 1.42, 11.3, 8.64, '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_refi_budget`
--

CREATE TABLE IF NOT EXISTS `ci_refi_budget` (
`RefiBudgetID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `PreliminaryWork` varchar(255) NOT NULL,
  `RefiRehabBudgetMethod` varchar(255) NOT NULL,
  `RefiRehabDrawSelection` varchar(255) NOT NULL,
  `LumpSumRefiBudget` double NOT NULL,
  `DetailsNotes` varchar(255) NOT NULL,
  `SqFt` double NOT NULL,
  `Quantity` double NOT NULL,
  `Rate` double NOT NULL,
  `Bid1` double NOT NULL,
  `Bid2` double NOT NULL,
  `Bid3` double NOT NULL,
  `Budget` double NOT NULL,
  `MonthPaid` int(11) NOT NULL,
  `TotalBid1Total` double NOT NULL,
  `TotalBid2Total` double NOT NULL,
  `TotalBid3Total` double NOT NULL,
  `DetailedRefiBudget` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_refi_budget`
--

INSERT INTO `ci_refi_budget` (`RefiBudgetID`, `PropertyID`, `UserID`, `PreliminaryWork`, `RefiRehabBudgetMethod`, `RefiRehabDrawSelection`, `LumpSumRefiBudget`, `DetailsNotes`, `SqFt`, `Quantity`, `Rate`, `Bid1`, `Bid2`, `Bid3`, `Budget`, `MonthPaid`, `TotalBid1Total`, `TotalBid2Total`, `TotalBid3Total`, `DetailedRefiBudget`, `CreatedOn`) VALUES
(1, 1448009806, 3, 'Demo', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 15000, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(2, 1448009806, 3, 'Architectural fees', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 10000, 2, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(3, 1448009806, 3, 'Master Building Permit', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 12000, 3, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(4, 1448009806, 3, '&nbsp;&nbsp;&nbsp;Plumbing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(5, 1448009806, 3, '&nbsp;&nbsp;&nbsp;Electrical', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(6, 1448009806, 3, '&nbsp;&nbsp;&nbsp;HVAC', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(7, 1448009806, 3, 'Dumpster', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(8, 1448009806, 3, 'Other', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(9, 1448009806, 3, 'Wall Framing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(10, 1448009806, 3, 'Floor Framing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(11, 1448009806, 3, 'Ceiling Framing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(12, 1448009806, 3, 'Electrical', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(13, 1448009806, 3, 'Plumbing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(14, 1448009806, 3, 'HVAC', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(15, 1448009806, 3, 'Flooring', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(16, 1448009806, 3, 'Sheetrock', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(17, 1448009806, 3, 'Windows', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(18, 1448009806, 3, 'Interior Doors', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(19, 1448009806, 3, 'Trim', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(20, 1448009806, 3, 'Bathroom Vanities', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(21, 1448009806, 3, 'Bathroom Fixtures', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(22, 1448009806, 3, 'Kitchen Cabinets', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(23, 1448009806, 3, 'Labor to install Kitchen', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(24, 1448009806, 3, 'Floor Coverings 1', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(25, 1448009806, 3, 'Floor Coverings 2', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(26, 1448009806, 3, 'Floor Coverings 3', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(27, 1448009806, 3, 'Interior Painting', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(28, 1448009806, 3, 'Light Fixtures', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(29, 1448009806, 3, 'Other Fixtures', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(30, 1448009806, 3, 'Appliances', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(31, 1448009806, 3, 'Other Interior', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(32, 1448009806, 3, 'Exterior Trim', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(33, 1448009806, 3, 'Exterior Doors', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(34, 1448009806, 3, 'Porches', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(35, 1448009806, 3, 'Siding', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(36, 1448009806, 3, 'Exterior Painting', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(37, 1448009806, 3, 'Roof', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(38, 1448009806, 3, 'Gutters, Downspouts', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(39, 1448009806, 3, 'Fencing', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(40, 1448009806, 3, 'Landscaping', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(41, 1448009806, 3, 'Driveway, concrete work', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(42, 1448009806, 3, 'Foudation Work', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(43, 1448009806, 3, 'Brick Pointing, replacement', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(44, 1448009806, 3, 'Other Exterior', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(45, 1448009806, 3, 'Contingency', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(46, 1448009806, 3, 'GCFee', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(47, 1448009806, 3, 'Cleanup', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46'),
(48, 1448009806, 3, 'Others', 'Detailed Input', 'Fund Rehab in Draws', 0, '', 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 37000, '2015-11-20 09:56:46');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_advance`
--

CREATE TABLE IF NOT EXISTS `ci_rental_advance` (
`RentalAdvanceID` int(11) NOT NULL,
  `PropertyID` int(11) DEFAULT NULL,
  `UserID` int(11) DEFAULT NULL,
  `OverrideRents` varchar(45) DEFAULT NULL,
  `Year_0` varchar(45) DEFAULT NULL,
  `Year_1` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_1` double DEFAULT NULL,
  `IndividualRent_Year_2_1` double DEFAULT NULL,
  `IndividualRent_Year_3_1` double DEFAULT NULL,
  `IndividualRent_Year_4_1` double DEFAULT NULL,
  `IndividualRent_Year_5_1` double DEFAULT NULL,
  `IndividualRent_Year_6_1` double DEFAULT NULL,
  `IndividualRent_Year_7_1` double DEFAULT NULL,
  `IndividualRent_Year_8_1` double DEFAULT NULL,
  `IndividualRent_Year_9_1` double DEFAULT NULL,
  `IndividualRent_Year_10_1` double DEFAULT NULL,
  `IndividualRent_Year_11_1` double DEFAULT NULL,
  `IndividualRent_Year_12_1` double DEFAULT NULL,
  `IndividualRent_Total_1` double DEFAULT NULL,
  `OverrideVacancyRates_Year_1` double DEFAULT NULL,
  `OverrideConcessions_Year_1` double DEFAULT NULL,
  `CapitalImprovementSchedule_1` double DEFAULT NULL,
  `Purpose_1` varchar(150) DEFAULT NULL,
  `FairMarketValue_1` double DEFAULT NULL,
  `TotalCashOutlay_1` double DEFAULT NULL,
  `Year_2` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_2` double DEFAULT NULL,
  `IndividualRent_Year_2_2` double DEFAULT NULL,
  `IndividualRent_Year_3_2` double DEFAULT NULL,
  `IndividualRent_Year_4_2` double DEFAULT NULL,
  `IndividualRent_Year_5_2` double DEFAULT NULL,
  `IndividualRent_Year_6_2` double DEFAULT NULL,
  `IndividualRent_Year_7_2` double DEFAULT NULL,
  `IndividualRent_Year_8_2` double DEFAULT NULL,
  `IndividualRent_Year_9_2` double DEFAULT NULL,
  `IndividualRent_Year_10_2` double DEFAULT NULL,
  `IndividualRent_Year_11_2` double DEFAULT NULL,
  `IndividualRent_Year_12_2` double DEFAULT NULL,
  `IndividualRent_Total_2` double DEFAULT NULL,
  `OverrideVacancyRates_Year_2` double DEFAULT NULL,
  `OverrideConcessions_Year_2` double DEFAULT NULL,
  `CapitalImprovementSchedule_2` double DEFAULT NULL,
  `Purpose_2` varchar(150) DEFAULT NULL,
  `FairMarketValue_2` double DEFAULT NULL,
  `TotalCashOutlay_2` double DEFAULT NULL,
  `Year_3` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_3` double DEFAULT NULL,
  `IndividualRent_Year_2_3` double DEFAULT NULL,
  `IndividualRent_Year_3_3` double DEFAULT NULL,
  `IndividualRent_Year_4_3` double DEFAULT NULL,
  `IndividualRent_Year_5_3` double DEFAULT NULL,
  `IndividualRent_Year_6_3` double DEFAULT NULL,
  `IndividualRent_Year_7_3` double DEFAULT NULL,
  `IndividualRent_Year_8_3` double DEFAULT NULL,
  `IndividualRent_Year_9_3` double DEFAULT NULL,
  `IndividualRent_Year_10_3` double DEFAULT NULL,
  `IndividualRent_Year_11_3` double DEFAULT NULL,
  `IndividualRent_Year_12_3` double DEFAULT NULL,
  `IndividualRent_Total_3` double DEFAULT NULL,
  `OverrideVacancyRates_Year_3` double DEFAULT NULL,
  `OverrideConcessions_Year_3` double DEFAULT NULL,
  `CapitalImprovementSchedule_3` double DEFAULT NULL,
  `Purpose_3` varchar(150) DEFAULT NULL,
  `FairMarketValue_3` double DEFAULT NULL,
  `TotalCashOutlay_3` double DEFAULT NULL,
  `Year_4` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_4` double DEFAULT NULL,
  `IndividualRent_Year_2_4` double DEFAULT NULL,
  `IndividualRent_Year_3_4` double DEFAULT NULL,
  `IndividualRent_Year_4_4` double DEFAULT NULL,
  `IndividualRent_Year_5_4` double DEFAULT NULL,
  `IndividualRent_Year_6_4` double DEFAULT NULL,
  `IndividualRent_Year_7_4` double DEFAULT NULL,
  `IndividualRent_Year_8_4` double DEFAULT NULL,
  `IndividualRent_Year_9_4` double DEFAULT NULL,
  `IndividualRent_Year_10_4` double DEFAULT NULL,
  `IndividualRent_Year_11_4` double DEFAULT NULL,
  `IndividualRent_Year_12_4` double DEFAULT NULL,
  `IndividualRent_Total_4` double DEFAULT NULL,
  `OverrideVacancyRates_Year_4` double DEFAULT NULL,
  `OverrideConcessions_Year_4` double DEFAULT NULL,
  `CapitalImprovementSchedule_4` double DEFAULT NULL,
  `Purpose_4` varchar(150) DEFAULT NULL,
  `FairMarketValue_4` double DEFAULT NULL,
  `TotalCashOutlay_4` double DEFAULT NULL,
  `Year_5` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_5` double DEFAULT NULL,
  `IndividualRent_Year_2_5` double DEFAULT NULL,
  `IndividualRent_Year_3_5` double DEFAULT NULL,
  `IndividualRent_Year_4_5` double DEFAULT NULL,
  `IndividualRent_Year_5_5` double DEFAULT NULL,
  `IndividualRent_Year_6_5` double DEFAULT NULL,
  `IndividualRent_Year_7_5` double DEFAULT NULL,
  `IndividualRent_Year_8_5` double DEFAULT NULL,
  `IndividualRent_Year_9_5` double DEFAULT NULL,
  `IndividualRent_Year_10_5` double DEFAULT NULL,
  `IndividualRent_Year_11_5` double DEFAULT NULL,
  `IndividualRent_Year_12_5` double DEFAULT NULL,
  `IndividualRent_Total_5` double DEFAULT NULL,
  `OverrideVacancyRates_Year_5` double DEFAULT NULL,
  `OverrideConcessions_Year_5` double DEFAULT NULL,
  `CapitalImprovementSchedule_5` double DEFAULT NULL,
  `Purpose_5` varchar(150) DEFAULT NULL,
  `FairMarketValue_5` double DEFAULT NULL,
  `TotalCashOutlay_5` double DEFAULT NULL,
  `Year_6` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_6` double DEFAULT NULL,
  `IndividualRent_Year_2_6` double DEFAULT NULL,
  `IndividualRent_Year_3_6` double DEFAULT NULL,
  `IndividualRent_Year_4_6` double DEFAULT NULL,
  `IndividualRent_Year_5_6` double DEFAULT NULL,
  `IndividualRent_Year_6_6` double DEFAULT NULL,
  `IndividualRent_Year_7_6` double DEFAULT NULL,
  `IndividualRent_Year_8_6` double DEFAULT NULL,
  `IndividualRent_Year_9_6` double DEFAULT NULL,
  `IndividualRent_Year_10_6` double DEFAULT NULL,
  `IndividualRent_Year_11_6` double DEFAULT NULL,
  `IndividualRent_Year_12_6` double DEFAULT NULL,
  `IndividualRent_Total_6` double DEFAULT NULL,
  `OverrideVacancyRates_Year_6` double DEFAULT NULL,
  `OverrideConcessions_Year_6` double DEFAULT NULL,
  `CapitalImprovementSchedule_6` double DEFAULT NULL,
  `Purpose_6` varchar(150) DEFAULT NULL,
  `FairMarketValue_6` double DEFAULT NULL,
  `TotalCashOutlay_6` double DEFAULT NULL,
  `Year_7` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_7` double DEFAULT NULL,
  `IndividualRent_Year_2_7` double DEFAULT NULL,
  `IndividualRent_Year_3_7` double DEFAULT NULL,
  `IndividualRent_Year_4_7` double DEFAULT NULL,
  `IndividualRent_Year_5_7` double DEFAULT NULL,
  `IndividualRent_Year_6_7` double DEFAULT NULL,
  `IndividualRent_Year_7_7` double DEFAULT NULL,
  `IndividualRent_Year_8_7` double DEFAULT NULL,
  `IndividualRent_Year_9_7` double DEFAULT NULL,
  `IndividualRent_Year_10_7` double DEFAULT NULL,
  `IndividualRent_Year_11_7` double DEFAULT NULL,
  `IndividualRent_Year_12_7` double DEFAULT NULL,
  `IndividualRent_Total_7` double DEFAULT NULL,
  `OverrideVacancyRates_Year_7` double DEFAULT NULL,
  `OverrideConcessions_Year_7` double DEFAULT NULL,
  `CapitalImprovementSchedule_7` double DEFAULT NULL,
  `Purpose_7` varchar(150) DEFAULT NULL,
  `FairMarketValue_7` double DEFAULT NULL,
  `TotalCashOutlay_7` double DEFAULT NULL,
  `Year_8` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_8` double DEFAULT NULL,
  `IndividualRent_Year_2_8` double DEFAULT NULL,
  `IndividualRent_Year_3_8` double DEFAULT NULL,
  `IndividualRent_Year_4_8` double DEFAULT NULL,
  `IndividualRent_Year_5_8` double DEFAULT NULL,
  `IndividualRent_Year_6_8` double DEFAULT NULL,
  `IndividualRent_Year_7_8` double DEFAULT NULL,
  `IndividualRent_Year_8_8` double DEFAULT NULL,
  `IndividualRent_Year_9_8` double DEFAULT NULL,
  `IndividualRent_Year_10_8` double DEFAULT NULL,
  `IndividualRent_Year_11_8` double DEFAULT NULL,
  `IndividualRent_Year_12_8` double DEFAULT NULL,
  `IndividualRent_Total_8` double DEFAULT NULL,
  `OverrideVacancyRates_Year_8` double DEFAULT NULL,
  `OverrideConcessions_Year_8` double DEFAULT NULL,
  `CapitalImprovementSchedule_8` double DEFAULT NULL,
  `Purpose_8` varchar(150) DEFAULT NULL,
  `FairMarketValue_8` double DEFAULT NULL,
  `TotalCashOutlay_8` double DEFAULT NULL,
  `Year_9` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_9` double DEFAULT NULL,
  `IndividualRent_Year_2_9` double DEFAULT NULL,
  `IndividualRent_Year_3_9` double DEFAULT NULL,
  `IndividualRent_Year_4_9` double DEFAULT NULL,
  `IndividualRent_Year_5_9` double DEFAULT NULL,
  `IndividualRent_Year_6_9` double DEFAULT NULL,
  `IndividualRent_Year_7_9` double DEFAULT NULL,
  `IndividualRent_Year_8_9` double DEFAULT NULL,
  `IndividualRent_Year_9_9` double DEFAULT NULL,
  `IndividualRent_Year_10_9` double DEFAULT NULL,
  `IndividualRent_Year_11_9` double DEFAULT NULL,
  `IndividualRent_Year_12_9` double DEFAULT NULL,
  `IndividualRent_Total_9` double DEFAULT NULL,
  `OverrideVacancyRates_Year_9` double DEFAULT NULL,
  `OverrideConcessions_Year_9` double DEFAULT NULL,
  `CapitalImprovementSchedule_9` double DEFAULT NULL,
  `Purpose_9` varchar(150) DEFAULT NULL,
  `FairMarketValue_9` double DEFAULT NULL,
  `TotalCashOutlay_9` double DEFAULT NULL,
  `Year_10` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_10` double DEFAULT NULL,
  `IndividualRent_Year_2_10` double DEFAULT NULL,
  `IndividualRent_Year_3_10` double DEFAULT NULL,
  `IndividualRent_Year_4_10` double DEFAULT NULL,
  `IndividualRent_Year_5_10` double DEFAULT NULL,
  `IndividualRent_Year_6_10` double DEFAULT NULL,
  `IndividualRent_Year_7_10` double DEFAULT NULL,
  `IndividualRent_Year_8_10` double DEFAULT NULL,
  `IndividualRent_Year_9_10` double DEFAULT NULL,
  `IndividualRent_Year_10_10` double DEFAULT NULL,
  `IndividualRent_Year_11_10` double DEFAULT NULL,
  `IndividualRent_Year_12_10` double DEFAULT NULL,
  `IndividualRent_Total_10` double DEFAULT NULL,
  `OverrideVacancyRates_Year_10` double DEFAULT NULL,
  `OverrideConcessions_Year_10` double DEFAULT NULL,
  `CapitalImprovementSchedule_10` double DEFAULT NULL,
  `Purpose_10` varchar(150) DEFAULT NULL,
  `FairMarketValue_10` double DEFAULT NULL,
  `TotalCashOutlay_10` double DEFAULT NULL,
  `Year_11` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_11` double DEFAULT NULL,
  `IndividualRent_Year_2_11` double DEFAULT NULL,
  `IndividualRent_Year_3_11` double DEFAULT NULL,
  `IndividualRent_Year_4_11` double DEFAULT NULL,
  `IndividualRent_Year_5_11` double DEFAULT NULL,
  `IndividualRent_Year_6_11` double DEFAULT NULL,
  `IndividualRent_Year_7_11` double DEFAULT NULL,
  `IndividualRent_Year_8_11` double DEFAULT NULL,
  `IndividualRent_Year_9_11` double DEFAULT NULL,
  `IndividualRent_Year_10_11` double DEFAULT NULL,
  `IndividualRent_Year_11_11` double DEFAULT NULL,
  `IndividualRent_Year_12_11` double DEFAULT NULL,
  `IndividualRent_Total_11` double DEFAULT NULL,
  `OverrideVacancyRates_Year_11` double DEFAULT NULL,
  `OverrideConcessions_Year_11` double DEFAULT NULL,
  `CapitalImprovementSchedule_11` double DEFAULT NULL,
  `Purpose_11` varchar(150) DEFAULT NULL,
  `FairMarketValue_11` double DEFAULT NULL,
  `TotalCashOutlay_11` double DEFAULT NULL,
  `Year_12` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_12` double DEFAULT NULL,
  `IndividualRent_Year_2_12` double DEFAULT NULL,
  `IndividualRent_Year_3_12` double DEFAULT NULL,
  `IndividualRent_Year_4_12` double DEFAULT NULL,
  `IndividualRent_Year_5_12` double DEFAULT NULL,
  `IndividualRent_Year_6_12` double DEFAULT NULL,
  `IndividualRent_Year_7_12` double DEFAULT NULL,
  `IndividualRent_Year_8_12` double DEFAULT NULL,
  `IndividualRent_Year_9_12` double DEFAULT NULL,
  `IndividualRent_Year_10_12` double DEFAULT NULL,
  `IndividualRent_Year_11_12` double DEFAULT NULL,
  `IndividualRent_Year_12_12` double DEFAULT NULL,
  `IndividualRent_Total_12` double DEFAULT NULL,
  `OverrideVacancyRates_Year_12` double DEFAULT NULL,
  `OverrideConcessions_Year_12` double DEFAULT NULL,
  `CapitalImprovementSchedule_12` double DEFAULT NULL,
  `Purpose_12` varchar(150) DEFAULT NULL,
  `FairMarketValue_12` double DEFAULT NULL,
  `TotalCashOutlay_12` double DEFAULT NULL,
  `Year_13` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_13` double DEFAULT NULL,
  `IndividualRent_Year_2_13` double DEFAULT NULL,
  `IndividualRent_Year_3_13` double DEFAULT NULL,
  `IndividualRent_Year_4_13` double DEFAULT NULL,
  `IndividualRent_Year_5_13` double DEFAULT NULL,
  `IndividualRent_Year_6_13` double DEFAULT NULL,
  `IndividualRent_Year_7_13` double DEFAULT NULL,
  `IndividualRent_Year_8_13` double DEFAULT NULL,
  `IndividualRent_Year_9_13` double DEFAULT NULL,
  `IndividualRent_Year_10_13` double DEFAULT NULL,
  `IndividualRent_Year_11_13` double DEFAULT NULL,
  `IndividualRent_Year_12_13` double DEFAULT NULL,
  `IndividualRent_Total_13` double DEFAULT NULL,
  `OverrideVacancyRates_Year_13` double DEFAULT NULL,
  `OverrideConcessions_Year_13` double DEFAULT NULL,
  `CapitalImprovementSchedule_13` double DEFAULT NULL,
  `Purpose_13` varchar(150) DEFAULT NULL,
  `FairMarketValue_13` double DEFAULT NULL,
  `TotalCashOutlay_13` double DEFAULT NULL,
  `Year_14` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_14` double DEFAULT NULL,
  `IndividualRent_Year_2_14` double DEFAULT NULL,
  `IndividualRent_Year_3_14` double DEFAULT NULL,
  `IndividualRent_Year_4_14` double DEFAULT NULL,
  `IndividualRent_Year_5_14` double DEFAULT NULL,
  `IndividualRent_Year_6_14` double DEFAULT NULL,
  `IndividualRent_Year_7_14` double DEFAULT NULL,
  `IndividualRent_Year_8_14` double DEFAULT NULL,
  `IndividualRent_Year_9_14` double DEFAULT NULL,
  `IndividualRent_Year_10_14` double DEFAULT NULL,
  `IndividualRent_Year_11_14` double DEFAULT NULL,
  `IndividualRent_Year_12_14` double DEFAULT NULL,
  `IndividualRent_Total_14` double DEFAULT NULL,
  `OverrideVacancyRates_Year_14` double DEFAULT NULL,
  `OverrideConcessions_Year_14` double DEFAULT NULL,
  `CapitalImprovementSchedule_14` double DEFAULT NULL,
  `Purpose_14` varchar(150) DEFAULT NULL,
  `FairMarketValue_14` double DEFAULT NULL,
  `TotalCashOutlay_14` double DEFAULT NULL,
  `Year_15` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_15` double DEFAULT NULL,
  `IndividualRent_Year_2_15` double DEFAULT NULL,
  `IndividualRent_Year_3_15` double DEFAULT NULL,
  `IndividualRent_Year_4_15` double DEFAULT NULL,
  `IndividualRent_Year_5_15` double DEFAULT NULL,
  `IndividualRent_Year_6_15` double DEFAULT NULL,
  `IndividualRent_Year_7_15` double DEFAULT NULL,
  `IndividualRent_Year_8_15` double DEFAULT NULL,
  `IndividualRent_Year_9_15` double DEFAULT NULL,
  `IndividualRent_Year_10_15` double DEFAULT NULL,
  `IndividualRent_Year_11_15` double DEFAULT NULL,
  `IndividualRent_Year_12_15` double DEFAULT NULL,
  `IndividualRent_Total_15` double DEFAULT NULL,
  `OverrideVacancyRates_Year_15` double DEFAULT NULL,
  `OverrideConcessions_Year_15` double DEFAULT NULL,
  `CapitalImprovementSchedule_15` double DEFAULT NULL,
  `Purpose_15` varchar(150) DEFAULT NULL,
  `FairMarketValue_15` double DEFAULT NULL,
  `TotalCashOutlay_15` double DEFAULT NULL,
  `Year_16` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_16` double DEFAULT NULL,
  `IndividualRent_Year_2_16` double DEFAULT NULL,
  `IndividualRent_Year_3_16` double DEFAULT NULL,
  `IndividualRent_Year_4_16` double DEFAULT NULL,
  `IndividualRent_Year_5_16` double DEFAULT NULL,
  `IndividualRent_Year_6_16` double DEFAULT NULL,
  `IndividualRent_Year_7_16` double DEFAULT NULL,
  `IndividualRent_Year_8_16` double DEFAULT NULL,
  `IndividualRent_Year_9_16` double DEFAULT NULL,
  `IndividualRent_Year_10_16` double DEFAULT NULL,
  `IndividualRent_Year_11_16` double DEFAULT NULL,
  `IndividualRent_Year_12_16` double DEFAULT NULL,
  `IndividualRent_Total_16` double DEFAULT NULL,
  `OverrideVacancyRates_Year_16` double DEFAULT NULL,
  `OverrideConcessions_Year_16` double DEFAULT NULL,
  `CapitalImprovementSchedule_16` double DEFAULT NULL,
  `Purpose_16` varchar(150) DEFAULT NULL,
  `FairMarketValue_16` double DEFAULT NULL,
  `TotalCashOutlay_16` double DEFAULT NULL,
  `Year_17` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_17` double DEFAULT NULL,
  `IndividualRent_Year_2_17` double DEFAULT NULL,
  `IndividualRent_Year_3_17` double DEFAULT NULL,
  `IndividualRent_Year_4_17` double DEFAULT NULL,
  `IndividualRent_Year_5_17` double DEFAULT NULL,
  `IndividualRent_Year_6_17` double DEFAULT NULL,
  `IndividualRent_Year_7_17` double DEFAULT NULL,
  `IndividualRent_Year_8_17` double DEFAULT NULL,
  `IndividualRent_Year_9_17` double DEFAULT NULL,
  `IndividualRent_Year_10_17` double DEFAULT NULL,
  `IndividualRent_Year_11_17` double DEFAULT NULL,
  `IndividualRent_Year_12_17` double DEFAULT NULL,
  `IndividualRent_Total_17` double DEFAULT NULL,
  `OverrideVacancyRates_Year_17` double DEFAULT NULL,
  `OverrideConcessions_Year_17` double DEFAULT NULL,
  `CapitalImprovementSchedule_17` double DEFAULT NULL,
  `Purpose_17` varchar(150) DEFAULT NULL,
  `FairMarketValue_17` double DEFAULT NULL,
  `TotalCashOutlay_17` double DEFAULT NULL,
  `Year_18` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_18` double DEFAULT NULL,
  `IndividualRent_Year_2_18` double DEFAULT NULL,
  `IndividualRent_Year_3_18` double DEFAULT NULL,
  `IndividualRent_Year_4_18` double DEFAULT NULL,
  `IndividualRent_Year_5_18` double DEFAULT NULL,
  `IndividualRent_Year_6_18` double DEFAULT NULL,
  `IndividualRent_Year_7_18` double DEFAULT NULL,
  `IndividualRent_Year_8_18` double DEFAULT NULL,
  `IndividualRent_Year_9_18` double DEFAULT NULL,
  `IndividualRent_Year_10_18` double DEFAULT NULL,
  `IndividualRent_Year_11_18` double DEFAULT NULL,
  `IndividualRent_Year_12_18` double DEFAULT NULL,
  `IndividualRent_Total_18` double DEFAULT NULL,
  `OverrideVacancyRates_Year_18` double DEFAULT NULL,
  `OverrideConcessions_Year_18` double DEFAULT NULL,
  `CapitalImprovementSchedule_18` double DEFAULT NULL,
  `Purpose_18` varchar(150) DEFAULT NULL,
  `FairMarketValue_18` double DEFAULT NULL,
  `TotalCashOutlay_18` double DEFAULT NULL,
  `Year_19` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_19` double DEFAULT NULL,
  `IndividualRent_Year_2_19` double DEFAULT NULL,
  `IndividualRent_Year_3_19` double DEFAULT NULL,
  `IndividualRent_Year_4_19` double DEFAULT NULL,
  `IndividualRent_Year_5_19` double DEFAULT NULL,
  `IndividualRent_Year_6_19` double DEFAULT NULL,
  `IndividualRent_Year_7_19` double DEFAULT NULL,
  `IndividualRent_Year_8_19` double DEFAULT NULL,
  `IndividualRent_Year_9_19` double DEFAULT NULL,
  `IndividualRent_Year_10_19` double DEFAULT NULL,
  `IndividualRent_Year_11_19` double DEFAULT NULL,
  `IndividualRent_Year_12_19` double DEFAULT NULL,
  `IndividualRent_Total_19` double DEFAULT NULL,
  `OverrideVacancyRates_Year_19` double DEFAULT NULL,
  `OverrideConcessions_Year_19` double DEFAULT NULL,
  `CapitalImprovementSchedule_19` double DEFAULT NULL,
  `Purpose_19` varchar(150) DEFAULT NULL,
  `FairMarketValue_19` double DEFAULT NULL,
  `TotalCashOutlay_19` double DEFAULT NULL,
  `Year_20` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_20` double DEFAULT NULL,
  `IndividualRent_Year_2_20` double DEFAULT NULL,
  `IndividualRent_Year_3_20` double DEFAULT NULL,
  `IndividualRent_Year_4_20` double DEFAULT NULL,
  `IndividualRent_Year_5_20` double DEFAULT NULL,
  `IndividualRent_Year_6_20` double DEFAULT NULL,
  `IndividualRent_Year_7_20` double DEFAULT NULL,
  `IndividualRent_Year_8_20` double DEFAULT NULL,
  `IndividualRent_Year_9_20` double DEFAULT NULL,
  `IndividualRent_Year_10_20` double DEFAULT NULL,
  `IndividualRent_Year_11_20` double DEFAULT NULL,
  `IndividualRent_Year_12_20` double DEFAULT NULL,
  `IndividualRent_Total_20` double DEFAULT NULL,
  `OverrideVacancyRates_Year_20` double DEFAULT NULL,
  `OverrideConcessions_Year_20` double DEFAULT NULL,
  `CapitalImprovementSchedule_20` double DEFAULT NULL,
  `Purpose_20` varchar(150) DEFAULT NULL,
  `FairMarketValue_20` double DEFAULT NULL,
  `TotalCashOutlay_20` double DEFAULT NULL,
  `Year_21` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_21` double DEFAULT NULL,
  `IndividualRent_Year_2_21` double DEFAULT NULL,
  `IndividualRent_Year_3_21` double DEFAULT NULL,
  `IndividualRent_Year_4_21` double DEFAULT NULL,
  `IndividualRent_Year_5_21` double DEFAULT NULL,
  `IndividualRent_Year_6_21` double DEFAULT NULL,
  `IndividualRent_Year_7_21` double DEFAULT NULL,
  `IndividualRent_Year_8_21` double DEFAULT NULL,
  `IndividualRent_Year_9_21` double DEFAULT NULL,
  `IndividualRent_Year_10_21` double DEFAULT NULL,
  `IndividualRent_Year_11_21` double DEFAULT NULL,
  `IndividualRent_Year_12_21` double DEFAULT NULL,
  `IndividualRent_Total_21` double DEFAULT NULL,
  `OverrideVacancyRates_Year_21` double DEFAULT NULL,
  `OverrideConcessions_Year_21` double DEFAULT NULL,
  `CapitalImprovementSchedule_21` double DEFAULT NULL,
  `Purpose_21` varchar(150) DEFAULT NULL,
  `FairMarketValue_21` double DEFAULT NULL,
  `TotalCashOutlay_21` double DEFAULT NULL,
  `Year_22` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_22` double DEFAULT NULL,
  `IndividualRent_Year_2_22` double DEFAULT NULL,
  `IndividualRent_Year_3_22` double DEFAULT NULL,
  `IndividualRent_Year_4_22` double DEFAULT NULL,
  `IndividualRent_Year_5_22` double DEFAULT NULL,
  `IndividualRent_Year_6_22` double DEFAULT NULL,
  `IndividualRent_Year_7_22` double DEFAULT NULL,
  `IndividualRent_Year_8_22` double DEFAULT NULL,
  `IndividualRent_Year_9_22` double DEFAULT NULL,
  `IndividualRent_Year_10_22` double DEFAULT NULL,
  `IndividualRent_Year_11_22` double DEFAULT NULL,
  `IndividualRent_Year_12_22` double DEFAULT NULL,
  `IndividualRent_Total_22` double DEFAULT NULL,
  `OverrideVacancyRates_Year_22` double DEFAULT NULL,
  `OverrideConcessions_Year_22` double DEFAULT NULL,
  `CapitalImprovementSchedule_22` double DEFAULT NULL,
  `Purpose_22` varchar(150) DEFAULT NULL,
  `FairMarketValue_22` double DEFAULT NULL,
  `TotalCashOutlay_22` double DEFAULT NULL,
  `Year_23` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_23` double DEFAULT NULL,
  `IndividualRent_Year_2_23` double DEFAULT NULL,
  `IndividualRent_Year_3_23` double DEFAULT NULL,
  `IndividualRent_Year_4_23` double DEFAULT NULL,
  `IndividualRent_Year_5_23` double DEFAULT NULL,
  `IndividualRent_Year_6_23` double DEFAULT NULL,
  `IndividualRent_Year_7_23` double DEFAULT NULL,
  `IndividualRent_Year_8_23` double DEFAULT NULL,
  `IndividualRent_Year_9_23` double DEFAULT NULL,
  `IndividualRent_Year_10_23` double DEFAULT NULL,
  `IndividualRent_Year_11_23` double DEFAULT NULL,
  `IndividualRent_Year_12_23` double DEFAULT NULL,
  `IndividualRent_Total_23` double DEFAULT NULL,
  `OverrideVacancyRates_Year_23` double DEFAULT NULL,
  `OverrideConcessions_Year_23` double DEFAULT NULL,
  `CapitalImprovementSchedule_23` double DEFAULT NULL,
  `Purpose_23` varchar(150) DEFAULT NULL,
  `FairMarketValue_23` double DEFAULT NULL,
  `TotalCashOutlay_23` double DEFAULT NULL,
  `Year_24` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_24` double DEFAULT NULL,
  `IndividualRent_Year_2_24` double DEFAULT NULL,
  `IndividualRent_Year_3_24` double DEFAULT NULL,
  `IndividualRent_Year_4_24` double DEFAULT NULL,
  `IndividualRent_Year_5_24` double DEFAULT NULL,
  `IndividualRent_Year_6_24` double DEFAULT NULL,
  `IndividualRent_Year_7_24` double DEFAULT NULL,
  `IndividualRent_Year_8_24` double DEFAULT NULL,
  `IndividualRent_Year_9_24` double DEFAULT NULL,
  `IndividualRent_Year_10_24` double DEFAULT NULL,
  `IndividualRent_Year_11_24` double DEFAULT NULL,
  `IndividualRent_Year_12_24` double DEFAULT NULL,
  `IndividualRent_Total_24` double DEFAULT NULL,
  `OverrideVacancyRates_Year_24` double DEFAULT NULL,
  `OverrideConcessions_Year_24` double DEFAULT NULL,
  `CapitalImprovementSchedule_24` double DEFAULT NULL,
  `Purpose_24` varchar(150) DEFAULT NULL,
  `FairMarketValue_24` double DEFAULT NULL,
  `TotalCashOutlay_24` double DEFAULT NULL,
  `Year_25` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_25` double DEFAULT NULL,
  `IndividualRent_Year_2_25` double DEFAULT NULL,
  `IndividualRent_Year_3_25` double DEFAULT NULL,
  `IndividualRent_Year_4_25` double DEFAULT NULL,
  `IndividualRent_Year_5_25` double DEFAULT NULL,
  `IndividualRent_Year_6_25` double DEFAULT NULL,
  `IndividualRent_Year_7_25` double DEFAULT NULL,
  `IndividualRent_Year_8_25` double DEFAULT NULL,
  `IndividualRent_Year_9_25` double DEFAULT NULL,
  `IndividualRent_Year_10_25` double DEFAULT NULL,
  `IndividualRent_Year_11_25` double DEFAULT NULL,
  `IndividualRent_Year_12_25` double DEFAULT NULL,
  `IndividualRent_Total_25` double DEFAULT NULL,
  `OverrideVacancyRates_Year_25` double DEFAULT NULL,
  `OverrideConcessions_Year_25` double DEFAULT NULL,
  `CapitalImprovementSchedule_25` double DEFAULT NULL,
  `Purpose_25` varchar(150) DEFAULT NULL,
  `FairMarketValue_25` double DEFAULT NULL,
  `TotalCashOutlay_25` double DEFAULT NULL,
  `Year_26` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_26` double DEFAULT NULL,
  `IndividualRent_Year_2_26` double DEFAULT NULL,
  `IndividualRent_Year_3_26` double DEFAULT NULL,
  `IndividualRent_Year_4_26` double DEFAULT NULL,
  `IndividualRent_Year_5_26` double DEFAULT NULL,
  `IndividualRent_Year_6_26` double DEFAULT NULL,
  `IndividualRent_Year_7_26` double DEFAULT NULL,
  `IndividualRent_Year_8_26` double DEFAULT NULL,
  `IndividualRent_Year_9_26` double DEFAULT NULL,
  `IndividualRent_Year_10_26` double DEFAULT NULL,
  `IndividualRent_Year_11_26` double DEFAULT NULL,
  `IndividualRent_Year_12_26` double DEFAULT NULL,
  `IndividualRent_Total_26` double DEFAULT NULL,
  `OverrideVacancyRates_Year_26` double DEFAULT NULL,
  `OverrideConcessions_Year_26` double DEFAULT NULL,
  `CapitalImprovementSchedule_26` double DEFAULT NULL,
  `Purpose_26` varchar(150) DEFAULT NULL,
  `FairMarketValue_26` double DEFAULT NULL,
  `TotalCashOutlay_26` double DEFAULT NULL,
  `Year_27` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_27` double DEFAULT NULL,
  `IndividualRent_Year_2_27` double DEFAULT NULL,
  `IndividualRent_Year_3_27` double DEFAULT NULL,
  `IndividualRent_Year_4_27` double DEFAULT NULL,
  `IndividualRent_Year_5_27` double DEFAULT NULL,
  `IndividualRent_Year_6_27` double DEFAULT NULL,
  `IndividualRent_Year_7_27` double DEFAULT NULL,
  `IndividualRent_Year_8_27` double DEFAULT NULL,
  `IndividualRent_Year_9_27` double DEFAULT NULL,
  `IndividualRent_Year_10_27` double DEFAULT NULL,
  `IndividualRent_Year_11_27` double DEFAULT NULL,
  `IndividualRent_Year_12_27` double DEFAULT NULL,
  `IndividualRent_Total_27` double DEFAULT NULL,
  `OverrideVacancyRates_Year_27` double DEFAULT NULL,
  `OverrideConcessions_Year_27` double DEFAULT NULL,
  `CapitalImprovementSchedule_27` double DEFAULT NULL,
  `Purpose_27` varchar(150) DEFAULT NULL,
  `FairMarketValue_27` double DEFAULT NULL,
  `TotalCashOutlay_27` double DEFAULT NULL,
  `Year_28` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_28` double DEFAULT NULL,
  `IndividualRent_Year_2_28` double DEFAULT NULL,
  `IndividualRent_Year_3_28` double DEFAULT NULL,
  `IndividualRent_Year_4_28` double DEFAULT NULL,
  `IndividualRent_Year_5_28` double DEFAULT NULL,
  `IndividualRent_Year_6_28` double DEFAULT NULL,
  `IndividualRent_Year_7_28` double DEFAULT NULL,
  `IndividualRent_Year_8_28` double DEFAULT NULL,
  `IndividualRent_Year_9_28` double DEFAULT NULL,
  `IndividualRent_Year_10_28` double DEFAULT NULL,
  `IndividualRent_Year_11_28` double DEFAULT NULL,
  `IndividualRent_Year_12_28` double DEFAULT NULL,
  `IndividualRent_Total_28` double DEFAULT NULL,
  `OverrideVacancyRates_Year_28` double DEFAULT NULL,
  `OverrideConcessions_Year_28` double DEFAULT NULL,
  `CapitalImprovementSchedule_28` double DEFAULT NULL,
  `Purpose_28` varchar(150) DEFAULT NULL,
  `FairMarketValue_28` double DEFAULT NULL,
  `TotalCashOutlay_28` double DEFAULT NULL,
  `Year_29` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_29` double DEFAULT NULL,
  `IndividualRent_Year_2_29` double DEFAULT NULL,
  `IndividualRent_Year_3_29` double DEFAULT NULL,
  `IndividualRent_Year_4_29` double DEFAULT NULL,
  `IndividualRent_Year_5_29` double DEFAULT NULL,
  `IndividualRent_Year_6_29` double DEFAULT NULL,
  `IndividualRent_Year_7_29` double DEFAULT NULL,
  `IndividualRent_Year_8_29` double DEFAULT NULL,
  `IndividualRent_Year_9_29` double DEFAULT NULL,
  `IndividualRent_Year_10_29` double DEFAULT NULL,
  `IndividualRent_Year_11_29` double DEFAULT NULL,
  `IndividualRent_Year_12_29` double DEFAULT NULL,
  `IndividualRent_Total_29` double DEFAULT NULL,
  `OverrideVacancyRates_Year_29` double DEFAULT NULL,
  `OverrideConcessions_Year_29` double DEFAULT NULL,
  `CapitalImprovementSchedule_29` double DEFAULT NULL,
  `Purpose_29` varchar(150) DEFAULT NULL,
  `FairMarketValue_29` double DEFAULT NULL,
  `TotalCashOutlay_29` double DEFAULT NULL,
  `Year_30` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_30` double DEFAULT NULL,
  `IndividualRent_Year_2_30` double DEFAULT NULL,
  `IndividualRent_Year_3_30` double DEFAULT NULL,
  `IndividualRent_Year_4_30` double DEFAULT NULL,
  `IndividualRent_Year_5_30` double DEFAULT NULL,
  `IndividualRent_Year_6_30` double DEFAULT NULL,
  `IndividualRent_Year_7_30` double DEFAULT NULL,
  `IndividualRent_Year_8_30` double DEFAULT NULL,
  `IndividualRent_Year_9_30` double DEFAULT NULL,
  `IndividualRent_Year_10_30` double DEFAULT NULL,
  `IndividualRent_Year_11_30` double DEFAULT NULL,
  `IndividualRent_Year_12_30` double DEFAULT NULL,
  `IndividualRent_Total_30` double DEFAULT NULL,
  `OverrideVacancyRates_Year_30` double DEFAULT NULL,
  `OverrideConcessions_Year_30` double DEFAULT NULL,
  `CapitalImprovementSchedule_30` double DEFAULT NULL,
  `Purpose_30` varchar(150) DEFAULT NULL,
  `FairMarketValue_30` double DEFAULT NULL,
  `TotalCashOutlay_30` double DEFAULT NULL,
  `Year_31` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_31` double DEFAULT NULL,
  `IndividualRent_Year_2_31` double DEFAULT NULL,
  `IndividualRent_Year_3_31` double DEFAULT NULL,
  `IndividualRent_Year_4_31` double DEFAULT NULL,
  `IndividualRent_Year_5_31` double DEFAULT NULL,
  `IndividualRent_Year_6_31` double DEFAULT NULL,
  `IndividualRent_Year_7_31` double DEFAULT NULL,
  `IndividualRent_Year_8_31` double DEFAULT NULL,
  `IndividualRent_Year_9_31` double DEFAULT NULL,
  `IndividualRent_Year_10_31` double DEFAULT NULL,
  `IndividualRent_Year_11_31` double DEFAULT NULL,
  `IndividualRent_Year_12_31` double DEFAULT NULL,
  `IndividualRent_Total_31` double DEFAULT NULL,
  `OverrideVacancyRates_Year_31` double DEFAULT NULL,
  `OverrideConcessions_Year_31` double DEFAULT NULL,
  `TotalCashOutlay_31` double DEFAULT NULL,
  `Year_32` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_32` double DEFAULT NULL,
  `IndividualRent_Year_2_32` double DEFAULT NULL,
  `IndividualRent_Year_3_32` double DEFAULT NULL,
  `IndividualRent_Year_4_32` double DEFAULT NULL,
  `IndividualRent_Year_5_32` double DEFAULT NULL,
  `IndividualRent_Year_6_32` double DEFAULT NULL,
  `IndividualRent_Year_7_32` double DEFAULT NULL,
  `IndividualRent_Year_8_32` double DEFAULT NULL,
  `IndividualRent_Year_9_32` double DEFAULT NULL,
  `IndividualRent_Year_10_32` double DEFAULT NULL,
  `IndividualRent_Year_11_32` double DEFAULT NULL,
  `IndividualRent_Year_12_32` double DEFAULT NULL,
  `IndividualRent_Total_32` double DEFAULT NULL,
  `OverrideVacancyRates_Year_32` double DEFAULT NULL,
  `OverrideConcessions_Year_32` double DEFAULT NULL,
  `TotalCashOutlay_32` double DEFAULT NULL,
  `Year_33` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_33` double DEFAULT NULL,
  `IndividualRent_Year_2_33` double DEFAULT NULL,
  `IndividualRent_Year_3_33` double DEFAULT NULL,
  `IndividualRent_Year_4_33` double DEFAULT NULL,
  `IndividualRent_Year_5_33` double DEFAULT NULL,
  `IndividualRent_Year_6_33` double DEFAULT NULL,
  `IndividualRent_Year_7_33` double DEFAULT NULL,
  `IndividualRent_Year_8_33` double DEFAULT NULL,
  `IndividualRent_Year_9_33` double DEFAULT NULL,
  `IndividualRent_Year_10_33` double DEFAULT NULL,
  `IndividualRent_Year_11_33` double DEFAULT NULL,
  `IndividualRent_Year_12_33` double DEFAULT NULL,
  `IndividualRent_Total_33` double DEFAULT NULL,
  `OverrideVacancyRates_Year_33` double DEFAULT NULL,
  `OverrideConcessions_Year_33` double DEFAULT NULL,
  `TotalCashOutlay_33` double DEFAULT NULL,
  `Year_34` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_34` double DEFAULT NULL,
  `IndividualRent_Year_2_34` double DEFAULT NULL,
  `IndividualRent_Year_3_34` double DEFAULT NULL,
  `IndividualRent_Year_4_34` double DEFAULT NULL,
  `IndividualRent_Year_5_34` double DEFAULT NULL,
  `IndividualRent_Year_6_34` double DEFAULT NULL,
  `IndividualRent_Year_7_34` double DEFAULT NULL,
  `IndividualRent_Year_8_34` double DEFAULT NULL,
  `IndividualRent_Year_9_34` double DEFAULT NULL,
  `IndividualRent_Year_10_34` double DEFAULT NULL,
  `IndividualRent_Year_11_34` double DEFAULT NULL,
  `IndividualRent_Year_12_34` double DEFAULT NULL,
  `IndividualRent_Total_34` double DEFAULT NULL,
  `OverrideVacancyRates_Year_34` double DEFAULT NULL,
  `OverrideConcessions_Year_34` double DEFAULT NULL,
  `TotalCashOutlay_34` double DEFAULT NULL,
  `Year_35` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_35` double DEFAULT NULL,
  `IndividualRent_Year_2_35` double DEFAULT NULL,
  `IndividualRent_Year_3_35` double DEFAULT NULL,
  `IndividualRent_Year_4_35` double DEFAULT NULL,
  `IndividualRent_Year_5_35` double DEFAULT NULL,
  `IndividualRent_Year_6_35` double DEFAULT NULL,
  `IndividualRent_Year_7_35` double DEFAULT NULL,
  `IndividualRent_Year_8_35` double DEFAULT NULL,
  `IndividualRent_Year_9_35` double DEFAULT NULL,
  `IndividualRent_Year_10_35` double DEFAULT NULL,
  `IndividualRent_Year_11_35` double DEFAULT NULL,
  `IndividualRent_Year_12_35` double DEFAULT NULL,
  `IndividualRent_Total_35` double DEFAULT NULL,
  `OverrideVacancyRates_Year_35` double DEFAULT NULL,
  `OverrideConcessions_Year_35` double DEFAULT NULL,
  `TotalCashOutlay_35` double DEFAULT NULL,
  `Year_36` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_36` double DEFAULT NULL,
  `IndividualRent_Year_2_36` double DEFAULT NULL,
  `IndividualRent_Year_3_36` double DEFAULT NULL,
  `IndividualRent_Year_4_36` double DEFAULT NULL,
  `IndividualRent_Year_5_36` double DEFAULT NULL,
  `IndividualRent_Year_6_36` double DEFAULT NULL,
  `IndividualRent_Year_7_36` double DEFAULT NULL,
  `IndividualRent_Year_8_36` double DEFAULT NULL,
  `IndividualRent_Year_9_36` double DEFAULT NULL,
  `IndividualRent_Year_10_36` double DEFAULT NULL,
  `IndividualRent_Year_11_36` double DEFAULT NULL,
  `IndividualRent_Year_12_36` double DEFAULT NULL,
  `IndividualRent_Total_36` double DEFAULT NULL,
  `OverrideVacancyRates_Year_36` double DEFAULT NULL,
  `OverrideConcessions_Year_36` double DEFAULT NULL,
  `TotalCashOutlay_36` double DEFAULT NULL,
  `Year_37` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_37` double DEFAULT NULL,
  `IndividualRent_Year_2_37` double DEFAULT NULL,
  `IndividualRent_Year_3_37` double DEFAULT NULL,
  `IndividualRent_Year_4_37` double DEFAULT NULL,
  `IndividualRent_Year_5_37` double DEFAULT NULL,
  `IndividualRent_Year_6_37` double DEFAULT NULL,
  `IndividualRent_Year_7_37` double DEFAULT NULL,
  `IndividualRent_Year_8_37` double DEFAULT NULL,
  `IndividualRent_Year_9_37` double DEFAULT NULL,
  `IndividualRent_Year_10_37` double DEFAULT NULL,
  `IndividualRent_Year_11_37` double DEFAULT NULL,
  `IndividualRent_Year_12_37` double DEFAULT NULL,
  `IndividualRent_Total_37` double DEFAULT NULL,
  `OverrideVacancyRates_Year_37` double DEFAULT NULL,
  `OverrideConcessions_Year_37` double DEFAULT NULL,
  `TotalCashOutlay_37` double DEFAULT NULL,
  `Year_38` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_38` double DEFAULT NULL,
  `IndividualRent_Year_2_38` double DEFAULT NULL,
  `IndividualRent_Year_3_38` double DEFAULT NULL,
  `IndividualRent_Year_4_38` double DEFAULT NULL,
  `IndividualRent_Year_5_38` double DEFAULT NULL,
  `IndividualRent_Year_6_38` double DEFAULT NULL,
  `IndividualRent_Year_7_38` double DEFAULT NULL,
  `IndividualRent_Year_8_38` double DEFAULT NULL,
  `IndividualRent_Year_9_38` double DEFAULT NULL,
  `IndividualRent_Year_10_38` double DEFAULT NULL,
  `IndividualRent_Year_11_38` double DEFAULT NULL,
  `IndividualRent_Year_12_38` double DEFAULT NULL,
  `IndividualRent_Total_38` double DEFAULT NULL,
  `OverrideVacancyRates_Year_38` double DEFAULT NULL,
  `OverrideConcessions_Year_38` double DEFAULT NULL,
  `TotalCashOutlay_38` double DEFAULT NULL,
  `Year_39` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_39` double DEFAULT NULL,
  `IndividualRent_Year_2_39` double DEFAULT NULL,
  `IndividualRent_Year_3_39` double DEFAULT NULL,
  `IndividualRent_Year_4_39` double DEFAULT NULL,
  `IndividualRent_Year_5_39` double DEFAULT NULL,
  `IndividualRent_Year_6_39` double DEFAULT NULL,
  `IndividualRent_Year_7_39` double DEFAULT NULL,
  `IndividualRent_Year_8_39` double DEFAULT NULL,
  `IndividualRent_Year_9_39` double DEFAULT NULL,
  `IndividualRent_Year_10_39` double DEFAULT NULL,
  `IndividualRent_Year_11_39` double DEFAULT NULL,
  `IndividualRent_Year_12_39` double DEFAULT NULL,
  `IndividualRent_Total_39` double DEFAULT NULL,
  `OverrideVacancyRates_Year_39` double DEFAULT NULL,
  `OverrideConcessions_Year_39` double DEFAULT NULL,
  `TotalCashOutlay_39` double DEFAULT NULL,
  `Year_40` varchar(45) DEFAULT NULL,
  `IndividualRent_Year_1_40` double DEFAULT NULL,
  `IndividualRent_Year_2_40` double DEFAULT NULL,
  `IndividualRent_Year_3_40` double DEFAULT NULL,
  `IndividualRent_Year_4_40` double DEFAULT NULL,
  `IndividualRent_Year_5_40` double DEFAULT NULL,
  `IndividualRent_Year_6_40` double DEFAULT NULL,
  `IndividualRent_Year_7_40` double DEFAULT NULL,
  `IndividualRent_Year_8_40` double DEFAULT NULL,
  `IndividualRent_Year_9_40` double DEFAULT NULL,
  `IndividualRent_Year_10_40` double DEFAULT NULL,
  `IndividualRent_Year_11_40` double DEFAULT NULL,
  `IndividualRent_Year_12_40` double DEFAULT NULL,
  `IndividualRent_Total_40` double DEFAULT NULL,
  `OverrideVacancyRates_Year_40` double DEFAULT NULL,
  `OverrideConcessions_Year_40` double DEFAULT NULL,
  `TotalCashOutlay_40` double DEFAULT NULL,
  `OverrideVacancyRates` varchar(45) DEFAULT NULL,
  `OverrideConcessions` varchar(45) DEFAULT NULL,
  `TotalCashOutlay_0` double DEFAULT NULL,
  `CreatedOn` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_rental_advance`
--

INSERT INTO `ci_rental_advance` (`RentalAdvanceID`, `PropertyID`, `UserID`, `OverrideRents`, `Year_0`, `Year_1`, `IndividualRent_Year_1_1`, `IndividualRent_Year_2_1`, `IndividualRent_Year_3_1`, `IndividualRent_Year_4_1`, `IndividualRent_Year_5_1`, `IndividualRent_Year_6_1`, `IndividualRent_Year_7_1`, `IndividualRent_Year_8_1`, `IndividualRent_Year_9_1`, `IndividualRent_Year_10_1`, `IndividualRent_Year_11_1`, `IndividualRent_Year_12_1`, `IndividualRent_Total_1`, `OverrideVacancyRates_Year_1`, `OverrideConcessions_Year_1`, `CapitalImprovementSchedule_1`, `Purpose_1`, `FairMarketValue_1`, `TotalCashOutlay_1`, `Year_2`, `IndividualRent_Year_1_2`, `IndividualRent_Year_2_2`, `IndividualRent_Year_3_2`, `IndividualRent_Year_4_2`, `IndividualRent_Year_5_2`, `IndividualRent_Year_6_2`, `IndividualRent_Year_7_2`, `IndividualRent_Year_8_2`, `IndividualRent_Year_9_2`, `IndividualRent_Year_10_2`, `IndividualRent_Year_11_2`, `IndividualRent_Year_12_2`, `IndividualRent_Total_2`, `OverrideVacancyRates_Year_2`, `OverrideConcessions_Year_2`, `CapitalImprovementSchedule_2`, `Purpose_2`, `FairMarketValue_2`, `TotalCashOutlay_2`, `Year_3`, `IndividualRent_Year_1_3`, `IndividualRent_Year_2_3`, `IndividualRent_Year_3_3`, `IndividualRent_Year_4_3`, `IndividualRent_Year_5_3`, `IndividualRent_Year_6_3`, `IndividualRent_Year_7_3`, `IndividualRent_Year_8_3`, `IndividualRent_Year_9_3`, `IndividualRent_Year_10_3`, `IndividualRent_Year_11_3`, `IndividualRent_Year_12_3`, `IndividualRent_Total_3`, `OverrideVacancyRates_Year_3`, `OverrideConcessions_Year_3`, `CapitalImprovementSchedule_3`, `Purpose_3`, `FairMarketValue_3`, `TotalCashOutlay_3`, `Year_4`, `IndividualRent_Year_1_4`, `IndividualRent_Year_2_4`, `IndividualRent_Year_3_4`, `IndividualRent_Year_4_4`, `IndividualRent_Year_5_4`, `IndividualRent_Year_6_4`, `IndividualRent_Year_7_4`, `IndividualRent_Year_8_4`, `IndividualRent_Year_9_4`, `IndividualRent_Year_10_4`, `IndividualRent_Year_11_4`, `IndividualRent_Year_12_4`, `IndividualRent_Total_4`, `OverrideVacancyRates_Year_4`, `OverrideConcessions_Year_4`, `CapitalImprovementSchedule_4`, `Purpose_4`, `FairMarketValue_4`, `TotalCashOutlay_4`, `Year_5`, `IndividualRent_Year_1_5`, `IndividualRent_Year_2_5`, `IndividualRent_Year_3_5`, `IndividualRent_Year_4_5`, `IndividualRent_Year_5_5`, `IndividualRent_Year_6_5`, `IndividualRent_Year_7_5`, `IndividualRent_Year_8_5`, `IndividualRent_Year_9_5`, `IndividualRent_Year_10_5`, `IndividualRent_Year_11_5`, `IndividualRent_Year_12_5`, `IndividualRent_Total_5`, `OverrideVacancyRates_Year_5`, `OverrideConcessions_Year_5`, `CapitalImprovementSchedule_5`, `Purpose_5`, `FairMarketValue_5`, `TotalCashOutlay_5`, `Year_6`, `IndividualRent_Year_1_6`, `IndividualRent_Year_2_6`, `IndividualRent_Year_3_6`, `IndividualRent_Year_4_6`, `IndividualRent_Year_5_6`, `IndividualRent_Year_6_6`, `IndividualRent_Year_7_6`, `IndividualRent_Year_8_6`, `IndividualRent_Year_9_6`, `IndividualRent_Year_10_6`, `IndividualRent_Year_11_6`, `IndividualRent_Year_12_6`, `IndividualRent_Total_6`, `OverrideVacancyRates_Year_6`, `OverrideConcessions_Year_6`, `CapitalImprovementSchedule_6`, `Purpose_6`, `FairMarketValue_6`, `TotalCashOutlay_6`, `Year_7`, `IndividualRent_Year_1_7`, `IndividualRent_Year_2_7`, `IndividualRent_Year_3_7`, `IndividualRent_Year_4_7`, `IndividualRent_Year_5_7`, `IndividualRent_Year_6_7`, `IndividualRent_Year_7_7`, `IndividualRent_Year_8_7`, `IndividualRent_Year_9_7`, `IndividualRent_Year_10_7`, `IndividualRent_Year_11_7`, `IndividualRent_Year_12_7`, `IndividualRent_Total_7`, `OverrideVacancyRates_Year_7`, `OverrideConcessions_Year_7`, `CapitalImprovementSchedule_7`, `Purpose_7`, `FairMarketValue_7`, `TotalCashOutlay_7`, `Year_8`, `IndividualRent_Year_1_8`, `IndividualRent_Year_2_8`, `IndividualRent_Year_3_8`, `IndividualRent_Year_4_8`, `IndividualRent_Year_5_8`, `IndividualRent_Year_6_8`, `IndividualRent_Year_7_8`, `IndividualRent_Year_8_8`, `IndividualRent_Year_9_8`, `IndividualRent_Year_10_8`, `IndividualRent_Year_11_8`, `IndividualRent_Year_12_8`, `IndividualRent_Total_8`, `OverrideVacancyRates_Year_8`, `OverrideConcessions_Year_8`, `CapitalImprovementSchedule_8`, `Purpose_8`, `FairMarketValue_8`, `TotalCashOutlay_8`, `Year_9`, `IndividualRent_Year_1_9`, `IndividualRent_Year_2_9`, `IndividualRent_Year_3_9`, `IndividualRent_Year_4_9`, `IndividualRent_Year_5_9`, `IndividualRent_Year_6_9`, `IndividualRent_Year_7_9`, `IndividualRent_Year_8_9`, `IndividualRent_Year_9_9`, `IndividualRent_Year_10_9`, `IndividualRent_Year_11_9`, `IndividualRent_Year_12_9`, `IndividualRent_Total_9`, `OverrideVacancyRates_Year_9`, `OverrideConcessions_Year_9`, `CapitalImprovementSchedule_9`, `Purpose_9`, `FairMarketValue_9`, `TotalCashOutlay_9`, `Year_10`, `IndividualRent_Year_1_10`, `IndividualRent_Year_2_10`, `IndividualRent_Year_3_10`, `IndividualRent_Year_4_10`, `IndividualRent_Year_5_10`, `IndividualRent_Year_6_10`, `IndividualRent_Year_7_10`, `IndividualRent_Year_8_10`, `IndividualRent_Year_9_10`, `IndividualRent_Year_10_10`, `IndividualRent_Year_11_10`, `IndividualRent_Year_12_10`, `IndividualRent_Total_10`, `OverrideVacancyRates_Year_10`, `OverrideConcessions_Year_10`, `CapitalImprovementSchedule_10`, `Purpose_10`, `FairMarketValue_10`, `TotalCashOutlay_10`, `Year_11`, `IndividualRent_Year_1_11`, `IndividualRent_Year_2_11`, `IndividualRent_Year_3_11`, `IndividualRent_Year_4_11`, `IndividualRent_Year_5_11`, `IndividualRent_Year_6_11`, `IndividualRent_Year_7_11`, `IndividualRent_Year_8_11`, `IndividualRent_Year_9_11`, `IndividualRent_Year_10_11`, `IndividualRent_Year_11_11`, `IndividualRent_Year_12_11`, `IndividualRent_Total_11`, `OverrideVacancyRates_Year_11`, `OverrideConcessions_Year_11`, `CapitalImprovementSchedule_11`, `Purpose_11`, `FairMarketValue_11`, `TotalCashOutlay_11`, `Year_12`, `IndividualRent_Year_1_12`, `IndividualRent_Year_2_12`, `IndividualRent_Year_3_12`, `IndividualRent_Year_4_12`, `IndividualRent_Year_5_12`, `IndividualRent_Year_6_12`, `IndividualRent_Year_7_12`, `IndividualRent_Year_8_12`, `IndividualRent_Year_9_12`, `IndividualRent_Year_10_12`, `IndividualRent_Year_11_12`, `IndividualRent_Year_12_12`, `IndividualRent_Total_12`, `OverrideVacancyRates_Year_12`, `OverrideConcessions_Year_12`, `CapitalImprovementSchedule_12`, `Purpose_12`, `FairMarketValue_12`, `TotalCashOutlay_12`, `Year_13`, `IndividualRent_Year_1_13`, `IndividualRent_Year_2_13`, `IndividualRent_Year_3_13`, `IndividualRent_Year_4_13`, `IndividualRent_Year_5_13`, `IndividualRent_Year_6_13`, `IndividualRent_Year_7_13`, `IndividualRent_Year_8_13`, `IndividualRent_Year_9_13`, `IndividualRent_Year_10_13`, `IndividualRent_Year_11_13`, `IndividualRent_Year_12_13`, `IndividualRent_Total_13`, `OverrideVacancyRates_Year_13`, `OverrideConcessions_Year_13`, `CapitalImprovementSchedule_13`, `Purpose_13`, `FairMarketValue_13`, `TotalCashOutlay_13`, `Year_14`, `IndividualRent_Year_1_14`, `IndividualRent_Year_2_14`, `IndividualRent_Year_3_14`, `IndividualRent_Year_4_14`, `IndividualRent_Year_5_14`, `IndividualRent_Year_6_14`, `IndividualRent_Year_7_14`, `IndividualRent_Year_8_14`, `IndividualRent_Year_9_14`, `IndividualRent_Year_10_14`, `IndividualRent_Year_11_14`, `IndividualRent_Year_12_14`, `IndividualRent_Total_14`, `OverrideVacancyRates_Year_14`, `OverrideConcessions_Year_14`, `CapitalImprovementSchedule_14`, `Purpose_14`, `FairMarketValue_14`, `TotalCashOutlay_14`, `Year_15`, `IndividualRent_Year_1_15`, `IndividualRent_Year_2_15`, `IndividualRent_Year_3_15`, `IndividualRent_Year_4_15`, `IndividualRent_Year_5_15`, `IndividualRent_Year_6_15`, `IndividualRent_Year_7_15`, `IndividualRent_Year_8_15`, `IndividualRent_Year_9_15`, `IndividualRent_Year_10_15`, `IndividualRent_Year_11_15`, `IndividualRent_Year_12_15`, `IndividualRent_Total_15`, `OverrideVacancyRates_Year_15`, `OverrideConcessions_Year_15`, `CapitalImprovementSchedule_15`, `Purpose_15`, `FairMarketValue_15`, `TotalCashOutlay_15`, `Year_16`, `IndividualRent_Year_1_16`, `IndividualRent_Year_2_16`, `IndividualRent_Year_3_16`, `IndividualRent_Year_4_16`, `IndividualRent_Year_5_16`, `IndividualRent_Year_6_16`, `IndividualRent_Year_7_16`, `IndividualRent_Year_8_16`, `IndividualRent_Year_9_16`, `IndividualRent_Year_10_16`, `IndividualRent_Year_11_16`, `IndividualRent_Year_12_16`, `IndividualRent_Total_16`, `OverrideVacancyRates_Year_16`, `OverrideConcessions_Year_16`, `CapitalImprovementSchedule_16`, `Purpose_16`, `FairMarketValue_16`, `TotalCashOutlay_16`, `Year_17`, `IndividualRent_Year_1_17`, `IndividualRent_Year_2_17`, `IndividualRent_Year_3_17`, `IndividualRent_Year_4_17`, `IndividualRent_Year_5_17`, `IndividualRent_Year_6_17`, `IndividualRent_Year_7_17`, `IndividualRent_Year_8_17`, `IndividualRent_Year_9_17`, `IndividualRent_Year_10_17`, `IndividualRent_Year_11_17`, `IndividualRent_Year_12_17`, `IndividualRent_Total_17`, `OverrideVacancyRates_Year_17`, `OverrideConcessions_Year_17`, `CapitalImprovementSchedule_17`, `Purpose_17`, `FairMarketValue_17`, `TotalCashOutlay_17`, `Year_18`, `IndividualRent_Year_1_18`, `IndividualRent_Year_2_18`, `IndividualRent_Year_3_18`, `IndividualRent_Year_4_18`, `IndividualRent_Year_5_18`, `IndividualRent_Year_6_18`, `IndividualRent_Year_7_18`, `IndividualRent_Year_8_18`, `IndividualRent_Year_9_18`, `IndividualRent_Year_10_18`, `IndividualRent_Year_11_18`, `IndividualRent_Year_12_18`, `IndividualRent_Total_18`, `OverrideVacancyRates_Year_18`, `OverrideConcessions_Year_18`, `CapitalImprovementSchedule_18`, `Purpose_18`, `FairMarketValue_18`, `TotalCashOutlay_18`, `Year_19`, `IndividualRent_Year_1_19`, `IndividualRent_Year_2_19`, `IndividualRent_Year_3_19`, `IndividualRent_Year_4_19`, `IndividualRent_Year_5_19`, `IndividualRent_Year_6_19`, `IndividualRent_Year_7_19`, `IndividualRent_Year_8_19`, `IndividualRent_Year_9_19`, `IndividualRent_Year_10_19`, `IndividualRent_Year_11_19`, `IndividualRent_Year_12_19`, `IndividualRent_Total_19`, `OverrideVacancyRates_Year_19`, `OverrideConcessions_Year_19`, `CapitalImprovementSchedule_19`, `Purpose_19`, `FairMarketValue_19`, `TotalCashOutlay_19`, `Year_20`, `IndividualRent_Year_1_20`, `IndividualRent_Year_2_20`, `IndividualRent_Year_3_20`, `IndividualRent_Year_4_20`, `IndividualRent_Year_5_20`, `IndividualRent_Year_6_20`, `IndividualRent_Year_7_20`, `IndividualRent_Year_8_20`, `IndividualRent_Year_9_20`, `IndividualRent_Year_10_20`, `IndividualRent_Year_11_20`, `IndividualRent_Year_12_20`, `IndividualRent_Total_20`, `OverrideVacancyRates_Year_20`, `OverrideConcessions_Year_20`, `CapitalImprovementSchedule_20`, `Purpose_20`, `FairMarketValue_20`, `TotalCashOutlay_20`, `Year_21`, `IndividualRent_Year_1_21`, `IndividualRent_Year_2_21`, `IndividualRent_Year_3_21`, `IndividualRent_Year_4_21`, `IndividualRent_Year_5_21`, `IndividualRent_Year_6_21`, `IndividualRent_Year_7_21`, `IndividualRent_Year_8_21`, `IndividualRent_Year_9_21`, `IndividualRent_Year_10_21`, `IndividualRent_Year_11_21`, `IndividualRent_Year_12_21`, `IndividualRent_Total_21`, `OverrideVacancyRates_Year_21`, `OverrideConcessions_Year_21`, `CapitalImprovementSchedule_21`, `Purpose_21`, `FairMarketValue_21`, `TotalCashOutlay_21`, `Year_22`, `IndividualRent_Year_1_22`, `IndividualRent_Year_2_22`, `IndividualRent_Year_3_22`, `IndividualRent_Year_4_22`, `IndividualRent_Year_5_22`, `IndividualRent_Year_6_22`, `IndividualRent_Year_7_22`, `IndividualRent_Year_8_22`, `IndividualRent_Year_9_22`, `IndividualRent_Year_10_22`, `IndividualRent_Year_11_22`, `IndividualRent_Year_12_22`, `IndividualRent_Total_22`, `OverrideVacancyRates_Year_22`, `OverrideConcessions_Year_22`, `CapitalImprovementSchedule_22`, `Purpose_22`, `FairMarketValue_22`, `TotalCashOutlay_22`, `Year_23`, `IndividualRent_Year_1_23`, `IndividualRent_Year_2_23`, `IndividualRent_Year_3_23`, `IndividualRent_Year_4_23`, `IndividualRent_Year_5_23`, `IndividualRent_Year_6_23`, `IndividualRent_Year_7_23`, `IndividualRent_Year_8_23`, `IndividualRent_Year_9_23`, `IndividualRent_Year_10_23`, `IndividualRent_Year_11_23`, `IndividualRent_Year_12_23`, `IndividualRent_Total_23`, `OverrideVacancyRates_Year_23`, `OverrideConcessions_Year_23`, `CapitalImprovementSchedule_23`, `Purpose_23`, `FairMarketValue_23`, `TotalCashOutlay_23`, `Year_24`, `IndividualRent_Year_1_24`, `IndividualRent_Year_2_24`, `IndividualRent_Year_3_24`, `IndividualRent_Year_4_24`, `IndividualRent_Year_5_24`, `IndividualRent_Year_6_24`, `IndividualRent_Year_7_24`, `IndividualRent_Year_8_24`, `IndividualRent_Year_9_24`, `IndividualRent_Year_10_24`, `IndividualRent_Year_11_24`, `IndividualRent_Year_12_24`, `IndividualRent_Total_24`, `OverrideVacancyRates_Year_24`, `OverrideConcessions_Year_24`, `CapitalImprovementSchedule_24`, `Purpose_24`, `FairMarketValue_24`, `TotalCashOutlay_24`, `Year_25`, `IndividualRent_Year_1_25`, `IndividualRent_Year_2_25`, `IndividualRent_Year_3_25`, `IndividualRent_Year_4_25`, `IndividualRent_Year_5_25`, `IndividualRent_Year_6_25`, `IndividualRent_Year_7_25`, `IndividualRent_Year_8_25`, `IndividualRent_Year_9_25`, `IndividualRent_Year_10_25`, `IndividualRent_Year_11_25`, `IndividualRent_Year_12_25`, `IndividualRent_Total_25`, `OverrideVacancyRates_Year_25`, `OverrideConcessions_Year_25`, `CapitalImprovementSchedule_25`, `Purpose_25`, `FairMarketValue_25`, `TotalCashOutlay_25`, `Year_26`, `IndividualRent_Year_1_26`, `IndividualRent_Year_2_26`, `IndividualRent_Year_3_26`, `IndividualRent_Year_4_26`, `IndividualRent_Year_5_26`, `IndividualRent_Year_6_26`, `IndividualRent_Year_7_26`, `IndividualRent_Year_8_26`, `IndividualRent_Year_9_26`, `IndividualRent_Year_10_26`, `IndividualRent_Year_11_26`, `IndividualRent_Year_12_26`, `IndividualRent_Total_26`, `OverrideVacancyRates_Year_26`, `OverrideConcessions_Year_26`, `CapitalImprovementSchedule_26`, `Purpose_26`, `FairMarketValue_26`, `TotalCashOutlay_26`, `Year_27`, `IndividualRent_Year_1_27`, `IndividualRent_Year_2_27`, `IndividualRent_Year_3_27`, `IndividualRent_Year_4_27`, `IndividualRent_Year_5_27`, `IndividualRent_Year_6_27`, `IndividualRent_Year_7_27`, `IndividualRent_Year_8_27`, `IndividualRent_Year_9_27`, `IndividualRent_Year_10_27`, `IndividualRent_Year_11_27`, `IndividualRent_Year_12_27`, `IndividualRent_Total_27`, `OverrideVacancyRates_Year_27`, `OverrideConcessions_Year_27`, `CapitalImprovementSchedule_27`, `Purpose_27`, `FairMarketValue_27`, `TotalCashOutlay_27`, `Year_28`, `IndividualRent_Year_1_28`, `IndividualRent_Year_2_28`, `IndividualRent_Year_3_28`, `IndividualRent_Year_4_28`, `IndividualRent_Year_5_28`, `IndividualRent_Year_6_28`, `IndividualRent_Year_7_28`, `IndividualRent_Year_8_28`, `IndividualRent_Year_9_28`, `IndividualRent_Year_10_28`, `IndividualRent_Year_11_28`, `IndividualRent_Year_12_28`, `IndividualRent_Total_28`, `OverrideVacancyRates_Year_28`, `OverrideConcessions_Year_28`, `CapitalImprovementSchedule_28`, `Purpose_28`, `FairMarketValue_28`, `TotalCashOutlay_28`, `Year_29`, `IndividualRent_Year_1_29`, `IndividualRent_Year_2_29`, `IndividualRent_Year_3_29`, `IndividualRent_Year_4_29`, `IndividualRent_Year_5_29`, `IndividualRent_Year_6_29`, `IndividualRent_Year_7_29`, `IndividualRent_Year_8_29`, `IndividualRent_Year_9_29`, `IndividualRent_Year_10_29`, `IndividualRent_Year_11_29`, `IndividualRent_Year_12_29`, `IndividualRent_Total_29`, `OverrideVacancyRates_Year_29`, `OverrideConcessions_Year_29`, `CapitalImprovementSchedule_29`, `Purpose_29`, `FairMarketValue_29`, `TotalCashOutlay_29`, `Year_30`, `IndividualRent_Year_1_30`, `IndividualRent_Year_2_30`, `IndividualRent_Year_3_30`, `IndividualRent_Year_4_30`, `IndividualRent_Year_5_30`, `IndividualRent_Year_6_30`, `IndividualRent_Year_7_30`, `IndividualRent_Year_8_30`, `IndividualRent_Year_9_30`, `IndividualRent_Year_10_30`, `IndividualRent_Year_11_30`, `IndividualRent_Year_12_30`, `IndividualRent_Total_30`, `OverrideVacancyRates_Year_30`, `OverrideConcessions_Year_30`, `CapitalImprovementSchedule_30`, `Purpose_30`, `FairMarketValue_30`, `TotalCashOutlay_30`, `Year_31`, `IndividualRent_Year_1_31`, `IndividualRent_Year_2_31`, `IndividualRent_Year_3_31`, `IndividualRent_Year_4_31`, `IndividualRent_Year_5_31`, `IndividualRent_Year_6_31`, `IndividualRent_Year_7_31`, `IndividualRent_Year_8_31`, `IndividualRent_Year_9_31`, `IndividualRent_Year_10_31`, `IndividualRent_Year_11_31`, `IndividualRent_Year_12_31`, `IndividualRent_Total_31`, `OverrideVacancyRates_Year_31`, `OverrideConcessions_Year_31`, `TotalCashOutlay_31`, `Year_32`, `IndividualRent_Year_1_32`, `IndividualRent_Year_2_32`, `IndividualRent_Year_3_32`, `IndividualRent_Year_4_32`, `IndividualRent_Year_5_32`, `IndividualRent_Year_6_32`, `IndividualRent_Year_7_32`, `IndividualRent_Year_8_32`, `IndividualRent_Year_9_32`, `IndividualRent_Year_10_32`, `IndividualRent_Year_11_32`, `IndividualRent_Year_12_32`, `IndividualRent_Total_32`, `OverrideVacancyRates_Year_32`, `OverrideConcessions_Year_32`, `TotalCashOutlay_32`, `Year_33`, `IndividualRent_Year_1_33`, `IndividualRent_Year_2_33`, `IndividualRent_Year_3_33`, `IndividualRent_Year_4_33`, `IndividualRent_Year_5_33`, `IndividualRent_Year_6_33`, `IndividualRent_Year_7_33`, `IndividualRent_Year_8_33`, `IndividualRent_Year_9_33`, `IndividualRent_Year_10_33`, `IndividualRent_Year_11_33`, `IndividualRent_Year_12_33`, `IndividualRent_Total_33`, `OverrideVacancyRates_Year_33`, `OverrideConcessions_Year_33`, `TotalCashOutlay_33`, `Year_34`, `IndividualRent_Year_1_34`, `IndividualRent_Year_2_34`, `IndividualRent_Year_3_34`, `IndividualRent_Year_4_34`, `IndividualRent_Year_5_34`, `IndividualRent_Year_6_34`, `IndividualRent_Year_7_34`, `IndividualRent_Year_8_34`, `IndividualRent_Year_9_34`, `IndividualRent_Year_10_34`, `IndividualRent_Year_11_34`, `IndividualRent_Year_12_34`, `IndividualRent_Total_34`, `OverrideVacancyRates_Year_34`, `OverrideConcessions_Year_34`, `TotalCashOutlay_34`, `Year_35`, `IndividualRent_Year_1_35`, `IndividualRent_Year_2_35`, `IndividualRent_Year_3_35`, `IndividualRent_Year_4_35`, `IndividualRent_Year_5_35`, `IndividualRent_Year_6_35`, `IndividualRent_Year_7_35`, `IndividualRent_Year_8_35`, `IndividualRent_Year_9_35`, `IndividualRent_Year_10_35`, `IndividualRent_Year_11_35`, `IndividualRent_Year_12_35`, `IndividualRent_Total_35`, `OverrideVacancyRates_Year_35`, `OverrideConcessions_Year_35`, `TotalCashOutlay_35`, `Year_36`, `IndividualRent_Year_1_36`, `IndividualRent_Year_2_36`, `IndividualRent_Year_3_36`, `IndividualRent_Year_4_36`, `IndividualRent_Year_5_36`, `IndividualRent_Year_6_36`, `IndividualRent_Year_7_36`, `IndividualRent_Year_8_36`, `IndividualRent_Year_9_36`, `IndividualRent_Year_10_36`, `IndividualRent_Year_11_36`, `IndividualRent_Year_12_36`, `IndividualRent_Total_36`, `OverrideVacancyRates_Year_36`, `OverrideConcessions_Year_36`, `TotalCashOutlay_36`, `Year_37`, `IndividualRent_Year_1_37`, `IndividualRent_Year_2_37`, `IndividualRent_Year_3_37`, `IndividualRent_Year_4_37`, `IndividualRent_Year_5_37`, `IndividualRent_Year_6_37`, `IndividualRent_Year_7_37`, `IndividualRent_Year_8_37`, `IndividualRent_Year_9_37`, `IndividualRent_Year_10_37`, `IndividualRent_Year_11_37`, `IndividualRent_Year_12_37`, `IndividualRent_Total_37`, `OverrideVacancyRates_Year_37`, `OverrideConcessions_Year_37`, `TotalCashOutlay_37`, `Year_38`, `IndividualRent_Year_1_38`, `IndividualRent_Year_2_38`, `IndividualRent_Year_3_38`, `IndividualRent_Year_4_38`, `IndividualRent_Year_5_38`, `IndividualRent_Year_6_38`, `IndividualRent_Year_7_38`, `IndividualRent_Year_8_38`, `IndividualRent_Year_9_38`, `IndividualRent_Year_10_38`, `IndividualRent_Year_11_38`, `IndividualRent_Year_12_38`, `IndividualRent_Total_38`, `OverrideVacancyRates_Year_38`, `OverrideConcessions_Year_38`, `TotalCashOutlay_38`, `Year_39`, `IndividualRent_Year_1_39`, `IndividualRent_Year_2_39`, `IndividualRent_Year_3_39`, `IndividualRent_Year_4_39`, `IndividualRent_Year_5_39`, `IndividualRent_Year_6_39`, `IndividualRent_Year_7_39`, `IndividualRent_Year_8_39`, `IndividualRent_Year_9_39`, `IndividualRent_Year_10_39`, `IndividualRent_Year_11_39`, `IndividualRent_Year_12_39`, `IndividualRent_Total_39`, `OverrideVacancyRates_Year_39`, `OverrideConcessions_Year_39`, `TotalCashOutlay_39`, `Year_40`, `IndividualRent_Year_1_40`, `IndividualRent_Year_2_40`, `IndividualRent_Year_3_40`, `IndividualRent_Year_4_40`, `IndividualRent_Year_5_40`, `IndividualRent_Year_6_40`, `IndividualRent_Year_7_40`, `IndividualRent_Year_8_40`, `IndividualRent_Year_9_40`, `IndividualRent_Year_10_40`, `IndividualRent_Year_11_40`, `IndividualRent_Year_12_40`, `IndividualRent_Total_40`, `OverrideVacancyRates_Year_40`, `OverrideConcessions_Year_40`, `TotalCashOutlay_40`, `OverrideVacancyRates`, `OverrideConcessions`, `TotalCashOutlay_0`, `CreatedOn`) VALUES
(1, 1448011135, 3, NULL, '01-12-2015', '01-12-2016', 2500, 3500, 4500, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10500, 3, 2, 0, '', 1, -253000, '01-12-2017', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2018', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2019', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2020', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2021', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2022', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2023', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2024', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2025', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2026', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2027', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2028', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2029', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2030', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2031', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2032', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2033', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2034', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2035', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2036', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-12-2037', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 1, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -253000, '01-01-1970', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, -253000, 'No', 'No', -253000, '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_cash_outlay`
--

CREATE TABLE IF NOT EXISTS `ci_rental_cash_outlay` (
`RentalCashOutlayID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `RollupFirst` enum('Yes','No') NOT NULL DEFAULT 'No',
  `RollUpSecond` enum('Yes','No') NOT NULL DEFAULT 'No',
  `InspectionAppraisal` double NOT NULL,
  `MortgageOneOriginationFeePercent` double NOT NULL,
  `MortgageTwoOriginationFeePercent` double NOT NULL,
  `MortgageOneOriginationFee` double NOT NULL,
  `MortgageTwoOriginationFee` double NOT NULL,
  `MortgageOneDiscountFeePercent` double NOT NULL,
  `MortgageTwoDiscountFeePercent` double NOT NULL,
  `MortgageOneDiscountFee` double NOT NULL,
  `MortgageTwoDiscountFee` double NOT NULL,
  `ClosingCostsMiscFeePercent` double NOT NULL,
  `ClosingCostsMiscFee` double NOT NULL,
  `TotalSettlement` double NOT NULL,
  `CashbackFromSellerPercent` double NOT NULL,
  `CashbackFromSeller` double NOT NULL,
  `InitialCapitalReserves_1` double NOT NULL,
  `InitialCapitalReserves_2` double NOT NULL,
  `InitialCapitalImprovements` double NOT NULL,
  `FMVProperty` enum('Yes','No') NOT NULL DEFAULT 'No',
  `FMVMultiplier` double NOT NULL,
  `FeeName` double NOT NULL,
  `TotalCashOutlay` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_rental_cash_outlay`
--

INSERT INTO `ci_rental_cash_outlay` (`RentalCashOutlayID`, `PropertyID`, `UserID`, `RollupFirst`, `RollUpSecond`, `InspectionAppraisal`, `MortgageOneOriginationFeePercent`, `MortgageTwoOriginationFeePercent`, `MortgageOneOriginationFee`, `MortgageTwoOriginationFee`, `MortgageOneDiscountFeePercent`, `MortgageTwoDiscountFeePercent`, `MortgageOneDiscountFee`, `MortgageTwoDiscountFee`, `ClosingCostsMiscFeePercent`, `ClosingCostsMiscFee`, `TotalSettlement`, `CashbackFromSellerPercent`, `CashbackFromSeller`, `InitialCapitalReserves_1`, `InitialCapitalReserves_2`, `InitialCapitalImprovements`, `FMVProperty`, `FMVMultiplier`, `FeeName`, `TotalCashOutlay`, `CreatedOn`) VALUES
(1, 1448011135, 3, 'Yes', 'No', 1000, 1, 0, 7500, 0, 2, 0, 15000, 0, 2, 20000, 43500, 2, 20000, 1000, 2, 1000, 'No', 2, 0, 253000, '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_financial_analysis`
--

CREATE TABLE IF NOT EXISTS `ci_rental_financial_analysis` (
`RentalFinancilaAnalysisID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `UseFinancing` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `DownPaymentPercent` double NOT NULL,
  `DownPayment` double NOT NULL,
  `FirstMortgageUsed` enum('Yes','No') NOT NULL DEFAULT 'Yes',
  `SecondMortgageUsed` enum('Yes','No') NOT NULL DEFAULT 'No',
  `FirstMortgageLoanType` enum('Fixed','Interest Only') NOT NULL DEFAULT 'Fixed',
  `SecondMortgageLoanType` enum('Fixed','Interest Only') NOT NULL DEFAULT 'Interest Only',
  `FirstMortgageTerm` int(11) NOT NULL,
  `SecondMortgageTerm` int(11) NOT NULL,
  `FirstMortgageLTVPercent` double NOT NULL,
  `SecondMortgageLTVPercent` double NOT NULL,
  `FirstMortgageAmount` double NOT NULL,
  `SecondMortgageAmount` double NOT NULL,
  `FirstMortgageRate` double NOT NULL,
  `SecondMortgageRate` double NOT NULL,
  `FirstMortgagePayment` double NOT NULL,
  `SecondMortgagePayment` double NOT NULL,
  `FirstMortgagePaymentTotal` double NOT NULL,
  `TotalLoanAmount` double NOT NULL,
  `InterestRate` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_rental_financial_analysis`
--

INSERT INTO `ci_rental_financial_analysis` (`RentalFinancilaAnalysisID`, `PropertyID`, `UserID`, `UseFinancing`, `DownPaymentPercent`, `DownPayment`, `FirstMortgageUsed`, `SecondMortgageUsed`, `FirstMortgageLoanType`, `SecondMortgageLoanType`, `FirstMortgageTerm`, `SecondMortgageTerm`, `FirstMortgageLTVPercent`, `SecondMortgageLTVPercent`, `FirstMortgageAmount`, `SecondMortgageAmount`, `FirstMortgageRate`, `SecondMortgageRate`, `FirstMortgagePayment`, `SecondMortgagePayment`, `FirstMortgagePaymentTotal`, `TotalLoanAmount`, `InterestRate`, `CreatedOn`) VALUES
(1, 1448011135, 3, 'Yes', 25, 250000, 'Yes', 'No', 'Fixed', 'Interest Only', 40, 0, 75, 0, 772500, 0, 10, 0, -6559.65, 0, -6559.65, 772500, 10, '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_headlines`
--

CREATE TABLE IF NOT EXISTS `ci_rental_headlines` (
`HeadlineID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Headline` varchar(255) NOT NULL,
  `SubHeadline1` varchar(255) NOT NULL,
  `SubHeadline2` varchar(255) NOT NULL,
  `SubHeadline3` varchar(255) NOT NULL,
  `SubHeadline4` varchar(255) NOT NULL,
  `SubHeadline5` varchar(255) NOT NULL,
  `UnitMix` varchar(255) NOT NULL,
  `DescProp` longtext NOT NULL,
  `AreaNotes` longtext NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_rental_headlines`
--

INSERT INTO `ci_rental_headlines` (`HeadlineID`, `PropertyID`, `UserID`, `Headline`, `SubHeadline1`, `SubHeadline2`, `SubHeadline3`, `SubHeadline4`, `SubHeadline5`, `UnitMix`, `DescProp`, `AreaNotes`, `CreatedOn`) VALUES
(1, 1448011135, 3, 'Headline', 'Sub Headline 1', 'Sub Headline 2', 'Sub Headline 3', 'Sub Headline 4', 'Sub Headline 5', '5', 'Description of Property', 'Notes on Area', '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_helpful_calculator`
--

CREATE TABLE IF NOT EXISTS `ci_rental_helpful_calculator` (
`RentalCalID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `ExpectedHoldingPeriod` double NOT NULL,
  `OptionOneRate` double NOT NULL,
  `OptionTwoRate` double NOT NULL,
  `OptionThreeRate` double NOT NULL,
  `OptionOnePoints` double NOT NULL,
  `OptionTwoPoints` double NOT NULL,
  `OptionThreePoints` double NOT NULL,
  `OptionOneFactor` double NOT NULL,
  `OptionTwoFactor` double NOT NULL,
  `OptionThreeFactor` double NOT NULL,
  `OptionOneRank` int(11) NOT NULL,
  `OptionTwoRank` int(11) NOT NULL,
  `OptionThreeRank` int(11) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_rental_helpful_calculator`
--

INSERT INTO `ci_rental_helpful_calculator` (`RentalCalID`, `PropertyID`, `UserID`, `ExpectedHoldingPeriod`, `OptionOneRate`, `OptionTwoRate`, `OptionThreeRate`, `OptionOnePoints`, `OptionTwoPoints`, `OptionThreePoints`, `OptionOneFactor`, `OptionTwoFactor`, `OptionThreeFactor`, `OptionOneRank`, `OptionTwoRank`, `OptionThreeRank`, `CreatedOn`) VALUES
(1, 1448011135, 3, 3, 5, 6.5, 7.25, 3, 1, 0, 0.18, 0.21, 0.22, 1, 2, 3, '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_helpful_calculator1`
--

CREATE TABLE IF NOT EXISTS `ci_rental_helpful_calculator1` (
`HelpCalcID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `MortgagePayments` double NOT NULL,
  `Insurance` double NOT NULL,
  `RealEstateTaxes` double NOT NULL,
  `HelpfulTotal` double NOT NULL,
  `MonthsReserve` int(11) NOT NULL,
  `AmountHold` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_rental_helpful_calculator1`
--

INSERT INTO `ci_rental_helpful_calculator1` (`HelpCalcID`, `PropertyID`, `UserID`, `MortgagePayments`, `Insurance`, `RealEstateTaxes`, `HelpfulTotal`, `MonthsReserve`, `AmountHold`, `CreatedOn`) VALUES
(1, 1448011135, 3, 6559.65, 200, 0, 6759.65, 0, 0, '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_holding_resale`
--

CREATE TABLE IF NOT EXISTS `ci_rental_holding_resale` (
`RentalResaleID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `YearSale` int(11) NOT NULL,
  `Resale` varchar(500) NOT NULL,
  `AppreciationRate` double NOT NULL,
  `DiscRate` double NOT NULL,
  `EquityInterestRatePaid` double NOT NULL,
  `ReinvestmentRateReceived` double NOT NULL,
  `PurchaseDate` varchar(255) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_rental_holding_resale`
--

INSERT INTO `ci_rental_holding_resale` (`RentalResaleID`, `PropertyID`, `UserID`, `YearSale`, `Resale`, `AppreciationRate`, `DiscRate`, `EquityInterestRatePaid`, `ReinvestmentRateReceived`, `PurchaseDate`, `CreatedOn`) VALUES
(1, 1448011135, 3, 5, 'Set Cap Rate', 5, 10, 10.17, 10, '12-01-2015', '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_operating_expenses`
--

CREATE TABLE IF NOT EXISTS `ci_rental_operating_expenses` (
`RentalExpenseseID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `OperatingExpensesOption_1` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_1` double NOT NULL,
  `OperatingExpenseAnnual_1` double NOT NULL,
  `OperatingExpensePercentage_1` double NOT NULL,
  `OperatingExpenseGrowthRate_1` double NOT NULL,
  `OperatingExpensesOption_2` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_2` double NOT NULL,
  `OperatingExpenseAnnual_2` double NOT NULL,
  `OperatingExpensePercentage_2` double NOT NULL,
  `OperatingExpenseGrowthRate_2` double NOT NULL,
  `OperatingExpensesOption_3` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_3` double NOT NULL,
  `OperatingExpenseAnnual_3` double NOT NULL,
  `OperatingExpensePercentage_3` double NOT NULL,
  `OperatingExpenseGrowthRate_3` double NOT NULL,
  `OperatingExpensesOption_4` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_4` double NOT NULL,
  `OperatingExpenseAnnual_4` double NOT NULL,
  `OperatingExpensePercentage_4` double NOT NULL,
  `OperatingExpenseGrowthRate_4` double NOT NULL,
  `OperatingExpensesOption_5` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_5` double NOT NULL,
  `OperatingExpenseAnnual_5` double NOT NULL,
  `OperatingExpensePercentage_5` double NOT NULL,
  `OperatingExpenseGrowthRate_5` double NOT NULL,
  `OperatingExpensesOption_6` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_6` double NOT NULL,
  `OperatingExpenseAnnual_6` double NOT NULL,
  `OperatingExpensePercentage_6` double NOT NULL,
  `OperatingExpenseGrowthRate_6` double NOT NULL,
  `OperatingExpensesOption_7` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_7` double NOT NULL,
  `OperatingExpenseAnnual_7` double NOT NULL,
  `OperatingExpensePercentage_7` double NOT NULL,
  `OperatingExpenseGrowthRate_7` double NOT NULL,
  `OperatingExpensesOption_8` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_8` double NOT NULL,
  `OperatingExpenseAnnual_8` double NOT NULL,
  `OperatingExpensePercentage_8` double NOT NULL,
  `OperatingExpenseGrowthRate_8` double NOT NULL,
  `OperatingExpensesOption_9` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_9` double NOT NULL,
  `OperatingExpenseAnnual_9` double NOT NULL,
  `OperatingExpensePercentage_9` double NOT NULL,
  `OperatingExpenseGrowthRate_9` double NOT NULL,
  `OperatingExpensesOption_10` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_10` double NOT NULL,
  `OperatingExpenseAnnual_10` double NOT NULL,
  `OperatingExpensePercentage_10` double NOT NULL,
  `OperatingExpenseGrowthRate_10` double NOT NULL,
  `OperatingExpensesOption_11` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_11` double NOT NULL,
  `OperatingExpenseAnnual_11` double NOT NULL,
  `OperatingExpensePercentage_11` double NOT NULL,
  `OperatingExpenseGrowthRate_11` double NOT NULL,
  `OperatingExpensesOption_12` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_12` double NOT NULL,
  `OperatingExpenseAnnual_12` double NOT NULL,
  `OperatingExpensePercentage_12` double NOT NULL,
  `OperatingExpenseGrowthRate_12` double NOT NULL,
  `OperatingExpensesOption_13` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_13` double NOT NULL,
  `OperatingExpenseAnnual_13` double NOT NULL,
  `OperatingExpensePercentage_13` double NOT NULL,
  `OperatingExpenseGrowthRate_13` double NOT NULL,
  `OperatingExpensesOption_14` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_14` double NOT NULL,
  `OperatingExpenseAnnual_14` double NOT NULL,
  `OperatingExpensePercentage_14` double NOT NULL,
  `OperatingExpenseGrowthRate_14` double NOT NULL,
  `OperatingExpensesOption_15` varchar(255) NOT NULL,
  `OperatingExpenseMonthly_15` double NOT NULL,
  `OperatingExpenseAnnual_15` double NOT NULL,
  `OperatingExpensePercentage_15` double NOT NULL,
  `OperatingExpenseGrowthRate_15` double NOT NULL,
  `OperatingExpenseWaterSewerMonthly` double NOT NULL,
  `OperatingExpenseWaterSewerAnnual` double NOT NULL,
  `OperatingExpenseWaterSewerPercentage` double NOT NULL,
  `OperatingExpenseWaterSewerGrowthRate` double NOT NULL,
  `OperatingExpenseElectricityMonthly` double NOT NULL,
  `OperatingExpenseElectricityAnnual` double NOT NULL,
  `OperatingExpenseElectricityPercentage` double NOT NULL,
  `OperatingExpenseElectricityGrowthRate` double NOT NULL,
  `OperatingExpenseGasMonthly` double NOT NULL,
  `OperatingExpenseGasAnnual` double NOT NULL,
  `OperatingExpenseGasPercentage` double NOT NULL,
  `OperatingExpenseGasGrowthRate` double NOT NULL,
  `OperatingExpenseFuelOilMonthly` double NOT NULL,
  `OperatingExpenseFuelOilAnnual` double NOT NULL,
  `OperatingExpenseFuelOilPercentage` double NOT NULL,
  `OperatingExpenseFuelOilGrowthRate` double NOT NULL,
  `OperatingExpenseOtherUtilitiesMonthly` double NOT NULL,
  `OperatingExpenseOtherUtilitiesAnnual` double NOT NULL,
  `OperatingExpenseOtherUtilitiesPercentage` double NOT NULL,
  `OperatingExpenseOtherUtilitiesGrowthRate` double NOT NULL,
  `OperatingExpenseReservesMonthly` double NOT NULL,
  `OperatingExpenseReservesAnnual` double NOT NULL,
  `OperatingExpenseReservesPercentage` double NOT NULL,
  `OperatingExpenseReservesGrowthRate` double NOT NULL,
  `OperatingExpenseTotalMonthly` double NOT NULL,
  `OperatingExpenseTotalAnnual` double NOT NULL,
  `OperatingExpenseTotalPercentage` double NOT NULL,
  `OperatingExpensesIncomeMonthly` double NOT NULL,
  `NetOperatingIncomeMonthly` double NOT NULL,
  `NetOperatingIncomeAnnual` double NOT NULL,
  `NetOperatingIncomePercentage` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_rental_operating_expenses`
--

INSERT INTO `ci_rental_operating_expenses` (`RentalExpenseseID`, `PropertyID`, `UserID`, `OperatingExpensesOption_1`, `OperatingExpenseMonthly_1`, `OperatingExpenseAnnual_1`, `OperatingExpensePercentage_1`, `OperatingExpenseGrowthRate_1`, `OperatingExpensesOption_2`, `OperatingExpenseMonthly_2`, `OperatingExpenseAnnual_2`, `OperatingExpensePercentage_2`, `OperatingExpenseGrowthRate_2`, `OperatingExpensesOption_3`, `OperatingExpenseMonthly_3`, `OperatingExpenseAnnual_3`, `OperatingExpensePercentage_3`, `OperatingExpenseGrowthRate_3`, `OperatingExpensesOption_4`, `OperatingExpenseMonthly_4`, `OperatingExpenseAnnual_4`, `OperatingExpensePercentage_4`, `OperatingExpenseGrowthRate_4`, `OperatingExpensesOption_5`, `OperatingExpenseMonthly_5`, `OperatingExpenseAnnual_5`, `OperatingExpensePercentage_5`, `OperatingExpenseGrowthRate_5`, `OperatingExpensesOption_6`, `OperatingExpenseMonthly_6`, `OperatingExpenseAnnual_6`, `OperatingExpensePercentage_6`, `OperatingExpenseGrowthRate_6`, `OperatingExpensesOption_7`, `OperatingExpenseMonthly_7`, `OperatingExpenseAnnual_7`, `OperatingExpensePercentage_7`, `OperatingExpenseGrowthRate_7`, `OperatingExpensesOption_8`, `OperatingExpenseMonthly_8`, `OperatingExpenseAnnual_8`, `OperatingExpensePercentage_8`, `OperatingExpenseGrowthRate_8`, `OperatingExpensesOption_9`, `OperatingExpenseMonthly_9`, `OperatingExpenseAnnual_9`, `OperatingExpensePercentage_9`, `OperatingExpenseGrowthRate_9`, `OperatingExpensesOption_10`, `OperatingExpenseMonthly_10`, `OperatingExpenseAnnual_10`, `OperatingExpensePercentage_10`, `OperatingExpenseGrowthRate_10`, `OperatingExpensesOption_11`, `OperatingExpenseMonthly_11`, `OperatingExpenseAnnual_11`, `OperatingExpensePercentage_11`, `OperatingExpenseGrowthRate_11`, `OperatingExpensesOption_12`, `OperatingExpenseMonthly_12`, `OperatingExpenseAnnual_12`, `OperatingExpensePercentage_12`, `OperatingExpenseGrowthRate_12`, `OperatingExpensesOption_13`, `OperatingExpenseMonthly_13`, `OperatingExpenseAnnual_13`, `OperatingExpensePercentage_13`, `OperatingExpenseGrowthRate_13`, `OperatingExpensesOption_14`, `OperatingExpenseMonthly_14`, `OperatingExpenseAnnual_14`, `OperatingExpensePercentage_14`, `OperatingExpenseGrowthRate_14`, `OperatingExpensesOption_15`, `OperatingExpenseMonthly_15`, `OperatingExpenseAnnual_15`, `OperatingExpensePercentage_15`, `OperatingExpenseGrowthRate_15`, `OperatingExpenseWaterSewerMonthly`, `OperatingExpenseWaterSewerAnnual`, `OperatingExpenseWaterSewerPercentage`, `OperatingExpenseWaterSewerGrowthRate`, `OperatingExpenseElectricityMonthly`, `OperatingExpenseElectricityAnnual`, `OperatingExpenseElectricityPercentage`, `OperatingExpenseElectricityGrowthRate`, `OperatingExpenseGasMonthly`, `OperatingExpenseGasAnnual`, `OperatingExpenseGasPercentage`, `OperatingExpenseGasGrowthRate`, `OperatingExpenseFuelOilMonthly`, `OperatingExpenseFuelOilAnnual`, `OperatingExpenseFuelOilPercentage`, `OperatingExpenseFuelOilGrowthRate`, `OperatingExpenseOtherUtilitiesMonthly`, `OperatingExpenseOtherUtilitiesAnnual`, `OperatingExpenseOtherUtilitiesPercentage`, `OperatingExpenseOtherUtilitiesGrowthRate`, `OperatingExpenseReservesMonthly`, `OperatingExpenseReservesAnnual`, `OperatingExpenseReservesPercentage`, `OperatingExpenseReservesGrowthRate`, `OperatingExpenseTotalMonthly`, `OperatingExpenseTotalAnnual`, `OperatingExpenseTotalPercentage`, `OperatingExpensesIncomeMonthly`, `NetOperatingIncomeMonthly`, `NetOperatingIncomeAnnual`, `NetOperatingIncomePercentage`, `CreatedOn`) VALUES
(1, 1448011135, 3, 'Taxes', 1000, 12000, 1.79, 3, 'Insurance', 500, 6000, 0.89, 3, 'Landscape', 200, 2400, 0.36, 3, 'Super', 100, 1200, 0.18, 3, 'Garbage', 0, 0, 0, 3, 'Exterminator', 0, 0, 0, 3, 'Snow Removal', 0, 0, 0, 3, 'Accounting', 0, 0, 0, 3, 'Legal', 0, 0, 0, 3, 'Management', 0, 0, 0, 3, 'Advertising', 0, 0, 0, 3, 'Repairs and Maintenance', 0, 0, 0, 3, 'Miscellanious', 0, 0, 0, 3, 'Other', 0, 0, 0, 3, 'Other', 0, 0, 0, 3, 100, 1200, 0.18, 3, 125, 1500, 0.22, 3, 75, 900, 0.13, 3, 0, 0, 0, 3, 10, 120, 0.02, 3, 0, 0, 0, 3, 2110, 25320, 3.77, 3.77, 53880, 646560, 96.23, '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_operating_income`
--

CREATE TABLE IF NOT EXISTS `ci_rental_operating_income` (
`RentalIncomeID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `PropertyNoOfUnit_1` int(11) NOT NULL,
  `PropertyUnitType_1` varchar(50) NOT NULL,
  `PropertySquareFeet_1` double NOT NULL,
  `PropertyMonthlyRent_1` double NOT NULL,
  `PropertyAnnualRent_1` double NOT NULL,
  `PropertyPercentage_1` double NOT NULL,
  `PropertyRentSqFtPercentage_1` double NOT NULL,
  `PropertyAnnualGrowthRate_1` double NOT NULL,
  `PropertyNoOfUnit_2` int(11) NOT NULL,
  `PropertyUnitType_2` varchar(50) NOT NULL,
  `PropertySquareFeet_2` double NOT NULL,
  `PropertyMonthlyRent_2` double NOT NULL,
  `PropertyAnnualRent_2` double NOT NULL,
  `PropertyPercentage_2` double NOT NULL,
  `PropertyRentSqFtPercentage_2` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_2` double NOT NULL,
  `PropertyNoOfUnit_3` int(11) NOT NULL,
  `PropertyUnitType_3` varchar(50) NOT NULL,
  `PropertySquareFeet_3` double NOT NULL,
  `PropertyMonthlyRent_3` double NOT NULL,
  `PropertyAnnualRent_3` double NOT NULL,
  `PropertyPercentage_3` double NOT NULL,
  `PropertyRentSqFtPercentage_3` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_3` double NOT NULL,
  `PropertyNoOfUnit_4` int(11) NOT NULL,
  `PropertyUnitType_4` varchar(50) NOT NULL,
  `PropertySquareFeet_4` double NOT NULL,
  `PropertyMonthlyRent_4` double NOT NULL,
  `PropertyAnnualRent_4` double NOT NULL,
  `PropertyPercentage_4` double NOT NULL,
  `PropertyRentSqFtPercentage_4` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_4` double NOT NULL,
  `PropertyNoOfUnit_5` int(11) NOT NULL,
  `PropertyUnitType_5` varchar(50) NOT NULL,
  `PropertySquareFeet_5` double NOT NULL,
  `PropertyMonthlyRent_5` double NOT NULL,
  `PropertyAnnualRent_5` double NOT NULL,
  `PropertyPercentage_5` double NOT NULL,
  `PropertyRentSqFtPercentage_5` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_5` double NOT NULL,
  `PropertyNoOfUnit_6` int(11) NOT NULL,
  `PropertyUnitType_6` varchar(50) NOT NULL,
  `PropertySquareFeet_6` double NOT NULL,
  `PropertyMonthlyRent_6` double NOT NULL,
  `PropertyAnnualRent_6` double NOT NULL,
  `PropertyPercentage_6` double NOT NULL,
  `PropertyRentSqFtPercentage_6` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_6` double NOT NULL,
  `PropertyNoOfUnit_7` int(11) NOT NULL,
  `PropertyUnitType_7` varchar(50) NOT NULL,
  `PropertySquareFeet_7` double NOT NULL,
  `PropertyMonthlyRent_7` double NOT NULL,
  `PropertyAnnualRent_7` double NOT NULL,
  `PropertyPercentage_7` double NOT NULL,
  `PropertyRentSqFtPercentage_7` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_7` double NOT NULL,
  `PropertyNoOfUnit_8` int(11) NOT NULL,
  `PropertyUnitType_8` varchar(50) NOT NULL,
  `PropertySquareFeet_8` double NOT NULL,
  `PropertyMonthlyRent_8` double NOT NULL,
  `PropertyAnnualRent_8` double NOT NULL,
  `PropertyPercentage_8` double NOT NULL,
  `PropertyRentSqFtPercentage_8` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_8` double NOT NULL,
  `PropertyNoOfUnit_9` int(11) NOT NULL,
  `PropertyUnitType_9` varchar(50) NOT NULL,
  `PropertySquareFeet_9` double NOT NULL,
  `PropertyMonthlyRent_9` double NOT NULL,
  `PropertyAnnualRent_9` double NOT NULL,
  `PropertyPercentage_9` double NOT NULL,
  `PropertyRentSqFtPercentage_9` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_9` double NOT NULL,
  `PropertyNoOfUnit_10` int(11) NOT NULL,
  `PropertyUnitType_10` varchar(50) NOT NULL,
  `PropertySquareFeet_10` double NOT NULL,
  `PropertyMonthlyRent_10` double NOT NULL,
  `PropertyAnnualRent_10` double NOT NULL,
  `PropertyPercentage_10` double NOT NULL,
  `PropertyRentSqFtPercentage_10` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_10` double NOT NULL,
  `PropertyNoOfUnit_11` int(11) NOT NULL,
  `PropertyUnitType_11` varchar(50) NOT NULL,
  `PropertySquareFeet_11` double NOT NULL,
  `PropertyMonthlyRent_11` double NOT NULL,
  `PropertyAnnualRent_11` double NOT NULL,
  `PropertyPercentage_11` double NOT NULL,
  `PropertyRentSqFtPercentage_11` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_11` double NOT NULL,
  `PropertyNoOfUnit_12` int(11) NOT NULL,
  `PropertyUnitType_12` varchar(50) NOT NULL,
  `PropertySquareFeet_12` double NOT NULL,
  `PropertyMonthlyRent_12` double NOT NULL,
  `PropertyAnnualRent_12` double NOT NULL,
  `PropertyPercentage_12` double NOT NULL,
  `PropertyRentSqFtPercentage_12` double DEFAULT NULL,
  `PropertyAnnualGrowthRate_12` double NOT NULL,
  `PropertyNoOfUnit_Total` int(11) NOT NULL,
  `PropertySquareFeet_Total` double NOT NULL,
  `GrossScheduledIncomeMonthly` double NOT NULL,
  `GrossScheduledIncomeAnnual` double NOT NULL,
  `GrossScheduledIncomePercentage` double NOT NULL,
  `VacancyLossPercentage` double NOT NULL,
  `VacancyLossMonthly` double NOT NULL,
  `VacancyLossAnnual` double NOT NULL,
  `ConcessionsParcentage` double NOT NULL,
  `ConcessionsMonthly` double NOT NULL,
  `ConcessionsAnnual` double NOT NULL,
  `ManagementFeePercentage` double NOT NULL,
  `ManagementFeeMonthly` double NOT NULL,
  `ManagementFeeAnnual` double NOT NULL,
  `OtherIncomeMonthly` double NOT NULL,
  `OtherIncomeAnnual` double NOT NULL,
  `OtherIncomeAnnualGrowthRate` double NOT NULL,
  `OperatingIncomeMonthlyTotal` double NOT NULL,
  `OperatingIncomeAnnualTotal` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_rental_operating_income`
--

INSERT INTO `ci_rental_operating_income` (`RentalIncomeID`, `PropertyID`, `UserID`, `PropertyNoOfUnit_1`, `PropertyUnitType_1`, `PropertySquareFeet_1`, `PropertyMonthlyRent_1`, `PropertyAnnualRent_1`, `PropertyPercentage_1`, `PropertyRentSqFtPercentage_1`, `PropertyAnnualGrowthRate_1`, `PropertyNoOfUnit_2`, `PropertyUnitType_2`, `PropertySquareFeet_2`, `PropertyMonthlyRent_2`, `PropertyAnnualRent_2`, `PropertyPercentage_2`, `PropertyRentSqFtPercentage_2`, `PropertyAnnualGrowthRate_2`, `PropertyNoOfUnit_3`, `PropertyUnitType_3`, `PropertySquareFeet_3`, `PropertyMonthlyRent_3`, `PropertyAnnualRent_3`, `PropertyPercentage_3`, `PropertyRentSqFtPercentage_3`, `PropertyAnnualGrowthRate_3`, `PropertyNoOfUnit_4`, `PropertyUnitType_4`, `PropertySquareFeet_4`, `PropertyMonthlyRent_4`, `PropertyAnnualRent_4`, `PropertyPercentage_4`, `PropertyRentSqFtPercentage_4`, `PropertyAnnualGrowthRate_4`, `PropertyNoOfUnit_5`, `PropertyUnitType_5`, `PropertySquareFeet_5`, `PropertyMonthlyRent_5`, `PropertyAnnualRent_5`, `PropertyPercentage_5`, `PropertyRentSqFtPercentage_5`, `PropertyAnnualGrowthRate_5`, `PropertyNoOfUnit_6`, `PropertyUnitType_6`, `PropertySquareFeet_6`, `PropertyMonthlyRent_6`, `PropertyAnnualRent_6`, `PropertyPercentage_6`, `PropertyRentSqFtPercentage_6`, `PropertyAnnualGrowthRate_6`, `PropertyNoOfUnit_7`, `PropertyUnitType_7`, `PropertySquareFeet_7`, `PropertyMonthlyRent_7`, `PropertyAnnualRent_7`, `PropertyPercentage_7`, `PropertyRentSqFtPercentage_7`, `PropertyAnnualGrowthRate_7`, `PropertyNoOfUnit_8`, `PropertyUnitType_8`, `PropertySquareFeet_8`, `PropertyMonthlyRent_8`, `PropertyAnnualRent_8`, `PropertyPercentage_8`, `PropertyRentSqFtPercentage_8`, `PropertyAnnualGrowthRate_8`, `PropertyNoOfUnit_9`, `PropertyUnitType_9`, `PropertySquareFeet_9`, `PropertyMonthlyRent_9`, `PropertyAnnualRent_9`, `PropertyPercentage_9`, `PropertyRentSqFtPercentage_9`, `PropertyAnnualGrowthRate_9`, `PropertyNoOfUnit_10`, `PropertyUnitType_10`, `PropertySquareFeet_10`, `PropertyMonthlyRent_10`, `PropertyAnnualRent_10`, `PropertyPercentage_10`, `PropertyRentSqFtPercentage_10`, `PropertyAnnualGrowthRate_10`, `PropertyNoOfUnit_11`, `PropertyUnitType_11`, `PropertySquareFeet_11`, `PropertyMonthlyRent_11`, `PropertyAnnualRent_11`, `PropertyPercentage_11`, `PropertyRentSqFtPercentage_11`, `PropertyAnnualGrowthRate_11`, `PropertyNoOfUnit_12`, `PropertyUnitType_12`, `PropertySquareFeet_12`, `PropertyMonthlyRent_12`, `PropertyAnnualRent_12`, `PropertyPercentage_12`, `PropertyRentSqFtPercentage_12`, `PropertyAnnualGrowthRate_12`, `PropertyNoOfUnit_Total`, `PropertySquareFeet_Total`, `GrossScheduledIncomeMonthly`, `GrossScheduledIncomeAnnual`, `GrossScheduledIncomePercentage`, `VacancyLossPercentage`, `VacancyLossMonthly`, `VacancyLossAnnual`, `ConcessionsParcentage`, `ConcessionsMonthly`, `ConcessionsAnnual`, `ManagementFeePercentage`, `ManagementFeeMonthly`, `ManagementFeeAnnual`, `OtherIncomeMonthly`, `OtherIncomeAnnual`, `OtherIncomeAnnualGrowthRate`, `OperatingIncomeMonthlyTotal`, `OperatingIncomeAnnualTotal`, `CreatedOn`) VALUES
(1, 1448011135, 3, 2, '1', 600, 2500, 60000, 8.547008547008547, 4.17, 5, 5, '2', 900, 3500, 210000, 29.914529914529915, 3.89, 5, 8, '3', 1250, 4500, 432000, 61.53846153846154, 3.6, 5, 0, '1', 0, 0, 0, 0, 0, 5, 0, '1', 0, 0, 0, 0, 0, 5, 0, '1', 0, 0, 0, 0, 0, 5, 0, '1', 0, 0, 0, 0, 0, 5, 0, '1', 0, 0, 0, 0, 0, 5, 0, '1', 0, 0, 0, 0, 0, 5, 0, '1', 0, 0, 0, 0, 0, 5, 0, '1', 0, 0, 0, 0, 0, 5, 0, '1', 0, 0, 0, 0, 0, 5, 15, 15700, 58500, 702000, 100, 3, 1755, 21060, 2, 1170, 14040, 1, 585, 7020, 1000, 12000, 5, 55990, 671880, '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_price_assumptions`
--

CREATE TABLE IF NOT EXISTS `ci_rental_price_assumptions` (
`RentalPriceAssumptionsID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `PurchasePrice` double NOT NULL,
  `OverrideInitialValue` varchar(50) NOT NULL,
  `InitialMarketValue` double NOT NULL,
  `FirstYearMonthlyCashflow` double NOT NULL,
  `FirstYearAnnualCOCReturn` double NOT NULL,
  `RentGrowthOveride` varchar(50) NOT NULL,
  `RentGrowthRate` double NOT NULL,
  `OpExpensesGrowthOverride` varchar(50) NOT NULL,
  `OpExpensesGrowthRate` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_rental_price_assumptions`
--

INSERT INTO `ci_rental_price_assumptions` (`RentalPriceAssumptionsID`, `PropertyID`, `UserID`, `PurchasePrice`, `OverrideInitialValue`, `InitialMarketValue`, `FirstYearMonthlyCashflow`, `FirstYearAnnualCOCReturn`, `RentGrowthOveride`, `RentGrowthRate`, `OpExpensesGrowthOverride`, `OpExpensesGrowthRate`, `CreatedOn`) VALUES
(1, 1448011135, 3, 1000000, 'Yes', 1500000, 47320.35, 224.44, '', 0, '', 0, '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_property_information`
--

CREATE TABLE IF NOT EXISTS `ci_rental_property_information` (
`RentalPropertyID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `PropertyName` varchar(255) NOT NULL,
  `PropertyStreetAddress` varchar(255) NOT NULL,
  `PropertyCityTown` varchar(255) NOT NULL,
  `PropertyStateProvince` varchar(255) NOT NULL,
  `PropertyZipCode` varchar(255) NOT NULL,
  `PropertyCountry` varchar(255) NOT NULL,
  `PropertyClosingDate` varchar(255) NOT NULL,
  `YourName` varchar(255) NOT NULL,
  `CompanyName` varchar(255) NOT NULL,
  `CompanyStreet` varchar(255) NOT NULL,
  `CompanyCity` varchar(255) NOT NULL,
  `CompanyState` varchar(255) NOT NULL,
  `CompanyZip` varchar(255) NOT NULL,
  `CompanyCountry` varchar(255) NOT NULL,
  `PhoneNumber` varchar(50) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `FacebookURL` varchar(255) NOT NULL,
  `LinkedinURL` varchar(255) NOT NULL,
  `TwitterURL` varchar(255) NOT NULL,
  `Website` varchar(255) NOT NULL,
  `Submitted` enum('Yes','No') NOT NULL DEFAULT 'No',
  `CreatedOn` datetime NOT NULL,
  `ProposalStatus` enum('draft','publish') NOT NULL DEFAULT 'draft'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_rental_property_information`
--

INSERT INTO `ci_rental_property_information` (`RentalPropertyID`, `PropertyID`, `UserID`, `PropertyName`, `PropertyStreetAddress`, `PropertyCityTown`, `PropertyStateProvince`, `PropertyZipCode`, `PropertyCountry`, `PropertyClosingDate`, `YourName`, `CompanyName`, `CompanyStreet`, `CompanyCity`, `CompanyState`, `CompanyZip`, `CompanyCountry`, `PhoneNumber`, `Email`, `FacebookURL`, `LinkedinURL`, `TwitterURL`, `Website`, `Submitted`, `CreatedOn`, `ProposalStatus`) VALUES
(1, 1448011135, 3, 'VIP Tower', '225 VIP Road', 'Kolkata', 'WB', '700059', 'IN', '12-01-2015', 'Tanushree Sen', 'Wisely', 'Guwahati', 'Guwahati', 'Assam', '568520', 'IN', '9706505789', 'sen.tanu2010@gmail.com', 'facebook.com/tanushree.sen', 'linkedin.com/pub/tanushree-sen', 'twitter.com/TanushreeSen', 'wisely.co', 'No', '2015-11-20 10:18:55', 'publish');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_property_photos`
--

CREATE TABLE IF NOT EXISTS `ci_rental_property_photos` (
`RentalPhotoID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Photos` longtext NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_rental_property_photos`
--

INSERT INTO `ci_rental_property_photos` (`RentalPhotoID`, `PropertyID`, `UserID`, `Photos`, `CreatedOn`) VALUES
(1, 1448011135, 3, 'http://localhost/cblist/assets/rental/3_1448011889681.jpg|http://localhost/cblist/assets/rental/3_2896023778687.jpg|http://localhost/cblist/assets/rental/3_4344035667547.jpg', '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_rental_tax_assumption`
--

CREATE TABLE IF NOT EXISTS `ci_rental_tax_assumption` (
`RentalTaxAssumptionID` int(11) NOT NULL,
  `PropertyID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `GAI` double NOT NULL,
  `DepYears` double NOT NULL,
  `LandPercentage` double NOT NULL,
  `TaxBracket` double NOT NULL,
  `CapitalGainsPercentage` double NOT NULL,
  `CostOfSale` double NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ci_rental_tax_assumption`
--

INSERT INTO `ci_rental_tax_assumption` (`RentalTaxAssumptionID`, `PropertyID`, `UserID`, `Designation`, `GAI`, `DepYears`, `LandPercentage`, `TaxBracket`, `CapitalGainsPercentage`, `CostOfSale`, `CreatedOn`) VALUES
(1, 1448011135, 3, 'Passive Participant', 100000, 27.5, 20, 30, 20, 7, '2015-11-20 10:18:55');

-- --------------------------------------------------------

--
-- Table structure for table `ci_sessions`
--

CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(40) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('20a4fb0f78e67dfd6202338353f1f1c21cf08cc8', '::1', 1448256686, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383235363638363b),
('2ad7304cdaef445fa1f507e48a1d8dc19d8e069e', '::1', 1448263768, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383236333736383b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('49731af28c9b560383ce5d9b73ed20bcae45305e', '::1', 1448011036, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383031313033363b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('5465a210a5a5004418ccc6f9320cac1a1db585ee', '::1', 1448260204, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383236303230343b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('57cb02167dc04ce26ae8ef2218be1ad5964a0575', '::1', 1448012732, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383031323733323b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('5f2c5ff96191b03033d230ada0fdf2e640ba3af7', '::1', 1448010599, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383031303539393b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('60ca656c7899a268949f8e87c744b69fa2acf672', '::1', 1448258193, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383235383139333b),
('60f8aefd5e0b0239190c3fc9b177b4ffe68d8ca0', '::1', 1448010263, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383031303236333b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('63b785e5ee89e35aa7ee13f1d8ef84e4f1437bd7', '::1', 1448259967, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383235393837353b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('748ba838abc3ebe5d396fb8d4ca377f301165c68', '::1', 1448264741, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383236343734313b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('75d2e2a4a094d7c0850d825022a49dfc6051d9d9', '::1', 1448257084, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383235373038343b),
('79e58376c809758aadb1b6d79e8d9f48421889e2', '::1', 1448011743, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383031313734333b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('842a91ad2e8a9b4efc35eba21d6d64597db1a7b2', '::1', 1448012092, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383031323039323b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('af909ed92207e7e1732760504220ba03e81c9dda', '::1', 1448009806, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383030393732303b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('d9061fb7b3e7f93c78e64dc3af1aff07b9565d96', '::1', 1448013402, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383031333430323b),
('e34d1c59adf5ee4ae2c6668e2f34bb5ab94d1dea', '::1', 1448262668, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383236323636383b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b),
('e39d4cc9b7e67a720794421b18dd18d39cbf66bd', '::1', 1448261042, 0x5f5f63695f6c6173745f726567656e65726174657c693a313434383236313034323b757365725f69647c733a313a2233223b6c696e6b6564696e7c733a303a22223b757365725f656d61696c7c733a32323a2273656e2e74616e753230313040676d61696c2e636f6d223b6e616d657c733a31333a2254616e7573687265652053656e223b6c6f676765645f696e7c623a313b);

-- --------------------------------------------------------

--
-- Table structure for table `ci_users`
--

CREATE TABLE IF NOT EXISTS `ci_users` (
`id` int(11) NOT NULL,
  `linkedin` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fname` varchar(255) NOT NULL,
  `dob` varchar(255) NOT NULL,
  `picture` varchar(255) NOT NULL,
  `picture_medium` varchar(255) NOT NULL,
  `picture_small` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `town` varchar(255) NOT NULL,
  `state` varchar(255) NOT NULL,
  `zip` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `facebook_url` varchar(255) NOT NULL,
  `twitter_url` varchar(255) NOT NULL,
  `linkedin_url` longtext NOT NULL,
  `recommenders` int(11) NOT NULL,
  `register_on` datetime NOT NULL,
  `user_type` varchar(50) DEFAULT NULL,
  `user_experience` double NOT NULL,
  `profile_completion` varchar(5) NOT NULL,
  `paidstatus` enum('0','1') NOT NULL DEFAULT '0',
  `paidtype` enum('N','M','H') NOT NULL DEFAULT 'N'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ci_users`
--

INSERT INTO `ci_users` (`id`, `linkedin`, `email`, `phone`, `password`, `fname`, `dob`, `picture`, `picture_medium`, `picture_small`, `street`, `town`, `state`, `zip`, `country`, `facebook_url`, `twitter_url`, `linkedin_url`, `recommenders`, `register_on`, `user_type`, `user_experience`, `profile_completion`, `paidstatus`, `paidtype`) VALUES
(1, '', 'dummy.investor@gmail.com', '9038077088', '39bd593cddb6fecdcf9783709a0355c01fc05061', 'Surajit Pramanik', '08-26-1987', 'http://wisely.co/cblist/assets/picture/1_1437149706987.jpg', 'http://wisely.co/cblist/assets/picture/1_1437149706987-200x200.jpg', 'http://wisely.co/cblist/assets/picture/1_1437149706987-80x80.jpg', 'RF 22/4, Raghunathpur', 'Kolkata', 'West Bengal', '700059', 'IN', 'https://www.facebook.com/surajit.pramanik', 'https://www.twitter.com/SurajitPramani2', 'https://www.linkedin.com/pub/surajit-pramanik', 0, '2015-03-18 00:46:08', 'investor', 0, 'no', '1', 'M'),
(2, '', 'dummy.lender@gmail.com', '9804105617', '39bd593cddb6fecdcf9783709a0355c01fc05061', 'Surajit Pramanik', '07-18-1985', 'http://wisely.co/cblist/assets/picture/2_1435185971892.jpg', 'http://wisely.co/cblist/assets/picture/2_1435185971892-200x200.jpg', 'http://wisely.co/cblist/assets/picture/2_1435185971892-80x80.jpg', 'Kolkata', 'Kolkata', 'WB', '700059', 'IN', 'https://www.facebook.com/sueajit.pramanik', 'https://www.twitter.com/SurajitPramanik', 'https://www.linkedin.com/pub/surajit-pramanik', 0, '2015-03-27 17:44:20', 'cash_lender', 0, '', '1', 'H'),
(3, '', 'sen.tanu2010@gmail.com', '9706505789', '39bd593cddb6fecdcf9783709a0355c01fc05061', 'Tanushree Sen', '10-05-1990', 'http://www.wisely.co/cblist/assets/picture/3-1434557376.jpg', 'http://www.wisely.co/cblist/assets/picture/3-1434557376-200x200.jpg', 'http://www.wisely.co/cblist/assets/picture/3-1434557376-80x80.jpg', 'Guwahati', 'Guwahati', 'Assam', '568520', 'IN', 'facebook.com/tanushree.sen', 'twitter.com/TanushreeSen', 'linkedin.com/pub/tanushree-sen', 0, '2015-05-28 09:53:58', 'investor', 0, 'no', '1', 'M'),
(4, '', 'subhro.pramanik7@gmail.com', '9804526352', '988ae52554700f1fa03014a57f4fb99d4782fa68', 'Subhro Pramanik', '04-07-1988', 'http://wisely.co/cblist/assets/images/default-pic.jpg', 'http://wisely.co/cblist/assets/images/default-pic.jpg', 'http://wisely.co/cblist/assets/images/default-pic-small.jpg', 'Kolkata, WB', 'Not updated', 'Not updated', 'Not updated', 'IN', 'https://www.facebook.com', 'https://www.twitter.com', 'https://www.linkedin.com/', 0, '2015-05-28 18:34:27', 'investor', 0, 'no', '1', 'M'),
(5, '', 'jitdxpert@hotmail.com', 'Not updated', '39bd593cddb6fecdcf9783709a0355c01fc05061', 'Desmond Davidson', 'Not updated', 'http://wisely.co/cblist/assets/images/default-pic.jpg', 'http://wisely.co/cblist/assets/images/default-pic.jpg', 'http://wisely.co/cblist/assets/images/default-pic-small.jpg', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 0, '2015-05-29 07:13:42', 'investor', 0, 'no', '1', 'H'),
(6, '', 'webtestersudipsen@gmail.com', 'Not updated', '2adf4ace91b5811d10bbf282eee99affaf67604d', 'Sudip Sen', 'Not updated', 'http://wisely.co/cblist/assets/images/default-pic.jpg', 'http://wisely.co/cblist/assets/images/default-pic.jpg', 'http://wisely.co/cblist/assets/images/default-pic-small.jpg', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 0, '2015-06-13 09:16:03', 'investor', 0, 'no', '0', 'N'),
(7, '', 'jitdxpert@ymail.com', '9830253620', '39bd593cddb6fecdcf9783709a0355c01fc05061', 'Anand Jaiswal', '06-01-1987', 'http://wisely.co/cblist/assets/picture/7_1435182818919.jpg', 'http://wisely.co/cblist/assets/picture/7_1435182818919-200x200.jpg', 'http://wisely.co/cblist/assets/picture/7_1435182818919-80x80.jpg', 'Mumbai', 'Not updated', 'Not updated', 'Not updated', 'IN', 'https://www.facebook.com/anand.jaiswal', 'https://www.twitter.com/AnandJaiswal', 'https://www.linkedin.com/pub/anand.jaiswal', 0, '2015-06-13 10:12:00', 'cash_lender', 0, '', '1', 'H'),
(8, '', 'test.investor@gmail.com', '9083614277', '39bd593cddb6fecdcf9783709a0355c01fc05061', 'Art Matuschat', '06-22-1966', 'http://www.wisely.co/cblist/assets/picture/8_1435890955763.JPG', 'http://www.wisely.co/cblist/assets/picture/8_1435890955763-200x200.JPG', 'http://www.wisely.co/cblist/assets/picture/8_1435890955763-80x80.JPG', '108 Potomac Drive', 'Basking Ridge', 'NJ', '07920', 'US', 'facebook.com/art.matuschat', 'twitter.com/artmatuschat', 'linkedin.com/in/artmatuschat/', 0, '2015-05-28 09:53:58', 'investor', 0, 'no', '1', 'M'),
(9, '', 'test.cashlender@gmail.com', '9083614277', '39bd593cddb6fecdcf9783709a0355c01fc05061', 'John  G. Moneybags', '06-17-2015', 'http://www.wisely.co/cblist/assets/picture/9_1437079702683.jpg', 'http://www.wisely.co/cblist/assets/picture/9_1437079702683-200x200.jpg', 'http://www.wisely.co/cblist/assets/picture/9_1437079702683-80x80.jpg', '411 KingGeorge Road', 'Basking Ridge', 'NJ', '07920', 'US', 'https://www.facebook.com/newjerseyrealtysolutions', 'https://twitter.com/NJRSolutions', 'www.linkedin.com/in/artmatuschat/', 0, '2015-06-13 10:12:00', 'cash_lender', 0, '', '1', 'H'),
(10, '', 'tanushree.new@gmail.com', '9988762134', '8cb2237d0679ca88db6464eac60da96345513964', 'Tanushree', '06-01-2008', 'http://wisely.co/cblist/assets/picture/10_1437142059637.jpg', 'http://wisely.co/cblist/assets/picture/10_1437142059637-200x200.jpg', 'http://wisely.co/cblist/assets/picture/10_1437142059637-80x80.jpg', 'Hill road', 'new delhi', 'Delhi', '120003', 'IN', 'https://www.facebook.com/tanushree', 'https://twitter.com/tanushree', 'https://www.linkedin.com/ads/', 0, '2015-05-28 09:53:58', 'investor', 0, 'no', '0', 'N'),
(11, '', 'sudip.new@gmail.com', '9087654323', '39bd593cddb6fecdcf9783709a0355c01fc05061', 'Sudip Sen', '09-09-2015', 'http://wisely.co/cblist/assets/picture/11_1437220379513.jpg', 'http://wisely.co/cblist/assets/picture/11_1437220379513-200x200.jpg', 'http://wisely.co/cblist/assets/picture/11_1437220379513-80x80.jpg', 'delhi', 'new delhi', 'delhi', '12345', 'AT', 'facebook.com/tanushree', 'twitter.com/tanushree', 'linkedin.com/in/gregorwegener', 0, '2015-05-28 09:53:58', 'investor', 0, 'no', '1', 'H'),
(12, '', 'jitdxpert@gmail.com', '9038077088', '39bd593cddb6fecdcf9783709a0355c01fc05061', 'Surajit Pramanik', '08-26-1987', 'http://localhost/cblist/assets/picture/12_1446958741161.jpg', 'http://localhost/cblist/assets/picture/12_1446958741161-200x200.jpg', 'http://localhost/cblist/assets/picture/12_1446958741161-80x80.jpg', 'RF 22/4, Raghunathpur', 'Kolkata', 'West Bengal', '700059', 'IN', 'facebook.com/surajit.pramanik', 'twitter.com/SurajitPramanik', 'linkedin.com/pub/surajit-pramanik', 0, '2015-07-27 07:19:10', 'investor', 0, 'no', '1', 'H'),
(13, '', 'haldarapu@gmail.com', '8017682501', '5c797fb9baeb8a339f305538119cead5269c28c7', 'Apurba Haldar', '03-10-1990', 'http://soumitrapaul.com/cblist/assets/picture/13_1438176480330.jpg', 'http://soumitrapaul.com/cblist/assets/picture/13_1438176480330-200x200.jpg', 'http://soumitrapaul.com/cblist/assets/picture/13_1438176480330-80x80.jpg', '63,Kaverappa Layout', 'Bangalore', 'Karnataka', '560103', 'IN', 'https://www.facebook.com/apurba.haldar', 'https://twitter.com/apurba65', 'https://in.linkedin.com/in/apurbahaldar', 0, '2015-07-27 07:19:10', 'investor', 0, 'no', '0', 'N'),
(14, '', 'paul.nanigopal@gmail.com', 'Not updated', 'd909b9a37a2e8438532da53e4dedb298af72a8f0', 'Tsen', 'Not updated', 'http://www.soumitrapaul.com/cblist/assets/images/default-pic.jpg', 'http://www.soumitrapaul.com/cblist/assets/images/default-pic.jpg', 'http://www.soumitrapaul.com/cblist/assets/images/default-pic-small.jpg', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 'Not updated', 0, '2015-07-29 07:48:00', 'investor', 0, 'no', '0', 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ci_analyser_extra_details`
--
ALTER TABLE `ci_analyser_extra_details`
 ADD PRIMARY KEY (`ExtraID`);

--
-- Indexes for table `ci_analyser_financing_assumption`
--
ALTER TABLE `ci_analyser_financing_assumption`
 ADD PRIMARY KEY (`ComparisonID`);

--
-- Indexes for table `ci_analyser_property_details`
--
ALTER TABLE `ci_analyser_property_details`
 ADD PRIMARY KEY (`DetailedID`);

--
-- Indexes for table `ci_closing_costs`
--
ALTER TABLE `ci_closing_costs`
 ADD PRIMARY KEY (`ClosingID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_comparable_reports`
--
ALTER TABLE `ci_comparable_reports`
 ADD PRIMARY KEY (`ReportID`);

--
-- Indexes for table `ci_financing_assumption`
--
ALTER TABLE `ci_financing_assumption`
 ADD PRIMARY KEY (`FinancingID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_flip_analysis`
--
ALTER TABLE `ci_flip_analysis`
 ADD PRIMARY KEY (`FlipID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_flip_budget`
--
ALTER TABLE `ci_flip_budget`
 ADD PRIMARY KEY (`FlipBudgetID`);

--
-- Indexes for table `ci_holding_costs`
--
ALTER TABLE `ci_holding_costs`
 ADD PRIMARY KEY (`HoldingID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_max_offer`
--
ALTER TABLE `ci_max_offer`
 ADD PRIMARY KEY (`OfferID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_offer_rate`
--
ALTER TABLE `ci_offer_rate`
 ADD PRIMARY KEY (`RentalID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_operating_expenses`
--
ALTER TABLE `ci_operating_expenses`
 ADD PRIMARY KEY (`ExpensesID`);

--
-- Indexes for table `ci_operating_income`
--
ALTER TABLE `ci_operating_income`
 ADD PRIMARY KEY (`IncomeID`);

--
-- Indexes for table `ci_payment`
--
ALTER TABLE `ci_payment`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_personal_information`
--
ALTER TABLE `ci_personal_information`
 ADD PRIMARY KEY (`PersonalID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_portfolio`
--
ALTER TABLE `ci_portfolio`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_previouspayment`
--
ALTER TABLE `ci_previouspayment`
 ADD PRIMARY KEY (`old_payment_id`);

--
-- Indexes for table `ci_profile`
--
ALTER TABLE `ci_profile`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_property_headlines`
--
ALTER TABLE `ci_property_headlines`
 ADD PRIMARY KEY (`HeadlineID`);

--
-- Indexes for table `ci_property_information`
--
ALTER TABLE `ci_property_information`
 ADD PRIMARY KEY (`InfoID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_property_photos`
--
ALTER TABLE `ci_property_photos`
 ADD PRIMARY KEY (`PhotoID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_proposal_view`
--
ALTER TABLE `ci_proposal_view`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_purchase_assumption`
--
ALTER TABLE `ci_purchase_assumption`
 ADD PRIMARY KEY (`PurchaseID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_refi_analysis`
--
ALTER TABLE `ci_refi_analysis`
 ADD PRIMARY KEY (`RefiID`), ADD UNIQUE KEY `PropertyID` (`PropertyID`);

--
-- Indexes for table `ci_refi_budget`
--
ALTER TABLE `ci_refi_budget`
 ADD PRIMARY KEY (`RefiBudgetID`);

--
-- Indexes for table `ci_rental_advance`
--
ALTER TABLE `ci_rental_advance`
 ADD PRIMARY KEY (`RentalAdvanceID`);

--
-- Indexes for table `ci_rental_cash_outlay`
--
ALTER TABLE `ci_rental_cash_outlay`
 ADD PRIMARY KEY (`RentalCashOutlayID`);

--
-- Indexes for table `ci_rental_financial_analysis`
--
ALTER TABLE `ci_rental_financial_analysis`
 ADD PRIMARY KEY (`RentalFinancilaAnalysisID`);

--
-- Indexes for table `ci_rental_headlines`
--
ALTER TABLE `ci_rental_headlines`
 ADD PRIMARY KEY (`HeadlineID`);

--
-- Indexes for table `ci_rental_helpful_calculator`
--
ALTER TABLE `ci_rental_helpful_calculator`
 ADD PRIMARY KEY (`RentalCalID`);

--
-- Indexes for table `ci_rental_helpful_calculator1`
--
ALTER TABLE `ci_rental_helpful_calculator1`
 ADD PRIMARY KEY (`HelpCalcID`);

--
-- Indexes for table `ci_rental_holding_resale`
--
ALTER TABLE `ci_rental_holding_resale`
 ADD PRIMARY KEY (`RentalResaleID`);

--
-- Indexes for table `ci_rental_operating_expenses`
--
ALTER TABLE `ci_rental_operating_expenses`
 ADD PRIMARY KEY (`RentalExpenseseID`);

--
-- Indexes for table `ci_rental_operating_income`
--
ALTER TABLE `ci_rental_operating_income`
 ADD PRIMARY KEY (`RentalIncomeID`);

--
-- Indexes for table `ci_rental_price_assumptions`
--
ALTER TABLE `ci_rental_price_assumptions`
 ADD PRIMARY KEY (`RentalPriceAssumptionsID`);

--
-- Indexes for table `ci_rental_property_information`
--
ALTER TABLE `ci_rental_property_information`
 ADD PRIMARY KEY (`RentalPropertyID`);

--
-- Indexes for table `ci_rental_property_photos`
--
ALTER TABLE `ci_rental_property_photos`
 ADD PRIMARY KEY (`RentalPhotoID`);

--
-- Indexes for table `ci_rental_tax_assumption`
--
ALTER TABLE `ci_rental_tax_assumption`
 ADD PRIMARY KEY (`RentalTaxAssumptionID`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `ci_sessions_id_ip` (`id`,`ip_address`), ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `ci_users`
--
ALTER TABLE `ci_users`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ci_analyser_extra_details`
--
ALTER TABLE `ci_analyser_extra_details`
MODIFY `ExtraID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ci_analyser_financing_assumption`
--
ALTER TABLE `ci_analyser_financing_assumption`
MODIFY `ComparisonID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ci_analyser_property_details`
--
ALTER TABLE `ci_analyser_property_details`
MODIFY `DetailedID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ci_closing_costs`
--
ALTER TABLE `ci_closing_costs`
MODIFY `ClosingID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_comparable_reports`
--
ALTER TABLE `ci_comparable_reports`
MODIFY `ReportID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ci_financing_assumption`
--
ALTER TABLE `ci_financing_assumption`
MODIFY `FinancingID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_flip_analysis`
--
ALTER TABLE `ci_flip_analysis`
MODIFY `FlipID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_flip_budget`
--
ALTER TABLE `ci_flip_budget`
MODIFY `FlipBudgetID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `ci_holding_costs`
--
ALTER TABLE `ci_holding_costs`
MODIFY `HoldingID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_max_offer`
--
ALTER TABLE `ci_max_offer`
MODIFY `OfferID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ci_offer_rate`
--
ALTER TABLE `ci_offer_rate`
MODIFY `RentalID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ci_operating_expenses`
--
ALTER TABLE `ci_operating_expenses`
MODIFY `ExpensesID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_operating_income`
--
ALTER TABLE `ci_operating_income`
MODIFY `IncomeID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_payment`
--
ALTER TABLE `ci_payment`
MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `ci_personal_information`
--
ALTER TABLE `ci_personal_information`
MODIFY `PersonalID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_portfolio`
--
ALTER TABLE `ci_portfolio`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ci_previouspayment`
--
ALTER TABLE `ci_previouspayment`
MODIFY `old_payment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ci_profile`
--
ALTER TABLE `ci_profile`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ci_property_headlines`
--
ALTER TABLE `ci_property_headlines`
MODIFY `HeadlineID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_property_information`
--
ALTER TABLE `ci_property_information`
MODIFY `InfoID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_property_photos`
--
ALTER TABLE `ci_property_photos`
MODIFY `PhotoID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_proposal_view`
--
ALTER TABLE `ci_proposal_view`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ci_purchase_assumption`
--
ALTER TABLE `ci_purchase_assumption`
MODIFY `PurchaseID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_refi_analysis`
--
ALTER TABLE `ci_refi_analysis`
MODIFY `RefiID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_refi_budget`
--
ALTER TABLE `ci_refi_budget`
MODIFY `RefiBudgetID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `ci_rental_advance`
--
ALTER TABLE `ci_rental_advance`
MODIFY `RentalAdvanceID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_cash_outlay`
--
ALTER TABLE `ci_rental_cash_outlay`
MODIFY `RentalCashOutlayID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_financial_analysis`
--
ALTER TABLE `ci_rental_financial_analysis`
MODIFY `RentalFinancilaAnalysisID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_headlines`
--
ALTER TABLE `ci_rental_headlines`
MODIFY `HeadlineID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_helpful_calculator`
--
ALTER TABLE `ci_rental_helpful_calculator`
MODIFY `RentalCalID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_helpful_calculator1`
--
ALTER TABLE `ci_rental_helpful_calculator1`
MODIFY `HelpCalcID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_holding_resale`
--
ALTER TABLE `ci_rental_holding_resale`
MODIFY `RentalResaleID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_operating_expenses`
--
ALTER TABLE `ci_rental_operating_expenses`
MODIFY `RentalExpenseseID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_operating_income`
--
ALTER TABLE `ci_rental_operating_income`
MODIFY `RentalIncomeID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_price_assumptions`
--
ALTER TABLE `ci_rental_price_assumptions`
MODIFY `RentalPriceAssumptionsID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_property_information`
--
ALTER TABLE `ci_rental_property_information`
MODIFY `RentalPropertyID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_property_photos`
--
ALTER TABLE `ci_rental_property_photos`
MODIFY `RentalPhotoID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_rental_tax_assumption`
--
ALTER TABLE `ci_rental_tax_assumption`
MODIFY `RentalTaxAssumptionID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ci_users`
--
ALTER TABLE `ci_users`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
