<?php if ( !defined('BASEPATH') ) exit('No direct script access allowed');

require_once APPPATH . "/third_party/dompdf/dompdf_config.inc.php";

class DomPdf_lib extends DOMPDF {
	public function __construct() {
		parent::__construct();
	}
}