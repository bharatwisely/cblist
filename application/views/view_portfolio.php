<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
		                <h2>Portfolio</h2>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    	<?php if ( !$portfolio ) { ?>
                        	<a href="<?=base_url('pages/new-portfolio')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to create your first portfolio."><i class="fa fa-plus-square-o"></i> Create Portfolio</a>
                        <?php } elseif ( !$proposal ) { ?>
                        	<a href="<?=base_url('pages/proposal-writer')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to post your first proposal."><i class="fa fa-plus-square-o"></i> Create Proposal</a>
                        <?php } else { ?>
                        	<!-- No action needed -->
                        <?php } ?>
                    </div>
                </div>
                
                <div class="page-content">
                	<?php if ( !$portfolio ) { ?>
                        <div class="alert alert-warning" role="alert">
                        	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-info-circle"></i> Now you have to create your portfolio. For creating your first portfolio click the above '<a href="<?=base_url('pages/new-portfolio')?>/"><u>Create Portfolio</u></a>' botton.&nbsp;&nbsp;&nbsp; 
                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Portfolio Tutorial">
                                <span data-target="#portfolio_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                            </a>
                        </div>
                    <?php } elseif ( !$proposal ) { ?>
                        <div class="alert alert-warning" role="alert">
                        	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-info-circle"></i> Now you have to create your proposal. For creating your first proposal click the above '<a href="<?=base_url('pages/proposal-writer')?>/"><u>Create Proposal</u></a>' botton.&nbsp;&nbsp;&nbsp;
                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Proposal Tutorial">
                                <span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                            </a>
                        </div>
                    <?php } else { ?>
                        <!-- No action needed -->
                    <?php } ?>
                	
					<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-file-picture-o"></i> Portfolio Lists</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#portfolio_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                    <h3 class="box-title pull-right">
                                        <a href="<?=base_url('pages/new-portfolio')?>/" class="action-btn" data-toggle="tooltip" data-placement="top" title="New Portfolio">
                                            <i class="fa fa-plus-square-o"></i> New
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
                                    <?php if ( $this->session->userdata('portfolio_deleted') ) { ?>
                                        <div class="alert alert-success" role="alert">
                                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-info-circle"></i> Portfolio is successfully deleted.
                                        </div>
                                    <?php } ?>
                            
                                    <?php if ( $portfolio ) { $i = 0; ?>
                                        <?php foreach ( $portfolio as $k => $port ) {
                                            if ( $i % 2 == 0 ) { $bg = '#F8F8F8'; } else { $bg = ''; } ?>
                                            <div class="container-fluid portfolio-row" style="background-color:<?php echo $bg; ?>;">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="col-xs-12">
                                                            <?php if ( $port->images ) { ?>
                                                                <div class="portfolio-thumb-sec">
                                                                    <?php $i = 0; $images = explode('|', $port->images); ?>
                                                                    <?php foreach ( $images as $img ) { ?>
                                                                        <?php $display = ($i < 4) ? 'style="display:block;"' : 'style="display:none;"'; ?>
                                                                        <div class="portfolio-thumb" <?=$display?>>
                                                                            <a class="fancybox" href="<?=$img?>" data-fancybox-group="gallery-<?=$port->id?>">
                                                                                <img src="<?=$img?>" alt="portfolio" width="115" height="90" />
                                                                            </a>
                                                                        </div>
                                                                        <?php $i++; ?>
                                                                    <?php } ?>
                                                                </div>
                                                            <?php } ?>
                                                            <div class="portfolio-desc-sec">
                                                                <p>
                                                                    <strong><?=$port->property_name?></strong> 
                                                                    <span class="pull-right">
                                                                        <a class="action-btn" href="<?=base_url('pages/edit-portfolio/' . $port->id . '/portfolio')?>/" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <a class="action-btn" onclick="return confirm('Are you want to delete this portfolio?');" href="<?=base_url('pages/delete-portfolio/' . $port->id . '/portfolio')?>/" data-toggle="tooltip" data-placement="top" title="Delete"><i class="fa fa-close"></i></a>
                                                                    </span>
                                                                </p>
                                                                <p><?=$port->location?></p>
                                                                <p><?=mb_substr($port->info_one, 0, 350)?></p>
                                                                <p><?=mb_substr($port->info_two, 0, 300)?></p>
                                                                <p><a href="<?=base_url('pages/investor-portfolio/' . $port->id . '/portfolio')?>/"><i class="fa fa-caret-square-o-right"></i> Read More</a></p>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $i++; } ?>
                                    <?php } else { ?>
                                        <p>You have not create any portfolio yet. Create your first portfolio from <a href="<?=base_url('pages/new-portfolio')?>/">here</a>.</p>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>