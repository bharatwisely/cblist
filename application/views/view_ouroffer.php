<header id="header">
	<div class="container">
    	<div class="row">
        	<nav class="navbar">
            	<div class="container-fluid">
                	<div class="navbar-header">
                    	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        	<span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1 class="home-logo">
                        	<a class="navbar-brand" href="<?=base_url()?>">
                        		<img src="<?=base_url('assets/images/logo.png')?>" alt="CBList" width="200" />
                        	</a>
                        </h1>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                        	<li <?php if($this->uri->segment(2) == ''):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url()?>">Home</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'aboutus'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/aboutus')?>/">About Us</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'blog'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/blog')?>/">Blog</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'our-offer'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'features'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </li>
                            <?php if ( $user_id ) { ?>
                                <li>
                                    <a href="<?=base_url('pages/profile')?>/">Profile</a>
                                </li>
                                <li>
                                    <a href="<?=base_url('pages/logout')?>/">Logout</a>
                                </li>
                            <?php } else { ?>
                                <li <?php if($this->uri->segment(2) == 'investors-login'):?> class="active" <?php else:?> <?php endif;?>>
                                    <a href="<?=base_url('pages/investors-login')?>/">Investors Login</a>
                                </li>
                                <li <?php if($this->uri->segment(2) == 'cashlenders-login'):?> class="active" <?php else:?> <?php endif;?>>
                                    <a href="<?=base_url('pages/cashlenders-login')?>/">Cash Lenders Login</a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>        	
    </div>
</header>

<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
            	<h1 style="margin-top:0;">Our Offer</h1>
            	<p>Congratulations!</p>
                <p>You are moments away from securing your copy of the Deals For Life Program and getting started in one of the most exciting & lucrative business activities in America today … FINDING, FUNDING, FIXING & FLIPPING real estate</p>
            
                <p>Thank you for putting your trust and confidence in me and in my Deals For Life Program.  I promise you will be delighted!</p>
                <p>Deals For Life gives you the EXACT, step-by-step process I use find, fund, fix and flip one great real estate deal after another and earn a very comfortable six-figure income doing it. </p>
                <p>Whether your goal is get out of debt, pay cash for a couple of great new cars, save for a dream vacation, pay off you home years ahead of schedule, super charge your savings account — or just make a fortune in real estate — Deals For Life gives you everything you need to successfully and profitably FIND, FUND, FIX and FLIP one property after another. </p>
                <p>This is far more than just a system to profit in real estate.  </p>
                <p>This is a time-tested, proven money-making system that can put you in total control of your time, your income and your life.</p>
                <p>This System will save you years of learning and help you avoid costly mistakes.</p>
                <p>With this System you have EVERYTHING you need find, fund, fix and flip properties right away — and begin building wealth for yourself and your family.</p>
                <p>The total value of the Deals For Life Program is staggering ...</p>
            
                <p>ART – We need images of the system here. </p>
                <p>Something like this … </p>
            
            
                <p>Here’s a breakdown of everything you receive.</p>
                <p>Deals For Life Realty Fund Connect Software & Proprietary Systems For Building Wealth Finding, Funding, Fixing & Flipping Real Estate…</p>
                <p>Here’s what you get… </p>
                <p>Value Cost</p>
                
                <p>1. Push-Button Funding Proposals that make you look like a real pro.  Just fill in the blanks, click the mouse and you have a complete, world-class proposal certain to impress any lender. $297 included</p>
                
                <p>2. Exclusive access to a network of qualified lenders active in the business of providing short-term financing to Find, Fix and Flip professionals.  These short-term lenders want the kinds of deals you will do with the Deals For Life System.  Having direct access to REAL and UNLIMITED funding sources is the KEY TO THE KINGDOM in the FIND, FUND, FIX AN FLIP BUSINESS.  And you get free list upgrades for life. $9,500 included</p>
                
                <p>3. Profits Now Fill In the Blanks & Click Deal Analyzer – Fill in the blanks, click the mouse and this powerful deal-analyzer software tells you exactly how much you can spend to acquire a property, fix it up AND how much you need to sell it for to pay all fees, commissions, cost of money, and still make the profit you want.  This makes analyzing a project push-button simple and gives you every important number you need to make a decision do the deal or pass. $497 included</p>
                
                <p>4. All future software upgrades for all software functions FOR LIFE $3,500 included</p>
                
                <p>5. Deals For Life Library – Having the right documents is an important key to your success.  The Deals For Life Library is loaded with Sample Real Estate Agreements, Contractor Agreements that protect you from the IRS and protect your from overages on a job, Release of Lien Agreements, Letters of Intent and a number of other important documents to help you do it right the very first time. $997 included</p>
                
                <p>6. Deals For Life Proprietary INSTANT CREDIBILITY SYSTEM – everything you need to be seen as a REAL PRO by everyone involved on your very first deal including; realtors, contractors, title companies, bankers, private money lenders, home inspectors, and more. $297 included</p>
                
                <p>7. Art Matuschat’s Exclusive DEAL ATTRACT SYSTEM To FIND PROPERTIES THAT MEET YOUR REQUIREMENTS - Keeps your phones ringing with calls from bankers, realtors and others, all offering you first-look at property that meets FIND, FIX & FLIP criteria; Includes video, audio, transcripts, examples you can copy and more.  Master the process of finding these properties the way Art teaches, and you can earn a GREAT living anywhere! $997 included</p>
                
                <p>8. Art Matuschat’s Proprietary COMPLETE SYSTEM TO FLIP PROPERTY FAST – These are the exact same secrets, strategies and techniques Art uses to have buyers lined up for properties BEFORE rehab work is even complete.  Includes video audio and transcripts.  Any single secret or technique revealed in this module could save you thousands — and help you make thousands more on every project you do. $997 included</p>
                
                <p>9. Art’s Proprietary DREAM TEAM SECRETS REVEALED – Art teaches you how to find the PERFECT team of professionals to make your life easy, including; the perfect realtor to work with; how to select the title company you rely on; finding the right contractors, appraiser, home inspection professional and more. Includes video, audio, transcripts and workbook. Follow Art’s advice on building your Dream Team — and you FIND, FIX & FLIP business be like a well-oiled machine — you can run it from your kitchen table if you like. $497 included</p>
                
                <p>10. Art’s Proprietary HYBRID MONEY STRATEGY – Art is one of the most successful find, fix and flip real estate pros in the U.S. because he gets deals funded others simply give up on. The Hybrid Money Strategy reveals the secrets, tactics and methods Art has developed over twenty years.  These strategies will help you close deals others can’t.  This Module alone can help you make tens of thousands of dollars in profit every year you would almost certainly never make otherwise.  Includes videos, audios, transcript, and more. $1997 included</p>
                
                <p>11. Art’s COMPLETE REHAB TO RICHES SYSTEM to get property ready to sell fast.  This module could cut YEARS off your learning curve! Art is a master of the Quick Rehab and the Fast Sale. You learn step-by-step how to determine exactly what a property needs and doesn’t need.  You learn Art’s secrets of how to save money and ‘gently’ rehab a house so it sells FAST to investors or first-time buyers looking for a deal. You get checklists, fill-in-the blank templets, video instructions and a great deal more more. $497 included</p>
                
                <p>12. BONUS - Quick Start Video With Art Matuschat — In this recorded presentation you can access immediately on line, Art explains the entire system from A to Z, answers all the most frequently asked questions, tells you what to do before your package arrives, and gives you quick-start tips on how to get your first deal working and a nice pay day coming your way fast. $97 Included</p>
                
                <p>13. BONUS – INSIDER SECRETS REVEALED — How to Earn $75,000 or More On One Single Find, Fix & Flip Real Estate Project – Besides being a top expert on finding, fixing and flipping homes and small commercial properties, Art has architected numerous larger commercial projects that netted from $75,000 to well over one million dollars in profit.  In this BONUS DVD Art discusses four of these projects in great detail, and explains how to find these kinds of projects for yourself, put them together and profit without spending a penny of your own money.  Art’s advice in this BONUS DVD could lead to your very first MILLION DOLLAR PAY DAY. 	  $997        included </p>
                
                <p>14. BONUS – Nine (9) Training & Group Coaching Calls with Art Matuschat – Each week for nine weeks you can call in and; 1) listen to Art’s training on some aspect of the FIND, FIX & FLIP business.  And then, after the training, Art joins the call LIVE to answer your questions.  Art is well-known for his ability to make a deal work when no one else can.  He charges real estate professionals $500 an hour, with a one hour minimum for phone consultation.  This BONUS gives you FREE ACCESS to Art for 9 weeks … you can ask any question and get advice on how to solve any problem with any deal.   This very real $4500 value is INCLUDED in your Deals For Life System.</p>
                
                <p>$4,500 included</p>
                <p>Total value of the Deals For Life Wealth Building Program, including all course modules, software, DVD, audios, transcripts, flow charts, step-by-step guides, coaching calls and other bonuses is $25,667.00.	$25,667.        </p>
                
                <p>The real value of the Deals For Life Program is $25,667.  But Art generously offers the program for only $4997 — a full 80% discount… </p>
                
                <p>However…</p>
                
                <p>But By Special Arrangement with your Host — Today You get the COMPLETE DEALS FOR LIFE WEALTH BUILDING SYSTEM plus all bonuses, $25,667 in valuable resources – for just $2997 – 88% off the real value  and a full 40% discount off the regular price.</p>
                
                <p>Plus you get my personal </p>
                <p>100% Satisfaction Guarantee</p>
                
                <p>And You Get My Special Deals For Life </p>
                <p>$25,000 Profit Guarantee… </p>
                
                <p>I personally guarantee my system works when you do.</p>
                
                <p>Watch the videos, follow the system, attend the 9 bonus calls, use the software, do the work … and close two (2) deals.  </p>
                
                <p>If after your second (2rd) deal you have not made AT LEAST $25,000 in profit — I will personally coach you by phone to FIND, FUND, FIX and FLIP a deal that puts you well over the $25,000 mark.  </p>
                
                <p>To qualify you must provide documentation that proves you have closed 2 deals. </p>
                
                <p>Complete the Information below… </p>
                <p>And you will receive a Welcome Email with all the details on when your package will be shipped and how to access the online portion of the program so you can get started right away! </p>
                
                <p>ART …. The shopping cart fill in the blank forms, logos and other legal stuff I will leave to the Shopping Cart guys goes here below. </p>
            </div>
        </div>
    </div>
</section>

<footer>
	<div class="container">
        <div class="row">
            <div class="col-xs-6">
                <p class="text-left">
                    <a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                    <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                    <a href="<?=base_url('pages/features')?>/">Features</a>
                </p>
            </div>
            <div class="col-xs-6">
                <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
            </div>
        </div>
	</div>
</footer>