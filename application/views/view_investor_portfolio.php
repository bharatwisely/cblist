<section id="wrapper">
    <div class="container-fluid">
        <div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>Investor's Portfolio</h2>
                    </div>
                </div>
                
                <div class="page-content">
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-file-text-o"></i> <?php echo $single->property_name;?></h3>
                                </div>
                                <div class="box-body">
                                	<div class="fields-panel">
	                                	<div class="row">
    	                                	<div class="col-xs-8">
                                        		<div class="panel-field">
                                                    <label>Property Name: </label>
                                                    <?php echo $single->property_name;?>
                                                </div>
                                                <div class="panel-field">
                                                    <label>Property Information: </label>
                                                    <?php echo $single->info_one;?>
                                                </div>
                                                <?php if ( $single->info_two ) { ?>
                                                    <div class="panel-field">
                                                        <label>Additional Property Information: </label>
                                                        <?php echo $single->info_two;?>
                                                    </div>
                                                <?php } ?>
                                                <div class="panel-field">
                                                    <label>Property Video: </label>
                                                    <div id="video"></div>
                                                    <script type="text/javascript">
                                                    jwplayer("video").setup({
                                                        file: "<?php echo $single->youtube_link;?>",
                                                        width: "100%",
                                                        height: 400,
                                                    });
                                                    </script>
                                                </div>
                                            </div>
                                            <div class="col-xs-4">
												<?php if ( $single->images ) { ?>
                                                    <div class="panel-field">
                                                        <div class="portfolio-pic">
                                                            <?php $images = explode('|', $single->images); $i = 0; ?>
                                                            <?php foreach ( $images as $img ) { ?>
                                                                <div class="col-xs-6 image-upload">
                                                                	<div class="row">
                                                                    	<a href="<?=$img?>" data-fancybox-group="portfolio" class="fancybox">
		                                                                    <img src="<?=$img?>" alt="pic" class="portfolio-img thumbnail" />
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            <?php $i++;
																if ( $i%2 == 0 ) echo '<div class="clearfix"></div>';
                                                            } ?>
                                                        </div>
                                                    </div>
                                                <?php } ?>
                                                <div class="panel-field">
                                                    <label>Property Location: </label>
                                                    <?=form_hidden('property_add', $single->location)?>
                                                    <div id="map_canvas" style="top:0;margin:0;"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>