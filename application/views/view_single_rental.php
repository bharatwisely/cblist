<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <h2>Public Proposal</h2>
                    </div>
                </div>
                <div class="page-content">
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                            	<div class="box-header">
                                    <h3 class="box-title col-xs-8"><?=$single_proposal->PropertyName?></h3>
                                    <h3 class="box-title col-xs-4">Exclusively Presented By:</h3>
                                </div>
                                <div class="box-body">
                                	<div class="row">
                                        <div class="col-xs-8">
                                            <p><i class="fa fa-map-marker"></i> <?=$single_proposal->PropertyStreetAddress?>, <?=$single_proposal->PropertyCityTown?>, <?=$single_proposal->PropertyStateProvince?>, <?=$single_proposal->PropertyZipCode?>, <?=$single_proposal->PropertyCountry?></p>
                                        </div>
                                        <div class="col-xs-4">
                                            <?php if ( $single_proposal->YourName ) { ?>
                                                <p><i class="fa fa-user"></i> <?=$single_proposal->YourName?></p>
                                            <?php } else { ?>
                                                <p><i class="fa fa-user"></i> <?=$single_proposal->fname?></p>
                                            <?php } ?>
                                            <?php if ( $single_proposal->Website ) { ?>
                                                <p>
                                                	<i class="fa fa-link"></i> 
                                                    <a target="_blank" href="<?=prep_url(strtolower($single_proposal->Website))?>"><?=$single_proposal->Website?></a>
                                                </p>
                                            <?php } ?>
                                        </div>
                                        <div class="col-xs-12">
                                            <?php if ( $this->pages_model->get_cashlender_proposal_view($single_proposal->UserID, $user->id, $single_proposal->PropertyID) != 'viewed' || $user->id != $single_proposal->UserID ) { ?>
                                                <p class="text-right"><a class="btn btn-block btn-primary" href="#" data-toggle="modal" data-target="#confirm-view">Read More</a></p>
                                                <!-- Investor proposal view confirmation Popup for cashlender -->
                                                <div class="modal fade" id="confirm-view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <form method="post" action="">
                                                                <div class="modal-header">
                                                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                                    <h4 class="modal-title" id="myModalLabel">Privacy Confirmation</h4>
                                                                </div>
                                                                <div class="modal-body">This information you have requested to view has been set us private by the investor. To proceed we will share your email id with the investor. Do you wish to continue?</div>
                                                                <div class="modal-footer">
                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                                                                    <button type="submit" class="btn btn-primary" name="accept_confirm">Yes</button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>