<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <h2>Public Profile</h2>
                    </div>
                </div>
                
                <div class="page-content">
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-body">
                                	<div class="row">
                                        <div class="col-xs-3 profile-pic">
                                            <img class="thumbnail" src="<?=isset($public_user->picture_medium) ? $public_user->picture_medium : base_url('assets/images/default-pic.jpg')?>" alt="picture" />
                                        </div>
                                        <div class="col-xs-9">
                                            <div class="fields-panel">
                                                <h1 class="profile-name"><?=$public_user->fname?> <span class="phone"><i class="fa fa-phone-square"></i> <?=$public_user->phone?></span></h1>
                                                <div class="row">
                                                    <div class="col-xs-6">
                                                        <div class="panel-field-public">
                                                            <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;<?=$public_user->email?><br />
                                                            <i class="fa fa-location-arrow"></i>&nbsp;&nbsp;<?=$public_user->street?>, <?=$public_user->town?>, <?=$public_user->state?>, <?=$public_user->zip?>, <?=$public_user->country?>
                                                        </div>
                                                        <div class="panel-field-public">
                                                            <label><u>About <?=$public_user->fname?>:</u> </label>
                                                            <?php if ( !empty($public_user->about_you) ) { ?>
                                                                <p><?=$public_user->about_you?></p>
                                                            <?php } else { ?>
                                                                <p><?=$public_user->fname?> is not update his description.</p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-6">
                                                        <div id="video"></div>
                                                        <script type="text/javascript">
                                                        jwplayer("video").setup({
                                                            file: "<?=$public_user->youtube_link?>",
                                                            width: "100%",
                                                            height: 275,
                                                        });
                                                        </script>
                                                    </div>
                                                </div>
                                                <div class="panel-field-public">
                                                    <label><u>Sales pitch for cash buyers:</u> </label>
                                                    <?php if ( $public_user->sales_pitch ) { ?>
                                                        <p><?=$public_user->sales_pitch?></p>
                                                    <?php } else { ?>
                                                        <p>Sales picth for cash buyers is not available.</p>
                                                    <?php } ?>
                                                </div>
                                                <div class="panel-field-public">
                                                    <label><u>About investments:</u> </label>
                                                    <?php if ( $public_user->about_investment ) { ?>
                                                        <p><?=$public_user->about_investment?></p>
                                                    <?php } else { ?>
                                                        <p>About investments is not available.</p>
                                                    <?php } ?>
                                                </div>
                                                <?php if ( $public_portfolio ) { ?>
                                                    <div class="panel-field-public">
                                                        <label><u>Portfolios:</u>
                                                            <?php if ( count($public_portfolio) > 4 ) { ?>
                                                                <a class="view-all" href="#">View All</a>
                                                            <?php } ?>
                                                        </label>
                                                        <div class="row">
                                                            <?php $n = 0; foreach ( $public_portfolio as $pub_port ) {
                                                                if ( $n < 4 ) { ?>
                                                                    <?=form_hidden('property_add', $pub_port->location)?>
                                                                    <div class="col-xs-6 public-portfolio">
                                                                        <div class="row">
                                                                            <?php if ( $pub_port->images ) { ?>
                                                                                <div class="col-xs-4">
                                                                                    <div class="port-img">
                                                                                        <?php $port_img = explode('|', $pub_port->images); $i = 0;
                                                                                        foreach ( $port_img as $pimg ) {
                                                                                            if ( $i < 4 ) { ?>
                                                                                                <a class="fancybox" data-fancybox-group="public-portfolio-<?=$pub_port->id?>" href="<?=$pimg?>">
                                                                                                    <img src="<?=$pimg?>" alt="thumb" />
                                                                                                </a>
                                                                                            <?php 
                                                                                            } $i++;
                                                                                        } ?>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-xs-8">
                                                                                    <div class="row">
                                                                                        <p><strong><?=$pub_port->property_name?></strong></p>
                                                                                        <p><?=mb_substr($pub_port->info_one, 0, 110)?>...</p>
                                                                                        <p><a href="<?=base_url('pages/investor-portfolio/' . $pub_port->id . '/portfolio')?>/">Read More</a></p>
                                                                                    </div>
                                                                                </div>
                                                                            <?php } else { ?>
                                                                                <div class="col-xs-12">
                                                                                    <p><strong><?=$pub_port->property_name?></strong></p>
                                                                                    <p><?=mb_substr($pub_port->info_one, 0, 110)?>...</p>
                                                                                    <p><a href="<?=base_url('pages/investor-portfolio/' . $pub_port->id . '/portfolio')?>/">Read More</a></p>
                                                                                </div>
                                                                            <?php } ?>
                                                                        </div>
                                                                    </div>
                                                                <?php 
                                                                } $n++;
                                                            } ?>
                                                        </div>
                                                    </div>
                                                    <div class="panel-field-public">
                                                        <div id="map_canvas"></div>
                                                    </div>
                                                <?php } else { ?>
                                                    <label>Portfolios: </label>
                                                    <p><?=$public_user->fname?> is not create any portfolio yet.</p>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>