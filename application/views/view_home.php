<header id="header">
	<div class="container">
    	<div class="row">
			<nav class="navbar">
            	<div class="container-fluid">
                	<div class="navbar-header">
                    	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        	<span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1 class="home-logo">
                        	<a class="navbar-brand" href="<?=base_url()?>">
                        		<img src="<?=base_url('assets/images/logo.png')?>" alt="CBList" width="200" />
                        	</a>
                        </h1>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                        	<li <?php if($this->uri->segment(2) == ''):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url()?>">Home</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'aboutus'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/aboutus')?>/">About Us</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'blog'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/blog')?>/">Blog</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'our-offer'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'features'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </li>
                            <?php if ( $user_id ) { ?>
                            	<li>
                                    <a href="<?=base_url('pages/profile')?>/">Profile</a>
                                </li>
                                <li>
                                    <a href="<?=base_url('pages/logout')?>/">Logout</a>
                                </li>
                            <?php } else { ?>
                                <li <?php if($this->uri->segment(2) == 'investors-login'):?> class="active" <?php else:?> <?php endif;?>>
                                    <a href="<?=base_url('pages/investors-login')?>/">Investor Login</a>
                                </li>
                                <li <?php if($this->uri->segment(2) == 'cashlenders-login'):?> class="active" <?php else:?> <?php endif;?>>
                                    <a href="<?=base_url('pages/cashlenders-login')?>/">Lender Login</a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>

<section id="body">
    <div class="container">
        <div class="row">
            <div class="col-xs-6">
                <div id="video">
                	<img src="<?=base_url('assets/video/poster.jpg')?>" alt="video" width="100%" height="330" />
                </div>
            </div>
            <div class="col-xs-6">
                <div class="panel panel-default oneline">
                    <p></p>
                </div>
                <div class="panel panel-default oneline">
                    <p></p><br>
                    <?php if ( ! $user_id ) { ?><br />
	                    <a href="<?=base_url('pages/investors-signup')?>/" class="btn btn-default">Investors Join Now</a>
						<a href="<?=base_url('pages/cashlenders-signup')?>/" class="btn btn-default">Lenders Join Now</a>
                    <?php } else { ?>
                    	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
					<?php } ?>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xs-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Art of Raising Money!</div>
                    <div class="panel-body">
	                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="panel panel-default">
                    <div class="panel-heading">What cash investors are saying about!</div>
                    <div class="panel-body">
	                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
            <div class="col-xs-4">
                <div class="panel panel-default">
                    <div class="panel-heading">Join Our Free Webinar Series</div>
                    <div class="panel-body">
	                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<footer id="footer">
	<div class="container">
        <div class="row">
            <div class="col-xs-6">
                <p class="text-left">
                    <a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                    <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                    <a href="<?=base_url('pages/features')?>/">Features</a>
                </p>
            </div>
            <div class="col-xs-6">
                <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
            </div>
        </div>
	</div>
</footer>