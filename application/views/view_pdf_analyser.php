<?php 
// B23
$P1CashFlow = number_format(($print_data['Analyser']->P1NOI + $print_data['Analyser']->P1MortPymt), 0);
$P2CashFlow = number_format(($print_data['Analyser']->P2NOI + $print_data['Analyser']->P2MortPymt), 0);
$P3CashFlow = number_format(($print_data['Analyser']->P3NOI + $print_data['Analyser']->P3MortPymt), 0);
$P4CashFlow = number_format(($print_data['Analyser']->P4NOI + $print_data['Analyser']->P4MortPymt), 0);
$P5CashFlow = number_format(($print_data['Analyser']->P5NOI + $print_data['Analyser']->P5MortPymt), 0);

// B25
$P1CapRate = ($print_data['Analyser']->P1Price > 0 ? number_format(($print_data['Analyser']->P1NOI * 12) / ($print_data['Analyser']->P1Price + $print_data['Analyser']->P1CapImp)*100, 2) : 'N/A');
$P2CapRate = ($print_data['Analyser']->P2Price > 0 ? number_format(($print_data['Analyser']->P2NOI * 12) / ($print_data['Analyser']->P2Price + $print_data['Analyser']->P2CapImp)*100, 2) : 'N/A');
$P3CapRate = ($print_data['Analyser']->P3Price > 0 ? number_format(($print_data['Analyser']->P3NOI * 12) / ($print_data['Analyser']->P3Price + $print_data['Analyser']->P3CapImp)*100, 2) : 'N/A');
$P4CapRate = ($print_data['Analyser']->P4Price > 0 ? number_format(($print_data['Analyser']->P4NOI * 12) / ($print_data['Analyser']->P4Price + $print_data['Analyser']->P4CapImp)*100, 2) : 'N/A');
$P5CapRate = ($print_data['Analyser']->P5Price > 0 ? number_format(($print_data['Analyser']->P5NOI * 12) / ($print_data['Analyser']->P5Price + $print_data['Analyser']->P5CapImp)*100, 2) : 'N/A');

$P1CapRateRank = ($P1CapRate=='N/A' ? 'N/A' : RANK($P1CapRate, array($P1CapRate, $P2CapRate, $P3CapRate, $P4CapRate, $P5CapRate), 0));
$P2CapRateRank = ($P2CapRate=='N/A' ? 'N/A' : RANK($P2CapRate, array($P1CapRate, $P2CapRate, $P3CapRate, $P4CapRate, $P5CapRate), 0));
$P3CapRateRank = ($P3CapRate=='N/A' ? 'N/A' : RANK($P3CapRate, array($P1CapRate, $P2CapRate, $P3CapRate, $P4CapRate, $P5CapRate), 0));
$P4CapRateRank = ($P4CapRate=='N/A' ? 'N/A' : RANK($P4CapRate, array($P1CapRate, $P2CapRate, $P3CapRate, $P4CapRate, $P5CapRate), 0));
$P5CapRateRank = ($P5CapRate=='N/A' ? 'N/A' : RANK($P5CapRate, array($P1CapRate, $P2CapRate, $P3CapRate, $P4CapRate, $P5CapRate), 0));

// B26
$P1PreTaxCashCash = ($print_data['Analyser']->P1Price > 0 ? number_format(($P1CashFlow * 12 / $print_data['Analyser']->P1CapOut)*100, 2) : "N/A");
$P2PreTaxCashCash = ($print_data['Analyser']->P2Price > 0 ? number_format(($P2CashFlow * 12 / $print_data['Analyser']->P2CapOut)*100, 2) : "N/A");
$P3PreTaxCashCash = ($print_data['Analyser']->P3Price > 0 ? number_format(($P3CashFlow * 12 / $print_data['Analyser']->P3CapOut)*100, 2) : "N/A");
$P4PreTaxCashCash = ($print_data['Analyser']->P4Price > 0 ? number_format(($P4CashFlow * 12 / $print_data['Analyser']->P4CapOut)*100, 2) : "N/A");
$P5PreTaxCashCash = ($print_data['Analyser']->P5Price > 0 ? number_format(($P5CashFlow * 12 / $print_data['Analyser']->P5CapOut)*100, 2) : "N/A");

$P1PreTaxCashCashRank = ($P1PreTaxCashCash=='N/A' ? 'N/A' : RANK($P1PreTaxCashCash, array($P1PreTaxCashCash, $P2PreTaxCashCash, $P3PreTaxCashCash, $P4PreTaxCashCash, $P5PreTaxCashCash), 0));
$P2PreTaxCashCashRank = ($P2PreTaxCashCash=='N/A' ? 'N/A' : RANK($P2PreTaxCashCash, array($P1PreTaxCashCash, $P2PreTaxCashCash, $P3PreTaxCashCash, $P4PreTaxCashCash, $P5PreTaxCashCash), 0));
$P3PreTaxCashCashRank = ($P3PreTaxCashCash=='N/A' ? 'N/A' : RANK($P3PreTaxCashCash, array($P1PreTaxCashCash, $P2PreTaxCashCash, $P3PreTaxCashCash, $P4PreTaxCashCash, $P5PreTaxCashCash), 0));
$P4PreTaxCashCashRank = ($P4PreTaxCashCash=='N/A' ? 'N/A' : RANK($P4PreTaxCashCash, array($P1PreTaxCashCash, $P2PreTaxCashCash, $P3PreTaxCashCash, $P4PreTaxCashCash, $P5PreTaxCashCash), 0));
$P5PreTaxCashCashRank = ($P5PreTaxCashCash=='N/A' ? 'N/A' : RANK($P5PreTaxCashCash, array($P1PreTaxCashCash, $P2PreTaxCashCash, $P3PreTaxCashCash, $P4PreTaxCashCash, $P5PreTaxCashCash), 0));

// B27
$P1GrossRentMultiplierGRM = ($print_data['Analyser']->P1Price > 0 ? number_format(($print_data['Analyser']->P1Price + $print_data['Analyser']->P1CapImp) / ($print_data['Analyser']->P1Rent * 12), 2) : "N/A");
$P2GrossRentMultiplierGRM = ($print_data['Analyser']->P2Price > 0 ? number_format(($print_data['Analyser']->P2Price + $print_data['Analyser']->P2CapImp) / ($print_data['Analyser']->P2Rent * 12), 2) : "N/A");
$P3GrossRentMultiplierGRM = ($print_data['Analyser']->P3Price > 0 ? number_format(($print_data['Analyser']->P3Price + $print_data['Analyser']->P3CapImp) / ($print_data['Analyser']->P3Rent * 12), 2) : "N/A");
$P4GrossRentMultiplierGRM = ($print_data['Analyser']->P4Price > 0 ? number_format(($print_data['Analyser']->P4Price + $print_data['Analyser']->P4CapImp) / ($print_data['Analyser']->P4Rent * 12), 2) : "N/A");
$P5GrossRentMultiplierGRM = ($print_data['Analyser']->P5Price > 0 ? number_format(($print_data['Analyser']->P5Price + $print_data['Analyser']->P5CapImp) / ($print_data['Analyser']->P5Rent * 12), 2) : "N/A");

$P1GrossRentMultiplierGRMRank = ($P1GrossRentMultiplierGRM=='N/A' ? 'N/A' : RANK($P1GrossRentMultiplierGRM, array($P1GrossRentMultiplierGRM, $P2GrossRentMultiplierGRM, $P3GrossRentMultiplierGRM, $P4GrossRentMultiplierGRM, $P5GrossRentMultiplierGRM), 1));
$P2GrossRentMultiplierGRMRank = ($P2GrossRentMultiplierGRM=='N/A' ? 'N/A' : RANK($P2GrossRentMultiplierGRM, array($P1GrossRentMultiplierGRM, $P2GrossRentMultiplierGRM, $P3GrossRentMultiplierGRM, $P4GrossRentMultiplierGRM, $P5GrossRentMultiplierGRM), 1));
$P3GrossRentMultiplierGRMRank = ($P3GrossRentMultiplierGRM=='N/A' ? 'N/A' : RANK($P3GrossRentMultiplierGRM, array($P1GrossRentMultiplierGRM, $P2GrossRentMultiplierGRM, $P3GrossRentMultiplierGRM, $P4GrossRentMultiplierGRM, $P5GrossRentMultiplierGRM), 1));
$P4GrossRentMultiplierGRMRank = ($P4GrossRentMultiplierGRM=='N/A' ? 'N/A' : RANK($P4GrossRentMultiplierGRM, array($P1GrossRentMultiplierGRM, $P2GrossRentMultiplierGRM, $P3GrossRentMultiplierGRM, $P4GrossRentMultiplierGRM, $P5GrossRentMultiplierGRM), 1));
$P5GrossRentMultiplierGRMRank = ($P5GrossRentMultiplierGRM=='N/A' ? 'N/A' : RANK($P5GrossRentMultiplierGRM, array($P1GrossRentMultiplierGRM, $P2GrossRentMultiplierGRM, $P3GrossRentMultiplierGRM, $P4GrossRentMultiplierGRM, $P5GrossRentMultiplierGRM), 1));

// B28
$P1SqFoot = ($print_data['Analyser']->P1Price > 0 ? number_format(($print_data['Analyser']->P1Price + $print_data['Analyser']->P1CapImp) / $print_data['Analyser']->P1SqFeet, 2) : "N/A");
$P2SqFoot = ($print_data['Analyser']->P2Price > 0 ? number_format(($print_data['Analyser']->P2Price + $print_data['Analyser']->P2CapImp) / $print_data['Analyser']->P2SqFeet, 2) : "N/A");
$P3SqFoot = ($print_data['Analyser']->P3Price > 0 ? number_format(($print_data['Analyser']->P3Price + $print_data['Analyser']->P3CapImp) / $print_data['Analyser']->P3SqFeet, 2) : "N/A");
$P4SqFoot = ($print_data['Analyser']->P4Price > 0 ? number_format(($print_data['Analyser']->P4Price + $print_data['Analyser']->P4CapImp) / $print_data['Analyser']->P4SqFeet, 2) : "N/A");
$P5SqFoot = ($print_data['Analyser']->P5Price > 0 ? number_format(($print_data['Analyser']->P5Price + $print_data['Analyser']->P5CapImp) / $print_data['Analyser']->P5SqFeet, 2) : "N/A");

$P1SqFootRank = ($P1SqFoot=='N/A' ? 'N/A' : RANK($P1SqFoot, array($P1SqFoot, $P2SqFoot, $P3SqFoot, $P4SqFoot, $P5SqFoot), 1));
$P2SqFootRank = ($P2SqFoot=='N/A' ? 'N/A' : RANK($P2SqFoot, array($P1SqFoot, $P2SqFoot, $P3SqFoot, $P4SqFoot, $P5SqFoot), 1));
$P3SqFootRank = ($P3SqFoot=='N/A' ? 'N/A' : RANK($P3SqFoot, array($P1SqFoot, $P2SqFoot, $P3SqFoot, $P4SqFoot, $P5SqFoot), 1));
$P4SqFootRank = ($P4SqFoot=='N/A' ? 'N/A' : RANK($P4SqFoot, array($P1SqFoot, $P2SqFoot, $P3SqFoot, $P4SqFoot, $P5SqFoot), 1));
$P5SqFootRank = ($P5SqFoot=='N/A' ? 'N/A' : RANK($P5SqFoot, array($P1SqFoot, $P2SqFoot, $P3SqFoot, $P4SqFoot, $P5SqFoot), 1));

// B29
$P1PaybackPeriod = ($print_data['Analyser']->P1Price > 0 ? ($print_data['Analyser']->P1CapOut/($P1CashFlow*12) > 0 ? number_format($print_data['Analyser']->P1CapOut/($P1CashFlow*12), 2) : "Negative") : "N/A");
$P2PaybackPeriod = ($print_data['Analyser']->P2Price > 0 ? ($print_data['Analyser']->P2CapOut/($P2CashFlow*12) > 0 ? number_format($print_data['Analyser']->P2CapOut/($P2CashFlow*12), 2) : "Negative") : "N/A");
$P3PaybackPeriod = ($print_data['Analyser']->P3Price > 0 ? ($print_data['Analyser']->P3CapOut/($P3CashFlow*12) > 0 ? number_format($print_data['Analyser']->P3CapOut/($P3CashFlow*12), 2) : "Negative") : "N/A");
$P4PaybackPeriod = ($print_data['Analyser']->P4Price > 0 ? ($print_data['Analyser']->P4CapOut/($P4CashFlow*12) > 0 ? number_format($print_data['Analyser']->P4CapOut/($P4CashFlow*12), 2) : "Negative") : "N/A");
$P5PaybackPeriod = ($print_data['Analyser']->P5Price > 0 ? ($print_data['Analyser']->P5CapOut/($P5CashFlow*12) > 0 ? number_format($print_data['Analyser']->P5CapOut/($P5CashFlow*12), 2) : "Negative") : "N/A");

$P1PaybackPeriodRank = ($P1PaybackPeriod=='Negative' ? 'N/A' : ($P1PaybackPeriod=='N/A' ? 'N/A' : RANK($P1PaybackPeriod, array($P1PaybackPeriod, $P2PaybackPeriod, $P3PaybackPeriod, $P4PaybackPeriod, $P5PaybackPeriod), 1)));
$P2PaybackPeriodRank = ($P2PaybackPeriod=='Negative' ? 'N/A' : ($P2PaybackPeriod=='N/A' ? 'N/A' : RANK($P2PaybackPeriod, array($P1PaybackPeriod, $P2PaybackPeriod, $P3PaybackPeriod, $P4PaybackPeriod, $P5PaybackPeriod), 1)));
$P3PaybackPeriodRank = ($P3PaybackPeriod=='Negative' ? 'N/A' : ($P3PaybackPeriod=='N/A' ? 'N/A' : RANK($P3PaybackPeriod, array($P1PaybackPeriod, $P2PaybackPeriod, $P3PaybackPeriod, $P4PaybackPeriod, $P5PaybackPeriod), 1)));
$P4PaybackPeriodRank = ($P4PaybackPeriod=='Negative' ? 'N/A' : ($P4PaybackPeriod=='N/A' ? 'N/A' : RANK($P4PaybackPeriod, array($P1PaybackPeriod, $P2PaybackPeriod, $P3PaybackPeriod, $P4PaybackPeriod, $P5PaybackPeriod), 1)));
$P5PaybackPeriodRank = ($P5PaybackPeriod=='Negative' ? 'N/A' : ($P5PaybackPeriod=='N/A' ? 'N/A' : RANK($P5PaybackPeriod, array($P1PaybackPeriod, $P2PaybackPeriod, $P3PaybackPeriod, $P4PaybackPeriod, $P5PaybackPeriod), 1)));

// B30
$P1DebtCoverageRatioDCR = ($print_data['Analyser']->P1MortPymt < 0 ? ($print_data['Analyser']->P1Price > 0 ? number_format(($print_data['Analyser']->P1NOI*12)/(-($print_data['Analyser']->P1MortPymt)*12), 2) : "N/A") : "N/A");
$P2DebtCoverageRatioDCR = ($print_data['Analyser']->P2MortPymt < 0 ? ($print_data['Analyser']->P2Price > 0 ? number_format(($print_data['Analyser']->P2NOI*12)/(-($print_data['Analyser']->P2MortPymt)*12), 2) : "N/A") : "N/A");
$P3DebtCoverageRatioDCR = ($print_data['Analyser']->P3MortPymt < 0 ? ($print_data['Analyser']->P3Price > 0 ? number_format(($print_data['Analyser']->P3NOI*12)/(-($print_data['Analyser']->P3MortPymt)*12), 2) : "N/A") : "N/A");
$P4DebtCoverageRatioDCR = ($print_data['Analyser']->P4MortPymt < 0 ? ($print_data['Analyser']->P4Price > 0 ? number_format(($print_data['Analyser']->P4NOI*12)/(-($print_data['Analyser']->P4MortPymt)*12), 2) : "N/A") : "N/A");
$P5DebtCoverageRatioDCR = ($print_data['Analyser']->P5MortPymt < 0 ? ($print_data['Analyser']->P5Price > 0 ? number_format(($print_data['Analyser']->P5NOI*12)/(-($print_data['Analyser']->P5MortPymt)*12), 2) : "N/A") : "N/A");

$P1DebtCoverageRatioDCRRank = ($P1DebtCoverageRatioDCR=='N/A' ? 'N/A' : RANK($P1DebtCoverageRatioDCR, array($P1DebtCoverageRatioDCR, $P2DebtCoverageRatioDCR, $P3DebtCoverageRatioDCR, $P4DebtCoverageRatioDCR, $P5DebtCoverageRatioDCR), 0));
$P2DebtCoverageRatioDCRRank = ($P2DebtCoverageRatioDCR=='N/A' ? 'N/A' : RANK($P2DebtCoverageRatioDCR, array($P1DebtCoverageRatioDCR, $P2DebtCoverageRatioDCR, $P3DebtCoverageRatioDCR, $P4DebtCoverageRatioDCR, $P5DebtCoverageRatioDCR), 0));
$P3DebtCoverageRatioDCRRank = ($P3DebtCoverageRatioDCR=='N/A' ? 'N/A' : RANK($P3DebtCoverageRatioDCR, array($P1DebtCoverageRatioDCR, $P2DebtCoverageRatioDCR, $P3DebtCoverageRatioDCR, $P4DebtCoverageRatioDCR, $P5DebtCoverageRatioDCR), 0));
$P4DebtCoverageRatioDCRRank = ($P4DebtCoverageRatioDCR=='N/A' ? 'N/A' : RANK($P4DebtCoverageRatioDCR, array($P1DebtCoverageRatioDCR, $P2DebtCoverageRatioDCR, $P3DebtCoverageRatioDCR, $P4DebtCoverageRatioDCR, $P5DebtCoverageRatioDCR), 0));
$P5DebtCoverageRatioDCRRank = ($P5DebtCoverageRatioDCR=='N/A' ? 'N/A' : RANK($P5DebtCoverageRatioDCR, array($P1DebtCoverageRatioDCR, $P2DebtCoverageRatioDCR, $P3DebtCoverageRatioDCR, $P4DebtCoverageRatioDCR, $P5DebtCoverageRatioDCR), 0));

// B31
$P1BreakEvenRatio = ($print_data['Analyser']->P1Price > 0 ? number_format(-($print_data['Analyser']->P1MortPymt*12-($print_data['Analyser']->P1Taxes+$print_data['Analyser']->P1Ins+$print_data['Analyser']->P1Maint+$print_data['Analyser']->P1OpEx))/(($print_data['Analyser']->P1Rent*12*(1-(($print_data['Analyser']->P1Vacancy/100)+($print_data['Analyser']->P1Management/100)))))*100, 0) : "N/A");
$P2BreakEvenRatio = ($print_data['Analyser']->P2Price > 0 ? number_format(-($print_data['Analyser']->P2MortPymt*12-($print_data['Analyser']->P2Taxes+$print_data['Analyser']->P2Ins+$print_data['Analyser']->P2Maint+$print_data['Analyser']->P2OpEx))/(($print_data['Analyser']->P2Rent*12*(1-(($print_data['Analyser']->P2Vacancy/100)+($print_data['Analyser']->P2Management/100)))))*100, 0) : "N/A");
$P3BreakEvenRatio = ($print_data['Analyser']->P3Price > 0 ? number_format(-($print_data['Analyser']->P3MortPymt*12-($print_data['Analyser']->P3Taxes+$print_data['Analyser']->P3Ins+$print_data['Analyser']->P3Maint+$print_data['Analyser']->P3OpEx))/(($print_data['Analyser']->P3Rent*12*(1-(($print_data['Analyser']->P3Vacancy/100)+($print_data['Analyser']->P3Management/100)))))*100, 0) : "N/A");
$P4BreakEvenRatio = ($print_data['Analyser']->P4Price > 0 ? number_format(-($print_data['Analyser']->P4MortPymt*12-($print_data['Analyser']->P4Taxes+$print_data['Analyser']->P4Ins+$print_data['Analyser']->P4Maint+$print_data['Analyser']->P4OpEx))/(($print_data['Analyser']->P4Rent*12*(1-(($print_data['Analyser']->P4Vacancy/100)+($print_data['Analyser']->P4Management/100)))))*100, 0) : "N/A");
$P5BreakEvenRatio = ($print_data['Analyser']->P5Price > 0 ? number_format(-($print_data['Analyser']->P5MortPymt*12-($print_data['Analyser']->P5Taxes+$print_data['Analyser']->P5Ins+$print_data['Analyser']->P5Maint+$print_data['Analyser']->P5OpEx))/(($print_data['Analyser']->P5Rent*12*(1-(($print_data['Analyser']->P5Vacancy/100)+($print_data['Analyser']->P5Management/100)))))*100, 0) : "N/A");

$P1BreakEvenRatioRank = ($P1BreakEvenRatio=='N/A' ? 'N/A' : RANK($P1BreakEvenRatio, array($P1BreakEvenRatio, $P2BreakEvenRatio, $P3BreakEvenRatio, $P4BreakEvenRatio, $P5BreakEvenRatio), 1));
$P2BreakEvenRatioRank = ($P2BreakEvenRatio=='N/A' ? 'N/A' : RANK($P2BreakEvenRatio, array($P1BreakEvenRatio, $P2BreakEvenRatio, $P3BreakEvenRatio, $P4BreakEvenRatio, $P5BreakEvenRatio), 1));
$P3BreakEvenRatioRank = ($P3BreakEvenRatio=='N/A' ? 'N/A' : RANK($P3BreakEvenRatio, array($P1BreakEvenRatio, $P2BreakEvenRatio, $P3BreakEvenRatio, $P4BreakEvenRatio, $P5BreakEvenRatio), 1));
$P4BreakEvenRatioRank = ($P4BreakEvenRatio=='N/A' ? 'N/A' : RANK($P4BreakEvenRatio, array($P1BreakEvenRatio, $P2BreakEvenRatio, $P3BreakEvenRatio, $P4BreakEvenRatio, $P5BreakEvenRatio), 1));
$P5BreakEvenRatioRank = ($P5BreakEvenRatio=='N/A' ? 'N/A' : RANK($P5BreakEvenRatio, array($P1BreakEvenRatio, $P2BreakEvenRatio, $P3BreakEvenRatio, $P4BreakEvenRatio, $P5BreakEvenRatio), 1));
?>
<div class="Analyser">
    <h3 class="text-center"><strong>RENTAL PROPERTY COMPARISON ANALYSER REPORT</strong></h3><hr />
    <div class="row">
    	<div class="col-lg-6 col-lg-offset-3">
        	<p class="text-center"><strong>FINANCING ASSUMPTIONS</strong></p>
            <p class="text-center">***These apply to all properties</p>
            <table class="table table-bordered text-center">
                <tbody>
                	<tr>
                    	<th class="text-left">Down Payment (%)</th>
                        <td colspan="2"><?=round($print_data['Analyser']->DownPayment)?>%</td>
                    </tr>
                    <tr>
                    	<th>&nbsp;</th>
                        <td>1st Mortgage</td>
                        <td>2nd Mortgage</td>
                    </tr>
                    <tr>
                    	<th class="text-left">Used?</th>
                        <td><?=$print_data['Analyser']->Used_1?></td>
                        <td><?=$print_data['Analyser']->Used_2?></td>
                    </tr>
                    <tr>
                    	<th class="text-left">Loan Type</th>
                        <td><?=$print_data['Analyser']->LoanType_1?></td>
                        <td><?=$print_data['Analyser']->LoanType_2?></td>
                    </tr>
                    <tr>
                    	<th class="text-left">Term (In Years)</th>
                        <td><?=$print_data['Analyser']->TermYears_1?></td>
                        <td><?=$print_data['Analyser']->TermYears_2?></td>
                    </tr>
                    <tr>
                    	<th class="text-left">LTV (%)</th>
                        <td><?=round($print_data['Analyser']->LTV_1)?>%</td>
                        <td><?=round($print_data['Analyser']->LTV_2)?>%</td>
                    </tr>
                    <tr>
                    	<th class="text-left">Rate (%)</th>
                        <td><?=round($print_data['Analyser']->Rate_1)?>%</td>
                        <td><?=round($print_data['Analyser']->Rate_2)?>%</td>
                    </tr>
                    <tr>
                    	<th colspan="3">&nbsp;</th>
                    </tr>
                    <tr>
                    	<th class="text-left">Origination fee</th>
                        <td><?=round($print_data['Analyser']->OriginationFee_1)?>%</td>
                        <td><?=round($print_data['Analyser']->OriginationFee_2)?>%</td>
                    </tr>
                    <tr>
                    	<th class="text-left">Discount fee</th>
                        <td><?=round($print_data['Analyser']->DiscountFee_1)?>%</td>
                        <td><?=round($print_data['Analyser']->DiscountFee_2)?>%</td>
                    </tr>
                    <tr>
                    	<th class="text-left">Closing costs & misc. fees</th>
                        <td colspan="2"><?=round($print_data['Analyser']->ClosingMiscFees)?>%</td>
                    </tr>
                    <tr>
                    	<th class="text-left">Cash back from Seller</th>
                        <td colspan="2"><?=round($print_data['Analyser']->CashbackSeller)?>%</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
	    <div class="col-lg-12">
            <table class="table table-bordered text-center">
            	<thead>
                	<tr>
                    	<th>&nbsp;</th>
	                	<th colspan="2" class="text-center">Property 1</th>
                        <th colspan="2" class="text-center">Property 2</th>
                        <th colspan="2" class="text-center">Property 3</th>
                        <th colspan="2" class="text-center">Property 4</th>
                        <th colspan="2" class="text-center">Property 5</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                    	<td class="text-left">Address</td>
                        <td><?=$print_data['Analyser']->P1Address?></td>
                        <td>&nbsp;</td>
                        <td><?=$print_data['Analyser']->P2Address?></td>
                        <td>&nbsp;</td>
                        <td><?=$print_data['Analyser']->P3Address?></td>
                        <td>&nbsp;</td>
                        <td><?=$print_data['Analyser']->P4Address?></td>
                        <td>&nbsp;</td>
                        <td><?=$print_data['Analyser']->P5Address?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Price</td>
                        <td>$<?=$print_data['Analyser']->P1Price?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P2Price?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P3Price?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P4Price?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P5Price?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Sq Feet</td>
                        <td><?=$print_data['Analyser']->P1SqFeet?></td>
                        <td>&nbsp;</td>
                        <td><?=$print_data['Analyser']->P2SqFeet?></td>
                        <td>&nbsp;</td>
                        <td><?=$print_data['Analyser']->P3SqFeet?></td>
                        <td>&nbsp;</td>
                        <td><?=$print_data['Analyser']->P4SqFeet?></td>
                        <td>&nbsp;</td>
                        <td><?=$print_data['Analyser']->P5SqFeet?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Initial Capital Improvements</td>
                        <td>$<?=$print_data['Analyser']->P1CapImp?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P2CapImp?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P3CapImp?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P4CapImp?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P5CapImp?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Total Initial Capital Outlay</td>
                        <td>$<?=$print_data['Analyser']->P1CapOut?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P2CapOut?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P3CapOut?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P4CapOut?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P5CapOut?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="text-left">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td class="text-right">% of Rent</td>
                        <td>&nbsp;</td>
                        <td class="text-right">% of Rent</td>
                        <td>&nbsp;</td>
                        <td class="text-right">% of Rent</td>
                        <td>&nbsp;</td>
                        <td class="text-right">% of Rent</td>
                        <td>&nbsp;</td>
                        <td class="text-right">% of Rent</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Total Rent (Monthly)</td>
                        <td>$<?=$print_data['Analyser']->P1Rent?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P2Rent?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P3Rent?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P4Rent?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P5Rent?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Other Income (Monthly)</td>
                        <td>$<?=$print_data['Analyser']->P1Income?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P2Income?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P3Income?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P4Income?></td>
                        <td>&nbsp;</td>
                        <td>$<?=$print_data['Analyser']->P5Income?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Vacancy (%)</td>
                        <td><?=round($print_data['Analyser']->P1Vacancy)?>%</td>
                        <td>&nbsp;</td>
                        <td><?=round($print_data['Analyser']->P2Vacancy)?>%</td>
                        <td>&nbsp;</td>
                        <td><?=round($print_data['Analyser']->P3Vacancy)?>%</td>
                        <td>&nbsp;</td>
                        <td><?=round($print_data['Analyser']->P4Vacancy)?>%</td>
                        <td>&nbsp;</td>
                        <td><?=round($print_data['Analyser']->P5Vacancy)?>%</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Management (%)</td>
                        <td><?=round($print_data['Analyser']->P1Management)?>%</td>
                        <td>&nbsp;</td>
                        <td><?=round($print_data['Analyser']->P2Management)?>%</td>
                        <td>&nbsp;</td>
                        <td><?=round($print_data['Analyser']->P3Management)?>%</td>
                        <td>&nbsp;</td>
                        <td><?=round($print_data['Analyser']->P4Management)?>%</td>
                        <td>&nbsp;</td>
                        <td><?=round($print_data['Analyser']->P5Management)?>%</td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Taxes (Annual)</td>
                        <td>$<?=$print_data['Analyser']->P1Taxes?></td>
                        <td><?=round($print_data['Analyser']->P1TaxesRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P2Taxes?></td>
                        <td><?=round($print_data['Analyser']->P2TaxesRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P3Taxes?></td>
                        <td><?=round($print_data['Analyser']->P3TaxesRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P4Taxes?></td>
                        <td><?=round($print_data['Analyser']->P4TaxesRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P5Taxes?></td>
                        <td><?=round($print_data['Analyser']->P5TaxesRent)?>%</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Insurance (Annual)</td>
                        <td>$<?=$print_data['Analyser']->P1Ins?></td>
                        <td><?=round($print_data['Analyser']->P1InsRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P2Ins?></td>
                        <td><?=round($print_data['Analyser']->P2InsRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P3Ins?></td>
                        <td><?=round($print_data['Analyser']->P3InsRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P4Ins?></td>
                        <td><?=round($print_data['Analyser']->P4InsRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P5Ins?></td>
                        <td><?=round($print_data['Analyser']->P5InsRent)?>%</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Maintenance (Annual)</td>
                        <td>$<?=$print_data['Analyser']->P1Maint?></td>
                        <td><?=round($print_data['Analyser']->P1MaintRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P2Maint?></td>
                        <td><?=round($print_data['Analyser']->P2MaintRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P3Maint?></td>
                        <td><?=round($print_data['Analyser']->P3MaintRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P4Maint?></td>
                        <td><?=round($print_data['Analyser']->P4MaintRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P5Maint?></td>
                        <td><?=round($print_data['Analyser']->P5MaintRent)?>%</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Other Operating Expenses (Annual)</td>
                        <td>$<?=$print_data['Analyser']->P1OpEx?></td>
                        <td><?=round($print_data['Analyser']->P1OpExRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P2OpEx?></td>
                        <td><?=round($print_data['Analyser']->P2OpExRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P3OpEx?></td>
                        <td><?=round($print_data['Analyser']->P3OpExRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P4OpEx?></td>
                        <td><?=round($print_data['Analyser']->P4OpExRent)?>%</td>
                        <td>$<?=$print_data['Analyser']->P5OpEx?></td>
                        <td><?=round($print_data['Analyser']->P5OpExRent)?>%</td>
                    </tr>
                    <tr>
                    	<td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><?=round($print_data['Analyser']->P1RentPercent)?>%</td>
                        <td></td>
                        <td><?=round($print_data['Analyser']->P2RentPercent)?>%</td>
                        <td></td>
                        <td><?=round($print_data['Analyser']->P3RentPercent)?>%</td>
                        <td></td>
                        <td><?=round($print_data['Analyser']->P4RentPercent)?>%</td>
                        <td></td>
                        <td><?=round($print_data['Analyser']->P5RentPercent)?>%</td>
                    </tr>
                    <tr>
                    	<td class="text-left">NOI (Monthly)</td>
                        <td>$<?=number_format($print_data['Analyser']->P1NOI, 0)?></td>
                        <td>&nbsp;</td>
                        <td>$<?=number_format($print_data['Analyser']->P2NOI, 0)?></td>
                        <td>&nbsp;</td>
                        <td>$<?=number_format($print_data['Analyser']->P3NOI, 0)?></td>
                        <td>&nbsp;</td>
                        <td>$<?=number_format($print_data['Analyser']->P4NOI, 0)?></td>
                        <td>&nbsp;</td>
                        <td>$<?=number_format($print_data['Analyser']->P5NOI, 0)?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td class="text-left">Mortgage Payment (Monthly)</td>
                        <td>$<?=number_format($print_data['Analyser']->P1MortPymt, 0)?></td>
                        <td>&nbsp;</td>
                        <td>$<?=number_format($print_data['Analyser']->P2MortPymt, 0)?></td>
                        <td>&nbsp;</td>
                        <td>$<?=number_format($print_data['Analyser']->P3MortPymt, 0)?></td>
                        <td>&nbsp;</td>
                        <td>$<?=number_format($print_data['Analyser']->P4MortPymt, 0)?></td>
                        <td>&nbsp;</td>
                        <td>$<?=number_format($print_data['Analyser']->P5MortPymt, 0)?></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                    	<td colspan="11">&nbsp;</td>
                    </tr>
                	<tr>
                    	<td class="text-left">Cash Flow (Monthly)</td>
                        <td colspan="2"><?=$P1CashFlow?></td>
                        <td colspan="2"><?=$P2CashFlow?></td>
                        <td colspan="2"><?=$P3CashFlow?></td>
                        <td colspan="2"><?=$P4CashFlow?></td>
                        <td colspan="2"><?=$P5CashFlow?></td>
                    </tr>
                    <tr>
                    	<td>&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>Rent</td>
                        <td>&nbsp;</td>
                        <td>Rent</td>
                        <td>&nbsp;</td>
                        <td>Rent</td>
                        <td>&nbsp;</td>
                        <td>Rent</td>
                        <td>&nbsp;</td>
                        <td>Rent</td>
                    </tr>
                	<tr>
                    	<td class="text-left">Cap Rate</td>
                        <td><?=($P1CapRate != 'N/A' ? round($P1CapRate) . '%' : $P1CapRate)?></td>
                        <td><?=$P1CapRateRank?></td>
                        <td><?=($P2CapRate != 'N/A' ? round($P2CapRate) . '%' : $P2CapRate)?></td>
                        <td><?=$P2CapRateRank?></td>
                        <td><?=($P3CapRate != 'N/A' ? round($P3CapRate) . '%' : $P3CapRate)?></td>
                        <td><?=$P3CapRateRank?></td>
                        <td><?=($P4CapRate != 'N/A' ? round($P4CapRate) . '%' : $P4CapRate)?></td>
                        <td><?=$P4CapRateRank?></td>
                        <td><?=($P5CapRate != 'N/A' ? round($P5CapRate) . '%' : $P5CapRate)?></td>
                        <td><?=$P5CapRateRank?></td>
                    </tr>
                	<tr>
                    	<td class="text-left">Pre-tax Cash/Cash</td>
                        <td><?=($P1PreTaxCashCash != 'N/A' ? round($P1PreTaxCashCash) . '%' : $P1PreTaxCashCash)?></td>
                        <td><?=$P1PreTaxCashCashRank?></td>
                        <td><?=($P2PreTaxCashCash != 'N/A' ? round($P2PreTaxCashCash) . '%' : $P2PreTaxCashCash)?></td>
                        <td><?=$P2PreTaxCashCashRank?></td>
                        <td><?=($P3PreTaxCashCash != 'N/A' ? round($P3PreTaxCashCash) . '%' : $P3PreTaxCashCash)?></td>
                        <td><?=$P3PreTaxCashCashRank?></td>
                        <td><?=($P4PreTaxCashCash != 'N/A' ? round($P4PreTaxCashCash) . '%' : $P4PreTaxCashCash)?></td>
                        <td><?=$P4PreTaxCashCashRank?></td>
                        <td><?=($P5PreTaxCashCash != 'N/A' ? round($P5PreTaxCashCash) . '%' : $P5PreTaxCashCash)?></td>
                        <td><?=$P5PreTaxCashCashRank?></td>
                    </tr>
                	<tr>
                    	<td class="text-left">Gross Rent Multiplier (GRM)</td>
                        <td><?=$P1GrossRentMultiplierGRM?></td>
                        <td><?=$P1GrossRentMultiplierGRMRank?></td>
                        <td><?=$P2GrossRentMultiplierGRM?></td>
                        <td><?=$P2GrossRentMultiplierGRMRank?></td>
                        <td><?=$P3GrossRentMultiplierGRM?></td>
                        <td><?=$P3GrossRentMultiplierGRMRank?></td>
                        <td><?=$P4GrossRentMultiplierGRM?></td>
                        <td><?=$P4GrossRentMultiplierGRMRank?></td>
                        <td><?=$P5GrossRentMultiplierGRM?></td>
                        <td><?=$P5GrossRentMultiplierGRMRank?></td>
                    </tr>
                	<tr>
                    	<td class="text-left">$/SqFoot</td>
                        <td><?=($P1SqFoot != 'N/A' ? '$' . $P1SqFoot : $P1SqFoot)?></td>
                        <td><?=$P1SqFootRank?></td>
                        <td><?=($P2SqFoot != 'N/A' ? '$' . $P2SqFoot : $P2SqFoot)?></td>
                        <td><?=$P2SqFootRank?></td>
                        <td><?=($P3SqFoot != 'N/A' ? '$' . $P3SqFoot : $P3SqFoot)?></td>
                        <td><?=$P3SqFootRank?></td>
                        <td><?=($P4SqFoot != 'N/A' ? '$' . $P4SqFoot : $P4SqFoot)?></td>
                        <td><?=$P4SqFootRank?></td>
                        <td><?=($P5SqFoot != 'N/A' ? '$' . $P5SqFoot : $P5SqFoot)?></td>
                        <td><?=$P5SqFootRank?></td>
                    </tr>
                	<tr>
                    	<td class="text-left">Payback period</td>
                        <td><?=$P1PaybackPeriod?></td>
                        <td><?=$P1PaybackPeriodRank?></td>
                        <td><?=$P2PaybackPeriod?></td>
                        <td><?=$P2PaybackPeriodRank?></td>
                        <td><?=$P3PaybackPeriod?></td>
                        <td><?=$P3PaybackPeriodRank?></td>
                        <td><?=$P4PaybackPeriod?></td>
                        <td><?=$P4PaybackPeriodRank?></td>
                        <td><?=$P5PaybackPeriod?></td>
                        <td><?=$P5PaybackPeriodRank?></td>
                    </tr>
                	<tr>
                    	<td class="text-left">Debt Coverage Ratio (DCR)</td>
                        <td><?=$P1DebtCoverageRatioDCR?></td>
                        <td><?=$P1DebtCoverageRatioDCRRank?></td>
                        <td><?=$P2DebtCoverageRatioDCR?></td>
                        <td><?=$P2DebtCoverageRatioDCRRank?></td>
                        <td><?=$P3DebtCoverageRatioDCR?></td>
                        <td><?=$P3DebtCoverageRatioDCRRank?></td>
                        <td><?=$P4DebtCoverageRatioDCR?></td>
                        <td><?=$P4DebtCoverageRatioDCRRank?></td>
                        <td><?=$P5DebtCoverageRatioDCR?></td>
                        <td><?=$P5DebtCoverageRatioDCRRank?></td>
                    </tr>
                	<tr>
                    	<td class="text-left">Break-Even Ratio</td>
                        <td><?=($P1BreakEvenRatio != 'N/A' ? round($P1BreakEvenRatio) . '%' : $P1BreakEvenRatio)?></td>
                        <td><?=$P1BreakEvenRatioRank?></td>
                        <td><?=($P2BreakEvenRatio != 'N/A' ? round($P2BreakEvenRatio) . '%' : $P2BreakEvenRatio)?></td>
                        <td><?=$P2BreakEvenRatioRank?></td>
                        <td><?=($P3BreakEvenRatio != 'N/A' ? round($P3BreakEvenRatio) . '%' : $P3BreakEvenRatio)?></td>
                        <td><?=$P3BreakEvenRatioRank?></td>
                        <td><?=($P4BreakEvenRatio != 'N/A' ? round($P4BreakEvenRatio) . '%' : $P4BreakEvenRatio)?></td>
                        <td><?=$P4BreakEvenRatioRank?></td>
                        <td><?=($P5BreakEvenRatio != 'N/A' ? round($P5BreakEvenRatio) . '%' : $P5BreakEvenRatio)?></td>
                        <td><?=$P5BreakEvenRatioRank?></td>
                    </tr>
                	<tr>
                    	<td class="text-left">Average Rank</td>
                        <td>&nbsp;</td>
                        <td><?=($P1CapRateRank=="N/A" ? "N/A" : AVERAGE(array($P1CapRateRank, $P1PreTaxCashCashRank, $P1GrossRentMultiplierGRMRank, $P1SqFootRank, $P1PaybackPeriodRank, $P1DebtCoverageRatioDCRRank, $P1BreakEvenRatioRank)))?></td>
                        <td>&nbsp;</td>
                        <td><?=($P2CapRateRank=="N/A" ? "N/A" : AVERAGE(array($P2CapRateRank, $P2PreTaxCashCashRank, $P2GrossRentMultiplierGRMRank, $P2SqFootRank, $P2PaybackPeriodRank, $P2DebtCoverageRatioDCRRank, $P2BreakEvenRatioRank)))?></td>
                        <td>&nbsp;</td>
                        <td><?=($P3CapRateRank=="N/A" ? "N/A" : AVERAGE(array($P3CapRateRank, $P3PreTaxCashCashRank, $P3GrossRentMultiplierGRMRank, $P3SqFootRank, $P3PaybackPeriodRank, $P3DebtCoverageRatioDCRRank, $P3BreakEvenRatioRank)))?></td>
                        <td>&nbsp;</td>
                        <td><?=($P4CapRateRank=="N/A" ? "N/A" : AVERAGE(array($P4CapRateRank, $P4PreTaxCashCashRank, $P4GrossRentMultiplierGRMRank, $P4SqFootRank, $P4PaybackPeriodRank, $P4DebtCoverageRatioDCRRank, $P4BreakEvenRatioRank)))?></td>
                        <td>&nbsp;</td>
                        <td><?=($P5CapRateRank=="N/A" ? "N/A" : AVERAGE(array($P5CapRateRank, $P5PreTaxCashCashRank, $P5GrossRentMultiplierGRMRank, $P5SqFootRank, $P5PaybackPeriodRank, $P5DebtCoverageRatioDCRRank, $P5BreakEvenRatioRank)))?></td>
                    </tr>
                	<tr>
                    	<td class="text-left">Median Rank</td>
                        <td>&nbsp;</td>
                        <td><?=($P1CapRateRank=="N/A" ? "N/A" : MEDIAN(array($P1CapRateRank, $P1PreTaxCashCashRank, $P1GrossRentMultiplierGRMRank, $P1SqFootRank, $P1PaybackPeriodRank, $P1DebtCoverageRatioDCRRank, $P1BreakEvenRatioRank)))?></td>
                        <td>&nbsp;</td>
                        <td><?=($P2CapRateRank=="N/A" ? "N/A" : MEDIAN(array($P2CapRateRank, $P2PreTaxCashCashRank, $P2GrossRentMultiplierGRMRank, $P2SqFootRank, $P2PaybackPeriodRank, $P2DebtCoverageRatioDCRRank, $P2BreakEvenRatioRank)))?></td>
                        <td>&nbsp;</td>
                        <td><?=($P3CapRateRank=="N/A" ? "N/A" : MEDIAN(array($P3CapRateRank, $P3PreTaxCashCashRank, $P3GrossRentMultiplierGRMRank, $P3SqFootRank, $P3PaybackPeriodRank, $P3DebtCoverageRatioDCRRank, $P3BreakEvenRatioRank)))?></td>
                        <td>&nbsp;</td>
                        <td><?=($P4CapRateRank=="N/A" ? "N/A" : MEDIAN(array($P4CapRateRank, $P4PreTaxCashCashRank, $P4GrossRentMultiplierGRMRank, $P4SqFootRank, $P4PaybackPeriodRank, $P4DebtCoverageRatioDCRRank, $P4BreakEvenRatioRank)))?></td>
                        <td>&nbsp;</td>
                        <td><?=($P5CapRateRank=="N/A" ? "N/A" : MEDIAN(array($P5CapRateRank, $P5PreTaxCashCashRank, $P5GrossRentMultiplierGRMRank, $P5SqFootRank, $P5PaybackPeriodRank, $P5DebtCoverageRatioDCRRank, $P5BreakEvenRatioRank)))?></td>
                    </tr>
				</tbody>
            </table>
        </div>
    </div>
</div>