<style type="text/css">
.fields-panel h3{font-size:18px;text-align:center;}
.fields-panel h4{text-align:center;}
.assump-box{background:transparent;padding:10px 15px;margin-bottom:10px;}
.assump-box-child{background:none;z-index:1;position:relative;}
.panel-field input, .panel-field select{margin-bottom:0;padding:5px;font-size:16px;border:1px solid #34a15e;background:#ceead9;box-shadow:none;color:#000;}
.panel-field input[readonly]{color:#000;}
.panel-field button{font-size:14px;}
.panel-field label{margin-bottom:0;font-weight:normal;font-size:18px;color:#222;line-height:1;}
.assump-box .full-section-box:last-child .panel-field > .row {margin-right:0;}
.form-inline .panel-field .input-group{width:100%;}
.form-inline .panel-field .form-control{width:100%;box-shadow:none;}
.form-inline .panel-field .input-group .input-group-addon{width:20px;}
hr{margin-bottom:10px;border-top:1px solid #ccc;}
.input-group-addon {background:#007C30;border:1px solid #007C30;border-radius:0;padding:5px;color:#fff;}
.input-group .form-control {border:1px solid #007C30;}
.panel-field:nth-child(odd) .form-control {background-color:#c6eefd;border:1px solid #2a6c84;}
.panel-field:nth-child(even) .form-control {background:#ceead9;border:1px solid #007c30;}
.panel-field:nth-child(odd) .input-group-addon {background-color:#2a6c84;border:1px solid #2a6c84;}
.panel-field:nth-child(even) .input-group-addon {background-color:#007c30;border:1px solid #007c30;}
.topButtons{top:-55px;}
#TaxesRow .input-group, #InsuranceRow .input-group, #MaintenanceRow .input-group, #OpExRow .input-group {margin: 0 5px;}
</style>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <h2>Rental Property Comparison Analyser</h2>
                    </div>
                </div>
                
                <div class="page-content">
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-calculator"></i> Rental Property Comparison Analyser</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#analyser_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                    <h3 class="box-title col-xs-6 col-xs-offset-3">
                                        <div class="progress">
                                            <div class="progress-bar" data-transitiongoal="0"></div>
                                        </div>
                                    </h3>
                                </div>
                                <div class="box-body">
                                	<div class="offer-calc-page">
										<?php if ( $success ) { ?>
                                            <div class="alert alert-success" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-info-circle"></i> <?=$success?>
                                            </div>
                                        <?php } ?>

                                		<input type="hidden" id="page_type" value="COMPARISON" />
                                        <input type="hidden" id="PageStatus" value="INSERT" />
                                        <div class="fields-panel flex-box offer-calculator-screen">
                                            <div class="flex-box-child">
                                                <div class="col-xs-12 assump-box" id="QuickAnalyser">
                                                
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>1) FINANCING ASSUMPTIONS (applied to all properties)</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child"><!-- STEP (1) -->
                                                        
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/comparison-analyser') . '/', $attributes)?>
                                                            
                                                                <!--<h4>These apply to all properties</h4><hr />-->
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-sm btn-primary on_next_section analyser1 trigger-bar" data-rel="33">NEXT</button>
                                                                </div><!-- Next Button -->
                                                                
                                                                <div class="panel-field" id="DownPaymentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Down Payment %:</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "DownPayment",
                                                                                'id'    => "DownPayment",
                                                                                'value' => 20,
                                                                                'class'  => 'form-control number-field NumberOnly percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Down Payment % -->
                                                                <div class="panel-field" id="MortgageRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">&nbsp;</label>
                                                                        <div class="col-xs-3 text-center"><strong>1st Mortgage</strong></div>
                                                                        <div class="col-xs-3 text-center"><strong>2nd Mortgage</strong></div>
                                                                    </div>
                                                                </div><!-- Mortgage -->
                                                                <div class="panel-field" id="UsedRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Mortgages Used?</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes'  => 'Yes',
                                                                                'No'  => 'No',
                                                                            );
                                                                            $attrb = 'id="Used_1" class="form-control"'; ?>
                                                                            <?=form_dropdown('Used_1', $options, 'Yes', $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes'  => 'Yes',
                                                                                'No'  => 'No',
                                                                            );
                                                                            $attrb = 'id="Used_2" class="form-control"'; ?>
                                                                            <?=form_dropdown('Used_2', $options, 'No', $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Used? -->
                                                                <div class="panel-field" id="LoanTypeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Loan Type:</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Fixed'  => 'Fixed',
                                                                                'Interest Only'  => 'Interest Only',
                                                                            );
                                                                            $attrb = 'id="LoanType_1" class="form-control"'; ?>
                                                                            <?=form_dropdown('LoanType_1', $options, 'Interest Only', $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Fixed'  => 'Fixed',
                                                                                'Interest Only'  => 'Interest Only',
                                                                            );
                                                                            $attrb = 'id="LoanType_2" class="form-control hide"'; ?>
                                                                            <?=form_dropdown('LoanType_2', $options, 'Interest Only', $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Used? -->
                                                                <div class="panel-field" id="TermYearsRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Term (In Years):</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TermYears_1",
                                                                                'id'    => "TermYears_1",
                                                                                'value' => 30,
                                                                                'class'  => "form-control NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TermYears_2",
                                                                                'id'    => "TermYears_2",
                                                                                'value' => 30,
                                                                                'class'  => "form-control NumberOnly hide",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Term in Years -->
                                                                <div class="panel-field" id="LTVRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">LTV %: <span class="require">*</span></label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "LTV_1",
                                                                                'id'    => "LTV_1",
                                                                                'value' => ($reset) ? "" : set_value("LTV_1"),
                                                                                'class'  => "form-control number-field NumberOnly Required percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "LTV_2",
                                                                                'id'    => "LTV_2",
                                                                                'value' => ($reset) ? "" : set_value("LTV_2"),
                                                                                'class'  => "form-control number-field NumberOnly percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group hide">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- LTV % -->
                                                                <div class="panel-field" id="RateRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Interest Rate: <span class="require">*</span></label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Rate_1",
                                                                                'id'    => "Rate_1",
                                                                                'value' => ($reset) ? "" : set_value("Rate_1"),
                                                                                'class'  => "form-control number-field NumberOnly Required",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Rate_2",
                                                                                'id'    => "Rate_2",
                                                                                'value' => ($reset) ? "" : set_value("Rate_2"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group hide">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Rate -->
                                                                <div class="panel-field hide" id="InterestOnlyRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Interest Only?</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "InterestOnly_1",
                                                                                'id'    => "InterestOnly_1",
                                                                                'value' => 'Yes',
                                                                                'class'  => "form-control",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "InterestOnly_2",
                                                                                'id'    => "InterestOnly_2",
                                                                                'value' => 'Yes',
                                                                                'class'  => "form-control",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- InterestOnly -->
                                                                
                                                                <div class="panel-field" id="OriginationFeeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Origination fee: <span class="require">*</span></label>	
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OriginationFee_1",
                                                                                'id'    => "OriginationFee_1",
                                                                                'value' => ($reset) ? "" : set_value("OriginationFee_1"),
                                                                                'class'  => "form-control number-field NumberOnly Required percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>    
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OriginationFee_2",
                                                                                'id'    => "OriginationFee_2",
                                                                                'value' => ($reset) ? "" : set_value("OriginationFee_2"),
                                                                                'class'  => "form-control number-field NumberOnly percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group hide">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>    
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Origination Fee -->
                                                                <div class="panel-field" id="DiscountFeeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Discount fee: <span class="require">*</span></label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "DiscountFee_1",
                                                                                'id'    => "DiscountFee_1",
                                                                                'value' => ($reset) ? "" : set_value("DiscountFee_1"),
                                                                                'class'  => "form-control number-field NumberOnly Required percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>                     
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "DiscountFee_2",
                                                                                'id'    => "DiscountFee_2",
                                                                                'value' => ($reset) ? "" : set_value("DiscountFee_2"),
                                                                                'class'  => "form-control number-field NumberOnly percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group hide">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>                     
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Discount Fee -->
                                                                <div class="panel-field" id="ClosingMiscFeesRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Closing costs & misc. fees: <span class="require">*</span></label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ClosingMiscFees",
                                                                                'id'    => "ClosingMiscFees",
                                                                                'value' => ($reset) ? "" : set_value("ClosingMiscFees"),
                                                                                'class'  => "form-control number-field Required NumberOnly percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                             <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                             </div>                     
                                                                        </div>
                                                                        <div class="col-xs-3">&nbsp;</div>
                                                                    </div>
                                                                </div><!-- Closing costs & misc. fees -->
                                                                <div class="panel-field" id="CashbackSellerRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Cash back from Seller:</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CashbackSeller",
                                                                                'id'    => "CashbackSeller",
                                                                                'value' => ($reset) ? "" : set_value("CashbackSeller"),
                                                                                'class'  => "form-control number-field NumberOnly percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>                     
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Cash back from Seller -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-sm btn-primary on_next_section analyser1 trigger-bar" data-rel="33">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="analyser_financing_assumption" />
                                                                </div><!-- Next Button -->
                                                            
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                    </div>
                                                        
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>2) PROPERTY INFO</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child"><!-- STEP (2) -->
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/comparison-analyser') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-sm btn-default on_prev_section trigger-bar" data-rel="0">BACK</button>
                                                                    <button type="button" class="btn btn-sm btn-primary on_next_section trigger-bar" data-rel="66">NEXT</button>
                                                                </div><!-- BACK NEXT Button -->
                                                                
                                                                <div class="panel-field text-center" id="P1HeaderRow">
                                                                    <!--<div class="row">
                                                                        <label class="col-xs-2">&nbsp;</label>
                                                                        <label class="col-xs-2"><strong>Property 1</strong></label>
                                                                        <label class="col-xs-2"><strong>Property 2</strong></label>
                                                                        <label class="col-xs-2"><strong>Property 3</strong></label>
                                                                        <label class="col-xs-2"><strong>Property 4</strong></label>
                                                                        <label class="col-xs-2"><strong>Property 5</strong></label>
                                                                    </div>-->
                                                                    <hr />
                                                                </div><!-- Header -->
                                                                <div class="panel-field" id="AddressRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Address:</label>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P1Address",
                                                                                'id'    => "P1Address",
                                                                                'value' => ($reset) ? "" : set_value("P1Address"),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P2Address",
                                                                                'id'    => "P2Address",
                                                                                'value' => ($reset) ? "" : set_value("P2Address"),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P3Address",
                                                                                'id'    => "P3Address",
                                                                                'value' => ($reset) ? "" : set_value("P3Address"),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P4Address",
                                                                                'id'    => "P4Address",
                                                                                'value' => ($reset) ? "" : set_value("P4Address"),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P5Address",
                                                                                'id'    => "P5Address",
                                                                                'value' => ($reset) ? "" : set_value("P5Address"),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Address -->
                                                                <div class="panel-field" id="PriceRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Price:</label>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P1Price",
                                                                                'id'    => "P1Price",
                                                                                'value' => ($reset) ? "" : set_value("P1Price"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P2Price",
                                                                                'id'    => "P2Price",
                                                                                'value' => ($reset) ? "" : set_value("P2Price"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P3Price",
                                                                                'id'    => "P3Price",
                                                                                'value' => ($reset) ? "" : set_value("P3Price"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P4Price",
                                                                                'id'    => "P4Price",
                                                                                'value' => ($reset) ? "" : set_value("P4Price"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P5Price",
                                                                                'id'    => "P5Price",
                                                                                'value' => ($reset) ? "" : set_value("P5Price"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Price -->
                                                                <div class="panel-field" id="SqFeetRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Sq Feet:</label>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P1SqFeet",
                                                                                'id'    => "P1SqFeet",
                                                                                'value' => ($reset) ? "" : set_value("P1SqFeet"),
                                                                                'class'  => "form-control NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P2SqFeet",
                                                                                'id'    => "P2SqFeet",
                                                                                'value' => ($reset) ? "" : set_value("P2SqFeet"),
                                                                                'class'  => "form-control NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P3SqFeet",
                                                                                'id'    => "P3SqFeet",
                                                                                'value' => ($reset) ? "" : set_value("P3SqFeet"),
                                                                                'class'  => "form-control NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P4SqFeet",
                                                                                'id'    => "P4SqFeet",
                                                                                'value' => ($reset) ? "" : set_value("P4SqFeet"),
                                                                                'class'  => "form-control NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P5SqFeet",
                                                                                'id'    => "P5SqFeet",
                                                                                'value' => ($reset) ? "" : set_value("P5SqFeet"),
                                                                                'class'  => "form-control NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Sq Feet -->
                                                                <div class="panel-field" id="CapImpRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Initial Capital Improvements:</label>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P1CapImp",
                                                                                'id'    => "P1CapImp",
                                                                                'value' => ($reset) ? "" : set_value("P1CapImp"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P2CapImp",
                                                                                'id'    => "P2CapImp",
                                                                                'value' => ($reset) ? "" : set_value("P2CapImp"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P3CapImp",
                                                                                'id'    => "P3CapImp",
                                                                                'value' => ($reset) ? "" : set_value("P3CapImp"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P4CapImp",
                                                                                'id'    => "P4CapImp",
                                                                                'value' => ($reset) ? "" : set_value("P4CapImp"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P5CapImp",
                                                                                'id'    => "P5CapImp",
                                                                                'value' => ($reset) ? "" : set_value("P5CapImp"),
                                                                                'class'  => "form-control number-field NumberOnly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Initial Capital Improvements -->
                                                                <div class="panel-field" id="CapOutRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Total Initial Capital Outlay:</label>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P1CapOut",
                                                                                'id'    => "P1CapOut",
                                                                                'value' => ($reset) ? "" : set_value("P1CapOut"),
                                                                                'class'  => "form-control number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P2CapOut",
                                                                                'id'    => "P2CapOut",
                                                                                'value' => ($reset) ? "" : set_value("P2CapOut"),
                                                                                'class'  => "form-control number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P3CapOut",
                                                                                'id'    => "P3CapOut",
                                                                                'value' => ($reset) ? "" : set_value("P3CapOut"),
                                                                                'class'  => "form-control number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P4CapOut",
                                                                                'id'    => "P4CapOut",
                                                                                'value' => ($reset) ? "" : set_value("P4CapOut"),
                                                                                'class'  => "form-control number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "P5CapOut",
                                                                                'id'    => "P5CapOut",
                                                                                'value' => ($reset) ? "" : set_value("P5CapOut"),
                                                                                'class'  => "form-control number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>                                                                        
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Initial Capital Improvements -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-sm btn-default on_prev_section trigger-bar" data-rel="0">BACK</button>
                                                                    <button type="button" class="btn btn-sm btn-primary on_next_section trigger-bar" data-rel="66">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" id="ProposalStatus" name="ProposalStatus" value="draft" />
                                                                    <input type="hidden" name="TableName" value="analyser_property_details" />
                                                                </div><!-- BACK NEXT Button -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>                                                
                                                    </div>
                                                    
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>3) RENT AND EXPENSES</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child"><!-- STEP (3) -->
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/comparison-analyser') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide loading" />
                                                                    <button type="button" class="btn btn-sm btn-default on_prev_section trigger-bar" data-rel="33">BACK</button>
                                                                    <button type="button" class="btn btn-sm btn-primary on_save_event trigger-bar" data-rel="100">SAVE</button>
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                                <div class="panel-field text-center" id="P2HeaderRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-1">&nbsp;</label>
                                                                        <label class="col-xs-3"><strong></strong></label>
                                                                        <label class="col-xs-1"><strong></strong></label>
                                                                        <label class="col-xs-3"><strong></strong></label>
                                                                        <label class="col-xs-1"><strong></strong></label>
                                                                        <label class="col-xs-3"><strong></strong></label>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Header2 -->
                                                                <div class="panel-field" id="P2RentHeaderRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">&nbsp;</label>
                                                                        <label class="col-xs-1"></label>
                                                                        <label class="col-xs-1">% of Rent</label>
                                                                        <label class="col-xs-1"></label>
                                                                        <label class="col-xs-1">% of Rent</label>
                                                                        <label class="col-xs-1"></label>
                                                                        <label class="col-xs-1">% of Rent</label>
                                                                        <label class="col-xs-1"></label>
                                                                        <label class="col-xs-1">% of Rent</label>
                                                                        <label class="col-xs-1"></label>
                                                                        <label class="col-xs-1">% of Rent</label>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Rent Header -->
                                                                <div class="panel-field" id="RentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Total Rent (Monthly):</label>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1Rent",
                                                                                    'id'    => "P1Rent",
                                                                                    'value' => ($reset) ? "" : set_value("P1Rent"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2Rent",
                                                                                    'id'    => "P2Rent",
                                                                                    'value' => ($reset) ? "" : set_value("P2Rent"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3Rent",
                                                                                    'id'    => "P3Rent",
                                                                                    'value' => ($reset) ? "" : set_value("P3Rent"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4Rent",
                                                                                    'id'    => "P4Rent",
                                                                                    'value' => ($reset) ? "" : set_value("P4Rent"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5Rent",
                                                                                    'id'    => "P5Rent",
                                                                                    'value' => ($reset) ? "" : set_value("P5Rent"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                    </div>
                                                                </div><!-- Total Rent (Monthly) -->
                                                                <div class="panel-field" id="OtherIncomeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Other Income (Monthly):</label> 
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1Income",
                                                                                    'id'    => "P1Income",
                                                                                    'value' => ($reset) ? "" : set_value("P1Income"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2Income",
                                                                                    'id'    => "P2Income",
                                                                                    'value' => ($reset) ? "" : set_value("P2Income"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3Income",
                                                                                    'id'    => "P3Income",
                                                                                    'value' => ($reset) ? "" : set_value("P3Income"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4Income",
                                                                                    'id'    => "P4Income",
                                                                                    'value' => ($reset) ? "" : set_value("P4Income"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5Income",
                                                                                    'id'    => "P5Income",
                                                                                    'value' => ($reset) ? "" : set_value("P5Income"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                    </div>
                                                                </div><!-- Other Income (Monthly) -->
                                                                <div class="panel-field" id="VacancyRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Vacancy (%):</label>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1Vacancy",
                                                                                    'id'    => "P1Vacancy",
                                                                                    'value' => ($reset) ? "" : set_value("P1Vacancy"),
                                                                                    'class'  => "form-control number-field NumberOnly percentageField",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2Vacancy",
                                                                                    'id'    => "P2Vacancy",
                                                                                    'value' => ($reset) ? "" : set_value("P2Vacancy"),
                                                                                    'class'  => "form-control number-field NumberOnly percentageField",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3Vacancy",
                                                                                    'id'    => "P3Vacancy",
                                                                                    'value' => ($reset) ? "" : set_value("P3Vacancy"),
                                                                                    'class'  => "form-control number-field NumberOnly percentageField",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4Vacancy",
                                                                                    'id'    => "P4Vacancy",
                                                                                    'value' => ($reset) ? "" : set_value("P4Vacancy"),
                                                                                    'class'  => "form-control number-field NumberOnly percentageField",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5Vacancy",
                                                                                    'id'    => "P5Vacancy",
                                                                                    'value' => ($reset) ? "" : set_value("P5Vacancy"),
                                                                                    'class'  => "form-control number-field NumberOnly percentageField",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                    </div>
                                                                </div><!-- Vacancy (%) -->
                                                                <div class="panel-field" id="ManagementRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Management (%):</label>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1Management",
                                                                                    'id'    => "P1Management",
                                                                                    'value' => ($reset) ? "" : set_value("P1Management"),
                                                                                    'class'  => "form-control number-field NumberOnly percentageField",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2Management",
                                                                                    'id'    => "P2Management",
                                                                                    'value' => ($reset) ? "" : set_value("P2Management"),
                                                                                    'class'  => "form-control number-field NumberOnly percentageField",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3Management",
                                                                                    'id'    => "P3Management",
                                                                                    'value' => ($reset) ? "" : set_value("P3Management"),
                                                                                    'class'  => "form-control number-field NumberOnly percentageField",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4Management",
                                                                                    'id'    => "P4Management",
                                                                                    'value' => ($reset) ? "" : set_value("P4Management"),
                                                                                    'class'  => "form-control number-field NumberOnly percentageField",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5Management",
                                                                                    'id'    => "P5Management",
                                                                                    'value' => ($reset) ? "" : set_value("P5Management"),
                                                                                    'class'  => "form-control number-field NumberOnly percentageField",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                    </div>
                                                                </div><!-- Management (%) -->
                                                                <div class="panel-field" id="TaxesRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Taxes (Annual):</label>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1Taxes",
                                                                                    'id'    => "P1Taxes",
                                                                                    'value' => ($reset) ? "" : set_value("P1Taxes"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1TaxesRent",
                                                                                    'id'    => "P1TaxesRent",
                                                                                    'value' => ($reset) ? "" : set_value("P1TaxesRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2Taxes",
                                                                                    'id'    => "P2Taxes",
                                                                                    'value' => ($reset) ? "" : set_value("P2Taxes"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2TaxesRent",
                                                                                    'id'    => "P2TaxesRent",
                                                                                    'value' => ($reset) ? "" : set_value("P2TaxesRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3Taxes",
                                                                                    'id'    => "P3Taxes",
                                                                                    'value' => ($reset) ? "" : set_value("P3Taxes"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3TaxesRent",
                                                                                    'id'    => "P3TaxesRent",
                                                                                    'value' => ($reset) ? "" : set_value("P3TaxesRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4Taxes",
                                                                                    'id'    => "P4Taxes",
                                                                                    'value' => ($reset) ? "" : set_value("P4Taxes"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4TaxesRent",
                                                                                    'id'    => "P4TaxesRent",
                                                                                    'value' => ($reset) ? "" : set_value("P4TaxesRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5Taxes",
                                                                                    'id'    => "P5Taxes",
                                                                                    'value' => ($reset) ? "" : set_value("P5Taxes"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5TaxesRent",
                                                                                    'id'    => "P5TaxesRent",
                                                                                    'value' => ($reset) ? "" : set_value("P5TaxesRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Taxes (Annual) -->
                                                                <div class="panel-field" id="InsuranceRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Insurance (Annual):</label>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1Ins",
                                                                                    'id'    => "P1Ins",
                                                                                    'value' => ($reset) ? "" : set_value("P1Ins"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1InsRent",
                                                                                    'id'    => "P1InsRent",
                                                                                    'value' => ($reset) ? "" : set_value("P1InsRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2Ins",
                                                                                    'id'    => "P2Ins",
                                                                                    'value' => ($reset) ? "" : set_value("P2Ins"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2InsRent",
                                                                                    'id'    => "P2InsRent",
                                                                                    'value' => ($reset) ? "" : set_value("P2InsRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3Ins",
                                                                                    'id'    => "P3Ins",
                                                                                    'value' => ($reset) ? "" : set_value("P3Ins"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3InsRent",
                                                                                    'id'    => "P3InsRent",
                                                                                    'value' => ($reset) ? "" : set_value("P3InsRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4Ins",
                                                                                    'id'    => "P4Ins",
                                                                                    'value' => ($reset) ? "" : set_value("P4Ins"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4InsRent",
                                                                                    'id'    => "P4InsRent",
                                                                                    'value' => ($reset) ? "" : set_value("P4InsRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5Ins",
                                                                                    'id'    => "P5Ins",
                                                                                    'value' => ($reset) ? "" : set_value("P5Ins"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5InsRent",
                                                                                    'id'    => "P5InsRent",
                                                                                    'value' => ($reset) ? "" : set_value("P5InsRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Insurance (Annual) -->
                                                                <div class="panel-field" id="MaintenanceRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Maintenance (Annual):</label>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1Maint",
                                                                                    'id'    => "P1Maint",
                                                                                    'value' => ($reset) ? "" : set_value("P1Maint"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1MaintRent",
                                                                                    'id'    => "P1MaintRent",
                                                                                    'value' => ($reset) ? "" : set_value("P1MaintRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2Maint",
                                                                                    'id'    => "P2Maint",
                                                                                    'value' => ($reset) ? "" : set_value("P2Maint"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2MaintRent",
                                                                                    'id'    => "P2MaintRent",
                                                                                    'value' => ($reset) ? "" : set_value("P2MaintRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3Maint",
                                                                                    'id'    => "P3Maint",
                                                                                    'value' => ($reset) ? "" : set_value("P3Maint"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                           </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3MaintRent",
                                                                                    'id'    => "P3MaintRent",
                                                                                    'value' => ($reset) ? "" : set_value("P3MaintRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4Maint",
                                                                                    'id'    => "P4Maint",
                                                                                    'value' => ($reset) ? "" : set_value("P4Maint"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4MaintRent",
                                                                                    'id'    => "P4MaintRent",
                                                                                    'value' => ($reset) ? "" : set_value("P4MaintRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5Maint",
                                                                                    'id'    => "P5Maint",
                                                                                    'value' => ($reset) ? "" : set_value("P5Maint"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5MaintRent",
                                                                                    'id'    => "P5MaintRent",
                                                                                    'value' => ($reset) ? "" : set_value("P5MaintRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Maintenance (Annual) -->
                                                                <div class="panel-field" id="OpExRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Other Operating Expenses (Annual):</label>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1OpEx",
                                                                                    'id'    => "P1OpEx",
                                                                                    'value' => ($reset) ? "" : set_value("P1OpEx"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1OpExRent",
                                                                                    'id'    => "P1OpExRent",
                                                                                    'value' => ($reset) ? "" : set_value("P1OpExRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2OpEx",
                                                                                    'id'    => "P2OpEx",
                                                                                    'value' => ($reset) ? "" : set_value("P2OpEx"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2OpExRent",
                                                                                    'id'    => "P2OpExRent",
                                                                                    'value' => ($reset) ? "" : set_value("P2OpExRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3OpEx",
                                                                                    'id'    => "P3OpEx",
                                                                                    'value' => ($reset) ? "" : set_value("P3OpEx"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3OpExRent",
                                                                                    'id'    => "P3OpExRent",
                                                                                    'value' => ($reset) ? "" : set_value("P3OpExRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4OpEx",
                                                                                    'id'    => "P4OpEx",
                                                                                    'value' => ($reset) ? "" : set_value("P4OpEx"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4OpExRent",
                                                                                    'id'    => "P4OpExRent",
                                                                                    'value' => ($reset) ? "" : set_value("P4OpExRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5OpEx",
                                                                                    'id'    => "P5OpEx",
                                                                                    'value' => ($reset) ? "" : set_value("P5OpEx"),
                                                                                    'class'  => "form-control number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => ""
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5OpExRent",
                                                                                    'id'    => "P5OpExRent",
                                                                                    'value' => ($reset) ? "" : set_value("P5OpExRent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Other Operating Expenses (Annual) -->
                                                                <div class="panel-field" id="RentPercentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2"></label>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1RentPercent",
                                                                                    'id'    => "P1RentPercent",
                                                                                    'value' => ($reset) ? "" : set_value("P1RentPercent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">	
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2RentPercent",
                                                                                    'id'    => "P2RentPercent",
                                                                                    'value' => ($reset) ? "" : set_value("P2RentPercent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3RentPercent",
                                                                                    'id'    => "P3RentPercent",
                                                                                    'value' => ($reset) ? "" : set_value("P3RentPercent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4RentPercent",
                                                                                    'id'    => "P4RentPercent",
                                                                                    'value' => ($reset) ? "" : set_value("P4RentPercent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5RentPercent",
                                                                                    'id'    => "P5RentPercent",
                                                                                    'value' => ($reset) ? "" : set_value("P5RentPercent"),
                                                                                    'class'  => "form-control number-field percentageField",
                                                                                    "readonly" => "readonly",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- $ of Rent -->
                                                                
                                                                <div class="panel-field" id="NOIRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">NOI (Monthly):</label>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1NOI",
                                                                                    'id'    => "P1NOI",
                                                                                    'value' => ($reset) ? "" : set_value("P1NOI"),
                                                                                    'class'  => "form-control number-field",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "",
                                                                                    "readonly" => "readonly"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2NOI",
                                                                                    'id'    => "P2NOI",
                                                                                    'value' => ($reset) ? "" : set_value("P2NOI"),
                                                                                    'class'  => "form-control number-field",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "",
                                                                                    "readonly" => "readonly"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3NOI",
                                                                                    'id'    => "P3NOI",
                                                                                    'value' => ($reset) ? "" : set_value("P3NOI"),
                                                                                    'class'  => "form-control number-field",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "",
                                                                                    "readonly" => "readonly"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4NOI",
                                                                                    'id'    => "P4NOI",
                                                                                    'value' => ($reset) ? "" : set_value("P4NOI"),
                                                                                    'class'  => "form-control number-field",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "",
                                                                                    "readonly" => "readonly"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5NOI",
                                                                                    'id'    => "P5NOI",
                                                                                    'value' => ($reset) ? "" : set_value("P5NOI"),
                                                                                    'class'  => "form-control number-field",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "",
                                                                                    "readonly" => "readonly"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                    </div>
                                                                </div><!-- NOI (Monthly) -->
                                                                <div class="panel-field" id="MortPymtRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Mortgage Payment (Monthly):</label>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P1MortPymt",
                                                                                    'id'    => "P1MortPymt",
                                                                                    'value' => ($reset) ? "" : set_value("P1MortPymt"),
                                                                                    'class'  => "form-control number-field",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "",
                                                                                    "readonly" => "readonly"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P2MortPymt",
                                                                                    'id'    => "P2MortPymt",
                                                                                    'value' => ($reset) ? "" : set_value("P2MortPymt"),
                                                                                    'class'  => "form-control number-field",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "",
                                                                                    "readonly" => "readonly"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P3MortPymt",
                                                                                    'id'    => "P3MortPymt",
                                                                                    'value' => ($reset) ? "" : set_value("P3MortPymt"),
                                                                                    'class'  => "form-control number-field",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "",
                                                                                    "readonly" => "readonly"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P4MortPymt",
                                                                                    'id'    => "P4MortPymt",
                                                                                    'value' => ($reset) ? "" : set_value("P4MortPymt"),
                                                                                    'class'  => "form-control number-field",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "",
                                                                                    "readonly" => "readonly"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                        <div class="col-xs-1">
                                                                            <div class="row">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "P5MortPymt",
                                                                                    'id'    => "P5MortPymt",
                                                                                    'value' => ($reset) ? "" : set_value("P5MortPymt"),
                                                                                    'class'  => "form-control number-field",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "",
                                                                                    "readonly" => "readonly"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                    </div>
                                                                </div><!-- Mortgage Payment (Monthly) -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide loading" />
                                                                    <button type="button" class="btn btn-sm btn-default on_prev_section trigger-bar" data-rel="33">BACK</button>
                                                                    <button type="button" class="btn btn-sm btn-primary on_save_event trigger-bar" data-rel="100">SAVE</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="analyser_extra_details" />
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>                                                
                                                    </div>
                                                    
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>