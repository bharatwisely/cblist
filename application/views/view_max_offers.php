<style type="text/css">.portfolio-desc-sec p{margin:0;}</style>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
		                <h2>Max Offers</h2>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    	<?php if ( !$proposal ) { ?>
                        	<a href="<?=base_url('pages/proposal-writer')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to post your first proposal."><i class="fa fa-plus-square-o"></i> Create Proposal</a>
                        <?php } else { ?>
                        	<!-- No action needed -->
                        <?php } ?>
                    </div>
                </div>
                
                <div class="page-content">
                	<?php if ( !$proposal ) { ?>
                        <div class="alert alert-warning" role="alert">
                        	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-info-circle"></i> Now you have to create your proposals. For creating your first proposal click the above '<a href="<?=base_url('pages/proposal-writer')?>/"><u>Create Proposal</u></a>' botton.&nbsp;&nbsp;&nbsp; 
                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Proposal Tutorial">
                                <span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                            </a>
                        </div>
                    <?php } else { ?>
                        <!-- No action needed -->
                    <?php } ?>
                    
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-file-text-o"></i> Offer List</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#offer_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                    <h3 class="box-title pull-right">
                                        <a href="<?=base_url('pages/offer-calculator')?>/" class="action-btn" data-toggle="tooltip" data-placement="top" title="New Offer">
                                        	<i class="fa fa-plus-square-o"></i> New
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
									<?php if ( $this->session->userdata('offer_deleted') ) { ?>
                                        <div class="alert alert-success" role="alert" style="margin-bottom:10px;">
                                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-info-circle"></i> Max offer deleted!
                                        </div>
                                    <?php } ?>
									<?php if ( $offers ) { $i = 0; ?>
                                        <?php foreach ( $offers as $public ) {
                                            if ( $i % 2 == 0 ) { $bg = '#F8F8F8'; } else { $bg = ''; } ?>
                                            <div class="container-fluid proposal-row" style="background-color:<?php echo $bg; ?>;">
                                                <div class="col-xs-12">
                                                    <div class="row">
                                                        <div class="portfolio-desc-sec">
                                                            <div class="col-xs-4">
                                                                <p><strong><?=$public->address_name?></strong></p>
                                                            </div>
                                                            <div class="col-xs-4">
                                                                <p>MAX OFFER: <strong>$<?=number_format($public->max_offer, 2)?></strong></p>
                                                            </div>
                                                            <?php $propertyId = $public->PropertyID; ?>
                                                            <div class="col-xs-4 text-right">
                                                                <a class="action-btn" href="<?=base_url('pages/edit-offer/' . $propertyId . '/MaxOffers')?>/" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                    <button type="button" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
                                                                </a>
                                                                <a class="action-btn" href="<?=base_url('pages/print-report/' . $propertyId . '/MaxOffers')?>/" data-toggle="tooltip" data-placement="top" title="Reports" target="_blank">
                                                                    <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></button>
                                                                </a>
                                                                <a class="action-btn" onclick="return confirm('Are you want to delete this offer?');" href="<?=base_url('pages/delete-offer/' . $propertyId . '/MaxOffers')?>/" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                    <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php $i++; } ?>
                                    <?php } else { ?>
                                        <p>You have not calculate any max offer yet. Calculate your first max offer from <a href="<?=base_url('pages/offer-calculator')?>/">here</a>.</p>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>