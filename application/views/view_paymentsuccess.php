<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
			<input type="hidden" id="paymentsuccess_flag" value="true" />
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <h2>Payment Status</h2>
                    </div>
                </div>
                
            	<div class="page-content">
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-user"></i> Payment Status</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#payment_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
									<div id="response_demo" style="display:none;"></div>
                                    <div class="col-xs-2 text-center">
                                        <span class="sucess_image" style="display:none;">
                                            <img id="payment_stat" src="<?=base_url('assets/images/success_payment.png')?>" alt="success" /><br /><br />
                                        </span>
                                        <span class="pending_image">
                                            <img id="payment_stat" src="<?=base_url('assets/images/pending_payment.png')?>" alt="pending" /><br /><br />
                                        </span>
                                    </div>
                                    <div class="col-xs-10">                                   
                                        <div class="fields-panel">
                                            <div class="panel-field">
                                                <div class="profile-data" style="display:block;">
                                                    <h3 class="success_header" style="display:none;">Congratulations, Your Payment is Successful !!!</h3>
                                                    <h3 class="pending_header">Please Wait, Your Payment is Pending !!!</h3>
                                                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages of Lorem Ipsum.</p>
                                                </div>
                                            </div>
                                            <div class="panel-field transaction_data" style="display:none;">
                                                <div class="profile-data">
                                                    <label>Your Transaction Details: </label>
                                                    <div class="row">
                                                        <div class="col-xs-3">
                                                            <strong>Subscription Id</strong>
                                                            <p id="subscription_id"></p>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <strong>Subscription Date</strong>
                                                            <p id="subscription_date"></p>
                                                        </div>
                                                        <div class="col-xs-3">
                                                            <strong>Your Paypal Email</strong>
                                                            <p id="subscription_email"></p>
                                                        </div>
                                                        <div class="col-xs-3 text-right">
                                                            <strong>Subscription Amount</strong>
                                                            <p id="subscription_amount"></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>	
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                    </footer>
            </aside>
        </div>
    </div>
</section>