<!-- Rehub Budget Refi Analysis Modal -->
<div class="modal fade" id="RefiRehubModal" tabindex="-1" role="dialog" aria-labelledby="RefiRehubModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header text-center">
                <h4 class="modal-title" id="RefiRehubModalLabel">
                	<span id="RefiTitle"></span> Scope of Work and Budget
                    <a class="modal-help" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body">
            	<?php $attributes = array('class' => 'form-horizontal', 'name' => 'RefiBudgetForm'); ?>
				<?=form_open(base_url('pages/proposal-writer') . '/', $attributes)?>
                    
                    <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                    <div class="row">
                        <div class="col-lg-12 assump-box">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="panel-field" id="RefiRehabBudgetMethodRow">
                                            <label class="col-lg-8">1. How do you want to enter your Holding Costs?</label>
                                            <div class="col-lg-4">
                                                <?php 
                                                $options = array(
                                                    'Detailed Input'  => 'Detailed Input',
                                                    'Quick Lump Sum'  => 'Quick Lump Sum',
                                                );
                                                $attrb = 'id="RefiRehabBudgetMethod" class="form-control"'; ?>
                                                <?=form_dropdown('RefiRehabBudgetMethod', $options, 'Detailed Input', $attrb)?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="row">
                                        <div class="panel-field" id="RefiRehabDrawSelectionRow">
                                            <label class="col-lg-5">2. Overide:</label>
                                            <div class="col-lg-7">
                                                <?php 
                                                $options = array(
                                                    'Fund Rehab in Draws'  => 'Fund Rehab in Draws',
                                                    'Fund Rehab at Closing' => 'Fund Rehab at Closing',
                                                );
                                                $attrb = 'id="RefiRehabDrawSelection" class="form-control"'; ?>
                                                <?=form_dropdown('RefiRehabDrawSelection', $options, 'Fund Rehab in Draws', $attrb)?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <hr />
                                    <div class="row">
                                        <div class="panel-field" id="LumpSumRefiBudgetRow" style="display:none;">
                                            <label class="col-lg-6">Enter Lump Sum Budget Below:</label>
                                            <div class="col-lg-6">
                                                <?php 
                                                $field = array(
                                                    'name'  => "LumpSumRefiBudget",
                                                    'id'    => "LumpSumRefiBudget",
                                                    'value' => ($reset) ? "" : set_value("LumpSumRefiBudget"),
                                                    'class'	=> 'form-control NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">    
                        <div class="col-lg-12">
                            <div class="panel-field" id="DetailedInputRefiBudgetRow">
                                <table width="100%" class="table table-left" id="RefiTable">
                                    <thead>
                                        <tr>
                                            <th>General Conditions:</th>
                                            <th>Details/Notes</th>
                                            <th>Sq Ft</th>
                                            <th>Quantity</th>
                                            <th>Rate</th>
                                            <th>Bid 1</th>
                                            <th>Bid 2</th>
                                            <th>Bid 3</th>
                                            <th width="100" bgcolor="#007C30" style="color:#fff;"><strong>BUDGET</strong></th>
                                            <th>Month to be Paid</th>
                                            <th width="80">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Demo</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesDemo",
                                                    'id'    => "DetailsNotesDemo",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesDemo"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtDemo",
                                                    'id'    => "SqFtDemo",
                                                    'value' => ($reset) ? "" : set_value("SqFtDemo"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityDemo",
                                                    'id'    => "QuantityDemo",
                                                    'value' => ($reset) ? "" : set_value("QuantityDemo"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateDemo",
                                                    'id'    => "RateDemo",
                                                    'value' => ($reset) ? "" : set_value("RateDemo"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Demo",
                                                    'id'    => "Bid1Demo",
                                                    'value' => ($reset) ? "" : set_value("Bid1Demo"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Demo",
                                                    'id'    => "Bid2Demo",
                                                    'value' => ($reset) ? "" : set_value("Bid2Demo"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Demo",
                                                    'id'    => "Bid3Demo",
                                                    'value' => ($reset) ? "" : set_value("Bid3Demo"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetDemo",
                                                    'id'    => "BudgetDemo",
                                                    'value' => ($reset) ? "" : set_value("BudgetDemo"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidDemo" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidDemo', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Architectural fees</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesArchitectural",
                                                    'id'    => "DetailsNotesArchitectural",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesArchitectural"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtArchitectural",
                                                    'id'    => "SqFtArchitectural",
                                                    'value' => ($reset) ? "" : set_value("SqFtArchitectural"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityArchitectural",
                                                    'id'    => "QuantityArchitectural",
                                                    'value' => ($reset) ? "" : set_value("QuantityArchitectural"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateArchitectural",
                                                    'id'    => "RateArchitectural",
                                                    'value' => ($reset) ? "" : set_value("RateArchitectural"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Architectural",
                                                    'id'    => "Bid1Architectural",
                                                    'value' => ($reset) ? "" : set_value("Bid1Architectural"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Architectural",
                                                    'id'    => "Bid2Architectural",
                                                    'value' => ($reset) ? "" : set_value("Bid2Architectural"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Architectural",
                                                    'id'    => "Bid3Architectural",
                                                    'value' => ($reset) ? "" : set_value("Bid3Architectural"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetArchitectural",
                                                    'id'    => "BudgetArchitectural",
                                                    'value' => ($reset) ? "" : set_value("BudgetArchitectural"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidArchitectural" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidArchitectural', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Master Building Permit</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesMasterBuildingPermit",
                                                    'id'    => "DetailsNotesMasterBuildingPermit",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesMasterBuildingPermit"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtMasterBuildingPermit",
                                                    'id'    => "SqFtMasterBuildingPermit",
                                                    'value' => ($reset) ? "" : set_value("SqFtMasterBuildingPermit"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityMasterBuildingPermit",
                                                    'id'    => "QuantityMasterBuildingPermit",
                                                    'value' => ($reset) ? "" : set_value("QuantityMasterBuildingPermit"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateMasterBuildingPermit",
                                                    'id'    => "RateMasterBuildingPermit",
                                                    'value' => ($reset) ? "" : set_value("RateMasterBuildingPermit"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1MasterBuildingPermit",
                                                    'id'    => "Bid1MasterBuildingPermit",
                                                    'value' => ($reset) ? "" : set_value("Bid1MasterBuildingPermit"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2MasterBuildingPermit",
                                                    'id'    => "Bid2MasterBuildingPermit",
                                                    'value' => ($reset) ? "" : set_value("Bid2MasterBuildingPermit"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3MasterBuildingPermit",
                                                    'id'    => "Bid3MasterBuildingPermit",
                                                    'value' => ($reset) ? "" : set_value("Bid3MasterBuildingPermit"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetMasterBuildingPermit",
                                                    'id'    => "BudgetMasterBuildingPermit",
                                                    'value' => ($reset) ? "" : set_value("BudgetMasterBuildingPermit"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidMasterBuildingPermit" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidMasterBuildingPermit', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="11">Trade Permits:</td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;Plumbing</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesPlumbing",
                                                    'id'    => "DetailsNotesPlumbing",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesPlumbing"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtPlumbing",
                                                    'id'    => "SqFtPlumbing",
                                                    'value' => ($reset) ? "" : set_value("SqFtPlumbing"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityPlumbing",
                                                    'id'    => "QuantityPlumbing",
                                                    'value' => ($reset) ? "" : set_value("QuantityPlumbing"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RatePlumbing",
                                                    'id'    => "RatePlumbing",
                                                    'value' => ($reset) ? "" : set_value("RatePlumbing"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Plumbing",
                                                    'id'    => "Bid1Plumbing",
                                                    'value' => ($reset) ? "" : set_value("Bid1Plumbing"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Plumbing",
                                                    'id'    => "Bid2Plumbing",
                                                    'value' => ($reset) ? "" : set_value("Bid2Plumbing"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Plumbing",
                                                    'id'    => "Bid3Plumbing",
                                                    'value' => ($reset) ? "" : set_value("Bid3Plumbing"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetPlumbing",
                                                    'id'    => "BudgetPlumbing",
                                                    'value' => ($reset) ? "" : set_value("BudgetPlumbing"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidPlumbing" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidPlumbing', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;Electrical</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesElectrical",
                                                    'id'    => "DetailsNotesElectrical",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesElectrical"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtElectrical",
                                                    'id'    => "SqFtElectrical",
                                                    'value' => ($reset) ? "" : set_value("SqFtElectrical"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityElectrical",
                                                    'id'    => "QuantityElectrical",
                                                    'value' => ($reset) ? "" : set_value("QuantityElectrical"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateElectrical",
                                                    'id'    => "RateElectrical",
                                                    'value' => ($reset) ? "" : set_value("RateElectrical"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Electrical",
                                                    'id'    => "Bid1Electrical",
                                                    'value' => ($reset) ? "" : set_value("Bid1Electrical"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Electrical",
                                                    'id'    => "Bid2Electrical",
                                                    'value' => ($reset) ? "" : set_value("Bid2Electrical"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Electrical",
                                                    'id'    => "Bid3Electrical",
                                                    'value' => ($reset) ? "" : set_value("Bid3Electrical"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetElectrical",
                                                    'id'    => "BudgetElectrical",
                                                    'value' => ($reset) ? "" : set_value("BudgetElectrical"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidElectrical" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidElectrical', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;&nbsp;HVAC</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesHVAC",
                                                    'id'    => "DetailsNotesHVAC",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesHVAC"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtHVAC",
                                                    'id'    => "SqFtHVAC",
                                                    'value' => ($reset) ? "" : set_value("SqFtHVAC"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityHVAC",
                                                    'id'    => "QuantityHVAC",
                                                    'value' => ($reset) ? "" : set_value("QuantityHVAC"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateHVAC",
                                                    'id'    => "RateHVAC",
                                                    'value' => ($reset) ? "" : set_value("RateHVAC"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1HVAC",
                                                    'id'    => "Bid1HVAC",
                                                    'value' => ($reset) ? "" : set_value("Bid1HVAC"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2HVAC",
                                                    'id'    => "Bid2HVAC",
                                                    'value' => ($reset) ? "" : set_value("Bid2HVAC"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3HVAC",
                                                    'id'    => "Bid3HVAC",
                                                    'value' => ($reset) ? "" : set_value("Bid3HVAC"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetHVAC",
                                                    'id'    => "BudgetHVAC",
                                                    'value' => ($reset) ? "" : set_value("BudgetHVAC"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidHVAC" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidHVAC', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Dumpster</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesDumpster",
                                                    'id'    => "DetailsNotesDumpster",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesDumpster"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtDumpster",
                                                    'id'    => "SqFtDumpster",
                                                    'value' => ($reset) ? "" : set_value("SqFtDumpster"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityDumpster",
                                                    'id'    => "QuantityDumpster",
                                                    'value' => ($reset) ? "" : set_value("QuantityDumpster"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateDumpster",
                                                    'id'    => "RateDumpster",
                                                    'value' => ($reset) ? "" : set_value("RateDumpster"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Dumpster",
                                                    'id'    => "Bid1Dumpster",
                                                    'value' => ($reset) ? "" : set_value("Bid1Dumpster"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Dumpster",
                                                    'id'    => "Bid2Dumpster",
                                                    'value' => ($reset) ? "" : set_value("Bid2Dumpster"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Dumpster",
                                                    'id'    => "Bid3Dumpster",
                                                    'value' => ($reset) ? "" : set_value("Bid3Dumpster"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetDumpster",
                                                    'id'    => "BudgetDumpster",
                                                    'value' => ($reset) ? "" : set_value("BudgetDumpster"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidDumpster" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidDumpster', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Other</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesOther",
                                                    'id'    => "DetailsNotesOther",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesOther"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtOther",
                                                    'id'    => "SqFtOther",
                                                    'value' => ($reset) ? "" : set_value("SqFtOther"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityOther",
                                                    'id'    => "QuantityOther",
                                                    'value' => ($reset) ? "" : set_value("QuantityOther"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateOther",
                                                    'id'    => "RateOther",
                                                    'value' => ($reset) ? "" : set_value("RateOther"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Other",
                                                    'id'    => "Bid1Other",
                                                    'value' => ($reset) ? "" : set_value("Bid1Other"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Other",
                                                    'id'    => "Bid2Other",
                                                    'value' => ($reset) ? "" : set_value("Bid2Other"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Other",
                                                    'id'    => "Bid3Other",
                                                    'value' => ($reset) ? "" : set_value("Bid3Other"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetOther",
                                                    'id'    => "BudgetOther",
                                                    'value' => ($reset) ? "" : set_value("BudgetOther"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidOther" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidOther', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="11"><strong><u>Interior Work:</u></strong></td>
                                        </tr>
                                        <tr>
                                            <td>Wall Framing</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesWallFraming",
                                                    'id'    => "DetailsNotesWallFraming",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesWallFraming"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtWallFraming",
                                                    'id'    => "SqFtWallFraming",
                                                    'value' => ($reset) ? "" : set_value("SqFtWallFraming"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityWallFraming",
                                                    'id'    => "QuantityWallFraming",
                                                    'value' => ($reset) ? "" : set_value("QuantityWallFraming"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateWallFraming",
                                                    'id'    => "RateWallFraming",
                                                    'value' => ($reset) ? "" : set_value("RateWallFraming"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1WallFraming",
                                                    'id'    => "Bid1WallFraming",
                                                    'value' => ($reset) ? "" : set_value("Bid1WallFraming"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2WallFraming",
                                                    'id'    => "Bid2WallFraming",
                                                    'value' => ($reset) ? "" : set_value("Bid2WallFraming"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3WallFraming",
                                                    'id'    => "Bid3WallFraming",
                                                    'value' => ($reset) ? "" : set_value("Bid3WallFraming"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetWallFraming",
                                                    'id'    => "BudgetWallFraming",
                                                    'value' => ($reset) ? "" : set_value("BudgetWallFraming"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidWallFraming" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidWallFraming', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Floor Framing</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesFloorFraming",
                                                    'id'    => "DetailsNotesFloorFraming",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesFloorFraming"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtFloorFraming",
                                                    'id'    => "SqFtFloorFraming",
                                                    'value' => ($reset) ? "" : set_value("SqFtFloorFraming"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityFloorFraming",
                                                    'id'    => "QuantityFloorFraming",
                                                    'value' => ($reset) ? "" : set_value("QuantityFloorFraming"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateFloorFraming",
                                                    'id'    => "RateFloorFraming",
                                                    'value' => ($reset) ? "" : set_value("RateFloorFraming"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1FloorFraming",
                                                    'id'    => "Bid1FloorFraming",
                                                    'value' => ($reset) ? "" : set_value("Bid1FloorFraming"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2FloorFraming",
                                                    'id'    => "Bid2FloorFraming",
                                                    'value' => ($reset) ? "" : set_value("Bid2FloorFraming"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3FloorFraming",
                                                    'id'    => "Bid3FloorFraming",
                                                    'value' => ($reset) ? "" : set_value("Bid3FloorFraming"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetFloorFraming",
                                                    'id'    => "BudgetFloorFraming",
                                                    'value' => ($reset) ? "" : set_value("BudgetFloorFraming"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidFloorFraming" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidFloorFraming', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Ceiling Framing</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesCeilingFraming",
                                                    'id'    => "DetailsNotesCeilingFraming",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesCeilingFraming"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtCeilingFraming",
                                                    'id'    => "SqFtCeilingFraming",
                                                    'value' => ($reset) ? "" : set_value("SqFtCeilingFraming"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityCeilingFraming",
                                                    'id'    => "QuantityCeilingFraming",
                                                    'value' => ($reset) ? "" : set_value("QuantityCeilingFraming"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateCeilingFraming",
                                                    'id'    => "RateCeilingFraming",
                                                    'value' => ($reset) ? "" : set_value("RateCeilingFraming"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1CeilingFraming",
                                                    'id'    => "Bid1CeilingFraming",
                                                    'value' => ($reset) ? "" : set_value("Bid1CeilingFraming"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2CeilingFraming",
                                                    'id'    => "Bid2CeilingFraming",
                                                    'value' => ($reset) ? "" : set_value("Bid2CeilingFraming"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3CeilingFraming",
                                                    'id'    => "Bid3CeilingFraming",
                                                    'value' => ($reset) ? "" : set_value("Bid3CeilingFraming"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetCeilingFraming",
                                                    'id'    => "BudgetCeilingFraming",
                                                    'value' => ($reset) ? "" : set_value("BudgetCeilingFraming"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidCeilingFraming" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidCeilingFraming', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Electrical</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesInteriorElectrical",
                                                    'id'    => "DetailsNotesInteriorElectrical",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesInteriorElectrical"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtInteriorElectrical",
                                                    'id'    => "SqFtInteriorElectrical",
                                                    'value' => ($reset) ? "" : set_value("SqFtInteriorElectrical"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityInteriorElectrical",
                                                    'id'    => "QuantityInteriorElectrical",
                                                    'value' => ($reset) ? "" : set_value("QuantityInteriorElectrical"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateInteriorElectrical",
                                                    'id'    => "RateInteriorElectrical",
                                                    'value' => ($reset) ? "" : set_value("RateInteriorElectrical"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1InteriorElectrical",
                                                    'id'    => "Bid1InteriorElectrical",
                                                    'value' => ($reset) ? "" : set_value("Bid1InteriorElectrical"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2InteriorElectrical",
                                                    'id'    => "Bid2InteriorElectrical",
                                                    'value' => ($reset) ? "" : set_value("Bid2InteriorElectrical"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3InteriorElectrical",
                                                    'id'    => "Bid3InteriorElectrical",
                                                    'value' => ($reset) ? "" : set_value("Bid3InteriorElectrical"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetInteriorElectrical",
                                                    'id'    => "BudgetInteriorElectrical",
                                                    'value' => ($reset) ? "" : set_value("BudgetInteriorElectrical"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidInteriorElectrical" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidInteriorElectrical', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Plumbing</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesInteriorPlumbing",
                                                    'id'    => "DetailsNotesInteriorPlumbing",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesInteriorPlumbing"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtInteriorPlumbing",
                                                    'id'    => "SqFtInteriorPlumbing",
                                                    'value' => ($reset) ? "" : set_value("SqFtInteriorPlumbing"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityInteriorPlumbing",
                                                    'id'    => "QuantityInteriorPlumbing",
                                                    'value' => ($reset) ? "" : set_value("QuantityInteriorPlumbing"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateInteriorPlumbing",
                                                    'id'    => "RateInteriorPlumbing",
                                                    'value' => ($reset) ? "" : set_value("RateInteriorPlumbing"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1InteriorPlumbing",
                                                    'id'    => "Bid1InteriorPlumbing",
                                                    'value' => ($reset) ? "" : set_value("Bid1InteriorPlumbing"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2InteriorPlumbing",
                                                    'id'    => "Bid2InteriorPlumbing",
                                                    'value' => ($reset) ? "" : set_value("Bid2InteriorPlumbing"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3InteriorPlumbing",
                                                    'id'    => "Bid3InteriorPlumbing",
                                                    'value' => ($reset) ? "" : set_value("Bid3InteriorPlumbing"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetInteriorPlumbing",
                                                    'id'    => "BudgetInteriorPlumbing",
                                                    'value' => ($reset) ? "" : set_value("BudgetInteriorPlumbing"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidInteriorPlumbing" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidInteriorPlumbing', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>HVAC</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesInteriorHVAC",
                                                    'id'    => "DetailsNotesInteriorHVAC",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesInteriorHVAC"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtInteriorHVAC",
                                                    'id'    => "SqFtInteriorHVAC",
                                                    'value' => ($reset) ? "" : set_value("SqFtInteriorHVAC"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityInteriorHVAC",
                                                    'id'    => "QuantityInteriorHVAC",
                                                    'value' => ($reset) ? "" : set_value("QuantityInteriorHVAC"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateInteriorHVAC",
                                                    'id'    => "RateInteriorHVAC",
                                                    'value' => ($reset) ? "" : set_value("RateInteriorHVAC"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1InteriorHVAC",
                                                    'id'    => "Bid1InteriorHVAC",
                                                    'value' => ($reset) ? "" : set_value("Bid1InteriorHVAC"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2InteriorHVAC",
                                                    'id'    => "Bid2InteriorHVAC",
                                                    'value' => ($reset) ? "" : set_value("Bid2InteriorHVAC"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3InteriorHVAC",
                                                    'id'    => "Bid3InteriorHVAC",
                                                    'value' => ($reset) ? "" : set_value("Bid3InteriorHVAC"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetInteriorHVAC",
                                                    'id'    => "BudgetInteriorHVAC",
                                                    'value' => ($reset) ? "" : set_value("BudgetInteriorHVAC"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidInteriorHVAC" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
        
                                                <?=form_dropdown('MonthPaidInteriorHVAC', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Flooring</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesFlooring",
                                                    'id'    => "DetailsNotesFlooring",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesFlooring"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtFlooring",
                                                    'id'    => "SqFtFlooring",
                                                    'value' => ($reset) ? "" : set_value("SqFtFlooring"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityFlooring",
                                                    'id'    => "QuantityFlooring",
                                                    'value' => ($reset) ? "" : set_value("QuantityFlooring"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateFlooring",
                                                    'id'    => "RateFlooring",
                                                    'value' => ($reset) ? "" : set_value("RateFlooring"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Flooring",
                                                    'id'    => "Bid1Flooring",
                                                    'value' => ($reset) ? "" : set_value("Bid1Flooring"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Flooring",
                                                    'id'    => "Bid2Flooring",
                                                    'value' => ($reset) ? "" : set_value("Bid2Flooring"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Flooring",
                                                    'id'    => "Bid3Flooring",
                                                    'value' => ($reset) ? "" : set_value("Bid3Flooring"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetFlooring",
                                                    'id'    => "BudgetFlooring",
                                                    'value' => ($reset) ? "" : set_value("BudgetFlooring"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidFlooring" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidFlooring', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Sheetrock</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesSheetrock",
                                                    'id'    => "DetailsNotesSheetrock",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesSheetrock"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtSheetrock",
                                                    'id'    => "SqFtSheetrock",
                                                    'value' => ($reset) ? "" : set_value("SqFtSheetrock"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantitySheetrock",
                                                    'id'    => "QuantitySheetrock",
                                                    'value' => ($reset) ? "" : set_value("QuantitySheetrock"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateSheetrock",
                                                    'id'    => "RateSheetrock",
                                                    'value' => ($reset) ? "" : set_value("RateSheetrock"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Sheetrock",
                                                    'id'    => "Bid1Sheetrock",
                                                    'value' => ($reset) ? "" : set_value("Bid1Sheetrock"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Sheetrock",
                                                    'id'    => "Bid2Sheetrock",
                                                    'value' => ($reset) ? "" : set_value("Bid2Sheetrock"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Sheetrock",
                                                    'id'    => "Bid3Sheetrock",
                                                    'value' => ($reset) ? "" : set_value("Bid3Sheetrock"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetSheetrock",
                                                    'id'    => "BudgetSheetrock",
                                                    'value' => ($reset) ? "" : set_value("BudgetSheetrock"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidSheetrock" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidSheetrock', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Windows</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesWindows",
                                                    'id'    => "DetailsNotesWindows",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesWindows"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtWindows",
                                                    'id'    => "SqFtWindows",
                                                    'value' => ($reset) ? "" : set_value("SqFtWindows"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityWindows",
                                                    'id'    => "QuantityWindows",
                                                    'value' => ($reset) ? "" : set_value("QuantityWindows"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateWindows",
                                                    'id'    => "RateWindows",
                                                    'value' => ($reset) ? "" : set_value("RateWindows"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Windows",
                                                    'id'    => "Bid1Windows",
                                                    'value' => ($reset) ? "" : set_value("Bid1Windows"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Windows",
                                                    'id'    => "Bid2Windows",
                                                    'value' => ($reset) ? "" : set_value("Bid2Windows"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Windows",
                                                    'id'    => "Bid3Windows",
                                                    'value' => ($reset) ? "" : set_value("Bid3Windows"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetWindows",
                                                    'id'    => "BudgetWindows",
                                                    'value' => ($reset) ? "" : set_value("BudgetWindows"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidWindows" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidWindows', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Interior Doors</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesInteriorDoors",
                                                    'id'    => "DetailsNotesInteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesInteriorDoors"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtInteriorDoors",
                                                    'id'    => "SqFtInteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("SqFtInteriorDoors"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityInteriorDoors",
                                                    'id'    => "QuantityInteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("QuantityInteriorDoors"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateInteriorDoors",
                                                    'id'    => "RateInteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("RateInteriorDoors"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1InteriorDoors",
                                                    'id'    => "Bid1InteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("Bid1InteriorDoors"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2InteriorDoors",
                                                    'id'    => "Bid2InteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("Bid2InteriorDoors"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3InteriorDoors",
                                                    'id'    => "Bid3InteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("Bid3InteriorDoors"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetInteriorDoors",
                                                    'id'    => "BudgetInteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("BudgetInteriorDoors"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidInteriorDoors" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidInteriorDoors', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Trim</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesTrim",
                                                    'id'    => "DetailsNotesTrim",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesTrim"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtTrim",
                                                    'id'    => "SqFtTrim",
                                                    'value' => ($reset) ? "" : set_value("SqFtTrim"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityTrim",
                                                    'id'    => "QuantityTrim",
                                                    'value' => ($reset) ? "" : set_value("QuantityTrim"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateTrim",
                                                    'id'    => "RateTrim",
                                                    'value' => ($reset) ? "" : set_value("RateTrim"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Trim",
                                                    'id'    => "Bid1Trim",
                                                    'value' => ($reset) ? "" : set_value("Bid1Trim"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Trim",
                                                    'id'    => "Bid2Trim",
                                                    'value' => ($reset) ? "" : set_value("Bid2Trim"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Trim",
                                                    'id'    => "Bid3Trim",
                                                    'value' => ($reset) ? "" : set_value("Bid3Trim"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetTrim",
                                                    'id'    => "BudgetTrim",
                                                    'value' => ($reset) ? "" : set_value("BudgetTrim"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidTrim" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidTrim', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Bathroom Vanities</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesBathroomVanities",
                                                    'id'    => "DetailsNotesBathroomVanities",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesBathroomVanities"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtBathroomVanities",
                                                    'id'    => "SqFtBathroomVanities",
                                                    'value' => ($reset) ? "" : set_value("SqFtBathroomVanities"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityBathroomVanities",
                                                    'id'    => "QuantityBathroomVanities",
                                                    'value' => ($reset) ? "" : set_value("QuantityBathroomVanities"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateBathroomVanities",
                                                    'id'    => "RateBathroomVanities",
                                                    'value' => ($reset) ? "" : set_value("RateBathroomVanities"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1BathroomVanities",
                                                    'id'    => "Bid1BathroomVanities",
                                                    'value' => ($reset) ? "" : set_value("Bid1BathroomVanities"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2BathroomVanities",
                                                    'id'    => "Bid2BathroomVanities",
                                                    'value' => ($reset) ? "" : set_value("Bid2BathroomVanities"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3BathroomVanities",
                                                    'id'    => "Bid3BathroomVanities",
                                                    'value' => ($reset) ? "" : set_value("Bid3BathroomVanities"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetBathroomVanities",
                                                    'id'    => "BudgetBathroomVanities",
                                                    'value' => ($reset) ? "" : set_value("BudgetBathroomVanities"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidBathroomVanities" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidBathroomVanities', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Bathroom Fixtures</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesBathroomFixtures",
                                                    'id'    => "DetailsNotesBathroomFixtures",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesBathroomFixtures"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtBathroomFixtures",
                                                    'id'    => "SqFtBathroomFixtures",
                                                    'value' => ($reset) ? "" : set_value("SqFtBathroomFixtures"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityBathroomFixtures",
                                                    'id'    => "QuantityBathroomFixtures",
                                                    'value' => ($reset) ? "" : set_value("QuantityBathroomFixtures"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateBathroomFixtures",
                                                    'id'    => "RateBathroomFixtures",
                                                    'value' => ($reset) ? "" : set_value("RateBathroomFixtures"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1BathroomFixtures",
                                                    'id'    => "Bid1BathroomFixtures",
                                                    'value' => ($reset) ? "" : set_value("Bid1BathroomFixtures"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2BathroomFixtures",
                                                    'id'    => "Bid2BathroomFixtures",
                                                    'value' => ($reset) ? "" : set_value("Bid2BathroomFixtures"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3BathroomFixtures",
                                                    'id'    => "Bid3BathroomFixtures",
                                                    'value' => ($reset) ? "" : set_value("Bid3BathroomFixtures"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetBathroomFixtures",
                                                    'id'    => "BudgetBathroomFixtures",
                                                    'value' => ($reset) ? "" : set_value("BudgetBathroomFixtures"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidBathroomFixtures" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidBathroomFixtures', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Kitchen Cabinets</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesKitchenCabinets",
                                                    'id'    => "DetailsNotesKitchenCabinets",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesKitchenCabinets"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtKitchenCabinets",
                                                    'id'    => "SqFtKitchenCabinets",
                                                    'value' => ($reset) ? "" : set_value("SqFtKitchenCabinets"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityKitchenCabinets",
                                                    'id'    => "QuantityKitchenCabinets",
                                                    'value' => ($reset) ? "" : set_value("QuantityKitchenCabinets"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateKitchenCabinets",
                                                    'id'    => "RateKitchenCabinets",
                                                    'value' => ($reset) ? "" : set_value("RateKitchenCabinets"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1KitchenCabinets",
                                                    'id'    => "Bid1KitchenCabinets",
                                                    'value' => ($reset) ? "" : set_value("Bid1KitchenCabinets"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2KitchenCabinets",
                                                    'id'    => "Bid2KitchenCabinets",
                                                    'value' => ($reset) ? "" : set_value("Bid2KitchenCabinets"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3KitchenCabinets",
                                                    'id'    => "Bid3KitchenCabinets",
                                                    'value' => ($reset) ? "" : set_value("Bid3KitchenCabinets"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetKitchenCabinets",
                                                    'id'    => "BudgetKitchenCabinets",
                                                    'value' => ($reset) ? "" : set_value("BudgetKitchenCabinets"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidKitchenCabinets" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidKitchenCabinets', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Labor to install Kitchen</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesLaborInstallKitchen",
                                                    'id'    => "DetailsNotesLaborInstallKitchen",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesLaborInstallKitchen"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtLaborInstallKitchen",
                                                    'id'    => "SqFtLaborInstallKitchen",
                                                    'value' => ($reset) ? "" : set_value("SqFtLaborInstallKitchen"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityLaborInstallKitchen",
                                                    'id'    => "QuantityLaborInstallKitchen",
                                                    'value' => ($reset) ? "" : set_value("QuantityLaborInstallKitchen"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateLaborInstallKitchen",
                                                    'id'    => "RateLaborInstallKitchen",
                                                    'value' => ($reset) ? "" : set_value("RateLaborInstallKitchen"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1LaborInstallKitchen",
                                                    'id'    => "Bid1LaborInstallKitchen",
                                                    'value' => ($reset) ? "" : set_value("Bid1LaborInstallKitchen"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2LaborInstallKitchen",
                                                    'id'    => "Bid2LaborInstallKitchen",
                                                    'value' => ($reset) ? "" : set_value("Bid2LaborInstallKitchen"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3LaborInstallKitchen",
                                                    'id'    => "Bid3LaborInstallKitchen",
                                                    'value' => ($reset) ? "" : set_value("Bid3LaborInstallKitchen"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetLaborInstallKitchen",
                                                    'id'    => "BudgetLaborInstallKitchen",
                                                    'value' => ($reset) ? "" : set_value("BudgetLaborInstallKitchen"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidLaborInstallKitchen" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidLaborInstallKitchen', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Floor Coverings 1</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesFloorCoverings1",
                                                    'id'    => "DetailsNotesFloorCoverings1",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesFloorCoverings1"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtFloorCoverings1",
                                                    'id'    => "SqFtFloorCoverings1",
                                                    'value' => ($reset) ? "" : set_value("SqFtFloorCoverings1"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityFloorCoverings1",
                                                    'id'    => "QuantityFloorCoverings1",
                                                    'value' => ($reset) ? "" : set_value("QuantityFloorCoverings1"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateFloorCoverings1",
                                                    'id'    => "RateFloorCoverings1",
                                                    'value' => ($reset) ? "" : set_value("RateFloorCoverings1"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1FloorCoverings1",
                                                    'id'    => "Bid1FloorCoverings1",
                                                    'value' => ($reset) ? "" : set_value("Bid1FloorCoverings1"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2FloorCoverings1",
                                                    'id'    => "Bid2FloorCoverings1",
                                                    'value' => ($reset) ? "" : set_value("Bid2FloorCoverings1"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3FloorCoverings1",
                                                    'id'    => "Bid3FloorCoverings1",
                                                    'value' => ($reset) ? "" : set_value("Bid3FloorCoverings1"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetFloorCoverings1",
                                                    'id'    => "BudgetFloorCoverings1",
                                                    'value' => ($reset) ? "" : set_value("BudgetFloorCoverings1"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidFloorCoverings1" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidFloorCoverings1', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Floor Coverings 2</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesFloorCoverings2",
                                                    'id'    => "DetailsNotesFloorCoverings2",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesFloorCoverings2"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtFloorCoverings2",
                                                    'id'    => "SqFtFloorCoverings2",
                                                    'value' => ($reset) ? "" : set_value("SqFtFloorCoverings2"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityFloorCoverings2",
                                                    'id'    => "QuantityFloorCoverings2",
                                                    'value' => ($reset) ? "" : set_value("QuantityFloorCoverings2"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateFloorCoverings2",
                                                    'id'    => "RateFloorCoverings2",
                                                    'value' => ($reset) ? "" : set_value("RateFloorCoverings2"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1FloorCoverings2",
                                                    'id'    => "Bid1FloorCoverings2",
                                                    'value' => ($reset) ? "" : set_value("Bid1FloorCoverings2"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2FloorCoverings2",
                                                    'id'    => "Bid2FloorCoverings2",
                                                    'value' => ($reset) ? "" : set_value("Bid2FloorCoverings2"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3FloorCoverings2",
                                                    'id'    => "Bid3FloorCoverings2",
                                                    'value' => ($reset) ? "" : set_value("Bid3FloorCoverings2"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetFloorCoverings2",
                                                    'id'    => "BudgetFloorCoverings2",
                                                    'value' => ($reset) ? "" : set_value("BudgetFloorCoverings2"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
        
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidFloorCoverings2" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidFloorCoverings2', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Floor Coverings 3</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesFloorCoverings3",
                                                    'id'    => "DetailsNotesFloorCoverings3",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesFloorCoverings3"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtFloorCoverings3",
                                                    'id'    => "SqFtFloorCoverings3",
                                                    'value' => ($reset) ? "" : set_value("SqFtFloorCoverings3"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityFloorCoverings3",
                                                    'id'    => "QuantityFloorCoverings3",
                                                    'value' => ($reset) ? "" : set_value("QuantityFloorCoverings3"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateFloorCoverings3",
                                                    'id'    => "RateFloorCoverings3",
                                                    'value' => ($reset) ? "" : set_value("RateFloorCoverings3"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1FloorCoverings3",
                                                    'id'    => "Bid1FloorCoverings3",
                                                    'value' => ($reset) ? "" : set_value("Bid1FloorCoverings3"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2FloorCoverings3",
                                                    'id'    => "Bid2FloorCoverings3",
                                                    'value' => ($reset) ? "" : set_value("Bid2FloorCoverings3"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3FloorCoverings3",
                                                    'id'    => "Bid3FloorCoverings3",
                                                    'value' => ($reset) ? "" : set_value("Bid3FloorCoverings3"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetFloorCoverings3",
                                                    'id'    => "BudgetFloorCoverings3",
                                                    'value' => ($reset) ? "" : set_value("BudgetFloorCoverings3"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidFloorCoverings3" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidFloorCoverings3', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Interior Painting</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesInteriorPainting",
                                                    'id'    => "DetailsNotesInteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesInteriorPainting"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtInteriorPainting",
                                                    'id'    => "SqFtInteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("SqFtInteriorPainting"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityInteriorPainting",
                                                    'id'    => "QuantityInteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("QuantityInteriorPainting"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateInteriorPainting",
                                                    'id'    => "RateInteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("RateInteriorPainting"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1InteriorPainting",
                                                    'id'    => "Bid1InteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("Bid1InteriorPainting"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2InteriorPainting",
                                                    'id'    => "Bid2InteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("Bid2InteriorPainting"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3InteriorPainting",
                                                    'id'    => "Bid3InteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("Bid3InteriorPainting"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetInteriorPainting",
                                                    'id'    => "BudgetInteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("BudgetInteriorPainting"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidInteriorPainting" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidInteriorPainting', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Light Fixtures</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesLightFixtures",
                                                    'id'    => "DetailsNotesLightFixtures",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesLightFixtures"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtLightFixtures",
                                                    'id'    => "SqFtLightFixtures",
                                                    'value' => ($reset) ? "" : set_value("SqFtLightFixtures"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityLightFixtures",
                                                    'id'    => "QuantityLightFixtures",
                                                    'value' => ($reset) ? "" : set_value("QuantityLightFixtures"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateLightFixtures",
                                                    'id'    => "RateLightFixtures",
                                                    'value' => ($reset) ? "" : set_value("RateLightFixtures"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1LightFixtures",
                                                    'id'    => "Bid1LightFixtures",
                                                    'value' => ($reset) ? "" : set_value("Bid1LightFixtures"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2LightFixtures",
                                                    'id'    => "Bid2LightFixtures",
                                                    'value' => ($reset) ? "" : set_value("Bid2LightFixtures"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3LightFixtures",
                                                    'id'    => "Bid3LightFixtures",
                                                    'value' => ($reset) ? "" : set_value("Bid3LightFixtures"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetLightFixtures",
                                                    'id'    => "BudgetLightFixtures",
                                                    'value' => ($reset) ? "" : set_value("BudgetLightFixtures"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidLightFixtures" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidLightFixtures', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Other Fixtures</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesOtherFixtures",
                                                    'id'    => "DetailsNotesOtherFixtures",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesOtherFixtures"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtOtherFixtures",
                                                    'id'    => "SqFtOtherFixtures",
                                                    'value' => ($reset) ? "" : set_value("SqFtOtherFixtures"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityOtherFixtures",
                                                    'id'    => "QuantityOtherFixtures",
                                                    'value' => ($reset) ? "" : set_value("QuantityOtherFixtures"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateOtherFixtures",
                                                    'id'    => "RateOtherFixtures",
                                                    'value' => ($reset) ? "" : set_value("RateOtherFixtures"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1OtherFixtures",
                                                    'id'    => "Bid1OtherFixtures",
                                                    'value' => ($reset) ? "" : set_value("Bid1OtherFixtures"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2OtherFixtures",
                                                    'id'    => "Bid2OtherFixtures",
                                                    'value' => ($reset) ? "" : set_value("Bid2OtherFixtures"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3OtherFixtures",
                                                    'id'    => "Bid3OtherFixtures",
                                                    'value' => ($reset) ? "" : set_value("Bid3OtherFixtures"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetOtherFixtures",
                                                    'id'    => "BudgetOtherFixtures",
                                                    'value' => ($reset) ? "" : set_value("BudgetOtherFixtures"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidOtherFixtures" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidOtherFixtures', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Appliances</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesAppliances",
                                                    'id'    => "DetailsNotesAppliances",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesAppliances"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtAppliances",
                                                    'id'    => "SqFtAppliances",
                                                    'value' => ($reset) ? "" : set_value("SqFtAppliances"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityAppliances",
                                                    'id'    => "QuantityAppliances",
                                                    'value' => ($reset) ? "" : set_value("QuantityAppliances"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateAppliances",
                                                    'id'    => "RateAppliances",
                                                    'value' => ($reset) ? "" : set_value("RateAppliances"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Appliances",
                                                    'id'    => "Bid1Appliances",
                                                    'value' => ($reset) ? "" : set_value("Bid1Appliances"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Appliances",
                                                    'id'    => "Bid2Appliances",
                                                    'value' => ($reset) ? "" : set_value("Bid2Appliances"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Appliances",
                                                    'id'    => "Bid3Appliances",
                                                    'value' => ($reset) ? "" : set_value("Bid3Appliances"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetAppliances",
                                                    'id'    => "BudgetAppliances",
                                                    'value' => ($reset) ? "" : set_value("BudgetAppliances"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidAppliances" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidAppliances', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Other Interior</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesOtherInterior",
                                                    'id'    => "DetailsNotesOtherInterior",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesOtherInterior"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtOtherInterior",
                                                    'id'    => "SqFtOtherInterior",
                                                    'value' => ($reset) ? "" : set_value("SqFtOtherInterior"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityOtherInterior",
                                                    'id'    => "QuantityOtherInterior",
                                                    'value' => ($reset) ? "" : set_value("QuantityOtherInterior"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateOtherInterior",
                                                    'id'    => "RateOtherInterior",
                                                    'value' => ($reset) ? "" : set_value("RateOtherInterior"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1OtherInterior",
                                                    'id'    => "Bid1OtherInterior",
                                                    'value' => ($reset) ? "" : set_value("Bid1OtherInterior"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2OtherInterior",
                                                    'id'    => "Bid2OtherInterior",
                                                    'value' => ($reset) ? "" : set_value("Bid2OtherInterior"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3OtherInterior",
                                                    'id'    => "Bid3OtherInterior",
                                                    'value' => ($reset) ? "" : set_value("Bid3OtherInterior"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetOtherInterior",
                                                    'id'    => "BudgetOtherInterior",
                                                    'value' => ($reset) ? "" : set_value("BudgetOtherInterior"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidOtherInterior" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidOtherInterior', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="11"><strong><u>Exterior Work:</u></strong></td>
                                        </tr>
                                        <tr>
                                            <td>Exterior Trim</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesExteriorTrim",
                                                    'id'    => "DetailsNotesExteriorTrim",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesExteriorTrim"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtExteriorTrim",
                                                    'id'    => "SqFtExteriorTrim",
                                                    'value' => ($reset) ? "" : set_value("SqFtExteriorTrim"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityExteriorTrim",
                                                    'id'    => "QuantityExteriorTrim",
                                                    'value' => ($reset) ? "" : set_value("QuantityExteriorTrim"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateExteriorTrim",
                                                    'id'    => "RateExteriorTrim",
                                                    'value' => ($reset) ? "" : set_value("RateExteriorTrim"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1ExteriorTrim",
                                                    'id'    => "Bid1ExteriorTrim",
                                                    'value' => ($reset) ? "" : set_value("Bid1ExteriorTrim"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2ExteriorTrim",
                                                    'id'    => "Bid2ExteriorTrim",
                                                    'value' => ($reset) ? "" : set_value("Bid2ExteriorTrim"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3ExteriorTrim",
                                                    'id'    => "Bid3ExteriorTrim",
                                                    'value' => ($reset) ? "" : set_value("Bid3ExteriorTrim"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetExteriorTrim",
                                                    'id'    => "BudgetExteriorTrim",
                                                    'value' => ($reset) ? "" : set_value("BudgetExteriorTrim"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidExteriorTrim" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidExteriorTrim', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Exterior Doors</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesExteriorDoors",
                                                    'id'    => "DetailsNotesExteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesExteriorDoors"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtExteriorDoors",
                                                    'id'    => "SqFtExteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("SqFtExteriorDoors"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityExteriorDoors",
                                                    'id'    => "QuantityExteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("QuantityExteriorDoors"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateExteriorDoors",
                                                    'id'    => "RateExteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("RateExteriorDoors"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1ExteriorDoors",
                                                    'id'    => "Bid1ExteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("Bid1ExteriorDoors"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2ExteriorDoors",
                                                    'id'    => "Bid2ExteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("Bid2ExteriorDoors"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3ExteriorDoors",
                                                    'id'    => "Bid3ExteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("Bid3ExteriorDoors"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetExteriorDoors",
                                                    'id'    => "BudgetExteriorDoors",
                                                    'value' => ($reset) ? "" : set_value("BudgetExteriorDoors"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidExteriorDoors" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidExteriorDoors', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Porches</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesPorches",
                                                    'id'    => "DetailsNotesPorches",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesPorches"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtPorches",
                                                    'id'    => "SqFtPorches",
                                                    'value' => ($reset) ? "" : set_value("SqFtPorches"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityPorches",
                                                    'id'    => "QuantityPorches",
                                                    'value' => ($reset) ? "" : set_value("QuantityPorches"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RatePorches",
                                                    'id'    => "RatePorches",
                                                    'value' => ($reset) ? "" : set_value("RatePorches"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Porches",
                                                    'id'    => "Bid1Porches",
                                                    'value' => ($reset) ? "" : set_value("Bid1Porches"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Porches",
                                                    'id'    => "Bid2Porches",
                                                    'value' => ($reset) ? "" : set_value("Bid2Porches"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Porches",
                                                    'id'    => "Bid3Porches",
                                                    'value' => ($reset) ? "" : set_value("Bid3Porches"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetPorches",
                                                    'id'    => "BudgetPorches",
                                                    'value' => ($reset) ? "" : set_value("BudgetPorches"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidPorches" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidPorches', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Siding</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesSiding",
                                                    'id'    => "DetailsNotesSiding",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesSiding"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtSiding",
                                                    'id'    => "SqFtSiding",
                                                    'value' => ($reset) ? "" : set_value("SqFtSiding"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantitySiding",
                                                    'id'    => "QuantitySiding",
                                                    'value' => ($reset) ? "" : set_value("QuantitySiding"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateSiding",
                                                    'id'    => "RateSiding",
                                                    'value' => ($reset) ? "" : set_value("RateSiding"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Siding",
                                                    'id'    => "Bid1Siding",
                                                    'value' => ($reset) ? "" : set_value("Bid1Siding"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Siding",
                                                    'id'    => "Bid2Siding",
                                                    'value' => ($reset) ? "" : set_value("Bid2Siding"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Siding",
                                                    'id'    => "Bid3Siding",
                                                    'value' => ($reset) ? "" : set_value("Bid3Siding"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetSiding",
                                                    'id'    => "BudgetSiding",
                                                    'value' => ($reset) ? "" : set_value("BudgetSiding"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidSiding" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidSiding', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Exterior Painting</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesExteriorPainting",
                                                    'id'    => "DetailsNotesExteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesExteriorPainting"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtExteriorPainting",
                                                    'id'    => "SqFtExteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("SqFtExteriorPainting"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityExteriorPainting",
                                                    'id'    => "QuantityExteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("QuantityExteriorPainting"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateExteriorPainting",
                                                    'id'    => "RateExteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("RateExteriorPainting"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1ExteriorPainting",
                                                    'id'    => "Bid1ExteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("Bid1ExteriorPainting"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2ExteriorPainting",
                                                    'id'    => "Bid2ExteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("Bid2ExteriorPainting"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3ExteriorPainting",
                                                    'id'    => "Bid3ExteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("Bid3ExteriorPainting"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetExteriorPainting",
                                                    'id'    => "BudgetExteriorPainting",
                                                    'value' => ($reset) ? "" : set_value("BudgetExteriorPainting"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidExteriorPainting" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidExteriorPainting', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Roof</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesRoof",
                                                    'id'    => "DetailsNotesRoof",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesRoof"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtRoof",
                                                    'id'    => "SqFtRoof",
                                                    'value' => ($reset) ? "" : set_value("SqFtRoof"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityRoof",
                                                    'id'    => "QuantityRoof",
                                                    'value' => ($reset) ? "" : set_value("QuantityRoof"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateRoof",
                                                    'id'    => "RateRoof",
                                                    'value' => ($reset) ? "" : set_value("RateRoof"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Roof",
                                                    'id'    => "Bid1Roof",
                                                    'value' => ($reset) ? "" : set_value("Bid1Roof"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Roof",
                                                    'id'    => "Bid2Roof",
                                                    'value' => ($reset) ? "" : set_value("Bid2Roof"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Roof",
                                                    'id'    => "Bid3Roof",
                                                    'value' => ($reset) ? "" : set_value("Bid3Roof"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetRoof",
                                                    'id'    => "BudgetRoof",
                                                    'value' => ($reset) ? "" : set_value("BudgetRoof"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidRoof" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidRoof', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Gutters, Downspouts</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesGuttersDownspouts",
                                                    'id'    => "DetailsNotesGuttersDownspouts",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesGuttersDownspouts"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtGuttersDownspouts",
                                                    'id'    => "SqFtGuttersDownspouts",
                                                    'value' => ($reset) ? "" : set_value("SqFtGuttersDownspouts"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityGuttersDownspouts",
                                                    'id'    => "QuantityGuttersDownspouts",
                                                    'value' => ($reset) ? "" : set_value("QuantityGuttersDownspouts"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateGuttersDownspouts",
                                                    'id'    => "RateGuttersDownspouts",
                                                    'value' => ($reset) ? "" : set_value("RateGuttersDownspouts"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1GuttersDownspouts",
                                                    'id'    => "Bid1GuttersDownspouts",
                                                    'value' => ($reset) ? "" : set_value("Bid1GuttersDownspouts"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2GuttersDownspouts",
                                                    'id'    => "Bid2GuttersDownspouts",
                                                    'value' => ($reset) ? "" : set_value("Bid2GuttersDownspouts"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3GuttersDownspouts",
                                                    'id'    => "Bid3GuttersDownspouts",
                                                    'value' => ($reset) ? "" : set_value("Bid3GuttersDownspouts"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetGuttersDownspouts",
                                                    'id'    => "BudgetGuttersDownspouts",
                                                    'value' => ($reset) ? "" : set_value("BudgetGuttersDownspouts"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidGuttersDownspouts" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidGuttersDownspouts', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Fencing</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesFencing",
                                                    'id'    => "DetailsNotesFencing",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesFencing"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtFencing",
                                                    'id'    => "SqFtFencing",
                                                    'value' => ($reset) ? "" : set_value("SqFtFencing"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityFencing",
                                                    'id'    => "QuantityFencing",
                                                    'value' => ($reset) ? "" : set_value("QuantityFencing"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateFencing",
                                                    'id'    => "RateFencing",
                                                    'value' => ($reset) ? "" : set_value("RateFencing"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Fencing",
                                                    'id'    => "Bid1Fencing",
                                                    'value' => ($reset) ? "" : set_value("Bid1Fencing"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Fencing",
                                                    'id'    => "Bid2Fencing",
                                                    'value' => ($reset) ? "" : set_value("Bid2Fencing"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Fencing",
                                                    'id'    => "Bid3Fencing",
                                                    'value' => ($reset) ? "" : set_value("Bid3Fencing"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetFencing",
                                                    'id'    => "BudgetFencing",
                                                    'value' => ($reset) ? "" : set_value("BudgetFencing"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidFencing" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidFencing', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Landscaping</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesLandscaping",
                                                    'id'    => "DetailsNotesLandscaping",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesLandscaping"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtLandscaping",
                                                    'id'    => "SqFtLandscaping",
                                                    'value' => ($reset) ? "" : set_value("SqFtLandscaping"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityLandscaping",
                                                    'id'    => "QuantityLandscaping",
                                                    'value' => ($reset) ? "" : set_value("QuantityLandscaping"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateLandscaping",
                                                    'id'    => "RateLandscaping",
                                                    'value' => ($reset) ? "" : set_value("RateLandscaping"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Landscaping",
                                                    'id'    => "Bid1Landscaping",
                                                    'value' => ($reset) ? "" : set_value("Bid1Landscaping"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Landscaping",
                                                    'id'    => "Bid2Landscaping",
                                                    'value' => ($reset) ? "" : set_value("Bid2Landscaping"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Landscaping",
                                                    'id'    => "Bid3Landscaping",
                                                    'value' => ($reset) ? "" : set_value("Bid3Landscaping"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetLandscaping",
                                                    'id'    => "BudgetLandscaping",
                                                    'value' => ($reset) ? "" : set_value("BudgetLandscaping"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidLandscaping" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidLandscaping', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Driveway, concrete work</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesDrivewayConcrete",
                                                    'id'    => "DetailsNotesDrivewayConcrete",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesDrivewayConcrete"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtDrivewayConcrete",
                                                    'id'    => "SqFtDrivewayConcrete",
                                                    'value' => ($reset) ? "" : set_value("SqFtDrivewayConcrete"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityDrivewayConcrete",
                                                    'id'    => "QuantityDrivewayConcrete",
                                                    'value' => ($reset) ? "" : set_value("QuantityDrivewayConcrete"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateDrivewayConcrete",
                                                    'id'    => "RateDrivewayConcrete",
                                                    'value' => ($reset) ? "" : set_value("RateDrivewayConcrete"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1DrivewayConcrete",
                                                    'id'    => "Bid1DrivewayConcrete",
                                                    'value' => ($reset) ? "" : set_value("Bid1DrivewayConcrete"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2DrivewayConcrete",
                                                    'id'    => "Bid2DrivewayConcrete",
                                                    'value' => ($reset) ? "" : set_value("Bid2DrivewayConcrete"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3DrivewayConcrete",
                                                    'id'    => "Bid3DrivewayConcrete",
                                                    'value' => ($reset) ? "" : set_value("Bid3DrivewayConcrete"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetDrivewayConcrete",
                                                    'id'    => "BudgetDrivewayConcrete",
                                                    'value' => ($reset) ? "" : set_value("BudgetDrivewayConcrete"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidDrivewayConcrete" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidDrivewayConcrete', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Foudation work</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesFoudationWork",
                                                    'id'    => "DetailsNotesFoudationWork",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesFoudationWork"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtFoudationWork",
                                                    'id'    => "SqFtFoudationWork",
                                                    'value' => ($reset) ? "" : set_value("SqFtFoudationWork"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityFoudationWork",
                                                    'id'    => "QuantityFoudationWork",
                                                    'value' => ($reset) ? "" : set_value("QuantityFoudationWork"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateFoudationWork",
                                                    'id'    => "RateFoudationWork",
                                                    'value' => ($reset) ? "" : set_value("RateFoudationWork"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1FoudationWork",
                                                    'id'    => "Bid1FoudationWork",
                                                    'value' => ($reset) ? "" : set_value("Bid1FoudationWork"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2FoudationWork",
                                                    'id'    => "Bid2FoudationWork",
                                                    'value' => ($reset) ? "" : set_value("Bid2FoudationWork"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3FoudationWork",
                                                    'id'    => "Bid3FoudationWork",
                                                    'value' => ($reset) ? "" : set_value("Bid3FoudationWork"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetFoudationWork",
                                                    'id'    => "BudgetFoudationWork",
                                                    'value' => ($reset) ? "" : set_value("BudgetFoudationWork"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidFoudationWork" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidFoudationWork', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Brick pointing, replacement</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesBrickPointingReplacement",
                                                    'id'    => "DetailsNotesBrickPointingReplacement",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesBrickPointingReplacement"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtBrickPointingReplacement",
                                                    'id'    => "SqFtBrickPointingReplacement",
                                                    'value' => ($reset) ? "" : set_value("SqFtBrickPointingReplacement"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityBrickPointingReplacement",
                                                    'id'    => "QuantityBrickPointingReplacement",
                                                    'value' => ($reset) ? "" : set_value("QuantityBrickPointingReplacement"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateBrickPointingReplacement",
                                                    'id'    => "RateBrickPointingReplacement",
                                                    'value' => ($reset) ? "" : set_value("RateBrickPointingReplacement"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1BrickPointingReplacement",
                                                    'id'    => "Bid1BrickPointingReplacement",
                                                    'value' => ($reset) ? "" : set_value("Bid1BrickPointingReplacement"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2BrickPointingReplacement",
                                                    'id'    => "Bid2BrickPointingReplacement",
                                                    'value' => ($reset) ? "" : set_value("Bid2BrickPointingReplacement"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3BrickPointingReplacement",
                                                    'id'    => "Bid3BrickPointingReplacement",
                                                    'value' => ($reset) ? "" : set_value("Bid3BrickPointingReplacement"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetBrickPointingReplacement",
                                                    'id'    => "BudgetBrickPointingReplacement",
                                                    'value' => ($reset) ? "" : set_value("BudgetBrickPointingReplacement"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidBrickPointingReplacement" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidBrickPointingReplacement', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Other Exterior</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesOtherExterior",
                                                    'id'    => "DetailsNotesOtherExterior",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesOtherExterior"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtOtherExterior",
                                                    'id'    => "SqFtOtherExterior",
                                                    'value' => ($reset) ? "" : set_value("SqFtOtherExterior"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityOtherExterior",
                                                    'id'    => "QuantityOtherExterior",
                                                    'value' => ($reset) ? "" : set_value("QuantityOtherExterior"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateOtherExterior",
                                                    'id'    => "RateOtherExterior",
                                                    'value' => ($reset) ? "" : set_value("RateOtherExterior"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1OtherExterior",
                                                    'id'    => "Bid1OtherExterior",
                                                    'value' => ($reset) ? "" : set_value("Bid1OtherExterior"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2OtherExterior",
                                                    'id'    => "Bid2OtherExterior",
                                                    'value' => ($reset) ? "" : set_value("Bid2OtherExterior"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3OtherExterior",
                                                    'id'    => "Bid3OtherExterior",
                                                    'value' => ($reset) ? "" : set_value("Bid3OtherExterior"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetOtherExterior",
                                                    'id'    => "BudgetOtherExterior",
                                                    'value' => ($reset) ? "" : set_value("BudgetOtherExterior"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidOtherExterior" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidOtherExterior', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td colspan="11"><strong><u>Other:</u></strong></td>
                                        </tr>
                                        <tr>
                                            <td>Contingency</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesContingency",
                                                    'id'    => "DetailsNotesContingency",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesContingency"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtContingency",
                                                    'id'    => "SqFtContingency",
                                                    'value' => ($reset) ? "" : set_value("SqFtContingency"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityContingency",
                                                    'id'    => "QuantityContingency",
                                                    'value' => ($reset) ? "" : set_value("QuantityContingency"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateContingency",
                                                    'id'    => "RateContingency",
                                                    'value' => ($reset) ? "" : set_value("RateContingency"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Contingency",
                                                    'id'    => "Bid1Contingency",
                                                    'value' => ($reset) ? "" : set_value("Bid1Contingency"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Contingency",
                                                    'id'    => "Bid2Contingency",
                                                    'value' => ($reset) ? "" : set_value("Bid2Contingency"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Contingency",
                                                    'id'    => "Bid3Contingency",
                                                    'value' => ($reset) ? "" : set_value("Bid3Contingency"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetContingency",
                                                    'id'    => "BudgetContingency",
                                                    'value' => ($reset) ? "" : set_value("BudgetContingency"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidContingency" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidContingency', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>GCFee</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesGCFee",
                                                    'id'    => "DetailsNotesGCFee",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesGCFee"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtGCFee",
                                                    'id'    => "SqFtGCFee",
                                                    'value' => ($reset) ? "" : set_value("SqFtGCFee"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityGCFee",
                                                    'id'    => "QuantityGCFee",
                                                    'value' => ($reset) ? "" : set_value("QuantityGCFee"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateGCFee",
                                                    'id'    => "RateGCFee",
                                                    'value' => ($reset) ? "" : set_value("RateGCFee"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1GCFee",
                                                    'id'    => "Bid1GCFee",
                                                    'value' => ($reset) ? "" : set_value("Bid1GCFee"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2GCFee",
                                                    'id'    => "Bid2GCFee",
                                                    'value' => ($reset) ? "" : set_value("Bid2GCFee"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3GCFee",
                                                    'id'    => "Bid3GCFee",
                                                    'value' => ($reset) ? "" : set_value("Bid3GCFee"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetGCFee",
                                                    'id'    => "BudgetGCFee",
                                                    'value' => ($reset) ? "" : set_value("BudgetGCFee"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidGCFee" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidGCFee', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Cleanup</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesCleanup",
                                                    'id'    => "DetailsNotesCleanup",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesCleanup"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtCleanup",
                                                    'id'    => "SqFtCleanup",
                                                    'value' => ($reset) ? "" : set_value("SqFtCleanup"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityCleanup",
                                                    'id'    => "QuantityCleanup",
                                                    'value' => ($reset) ? "" : set_value("QuantityCleanup"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateCleanup",
                                                    'id'    => "RateCleanup",
                                                    'value' => ($reset) ? "" : set_value("RateCleanup"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1Cleanup",
                                                    'id'    => "Bid1Cleanup",
                                                    'value' => ($reset) ? "" : set_value("Bid1Cleanup"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2Cleanup",
                                                    'id'    => "Bid2Cleanup",
                                                    'value' => ($reset) ? "" : set_value("Bid2Cleanup"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3Cleanup",
                                                    'id'    => "Bid3Cleanup",
                                                    'value' => ($reset) ? "" : set_value("Bid3Cleanup"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetCleanup",
                                                    'id'    => "BudgetCleanup",
                                                    'value' => ($reset) ? "" : set_value("BudgetCleanup"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidCleanup" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidCleanup', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>Others</td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailsNotesOtherOther",
                                                    'id'    => "DetailsNotesOtherOther",
                                                    'value' => ($reset) ? "" : set_value("DetailsNotesOtherOther"),
                                                    'class'	=> 'form-control',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "SqFtOtherOther",
                                                    'id'    => "SqFtOtherOther",
                                                    'value' => ($reset) ? "" : set_value("SqFtOtherOther"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "QuantityOtherOther",
                                                    'id'    => "QuantityOtherOther",
                                                    'value' => ($reset) ? "" : set_value("QuantityOtherOther"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                ); ?>
                                                <?=form_input($field)?>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "RateOtherOther",
                                                    'id'    => "RateOtherOther",
                                                    'value' => ($reset) ? "" : set_value("RateOtherOther"),
                                                    'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid1OtherOther",
                                                    'id'    => "Bid1OtherOther",
                                                    'value' => ($reset) ? "" : set_value("Bid1OtherOther"),
                                                    'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid2OtherOther",
                                                    'id'    => "Bid2OtherOther",
                                                    'value' => ($reset) ? "" : set_value("Bid2OtherOther"),
                                                    'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "Bid3OtherOther",
                                                    'id'    => "Bid3OtherOther",
                                                    'value' => ($reset) ? "" : set_value("Bid3OtherOther"),
                                                    'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "BudgetOtherOther",
                                                    'id'    => "BudgetOtherOther",
                                                    'value' => ($reset) ? "" : set_value("BudgetOtherOther"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                $attrb = 'id="MonthPaidOtherOther" class="form-control RefiMonthPaidColumn" onChange="CheckRefiMonthRehab(this, \'MonthsRefiRehab\');"'; ?>
                                                <?=form_dropdown('MonthPaidOtherOther', $options, 1, $attrb)?>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="5"><strong>Total</strong></td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "TotalBid1Total",
                                                    'id'    => "TotalBid1Total",
                                                    'value' => ($reset) ? "" : set_value("TotalBid1Total"),
                                                    'class'	=> 'form-control Bid1InputBox',
                                                    'readonly' => 'readonly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "TotalBid2Total",
                                                    'id'    => "TotalBid2Total",
                                                    'value' => ($reset) ? "" : set_value("TotalBid2Total"),
                                                    'class'	=> 'form-control Bid2InputBox',
                                                    'readonly' => 'readonly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "TotalBid3Total",
                                                    'id'    => "TotalBid3Total",
                                                    'value' => ($reset) ? "" : set_value("TotalBid3Total"),
                                                    'class'	=> 'form-control Bid3InputBox',
                                                    'readonly' => 'readonly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailedRefiBudget",
                                                    'id'    => "DetailedRefiBudget",
                                                    'value' => ($reset) ? "" : set_value("DetailedRefiBudget"),
                                                    'class'	=> 'form-control BudgetInputBox RefiBudgetColumn',
                                                    'readonly' => 'readonly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                	<input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                    
                <?=form_close()?>
                
            </div>
            <div class="modal-footer">
            	<button type="button" id="RefiRehubReturn" class="btn btn-primary">RETURN TO ANALYSIS</button>
            </div>
        </div>
    </div>
</div>
<!-- Rehub Budget Refi Analysis Modal -->