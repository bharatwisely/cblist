<div class="modal fade" id="modalEmail" tabindex="-1" role="dialog" aria-labelledby="reportsLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header text-center">
                <h4 class="modal-title">EMAIL REPORTS</h4>
            </div>
            <div class="modal-body">
				<form role="form" class="form-horizontal" id="email_form">
                	<div class="form-group">
                    	<label class="col-sm-2" for="inputTo">To</label>
                        <div class="col-sm-10"><input class="form-control" name="inputTo" id="inputTo" placeholder="comma separated list of recipients" type="email"></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-sm-2" for="inputSubject">Subject</label>
                        <div class="col-sm-10"><input class="form-control" name="inputSubject" id="inputSubject" placeholder="subject" type="text"></div>
                    </div>
                    <div class="form-group">
                    	<label class="col-sm-12" for="inputBody">Message</label>
                        <div class="col-sm-12"><textarea style="resize:none;" class="form-control" name="inputBody" id="inputBody" rows="10"></textarea></div>
                    </div>
                    <div class="row">
	                    <div class="col-lg-12" id="attchments_views"></div>
                    </div>
                    <div id="attchments_files"></div>
                </form>
            </div>
            <div class="modal-footer">
            	<small class="modal-loading pull-left"></small>
                <button type="button" class="btn btn-default" id="btn_email_cancel" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary" id="btn_email_send" data-type="proposal" data-propertyId="" data-id="<?=$user->id?>">
                	Send <i class="fa fa-arrow-circle-right fa-lg"></i>
                </button>
            </div>
       	</div>
    </div>
</div>