<!DOCTYPE html>
<!--[if IE 8]><html lang="en" class="ie8"><![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"><![endif]-->
<!--[if !IE]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?=$page_title?></title>
<link href="<?=base_url('favicon.png')?>" rel="shortcut icon" />
<link href="<?=base_url('assets/css/pdf-css/pdf.css')?>" rel="stylesheet" type="text/css" />
<script src="<?=base_url('assets/js/plugins/jquery/jquery.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/rental-report.js'); ?>"></script>
</head>
<body>
<div class="container">