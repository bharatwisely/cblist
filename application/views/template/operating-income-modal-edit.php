<!-- Section1: Operating Income (Rent) Modal -->
<div class="modal fade" id="OperatingIncomeModal" tabindex="-1" role="dialog" aria-labelledby="OperatingIncomeModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header text-center">
                <h4 class="modal-title" id="OperatingIncomeModalLabel">
                	Operating Income/Rent
                    <a class="modal-help" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body">
            	<?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
				<?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                
                	<input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                    <input type="hidden" name="UserID" value="<?=$user->id?>" />
                    <div class="row">    
                        <div class="col-lg-12">
                            <table width="100%" class="table">
                                <thead>
                                    <tr>
                                        <th width="8%">Unit Type #</th>
                                        <th width="10%"># of units</th>
                                        <th width="25%">Unit type</th>
                                        <th width="15%">Square Ft.</th>
                                        <th width="20%">Monthly Rent</th>
                                        <th width="20%">Annual Rent</th>
                                        <th width="14%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_1",
                                                'id'    => "NoOfUnits_1",
                                                'value' => $income->NoOfUnits_1,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_1" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_1', $options, $income->UnitType_1, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_1",
                                                'id'    => "SquareFt_1",
                                                'value' => $income->SquareFt_1,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_1",
                                                'id'    => "MonthlyRent_1",
                                                'value' => number_format($income->MonthlyRent_1, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_1",
                                                'id'    => "AnnualRent_1",
                                                'value' => number_format($income->AnnualRent_1, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_1",
                                                'id'    => "AnnualPercent_1",
                                                'value' => round($income->AnnualPercent_1),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_2",
                                                'id'    => "NoOfUnits_2",
                                                'value' => $income->NoOfUnits_2,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_2" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_2', $options, $income->UnitType_2, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_2",
                                                'id'    => "SquareFt_2",
                                                'value' => $income->SquareFt_2,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_2",
                                                'id'    => "MonthlyRent_2",
                                                'value' => number_format($income->MonthlyRent_2, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_2",
                                                'id'    => "AnnualRent_2",
                                                'value' => number_format($income->AnnualRent_2, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_2",
                                                'id'    => "AnnualPercent_2",
                                                'value' => round($income->AnnualPercent_2),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_3",
                                                'id'    => "NoOfUnits_3",
                                                'value' => $income->NoOfUnits_3,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_3" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_3', $options, $income->UnitType_3, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_3",
                                                'id'    => "SquareFt_3",
                                                'value' => $income->SquareFt_3,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_3",
                                                'id'    => "MonthlyRent_3",
                                                'value' => number_format($income->MonthlyRent_3, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_3",
                                                'id'    => "AnnualRent_3",
                                                'value' => number_format($income->AnnualRent_3, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_3",
                                                'id'    => "AnnualPercent_3",
                                                'value' => round($income->AnnualPercent_3),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_4",
                                                'id'    => "NoOfUnits_4",
                                                'value' => $income->NoOfUnits_4,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_4" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_4', $options, $income->UnitType_4, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_4",
                                                'id'    => "SquareFt_4",
                                                'value' => $income->SquareFt_4,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_4",
                                                'id'    => "MonthlyRent_4",
                                                'value' => number_format($income->MonthlyRent_4, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_4",
                                                'id'    => "AnnualRent_4",
                                                'value' => number_format($income->AnnualRent_4, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_4",
                                                'id'    => "AnnualPercent_4",
                                                'value' => round($income->AnnualPercent_4),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_5",
                                                'id'    => "NoOfUnits_5",
                                                'value' => $income->NoOfUnits_5,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_5" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_5', $options, $income->UnitType_5, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_5",
                                                'id'    => "SquareFt_5",
                                                'value' => $income->SquareFt_5,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_5",
                                                'id'    => "MonthlyRent_5",
                                                'value' => number_format($income->MonthlyRent_5, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_5",
                                                'id'    => "AnnualRent_5",
                                                'value' => number_format($income->AnnualRent_5, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_5",
                                                'id'    => "AnnualPercent_5",
                                                'value' => round($income->AnnualPercent_5),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_6",
                                                'id'    => "NoOfUnits_6",
                                                'value' => $income->NoOfUnits_6,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_6" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_6', $options, $income->UnitType_6, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_6",
                                                'id'    => "SquareFt_6",
                                                'value' => $income->SquareFt_6,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_6",
                                                'id'    => "MonthlyRent_6",
                                                'value' => number_format($income->MonthlyRent_6, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_6",
                                                'id'    => "AnnualRent_6",
                                                'value' => number_format($income->AnnualRent_6, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_6",
                                                'id'    => "AnnualPercent_6",
                                                'value' => round($income->AnnualPercent_6),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_7",
                                                'id'    => "NoOfUnits_7",
                                                'value' => $income->NoOfUnits_7,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_7" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_7', $options, $income->UnitType_7, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_7",
                                                'id'    => "SquareFt_7",
                                                'value' => $income->SquareFt_7,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_7",
                                                'id'    => "MonthlyRent_7",
                                                'value' => number_format($income->MonthlyRent_7, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_7",
                                                'id'    => "AnnualRent_7",
                                                'value' => number_format($income->AnnualRent_7, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_7",
                                                'id'    => "AnnualPercent_7",
                                                'value' => round($income->AnnualPercent_7),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_8",
                                                'id'    => "NoOfUnits_8",
                                                'value' => $income->NoOfUnits_8,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_8" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_8', $options, $income->UnitType_8, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_8",
                                                'id'    => "SquareFt_8",
                                                'value' => $income->SquareFt_8,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_8",
                                                'id'    => "MonthlyRent_8",
                                                'value' => number_format($income->MonthlyRent_8, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_8",
                                                'id'    => "AnnualRent_8",
                                                'value' => number_format($income->AnnualRent_8, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_8",
                                                'id'    => "AnnualPercent_8",
                                                'value' => round($income->AnnualPercent_8),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_9",
                                                'id'    => "NoOfUnits_9",
                                                'value' => $income->NoOfUnits_9,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_9" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_9', $options, $income->UnitType_9, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_9",
                                                'id'    => "SquareFt_9",
                                                'value' => $income->SquareFt_9,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_9",
                                                'id'    => "MonthlyRent_9",
                                                'value' => number_format($income->MonthlyRent_9, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_9",
                                                'id'    => "AnnualRent_9",
                                                'value' => number_format($income->AnnualRent_9, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_9",
                                                'id'    => "AnnualPercent_9",
                                                'value' => round($income->AnnualPercent_9),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_10",
                                                'id'    => "NoOfUnits_10",
                                                'value' => $income->NoOfUnits_10,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_10" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_10', $options, $income->UnitType_10, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_10",
                                                'id'    => "SquareFt_10",
                                                'value' => $income->SquareFt_10,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_10",
                                                'id'    => "MonthlyRent_10",
                                                'value' => number_format($income->MonthlyRent_10, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_10",
                                                'id'    => "AnnualRent_10",
                                                'value' => number_format($income->AnnualRent_10, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_10",
                                                'id'    => "AnnualPercent_10",
                                                'value' => round($income->AnnualPercent_10),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_11",
                                                'id'    => "NoOfUnits_11",
                                                'value' => $income->NoOfUnits_11,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_11" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_11', $options, $income->UnitType_11, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_11",
                                                'id'    => "SquareFt_11",
                                                'value' => $income->SquareFt_11,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_11",
                                                'id'    => "MonthlyRent_11",
                                                'value' => number_format($income->MonthlyRent_11, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_11",
                                                'id'    => "AnnualRent_11",
                                                'value' => number_format($income->AnnualRent_11, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_11",
                                                'id'    => "AnnualPercent_11",
                                                'value' => round($income->AnnualPercent_11),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>12</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_12",
                                                'id'    => "NoOfUnits_12",
                                                'value' => $income->NoOfUnits_12,
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_12" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_12', $options, $income->UnitType_12, $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_12",
                                                'id'    => "SquareFt_12",
                                                'value' => $income->SquareFt_12,
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_12",
                                                'id'    => "MonthlyRent_12",
                                                'value' => number_format($income->MonthlyRent_12, 2, '.', ''),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_12",
                                                'id'    => "AnnualRent_12",
                                                'value' => number_format($income->AnnualRent_12, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_12",
                                                'id'    => "AnnualPercent_12",
                                                'value' => round($income->AnnualPercent_12),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>Total</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnit_Total",
                                                'id'    => "NoOfUnit_Total",
                                                'value' => $income->NoOfUnit_Total,
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_Total",
                                                'id'    => "SquareFt_Total",
                                                'value' => $income->SquareFt_Total,
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="5">&nbsp;</td>
                                        <td colspan="3">Gross Schedule Income</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_Gross",
                                                'id'    => "MonthlyRent_Gross",
                                                'value' => number_format($income->MonthlyRent_Gross, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_Gross",
                                                'id'    => "AnnualRent_Gross",
                                                'value' => number_format($income->AnnualRent_Gross, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_Gross",
                                                'id'    => "AnnualPercent_Gross",
                                                'value' => round($income->AnnualPercent_Gross),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">VACANCY LOSS</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "VacancyLossPercent",
                                                'id'    => "VacancyLossPercent",
                                                'value' => round($income->VacancyLossPercent),
                                                'class'	=> 'form-control NumberOnly percentageField',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "VacancyLossMonthly",
                                                'id'    => "VacancyLossMonthly",
                                                'value' => number_format($income->VacancyLossMonthly, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "VacancyLossAnnual",
                                                'id'    => "VacancyLossAnnual",
                                                'value' => number_format($income->VacancyLossAnnual, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td rowspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">MANAGEMENT</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "ManagementPercent",
                                                'id'    => "ManagementPercent",
                                                'value' => round($income->ManagementPercent),
                                                'class'	=> 'form-control NumberOnly percentageField',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "ManagementMonthly",
                                                'id'    => "ManagementMonthly",
                                                'value' => number_format($income->ManagementMonthly, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "ManagementAnnual",
                                                'id'    => "ManagementAnnual",
                                                'value' => number_format($income->ManagementAnnual, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Other Income</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OtherIncomeMonthly",
                                                'id'    => "OtherIncomeMonthly",
                                                'value' => number_format($income->OtherIncomeMonthly, 2, '.', ''),
                                                'class'	=> 'form-control NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OtherIncomeAnnual",
                                                'id'    => "OtherIncomeAnnual",
                                                'value' => number_format($income->OtherIncomeAnnual, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong>Gross Operating Income (Effective Gross Inc)</strong></td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyGrossOperatingIncome",
                                                'id'    => "MonthlyGrossOperatingIncome",
                                                'value' => number_format($income->MonthlyGrossOperatingIncome, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualGrossOperatingIncome",
                                                'id'    => "AnnualGrossOperatingIncome",
                                                'value' => number_format($income->AnnualGrossOperatingIncome, 2, '.', ''),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                    <input type="hidden" name="TableName" value="operating_income" />
                    
                <?=form_close()?>
            </div>
            <div class="modal-footer">
            	<button  type="button" id="OperatingIncomeReturn" class="btn btn-primary">PROCEED TO EXPENSE INPUT</button>
            </div>
        </div>
    </div>
</div>
<!-- Rehub Budget Flip Analysis Modal -->