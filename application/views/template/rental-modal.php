<div class="modal fade" id="rental_reports" tabindex="-1" role="dialog" aria-labelledby="reportsLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header text-center">
                <h4 class="modal-title" id="reportsLabel">GENERATE REPORTS</h4>
           	</div>
            <div class="modal-body">
            	<?php $attributes = array('class' => 'form-horizontal', 'name' => 'ReportForm'); ?>
               	<?=form_open(base_url('pages/rental-valuator') . '/', $attributes)?>
                    <div class="row">
                        <div class="col-lg-12">
                        	<div class="row">
                                <div class="col-lg-6">
                                    <p style="margin-top:10px;"><strong>Check reports to view or print:</strong></p>
                                </div>
                                <div class="col-lg-6 text-right">
                                    <small style="font-size:12px;">
                                    	Please turn off your pop-up blocker<br />to view or download the reports. 
	                                    <a href="#" data-toggle="modal" data-target="#popup_blocker_video" title="Watch video">
	                                        <i class="fa fa-video-camera"></i>
                                        </a>
                                    </small>
                                </div>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="RentalCoverPage"> Cover Page</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="SinglePagePropertySummery"> Property Summary</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="RentalAdditionalPics"> Property Photos</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="AnnualPropertyOperating"> Annual Property Operating Statement</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="RentReport"> Operating Income Report (Detailed)</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="OperatingExpensesReport"> Operating Expenses Report (Detailed)</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="IncomeStatement"> Income Statement</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="RentalCashFlowReport"> Cash Flow Report</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="DiscountCashFlowAnalysis"> Discounted Cash Flow Analysis (DCF)</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="LenderSummary"> Debt Coverage LTV Projections</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="RentalRefiAnalysis"> Refi Analysis </p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="FullTaxTreatment"> Taxation Report</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="PropertyValueEvolution"> Property Value and Disposition Report</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="FinancialRatiosReport"> Financial Ratios Report</p>
                                </label>
                            </div>
                        </div>
                    </div>
                <?=form_close()?>
            </div>
            <div class="modal-footer">
            	<small class="print-error pull-left"></small>
            	<button type="button" class="btn btn-default btn_proposal_cancel" data-type="rental" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn_proposal_print" data-type="rental" data-propertyId="" data-id="<?=$user->id?>">
                	<i class="fa fa-print"></i> Generate
                </button>
                <button type="button" class="btn btn-primary btn_proposal_download" data-type="rental" data-propertyId="" data-id="<?=$user->id?>">
                	<i class="fa fa-download"></i> Download
                </button>
                <button type="button" class="btn btn-primary btn_proposal_email" data-type="rental" data-propertyId="" data-id="<?=$user->id?>">
                	<i class="fa fa-envelope"></i> Email
                </button>
            </div>
       	</div>
    </div>
</div>