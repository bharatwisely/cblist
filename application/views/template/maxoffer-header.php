<!DOCTYPE html>
<!--[if IE 8]><html lang="en" class="ie8"><![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"><![endif]-->
<!--[if !IE]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?=$page_title?></title>
<link href="<?=base_url('favicon.png')?>" rel="shortcut icon" />
<link href="<?=base_url('assets/css/plugins/bootstrap/bootstrap.min.css')?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?=base_url('assets/css/plugins/font-awesome/font-awesome.min.css')?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?=base_url('assets/css/fonts.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/css/print-css.css')?>" rel="stylesheet" type="text/css" media="print" />
<script src="<?=base_url('assets/js/plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('assets/js/plugins/bootstrap/bootstrap.min.js')?>"></script>
<style type="text/css">
html, body {
	background-color:#e1e1e1;
	font-family: 'Open Sans Regular';
	font-size:14px;
	color:#000;
}
.btn {
	border-radius: 0;
}
.MaxOffers {
	border:15px solid #fff;
	margin-bottom:100px;
	padding-bottom:15px;
	background-color:#fff;
	page-break-after:always
}
</style>
</head>
<body>
<div class="container-fluid">
	<div class="row">
        <div class="col-lg-12 text-center">
            <br />
            <button type="button" class="btn btn-sm btn-default noprint" onClick="window.close();">
                <i class="fa fa-close"></i> CLOSE</button>
            </button>
            <a href="<?=base_url('pages/download-report/' . $this->uri->segment(3) . '/' . $this->uri->segment(4))?>/">
                <button type="button" class="btn btn-sm btn-danger noprint">
                    <i class="fa fa-download"></i> DOWNLOAD PDF</button>
                </button>
            </a>
            <button type="button" class="btn btn-sm btn-primary noprint" onClick="window.print();">
                <i class="fa fa-print"></i> PRINT REPORT</button>
            </button>
        </div>
    </div><br>