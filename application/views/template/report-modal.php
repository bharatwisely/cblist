<div class="modal fade" id="reports" tabindex="-1" role="dialog" aria-labelledby="reportsLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header text-center">
                <h4 class="modal-title" id="reportsLabel">GENERATE REPORTS</h4>
           	</div>
            <div class="modal-body">
            	<?php $attributes = array('class' => 'form-horizontal', 'name' => 'ReportForm'); ?>
               	<?=form_open(base_url('pages/rehab-valuator') . '/', $attributes)?>
                    <div class="row">
                    	<div class="col-lg-12 text-center">
                        	<p>
                            	<small style="font-size:12px;">
                                    Please turn off your pop-up blocker to view or download the reports. 
                                    <a href="#" data-toggle="modal" data-target="#popup_blocker_video" title="Watch video">
                                        <i class="fa fa-video-camera"></i>
                                    </a>
                                </small>
                            </p>
                            <div class="row">
                            	<div class="col-lg-6">
                                    <button class="btn btn-block btn-default" style="margin-bottom:15px;white-space:normal;" type="button" id="FullFlip">
                                        Full Presentation for Flip
                                    </button>
                                </div>
                                <div class="col-lg-6">
                                    <button class="btn btn-block btn-default" style="margin-bottom:15px;white-space:normal;" type="button" id="FullRefi">
                                        Full Presentation for Refi/Hold
                                    </button>
                                </div>
                                <div class="col-lg-6">
                                    <button class="btn btn-block btn-default" style="white-space:normal;" type="button" id="FullFlipLender">
                                        Full Presentation for Private Lender (with Flip Exit)
                                    </button>
                                </div>
                                <div class="col-lg-6">
                                    <button class="btn btn-block btn-default" style="white-space:normal;" type="button" id="FullRefiLender">
                                        Full Presentation for Private Lender (with Refi Exit)
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                        	<hr>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="ProjectSummary"> Project Summary</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="FlipRehabBudget"> Scope of Work+Budget Rehab/Flip</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="RefiRehabBudget"> Scope of Work+Budget Rehab/Refi</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="CoverPage"> Cover Page for Marketing Presentation</p>
                                </label>
                            </div>
                            <hr />
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="FlipMarketingSheet"> Marketing Sheet For Flip Exit</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="RefiMarketingSheet"> Marketing Sheet For Refi Exit</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="FlipCashFlow"> Cash Flow Report Flip Exit</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="RefiCashFlow"> Cash Flow Report Refi Exit</p>
                                </label>
                            </div>
                            <hr />
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="FlipFundingRequest"> Private Lender Funding Request Rehab/Flip</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="RefiFundingRequest"> Private Lender Funding Request Rehab/Refi</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="FlipCashFlowLender"> Cash Flow to Lender Rehab/Flip</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="RefiCashFlowLender"> Cash Flow to Lender Rehab/Refi</p>
                                </label>
                            </div>
                            <hr />
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="CompsReport"> Comparable Sales Report</p>
                                </label>
                            </div>
                            <div class="checkbox">
                                <label>
                                    <p><input type="checkbox" class="print-option print-feature" name="PicsPage"> Additional Pics Page</p>
                                </label>
                            </div>
                        </div>
                    </div>
                <?=form_close()?>
            </div>
            <div class="modal-footer">
            	<small class="print-error pull-left"></small>
            	<button type="button" class="btn btn-default btn_proposal_cancel" data-type="proposal" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn_proposal_print" data-type="proposal" data-propertyId="" data-id="<?=$user->id?>">
                	<i class="fa fa-print"></i> Generate
                </button>
                <button type="button" class="btn btn-primary btn_proposal_download" data-type="proposal" data-propertyId="" data-id="<?=$user->id?>">
                	<i class="fa fa-download"></i> Download
                </button>
                <button type="button" class="btn btn-primary btn_proposal_email" data-type="proposal" data-propertyId="" data-id="<?=$user->id?>">
                	<i class="fa fa-envelope"></i> Email
                </button>
            </div>
       	</div>
    </div>
</div>