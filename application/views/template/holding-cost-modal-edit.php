<!-- Holding Costs Modal -->
<div class="modal fade" id="HoldingCostsModal" tabindex="-1" role="dialog" aria-labelledby="HoldingCostsModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header text-center">
                <h4 class="modal-title" id="HoldingCostsModalLabel">
                	Holding Costs
                    <a class="modal-help" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body RehabForm">
				<p class="small text-center">Enter LumpSum Holding cost estimate for entire project length here</p>
                <div class="col-lg-12 assump-box">
                	<div class="row">
                    
                    	<?php $attributes = array('class' => 'form-horizontal row', 'name' => 'PropertyForm'); ?>
                        <?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                        	
                            <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                            <input type="hidden" name="UserID" value="<?=$user->id?>" />
                            <div class="panel-field" id="HoldingCostsOptionRow">
                                <label class="col-lg-8">How do you want to enter your Holding Costs?</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $options = array(
                                        'Detailed Input'  => 'Detailed Input',
                                        'Quick Lump Sum'  => 'Quick Lump Sum',
                                    );
                                    $attrb = 'id="HoldingCostsOption" class="form-control"'; ?>
                                    <?=form_dropdown('HoldingCostsOption', $options, $fields->HoldingCostsOption, $attrb)?>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="TotalHoldingCostsRow" style="display:none;">
                                <label class="col-lg-8">Enter Itemized Forecast Below</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "TotalHoldingCosts",
                                        'id'    => "TotalHoldingCosts",
                                        'value' => number_format($fields->TotalHoldingCosts, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                        'style'	=> 'display:none;',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="MonthlyAmountBelowRow">
                                <label class="col-lg-8"><font color="#f00">**Monthly Amounts Below</font></label>
                                <div class="col-lg-4">&nbsp;</div>
                            </div>
                            
                            <div class="panel-field" id="InsurancePolicyPremiumRow">
                                <label class="col-lg-8">Insurance Policy Premium</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "InsurancePolicyPremium",
                                        'id'    => "InsurancePolicyPremium",
                                        'value' => number_format($fields->InsurancePolicyPremium, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="ElectricalBillsRow">
                                <label class="col-lg-8">Electrical Bills</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "ElectricalBills",
                                        'id'    => "ElectricalBills",
                                        'value' => number_format($fields->ElectricalBills, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="GasBillsRow">
                                <label class="col-lg-8">Gas Bills</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "GasBills",
                                        'id'    => "GasBills",
                                        'value' => number_format($fields->GasBills, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="OilBillsRow">
                                <label class="col-lg-8">Oil Bills</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "OilBills",
                                        'id'    => "OilBills",
                                        'value' => number_format($fields->OilBills, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="WaterSewerBillsRow">
                                <label class="col-lg-8">Water/Sewer Bills</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "WaterSewerBills",
                                        'id'    => "WaterSewerBills",
                                        'value' => number_format($fields->WaterSewerBills, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="AlarmSystemBillsRow">
                                <label class="col-lg-8">Alarm System Bills</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "AlarmSystemBills",
                                        'id'    => "AlarmSystemBills",
                                        'value' => number_format($fields->AlarmSystemBills, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="MiscellaniousRow">
                                <label class="col-lg-8">Miscellanious</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "Miscellanious",
                                        'id'    => "Miscellanious",
                                        'value' => number_format($fields->Miscellanious, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="LawncareRow">
                                <label class="col-lg-8">Lawncare</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "Lawncare",
                                        'id'    => "Lawncare",
                                        'value' => number_format($fields->Lawncare, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="HoldingOther1Row">
                                <label class="col-lg-8">Other</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "HoldingOther1",
                                        'id'    => "HoldingOther1",
                                        'value' => number_format($fields->HoldingOther1, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="HoldingOther2Row">
                                <label class="col-lg-8">Other</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "HoldingOther2",
                                        'id'    => "HoldingOther2",
                                        'value' => number_format($fields->HoldingOther2, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="TotalPurchaseHoldingCostsRow">
                                <label class="col-lg-8"><strong>TOTAL (Per Month)</strong></label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "TotalPurchaseHoldingCosts",
                                        'id'    => "TotalPurchaseHoldingCosts",
                                        'value' => number_format($fields->TotalPurchaseHoldingCosts, 2, '.', ''),
                                        'class'	=> 'form-control HoldingCostsInput number-field',
                                        'readonly' => 'readonly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div> 
                            <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                            <input type="hidden" name="TableName" value="holding_costs" />
                        
                        <?=form_close()?>
                                               
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            	<button type="button" id="HoldingCostsReturn" class="btn btn-primary">RETURN TO ANALYSIS</button>
            </div>
        </div>
    </div>
</div>
<!-- Holding Costs Modal -->