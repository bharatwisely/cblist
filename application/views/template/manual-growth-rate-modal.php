<!-- Closing Costs Modal -->
<div class="modal fade" id="ManualGrowthRateModal" tabindex="-1" role="dialog" aria-labelledby="MoreOptionModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
			<div class="modal-header">
                <h4 class="modal-title text-center">
                	Manually Enter Rent Projections
                    <a class="modal-help pull-left" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                	<div class="row">
                    	
                        <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                        <?=form_open(base_url('pages/rental-calculation') . '/', $attributes)?>
                        	
                            <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                            <input type="hidden" name="UserID" value="<?=$user->id?>" />
                            <div class="panel-field" id="GrowthRateTableRow" style="overflow:hidden;">
                            	<div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                        	<th colspan="2"></th>
                                            <?php for ( $td = 0; $td <= 40; $td++ ) { ?>
                                                <th class="text-center"><?php echo $td; ?></th>
                                            <?php } ?>
                                        </tr>
                                        <tr id="growth_date_show"></tr>
                                        <tr>
                                        	<td colspan="2">
                                            	<!--<label style="width:120px;">Enter Individual Rents for Each Year?</label>-->
                                            </td>
                                            <!--<td>
												<?php 
                                                $options = array(
                                                    'Yes' => 'Yes',
                                                    'No'  => 'No',
                                                );
                                                $attrb = 'id="OverrideRents" class="form-control" style="width:100px;"'; ?>
                                                <?=form_dropdown('OverrideRents', $options, (isset($fields->OverrideRents) ? $fields->OverrideRents : 'No'), $attrb)?>
                                            </td>-->
                                            <td align="center"><span style="width:100px;float:left;">Unit Type #</span></td>
                                            <td colspan="40" style="padding:5px;">Enter Monthly Rents for each unit below for the years 2 through 12.<br />For years 13 through 40, the rental growth rate will be determined either by the rates in Income(Rent) Page in this tab or the overall rental growth rate in the Analysis Interface tab in cell F10</td>
                                        </tr>
                                        
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>1</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_1_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_1_$td",
															'id'    => "IndividualRent_Year_1_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_1_$td",
															'id'    => "IndividualRent_Year_1_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>2</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_2_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_2_$td",
															'id'    => "IndividualRent_Year_2_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_2_$td",
															'id'    => "IndividualRent_Year_2_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>3</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_3_$td"; ?>
                                                <td>
                                                	<?php  if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_3_$td",
															'id'    => "IndividualRent_Year_3_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_3_$td",
															'id'    => "IndividualRent_Year_3_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>4</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_4_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_4_$td",
															'id'    => "IndividualRent_Year_4_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_4_$td",
															'id'    => "IndividualRent_Year_4_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													}?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>5</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_5_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_5_$td",
															'id'    => "IndividualRent_Year_5_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_5_$td",
															'id'    => "IndividualRent_Year_5_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>6</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_6_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_6_$td",
															'id'    => "IndividualRent_Year_6_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_6_$td",
															'id'    => "IndividualRent_Year_6_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>7</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_7_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_7_$td",
															'id'    => "IndividualRent_Year_7_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_7_$td",
															'id'    => "IndividualRent_Year_7_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>8</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_8_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_8_$td",
															'id'    => "IndividualRent_Year_8_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_8_$td",
															'id'    => "IndividualRent_Year_8_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>9</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_9_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_9_$td",
															'id'    => "IndividualRent_Year_9_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_9_$td",
															'id'    => "IndividualRent_Year_9_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>10</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_10_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_10_$td",
															'id'    => "IndividualRent_Year_10_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_10_$td",
															'id'    => "IndividualRent_Year_10_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>11</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_11_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_11_$td",
															'id'    => "IndividualRent_Year_11_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly"
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_11_$td",
															'id'    => "IndividualRent_Year_11_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>12</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Year_12_$td"; ?>
                                                <td>
                                                	<?php if ( $td == 1 || $td > 12 ) {
														$field = array(
															'name'  => "IndividualRent_Year_12_$td",
															'id'    => "IndividualRent_Year_12_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
															"readonly" => "readonly",
														);
													} else {
														$field = array(
															'name'  => "IndividualRent_Year_12_$td",
															'id'    => "IndividualRent_Year_12_$td",
															'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
															'class'  => 'form-control number-field',
															"data-popover" => "true",
															"data-content" => "",
														);
													} ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        
                                        <tr class="text-center">
                                        	<td colspan="2"></td>
                                            <td>Total</td>
                                            <?php for ( $td = 1; $td <= 40; $td++ ) {
												$InputBox = "IndividualRent_Total_$td"; ?>
                                                <td>
                                                	<?php 
													$field = array(
														'name'  => "IndividualRent_Total_$td",
														'id'    => "IndividualRent_Total_$td",
														'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
														'class'  => 'form-control number-field',
														"data-popover" => "true",
														"data-content" => "",
														"readonly" => "readonly",
													); ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        
                                        <tr>
                                        	<td><span style="width:150px;float:left;">Override Vacancy Rates?</span></td>
                                            <td>
                                            	<?php 
                                                $options = array(
                                                    'Yes' => 'Yes',
                                                    'No'  => 'No',
                                                );
                                                $attrb = 'id="OverrideVacancyRates" class="form-control" style="width:100px;"'; ?>
                                                <?=form_dropdown('OverrideVacancyRates', $options, (isset($fields->OverrideVacancyRates) ? $fields->OverrideVacancyRates : 'No'), $attrb)?>
                                            </td>
                                            <td rowspan="2">*If choose Yes, enter Vacancy rates on the right</td>
                                            <td>
                                            	<?php 
												$field = array(
													'name'  => "OverrideVacancyRates_Year_1",
													'id'    => "OverrideVacancyRates_Year_1",
													'value' => (isset($fields->$InputBox) ? round($fields->$InputBox) : set_value($InputBox)),
													'class'  => 'form-control number-field percentageField',
													"data-popover" => "true",
													"data-content" => "",
													"readonly" => "readonly",
												); ?>
												<div class="input-group">
													<?=form_input($field)?>
                                                    <div class="input-group-addon">%</div>
                                                </div>
                                            </td>
                                            <?php for ( $td = 2; $td <= 40; $td++ ) {
												$InputBox = "OverrideVacancyRates_Year_$td"; ?>
                                                <td>
                                                	<?php 
													$field = array(
														'name'  => "OverrideVacancyRates_Year_$td",
														'id'    => "OverrideVacancyRates_Year_$td",
														'value' => (isset($fields->$InputBox) ? round($fields->$InputBox) : set_value($InputBox)),
														'class'  => 'form-control number-field percentageField',
														"data-popover" => "true",
														"data-content" => "",
													); ?>
													<div class="input-group">
														<?=form_input($field)?>
                                                        <div class="input-group-addon">%</div>
                                                    </div>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                        <tr>
                                        	<td>Override Concessions?</td>
                                            <td>
                                            	<?php 
                                                $options = array(
                                                    'Yes' => 'Yes',
                                                    'No'  => 'No',
                                                );
                                                $attrb = 'id="OverrideConcessions" class="form-control" style="width:100px;"'; ?>
                                                <?=form_dropdown('OverrideConcessions', $options, (isset($fields->OverrideConcessions) ? $fields->OverrideConcessions : 'No'), $attrb)?>
                                            </td>
                                            <td>
                                            	<?php 
												$field = array(
													'name'  => "OverrideConcessions_Year_1",
													'id'    => "OverrideConcessions_Year_1",
													'value' => (isset($fields->$InputBox) ? round($fields->$InputBox) : set_value($InputBox)),
													'class'  => 'form-control number-field percentageField',
													"data-popover" => "true",
													"data-content" => "",
													"readonly" => "readonly",
												); ?>
												<div class="input-group">
													<?=form_input($field)?>
                                                    <div class="input-group-addon">%</div>
                                                </div>
                                            </td>
                                            <?php for ( $td = 2; $td <= 40; $td++ ) {
												$InputBox = "OverrideConcessions_Year_$td"; ?>
                                                <td>
                                                	<?php 
													$field = array(
														'name'  => "OverrideConcessions_Year_$td",
														'id'    => "OverrideConcessions_Year_$td",
														'value' => (isset($fields->$InputBox) ? round($fields->$InputBox) : set_value($InputBox)),
														'class'  => 'form-control number-field percentageField',
														"data-popover" => "true",
														"data-content" => "",
													); ?>
													<div class="input-group">
														<?=form_input($field)?>
                                                        <div class="input-group-addon">%</div>
                                                    </div>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                            <input type="hidden" name="TableName" value="rental_advance" />
                        
                        <?=form_close()?>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            	<a class="modal-help pull-left" data-toggle="tooltip" data-placement="top" title="Need help" style="color:#000;">
                    <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                </a>
            	<button type="button" id="GrowthModalReturn" class="btn btn-primary">RETURN TO ANALYSIS</button>
            </div>
        </div>
    </div>
</div>
<!-- Closing Costs Modal -->