<!-- Closing Costs Modal -->
<div class="modal fade" id="MoreOptionModal" tabindex="-1" role="dialog" aria-labelledby="MoreOptionModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
                <h4 class="modal-title text-center">
                	Planned Capital Improvements
                    <a class="modal-help pull-right" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                	<div class="row">
                    	
                        <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                        <?=form_open(base_url('pages/rental-calculation') . '/', $attributes)?>
                        	
                            <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                            <input type="hidden" name="UserID" value="<?=$user->id?>" />
                            <div class="panel-field" id="OverrideTableRow" style="overflow:hidden;">
                            	<div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tr>
                                        	<th colspan="2"></th>
                                            <?php for ( $td = 0; $td <= 40; $td++ ) { ?>
                                                <th class="text-center"><?php echo $td; ?></th>
                                            <?php } ?>
                                        </tr>
                                        <tr id="date_show"></tr>
                                        <tr>
                                        	<td colspan="3"><span style="width:140px;float:left;">Description of Capital Improvement</span></td>
                                            <?php for ( $td = 1; $td <= 30; $td++ ) {
												$InputBox = "Purpose_$td"; ?>
                                                <td>
                                                	<?php 
													$field = array(
														'name'  => "Purpose_$td",
														'id'    => "Purpose_$td",
														'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
														'class'  => 'form-control',
														"data-popover" => "true",
														"data-content" => "",
													); ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                        	<td colspan="3"><span style="width:140px;float:left;">Cost</span></td>
                                            <?php for ( $td = 1; $td <= 30; $td++ ) {
												$InputBox = "CapitalImprovementSchedule_$td"; ?>
                                                <td>
                                                	<?php 
													$field = array(
														'name'  => "CapitalImprovementSchedule_$td",
														'id'    => "CapitalImprovementSchedule_$td",
														'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
														'class'  => 'form-control number-field',
														"data-popover" => "true",
														"data-content" => "",
													); ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                        	<td colspan="3"><strong style="width:140px;float:left;">Fair Market Value Multiplier</strong></td>
                                            <?php for ( $td = 1; $td <= 30; $td++ ) {
												$InputBox = "FairMarketValue_$td"; ?>
                                                <td>
                                                	<?php 
													$field = array(
														'name'  => "FairMarketValue_$td",
														'id'    => "FairMarketValue_$td",
														'value' => (isset($fields->$InputBox) ? $fields->$InputBox : 1),
														'class'  => 'form-control number-field',
														"data-popover" => "true",
														"data-content" => "",
													); ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                            <td colspan="10"></td>
                                        </tr>
                                        <tr>
                                        	<td colspan="2"><strong style="width:140px;float:left;">Total Cash Outlay</strong></td>
                                            <?php for ( $td = 0; $td <= 40; $td++ ) {
												$InputBox = "TotalCashOutlay_$td"; ?>
                                                <td>
                                                	<?php 
													$field = array(
														'name'  => "TotalCashOutlay_$td",
														'id'    => "TotalCashOutlay_$td",
														'value' => (isset($fields->$InputBox) ? $fields->$InputBox : set_value($InputBox)),
														'class'  => 'form-control number-field',
														"data-popover" => "true",
														"data-content" => "",
														"readonly" => "readonly",
													); ?>
													<?=form_input($field)?>
                                                </td>
                                            <?php } ?>
                                        </tr>
                                    </table>
                                </div>
                            </div>

                            <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                            <input type="hidden" name="TableName" value="rental_advance" />
                        
                        <?=form_close()?>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            	<button type="button" id="AdvaceModalReturn" class="btn btn-primary">RETURN TO ANALYSIS</button>
            </div>
        </div>
    </div>
</div>
<!-- Closing Costs Modal -->