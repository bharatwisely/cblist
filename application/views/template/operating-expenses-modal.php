<!-- Section1: Operating Income (Rent) Modal -->
<div class="modal fade" id="OperatingExpensesModal" tabindex="-1" role="dialog" aria-labelledby="OperatingExpensesModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header text-center">
                <h4 class="modal-title" id="OperatingExpensesModalLabel">
                	Operating Expenses
                    <a class="modal-help" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body">
            	<?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
				<?=form_open(base_url('pages/proposal-writer') . '/', $attributes)?>
            		
                    <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                    <input type="hidden" name="UserID" value="<?=$user->id?>" />
                    <div class="row">    
                        <div class="col-lg-12">
                            <table width="100%" class="table">
                                <thead>
                                    <tr>
                                        <th width="32%">&nbsp;</th>
                                        <th width="25%">Monthly</th>
                                        <th width="25%">Annual</th>
                                        <th width="18%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOption_1" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOption_1', $options, 'Taxes', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthly_1",
                                                'id'    => "OperatingExpensesMonthly_1",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthly_1"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnual_1",
                                                'id'    => "OperatingExpensesAnnual_1",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnual_1"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentage_1",
                                                'id'    => "OperatingExpensesPercentage_1",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentage_1"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOption_2" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOption_2', $options, 'Insurance', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthly_2",
                                                'id'    => "OperatingExpensesMonthly_2",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthly_2"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnual_2",
                                                'id'    => "OperatingExpensesAnnual_2",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnual_2"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentage_2",
                                                'id'    => "OperatingExpensesPercentage_2",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentage_2"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOption_3" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOption_3', $options, 'Landscaping', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthly_3",
                                                'id'    => "OperatingExpensesMonthly_3",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthly_3"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnual_3",
                                                'id'    => "OperatingExpensesAnnual_3",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnual_3"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentage_3",
                                                'id'    => "OperatingExpensesPercentage_3",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentage_3"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOption_4" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOption_4', $options, 'Super', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthly_4",
                                                'id'    => "OperatingExpensesMonthly_4",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthly_4"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnual_4",
                                                'id'    => "OperatingExpensesAnnual_4",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnual_4"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentage_4",
                                                'id'    => "OperatingExpensesPercentage_4",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentage_4"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOption_5" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOption_5', $options, 'Garbage', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthly_5",
                                                'id'    => "OperatingExpensesMonthly_5",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthly_5"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnual_5",
                                                'id'    => "OperatingExpensesAnnual_5",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnual_5"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentage_5",
                                                'id'    => "OperatingExpensesPercentage_5",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentage_5"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOption_6" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOption_6', $options, 'Exterminator', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthly_6",
                                                'id'    => "OperatingExpensesMonthly_6",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthly_6"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnual_6",
                                                'id'    => "OperatingExpensesAnnual_6",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnual_6"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentage_6",
                                                'id'    => "OperatingExpensesPercentage_6",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentage_6"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOption_7" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOption_7', $options, 'Snow Removal', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthly_7",
                                                'id'    => "OperatingExpensesMonthly_7",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthly_7"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnual_7",
                                                'id'    => "OperatingExpensesAnnual_7",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnual_7"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentage_7",
                                                'id'    => "OperatingExpensesPercentage_7",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentage_7"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOption_8" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOption_8', $options, 'Accounting', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthly_8",
                                                'id'    => "OperatingExpensesMonthly_8",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthly_8"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnual_8",
                                                'id'    => "OperatingExpensesAnnual_8",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnual_8"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentage_8",
                                                'id'    => "OperatingExpensesPercentage_8",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentage_8"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOption_9" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOption_9', $options, 'Legal', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthly_9",
                                                'id'    => "OperatingExpensesMonthly_9",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthly_9"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnual_9",
                                                'id'    => "OperatingExpensesAnnual_9",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnual_9"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentage_9",
                                                'id'    => "OperatingExpensesPercentage_9",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentage_9"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOption_10" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOption_10', $options, 'Management', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthly_10",
                                                'id'    => "OperatingExpensesMonthly_10",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthly_10"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnual_10",
                                                'id'    => "OperatingExpensesAnnual_10",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnual_10"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentage_10",
                                                'id'    => "OperatingExpensesPercentage_10",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentage_10"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        	<?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOptionOther_1" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOptionOther_1', $options, 'Advertising', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthlyOther_1",
                                                'id'    => "OperatingExpensesMonthlyOther_1",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthlyOther_1"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnualOther_1",
                                                'id'    => "OperatingExpensesAnnualOther_1",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnualOther_1"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentageOther_1",
                                                'id'    => "OperatingExpensesPercentageOther_1",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentageOther_1"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        	<?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOptionOther_2" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOptionOther_2', $options, 'Repairs & Reserve', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthlyOther_2",
                                                'id'    => "OperatingExpensesMonthlyOther_2",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthlyOther_2"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnualOther_2",
                                                'id'    => "OperatingExpensesAnnualOther_2",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnualOther_2"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentageOther_2",
                                                'id'    => "OperatingExpensesPercentageOther_2",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentageOther_2"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        	<?php 
                                            $options = array(
                                                '' => '',
												'Accounting' => 'Accounting',
												'Advertising' => 'Advertising',
												'Bank Charges' => 'Bank Charges',
												'Condo Association Fees' => 'Condo Association Fees',
												'Exterminator' => 'Exterminator',
												'Furniture Rental' => 'Furniture Rental',
												'Garbage' => 'Garbage',
												'General Admin' => 'General Admin',
												'Insurance' => 'Insurance',
												'Janitorial' => 'Janitorial',
												'Landscaping' => 'Landscaping',
												'Legal' => 'Legal',
												'Licenses' => 'Licenses',
												'Management' => 'Management',
												'Miscellaneous' => 'Miscellaneous',
												'Other' => 'Other',
												'Phone' => 'Phone',
												'Referalls or commissions' => 'Referalls or commissions',
												'Repairs & Maintenance' => 'Repairs & Maintenance',
												'Repairs & Reserve' => 'Repairs & Reserve',
												'Resident Superintendent' => 'Resident Superintendent',
												'Salaries & Related' => 'Salaries & Related',
												'Sewer Expense' => 'Sewer Expense', 
												'Snow Removal' => 'Snow Removal',
												'Super' => 'Super',
												'Supplies' => 'Supplies',
												'Supplies Office' => 'Supplies Office',																				
												'Taxes' => 'Taxes',
												'Warranties' => 'Warranties',
                                            );
                                            $attrb = 'id="OperatingExpensesOptionOther_3" class="form-control"'; ?>
                                            <?=form_dropdown('OperatingExpensesOptionOther_3', $options, 'Miscellaneous', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthlyOther_3",
                                                'id'    => "OperatingExpensesMonthlyOther_3",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthlyOther_3"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnualOther_3",
                                                'id'    => "OperatingExpensesAnnualOther_3",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnualOther_3"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentageOther_3",
                                                'id'    => "OperatingExpensesPercentageOther_3",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentageOther_3"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesOptionOther_4",
                                                'id'    => "OperatingExpensesOptionOther_4",
                                                'placeholder' => "Other (can edit this)",
                                                'class'	=> 'form-control',
                                                'style' => 'text-align:left;',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthlyOther_4",
                                                'id'    => "OperatingExpensesMonthlyOther_4",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthlyOther_4"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnualOther_4",
                                                'id'    => "OperatingExpensesAnnualOther_4",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnualOther_4"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentageOther_4",
                                                'id'    => "OperatingExpensesPercentageOther_4",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentageOther_4"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="padding:5px 0;text-align:left;" colspan="4"><strong>Utilities:</strong></td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">&nbsp;&nbsp;Water/Sewer</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesWaterSewerMonthly",
                                                'id'    => "OperatingExpensesUtilitiesWaterSewerMonthly",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesWaterSewerMonthly"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesWaterSewerAnnual",
                                                'id'    => "OperatingExpensesUtilitiesWaterSewerAnnual",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesWaterSewerAnnual"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesWaterSewerPercentage",
                                                'id'    => "OperatingExpensesUtilitiesWaterSewerPercentage",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesWaterSewerPercentage"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">&nbsp;&nbsp;Electricity</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesElectricityMonthly",
                                                'id'    => "OperatingExpensesUtilitiesElectricityMonthly",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesElectricityMonthly"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesElectricityAnnual",
                                                'id'    => "OperatingExpensesUtilitiesElectricityAnnual",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesElectricityAnnual"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesElectricityPercentage",
                                                'id'    => "OperatingExpensesUtilitiesElectricityPercentage",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesElectricityPercentage"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">&nbsp;&nbsp;Gas</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesGasMonthly",
                                                'id'    => "OperatingExpensesUtilitiesGasMonthly",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesGasMonthly"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesGasAnnual",
                                                'id'    => "OperatingExpensesUtilitiesGasAnnual",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesGasAnnual"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesGasPercentage",
                                                'id'    => "OperatingExpensesUtilitiesGasPercentage",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesGasPercentage"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">&nbsp;&nbsp;Fuel Oil</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesFuelOilMonthly",
                                                'id'    => "OperatingExpensesUtilitiesFuelOilMonthly",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesFuelOilMonthly"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesFuelOilAnnual",
                                                'id'    => "OperatingExpensesUtilitiesFuelOilAnnual",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesFuelOilAnnual"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesFuelOilPercentage",
                                                'id'    => "OperatingExpensesUtilitiesFuelOilPercentage",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesFuelOilPercentage"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">&nbsp;&nbsp;Other Utilities</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesOtherUtilitiesMonthly",
                                                'id'    => "OperatingExpensesUtilitiesOtherUtilitiesMonthly",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesOtherUtilitiesMonthly"),
                                                'class'	=> 'form-control OperatingExpenses_input number-field NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesOtherUtilitiesAnnual",
                                                'id'    => "OperatingExpensesUtilitiesOtherUtilitiesAnnual",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesOtherUtilitiesAnnual"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesUtilitiesOtherUtilitiesPercentage",
                                                'id'    => "OperatingExpensesUtilitiesOtherUtilitiesPercentage",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesUtilitiesOtherUtilitiesPercentage"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td><strong>Total Operating expenses</strong></td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesMonthlyTotal",
                                                'id'    => "OperatingExpensesMonthlyTotal",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesMonthlyTotal"),
                                                'class'	=> 'form-control OperatingExpenses_input',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesAnnualTotal",
                                                'id'    => "OperatingExpensesAnnualTotal",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesAnnualTotal"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OperatingExpensesPercentageTotal",
                                                'id'    => "OperatingExpensesPercentageTotal",
                                                'value' => ($reset) ? "" : set_value("OperatingExpensesPercentageTotal"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                    <input type="hidden" name="TableName" value="operating_expenses" />
                    
                <?=form_close()?>
            </div>
            <div class="modal-footer">
            	<button type="button" id="OperatingExpensesReturn" class="btn btn-primary">RETURN TO ANALYSIS</button>
            </div>
        </div>
    </div>
</div>
<!-- Rehub Budget Flip Analysis Modal -->