    <div class="row">
        <div class="col-lg-12 text-center">
            <button type="button" class="btn btn-sm btn-default noprint" onClick="window.close();">
                <i class="fa fa-close"></i> CLOSE</button>
            </button>
            <a href="<?=base_url('pages/download-report/' . $this->uri->segment(3) . '/' . $this->uri->segment(4))?>">
                <button type="button" class="btn btn-sm btn-danger noprint">
                    <i class="fa fa-download"></i> DOWNLOAD PDF</button>
                </button>
            </a>
            <button type="button" class="btn btn-sm btn-primary noprint" onClick="window.print();">
                <i class="fa fa-print"></i> PRINT REPORT</button>
            </button>
            <button type="button" class="btn btn-sm btn-info noprint btn_proposal_email" data-type="proposal" data-url="<?=base_url('pages/download-report/' . $this->uri->segment(3) . '/' . $this->uri->segment(4))?>">
                <i class="fa fa-envelope"></i> EMAIL REPORT</button>
            </button>
        </div>
    </div><br>
</div>
<?php $this->load->view('template/email-modal'); ?>
</body>
</html>