<!DOCTYPE html>
<!--[if IE 8]><html lang="en" class="ie8"><![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"><![endif]-->
<!--[if !IE]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?=$page_title?></title>
<link href="<?=base_url('favicon.png')?>" rel="shortcut icon" />
<link href="<?=base_url('assets/css/plugins/bootstrap/bootstrap.min.css')?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?=base_url('assets/css/plugins/font-awesome/font-awesome.min.css')?>" rel="stylesheet" type="text/css" media="all" />
<link href="<?=base_url('assets/css/fonts.css')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/css/print-css.css')?>" rel="stylesheet" type="text/css" media="print" />
<script src="<?=base_url('assets/js/plugins/jquery/jquery.min.js')?>"></script>
<script src="<?=base_url('assets/js/plugins/bootstrap/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/js/rental-report.js'); ?>"></script>
<script type="text/javascript">
$(document).ready(function(e) {
    //	print proposal 
	$('.btn_proposal_email').click(function(){
		$('#attchments_views').html('');
		$('#attchments_files').html('');
		var ajax_request = [];
		var type = $(this).attr('data-type');
		var url = $(this).attr('data-url');
		$.ajax({
			type: 'POST',
			dataType: "json",
			url: url + "/save/" + type + "/",
			data: {},
			success: function(data) {
				$('#modalEmail').modal('show');
				$('.modal-loading').html('<span class="text-danger"><img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" /> Please wait, attaching the reports to your email.</span>');
				$('#attchments_files').append('<input type="hidden" name="attachments[]" value="' + data.pdf_path + '" />');
				$('#attchments_views').append('<div class="col-lg-3 pdf-attach"><a target="_blank" href="' + data.pdf_url + '"><i class="fa fa-file-pdf-o"></i> ' + data.pdf_name + '</a></div>');
				$('#inputSubject').val(data.propertyName + ' - CBList Reports');
				$('.modal-loading').html('<span class="text-success"><i class="fa fa-check-square-o"></i> Reports are successfully attached.</span>');
			},
		});
	});
	
	$('#btn_email_send').click(function() {
		var inputTo = $('#inputTo').val();
		if ( inputTo == '' || inputTo == false ) {
			$('.modal-loading').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Please specify at least one recipient.</span>');
			return false;
		}
		
		$('.modal-loading').html('<span class="text-danger"><img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" /> Please wait, sending email.</span>');
		var dataString = $('#email_form').serialize();
		$.ajax({
			type: 'POST',
			url: '<?=base_url("pages/email-proposal/")?>',
			data: dataString,
			success: function(data) {
				if ( data == 'sent' ) {
					$('#email_form')[0].reset();
					$('#attchments_views').html('');
					$('#attchments_files').html('');
					$('.modal-loading').html('<span class="text-success"><i class="fa fa-check-square-o"></i> Email successfully sent.</span>');
					setTimeout(function() {
						$('.modal-loading').html('');
					}, 10000);
				} else {
					$('.modal-loading').html('<span class="text-danger"><i class="fa fa-exclamation-triangle"></i> Something went wrong, please try again.</span>');
				}
			},
		});
    });
});
</script>
<style type="text/css">
html, body {
	background-color:#e1e1e1;
	font-family: 'Open Sans Regular';
	font-size:12px;
	color:#000;
}
.table.table-bg {
	background:#F7F4F4;
}
.table.table-borderless td, .table.table-borderless th {
	border: 0 !important;
} 
.table.table-borderless {
	margin-bottom: 0;
}
.table-bordered {
	border-color:#ccc;
}
.table-bordered > thead > tr > th, .table-bordered > thead > tr > td {
	border-bottom-width:1px;
	border-color:#ccc;
	word-break:break-all;
}
.table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
	border-color:#ccc;
}
hr {
	margin:10px 0;
}
.form-control{margin-bottom:0;height:28px;padding:0 5px;border:none;font-size:12px;border:1px solid #ccc;}
.CoverPage, .SinglePagePropertySummer, .PicsPage, .AnnualPropertyOperating, .RentReport, .OperatingExpensesReport, .IncomeStatement, .RentalCashFlowReport, .DiscountCashFlowAnalysis, .LenderSummary, .RentalRefiAnalysis, .FullTaxTreatment, .PropertyValueEvolution, .FinancialRatiosReport {
	border:15px solid #fff;
	margin-bottom:100px;
	padding-bottom:15px;
	background-color:#fff;
	page-break-after:always
}
.btn {
	border-radius: 0;
}
.title-border {
	border:1px solid #ccc;
	padding:5px 0;
	margin-top:0;
	background-color:#eee;
	text-transform:uppercase;
}
.bg-border {
	border:1px solid #ccc;
}
.top-bg-border {
	border-top:1px solid #ccc;
}
.img-thumbnail {
	margin-bottom:5px;
	border-radius: 0;
	max-width:100%;
}
.thumbnail {
	height:200px;
	margin-bottom:5px;
	border-radius: 0;
}
</style>
</head>
<body>
<div class="container-fluid">
	<div class="row"><br>
        <div class="col-lg-12 text-center">
            <button type="button" class="btn btn-sm btn-default noprint" onClick="window.close();">
                <i class="fa fa-close"></i> CLOSE</button>
            </button>
            <a href="<?=base_url('pages/download-report/' . $this->uri->segment(3) . '/' . $this->uri->segment(4))?>">
                <button type="button" class="btn btn-sm btn-danger noprint">
                    <i class="fa fa-download"></i> DOWNLOAD PDF</button>
                </button>
            </a>
            <button type="button" class="btn btn-sm btn-primary noprint" onClick="window.print();">
                <i class="fa fa-print"></i> PRINT REPORT</button>
            </button>
            <button type="button" class="btn btn-sm btn-info noprint btn_proposal_email" data-type="rental" data-url="<?=base_url('pages/download-report/' . $this->uri->segment(3) . '/' . $this->uri->segment(4))?>">
                <i class="fa fa-envelope"></i> EMAIL REPORT</button>
            </button>
        </div>
    </div><br>