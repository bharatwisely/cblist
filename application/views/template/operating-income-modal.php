<!-- Section1: Operating Income (Rent) Modal -->
<div class="modal fade" id="OperatingIncomeModal" tabindex="-1" role="dialog" aria-labelledby="OperatingIncomeModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header text-center">
                <h4 class="modal-title" id="OperatingIncomeModalLabel">
                	Operating Income/Rent
                    <a class="modal-help" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body">
            	<?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
				<?=form_open(base_url('pages/proposal-writer') . '/', $attributes)?>
                
                	<input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                    <input type="hidden" name="UserID" value="<?=$user->id?>" />
                    <div class="row">    
                        <div class="col-lg-12">
                            <table width="100%" class="table">
                                <thead>
                                    <tr>
                                        <th width="8%">Unit Ref #</th>
                                        <th width="10%"># of units</th>
                                        <th width="25%">Unit type</th>
                                        <th width="15%">Square Ft.</th>
                                        <th width="20%">Monthly Rent</th>
                                        <th width="20%">Annual Rent</th>
                                        <th width="15%">&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_1",
                                                'id'    => "NoOfUnits_1",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_1"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_1" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_1', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_1",
                                                'id'    => "SquareFt_1",
                                                'value' => ($reset) ? "" : set_value("SquareFt_1"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_1",
                                                'id'    => "MonthlyRent_1",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_1"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_1",
                                                'id'    => "AnnualRent_1",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_1"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_1",
                                                'id'    => "AnnualPercent_1",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_1"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>2</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_2",
                                                'id'    => "NoOfUnits_2",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_2"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_2" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_2', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_2",
                                                'id'    => "SquareFt_2",
                                                'value' => ($reset) ? "" : set_value("SquareFt_2"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_2",
                                                'id'    => "MonthlyRent_2",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_2"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_2",
                                                'id'    => "AnnualRent_2",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_2"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_2",
                                                'id'    => "AnnualPercent_2",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_2"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>3</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_3",
                                                'id'    => "NoOfUnits_3",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_3"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_3" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_3', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_3",
                                                'id'    => "SquareFt_3",
                                                'value' => ($reset) ? "" : set_value("SquareFt_3"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_3",
                                                'id'    => "MonthlyRent_3",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_3"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_3",
                                                'id'    => "AnnualRent_3",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_3"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_3",
                                                'id'    => "AnnualPercent_3",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_3"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>4</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_4",
                                                'id'    => "NoOfUnits_4",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_4"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_4" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_4', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_4",
                                                'id'    => "SquareFt_4",
                                                'value' => ($reset) ? "" : set_value("SquareFt_4"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_4",
                                                'id'    => "MonthlyRent_4",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_4"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_4",
                                                'id'    => "AnnualRent_4",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_4"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_4",
                                                'id'    => "AnnualPercent_4",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_4"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>5</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_5",
                                                'id'    => "NoOfUnits_5",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_5"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_5" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_5', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_5",
                                                'id'    => "SquareFt_5",
                                                'value' => ($reset) ? "" : set_value("SquareFt_5"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_5",
                                                'id'    => "MonthlyRent_5",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_5"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_5",
                                                'id'    => "AnnualRent_5",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_5"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_5",
                                                'id'    => "AnnualPercent_5",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_5"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>6</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_6",
                                                'id'    => "NoOfUnits_6",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_6"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_6" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_6', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_6",
                                                'id'    => "SquareFt_6",
                                                'value' => ($reset) ? "" : set_value("SquareFt_6"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_6",
                                                'id'    => "MonthlyRent_6",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_6"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_6",
                                                'id'    => "AnnualRent_6",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_6"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_6",
                                                'id'    => "AnnualPercent_6",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_6"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>7</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_7",
                                                'id'    => "NoOfUnits_7",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_7"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_7" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_7', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_7",
                                                'id'    => "SquareFt_7",
                                                'value' => ($reset) ? "" : set_value("SquareFt_7"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_7",
                                                'id'    => "MonthlyRent_7",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_7"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_7",
                                                'id'    => "AnnualRent_7",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_7"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_7",
                                                'id'    => "AnnualPercent_7",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_7"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>8</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_8",
                                                'id'    => "NoOfUnits_8",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_8"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_8" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_8', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_8",
                                                'id'    => "SquareFt_8",
                                                'value' => ($reset) ? "" : set_value("SquareFt_8"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_8",
                                                'id'    => "MonthlyRent_8",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_8"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_8",
                                                'id'    => "AnnualRent_8",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_8"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_8",
                                                'id'    => "AnnualPercent_8",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_8"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>9</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_9",
                                                'id'    => "NoOfUnits_9",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_9"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_9" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_9', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_9",
                                                'id'    => "SquareFt_9",
                                                'value' => ($reset) ? "" : set_value("SquareFt_9"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_9",
                                                'id'    => "MonthlyRent_9",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_9"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_9",
                                                'id'    => "AnnualRent_9",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_9"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_9",
                                                'id'    => "AnnualPercent_9",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_9"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>10</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_10",
                                                'id'    => "NoOfUnits_10",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_10"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_10" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_10', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_10",
                                                'id'    => "SquareFt_10",
                                                'value' => ($reset) ? "" : set_value("SquareFt_10"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_10",
                                                'id'    => "MonthlyRent_10",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_10"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_10",
                                                'id'    => "AnnualRent_10",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_10"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_10",
                                                'id'    => "AnnualPercent_10",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_10"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>11</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_11",
                                                'id'    => "NoOfUnits_11",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_11"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_11" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_11', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_11",
                                                'id'    => "SquareFt_11",
                                                'value' => ($reset) ? "" : set_value("SquareFt_11"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_11",
                                                'id'    => "MonthlyRent_11",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_11"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_11",
                                                'id'    => "AnnualRent_11",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_11"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_11",
                                                'id'    => "AnnualPercent_11",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_11"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>12</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnits_12",
                                                'id'    => "NoOfUnits_12",
                                                'value' => ($reset) ? "" : set_value("NoOfUnits_12"),
                                                'class'	=> 'form-control NoOfUnits_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $options = array(
                                                ''     => '',
                                                '1br'  => '1br',
                                                '2br'  => '2br',
                                                '3br'  => '3br',
                                                '4br'  => '4br',
                                                '5br+' => '5br+',
												'Studio' => 'Studio',
												'Storage' => 'Storage',
												'Garage' => 'Garage',
												'Retail Store' => 'Retail Store',
                                            );
                                            $attrb = 'id="UnitType_12" class="form-control"'; ?>
                                            <?=form_dropdown('UnitType_12', $options, '', $attrb)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_12",
                                                'id'    => "SquareFt_12",
                                                'value' => ($reset) ? "" : set_value("SquareFt_12"),
                                                'class'	=> 'form-control SquareFt_input NumberOnly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_12",
                                                'id'    => "MonthlyRent_12",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_12"),
                                                'class'	=> 'form-control MonthlyRent_input NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_12",
                                                'id'    => "AnnualRent_12",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_12"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_12",
                                                'id'    => "AnnualPercent_12",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_12"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <td>Total</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "NoOfUnit_Total",
                                                'id'    => "NoOfUnit_Total",
                                                'value' => ($reset) ? "" : set_value("NoOfUnit_Total"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "SquareFt_Total",
                                                'id'    => "SquareFt_Total",
                                                'value' => ($reset) ? "" : set_value("SquareFt_Total"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <?=form_input($field)?>
                                        </td>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td rowspan="5">&nbsp;</td>
                                        <td colspan="3">Gross Schedule Income</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyRent_Gross",
                                                'id'    => "MonthlyRent_Gross",
                                                'value' => ($reset) ? "" : set_value("MonthlyRent_Gross"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualRent_Gross",
                                                'id'    => "AnnualRent_Gross",
                                                'value' => ($reset) ? "" : set_value("AnnualRent_Gross"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualPercent_Gross",
                                                'id'    => "AnnualPercent_Gross",
                                                'value' => ($reset) ? "" : set_value("AnnualPercent_Gross"),
                                                'class'	=> 'form-control percentageField',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">VACANCY LOSS</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "VacancyLossPercent",
                                                'id'    => "VacancyLossPercent",
                                                'value' => ($reset) ? "" : set_value("VacancyLossPercent"),
                                                'class'	=> 'form-control NumberOnly percentageField',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "VacancyLossMonthly",
                                                'id'    => "VacancyLossMonthly",
                                                'value' => ($reset) ? "" : set_value("VacancyLossMonthly"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "VacancyLossAnnual",
                                                'id'    => "VacancyLossAnnual",
                                                'value' => ($reset) ? "" : set_value("VacancyLossAnnual"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td rowspan="4">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">MANAGEMENT</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "ManagementPercent",
                                                'id'    => "ManagementPercent",
                                                'value' => ($reset) ? "" : set_value("ManagementPercent"),
                                                'class'	=> 'form-control NumberOnly percentageField',
                                            ); ?>
                                            <div class="input-group">
	                                            <?=form_input($field)?>
                                                <div class="input-group-addon">%</div>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "ManagementMonthly",
                                                'id'    => "ManagementMonthly",
                                                'value' => ($reset) ? "" : set_value("ManagementMonthly"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "ManagementAnnual",
                                                'id'    => "ManagementAnnual",
                                                'value' => ($reset) ? "" : set_value("ManagementAnnual"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">Other Income</td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OtherIncomeMonthly",
                                                'id'    => "OtherIncomeMonthly",
                                                'value' => ($reset) ? "" : set_value("OtherIncomeMonthly"),
                                                'class'	=> 'form-control NumberOnly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "OtherIncomeAnnual",
                                                'id'    => "OtherIncomeAnnual",
                                                'value' => ($reset) ? "" : set_value("OtherIncomeAnnual"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><strong>Gross Operating Income (Effective Gross Inc)</strong></td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "MonthlyGrossOperatingIncome",
                                                'id'    => "MonthlyGrossOperatingIncome",
                                                'value' => ($reset) ? "" : set_value("MonthlyGrossOperatingIncome"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                        <td>
                                            <?php 
                                            $field = array(
                                                'name'  => "AnnualGrossOperatingIncome",
                                                'id'    => "AnnualGrossOperatingIncome",
                                                'value' => ($reset) ? "" : set_value("AnnualGrossOperatingIncome"),
                                                'class'	=> 'form-control',
                                                'readonly' => 'readonly',
                                            ); ?>
                                            <div class="input-group">
                                            	<div class="input-group-addon">$</div>
	                                            <?=form_input($field)?>
                                            </div>
                                        </td>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
	                <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                    <input type="hidden" name="TableName" value="operating_income" />
                    
                <?=form_close()?>
            </div>
            <div class="modal-footer">
            	<button type="button" id="OperatingIncomeReturn" class="btn btn-primary">PROCEED TO EXPENSE INPUT</button>
            </div>
        </div>
    </div>
</div>
<!-- Rehub Budget Flip Analysis Modal -->