<?php $this->load->view('template/modals'); ?>

<!-- Bootstrap API Library -->
<script src="<?=base_url('assets/js/plugins/bootstrap/bootstrap.min.js')?>"></script>
<!-- Custom function js -->
<script src="<?=base_url('assets/js/functions.js')?>"></script>
<!-- Bootstrap Progress Bar Library -->
<script src="<?=base_url('assets/js/plugins/bootstrap/bootstrap-progressbar.js')?>"></script>
<!-- Bootstrap Datepicker Library -->
<script src="<?=base_url('assets/js/plugins/bootstrap/bootstrap-datepicker.js')?>"></script>
<!-- Bootstrap Switch Library -->
<script src="<?=base_url('assets/js/plugins/bootstrap/bootstrap-switch.min.js')?>"></script>
<!-- Bootstrap Slider Library -->
<script src="<?=base_url('assets/js/plugins/bootstrap/bootstrap-slider.js')?>"></script>
<!-- Bootstrap Star Rating Library -->
<script src="<?=base_url('assets/js/plugins/bootstrap/bootstrap-star-rating.min.js')?>"></script>
<!-- DataTablses jQuery Library -->
<script src="<?=base_url('assets/js/plugins/datatables/jquery.dataTables.js')?>"></script>
<script src="<?=base_url('assets/js/plugins/datatables/dataTables.bootstrap.js')?>"></script>
<!-- jQuery Character Counter Scripts -->
<script src="<?=base_url('assets/js/plugins/char-count/jquery.char-count.js')?>"></script>
<!-- Flex Slider Library -->
<script src="<?=base_url('assets/js/plugins/flexslider/jquery.flexslider-min.js')?>"></script>
<!-- Mousewheel Library -->
<script src="<?=base_url('assets/js/plugins/mousewheel/jquery.mousewheel-3.0.6.pack.js')?>"></script>
<!-- Fancybox Library -->
<script src="<?=base_url('assets/js/plugins/fancybox/jquery.fancybox.pack.js?v=2.1.5')?>"></script>
<script src="<?=base_url('assets/js/plugins/fancybox/jquery.fancybox-buttons.js?v=1.0.5')?>"></script>
<script src="<?=base_url('assets/js/plugins/fancybox/jquery.fancybox-media.js?v=1.0.6')?>"></script>
<script src="<?=base_url('assets/js/plugins/fancybox/jquery.fancybox-thumbs.js?v=1.0.7')?>"></script>
<!-- Morris.js charts -->
<script src="<?=base_url('assets/js/plugins/nanoscroller/jquery.nanoscroller.min.js')?>"></script>
<!-- Morris.js charts -->
<script src="<?=base_url('assets/js/plugins/morris/raphael-min.js')?>"></script>
<script src="<?=base_url('assets/js/plugins/morris/morris.min.js')?>"></script>
<script src="<?=base_url('assets/js/chart.js')?>"></script>
<!-- Responside Sidebar menu API -->
<script src="<?=base_url('assets/js/plugins/sidr/jquery.sidr.min.js')?>"></script>
<!-- Ajax file upload API -->
<script src="<?=base_url('assets/js/plugins/form/jquery.form.min.js')?>"></script>

<script src="<?=base_url('assets/js/common.js')?>"></script>
<?php if ( $this->uri->segment(2) == 'proposal-writer' || $this->uri->segment(2) == 'edit-proposal' || $this->uri->segment(2) == 'offer-to-proposal' ) { ?>
    <!-- Rehub Valuator Script -->
    <script src="<?=base_url('assets/js/rehab-valuator.js')?>"></script>
    <script src="<?=base_url('assets/js/flip-analysis.js')?>"></script>
    <script src="<?=base_url('assets/js/refi-analysis.js')?>"></script>
    <script src="<?=base_url('assets/js/operating-income.js')?>"></script>
    <script src="<?=base_url('assets/js/operating-expense.js')?>"></script>
<?php } ?>

<?php if ( $this->uri->segment(2) == 'offer-calculator' || $this->uri->segment(2) == 'edit-offer' ) { ?>
	<!-- Offer Calculator Script -->
	<script src="<?=base_url('assets/js/offer-calculator.js')?>"></script>
<?php } ?>

<?php if ( $this->uri->segment(2) == 'rental-calculation' || $this->uri->segment(2) == 'edit-rental' ) { ?>
	<!-- Rental Calculation Script -->
	<script src="<?=base_url('assets/js/rental-calculation.js')?>"></script>
<?php } ?>

<?php if ( $this->uri->segment(2) == 'comparison-analyser' ) { ?>
	<!-- Comparison Analyser Script -->
	<script src="<?=base_url('assets/js/comparison-analyser.js')?>"></script>
<?php } ?>

<?php if ( $this->uri->segment(2) == 'payment_sucessful' ) { ?>
<script type="text/javascript">
setInterval(function() {
	get_transaction_data();
1000});
</script>
<?php } ?>

<!-- Place script file -->
<script src="<?=base_url('assets/js/place.js')?>"></script>
<!-- Custom script file -->
<script src="<?=base_url('assets/js/scripts.js')?>"></script>
</body>
</html>