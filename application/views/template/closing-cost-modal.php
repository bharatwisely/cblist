<!-- Closing Costs Modal -->
<div class="modal fade" id="ClosingCostsModal" tabindex="-1" role="dialog" aria-labelledby="ClosingCostsModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header text-center">
                <h4 class="modal-title" id="ClosingCostsModalLabel">
                	Closing Costs
                    <a class="modal-help" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body">
				<p class="small text-center">*Note: these closing costs do not include any points or fees associated with a loan, which are entered in the next section!</p>
                <div class="col-lg-12 assump-box">
                	<div class="row">
                    	
                        <?php $attributes = array('class' => 'form-horizontal row', 'name' => 'PropertyForm'); ?>
                        <?=form_open(base_url('pages/proposal-writer') . '/', $attributes)?>
                        	
                            <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                            <input type="hidden" name="UserID" value="<?=$user->id?>" />
                            <div class="panel-field" id="ClosingCostsOptionRow">
                                <label class="col-lg-8">How do you want to enter your Closing Costs?</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $options = array(
                                        'Detailed Input'  => 'Detailed Input',
                                        'Quick Lump Sum'  => 'Quick Lump Sum',
                                    );
                                    $attrb = 'id="ClosingCostsOption" class="form-control"'; ?>
                                    <?=form_dropdown('ClosingCostsOption', $options, 'Detailed Input', $attrb)?>
                                </div>
                            </div>

                            <div class="panel-field" id="ItemizedForecastRow" style="display:none;">
                                <label class="col-lg-8">Enter Lump Sum Closing Cost Estimate Here</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "TotalClosingCosts",
                                        'id'    => "TotalClosingCosts",
                                        'value' => ($reset) ? "" : set_value("TotalClosingCosts"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                        'style'	=> 'display:none;',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="AttorneyClosingFeesRow">
                                <label class="col-lg-8">Attorney's Fees</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "AttorneyClosingFees",
                                        'id'    => "AttorneyClosingFees",
                                        'value' => ($reset) ? "" : set_value("AttorneyClosingFees"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="TitleSearchRow">
                                <label class="col-lg-8">Hazard Insurance</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "TitleSearch",
                                        'id'    => "TitleSearch",
                                        'value' => ($reset) ? "" : set_value("TitleSearch"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="TitleInsuranceRow">
                                <label class="col-lg-8">Title Insurance</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "TitleInsurance",
                                        'id'    => "TitleInsurance",
                                        'value' => ($reset) ? "" : set_value("TitleInsurance"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="RecordingFeesRow">
                                <label class="col-lg-8">Recording Fees</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "RecordingFees",
                                        'id'    => "RecordingFees",
                                        'value' => ($reset) ? "" : set_value("RecordingFees"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="AppraisalRow">
                                <label class="col-lg-8">Appraisal</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "Appraisal",
                                        'id'    => "Appraisal",
                                        'value' => ($reset) ? "" : set_value("Appraisal"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="RealEstateTaxesDueRow">
                                <label class="col-lg-8">Property Taxes</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "RealEstateTaxesDue",
                                        'id'    => "RealEstateTaxesDue",
                                        'value' => ($reset) ? "" : set_value("RealEstateTaxesDue"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="InspectionRow">
                                <label class="col-lg-8">Inspection</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "Inspection",
                                        'id'    => "Inspection",
                                        'value' => ($reset) ? "" : set_value("Inspection"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="ClosingOther1Row">
                                <label class="col-lg-8">Miscellaneous</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "ClosingOther1",
                                        'id'    => "ClosingOther1",
                                        'value' => ($reset) ? "" : set_value("ClosingOther1"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="ClosingOther2Row">
                                <label class="col-lg-8">Other</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "ClosingOther2",
                                        'id'    => "ClosingOther2",
                                        'value' => ($reset) ? "" : set_value("ClosingOther2"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="ClosingOther3Row">
                                <label class="col-lg-8">Other</label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "ClosingOther3",
                                        'id'    => "ClosingOther3",
                                        'value' => ($reset) ? "" : set_value("ClosingOther3"),
                                        'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel-field" id="TotalPurchaseClosingCostsRow">
                                <label class="col-lg-8"><strong>TOTAL</strong></label>
                                <div class="col-lg-4">
                                    <?php 
                                    $field = array(
                                        'name'  => "TotalPurchaseClosingCosts",
                                        'id'    => "TotalPurchaseClosingCosts",
                                        'value' => ($reset) ? "" : set_value("TotalPurchaseClosingCosts"),
                                        'class'	=> 'form-control ClosingCostsInput number-field',
                                        'readonly' => 'readonly',
                                    ); ?>
                                    <div class="input-group">
                                        <div class="input-group-addon">$</div>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                            <input type="hidden" name="TableName" value="closing_costs" />
                        
                        <?=form_close()?>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            	<button type="button" id="ClosingCostsReturn" class="btn btn-primary">RETURN TO ANALYSIS</button>
            </div>
        </div>
    </div>
</div>
<!-- Closing Costs Modal -->