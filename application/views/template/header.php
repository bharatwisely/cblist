<!DOCTYPE html>
<!--[if IE 8]><html lang="en" class="ie8"><![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"><![endif]-->
<!--[if !IE]><!--><html lang="en"><!--<![endif]-->
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<title><?=$page_title?></title>

<!-- Favicon -->
<link href="<?=base_url('favicon.png')?>" rel="shortcut icon" />

<!-- Bootstrap API Stylesheet -->
<link href="<?=base_url('assets/css/plugins/bootstrap/bootstrap.min.css')?>" rel="stylesheet" type="text/css" />
<!-- Bootstrap Progress Bar Stylesheet -->
<link href="<?=base_url('assets/css/plugins/bootstrap/bootstrap-progressbar.css')?>" rel="stylesheet" type="text/css" />
<!-- Bootstrap Datepicker Stylesheet -->
<link href="<?=base_url('assets/css/plugins/bootstrap/bootstrap-datepicker.css')?>" rel="stylesheet" type="text/css" />
<!-- Bootstrap Switch Stylesheet -->
<link href="<?=base_url('assets/css/plugins/bootstrap/bootstrap-switch.min.css')?>" rel="stylesheet" type="text/css" />
<!-- Bootstrap Slider Stylesheet -->
<link href="<?=base_url('assets/css/plugins/bootstrap/bootstrap-slider.css')?>" rel="stylesheet" type="text/css" />
<!-- Bootstrap Star Rating Stylesheet -->
<link href="<?=base_url('assets/css/plugins/bootstrap/bootstrap-star-rating.min.css')?>" rel="stylesheet" type="text/css" />
<!-- Bootstrap DataTables Stylesheet -->
<link href="<?=base_url('assets/css/plugins/datatables/dataTables.bootstrap.css')?>" rel="stylesheet" type="text/css" />
<!-- Font Awesome Stylesheet -->
<link href="<?=base_url('assets/css/plugins/font-awesome/font-awesome.min.css')?>" rel="stylesheet" type="text/css" />
<!-- Moris Chart Stylesheet -->
<link href="<?=base_url('assets/css/plugins/nanoscroller/nanoscroller.css')?>" rel="stylesheet" type="text/css" />
<!-- Moris Chart Stylesheet -->
<link href="<?=base_url('assets/css/plugins/morris/morris.css')?>" rel="stylesheet" type="text/css" />
<!-- Flex Slider Stylesheet -->
<link href="<?=base_url('assets/css/plugins/flexslider/flexslider.css')?>" rel="stylesheet" type="text/css" />
<!-- Fancybox Stylesheet -->
<link href="<?=base_url('assets/css/plugins/fancybox/jquery.fancybox.css?v=2.1.5')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/css/plugins/fancybox/jquery.fancybox-buttons.css?v=1.0.5')?>" rel="stylesheet" type="text/css" />
<link href="<?=base_url('assets/css/plugins/fancybox/jquery.fancybox-thumbs.css?v=1.0.7')?>" rel="stylesheet" type="text/css" />
<!-- Responsive Sidebar menu API -->
<link href="<?=base_url('assets/css/plugins/sidr/jquery.sidr.light.css')?>" rel="stylesheet" type="text/css" />

<!-- Main Stylesheet -->
<link href="<?=base_url('assets/css/style.css')?>" rel="stylesheet" type="text/css" />

<!-- jQuery API Library -->
<script src="<?=base_url('assets/js/plugins/jquery/jquery.min.js')?>"></script>
<!-- JWPlayer API Library -->
<script src="<?=base_url('assets/js/plugins/jwplayer/jwplayer.js')?>"></script>
<!-- Google Place API Library -->
<script src="https://maps.googleapis.com/maps/api/js?v=3.5&amp;libraries=places"></script>
<!-- Google Place Custom Scripts -->

<script type="text/javascript">
$(document).ready(function(e) {
    $('.page-content .box-body').css('min-height', $(window).height() - ($('.page-title').height() + $('.box-header').height() + 110));
});
</script>

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="<?=base_url('assets/js/plugins/html5shiv/html5shiv.min.js')?>"></script>
<script src="<?=base_url('assets/js/plugins/respond/respond.min.js')?>"></script>
<![endif]-->
</head>
<body>
<input type="hidden" id="base_url" value="<?=base_url()?>" />