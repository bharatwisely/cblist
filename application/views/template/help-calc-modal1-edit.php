<!-- Closing Costs Modal -->
<div class="modal fade" id="HelpfulCalcModal1" tabindex="-1" role="dialog" aria-labelledby="HelpfulCalcModal1Label" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
		<div class="modal-header">
                <h4 class="modal-title text-center">
                	Capital Reserves Calculator
                    <a class="modal-help pull-right" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                	<div class="row">
                    	<p><strong>Our Calculator below is simply yet effective It simply multiplies the key expenses of the property by the number of months. The greater the months the greater the reserves and the more conservative you are.</strong></p>
                        <p>How much should be set aside for replacement reserves? As always, It depends. Based on your inspection of the structure, roof mechanicals etc. prior to acquisition you will have a good estimate about what and when and how much. You can then calculate how much needs to be put aside monthly of those Capital Improvements or you may use our calculator below which simply multiplies the key expenses of the property by the number of months. The greater the months, the greater the reserves and the more conservative you are. Many lenders will also require a replacement reserve to be set aside, usually in escrow, to cover major capital expenditures over the term of the loan.</p>
                        <p><hr /></p>
                        
                        <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                        <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                            
                            <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                            <input type="hidden" name="UserID" value="<?=$user->id?>" />
                            
                            <div class="panel-field" id="MortgagePaymentsRow">
                                <div class="row">                                                                
                                    <label class="col-xs-6">Mortgage Payments</label>
                                    <div class="col-xs-6">
                                        <?php 
                                        $field = array(
                                            'name'  => "MortgagePayments",
                                            'id'    => "MortgagePayments",
                                            'value' => $fields->MortgagePayments,
                                            'class'	=> 'form-control number-field',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly'
                                        ); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- MortgagePayments -->
                            <div class="panel-field" id="InsuranceRow">
                                <div class="row">                                                                
                                    <label class="col-xs-6">Insurance</label>
                                    <div class="col-xs-6">
                                        <?php 
                                        $field = array(
                                            'name'  => "Insurance",
                                            'id'    => "Insurance",
                                            'value' => $fields->Insurance,
                                            'class'	=> 'form-control number-field',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly'
                                        ); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- Insurance -->
                            <div class="panel-field" id="RealEstateTaxesRow">
                                <div class="row">                                                                
                                    <label class="col-xs-6">Real Estate Taxes</label>
                                    <div class="col-xs-6">
                                        <?php 
                                        $field = array(
                                            'name'  => "RealEstateTaxes",
                                            'id'    => "RealEstateTaxes",
                                            'value' => $fields->RealEstateTaxes,
                                            'class'	=> 'form-control number-field',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly'
                                        ); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- RealEstateTaxes -->
                            <div class="panel-field" id="HelpfulTotalRow">
                                <div class="row">                                                                
                                    <label class="col-xs-6">Total</label>
                                    <div class="col-xs-6">
                                        <?php 
                                        $field = array(
                                            'name'  => "HelpfulTotal",
                                            'id'    => "HelpfulTotal",
                                            'value' => $fields->HelpfulTotal,
                                            'class'	=> 'form-control number-field',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly'
                                        ); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- HelpfulTotal -->                                                                
                            <div class="panel-field" id="MonthsReserveRow">
                                <div class="row">                                                                
                                    <label class="col-xs-6">Months' Reserve:</label>
                                    <div class="col-xs-6">
                                        <?php 
                                        $field = array(
                                            'name'  => "MonthsReserve",
                                            'id'    => "MonthsReserve",
                                            'value' => $fields->MonthsReserve,
                                            'class'	=> 'form-control number-field NumberOnly',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                        ); ?>
                                        <?=form_input($field)?>
                                    </div>
                                </div>
                            </div><!-- MonthsReserve -->
                            <div class="panel-field" id="AmountHoldRow">
                                <div class="row">                                                                
                                    <label class="col-xs-6">Amount to Hold:</label>
                                    <div class="col-xs-6">
                                        <?php 
                                        $field = array(
                                            'name'  => "AmountHold",
                                            'id'    => "AmountHold",
                                            'value' => $fields->AmountHold,
                                            'class'	=> 'form-control number-field',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly'
                                        ); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- AmountHold -->
                                
                            <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                            <input type="hidden" name="TableName" value="rental_helpful_calculator1" />
                        
                        <?=form_close()?>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            	<button type="button" id="HelpfullCalc1Return" class="btn btn-primary">RETURN TO ANALYSIS</button>
            </div>
        </div>
    </div>
</div>
<!-- Closing Costs Modal -->