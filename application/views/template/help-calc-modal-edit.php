<!-- Closing Costs Modal -->
<div class="modal fade" id="HelpfulCalcModal" tabindex="-1" role="dialog" aria-labelledby="HelpfulCalcModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
		<div class="modal-header">
                <h4 class="modal-title text-center">
                	POINT/RATE BREAK-EVEN CALCULATION
                    <a class="modal-help pull-right" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                	<div class="row">
                    
                        <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                        <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                            
                            <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                            <input type="hidden" name="UserID" value="<?=$user->id?>" />

                            <div class="panel-field" id="ExpectedHoldingPeriodRow">
                                <div class="row">                                                                
                                    <label class="col-xs-9">Expected Holding Period (in Years)</label>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "ExpectedHoldingPeriod",
                                            'id'    => "ExpectedHoldingPeriod",
                                            'value' => $fields->ExpectedHoldingPeriod,
                                            'class'	=> 'form-control number-field NumberOnly',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                        ); ?>
                                        <div class="input-group">
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- ExpectedHoldingPeriod -->
                            
                            <div class="panel-field" id="OptionRow">
                                <div class="row">                                                                
                                    <label class="col-xs-3"></label>
                                    <label class="col-xs-3 text-center"><strong>Option 1</strong></label>
                                    <label class="col-xs-3 text-center"><strong>Option 2</strong></label>
                                    <label class="col-xs-3 text-center"><strong>Option 3</strong></label>
                                </div>
                            </div><!-- Option -->
                            <div class="panel-field" id="OptionRateRow">
                                <div class="row">                                                                
                                    <label class="col-xs-3">Rate</label>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionOneRate",
                                            'id'    => "OptionOneRate",
                                            'value' => round($fields->OptionOneRate),
                                            'class'	=> 'form-control number-field NumberOnly percentageField',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                        ); ?>
                                        <div class="input-group">
                                            <?=form_input($field)?>
                                            <div class="input-group-addon">%</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionTwoRate",
                                            'id'    => "OptionTwoRate",
                                            'value' => round($fields->OptionTwoRate),
                                            'class'	=> 'form-control number-field NumberOnly percentageField',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                        ); ?>
                                        <div class="input-group">
                                            <?=form_input($field)?>
                                            <div class="input-group-addon">%</div>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionThreeRate",
                                            'id'    => "OptionThreeRate",
                                            'value' => round($fields->OptionThreeRate),
                                            'class'	=> 'form-control number-field NumberOnly percentageField',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                        ); ?>
                                        <div class="input-group">
                                            <?=form_input($field)?>
                                            <div class="input-group-addon">%</div>
                                        </div>
                                    </div>                                                                        
                                </div>
                            </div><!-- OptionRate -->
                            <div class="panel-field" id="OptionPointsRow">
                                <div class="row">                                                                
                                    <label class="col-xs-3">Points</label>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionOnePoints",
                                            'id'    => "OptionOnePoints",
                                            'value' => $fields->OptionOnePoints,
                                            'class'	=> 'form-control number-field NumberOnly',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                        ); ?>
                                        <div class="input-group">
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionTwoPoints",
                                            'id'    => "OptionTwoPoints",
                                            'value' => $fields->OptionTwoPoints,
                                            'class'	=> 'form-control number-field NumberOnly',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                        ); ?>
                                        <div class="input-group">
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionThreePoints",
                                            'id'    => "OptionThreePoints",
                                            'value' => $fields->OptionThreePoints,
                                            'class'	=> 'form-control number-field NumberOnly',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                        ); ?>
                                        <div class="input-group">
                                            <?=form_input($field)?>
                                        </div>
                                    </div>                                                                        
                                </div>
                            </div><!-- OptionPoints -->
                            <div class="panel-field" id="OptionRankRow">
                                <div class="row">                                                                
                                    <label class="col-xs-3">Rank (1=Best)</label>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionOneRank",
                                            'id'    => "OptionOneRank",
                                            'value' => $fields->OptionOneRank,
                                            'class'	=> 'form-control',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly',
                                        ); ?>
                                        <div class="input-group">
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionTwoRank",
                                            'id'    => "OptionTwoRank",
                                            'value' => $fields->OptionTwoRank,
                                            'class'	=> 'form-control',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly',
                                        ); ?>
                                        <div class="input-group">
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionThreeRank",
                                            'id'    => "OptionThreeRank",
                                            'value' => $fields->OptionThreeRank,
                                            'class'	=> 'form-control',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly',
                                        ); ?>
                                        <div class="input-group">
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                    <div class="col-xs-3"></div>
                                    <div class="col-xs-9">***If only comparing 2 options, set the extra ones to really high interest rates!</div>
                                </div>
                            </div><!-- OptionRank -->
                            <div class="panel-field hide" id="OptionFactorRow">
                                <div class="row">                                                                
                                    <label class="col-xs-3">Factor</label>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionOneFactor",
                                            'id'    => "OptionOneFactor",
                                            'value' => $fields->OptionOneFactor,
                                            'class'	=> 'form-control number-field',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly',
                                        ); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionTwoFactor",
                                            'id'    => "OptionTwoFactor",
                                            'value' => $fields->OptionTwoFactor,
                                            'class'	=> 'form-control number-field',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly',
                                        ); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <?=form_input($field)?>
                                        </div>
                                    </div>
                                    <div class="col-xs-3">
                                        <?php 
                                        $field = array(
                                            'name'  => "OptionThreeFactor",
                                            'id'    => "OptionThreeFactor",
                                            'value' => $fields->OptionThreeFactor,
                                            'class'	=> 'form-control number-field',
                                            'data-popover'=>'true',
                                            'data-content'=>"",
                                            'readonly' => 'readonly',
                                        ); ?>
                                        <div class="input-group">
                                            <div class="input-group-addon">$</div>
                                            <?=form_input($field)?>
                                        </div>
                                    </div>                                                                        
                                </div>
                            </div><!-- OptionFactor -->  
                                
                            <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                            <input type="hidden" name="TableName" value="rental_helpful_calculator" />
                        
                        <?=form_close()?>
                        
                    </div>
                </div>
            </div>
            <div class="modal-footer">
            	<button type="button" id="HelpfullCalcReturn" class="btn btn-primary">RETURN TO ANALYSIS</button>
            </div>
        </div>
    </div>
</div>
<!-- Closing Costs Modal -->