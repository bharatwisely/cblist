<!-- Rent Annual Growth Rate -->
<div class="modal fade" id="annual_growth_rate" tabindex="-1" role="dialog" aria-labelledby="annualgrowthRateModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="annualgrowthRateModalLabel">Rent Growth Rate Assumptions</h4>
            </div>
            <div class="modal-body text-center">
            	<div class="col-xs-10 col-xs-offset-1">
                	<p>Manually enter rents by unit type and year for years 2 through 12 and then for subsequent year use Annual Growth Rate according to the option selected below</p>
                </div>
                <div class="col-xs-10 col-xs-offset-1 form-group">
                    <button type="button" class="btn btn-md btn-default" id="ApplyAll">NO, Use Growth<br />Rate % to Project</button>&nbsp;&nbsp;&nbsp;
                    <button type="button" class="btn btn-md btn-default" id="Individually" data-target="#ManualGrowthRateModal" data-toggle="modal">YES, Manually<br />Enter</button>
                </div>
                <div class="col-md-6 col-md-offset-3 form-group hide text-left" id="ApplyAllInput">
                	<div class="input-group">
	                	<input type="text" class="form-control" id="AllGrowthRate" />
                        <div class="input-group-addon">%</div>
					</div>
                    <small id="RentRateValidate" class="text-danger hide">Input sould be limited to 0% to 100%.</small>
                </div>
				<div class="col-xs-12 text-left">
                	<div class="row">
                    	<hr />
	                	<small>Note: Entering YES here will project rents for years 2-12 based on manually entered growth and only at year 13 will the rent be projected using % growth rates entered in the operating Income(rent) input page</small>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="growth_confirm">Confirm</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Expense Annual Growth Rate -->
<div class="modal fade" id="expense_growth_rate" tabindex="-1" role="dialog" aria-labelledby="expensegrowthRateModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="expensegrowthRateModalLabel">Expense Annual Growth Rate</h4>
            </div>
            <div class="modal-body text-center">
	            <div class="col-xs-10 col-xs-offset-1">
            		<p>Apply Expense Growth Rate for all categories or apply individually?</p>
                </div>
                <div class="col-xs-10 col-xs-offset-1 form-group">
                    <button type="button" class="btn btn-md btn-default" id="ExpenseApplyAll">Apply to all</button>&nbsp;&nbsp;&nbsp;
                    <button type="button" class="btn btn-md btn-default" id="ExpenseIndividually">Individually</button>
                </div>
                <div class="col-md-6 col-md-offset-3 form-group hide text-left" id="ExpenseApplyAllInput">
                	<div class="input-group">
	                	<input type="text" class="form-control" id="ExpenseAllGrowthRate" />
                        <div class="input-group-addon">%</div>
					</div>
                    <small id="ExpenseRateValidate" class="text-danger hide">Input sould be limited to 0% to 100%.</small>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="expense_growth_confirm">Confirm</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>

<!-- Profile Help Video Modal -->
<div class="modal fade" id="profile_help_video" tabindex="-1" role="dialog" aria-labelledby="profileModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="profileModalLabel"><i class="fa fa-info-circle"></i> Profile FAQ</h4>
            </div>
            <div class="modal-body">
            	<div id="profile_tuts_video"></div>
				<script type="text/javascript">
                jwplayer("profile_tuts_video").setup({
                    file: "https://www.youtube.com/watch?v=1BMPoYosybY",
                    width: "100%",
                    height: 350,
                });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- Account Help Video Modal -->
<div class="modal fade" id="account_help_video" tabindex="-1" role="dialog" aria-labelledby="accountModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="accountModalLabel"><i class="fa fa-info-circle"></i> Account FAQ</h4>
            </div>
            <div class="modal-body">
            	<div id="account_tuts_video"></div>
				<script type="text/javascript">
                jwplayer("account_tuts_video").setup({
                    file: "https://www.youtube.com/watch?v=1BMPoYosybY",
                    width: "100%",
                    height: 350,
                });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- Offer Help Video Modal -->
<div class="modal fade" id="offer_help_video" tabindex="-1" role="dialog" aria-labelledby="offerModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="offerModalLabel"><i class="fa fa-info-circle"></i> Max Offer FAQ</h4>
            </div>
            <div class="modal-body">
            	<div id="offer_tuts_video"></div>
				<script type="text/javascript">
                jwplayer("offer_tuts_video").setup({
                    file: "https://www.youtube.com/watch?v=1BMPoYosybY",
                    width: "100%",
                    height: 350,
                });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- Portfolio Help Video Modal -->
<div class="modal fade" id="portfolio_help_video" tabindex="-1" role="dialog" aria-labelledby="portfolioModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="portfolioModalLabel"><i class="fa fa-info-circle"></i> Portfolio FAQ</h4>
            </div>
            <div class="modal-body">
            	<div id="portfolio_tuts_video"></div>
				<script type="text/javascript">
                jwplayer("portfolio_tuts_video").setup({
                    file: "https://www.youtube.com/watch?v=1BMPoYosybY",
                    width: "100%",
                    height: 350,
                });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- Proposal Help Video Modal -->
<div class="modal fade" id="proposal_help_video" tabindex="-1" role="dialog" aria-labelledby="proposalModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="proposalModalLabel"><i class="fa fa-info-circle"></i> Proposal FAQ</h4>
            </div>
            <div class="modal-body">
            	<div id="proposal_tuts_video"></div>
				<script type="text/javascript">
                jwplayer("proposal_tuts_video").setup({
                    file: "https://www.youtube.com/watch?v=1BMPoYosybY",
                    width: "100%",
                    height: 350,
                });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- Rental Help Video Modal -->
<div class="modal fade" id="rental_help_video" tabindex="-1" role="dialog" aria-labelledby="rentalModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="rentalModalLabel"><i class="fa fa-info-circle"></i> Rental FAQ</h4>
            </div>
            <div class="modal-body">
            	<div id="rental_tuts_video"></div>
				<script type="text/javascript">
                jwplayer("rental_tuts_video").setup({
                    file: "https://www.youtube.com/watch?v=1BMPoYosybY",
                    width: "100%",
                    height: 350,
                });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- Comparison Help Video Modal -->
<div class="modal fade" id="comparison_help_video" tabindex="-1" role="dialog" aria-labelledby="comparisonModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="comparisonModalLabel"><i class="fa fa-info-circle"></i> Analyser FAQ</h4>
            </div>
            <div class="modal-body">
            	<div id="comparison_tuts_video"></div>
				<script type="text/javascript">
                jwplayer("comparison_tuts_video").setup({
                    file: "https://www.youtube.com/watch?v=1BMPoYosybY",
                    width: "100%",
                    height: 350,
                });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- Payment Help Video Modal -->
<div class="modal fade" id="payment_help_video" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="paymentModalLabel"><i class="fa fa-info-circle"></i> Payment FAQ</h4>
            </div>
            <div class="modal-body">
            	<div id="payment_tuts_video"></div>
				<script type="text/javascript">
                jwplayer("payment_tuts_video").setup({
                    file: "https://www.youtube.com/watch?v=1BMPoYosybY",
                    width: "100%",
                    height: 350,
                });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- Payment Help Video Modal -->
<div class="modal fade" id="modal_help_video" tabindex="-1" role="dialog" aria-labelledby="modalModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalModalLabel"><i class="fa fa-info-circle"></i> Modal FAQ</h4>
            </div>
            <div class="modal-body">
            	<div id="modal_tuts_video"></div>
				<script type="text/javascript">
                jwplayer("modal_tuts_video").setup({
                    file: "https://www.youtube.com/watch?v=1BMPoYosybY",
                    width: "100%",
                    height: 350,
                });
                </script>
            </div>
        </div>
    </div>
</div>

<!-- Payment Help Video Modal -->
<div class="modal fade" id="popup_blocker_video" tabindex="-1" role="dialog" aria-labelledby="popupLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="popupLabel"><i class="fa fa-info-circle"></i> Popup Blocker FAQ</h4>
            </div>
            <div class="modal-body">
            	<div id="popup_tuts_video"></div>
				<script type="text/javascript">
                jwplayer("popup_tuts_video").setup({
                    file: "https://www.youtube.com/watch?v=1BMPoYosybY",
                    width: "100%",
                    height: 350,
                });
                </script>
            </div>
        </div>
    </div>
</div>
<!-- Help Information Modal -->
<div class="modal fade" id="info_help" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="infoModalLabel"><i class="fa fa-info-circle"></i> Profile Upgradation</h4>
            </div>
            <div class="modal-body">
            	<p>You can't access this link before updating your account, please follow the step by step information below.</p>
                <ol>
                	<li><p>Update your profile by clicking the '<strong>Profile</strong>' link from the left menu panel.</p></li>
                    <li><p>Update your account information by clicking your rounded profile image above the left menu panel.</p></li>
                    <li><p>Post at least one portfolio after updating your profile and account information.</p></li>
                    <li><p>You are done, now you can access all the links of your cblist account.</p></li>
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning" onclick="goto('<?=base_url('pages/accounts')?>/')">Complete Account</button>
                <?php if ( $profile == FALSE ) { ?>
	                <button type="button" class="btn btn-primary" onclick="goto('<?=base_url('pages/profile')?>/')">Update Profile</button>
				<?php } if ( $portfolio == FALSE ) { ?>
                	<button type="button" class="btn btn-primary" onclick="goto('<?=base_url('pages/new-portfolio')?>/')">Create Portfolio</button>
                <?php } if ( $portfolio == FALSE ) { ?>
                	<button type="button" class="btn btn-primary" onclick="goto('<?=base_url('pages/profile')?>/')">Create Proposal</button>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<!-- Help Upgrade Modal -->
<div class="modal fade" id="upgrade_modal" tabindex="-1" role="dialog" aria-labelledby="upgradeModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="upgradeModalLabel"><i class="fa fa-info-circle"></i> Upgrade Your Account</h4>
            </div>
            <div class="modal-body">
				<p>You have to paid for printing the reports. Please click on the update button below and choose your plan.</p>
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary" onclick="goto('<?=base_url('pages/payment')?>/')">Choose Plan</button>
            </div>
        </div>
    </div>
</div>

<!-- Proposal Save Confirm Modal -->
<div class="modal fade" id="confirm_modal" tabindex="-1" role="dialog" aria-labelledby="confirmModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="confirmModalLabel"><i class="fa fa-info-circle"></i> Confirm Exit</h4>
            </div>
            <div class="modal-body">
				<p>You are not saved the proposal yet. Do you want to save it?</p>
            </div>
            <div class="modal-footer">
            	<img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide draft-loading" />
	            <button type="button" class="btn btn-primary" id="SaveDraft">Save Draft</button>
            	<button type="button" class="btn btn-default" id="ExitProposal" data-rel="">Exit</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="LTVModal" tabindex="-1" role="dialog" aria-labelledby="LTVModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-info-circle"></i> ERROR: Check LTV</h4>
            </div>
            <div class="modal-body">
				<p>ERROR: Down Payment % and the sum of LTV % must total 100%. Please adjust your inputs.</p>
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="MonthsModal" tabindex="-1" role="dialog" aria-labelledby="MonthsModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-info-circle"></i> ERROR: Check Months</h4>
            </div>
            <div class="modal-body">
				<p>ATTENTION: The total time period between rehabbing the property and selling it must be no more than 24 months. Please change your inputs to make sure that the total period assumed to rehab and sell the property is 24months or less!</p>
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="MarketValueModal" tabindex="-1" role="dialog" aria-labelledby="MarketValueModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-info-circle"></i> Initial Market Value</h4>
            </div>
            <div class="modal-body">
				<p>If you are under contract for below market value, the 1) Great Job ! and 2) click yes and enter the market value to the right. This option is designed to account for below market purchases and then be able to use the market value as a base to project future value. For conservative projections use the contract price.</p>
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ImageErrorModal" tabindex="-1" role="dialog" aria-labelledby="ImageErrorModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><i class="fa fa-info-circle"></i> ERROR: Invalid Photo</h4>
            </div>
            <div class="modal-body">
				<p>Photo only allows file types of GIF, PNG, JPG, JPEG and BMP.</p>
            </div>
            <div class="modal-footer">
            	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>