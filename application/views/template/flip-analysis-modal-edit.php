<!-- Rehub Budget Flip Analysis Modal -->
<div class="modal fade" id="FlipRehubModal" tabindex="-1" role="dialog" aria-labelledby="FlipRehubModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header text-center">
                <h4 class="modal-title" id="FlipRehubModalLabel">
                	<span id="FlipTitle"><?=$refi[0]->PropertyStreetAddress?></span> Scope of Work and Budget
                    <a class="modal-help" data-toggle="tooltip" data-placement="top" title="Need help">
                        <i class="fa fa-video-camera" data-target="#modal_help_video" data-toggle="modal"></i>
                    </a>
                </h4>
            </div>
            <div class="modal-body">
            	<?php $attributes = array('class' => 'form-horizontal', 'name' => 'FlipBudgetForm'); ?>
				<?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                    
                    <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                    <div class="row">
                        <div class="col-lg-12 assump-box">
                            <div class="row">
                                <div class="col-lg-8">
                                    <div class="row">
                                        <div class="panel-field" id="FlipRehabBudgetMethodRow">
                                            <label class="col-lg-8">1. How do you want to enter your Holding Costs?</label>
                                            <div class="col-lg-4">
                                                <?php 
                                                $options = array(
                                                    'Detailed Input'  => 'Detailed Input',
                                                    'Quick Lump Sum'  => 'Quick Lump Sum',
                                                );
                                                $attrb = 'id="FlipRehabBudgetMethod" class="form-control"'; ?>
                                                <?=form_dropdown('FlipRehabBudgetMethod', $options, $flip[0]->FlipRehabBudgetMethod, $attrb)?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="row">
                                        <div class="panel-field" id="FlipRehabDrawSelectionRow">
                                            <label class="col-lg-5">2. Overide:</label>
                                            <div class="col-lg-7">
                                                <?php 
                                                $options = array(
                                                    'Fund Rehab in Draws'  => 'Fund Rehab in Draws',
                                                    'Fund Rehab at Closing' => 'Fund Rehab at Closing',
                                                );
                                                $attrb = 'id="FlipRehabDrawSelection" class="form-control"'; ?>
                                                <?=form_dropdown('FlipRehabDrawSelection', $options, $flip[0]->FlipRehabDrawSelection, $attrb)?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <hr />
                                    <div class="row">
                                        <div class="panel-field" id="LumpSumFlipBudgetRow" style="display:none;">
                                            <label class="col-lg-6">Enter Lump Sum Budget Below:</label>
                                            <div class="col-lg-6">
                                                <?php 
                                                $field = array(
                                                    'name'  => "LumpSumFlipBudget",
                                                    'id'    => "LumpSumFlipBudget",
                                                    'value' => $flip[0]->LumpSumFlipBudget,
                                                    'class'	=> 'form-control NumberOnly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">    
                        <div class="col-lg-12">
                            <div class="panel-field" id="DetailedInputFlipBudgetRow">
                                <table width="100%" class="table table-left" id="FlipTable">
                                    <thead>
                                        <tr>
                                            <th width="80">General Conditions:</th>
                                            <th>Details/Notes</th>
                                            <th>Sq Ft</th>
                                            <th>Quantity</th>
                                            <th>Rate</th>
                                            <th>Bid 1</th>
                                            <th>Bid 2</th>
                                            <th>Bid 3</th>
                                            <th width="100" bgcolor="#007C30" style="color:#fff;"><strong>BUDGET</strong></th>
                                            <th>Month to be Paid</th>
                                            <th width="80">&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php 
										$PreliWork = array('Demo', 'Architectural', 'MasterBuildingPermit', 'Plumbing', 'Electrical', 'HVAC', 'Dumpster', 'Other', 'WallFraming', 'FloorFraming', 'CeilingFraming', 'InteriorElectrical', 'InteriorPlumbing', 'InteriorHVAC', 'Flooring', 'Sheetrock', 'Windows', 'InteriorDoors', 'Trim', 'BathroomVanities', 'BathroomFixtures', 'KitchenCabinets', 'LaborInstallKitchen', 'FloorCoverings1', 'FloorCoverings2', 'FloorCoverings3', 'InteriorPainting', 'LightFixtures', 'OtherFixtures', 'Appliances', 'OtherInterior', 'ExteriorTrim', 'ExteriorDoors', 'Porches', 'Siding', 'ExteriorPainting', 'Roof', 'GuttersDownspouts', 'Fencing', 'Landscaping', 'DrivewayConcrete', 'FoudationWork', 'BrickPointingReplacement', 'OtherExterior', 'Contingency', 'GCFee', 'Cleanup', 'OtherOther'); ?>
                                    	<?php $flip_data = array_slice($flip, 0, 3); ?>
                                        <?php $work_data = array_slice($PreliWork, 0, 3); ?>
                                    	<?php foreach ( $flip_data as $key => $val ) { ?>
                                        	<tr>
                                                <td><?=$val->PreliminaryWork?></td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "DetailsNotes".$work_data[$key],
                                                        'id'    => "DetailsNotes".$work_data[$key],
                                                        'value' => $val->DetailsNotes,
                                                        'class'	=> 'form-control',
                                                    ); ?>
                                                    <?=form_input($field)?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "SqFt".$work_data[$key],
                                                        'id'    => "SqFt".$work_data[$key],
                                                        'value' => $val->SqFt,
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Quantity".$work_data[$key],
                                                        'id'    => "Quantity".$work_data[$key],
                                                        'value' => $val->Quantity,
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                    ); ?>
                                                    <?=form_input($field)?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Rate".$work_data[$key],
                                                        'id'    => "Rate".$work_data[$key],
                                                        'value' => number_format($val->Rate, 2, '.', ''),
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid1".$work_data[$key],
                                                        'id'    => "Bid1".$work_data[$key],
                                                        'value' => number_format($val->Bid1, 2, '.', ''),
                                                        'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid2".$work_data[$key],
                                                        'id'    => "Bid2".$work_data[$key],
                                                        'value' =>  number_format($val->Bid2, 2, '.', ''),
                                                        'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid3".$work_data[$key],
                                                        'id'    => "Bid3".$work_data[$key],
                                                        'value' => number_format($val->Bid3, 2, '.', ''),
                                                        'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Budget".$work_data[$key],
                                                        'id'    => "Budget".$work_data[$key],
                                                        'value' => number_format($val->Budget, 2, '.', ''),
                                                        'class'	=> 'form-control BudgetInputBox FlipBudgetColumn NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                    $attrb = 'id="MonthPaid'.$work_data[$key].'" class="form-control FlipMonthPaidColumn" onChange="CheckFlipMonthRehab(this, \'MonthsFlipRehab\');"'; ?>
                                                    <?=form_dropdown('MonthPaid'.$work_data[$key], $options, $val->MonthPaid, $attrb)?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                        
                                        <tr>
                                            <td colspan="11">&nbsp;Trade Permits:</td>
                                        </tr>
                                        <?php $flip_data = array_slice($flip, 3, 5); ?>
                                        <?php $work_data = array_slice($PreliWork, 3, 5); ?>
                                    	<?php foreach ( $flip_data as $key => $val ) { ?>
                                            <tr>
                                                <td>&nbsp;&nbsp;<?=$val->PreliminaryWork?></td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "DetailsNotes".$work_data[$key],
                                                        'id'    => "DetailsNotes".$work_data[$key],
                                                        'value' => $val->DetailsNotes,
                                                        'class'	=> 'form-control',
                                                    ); ?>
                                                    <?=form_input($field)?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "SqFt".$work_data[$key],
                                                        'id'    => "SqFt".$work_data[$key],
                                                        'value' => $val->SqFt,
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Quantity".$work_data[$key],
                                                        'id'    => "Quantity".$work_data[$key],
                                                        'value' => $val->Quantity,
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                    ); ?>
                                                    <?=form_input($field)?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Rate".$work_data[$key],
                                                        'id'    => "Rate".$work_data[$key],
                                                        'value' => number_format($val->Rate, 2, '.', ''),
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid1".$work_data[$key],
                                                        'id'    => "Bid1".$work_data[$key],
                                                        'value' => number_format($val->Bid1, 2, '.', ''),
                                                        'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid2".$work_data[$key],
                                                        'id'    => "Bid2".$work_data[$key],
                                                        'value' => number_format($val->Bid2, 2, '.', ''),
                                                        'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid3".$work_data[$key],
                                                        'id'    => "Bid3".$work_data[$key],
                                                        'value' => number_format($val->Bid3, 2, '.', ''),
                                                        'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Budget".$work_data[$key],
                                                        'id'    => "Budget".$work_data[$key],
                                                        'value' => number_format($val->Budget, 2, '.', ''),
                                                        'class'	=> 'form-control BudgetInputBox FlipBudgetColumn NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                    $attrb = 'id="MonthPaid'.$work_data[$key].'" class="form-control FlipMonthPaidColumn" onChange="CheckFlipMonthRehab(this, \'MonthsFlipRehab\');"'; ?>
                                                    <?=form_dropdown('MonthPaid'.$work_data[$key], $options, $val->MonthPaid, $attrb)?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                        
                                        <tr>
                                            <td colspan="11">&nbsp;<strong><u>Interior Work:</u></strong></td>
                                        </tr>
                                        <?php $flip_data = array_slice($flip, 8, 23); ?>
                                        <?php $work_data = array_slice($PreliWork, 8, 23); ?>
                                    	<?php foreach ( $flip_data as $key => $val ) { ?>
                                        	<tr>
                                                <td><?=$val->PreliminaryWork?></td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "DetailsNotes".$work_data[$key],
                                                        'id'    => "DetailsNotes".$work_data[$key],
                                                        'value' => $val->DetailsNotes,
                                                        'class'	=> 'form-control',
                                                    ); ?>
                                                    <?=form_input($field)?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "SqFt".$work_data[$key],
                                                        'id'    => "SqFt".$work_data[$key],
                                                        'value' => $val->SqFt,
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Quantity".$work_data[$key],
                                                        'id'    => "Quantity".$work_data[$key],
                                                        'value' => $val->Quantity,
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                    ); ?>
                                                    <?=form_input($field)?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Rate".$work_data[$key],
                                                        'id'    => "Rate".$work_data[$key],
                                                        'value' => number_format($val->Rate, 2, '.', ''),
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid1".$work_data[$key],
                                                        'id'    => "Bid1".$work_data[$key],
                                                        'value' => number_format($val->Bid1, 2, '.', ''),
                                                        'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid2".$work_data[$key],
                                                        'id'    => "Bid2".$work_data[$key],
                                                        'value' => number_format($val->Bid2, 2, '.', ''),
                                                        'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid3".$work_data[$key],
                                                        'id'    => "Bid3".$work_data[$key],
                                                        'value' => number_format($val->Bid3, 2, '.', ''),
                                                        'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Budget".$work_data[$key],
                                                        'id'    => "Budget".$work_data[$key],
                                                        'value' => number_format($val->Budget, 2, '.', ''),
                                                        'class'	=> 'form-control BudgetInputBox FlipBudgetColumn NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                    $attrb = 'id="MonthPaid'.$work_data[$key].'" class="form-control FlipMonthPaidColumn" onChange="CheckFlipMonthRehab(this, \'MonthsFlipRehab\');"'; ?>
                                                    <?=form_dropdown('MonthPaid'.$work_data[$key], $options, $val->MonthPaid, $attrb)?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                        
                                        <tr>
                                            <td colspan="11">&nbsp;<strong><u>Exterior Work:</u></strong></td>
                                        </tr>
                                        <?php $flip_data = array_slice($flip, 31, 13); ?>
                                        <?php $work_data = array_slice($PreliWork, 31, 13); ?>
                                    	<?php foreach ( $flip_data as $key => $val ) { ?>
                                        	<tr>
                                                <td><?=$val->PreliminaryWork?></td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "DetailsNotes".$work_data[$key],
                                                        'id'    => "DetailsNotes".$work_data[$key],
                                                        'value' => $val->DetailsNotes,
                                                        'class'	=> 'form-control',
                                                    ); ?>
                                                    <?=form_input($field)?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "SqFt".$work_data[$key],
                                                        'id'    => "SqFt".$work_data[$key],
                                                        'value' => $val->SqFt,
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Quantity".$work_data[$key],
                                                        'id'    => "Quantity".$work_data[$key],
                                                        'value' => $val->Quantity,
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                    ); ?>
                                                    <?=form_input($field)?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Rate".$work_data[$key],
                                                        'id'    => "Rate".$work_data[$key],
                                                        'value' => number_format($val->Rate, 2, '.', ''),
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid1".$work_data[$key],
                                                        'id'    => "Bid1".$work_data[$key],
                                                        'value' => number_format($val->Bid1, 2, '.', ''),
                                                        'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid2".$work_data[$key],
                                                        'id'    => "Bid2".$work_data[$key],
                                                        'value' => number_format($val->Bid2, 2, '.', ''),
                                                        'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid3".$work_data[$key],
                                                        'id'    => "Bid3".$work_data[$key],
                                                        'value' => number_format($val->Bid3, 2, '.', ''),
                                                        'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Budget".$work_data[$key],
                                                        'id'    => "Budget".$work_data[$key],
                                                        'value' => number_format($val->Budget, 2, '.', ''),
                                                        'class'	=> 'form-control BudgetInputBox FlipBudgetColumn NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                    $attrb = 'id="MonthPaid'.$work_data[$key].'" class="form-control FlipMonthPaidColumn" onChange="CheckFlipMonthRehab(this, \'MonthsFlipRehab\');"'; ?>
                                                    <?=form_dropdown('MonthPaid'.$work_data[$key], $options, $val->MonthPaid, $attrb)?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                        
                                        <tr>
                                            <td colspan="11">&nbsp;<strong><u>Other:</u></strong></td>
                                        </tr>
                                        <?php $flip_data = array_slice($flip, 44, 4); ?>
                                        <?php $work_data = array_slice($PreliWork, 44, 4); ?>
                                    	<?php foreach ( $flip_data as $key => $val ) { ?>
                                        	<tr>
                                                <td><?=$val->PreliminaryWork?></td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "DetailsNotes".$work_data[$key],
                                                        'id'    => "DetailsNotes".$work_data[$key],
                                                        'value' => $val->DetailsNotes,
                                                        'class'	=> 'form-control',
                                                    ); ?>
                                                    <?=form_input($field)?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "SqFt".$work_data[$key],
                                                        'id'    => "SqFt".$work_data[$key],
                                                        'value' => $val->SqFt,
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Quantity".$work_data[$key],
                                                        'id'    => "Quantity".$work_data[$key],
                                                        'value' => $val->Quantity,
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWithoutDecimal',
                                                    ); ?>
                                                    <?=form_input($field)?>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Rate".$work_data[$key],
                                                        'id'    => "Rate".$work_data[$key],
                                                        'value' => number_format($val->Rate, 2, '.', ''),
                                                        'class'	=> 'form-control NumberOnly NoPreZeroWith2Decimal',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid1".$work_data[$key],
                                                        'id'    => "Bid1".$work_data[$key],
                                                        'value' => number_format($val->Bid1, 2, '.', ''),
                                                        'class'	=> 'form-control Bid1InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid2".$work_data[$key],
                                                        'id'    => "Bid2".$work_data[$key],
                                                        'value' => number_format($val->Bid2, 2, '.', ''),
                                                        'class'	=> 'form-control Bid2InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Bid3".$work_data[$key],
                                                        'id'    => "Bid3".$work_data[$key],
                                                        'value' => number_format($val->Bid3, 2, '.', ''),
                                                        'class'	=> 'form-control Bid3InputBox NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $field = array(
                                                        'name'  => "Budget".$work_data[$key],
                                                        'id'    => "Budget".$work_data[$key],
                                                        'value' => number_format($val->Budget, 2, '.', ''),
                                                        'class'	=> 'form-control BudgetInputBox FlipBudgetColumn NumberOnly',
                                                    ); ?>
                                                    <div class="input-group">
                                                        <div class="input-group-addon">$</div>
                                                        <?=form_input($field)?>
                                                    </div>
                                                </td>
                                                <td>
                                                    <?php 
                                                    $options = array(0 => 0, 1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                    $attrb = 'id="MonthPaid'.$work_data[$key].'" class="form-control FlipMonthPaidColumn" onChange="CheckFlipMonthRehab(this, \'MonthsFlipRehab\');"'; ?>
                                                    <?=form_dropdown('MonthPaid'.$work_data[$key], $options, $val->MonthPaid, $attrb)?>
                                                </td>
                                                <td></td>
                                            </tr>
                                        <?php } ?>
                                        
                                    </tbody>
                                    <tfoot>
                                        <tr>
                                            <td colspan="5"><strong>Total</strong></td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "TotalBid1Total",
                                                    'id'    => "TotalBid1Total",
                                                    'value' => number_format($flip[0]->TotalBid1Total, 2, '.', ''),
                                                    'class'	=> 'form-control Bid1InputBox',
                                                    'readonly' => 'readonly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "TotalBid2Total",
                                                    'id'    => "TotalBid2Total",
                                                    'value' => number_format($flip[0]->TotalBid2Total, 2, '.', ''),
                                                    'class'	=> 'form-control Bid2InputBox',
                                                    'readonly' => 'readonly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "TotalBid3Total",
                                                    'id'    => "TotalBid3Total",
                                                    'value' => number_format($flip[0]->TotalBid3Total, 2, '.', ''),
                                                    'class'	=> 'form-control Bid3InputBox',
                                                    'readonly' => 'readonly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <?=form_input($field)?>
                                                </div>
                                            </td>
                                            <td>
                                                <?php 
                                                $field = array(
                                                    'name'  => "DetailedFlipBudget",
                                                    'id'    => "DetailedFlipBudget",
                                                    'value' => number_format($flip[0]->DetailedFlipBudget, 2, '.', ''),
                                                    'class'	=> 'form-control BudgetInputBox',
                                                    'readonly' => 'readonly',
                                                ); ?>
                                                <div class="input-group">
                                                    <div class="input-group-addon">$</div>
                                                    <strong><?=form_input($field)?></strong>
                                                </div>
                                            </td>
                                            <td colspan="2">&nbsp;</td>
                                        </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                
                <?=form_close()?>
                
            </div>
            <div class="modal-footer">
            	<button type="button" id="FlipRehubReturn" class="btn btn-primary">RETURN TO ANALYSIS</button>
            </div>
        </div>
    </div>
</div>
<!-- Rehub Budget Flip Analysis Modal -->