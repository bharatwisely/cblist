<style type="text/css">
h3, h4 {
	margin-top:0;
}
.table {
	font-size:12px;
}
.table.table-bg {
	background:#F7F4F4;
}
.table.table-bordered td, .table.table-bordered th {
	border: 1px solid #ddd !important;
} 
.table-responsive {
	overflow:auto;
}	
</style>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <h2>Proposal</h2>
                    </div>
                </div>
                
                <div class="page-content">
                	<div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
	                                <?php 
                                    if ( isset($print_data['SingleProposal']['CoverPage']) ) {
                                        $CP = $print_data['SingleProposal']['CoverPage']; ?>
                                        
                                        <h3 class="box-title"><?=$CP->PropertyName?></h3>
                                        <h3 class="box-title pull-right"><i class="fa fa-map-marker"></i> <?=$CP->PropertyStreetAddress?>, <?=$CP->PropertyCityTown?>, <?=$CP->PropertyStateProvince?>, <?=$CP->PropertyZipCode?>, <?=$CP->PropertyCountry?></h3>
                                        
                                    <?php } ?>
                                </div>
                                <div class="box-body">
									<?php 
									if ( isset($print_data['SingleProposal']['CoverPage']) ) {
										$CP = $print_data['SingleProposal']['CoverPage']; ?>
										
										<div class="CoverPage">
											<div class="row">
												<div class="col-lg-8">
													<?php if ( $CP->PropertyDescription ) { ?>
														<p><strong>Property Description:</strong></p>
														<p><?=stripslashes($CP->PropertyDescription)?></p>
														<p><hr /></p>
													<?php } ?>
													<?php if ( $CP->PropertyFeatures ) { ?>
														<p><strong>Property Features:</strong></p>
														<p><?=stripslashes($CP->PropertyFeatures)?></p>
														<p><hr /></p>
													<?php } ?>
													<?php if ( $CP->WorkNeeded ) { ?>
														<p><strong>Property Work Needed:</strong></p>
														<p><?=stripslashes($CP->WorkNeeded)?></p>
														<p><hr /></p>
													<?php } ?>
												</div>
												<div class="col-lg-4">
													<p><strong><u>Exclusively Presented By:</u></strong></p>
													<p><i class="fa fa-user"></i> <?=((strlen($CP->YourName)>0)?$CP->YourName:$CP->fname)?></p>
													<?php if ( $CP->CompanyName ) { ?>
														<p><i class="fa fa-building"></i> <?=$CP->CompanyName?></p>
													<?php } ?>
													<?php if ( $CP->CompanyStreet && $CP->CompanyCity ) { ?>
														<p><i class="fa fa-location-arrow"></i> <?=$CP->CompanyStreet?>, <?=$CP->CompanyCity?></p>
													<?php } ?>
													<?php if ( $CP->PhoneNumber ) { ?>
														<p><i class="fa fa-mobile"></i> <?=$CP->PhoneNumber?></p>
													<?php } ?>
													<p><i class="fa fa-envelope"></i> <?=((strlen($CP->Email)>0)?$CP->Email:$CP->email)?></p>
													<?php if ( $CP->Website ) { ?>
														<p><i class="fa fa-link"></i> <a href="<?=prep_url(strtolower($CP->Website))?>"><?=$CP->Website?></a></p>
													<?php } ?>
													<p><hr /></p>
												</div>
											</div>
										</div>
										
									<?php } ?>
                                

									<?php if ( isset($print_data['SingleProposal']['ProjectSummary']) ) {
                                        $PS = $print_data['SingleProposal']['ProjectSummary']; ?>
    
                                        <div class="ProjectSummary"><br />
                                            <?php $Property = $PS->Property; ?>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <?php 
                                                    $FlipBudget = $PS->FlipBudget; 
					
													$MonthsFlipRehab = $Property->MonthsFlipRehab ? $Property->MonthsFlipRehab : 0;
													$MonthsToSell = $Property->MonthsToSell ? $Property->MonthsToSell : 0;
													$RehabDiscountPoints = $Property->RehabDiscountPoints ? ($Property->RehabDiscountPoints/100) : 0;
													$ActualFinancedFlip = $Property->ActualFinancedFlip ? $Property->ActualFinancedFlip : 0;
													$OtherPoints = $Property->OtherPoints ? ($Property->OtherPoints/100) : 0;
													$FlipHoldingCosts = $Property->FlipHoldingCosts ? $Property->FlipHoldingCosts : 0;
													$RehabInterestRate = $Property->RehabInterestRate ? ($Property->RehabInterestRate/100) : 0;
													$LumpSumFlipBudget = $Property->LumpSumFlipBudget ? $Property->LumpSumFlipBudget : 0;
													$DetailedFlipBudget = $Property->DetailedFlipBudget ? $Property->DetailedFlipBudget : 0;
													$LenderSplit = $Property->LenderSplit ? ($Property->LenderSplit/100) : 0;
													
													$Row16Total = 0; $Row24Total = 0; $IRRReslt = 1;
													$MonthTotal = array();
													$MonthMatchArray = array();
													$FlipBudgetColumn = array(); $MonthPaidColumn = array();
													foreach ( $FlipBudget as $key => $flip ) {
														$FlipBudgetColumn[$key] = $flip->Budget;
														$MonthPaidColumn[$key] = $flip->MonthPaid;
													}
													
													$B2 = ($Property->FinancingUsed=="Financing" ? "Yes" : "No");
													$B3 = $Property->CapSelection;
													$B4 = $Property->FinancingCap ? ($Property->FinancingCap/100) : 0;
													
													$F2 = $Property->ARVFlip ? $Property->ARVFlip : 0;
													$F3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualFlipBudget ? $Property->ActualFlipBudget : 0);
													$F4 = ($Property->CapSelection=="ARV" ? $B4 * $F2 : $B4 * $F3);
													
													// FLIP
													for ( $i = 0; $i <= 24; $i++ ) {
														if ( $i == 0 ) {
															$B7[$i] = 1; // B7
															$B8[$i] = ($i > ($MonthsFlipRehab + $MonthsToSell) ? 0 : 1); // B8
															$B9[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B9
														} else {
															$B7[$i] = ( $i > $MonthsFlipRehab ? 0 : 1); // B7
															$B8[$i] = ($i > ($MonthsFlipRehab + $MonthsToSell) ? 0 : 1); // B8
															$B9[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B9
														}
													}
													
													$B10 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
													$B11 = $PurchaseFinanced = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B10 : min($B10, $F4)) : 0);
													$B12 = $B10-$B11;
													$B13 = $Property->ClosingCosts ? $Property->ClosingCosts : 0;
													
													for ( $i = 0; $i <= 24; $i++ ) {
														if ( $i == 0 ) {
															$B14[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab + $MonthsToSell) ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0)); // B14
															$B15[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab+$MonthsToSell) ? ($OtherPoints)*$Property->ActualFinancedFlip : 0)); // B15
															$B16[$i] = 0; // B16
															$Row16Total = $Row16Total + $B16[$i];
															$B17[$i] = $PurchaseFinanced + ($B13 + $B16[$i]) * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0); // B17
															$B18[$i] = 0; // B18
															$B19[$i] = 0; // B19
															$B20[$i] = 0; // B20
															$B21[$i] = 0; // B21
															$B22[$i] = 0; // B22
															$MonthTotal[$i] = ($Property->FlipRehabDrawSelection=="Fund Rehab at Closing" ? ($Property->FlipRehabBudgetMethod=="Quick Lump Sum" ? $LumpSumFlipBudget : $DetailedFlipBudget) : 0);
															$B23[$i] = $MonthTotal[$i]; // B23
															$B24[$i] = ($B3=="Cost" ? $MonthTotal[$i] * $B4 : max(0, min($F4, $MonthTotal[$i]))) * $B9[$i]; // B24
															$Row24Total = $Row24Total + $B24[$i];
															$B25[$i] = ($B23[$i] - $B24[$i]); // B25
															$B26[$i] = ($B23[$i]==0 ? 0 : $B23[$i]) * $B8[$i]; // B26
															$B27[$i] = $B24[$i]; // B27
															$B28[$i] = 0; // B28
															$B29[$i] = 0; // B29
															$B30[$i] = 0; // B30
															$B31[$i] = 0; // B31
															$B32[$i] = 0; // B32
															$B33[$i] = $B10+$B13+$B14[$i]+$B15[$i]+$B23[$i]; // B33
															$B34[$i] = ($PurchaseFinanced + $B27[$i]) * $B8[$i]; // B34
															$B35[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : $PurchaseFinanced+$Property->ClosingCosts*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+($Row24Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B14[$i]+$B15[$i]))); // B35
															$B36[$i] = $B12+$Property->ClosingCosts*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B16[$i]+$B18[$i]+$B25[$i]+$B29[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B14[$i]+$B15[$i]) : 0); // B36
															$B37[$i] = $B33[$i]-($B35[$i]+$B36[$i]); // B37
															$B38[$i] = $B12+$B13*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B14[$i]+$B15[$i]+$B16[$i]+$B19[$i]+$B25[$i]+$B29[$i]; // B38
														} else {
															$B14[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab + $MonthsToSell) ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0)); // B14
															$B15[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab+$MonthsToSell) ? ($OtherPoints)*$ActualFinancedFlip : 0)); // B15
															$B16[$i] = $FlipHoldingCosts/($MonthsFlipRehab+$MonthsToSell)*$B8[$i]; // B16
															$Row16Total = $Row16Total + $B16[$i];
															$B17[$i] = ($B17[$i-1] + $B16[$i] * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))*$B8[$i]; // B17
															$B18[$i] = $RehabInterestRate * $B17[$i-1] / 12 * ($i>($MonthsFlipRehab+$MonthsToSell) ? 0 : 1); // B18
															$B19[$i] = $Property->InterestPaymentsMade=="Yes" ? $B18[$i] : 0; // B19
															$B20[$i] = $B18[$i] - $B19[$i]; // B20
															if ( $i == 1 ) {
																$B21[$i] = $B20[$i] + $B21[$i-1]; // B21
															} else {
																$B21[$i] = ($B20[$i]+$B21[$i-1]+$B21[$i-1]*($RehabInterestRate/12))*$B8[$i]; // B21
															}
															$B22[$i] = $B21[$i] * ($i == ($MonthsFlipRehab + $MonthsToSell) ? 1 : 0); // B22
															$ColU = ($i<=$MonthsFlipRehab ? 1 : 0);
															$MonthMatch = 0;
															for ( $j = 0; $j < count($MonthPaidColumn); $j++ ) {
																if ( $MonthPaidColumn[$j] == $i ) {
																	$MonthMatch += $FlipBudgetColumn[$j];
																}
															}
															$MonthMatchArray[$i] = $MonthMatch;
															$MonthTotal[$i] = $Property->FlipRehabDrawSelection=="Fund Rehab at Closing" ? 0 : ($Property->FlipRehabBudgetMethod=="Detailed Input" ? $MonthMatchArray[$i] * $ColU : $LumpSumFlipBudget / $MonthsFlipRehab * $ColU);
															$B23[$i] = $MonthTotal[$i]; // B23
															$B24[$i] = ($B3=="Cost" ? $MonthTotal[$i] * $B4 : max(0, min($F4-($B34[$i-1]), $B23[$i]))) * $B9[$i]; // B24
															$Row24Total = $Row24Total + $B24[$i];
															$B25[$i] = $B23[$i] - $B24[$i]; // B25
															$B26[$i] = ($B23[$i]=="" ? $B26[$i-1] : $B23[$i] + $B26[$i-1]) * $B8[$i]; // B26
															$B27[$i] = ($B24[$i]==0 ? $B27[$i-1] : $B24[$i] + $B27[$i-1]) * $B8[$i]; // B27
															$B28[$i] = $B27[$i-1] * $RehabInterestRate/12 * $B8[$i] * $B9[$i]; // B28
															$B29[$i] = $Property->InterestPaymentsMade=="Yes" ? $B28[$i] : 0; // B29
															$B30[$i] = $B28[$i] - $B29[$i]; // B30
															if ( $i == 1 ) {
																$B31[$i] = $B30[$i] + $B31[$i-1]; // B31
															} else {
																$B31[$i] = ($B30[$i] + $B31[$i-1] + $B31[$i-1] * ($RehabInterestRate/12)) * $B8[$i]; // B31
															}
															$B32[$i] = $B31[$i] * ($i==($MonthsFlipRehab+$MonthsToSell) ? 1 : 0); // B32
															$B33[$i] = ($B33[$i-1]+($B14[$i]+$B15[$i]+$B16[$i])+$B19[$i]+$B20[$i]+$B21[$i-1]*$RehabInterestRate/12+$B23[$i]+$B29[$i]+$B30[$i]+$B31[$i-1]*$RehabInterestRate/12)*$B8[$i]; // B33
															$B34[$i] = ($PurchaseFinanced + $B27[$i]) * $B8[$i]; // B34
															$B35[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($PurchaseFinanced+$Property->ClosingCosts*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+($Row16Total)*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+$B27[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B14[$i]+$B15[$i]))+$B21[$i]+$B31[$i])*$B8[$i]); // B35
															$B36[$i] = ($B12+$B16[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B19[$i]+$B25[$i]+$B29[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B14[$i]+$B15[$i]) : 0)+$B36[$i-1])*$B8[$i]; // B36
															$B37[$i] = $B33[$i]-($B35[$i]+$B36[$i]); // B37
															$B38[$i] = $B16[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B19[$i]+$B25[$i]+$B29[$i]; // B38
														}
													}
													
													$B39 = array_sum($B38)-max($B36);
													$B40 = max($B33);
													
													for ( $i = 0; $i <= 24; $i++ ) {
														if ( $i == 0 ) {
															$B43[$i] = 0; // B43
															$B44[$i] = 0; // B44
															$B45[$i] = 0; // B45
															$B46[$i] = 0; // B46
															$B47[$i] = 0; // B47
															$B48[$i] = 0; // B48
															$B49[$i] = 0; // B49
														} else {
															$B43[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $Property->ResalePrice : 0); // B43
															$B44[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $Property->ResalePrice*($Property->CostOfSale/100) : 0); // B44
															$B45[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? min($B35[$i], $B43[$i]-$B44[$i]) : 0); // B45
															$B46[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? min($B36[$i], $B43[$i]-$B44[$i]-$B45[$i]) : 0); // B46
															$B47[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $B43[$i]-$B44[$i]-$B45[$i]-$B46[$i] : 0); // B47
															$B48[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $LenderSplit*($Property->FinancingUsed=="Financing" ? 1 : 0)*$B47[$i]*($Property->SplitWithLender=="Yes" ? 1 : 0) : 0); // B48
															$B49[$i] = $B47[$i]-$B48[$i]; // B49
														}
													}
													
													$B52 = $Property->PurchasePrice;
													$B53 = $B13;
													$B54 = array_sum($B16);
													$B55 = array_sum($B14);
													$B56 = array_sum($B15);
													$B57 = array_sum($B23);
													$B58 = array_sum($B19)+array_sum($B22);
													$B59 = array_sum($B29)+array_sum($B32);
													$B61 = ($B52+$B53+$B54+$B55+$B56+$B57+$B58+$B59);
													
													$B63 = $PurchaseClosingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0);
													$B64 = $HoldingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? array_sum($B16) : 0);
													$B65 = $FlipPoints = array_sum($B14);
													$B66 = $FlipOtherFinancingCosts = array_sum($B15);
													$B67 = $FlipLoanInterest = array_sum($B19)+array_sum($B22);
													$B68 = $FlipRehabInterest = array_sum($B29)+array_sum($B32);
													$B69 = ($B63+$B64+$B65+$B66+$B67+$B68);
													
													$G64 = $PurchaseFinanced;
													$G65 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0));
													$G66 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? array_sum($B16) : 0));
													$G67 = ($Property->PointsClosingUpfront=="Paid Backend" ? array_sum($B14)+array_sum($B15) : 0)+($Property->InterestPaymentsMade=="No" ? (array_sum($B22)+array_sum($B32)) : 0);
													$G68 = array_sum($B24);
													$G69 = ($G64+$G65+$G66+$G67+$G68);
													$G71 = $FundedByLenderFlip = $G64+$G65+$G66+$G68;
													
													$B71 = $Property->TotalLoanFlip;
													$B72 = $Property->CashRequiredFlip;
													$B73 = ($B71+$B72);
													
													$D73 = $B40-$B73;
													
													for ( $i = 0; $i <= 24; $i++ ) {
														if ( $i == 0 ) {
															$B75[$i] = 0; // B75
															$B76[$i] = 0; // B76
															$B78[$i] = 0; // B78
															$B79[$i] = 0; // B79
														} else {
															$B75[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($Property->CashRequiredFlip==0 ? "Infinite" : $B47[$i]/$Property->CashRequiredFlip) : 0); // B75
															$B76[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($B75[$i]=="Infinite" ? "Infinite" : $B75[$i]/($MonthsFlipRehab+$MonthsToSell)*12) : 0); // B76
															$B78[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($Property->CashRequiredFlip==0 ? "Infinite" : $B49[$i]/$Property->CashRequiredFlip) : 0); // B78
															$B79[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($B78[$i]=="Infinite" ? "Inifinite" : $B78[$i]/($MonthsFlipRehab+$MonthsToSell)*12) : 0); // B79
														}
													}
													
													// Cashflows to Lender
													$B84 = -$PurchaseFinanced;
													$B85 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$Property->ClosingCosts : 0));
													for ( $i = 0; $i <= 24; $i++ ) {
														if ( $i == 0 ) {
															$B86[$i] = 0;
															$B87[$i] = $B14[$i];
															$B88[$i] = $B15[$i];
															$B89[$i] = -$B24[$i];
															$B90[$i] = 0;
															$B91[$i] = 0;
															$B92[$i] = 0;
															$B93[$i] = 0;
															$B94[$i] = $B35[$i];
															$B95[$i] = 0;
															$B96[$i] = 0;
															
															$B98[$i] = $B84+$B85+$B86[$i]+$B89[$i];
															$B99[$i] = $B90[$i]+$B92[$i]+$B95[$i]+$B96[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B87[$i]+$B88[$i] : 0);
															$B100[$i] = ($B98[$i]+$B99[$i]);
														} else {
															$B86[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$B16[$i] : 0));
															$B87[$i] = $B14[$i];
															$B88[$i] = $B15[$i];
															$B89[$i] = -$B24[$i];
															$B90[$i] = $B19[$i];
															$B91[$i] = $B20[$i];
															$B92[$i] = $B29[$i];
															$B93[$i] = $B30[$i];
															$B94[$i] = $B35[$i];
															$B95[$i] = $B45[$i];
															$B96[$i] = $B48[$i];
															
															$B98[$i] = $B86[$i]+$B89[$i];
															$B99[$i] = $B90[$i]+$B92[$i]+$B95[$i]+$B96[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B87[$i]+$B88[$i] : 0);
															$B100[$i] = ($B98[$i]+$B99[$i]);
														}
													}
													$B101 = IRR($B100, 0.01);
													for ( $i=0; $i<12; $i++ ) {
														$IRRReslt *= ($B101+1);
													}
													$B102 = ($IRRReslt-1)*100;
													$C101 = $B101*12;
													$B103 = $TotalLenderInterestIncomeFlip = ($Property->InterestPaymentsMade=="No" ? array_sum($B22)+array_sum($B32) : array_sum($B19)+array_sum($B29));
													$B104 = $FeesToLenderFlip = array_sum($B14)+array_sum($B15);
													$B105 = array_sum($B48);
													$B106 = ($B103+$B104+$B105);
													
													$E104 = $B106/$G71;
													$E105 = $E104/($MonthsFlipRehab+$MonthsToSell)*12;
													
													$C106 = array_sum($B100)-$B106;
													
													$FinancingCostFlip = ($FlipPoints+$FlipOtherFinancingCosts+$FlipLoanInterest+$FlipRehabInterest);
                                                    ?>
                                                    <p class="text-center"><strong>PROJECT SUMMARY - FLIP</strong></p>
                                                    <table class="table table-bordered table-bg">
                                                        <tr>
                                                            <th colspan="2">PURCHASE/REHAB ASSUMPTIONS</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Purchase Price</td>
                                                            <td class="text-right"><?=number_format($PS->PurchasePrice, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Rehab Cost</td>
                                                            <td class="text-right"><?=number_format($PS->ActualFlipBudget, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total Holding and Closing Costs(not inc. sale)</td>
                                                            <td class="text-right"><?=number_format(($PS->ClosingCosts+$PS->FlipHoldingCosts), 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total Financial Costs(Not inc. Lender Split)</td>
                                                            <td class="text-right"><?=number_format($FinancingCostFlip, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total Project Cost Basis</td>
                                                            <td class="text-right"><?=number_format($PS->TotalCostBasisFlip, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total Amount Financed </td>
                                                            <td class="text-right"><?=number_format($PS->TotalLoanFlip, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total Cash Commited</td>
                                                            <td class="text-right"><?=number_format($PS->CashRequiredFlip, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2">RESULTS</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Projected Resale Prices</td>
                                                            <td class="text-right"><?=number_format($PS->ResalePrice, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Projected Cost of Sale</td>
                                                            <td class="text-right"><?=number_format((($PS->CostOfSale/100)*$PS->ResalePrice), 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Lender Split of Profits</td>
                                                            <td class="text-right">
                                                                <?=number_format(($PS->ResalePrice-$PS->TotalCostBasisFlip-($PS->CostOfSale/100)*$PS->ResalePrice)*(($PS->LenderSplit/100)*($PS->FinancingUsed=='Financing'?1:0)*($PS->SplitWithLender=='Yes'?1:0)), 2)?>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Flip Profit</td>
                                                            <td class="text-right"><?=number_format($PS->FlipProfit, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">ROI</td>
                                                            <td class="text-right"><?=($PS->FlipROI=='Infinite' ? 'Infinite' : round($PS->FlipROI).'%')?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Annualized ROI</td>
                                                            <td class="text-right"><?=($PS->FlipROIAnnualized=='Infinite' ? 'Infinite' : round($PS->FlipROIAnnualized).'%')?></td>
                                                        </tr>
                                                    </table>
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th colspan="2">Breakdown of Financial Costs: </th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Origination/Discount Points</td>
                                                            <td class="text-right"><?=number_format(($PS->RehabDiscountPoints/100)*$PS->ActualFinancedFlip)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Other closing Costs of loan</td>
                                                            <td class="text-right"><?=number_format(($PS->OtherPoints/100)*$PS->ActualFinancedFlip)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Intrest on Original Loan</td>
                                                            <td class="text-right"><?=number_format($FlipLoanInterest)?></td>
                                                        </tr>                        
                                                        <tr>
                                                            <td class="text-left">Intrest on Rehab Money</td>
                                                            <td class="text-right"><?=number_format($FlipRehabInterest)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total(Before Lender Split)</td>
                                                            <td class="text-right"><?=number_format($FinancingCostFlip)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Split of Profits to Lender</td>
                                                            <td class="text-right"><?=number_format(($PS->ResalePrice-$PS->TotalCostBasisFlip-($PS->CostOfSale/100)*$PS->ResalePrice)*(($PS->LenderSplit/100)*($PS->FinancingUsed=='Financing'?1:0)*($PS->SplitWithLender=='Yes'?1:0)))?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Cost of Financing</td>
                                                            <td class="text-right"><?=number_format(($PS->ResalePrice-$PS->TotalCostBasisFlip-($PS->CostOfSale/100)*$PS->ResalePrice)*(($PS->LenderSplit/100)*($PS->FinancingUsed=='Financing'?1:0)*($PS->SplitWithLender=='Yes'?1:0))+$FinancingCostFlip)?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                
                                                <div class="col-lg-6">
                                                    <?php 
                                                    $RefiBudget = $PS->RefiBudget;
					
													$MonthsRefiRehab = $Property->MonthsRefiRehab ? $Property->MonthsRefiRehab : 0;
													$MonthsToRefi = $Property->MonthsToRefi ? $Property->MonthsToRefi : 0;
													$RehabDiscountPoints = $Property->RehabDiscountPoints ? ($Property->RehabDiscountPoints/100) : 0;
													$ActualFinancedRent = $Property->ActualFinancedRent ? $Property->ActualFinancedRent : 0;
													$OtherPoints = $Property->OtherPoints ? ($Property->OtherPoints/100) : 0;
													$RefiHoldingCosts = $Property->RefiHoldingCosts ? $Property->RefiHoldingCosts : 0;
													$RehabInterestRate = $Property->RehabInterestRate ? ($Property->RehabInterestRate/100) : 0;
													$LumpSumRefiBudget = $Property->LumpSumRefiBudget ? $Property->LumpSumRefiBudget : 0;
													$DetailedRefiBudget = $Property->DetailedRefiBudget ? $Property->DetailedRefiBudget : 0; 
													$LenderSplit = $Property->LenderSplit ? ($Property->LenderSplit/100) : 0;
													
													$Row118Total = 0; $Row126Total = 0; $IRRReslt = 1;
													$MonthTotal = array();
													$MonthMatchArray = array();
													$RefiBudgetColumn = array(); $MonthPaidColumn = array();
													foreach ( $RefiBudget as $key => $refi ) {
														$RefiBudgetColumn[$key] = $refi->Budget;
														$MonthPaidColumn[$key] = $refi->MonthPaid;
													}
													
													$B2 = ($Property->FinancingUsed=="Financing" ? "Yes" : "No");
													$B3 = $Property->CapSelection;
													$B4 = $Property->FinancingCap ? ($Property->FinancingCap/100) : 0;
													
													$I2 = $Property->ARVRent ? $Property->ARVRent : 0;
													$I3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualRefiBudget ? $Property->ActualRefiBudget : 0);
													$I4 = ($Property->CapSelection=="ARV" ? $B4 * $I2 : $B4 * $I3);
													
													// REFI
													for ( $i = 0; $i <= 24; $i++ ) {
														if ( $i == 0 ) {
															$B109[$i] = 1; // B109
															$B110[$i] = ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B110
															$B111[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B111
														} else {
															$B109[$i] = ($i>$MonthsRefiRehab ? 0 : 1); // B109
															$B110[$i] = ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B110
															$B111[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B111
														}
													}
													
													$B112 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
													$B113 = $PurchaseFinancedRefi = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B112 : min($B112, $I4)) : 0);
													$B114 = $B112-$B113;
													$B115 = $Property->ClosingCosts ? $Property->ClosingCosts : 0;
													for ( $i = 0; $i <= 24; $i++ ) {
														if ( $i == 0 ) {
															$B116[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($RehabDiscountPoints)*$ActualFinancedRent : 0); // B116
															$B117[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($OtherPoints)*$ActualFinancedRent : 0); // B117
															$B118[$i] = 0; // B118
															$Row118Total = $Row118Total + $B118[$i];
															$B119[$i] = $PurchaseFinanced + ($B115 + $B118[$i]) * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0); // B119
															$B120[$i] = 0; // B120
															$B121[$i] = 0; // B121
															$B122[$i] = 0; // B122
															$B123[$i] = 0; // B123
															$B124[$i] = 0; // B124
															$B125[$i] = ($Property->RefiRehabDrawSelection=="Fund Rehab at Closing" ? ($Property->RefiRehabBudgetMethod=="Quick Lump Sum" ? $LumpSumRefiBudget : $DetailedRefiBudget) : 0); // B125
															$B126[$i] = ($B3=="Cost" ? $B125[$i] * $B4 : max(0, min($I4, $B125[$i]))) * $B111[$i]; // B126
															$Row126Total = $Row126Total + $B126[$i];
															$B127[$i] = $B125[$i] - $B126[$i]; // B127
															$B128[$i] = ($B125[$i]==0 ? 0 : $B125[$i]) * $B110[$i]; // B128
															$B129[$i] = $B126[$i]; // B129
															$B130[$i] = 0; // B130
															$B131[$i] = 0; // B131
															$B132[$i] = 0; // B132
															$B133[$i] = 0; // B133
															$B134[$i] = 0; // B134
															$B135[$i] = $B112+$B115+$B116[$i]+$B117[$i]+$B125[$i]; // B135
															$B136[$i] = $PurchaseFinancedRefi+($Row126Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])); // B136
															$B137[$i] = $PurchaseFinancedRefi+$Property->ClosingCosts*($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))+($Row126Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])); // B137
															$B138[$i] = $B114+($B115+$B118[$i])*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B120[$i]+$B127[$i]+$B131[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B116[$i]+$B117[$i]) : 0); // B138
															$B139[$i] = $B135[$i]-($B137[$i]+$B138[$i]); // B139
															$B140[$i] = $B114+$B115*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 :1))+$B116[$i]+$B117[$i]+$B118[$i]*($Property->FinanceClosingHolding=="Yes" ? 0 : 1)+$B121[$i]+$B127[$i]+$B131[$i]; // B140
														} else {
															$B116[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($RehabDiscountPoints)*$ActualFinancedRent : 0); // B116
															$B117[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($OtherPoints)*$ActualFinancedRent : 0); // B117				
															$B118[$i] = $RefiHoldingCosts/($MonthsRefiRehab+$MonthsToRefi)*$B110[$i]; // B118
															$Row118Total = $Row118Total + $B118[$i];
															$B119[$i] = ($B119[$i-1] + $B118[$i] * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))*$B110[$i]; // B119
															$B120[$i] = $RehabInterestRate*$B119[$i-1] / 12 * ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B120
															$B121[$i] = $Property->InterestPaymentsMade=="Yes" ? $B120[$i] : 0; // B121
															$B122[$i] = $B120[$i] - $B121[$i]; // B122
															if ( $i == 1 ) {
																$B123[$i] = $B122[$i] + $B123[$i-1]; // B123
															} else {
																$B123[$i] = ($B122[$i]+$B123[$i-1]+$B123[$i-1]*($RehabInterestRate/12))*$B110[$i]; // B123
															}
															$B124[$i] = $B123[$i] * ($i == ($MonthsRefiRehab + $MonthsToRefi) ? 1 : 0); // B124
															
															$ColU = ($i<=$MonthsRefiRehab ? 1 : 0);
															$MonthMatch = 0;
															for ( $j = 0; $j < count($MonthPaidColumn); $j++ ) {
																if ( $MonthPaidColumn[$j] == $i ) {
																	$MonthMatch += $RefiBudgetColumn[$j];
																}
															}
															$MonthMatchArray[$i] = $MonthMatch;
															$B125[$i] = $Property->RefiRehabDrawSelection=="Fund Rehab at Closing" ? 0 : ($Property->RefiRehabBudgetMethod=="Detailed Input" ? $MonthMatchArray[$i] * $ColU : $LumpSumFlipBudget / $MonthsRefiRehab * $ColU); // B125
															$B126[$i] = ($B3=="Cost" ? $B125[$i] * $B4 : max(0, min($I4-$B137[$i-1], $B125[$i]))) * $B111[$i]; // B126
															$B127[$i] = $B125[$i] - $B126[$i]; // B127
															$B128[$i] = ($B125[$i]==0 ? $B128[$i-1] : $B125[$i] + $B128[$i-1]) * $B110[$i]; // B128
															$B129[$i] = ($B126[$i]==0 ? $B129[$i-1] : $B126[$i] + $B129[$i-1]) * $B110[$i]; // B129
															$B130[$i] = $B129[$i-1] * $RehabInterestRate/12 * $B110[$i] * $B111[$i]; // B130
															$B131[$i] = $Property->InterestPaymentsMade=="Yes" ? $B130[$i] : 0; // B131
															$B132[$i] = $B130[$i] - $B131[$i]; // B132
															if ( $i == 1 ) {
																$B133[$i] = $B132[$i] + $B133[$i-1]; // B133
															} else {
																$B133[$i] = ($B132[$i] + $B133[$i-1] + $B133[$i-1] * ($RehabInterestRate/12)) * $B110[$i]; // B133
															}
															$B134[$i] = $B133[$i] * ($i==($MonthsRefiRehab+$MonthsToRefi) ? 1 : 0); // B134
															$B135[$i] = ($B135[$i-1]+($B116[$i]+$B117[$i]+$B118[$i])+$B121[$i]+$B122[$i]+$B123[$i-1]*$RehabInterestRate/12+$B125[$i]+$B131[$i]+$B132[$i]+$B133[$i-1]*$RehabInterestRate/12)*$B110[$i]; // B135
															$B136[$i] = ($PurchaseFinancedRefi+$B129[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])))*$B110[$i]; // B136
															$B137[$i] = ($PurchaseFinancedRefi+($Property->ClosingCosts+($Row118Total))*($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))+$B129[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i]))+$B123[$i]+$B133[$i])*$B110[$i]; // B137
															$B138[$i] = ($B118[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B121[$i]+$B127[$i]+$B131[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B116[$i]+$B117[$i]) : 0)+$B138[$i-1])*$B110[$i]; // B138
															$B139[$i] = $B135[$i]-($B137[$i]+$B138[$i]); // B139
															$B140[$i] = $B118[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B121[$i]+$B127[$i]+$B131[$i]; // B140
														}
													}
													
													$B141 = array_sum($B140)-max($B138);
													$B142 = max($B135);
													
													for ( $i = 0; $i <= 24; $i++ ) {
														if ( $i == 0 ) {
															$B144[$i] = 0; // B144
															$B145[$i] = 0; // B145
															$B145[$i] = 0; // B146
															$B147[$i] = 0; // B147
															$B149[$i] = 0; // B149
															$B150[$i] = 0; // B150
															$B151[$i] = 0; // B151
															$B152[$i] = 0; // B152
															$B153[$i] = 0; // B153
															$B154[$i] = 0; // B154
														} else {
															$B144[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->RefiAmount : 0); // B144
															$B145[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? -($Property->RefiDiscountPoints/100)*$Property->RefiAmount : 0); // B145
															$B146[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? -$B137[$i] : 0); // B146
															$B147[$i] = ($B144[$i]+$B145[$i]+$B146[$i]); // B147
															$B149[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? max(0, $B147[$i]-$Property->CashRequiredRent) : 0); // B149
															$B150[$i] = 	($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->LenderSplit*($Property->FinancingUsed=="Financing" ? 1 : 0)*$B149[$i]*($Property->SplitWithLender=="Yes" ? 1 : 0) : 0); // B150
															$B151[$i] = $B149[$i]-$B150[$i]; // B151
															$B152[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "infinite" : $B149[$i]/$Property->CashRequiredRent/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B152
															$B153[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? max(0, $B138[$i]-$B147[$i]) : 0); // B153
															$B154[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->ARVRent-$Property->RefiAmount : 0); // B154
														}
													}
													
													$B158 = $Property->PurchasePrice;
													$B159 = $B115;
													$B160 = array_sum($B118);
													$B161 = array_sum($B116);
													$B162 = array_sum($B117);
													$B163 = array_sum($B125);
													$B164 = array_sum($B121)+array_sum($B124);
													$B165 = array_sum($B131)+array_sum($B134);
													$B167 = ($B158+$B159+$B160+$B161+$B162+$B163+$B164+$B165);
													
													$B169 = $RefiClosingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? $B115 : 0);
													$B170 = $RefiHoldingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? array_sum($B118) : 0);
													$B171 = $RentPoints = array_sum($B116);
													$B172 = $RentOtherFinancingCosts = array_sum($B117);
													$B173 = $RentLoanInterest = array_sum($B121)+array_sum($B124);
													$B174 = $RentRehabInterest = array_sum($B131)+array_sum($B134);
													$B175 = ($B171+$B172+$B173+$B174);
													
													$G170 = $PurchaseFinanced;
													$G171 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0));
													$G172 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? array_sum($B118) : 0));
													$G173 = ($Property->PointsClosingUpfront=="Paid Backend" ? array_sum($B116)+array_sum($B117) : 0)+($Property->InterestPaymentsMade=="No" ? (array_sum($B124)+array_sum($B134)) : 0);
													$G174 = array_sum($B126);
													$G175 = ($G170+$G171+$G172+$G173+$G174);
													$G177 = $FundedByLenderREfi = $G170+$G171+$G172+$G174;
													
													$B177 = $Property->TotalLoanRent;
													$B178 = $Property->CashRequiredRent;
													$B179 = ($B177+$B178);
													
													$D179 = $B142-$B179;
													
													for ( $i = 0; $i <= 24; $i++ ) {
														if ( $i == 0 ) {
															$B181[$i] = 0; // B181
															$B182[$i] = 0; // B182
															$B184[$i] = 0; // B184
															$B185[$i] = 0; // B185
														} else {
															$B181[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "Infinite" : $B149[$i]/$Property->CashRequiredRent) : 0); // B181
															$B182[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($B181[$i]=="Infinite" ? "Infinite" : $B181[$i]/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B182
															$B184[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "Infinite" : $B151[$i]/$Property->CashRequiredRent) : 0); // B184
															$B185[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($B184[$i]=="Infinite" ? "Infinite" : $B184[$i]/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B185
														}
													}
													
													// Cashflows to Lender
													$B190 = -$PurchaseFinancedRefi;
													$B191 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$Property->ClosingCosts : 0));
													for ( $i = 0; $i <= 24; $i++ ) {
														if ( $i == 0 ) {
															$B192[$i] = 0;
															$B193[$i] = $B116[$i];
															$B194[$i] = $B117[$i];
															$B195[$i] = -$B126[$i];
															$B196[$i] = 0;
															$B197[$i] = 0;
															$B198[$i] = 0;
															$B199[$i] = 0;
															$B200[$i] = $B137[$i];
															$B201[$i] = 0;
															$B202[$i] = 0;
															
															$B204[$i] = $B190+$B191+$B192[$i]+$B195[$i];
															$B205[$i] = $B196[$i]+$B198[$i]+$B201[$i]+$B202[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B193[$i]+$B194[$i] : 0);
															$B206[$i] = ($B204[$i]+$B205[$i]);
														} else {
															$B192[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$B118[$i] : 0));
															$B193[$i] = $B116[$i];
															$B194[$i] = $B117[$i];
															$B195[$i] = -$B126[$i];
															$B196[$i] = $B121[$i];
															$B197[$i] = $B122[$i];
															$B198[$i] = $B131[$i];
															$B199[$i] = $B132[$i];
															$B200[$i] = $B137[$i];
															$B201[$i] = -$B146[$i];
															$B202[$i] = $B150[$i];
															
															$B204[$i] = $B192[$i]+$B195[$i];
															$B205[$i] = $B196[$i]+$B198[$i]+$B201[$i]+$B202[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B193[$i]+$B194[$i] : 0);
															$B206[$i] = ($B204[$i]+$B205[$i]);
														}
													}
													$B207 = IRR($B206, 0.01);
													for ( $i=0; $i<12; $i++ ) {
														$IRRReslt *= ($B207+1);
													}
													$B208 = ($IRRReslt-1)*100;
													$C207 = $B207*12;
													$B209 = $TotalLenderInterestIncomeRefi = ($Property->InterestPaymentsMade=="No" ? array_sum($B124)+array_sum($B134) : array_sum($B121)+array_sum($B131));
													$B210 = $FeesToLenderRefi = array_sum($B116)+array_sum($B117);
													$B211 = array_sum($B150);
													$B212 = ($B209+$B210+$B211);
													
													$E210 = $B212/$G177;
													$E211 = $E210/($MonthsRefiRehab+$MonthsToRefi)*12;
													
													$C212 = array_sum($B206)-$B212;
													
													$FinancingCostRent = $RentPoints+$RentOtherFinancingCosts+$RentLoanInterest+$RentRehabInterest;
                                                    ?>
                                                    <p class="text-center"><strong>PROJECT SUMMARY - REFI/RENT</strong></p>
                                                    <table class="table table-bordered table-bg">
                                                        <tr>
                                                            <th colspan="2">PURCHASE/REHAB ASSUMPTIONS</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Purchase Price</td>
                                                            <td class="text-right"><?=number_format($PS->PurchasePrice, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Rehab Cost</td>
                                                            <td class="text-right"><?=number_format($PS->ActualRefiBudget, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total Holding and Closing Costs(not inc. refi)</td>
                                                            <td class="text-right"><?=number_format(($PS->ClosingCosts+$PS->RefiHoldingCosts), 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total Financing Costs</td>
                                                            <td class="text-right"><?=number_format($FinancingCostRent, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total Project Cost Basis</td>
                                                            <td class="text-right"><?=number_format($PS->TotalCostBasisRent, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total Amount Financed</td>
                                                            <td class="text-right"><?=number_format($PS->TotalLoanRent, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total Cash Commited</td>
                                                            <td class="text-right"><?=number_format($PS->CashRequiredRent, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2">NEW LOAN ASSUMPTIONS</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Projected After-Repair Appraisal</td>
                                                            <td class="text-right"><?=number_format($PS->ARVRent, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">New Loan Amount</td>
                                                            <td class="text-right"><?=number_format($PS->RefiAmount, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Closing Costs on New Loan</td>
                                                            <td class="text-right"><?=number_format((($PS->RefiDiscountPoints/100)*$PS->RefiAmount), 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2">&nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <th colspan="2">RESULTS</th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Cash-Out at Refi</td>
                                                            <td class="text-right"><?=number_format($PS->CashOutRefi, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Lender Split of Cash-Out Profit</td>
                                                            <td class="text-right"><?=number_format(max(0, ($PS->CashOutRefi-$PS->CashRequiredRent)*$PS->LenderSplit*($PS->FinancingUsed=='Financing'?1:0)*($PS->SplitWithLender=='Yes'?1:0)), 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Profit at Refi</td>
                                                            <td class="text-right"><?=number_format($PS->RefiProfit, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">ROI on Downpayment/Cash investment (Annualized)</td>
                                                            <td class="text-right"><?=($PS->RefiROI=='N/A' ? 'N/A' : round($PS->RefiROI).'%')?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Original Investment Tied up in Deal after Refi</td>
                                                            <td class="text-right"><?=number_format($PS->CashTiedUP, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Equity Left in Property</td>
                                                            <td class="text-right"><?=number_format($PS->EquityLeft, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Monthly  Cashflow (Pretax)</td>
                                                            <td class="text-right"><?=number_format($PS->CashflowMonthly, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Cash-on-Cash Return(Annual)</td>
                                                            <td class="text-right"><?=($PS->RefiROIAnnual=='Infinite' ? 'Infinite' : round($PS->RefiROIAnnual).'%')?></td>
                                                        </tr>  
                                                    </table>
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th colspan="2">Breakdown of Financial Costs: </th>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Original/Discount Points</td>
                                                            <td class="text-right"><?=number_format(($PS->RehabDiscountPoints/100)*$PS->ActualFinancedRent)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Other Closing Costs for Loan</td>
                                                            <td class="text-right"><?=number_format(($PS->OtherPoints/100)*$PS->ActualFinancedRent)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Intrest on Original Loan</td>
                                                            <td class="text-right"><?=number_format($RentLoanInterest)?></td>
                                                        </tr>                        
                                                        <tr>
                                                            <td class="text-left">Intrest on Rehab Money</td>
                                                            <td class="text-right"><?=number_format($RentRehabInterest)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total (before Lender Split)</td>
                                                            <td class="text-right"><?=number_format($FinancingCostRent)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Split of Refi Profits to Lender</td>
                                                            <td class="text-right"><?=number_format(max(0, ($PS->CashOutRefi-$PS->CashRequiredRent)*$PS->LenderSplit*($PS->FinancingUsed=='Financing'?1:0)*($PS->SplitWithLender=='Yes'?1:0)))?></td>
                                                        </tr>
                                                        <tr>
                                                            <td class="text-left">Total</td>
                                                            <td class="text-right"><?=number_format($FinancingCostRent+(max(0, ($PS->CashOutRefi-$PS->CashRequiredRent)*$PS->LenderSplit*($PS->FinancingUsed=='Financing'?1:0)*($PS->SplitWithLender=='Yes'?1:0))))?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['FlipRehabBudget']) ) {
                                        $FB = $print_data['SingleProposal']['FlipRehabBudget']; ?>
                                        
                                        <div class="FlipRehabBudget"><br />
                                            <p class="text-center"><strong><?=$FB[0]->PropertyStreetAddress?> Scope of Work and Budget</strong></p>
                                            <?php if ( $FB[0]->FlipRehabBudgetMethod == 'Detailed Input' ) { ?>
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Preliminary Work</th>
                                                            <th class="text-center">Details/Notes</th>
                                                            <th class="text-center">Sq Ft</th>
                                                            <th class="text-center">Quantity</th>
                                                            <th class="text-center">Rate</th>
                                                            <th class="text-center">Bid 1</th>
                                                            <th class="text-center">Bid 2</th>
                                                            <th class="text-center">Bid 3</th>
                                                            <th class="text-center">Budget</th>
                                                            <th class="text-center">Month to be Paid</th>
                                                        </tr>
                                                        <tbody>
                                                            <?php for ( $i = 0; $i < count($FB); $i++ ) { ?>
                                                                
                                                                <tr>
                                                                    <td><?=$FB[$i]->PreliminaryWork?></td>
                                                                    <td class="text-center"><?=$FB[$i]->DetailsNotes?></td>
                                                                    <td class="text-center"><?=$FB[$i]->SqFt?></td>
                                                                    <td class="text-center"><?=$FB[$i]->Quantity?></td>
                                                                    <td class="text-center"><?=number_format($FB[$i]->Rate, 2)?></td>
                                                                    <td class="text-center"><?=number_format($FB[$i]->Bid1, 2)?></td>
                                                                    <td class="text-center"><?=number_format($FB[$i]->Bid2, 2)?></td>
                                                                    <td class="text-center"><?=number_format($FB[$i]->Bid3, 2)?></td>
                                                                    <td class="text-center"><?=number_format($FB[$i]->Budget, 2)?></td>
                                                                    <td class="text-center"><?=$FB[$i]->MonthPaid?></td>
                                                                </tr>
                                                                
                                                            <?php } ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="5"><strong>Total</strong></td>
                                                                <td class="text-center"><strong><?=number_format($FB[0]->TotalBid1Total, 2)?></strong></td>
                                                                <td class="text-center"><strong><?=number_format($FB[0]->TotalBid2Total, 2)?></strong></td>
                                                                <td class="text-center"><strong><?=number_format($FB[0]->TotalBid3Total, 2)?></strong></td>
                                                                <td class="text-center"><strong><?=number_format($FB[0]->DetailedFlipBudget, 2)?></strong></td>
                                                                <td></td>
                                                            </tr>
                                                        </tfoot>
                                                    </thead>
                                                </table>
                                            <?php } else { ?>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td><strong>Lump Sum Flip Budget: </strong></td>
                                                        <td><strong><?=number_format($FB[0]->LumpSumFlipBudget, 2)?></strong></td>
                                                    </tr>
                                                </table>
                                            <?php } ?>
                                        </div>
                                        
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['RefiRehabBudget']) ) {
                                        $RB = $print_data['SingleProposal']['RefiRehabBudget']; ?>
                                        
                                        <div class="RefiRehabBudget"><br />
                                            <p class="text-center"><strong><?=$RB[0]->PropertyStreetAddress?> Scope of Work and Budget</strong></p>
                                            <?php if ( $RB[0]->RefiRehabBudgetMethod == 'Detailed Input' ) { ?>
                                                <table class="table table-bordered">
                                                    <thead>
                                                        <tr>
                                                            <th>Preliminary Work</th>
                                                            <th class="text-center">Details/Notes</th>
                                                            <th class="text-center">Sq Ft</th>
                                                            <th class="text-center">Quantity</th>
                                                            <th class="text-center">Rate</th>
                                                            <th class="text-center">Bid 1</th>
                                                            <th class="text-center">Bid 2</th>
                                                            <th class="text-center">Bid 3</th>
                                                            <th class="text-center">Budget</th>
                                                            <th class="text-center">Month to be Paid</th>
                                                        </tr>
                                                        <tbody>
                                                            <?php for ( $i = 0; $i < count($RB); $i++ ) { ?>
                                                                
                                                                <tr>
                                                                    <td><?=$RB[$i]->PreliminaryWork?></td>
                                                                    <td class="text-center"><?=$RB[$i]->DetailsNotes?></td>
                                                                    <td class="text-center"><?=$RB[$i]->SqFt?></td>
                                                                    <td class="text-center"><?=$RB[$i]->Quantity?></td>
                                                                    <td class="text-center"><?=number_format($RB[$i]->Rate, 2)?></td>
                                                                    <td class="text-center"><?=number_format($RB[$i]->Bid1, 2)?></td>
                                                                    <td class="text-center"><?=number_format($RB[$i]->Bid2, 2)?></td>
                                                                    <td class="text-center"><?=number_format($RB[$i]->Bid3, 2)?></td>
                                                                    <td class="text-center"><?=number_format($RB[$i]->Budget, 2)?></td>
                                                                    <td class="text-center"><?=$RB[$i]->MonthPaid?></td>
                                                                </tr>
                                                                
                                                            <?php } ?>
                                                        </tbody>
                                                        <tfoot>
                                                            <tr>
                                                                <td colspan="5"><strong>Total</strong></td>
                                                                <td class="text-center"><strong><?=number_format($RB[0]->TotalBid1Total, 2)?></strong></td>
                                                                <td class="text-center"><strong><?=number_format($RB[0]->TotalBid2Total, 2)?></strong></td>
                                                                <td class="text-center"><strong><?=number_format($RB[0]->TotalBid3Total, 2)?></strong></td>
                                                                <td class="text-center"><strong><?=number_format($RB[0]->DetailedRefiBudget, 2)?></strong></td>
                                                                <td></td>
                                                            </tr>
                                                        </tfoot>
                                                    </thead>
                                                </table>
                                            <?php } else { ?>
                                                <table class="table table-bordered">
                                                    <tr>
                                                        <td><strong>Lump Sum Refi Budget: </strong></td>
                                                        <td><strong><?=number_format($RB[0]->LumpSumRefiBudget, 2)?></strong></td>
                                                    </tr>
                                                </table>
                                            <?php } ?>            
                                        </div>
                                        
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['FlipMarketingSheet']) ) {
                                        $FMS = $print_data['SingleProposal']['FlipMarketingSheet']; ?>
    
                                        <div class="FlipMarketingSheet"><br />
                                            <?php 
                                            $Property = $FMS->Property;
                                            $FlipBudget = $FMS->FlipBudget;
                                    
                                            $MonthsFlipRehab = $Property->MonthsFlipRehab ? $Property->MonthsFlipRehab : 0;
                                            $MonthsToSell = $Property->MonthsToSell ? $Property->MonthsToSell : 0;
                                            $RehabDiscountPoints = $Property->RehabDiscountPoints ? ($Property->RehabDiscountPoints/100) : 0;
                                            $ActualFinancedFlip = $Property->ActualFinancedFlip ? $Property->ActualFinancedFlip : 0;
                                            $OtherPoints = $Property->OtherPoints ? ($Property->OtherPoints/100) : 0;
                                            $FlipHoldingCosts = $Property->FlipHoldingCosts ? $Property->FlipHoldingCosts : 0;
                                            $RehabInterestRate = $Property->RehabInterestRate ? ($Property->RehabInterestRate/100) : 0;
                                            $LumpSumFlipBudget = $Property->LumpSumFlipBudget ? $Property->LumpSumFlipBudget : 0;
                                            $DetailedFlipBudget = $Property->DetailedFlipBudget ? $Property->DetailedFlipBudget : 0;
                                            $LenderSplit = $Property->LenderSplit ? ($Property->LenderSplit/100) : 0;
                                            
                                            $Row16Total = 0; $Row24Total = 0; $IRRReslt = 1;
                                            $MonthTotal = array();
                                            $MonthMatchArray = array();
                                            $FlipBudgetColumn = array(); $MonthPaidColumn = array();
                                            foreach ( $FlipBudget as $key => $flip ) {
                                                $FlipBudgetColumn[$key] = $flip->Budget;
                                                $MonthPaidColumn[$key] = $flip->MonthPaid;
                                            }
                                            
                                            $B2 = ($Property->FinancingUsed=="Financing" ? "Yes" : "No");
                                            $B3 = $Property->CapSelection;
                                            $B4 = $Property->FinancingCap ? ($Property->FinancingCap/100) : 0;
                                            
                                            $F2 = $Property->ARVFlip ? $Property->ARVFlip : 0;
                                            $F3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualFlipBudget ? $Property->ActualFlipBudget : 0);
                                            $F4 = ($Property->CapSelection=="ARV" ? $B4 * $F2 : $B4 * $F3);
                                            
                                            // FLIP
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B7[$i] = 1; // B7
                                                    $B8[$i] = ($i > ($MonthsFlipRehab + $MonthsToSell) ? 0 : 1); // B8
                                                    $B9[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B9
                                                } else {
                                                    $B7[$i] = ( $i > $MonthsFlipRehab ? 0 : 1); // B7
                                                    $B8[$i] = ($i > ($MonthsFlipRehab + $MonthsToSell) ? 0 : 1); // B8
                                                    $B9[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B9
                                                }
                                            }
                                            
                                            $B10 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
                                            $B11 = $PurchaseFinanced = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B10 : min($B10, $F4)) : 0);
                                            $B12 = $B10-$B11;
                                            $B13 = $Property->ClosingCosts ? $Property->ClosingCosts : 0;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B14[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab + $MonthsToSell) ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0)); // B14
                                                    $B15[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab+$MonthsToSell) ? ($OtherPoints)*$Property->ActualFinancedFlip : 0)); // B15
                                                    $B16[$i] = 0; // B16
                                                    $Row16Total = $Row16Total + $B16[$i];
                                                    $B17[$i] = $PurchaseFinanced + ($B13 + $B16[$i]) * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0); // B17
                                                    $B18[$i] = 0; // B18
                                                    $B19[$i] = 0; // B19
                                                    $B20[$i] = 0; // B20
                                                    $B21[$i] = 0; // B21
                                                    $B22[$i] = 0; // B22
                                                    $MonthTotal[$i] = ($Property->FlipRehabDrawSelection=="Fund Rehab at Closing" ? ($Property->FlipRehabBudgetMethod=="Quick Lump Sum" ? $LumpSumFlipBudget : $DetailedFlipBudget) : 0);
                                                    $B23[$i] = $MonthTotal[$i]; // B23
                                                    $B24[$i] = ($B3=="Cost" ? $MonthTotal[$i] * $B4 : max(0, min($F4, $MonthTotal[$i]))) * $B9[$i]; // B24
                                                    $Row24Total = $Row24Total + $B24[$i];
                                                    $B25[$i] = ($B23[$i] - $B24[$i]); // B25
                                                    $B26[$i] = ($B23[$i]==0 ? 0 : $B23[$i]) * $B8[$i]; // B26
                                                    $B27[$i] = $B24[$i]; // B27
                                                    $B28[$i] = 0; // B28
                                                    $B29[$i] = 0; // B29
                                                    $B30[$i] = 0; // B30
                                                    $B31[$i] = 0; // B31
                                                    $B32[$i] = 0; // B32
                                                    $B33[$i] = $B10+$B13+$B14[$i]+$B15[$i]+$B23[$i]; // B33
                                                    $B34[$i] = ($PurchaseFinanced + $B27[$i]) * $B8[$i]; // B34
                                                    $B35[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : $PurchaseFinanced+$Property->ClosingCosts*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+($Row24Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B14[$i]+$B15[$i]))); // B35
                                                    $B36[$i] = $B12+$Property->ClosingCosts*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B16[$i]+$B18[$i]+$B25[$i]+$B29[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B14[$i]+$B15[$i]) : 0); // B36
                                                    $B37[$i] = $B33[$i]-($B35[$i]+$B36[$i]); // B37
                                                    $B38[$i] = $B12+$B13*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B14[$i]+$B15[$i]+$B16[$i]+$B19[$i]+$B25[$i]+$B29[$i]; // B38
                                                } else {
                                                    $B14[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab + $MonthsToSell) ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0)); // B14
                                                    $B15[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab+$MonthsToSell) ? ($OtherPoints)*$ActualFinancedFlip : 0)); // B15
                                                    $B16[$i] = $FlipHoldingCosts/($MonthsFlipRehab+$MonthsToSell)*$B8[$i]; // B16
                                                    $Row16Total = $Row16Total + $B16[$i];
                                                    $B17[$i] = ($B17[$i-1] + $B16[$i] * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))*$B8[$i]; // B17
                                                    $B18[$i] = $RehabInterestRate * $B17[$i-1] / 12 * ($i>($MonthsFlipRehab+$MonthsToSell) ? 0 : 1); // B18
                                                    $B19[$i] = $Property->InterestPaymentsMade=="Yes" ? $B18[$i] : 0; // B19
                                                    $B20[$i] = $B18[$i] - $B19[$i]; // B20
                                                    if ( $i == 1 ) {
                                                        $B21[$i] = $B20[$i] + $B21[$i-1]; // B21
                                                    } else {
                                                        $B21[$i] = ($B20[$i]+$B21[$i-1]+$B21[$i-1]*($RehabInterestRate/12))*$B8[$i]; // B21
                                                    }
                                                    $B22[$i] = $B21[$i] * ($i == ($MonthsFlipRehab + $MonthsToSell) ? 1 : 0); // B22
                                                    $ColU = ($i<=$MonthsFlipRehab ? 1 : 0);
                                                    $MonthMatch = 0;
                                                    for ( $j = 0; $j < count($MonthPaidColumn); $j++ ) {
                                                        if ( $MonthPaidColumn[$j] == $i ) {
                                                            $MonthMatch += $FlipBudgetColumn[$j];
                                                        }
                                                    }
                                                    $MonthMatchArray[$i] = $MonthMatch;
                                                    $MonthTotal[$i] = $Property->FlipRehabDrawSelection=="Fund Rehab at Closing" ? 0 : ($Property->FlipRehabBudgetMethod=="Detailed Input" ? $MonthMatchArray[$i] * $ColU : $LumpSumFlipBudget / $MonthsFlipRehab * $ColU);
                                                    $B23[$i] = $MonthTotal[$i]; // B23
                                                    $B24[$i] = ($B3=="Cost" ? $MonthTotal[$i] * $B4 : max(0, min($F4-($B34[$i-1]), $B23[$i]))) * $B9[$i]; // B24
                                                    $Row24Total = $Row24Total + $B24[$i];
                                                    $B25[$i] = $B23[$i] - $B24[$i]; // B25
                                                    $B26[$i] = ($B23[$i]=="" ? $B26[$i-1] : $B23[$i] + $B26[$i-1]) * $B8[$i]; // B26
                                                    $B27[$i] = ($B24[$i]==0 ? $B27[$i-1] : $B24[$i] + $B27[$i-1]) * $B8[$i]; // B27
                                                    $B28[$i] = $B27[$i-1] * $RehabInterestRate/12 * $B8[$i] * $B9[$i]; // B28
                                                    $B29[$i] = $Property->InterestPaymentsMade=="Yes" ? $B28[$i] : 0; // B29
                                                    $B30[$i] = $B28[$i] - $B29[$i]; // B30
                                                    if ( $i == 1 ) {
                                                        $B31[$i] = $B30[$i] + $B31[$i-1]; // B31
                                                    } else {
                                                        $B31[$i] = ($B30[$i] + $B31[$i-1] + $B31[$i-1] * ($RehabInterestRate/12)) * $B8[$i]; // B31
                                                    }
                                                    $B32[$i] = $B31[$i] * ($i==($MonthsFlipRehab+$MonthsToSell) ? 1 : 0); // B32
                                                    $B33[$i] = ($B33[$i-1]+($B14[$i]+$B15[$i]+$B16[$i])+$B19[$i]+$B20[$i]+$B21[$i-1]*$RehabInterestRate/12+$B23[$i]+$B29[$i]+$B30[$i]+$B31[$i-1]*$RehabInterestRate/12)*$B8[$i]; // B33
                                                    $B34[$i] = ($PurchaseFinanced + $B27[$i]) * $B8[$i]; // B34
                                                    $B35[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($PurchaseFinanced+$Property->ClosingCosts*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+($Row16Total)*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+$B27[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B14[$i]+$B15[$i]))+$B21[$i]+$B31[$i])*$B8[$i]); // B35
                                                    $B36[$i] = ($B12+$B16[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B19[$i]+$B25[$i]+$B29[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B14[$i]+$B15[$i]) : 0)+$B36[$i-1])*$B8[$i]; // B36
                                                    $B37[$i] = $B33[$i]-($B35[$i]+$B36[$i]); // B37
                                                    $B38[$i] = $B16[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B19[$i]+$B25[$i]+$B29[$i]; // B38
                                                }
                                            }
                                            
                                            $B39 = array_sum($B38)-max($B36);
                                            $B40 = max($B33);
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B43[$i] = 0; // B43
                                                    $B44[$i] = 0; // B44
                                                    $B45[$i] = 0; // B45
                                                    $B46[$i] = 0; // B46
                                                    $B47[$i] = 0; // B47
                                                    $B48[$i] = 0; // B48
                                                    $B49[$i] = 0; // B49
                                                } else {
                                                    $B43[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $Property->ResalePrice : 0); // B43
                                                    $B44[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $Property->ResalePrice*($Property->CostOfSale/100) : 0); // B44
                                                    $B45[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? min($B35[$i], $B43[$i]-$B44[$i]) : 0); // B45
                                                    $B46[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? min($B36[$i], $B43[$i]-$B44[$i]-$B45[$i]) : 0); // B46
                                                    $B47[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $B43[$i]-$B44[$i]-$B45[$i]-$B46[$i] : 0); // B47
                                                    $B48[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $LenderSplit*($Property->FinancingUsed=="Financing" ? 1 : 0)*$B47[$i]*($Property->SplitWithLender=="Yes" ? 1 : 0) : 0); // B48
                                                    $B49[$i] = $B47[$i]-$B48[$i]; // B49
                                                }
                                            }
                                            
                                            $B52 = $Property->PurchasePrice;
                                            $B53 = $B13;
                                            $B54 = array_sum($B16);
                                            $B55 = array_sum($B14);
                                            $B56 = array_sum($B15);
                                            $B57 = array_sum($B23);
                                            $B58 = array_sum($B19)+array_sum($B22);
                                            $B59 = array_sum($B29)+array_sum($B32);
                                            $B61 = ($B52+$B53+$B54+$B55+$B56+$B57+$B58+$B59);
                                            
                                            $B63 = $PurchaseClosingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0);
                                            $B64 = $HoldingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? array_sum($B16) : 0);
                                            $B65 = $FlipPoints = array_sum($B14);
                                            $B66 = $FlipOtherFinancingCosts = array_sum($B15);
                                            $B67 = $FlipLoanInterest = array_sum($B19)+array_sum($B22);
                                            $B68 = $FlipRehabInterest = array_sum($B29)+array_sum($B32);
                                            $B69 = ($B63+$B64+$B65+$B66+$B67+$B68);
                                            
                                            $G64 = $PurchaseFinanced;
                                            $G65 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0));
                                            $G66 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? array_sum($B16) : 0));
                                            $G67 = ($Property->PointsClosingUpfront=="Paid Backend" ? array_sum($B14)+array_sum($B15) : 0)+($Property->InterestPaymentsMade=="No" ? (array_sum($B22)+array_sum($B32)) : 0);
                                            $G68 = array_sum($B24);
                                            $G69 = ($G64+$G65+$G66+$G67+$G68);
                                            $G71 = $FundedByLenderFlip = $G64+$G65+$G66+$G68;
                                            
                                            $B71 = $Property->TotalLoanFlip;
                                            $B72 = $Property->CashRequiredFlip;
                                            $B73 = ($B71+$B72);
                                            
                                            $D73 = $B40-$B73;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B75[$i] = 0; // B75
                                                    $B76[$i] = 0; // B76
                                                    $B78[$i] = 0; // B78
                                                    $B79[$i] = 0; // B79
                                                } else {
                                                    $B75[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($Property->CashRequiredFlip==0 ? "Infinite" : $B47[$i]/$Property->CashRequiredFlip) : 0); // B75
                                                    $B76[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($B75[$i]=="Infinite" ? "Infinite" : $B75[$i]/($MonthsFlipRehab+$MonthsToSell)*12) : 0); // B76
                                                    $B78[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($Property->CashRequiredFlip==0 ? "Infinite" : $B49[$i]/$Property->CashRequiredFlip) : 0); // B78
                                                    $B79[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($B78[$i]=="Infinite" ? "Inifinite" : $B78[$i]/($MonthsFlipRehab+$MonthsToSell)*12) : 0); // B79
                                                }
                                            }
                                            
                                            // Cashflows to Lender
                                            $B84 = -$PurchaseFinanced;
                                            $B85 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$Property->ClosingCosts : 0));
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B86[$i] = 0;
                                                    $B87[$i] = $B14[$i];
                                                    $B88[$i] = $B15[$i];
                                                    $B89[$i] = -$B24[$i];
                                                    $B90[$i] = 0;
                                                    $B91[$i] = 0;
                                                    $B92[$i] = 0;
                                                    $B93[$i] = 0;
                                                    $B94[$i] = $B35[$i];
                                                    $B95[$i] = 0;
                                                    $B96[$i] = 0;
                                                    
                                                    $B98[$i] = $B84+$B85+$B86[$i]+$B89[$i];
                                                    $B99[$i] = $B90[$i]+$B92[$i]+$B95[$i]+$B96[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B87[$i]+$B88[$i] : 0);
    
                                                    $B100[$i] = ($B98[$i]+$B99[$i]);
                                                } else {
                                                    $B86[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$B16[$i] : 0));
                                                    $B87[$i] = $B14[$i];
                                                    $B88[$i] = $B15[$i];
                                                    $B89[$i] = -$B24[$i];
                                                    $B90[$i] = $B19[$i];
                                                    $B91[$i] = $B20[$i];
                                                    $B92[$i] = $B29[$i];
                                                    $B93[$i] = $B30[$i];
                                                    $B94[$i] = $B35[$i];
                                                    $B95[$i] = $B45[$i];
                                                    $B96[$i] = $B48[$i];
                                                    
                                                    $B98[$i] = $B86[$i]+$B89[$i];
                                                    $B99[$i] = $B90[$i]+$B92[$i]+$B95[$i]+$B96[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B87[$i]+$B88[$i] : 0);
                                                    $B100[$i] = ($B98[$i]+$B99[$i]);
                                                }
                                            }
                                            $B101 = IRR($B100, 0.01);
                                            for ( $i=0; $i<12; $i++ ) {
                                                $IRRReslt *= ($B101+1);
                                            }
                                            $B102 = ($IRRReslt-1)*100;
                                            $C101 = $B101*12;
                                            $B103 = $TotalLenderInterestIncomeFlip = ($Property->InterestPaymentsMade=="No" ? array_sum($B22)+array_sum($B32) : array_sum($B19)+array_sum($B29));
                                            $B104 = $FeesToLenderFlip = array_sum($B14)+array_sum($B15);
                                            $B105 = array_sum($B48);
                                            $B106 = ($B103+$B104+$B105);
                                            
                                            $E104 = $B106/$G71;
                                            $E105 = $E104/($MonthsFlipRehab+$MonthsToSell)*12;
                                            
                                            $C106 = array_sum($B100)-$B106;
                                            
                                            $FinancingCostFlip = ($FlipPoints+$FlipOtherFinancingCosts+$FlipLoanInterest+$FlipRehabInterest);
                                            ?>
                                            <p class="text-center"><strong>Marketing Sheet for Flip</strong></p>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <td>Property Street Address:</td>
                                                            <td class="text-center"><strong><?=$FMS->PropertyStreetAddress?></strong></td>
                                                            <td class="text-center"><strong><u>Presented By:</u></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Property City, State, Zip:</td>
                                                            <td class="text-center"><strong><?=$FMS->PropertyCityTown?>, <?=$FMS->PropertyStateProvince?>, <?=$FMS->PropertyZipCode?>, <?=$FMS->PropertyCountry?></strong></td>
                                                            <td class="text-center"><strong><?=$FMS->YourName?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bedrooms:</td>
                                                            <td class="text-center"><?=$FMS->Bedrooms?></td>
                                                            <td class="text-center"><?=$FMS->CompanyName?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Baths:</td>
                                                            <td class="text-center"><?=$FMS->Bathrooms?></td>
                                                            <td class="text-center"><?=$FMS->PhoneNumber?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Square Feet:</td>
                                                            <td class="text-center"><?=$FMS->SquareFeet?></td>
                                                            <td class="text-center"><?=$FMS->Website?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Year Built:</td>
                                                            <td class="text-center"><?=$FMS->YearBuilt?></td>
                                                            <td class="text-center"><?=$FMS->Email?></td>
                                                        </tr>
                                                    </table>
                                                    <table class="table" style="border:none;">
                                                        <tr>
                                                            <td width="20%">Property Description:</td>
                                                            <td><?=$FMS->PropertyDescription?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">Work Needed:</td>
                                                            <td><?=$FMS->WorkNeeded?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-lg-7">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <th colspan="3"><u>PURCHASE/REHAB ASSUMPTIONS</u></th>
                                                        </tr>
                                                        <tr>
                                                            <td></td>
                                                            <td class="text-right"></td>
                                                            <td class="text-right">% of ARV</td>
                                                        </tr>
                                                        <tr>
                                                            <td>After-Repair Value (ARV)</td>
                                                            <td class="text-right"><?=number_format($FMS->ARVFlip, 2)?></td>
                                                            <td class="text-right"></td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Purchase Price</strong></td>
                                                            <td class="text-right"><strong><?=number_format($FMS->PurchasePrice, 2)?></strong></td>
                                                            <td class="text-right"><?=round(($FMS->ARVFlip == 0 ? 0 : round(($FMS->PurchasePrice/$FMS->ARVFlip)*100)))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Rehab Cost</td>
                                                            <td class="text-right"><?=number_format($FMS->ActualFlipBudget, 2)?></td>
                                                            <td class="text-right"><?=round(($FMS->ARVFlip == 0 ? 0 : round(($FMS->ActualFlipBudget/$FMS->ARVFlip)*100)))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Holding and Closing Costs</td>
                                                            <td class="text-right"><?=number_format($FMS->ClosingCosts+$FMS->FlipHoldingCosts, 2)?></td>
                                                            <td class="text-right"><?=round(($FMS->ARVFlip == 0 ? 0 : round((($FMS->ClosingCosts+$FMS->FlipHoldingCosts)/$FMS->ARVFlip)*100)))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Financing Costs</td>
                                                            <td class="text-right"><?=number_format($FinancingCostFlip, 2)?></td>
                                                            <td class="text-right"><?=round(($FMS->ARVFlip == 0 ? 0 : round(($FinancingCostFlip/$FMS->ARVFlip)*100)))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Total Project Cost Basis</strong></td>
                                                            <td class="text-right"><?=number_format($FMS->TotalCostBasisFlip, 2)?></td>
                                                            <td class="text-right"><?=round(($FMS->ARVFlip == 0 ? 0 : round(($FMS->TotalCostBasisFlip/$FMS->ARVFlip)*100)))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Amount Financed</td>
                                                            <td class="text-right"><?=number_format($FMS->TotalLoanFlip, 2)?></td>
                                                            <td class="text-right"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Cash Committed</td>
                                                            <td class="text-right"><?=number_format($FMS->CashRequiredFlip, 2)?></td>
                                                            <td class="text-right"></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-lg-5">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <th colspan="2"><u>PROJECTED RESULTS</u></th>
                                                        </tr>
                                                        <tr>
                                                            <td>Projected Resale Price</td>
                                                            <td class="text-right"><?=number_format($FMS->ResalePrice, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Projected Cost of Sale</td>
                                                            <td class="text-right"><?=number_format(($FMS->CostOfSale/100)*$FMS->ResalePrice, 2)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Flip Profit <?=$FMS->FinancingUsed=='Financing'? $FMS->SplitWithLender=='Yes'? ' (after lender split)':'':''?></strong></td>
                                                            <td class="text-right"><strong><?=number_format($FMS->FlipProfit, 2)?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>ROI</td>
                                                            <td class="text-right"><?=($FMS->FlipROI=='Infinite' ? 'Infinite' : round($FMS->FlipROI).'%')?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Annualized ROI</td>
                                                            <td class="text-right"><?=($FMS->FlipROIAnnualized=='Infinite' ? 'Infinite' : round($FMS->FlipROIAnnualized).'%')?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong><u>Timeline Assumptions:</u></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Time to Complete Rehab:</td>
                                                            <td class="text-right"><?=$FMS->MonthsFlipRehab==1?$FMS->MonthsFlipRehab . ' Month':$FMS->MonthsFlipRehab . ' Months'?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Time to Complete Sale:</td>
                                                            <td class="text-right"><?=$FMS->MonthsToSell==1?$FMS->MonthsToSell . ' Month':$FMS->MonthsToSell . ' Months'?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Time:</td>
                                                            <td class="text-right"><?=($FMS->MonthsFlipRehab+$FMS->MonthsToSell)==1?($FMS->MonthsFlipRehab+$FMS->MonthsToSell) . ' Month':($FMS->MonthsFlipRehab+$FMS->MonthsToSell) . ' Months'?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                            
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['RefiMarketingSheet']) ) {
                                        $RMS = $print_data['SingleProposal']['RefiMarketingSheet']; ?>
                                        
                                        <div class="RefiMarketingSheet"><br />
                                            <?php 
                                            $Property = $RMS->Property;
                                            $RefiBudget = $RMS->RefiBudget;
                                    
                                            $MonthsRefiRehab = $Property->MonthsRefiRehab ? $Property->MonthsRefiRehab : 0;
                                            $MonthsToRefi = $Property->MonthsToRefi ? $Property->MonthsToRefi : 0;
                                            $RehabDiscountPoints = $Property->RehabDiscountPoints ? ($Property->RehabDiscountPoints/100) : 0;
                                            $ActualFinancedRent = $Property->ActualFinancedRent ? $Property->ActualFinancedRent : 0;
                                            $OtherPoints = $Property->OtherPoints ? ($Property->OtherPoints/100) : 0;
                                            $RefiHoldingCosts = $Property->RefiHoldingCosts ? $Property->RefiHoldingCosts : 0;
                                            $RehabInterestRate = $Property->RehabInterestRate ? ($Property->RehabInterestRate/100) : 0;
                                            $LumpSumRefiBudget = $Property->LumpSumRefiBudget ? $Property->LumpSumRefiBudget : 0;
                                            $DetailedRefiBudget = $Property->DetailedRefiBudget ? $Property->DetailedRefiBudget : 0; 
                                            $LenderSplit = $Property->LenderSplit ? ($Property->LenderSplit/100) : 0;
                                            
                                            $Row118Total = 0; $Row126Total = 0; $IRRReslt = 1;
                                            $MonthTotal = array();
                                            $MonthMatchArray = array();
                                            $RefiBudgetColumn = array(); $MonthPaidColumn = array();
                                            foreach ( $RefiBudget as $key => $refi ) {
                                                $RefiBudgetColumn[$key] = $refi->Budget;
                                                $MonthPaidColumn[$key] = $refi->MonthPaid;
                                            }
                                            
                                            $B2 = ($Property->FinancingUsed=="Financing" ? "Yes" : "No");
                                            $B3 = $Property->CapSelection;
                                            $B4 = $Property->FinancingCap ? ($Property->FinancingCap/100) : 0;
											
											$F2 = $Property->ARVFlip ? $Property->ARVFlip : 0;
											$F3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualFlipBudget ? $Property->ActualFlipBudget : 0);
											$F4 = ($Property->CapSelection=="ARV" ? $B4 * $F2 : $B4 * $F3);
											
											$B10 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
											$B11 = $PurchaseFinanced = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B10 : min($B10, $F4)) : 0);
                                            
                                            $I2 = $Property->ARVRent ? $Property->ARVRent : 0;
                                            $I3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualRefiBudget ? $Property->ActualRefiBudget : 0);
                                            $I4 = ($Property->CapSelection=="ARV" ? $B4 * $I2 : $B4 * $I3);
                                            
                                            // REFI
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B109[$i] = 1; // B109
                                                    $B110[$i] = ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B110
                                                    $B111[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B111
                                                } else {
                                                    $B109[$i] = ($i>$MonthsRefiRehab ? 0 : 1); // B109
                                                    $B110[$i] = ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B110
                                                    $B111[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B111
                                                }
                                            }
                                            
                                            $B112 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
                                            $B113 = $PurchaseFinancedRefi = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B112 : min($B112, $I4)) : 0);
                                            $B114 = $B112-$B113;
                                            $B115 = $Property->ClosingCosts ? $Property->ClosingCosts : 0;
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B116[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($RehabDiscountPoints)*$ActualFinancedRent : 0); // B116
                                                    $B117[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($OtherPoints)*$ActualFinancedRent : 0); // B117
                                                    $B118[$i] = 0; // B118
                                                    $Row118Total = $Row118Total + $B118[$i];
                                                    $B119[$i] = $PurchaseFinanced + ($B115 + $B118[$i]) * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0); // B119
                                                    $B120[$i] = 0; // B120
                                                    $B121[$i] = 0; // B121
                                                    $B122[$i] = 0; // B122
                                                    $B123[$i] = 0; // B123
                                                    $B124[$i] = 0; // B124
                                                    $B125[$i] = ($Property->RefiRehabDrawSelection=="Fund Rehab at Closing" ? ($Property->RefiRehabBudgetMethod=="Quick Lump Sum" ? $LumpSumRefiBudget : $DetailedRefiBudget) : 0); // B125
                                                    $B126[$i] = ($B3=="Cost" ? $B125[$i] * $B4 : max(0, min($I4, $B125[$i]))) * $B111[$i]; // B126
                                                    $Row126Total = $Row126Total + $B126[$i];
                                                    $B127[$i] = $B125[$i] - $B126[$i]; // B127
                                                    $B128[$i] = ($B125[$i]==0 ? 0 : $B125[$i]) * $B110[$i]; // B128
                                                    $B129[$i] = $B126[$i]; // B129
                                                    $B130[$i] = 0; // B130
                                                    $B131[$i] = 0; // B131
                                                    $B132[$i] = 0; // B132
                                                    $B133[$i] = 0; // B133
                                                    $B134[$i] = 0; // B134
                                                    $B135[$i] = $B112+$B115+$B116[$i]+$B117[$i]+$B125[$i]; // B135
                                                    $B136[$i] = $PurchaseFinancedRefi+($Row126Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])); // B136
                                                    $B137[$i] = $PurchaseFinancedRefi+$Property->ClosingCosts*($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))+($Row126Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])); // B137
                                                    $B138[$i] = $B114+($B115+$B118[$i])*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B120[$i]+$B127[$i]+$B131[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B116[$i]+$B117[$i]) : 0); // B138
                                                    $B139[$i] = $B135[$i]-($B137[$i]+$B138[$i]); // B139
                                                    $B140[$i] = $B114+$B115*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 :1))+$B116[$i]+$B117[$i]+$B118[$i]*($Property->FinanceClosingHolding=="Yes" ? 0 : 1)+$B121[$i]+$B127[$i]+$B131[$i]; // B140
                                                } else {
                                                    $B116[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($RehabDiscountPoints)*$ActualFinancedRent : 0); // B116
                                                    $B117[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($OtherPoints)*$ActualFinancedRent : 0); // B117				
                                                    $B118[$i] = $RefiHoldingCosts/($MonthsRefiRehab+$MonthsToRefi)*$B110[$i]; // B118
                                                    $Row118Total = $Row118Total + $B118[$i];
                                                    $B119[$i] = ($B119[$i-1] + $B118[$i] * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))*$B110[$i]; // B119
                                                    $B120[$i] = $RehabInterestRate*$B119[$i-1] / 12 * ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B120
                                                    $B121[$i] = $Property->InterestPaymentsMade=="Yes" ? $B120[$i] : 0; // B121
                                                    $B122[$i] = $B120[$i] - $B121[$i]; // B122
                                                    if ( $i == 1 ) {
                                                        $B123[$i] = $B122[$i] + $B123[$i-1]; // B123
                                                    } else {
                                                        $B123[$i] = ($B122[$i]+$B123[$i-1]+$B123[$i-1]*($RehabInterestRate/12))*$B110[$i]; // B123
                                                    }
                                                    $B124[$i] = $B123[$i] * ($i == ($MonthsRefiRehab + $MonthsToRefi) ? 1 : 0); // B124
                                                    
                                                    $ColU = ($i<=$MonthsRefiRehab ? 1 : 0);
                                                    $MonthMatch = 0;
                                                    for ( $j = 0; $j < count($MonthPaidColumn); $j++ ) {
                                                        if ( $MonthPaidColumn[$j] == $i ) {
                                                            $MonthMatch += $RefiBudgetColumn[$j];
                                                        }
                                                    }
                                                    $MonthMatchArray[$i] = $MonthMatch;
                                                    $B125[$i] = $Property->RefiRehabDrawSelection=="Fund Rehab at Closing" ? 0 : ($Property->RefiRehabBudgetMethod=="Detailed Input" ? $MonthMatchArray[$i] * $ColU : $LumpSumFlipBudget / $MonthsRefiRehab * $ColU); // B125
                                                    $B126[$i] = ($B3=="Cost" ? $B125[$i] * $B4 : max(0, min($I4-$B137[$i-1], $B125[$i]))) * $B111[$i]; // B126
                                                    $B127[$i] = $B125[$i] - $B126[$i]; // B127
                                                    $B128[$i] = ($B125[$i]==0 ? $B128[$i-1] : $B125[$i] + $B128[$i-1]) * $B110[$i]; // B128
                                                    $B129[$i] = ($B126[$i]==0 ? $B129[$i-1] : $B126[$i] + $B129[$i-1]) * $B110[$i]; // B129
                                                    $B130[$i] = $B129[$i-1] * $RehabInterestRate/12 * $B110[$i] * $B111[$i]; // B130
                                                    $B131[$i] = $Property->InterestPaymentsMade=="Yes" ? $B130[$i] : 0; // B131
                                                    $B132[$i] = $B130[$i] - $B131[$i]; // B132
                                                    if ( $i == 1 ) {
                                                        $B133[$i] = $B132[$i] + $B133[$i-1]; // B133
                                                    } else {
                                                        $B133[$i] = ($B132[$i] + $B133[$i-1] + $B133[$i-1] * ($RehabInterestRate/12)) * $B110[$i]; // B133
                                                    }
                                                    $B134[$i] = $B133[$i] * ($i==($MonthsRefiRehab+$MonthsToRefi) ? 1 : 0); // B134
                                                    $B135[$i] = ($B135[$i-1]+($B116[$i]+$B117[$i]+$B118[$i])+$B121[$i]+$B122[$i]+$B123[$i-1]*$RehabInterestRate/12+$B125[$i]+$B131[$i]+$B132[$i]+$B133[$i-1]*$RehabInterestRate/12)*$B110[$i]; // B135
                                                    $B136[$i] = ($PurchaseFinancedRefi+$B129[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])))*$B110[$i]; // B136
                                                    $B137[$i] = ($PurchaseFinancedRefi+($Property->ClosingCosts+($Row118Total))*($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))+$B129[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i]))+$B123[$i]+$B133[$i])*$B110[$i]; // B137
                                                    $B138[$i] = ($B118[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B121[$i]+$B127[$i]+$B131[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B116[$i]+$B117[$i]) : 0)+$B138[$i-1])*$B110[$i]; // B138
                                                    $B139[$i] = $B135[$i]-($B137[$i]+$B138[$i]); // B139
                                                    $B140[$i] = $B118[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B121[$i]+$B127[$i]+$B131[$i]; // B140
                                                }
                                            }
                                            
                                            $B141 = array_sum($B140)-max($B138);
                                            $B142 = max($B135);
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B144[$i] = 0; // B144
                                                    $B145[$i] = 0; // B145
                                                    $B145[$i] = 0; // B146
                                                    $B147[$i] = 0; // B147
                                                    $B149[$i] = 0; // B149
                                                    $B150[$i] = 0; // B150
                                                    $B151[$i] = 0; // B151
                                                    $B152[$i] = 0; // B152
                                                    $B153[$i] = 0; // B153
                                                    $B154[$i] = 0; // B154
                                                } else {
                                                    $B144[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->RefiAmount : 0); // B144
                                                    $B145[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? -($Property->RefiDiscountPoints/100)*$Property->RefiAmount : 0); // B145
                                                    $B146[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? -$B137[$i] : 0); // B146
                                                    $B147[$i] = ($B144[$i]+$B145[$i]+$B146[$i]); // B147
                                                    $B149[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? max(0, $B147[$i]-$Property->CashRequiredRent) : 0); // B149
                                                    $B150[$i] = 	($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->LenderSplit*($Property->FinancingUsed=="Financing" ? 1 : 0)*$B149[$i]*($Property->SplitWithLender=="Yes" ? 1 : 0) : 0); // B150
                                                    $B151[$i] = $B149[$i]-$B150[$i]; // B151
                                                    $B152[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "infinite" : $B149[$i]/$Property->CashRequiredRent/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B152
                                                    $B153[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? max(0, $B138[$i]-$B147[$i]) : 0); // B153
                                                    $B154[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->ARVRent-$Property->RefiAmount : 0); // B154
                                                }
                                            }
                                            
                                            $B158 = $Property->PurchasePrice;
                                            $B159 = $B115;
                                            $B160 = array_sum($B118);
                                            $B161 = array_sum($B116);
                                            $B162 = array_sum($B117);
                                            $B163 = array_sum($B125);
                                            $B164 = array_sum($B121)+array_sum($B124);
                                            $B165 = array_sum($B131)+array_sum($B134);
                                            $B167 = ($B158+$B159+$B160+$B161+$B162+$B163+$B164+$B165);
                                            
                                            $B169 = $RefiClosingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? $B115 : 0);
                                            $B170 = $RefiHoldingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? array_sum($B118) : 0);
                                            $B171 = $RentPoints = array_sum($B116);
                                            $B172 = $RentOtherFinancingCosts = array_sum($B117);
                                            $B173 = $RentLoanInterest = array_sum($B121)+array_sum($B124);
                                            $B174 = $RentRehabInterest = array_sum($B131)+array_sum($B134);
                                            $B175 = ($B171+$B172+$B173+$B174);
                                            
                                            $G170 = $PurchaseFinanced;
                                            $G171 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0));
                                            $G172 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? array_sum($B118) : 0));
                                            $G173 = ($Property->PointsClosingUpfront=="Paid Backend" ? array_sum($B116)+array_sum($B117) : 0)+($Property->InterestPaymentsMade=="No" ? (array_sum($B124)+array_sum($B134)) : 0);
                                            $G174 = array_sum($B126);
                                            $G175 = ($G170+$G171+$G172+$G173+$G174);
                                            $G177 = $FundedByLenderREfi = $G170+$G171+$G172+$G174;
                                            
                                            $B177 = $Property->TotalLoanRent;
                                            $B178 = $Property->CashRequiredRent;
                                            $B179 = ($B177+$B178);
                                            
                                            $D179 = $B142-$B179;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B181[$i] = 0; // B181
                                                    $B182[$i] = 0; // B182
                                                    $B184[$i] = 0; // B184
                                                    $B185[$i] = 0; // B185
                                                } else {
                                                    $B181[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "Infinite" : $B149[$i]/$Property->CashRequiredRent) : 0); // B181
                                                    $B182[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($B181[$i]=="Infinite" ? "Infinite" : $B181[$i]/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B182
                                                    $B184[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "Infinite" : $B151[$i]/$Property->CashRequiredRent) : 0); // B184
                                                    $B185[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($B184[$i]=="Infinite" ? "Infinite" : $B184[$i]/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B185
                                                }
                                            }
                                            
                                            // Cashflows to Lender
                                            $B190 = -$PurchaseFinancedRefi;
                                            $B191 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$Property->ClosingCosts : 0));
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B192[$i] = 0;
                                                    $B193[$i] = $B116[$i];
                                                    $B194[$i] = $B117[$i];
                                                    $B195[$i] = -$B126[$i];
                                                    $B196[$i] = 0;
                                                    $B197[$i] = 0;
                                                    $B198[$i] = 0;
                                                    $B199[$i] = 0;
                                                    $B200[$i] = $B137[$i];
                                                    $B201[$i] = 0;
                                                    $B202[$i] = 0;
                                                    
                                                    $B204[$i] = $B190+$B191+$B192[$i]+$B195[$i];
                                                    $B205[$i] = $B196[$i]+$B198[$i]+$B201[$i]+$B202[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B193[$i]+$B194[$i] : 0);
                                                    $B206[$i] = ($B204[$i]+$B205[$i]);
                                                } else {
                                                    $B192[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$B118[$i] : 0));
                                                    $B193[$i] = $B116[$i];
                                                    $B194[$i] = $B117[$i];
                                                    $B195[$i] = -$B126[$i];
                                                    $B196[$i] = $B121[$i];
                                                    $B197[$i] = $B122[$i];
                                                    $B198[$i] = $B131[$i];
                                                    $B199[$i] = $B132[$i];
                                                    $B200[$i] = $B137[$i];
                                                    $B201[$i] = -$B146[$i];
                                                    $B202[$i] = $B150[$i];
                                                    
                                                    $B204[$i] = $B192[$i]+$B195[$i];
                                                    $B205[$i] = $B196[$i]+$B198[$i]+$B201[$i]+$B202[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B193[$i]+$B194[$i] : 0);
                                                    $B206[$i] = ($B204[$i]+$B205[$i]);
                                                }
                                            }
                                            $B207 = IRR($B206, 0.01);
                                            for ( $i=0; $i<12; $i++ ) {
                                                $IRRReslt *= ($B207+1);
                                            }
                                            $B208 = ($IRRReslt-1)*100;
                                            $C207 = $B207*12;
                                            $B209 = $TotalLenderInterestIncomeRefi = ($Property->InterestPaymentsMade=="No" ? array_sum($B124)+array_sum($B134) : array_sum($B121)+array_sum($B131));
                                            $B210 = $FeesToLenderRefi = array_sum($B116)+array_sum($B117);
                                            $B211 = array_sum($B150);
                                            $B212 = ($B209+$B210+$B211);
                                            
                                            $E210 = $B212/$G177;
                                            $E211 = $E210/($MonthsRefiRehab+$MonthsToRefi)*12;
                                            
                                            $C212 = array_sum($B206)-$B212;
                                            
                                            $FinancingCostRent = $RentPoints+$RentOtherFinancingCosts+$RentLoanInterest+$RentRehabInterest;
                                            ?>
                                            <p class="text-center"><strong>Marketing Sheet for Refi</strong></p>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <td>Property Street Address:</td>
                                                            <td class="text-center"><strong><?=$RMS->PropertyStreetAddress?></strong></td>
                                                            <td class="text-center"><strong><u>Presented By:</u></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Property City, State, Zip:</td>
                                                            <td class="text-center"><strong><?=$RMS->PropertyCityTown?>, <?=$RMS->PropertyStateProvince?>, <?=$RMS->PropertyZipCode?>, <?=$RMS->PropertyCountry?></strong></td>
                                                            <td class="text-center"><strong><?=$RMS->YourName?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bedrooms:</td>
                                                            <td class="text-center"><?=$RMS->Bedrooms?></td>
                                                            <td class="text-center"><?=$RMS->CompanyName?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Baths:</td>
                                                            <td class="text-center"><?=$RMS->Bathrooms?></td>
                                                            <td class="text-center"><?=$RMS->PhoneNumber?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Square Feet:</td>
                                                            <td class="text-center"><?=$RMS->SquareFeet?></td>
                                                            <td class="text-center"><?=$RMS->Website?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Year Built:</td>
                                                            <td class="text-center"><?=$RMS->YearBuilt?></td>
                                                            <td class="text-center"><?=$RMS->Email?></td>
                                                        </tr>
                                                    </table>
                                                    <table class="table" style="border:none;">
                                                        <tr>
                                                            <td width="20%">Property Description:</td>
                                                            <td><?=$RMS->PropertyDescription?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">Work Needed:</td>
                                                            <td><?=$RMS->WorkNeeded?></td>
                                                        </tr>
                                                    </table>
                                                    <div class="col-lg-8 col-lg-offset-2">
                                                        <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                            <tr>
                                                                <th colspan="3"><u>PURCHASE/REHAB ASSUMPTIONS</u></th>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td class="text-right"></td>
                                                                <td class="text-right">% of ARV</td>
                                                            </tr>
                                                            <tr>
                                                                <td>After-Repair Value (ARV)</td>
                                                                <td class="text-right"><?=number_format($RMS->ARVRent, 2)?></td>
                                                                <td class="text-right"></td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Purchase Price</strong></td>
                                                                <td class="text-right"><strong><?=number_format($RMS->PurchasePrice, 2)?></strong></td>
                                                                <td class="text-right"><?=round(($RMS->ARVRent == 0 ? 0 : round(($RMS->PurchasePrice/$RMS->ARVRent)*100)))?>%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Rehab Cost</td>
                                                                <td class="text-right"><?=number_format($RMS->ActualRefiBudget, 2)?></td>
                                                                <td class="text-right"><?=round(($RMS->ARVRent == 0 ? 0 : round(($RMS->ActualRefiBudget/$RMS->ARVRent)*100)))?>%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Holding and Closing Costs</td>
                                                                <td class="text-right"><?=number_format($RMS->ClosingCosts+$RMS->RefiHoldingCosts, 2)?></td>
                                                                <td class="text-right"><?=round(($RMS->ARVRent == 0 ? 0 : round((($RMS->ClosingCosts+$RMS->RefiHoldingCosts)/$RMS->ARVRent)*100)))?>%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Financing Costs</td>
                                                                <td class="text-right"><?=number_format($FinancingCostRent, 2)?></td>
                                                                <td class="text-right"><?=round(($RMS->ARVRent == 0 ? 0 : round(($FinancingCostRent/$RMS->ARVRent)*100)))?>%</td>
                                                            </tr>
                                                            <tr>
                                                                <td><strong>Total Project Cost Basis</strong></td>
                                                                <td class="text-right"><?=number_format($RMS->TotalCostBasisRent, 2)?></td>
                                                                <td class="text-right"><?=round(($RMS->ARVRent == 0 ? 0 : ($RMS->TotalCostBasisRent/$RMS->ARVRent)*100))?>%</td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Amount Financed</td>
                                                                <td class="text-right"><?=number_format($RMS->TotalLoanRent, 2)?></td>
                                                                <td class="text-right"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Cash Committed</td>
                                                                <td class="text-right"><?=number_format($RMS->CashRequiredRent, 2)?></td>
                                                                <td class="text-right"></td>
                                                            </tr>
                                                        </table><br>
                                                    </div>
                                                    <div class="col-lg-10 col-lg-offset-1">
                                                        <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                            <tr>
                                                                <th colspan="4"><u>PROJECTED RESULTS</u></th>
                                                            </tr>
                                                            <tr>
                                                                <td>Projected New Loan Amount (for Refi)</td>
                                                                <td class="text-right"><?=number_format($RMS->RefiAmount, 2)?></td>
                                                                <td>Cash Left in the Deal after Refi</td>
                                                                <td class="text-right"><?=number_format($RMS->CashTiedUP, 2)?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Closing costs on New Loan</td>
                                                                <td class="text-right"><?=number_format(($RMS->RefiDiscountPoints/100)*$RMS->RefiAmount, 2)?></td>
                                                                <td>Equity Left in the Deal after Refi</td>
                                                                <td class="text-right"><?=number_format($RMS->EquityLeft, 2)?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Cash-Out at Refi</td>
                                                                <td class="text-right"><?=number_format($RMS->CashOutRefi, 2)?></td>
                                                                <td>Monthly Cash Flow (before-tax)</td>
                                                                <td class="text-right"><?=number_format($RMS->CashflowMonthly, 2)?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Profit at Refi <?=$RMS->FinancingUsed=='Financing'?$RMS->SplitWithLender=='Yes'?' (after lender split)':'':''?></td>
                                                                <td class="text-right"><?=number_format($RMS->RefiProfit, 2)?></td>
                                                                <td>Cash-on-Cash Return (before-tax)</td>
                                                                <td class="text-right"><?=($RMS->RefiROIAnnual=='Infinite' ? 'Infinite' : round($RMS->RefiROIAnnual).'%')?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>*(cash pulled out in excess of investment)</td>
                                                                <td class="text-right"></td>
                                                                <td><strong>DCR of New Loan</strong></td>
                                                                <td class="text-right"><?=number_format($RMS->DCR, 2)?></td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td colspan="2">
                                                                    <em>Assuming <?=round($RMS->PermanentRate)?>% Rate and <?=$RMS->Amortization?> Year Amortization</em>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Assumed Time to Complete Rehab:</td>
                                                                <td class="text-right"><?=$RMS->MonthsRefiRehab==1?$RMS->MonthsRefiRehab . ' Month':$RMS->MonthsRefiRehab . ' Months'?></td>
                                                                <td colspan="2"></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Assumed Time to Complete Refi:</td>
                                                                <td class="text-right"><?=$RMS->MonthsToRefi==1?$RMS->MonthsToRefi . ' Month':$RMS->MonthsToRefi . ' Months'?></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Total Time between Acquisition and Refi:</td>
                                                                <td class="text-right"><?=($RMS->MonthsRefiRehab+$RMS->MonthsToRefi)==1?($RMS->MonthsRefiRehab+$RMS->MonthsToRefi) . ' Month':($RMS->MonthsRefiRehab+$RMS->MonthsToRefi) . ' Months'?></td>
                                                                <td colspan="2"></td>
                                
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['FlipCashFlow']) ) {
                                        $FCF = $print_data['SingleProposal']['FlipCashFlow']; ?>
                                        
                                        <div class="FlipCashFlow"><br />
                                            <?php 
                                            $Property = $FCF['Property'];
                                            $FlipBudget = $FCF['FlipBudget'];
                                            
                                            $MonthsFlipRehab = $Property->MonthsFlipRehab ? $Property->MonthsFlipRehab : 0;
                                            $MonthsToSell = $Property->MonthsToSell ? $Property->MonthsToSell : 0;
                                            $RehabDiscountPoints = $Property->RehabDiscountPoints ? ($Property->RehabDiscountPoints/100) : 0;
                                            $ActualFinancedFlip = $Property->ActualFinancedFlip ? $Property->ActualFinancedFlip : 0;
                                            $OtherPoints = $Property->OtherPoints ? ($Property->OtherPoints/100) : 0;
                                            $FlipHoldingCosts = $Property->FlipHoldingCosts ? $Property->FlipHoldingCosts : 0;
                                            $RehabInterestRate = $Property->RehabInterestRate ? ($Property->RehabInterestRate/100) : 0;
                                            $LumpSumFlipBudget = $Property->LumpSumFlipBudget ? $Property->LumpSumFlipBudget : 0;
                                            $DetailedFlipBudget = $Property->DetailedFlipBudget ? $Property->DetailedFlipBudget : 0;
                                            $LenderSplit = $Property->LenderSplit ? ($Property->LenderSplit/100) : 0;
                                            
                                            $Row16Total = 0; $Row24Total = 0; $IRRReslt = 1;
                                            $MonthTotal = array();
                                            $MonthMatchArray = array();
                                            $FlipBudgetColumn = array(); $MonthPaidColumn = array();
                                            foreach ( $FlipBudget as $key => $flip ) {
                                                $FlipBudgetColumn[$key] = $flip->Budget;
                                                $MonthPaidColumn[$key] = $flip->MonthPaid;
                                            }
                                            
                                            $B2 = ($Property->FinancingUsed=="Financing" ? "Yes" : "No");
                                            $B3 = $Property->CapSelection;
                                            $B4 = $Property->FinancingCap ? ($Property->FinancingCap/100) : 0;
                                            
                                            $F2 = $Property->ARVFlip ? $Property->ARVFlip : 0;
                                            $F3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualFlipBudget ? $Property->ActualFlipBudget : 0);
                                            $F4 = ($Property->CapSelection=="ARV" ? $B4 * $F2 : $B4 * $F3);
                                            
                                            // FLIP
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B7[$i] = 1; // B7
                                                    $B8[$i] = ($i > ($MonthsFlipRehab + $MonthsToSell) ? 0 : 1); // B8
                                                    $B9[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B9
                                                } else {
                                                    $B7[$i] = ( $i > $MonthsFlipRehab ? 0 : 1); // B7
                                                    $B8[$i] = ($i > ($MonthsFlipRehab + $MonthsToSell) ? 0 : 1); // B8
                                                    $B9[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B9
                                                }
                                            }
                                            
                                            $B10 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
                                            $B11 = $PurchaseFinanced = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B10 : min($B10, $F4)) : 0);
                                            $B12 = $B10-$B11;
                                            $B13 = $Property->ClosingCosts ? $Property->ClosingCosts : 0;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B14[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab + $MonthsToSell) ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0)); // B14
                                                    $B15[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab+$MonthsToSell) ? ($OtherPoints)*$Property->ActualFinancedFlip : 0)); // B15
                                                    $B16[$i] = 0; // B16
                                                    $Row16Total = $Row16Total + $B16[$i];
                                                    $B17[$i] = $PurchaseFinanced + ($B13 + $B16[$i]) * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0); // B17
                                                    $B18[$i] = 0; // B18
                                                    $B19[$i] = 0; // B19
                                                    $B20[$i] = 0; // B20
                                                    $B21[$i] = 0; // B21
                                                    $B22[$i] = 0; // B22
                                                    $MonthTotal[$i] = ($Property->FlipRehabDrawSelection=="Fund Rehab at Closing" ? ($Property->FlipRehabBudgetMethod=="Quick Lump Sum" ? $LumpSumFlipBudget : $DetailedFlipBudget) : 0);
                                                    $B23[$i] = $MonthTotal[$i]; // B23
                                                    $B24[$i] = ($B3=="Cost" ? $MonthTotal[$i] * $B4 : max(0, min($F4, $MonthTotal[$i]))) * $B9[$i]; // B24
                                                    $Row24Total = $Row24Total + $B24[$i];
                                                    $B25[$i] = ($B23[$i] - $B24[$i]); // B25
                                                    $B26[$i] = ($B23[$i]==0 ? 0 : $B23[$i]) * $B8[$i]; // B26
                                                    $B27[$i] = $B24[$i]; // B27
                                                    $B28[$i] = 0; // B28
                                                    $B29[$i] = 0; // B29
                                                    $B30[$i] = 0; // B30
                                                    $B31[$i] = 0; // B31
                                                    $B32[$i] = 0; // B32
                                                    $B33[$i] = $B10+$B13+$B14[$i]+$B15[$i]+$B23[$i]; // B33
                                                    $B34[$i] = ($PurchaseFinanced + $B27[$i]) * $B8[$i]; // B34
                                                    $B35[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : $PurchaseFinanced+$Property->ClosingCosts*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+($Row24Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B14[$i]+$B15[$i]))); // B35
                                                    $B36[$i] = $B12+$Property->ClosingCosts*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B16[$i]+$B18[$i]+$B25[$i]+$B29[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B14[$i]+$B15[$i]) : 0); // B36
                                                    $B37[$i] = $B33[$i]-($B35[$i]+$B36[$i]); // B37
                                                    $B38[$i] = $B12+$B13*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B14[$i]+$B15[$i]+$B16[$i]+$B19[$i]+$B25[$i]+$B29[$i]; // B38
                                                } else {
                                                    $B14[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab + $MonthsToSell) ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0)); // B14
                                                    $B15[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab+$MonthsToSell) ? ($OtherPoints)*$ActualFinancedFlip : 0)); // B15
                                                    $B16[$i] = $FlipHoldingCosts/($MonthsFlipRehab+$MonthsToSell)*$B8[$i]; // B16
                                                    $Row16Total = $Row16Total + $B16[$i];
                                                    $B17[$i] = ($B17[$i-1] + $B16[$i] * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))*$B8[$i]; // B17
                                                    $B18[$i] = $RehabInterestRate * $B17[$i-1] / 12 * ($i>($MonthsFlipRehab+$MonthsToSell) ? 0 : 1); // B18
                                                    $B19[$i] = $Property->InterestPaymentsMade=="Yes" ? $B18[$i] : 0; // B19
                                                    $B20[$i] = $B18[$i] - $B19[$i]; // B20
                                                    if ( $i == 1 ) {
                                                        $B21[$i] = $B20[$i] + $B21[$i-1]; // B21
                                                    } else {
                                                        $B21[$i] = ($B20[$i]+$B21[$i-1]+$B21[$i-1]*($RehabInterestRate/12))*$B8[$i]; // B21
                                                    }
                                                    $B22[$i] = $B21[$i] * ($i == ($MonthsFlipRehab + $MonthsToSell) ? 1 : 0); // B22
                                                    $ColU = ($i<=$MonthsFlipRehab ? 1 : 0);
                                                    $MonthMatch = 0;
                                                    for ( $j = 0; $j < count($MonthPaidColumn); $j++ ) {
                                                        if ( $MonthPaidColumn[$j] == $i ) {
                                                            $MonthMatch += $FlipBudgetColumn[$j];
                                                        }
                                                    }
                                                    $MonthMatchArray[$i] = $MonthMatch;
                                                    $MonthTotal[$i] = $Property->FlipRehabDrawSelection=="Fund Rehab at Closing" ? 0 : ($Property->FlipRehabBudgetMethod=="Detailed Input" ? $MonthMatchArray[$i] * $ColU : $LumpSumFlipBudget / $MonthsFlipRehab * $ColU);
                                                    $B23[$i] = $MonthTotal[$i]; // B23
                                                    $B24[$i] = ($B3=="Cost" ? $MonthTotal[$i] * $B4 : max(0, min($F4-($B34[$i-1]), $B23[$i]))) * $B9[$i]; // B24
                                                    $Row24Total = $Row24Total + $B24[$i];
                                                    $B25[$i] = $B23[$i] - $B24[$i]; // B25
                                                    $B26[$i] = ($B23[$i]=="" ? $B26[$i-1] : $B23[$i] + $B26[$i-1]) * $B8[$i]; // B26
                                                    $B27[$i] = ($B24[$i]==0 ? $B27[$i-1] : $B24[$i] + $B27[$i-1]) * $B8[$i]; // B27
                                                    $B28[$i] = $B27[$i-1] * $RehabInterestRate/12 * $B8[$i] * $B9[$i]; // B28
                                                    $B29[$i] = $Property->InterestPaymentsMade=="Yes" ? $B28[$i] : 0; // B29
                                                    $B30[$i] = $B28[$i] - $B29[$i]; // B30
                                                    if ( $i == 1 ) {
                                                        $B31[$i] = $B30[$i] + $B31[$i-1]; // B31
                                                    } else {
                                                        $B31[$i] = ($B30[$i] + $B31[$i-1] + $B31[$i-1] * ($RehabInterestRate/12)) * $B8[$i]; // B31
                                                    }
                                                    $B32[$i] = $B31[$i] * ($i==($MonthsFlipRehab+$MonthsToSell) ? 1 : 0); // B32
                                                    $B33[$i] = ($B33[$i-1]+($B14[$i]+$B15[$i]+$B16[$i])+$B19[$i]+$B20[$i]+$B21[$i-1]*$RehabInterestRate/12+$B23[$i]+$B29[$i]+$B30[$i]+$B31[$i-1]*$RehabInterestRate/12)*$B8[$i]; // B33
                                                    $B34[$i] = ($PurchaseFinanced + $B27[$i]) * $B8[$i]; // B34
                                                    $B35[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($PurchaseFinanced+$Property->ClosingCosts*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+($Row16Total)*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+$B27[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B14[$i]+$B15[$i]))+$B21[$i]+$B31[$i])*$B8[$i]); // B35
                                                    $B36[$i] = ($B12+$B16[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B19[$i]+$B25[$i]+$B29[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B14[$i]+$B15[$i]) : 0)+$B36[$i-1])*$B8[$i]; // B36
                                                    $B37[$i] = $B33[$i]-($B35[$i]+$B36[$i]); // B37
                                                    $B38[$i] = $B16[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B19[$i]+$B25[$i]+$B29[$i]; // B38
                                                }
                                            }
                                            
                                            $B39 = array_sum($B38)-max($B36);
                                            $B40 = max($B33);
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B43[$i] = 0; // B43
                                                    $B44[$i] = 0; // B44
                                                    $B45[$i] = 0; // B45
                                                    $B46[$i] = 0; // B46
                                                    $B47[$i] = 0; // B47
                                                    $B48[$i] = 0; // B48
                                                    $B49[$i] = 0; // B49
                                                } else {
                                                    $B43[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $Property->ResalePrice : 0); // B43
                                                    $B44[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $Property->ResalePrice*($Property->CostOfSale/100) : 0); // B44
                                                    $B45[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? min($B35[$i], $B43[$i]-$B44[$i]) : 0); // B45
                                                    $B46[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? min($B36[$i], $B43[$i]-$B44[$i]-$B45[$i]) : 0); // B46
                                                    $B47[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $B43[$i]-$B44[$i]-$B45[$i]-$B46[$i] : 0); // B47
                                                    $B48[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $LenderSplit*($Property->FinancingUsed=="Financing" ? 1 : 0)*$B47[$i]*($Property->SplitWithLender=="Yes" ? 1 : 0) : 0); // B48
                                                    $B49[$i] = $B47[$i]-$B48[$i]; // B49
                                                }
                                            }
                                            
                                            $B52 = $Property->PurchasePrice;
                                            $B53 = $B13;
                                            $B54 = array_sum($B16);
                                            $B55 = array_sum($B14);
                                            $B56 = array_sum($B15);
                                            $B57 = array_sum($B23);
                                            $B58 = array_sum($B19)+array_sum($B22);
                                            $B59 = array_sum($B29)+array_sum($B32);
                                            $B61 = ($B52+$B53+$B54+$B55+$B56+$B57+$B58+$B59);
                                            
                                            $B63 = $PurchaseClosingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0);
                                            $B64 = $HoldingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? array_sum($B16) : 0);
                                            $B65 = $FlipPoints = array_sum($B14);
                                            $B66 = $FlipOtherFinancingCosts = array_sum($B15);
                                            $B67 = $FlipLoanInterest = array_sum($B19)+array_sum($B22);
                                            $B68 = $FlipRehabInterest = array_sum($B29)+array_sum($B32);
                                            $B69 = ($B63+$B64+$B65+$B66+$B67+$B68);
                                            
                                            $G64 = $PurchaseFinanced;
                                            $G65 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0));
                                            $G66 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? array_sum($B16) : 0));
                                            $G67 = ($Property->PointsClosingUpfront=="Paid Backend" ? array_sum($B14)+array_sum($B15) : 0)+($Property->InterestPaymentsMade=="No" ? (array_sum($B22)+array_sum($B32)) : 0);
                                            $G68 = array_sum($B24);
                                            $G69 = ($G64+$G65+$G66+$G67+$G68);
                                            $G71 = $FundedByLenderFlip = $G64+$G65+$G66+$G68;
                                            
                                            $B71 = $Property->TotalLoanFlip;
                                            $B72 = $Property->CashRequiredFlip;
                                            $B73 = ($B71+$B72);
                                            
                                            $D73 = $B40-$B73;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B75[$i] = 0; // B75
                                                    $B76[$i] = 0; // B76
                                                    $B78[$i] = 0; // B78
                                                    $B79[$i] = 0; // B79
                                                } else {
                                                    $B75[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($Property->CashRequiredFlip==0 ? "Infinite" : $B47[$i]/$Property->CashRequiredFlip) : 0); // B75
                                                    $B76[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($B75[$i]=="Infinite" ? "Infinite" : $B75[$i]/($MonthsFlipRehab+$MonthsToSell)*12) : 0); // B76
                                                    $B78[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($Property->CashRequiredFlip==0 ? "Infinite" : $B49[$i]/$Property->CashRequiredFlip) : 0); // B78
                                                    $B79[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($B78[$i]=="Infinite" ? "Inifinite" : $B78[$i]/($MonthsFlipRehab+$MonthsToSell)*12) : 0); // B79
                                                }
                                            }
                                            
                                            // Cashflows to Lender
                                            $B84 = -$PurchaseFinanced;
                                            $B85 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$Property->ClosingCosts : 0));
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B86[$i] = 0;
                                                    $B87[$i] = $B14[$i];
                                                    $B88[$i] = $B15[$i];
                                                    $B89[$i] = -$B24[$i];
                                                    $B90[$i] = 0;
                                                    $B91[$i] = 0;
                                                    $B92[$i] = 0;
                                                    $B93[$i] = 0;
                                                    $B94[$i] = $B35[$i];
                                                    $B95[$i] = 0;
                                                    $B96[$i] = 0;
                                                    
                                                    $B98[$i] = $B84+$B85+$B86[$i]+$B89[$i];
                                                    $B99[$i] = $B90[$i]+$B92[$i]+$B95[$i]+$B96[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B87[$i]+$B88[$i] : 0);
                                                    $B100[$i] = ($B98[$i]+$B99[$i]);
                                                } else {
                                                    $B86[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$B16[$i] : 0));
                                                    $B87[$i] = $B14[$i];
                                                    $B88[$i] = $B15[$i];
                                                    $B89[$i] = -$B24[$i];
                                                    $B90[$i] = $B19[$i];
                                                    $B91[$i] = $B20[$i];
                                                    $B92[$i] = $B29[$i];
                                                    $B93[$i] = $B30[$i];
                                                    $B94[$i] = $B35[$i];
                                                    $B95[$i] = $B45[$i];
                                                    $B96[$i] = $B48[$i];
                                                    
                                                    $B98[$i] = $B86[$i]+$B89[$i];
                                                    $B99[$i] = $B90[$i]+$B92[$i]+$B95[$i]+$B96[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B87[$i]+$B88[$i] : 0);
                                                    $B100[$i] = ($B98[$i]+$B99[$i]);
                                                }
                                            }
                                            $B101 = IRR($B100, 0.01);
                                            for ( $i=0; $i<12; $i++ ) {
                                                $IRRReslt *= ($B101+1);
                                            }
                                            $B102 = ($IRRReslt-1)*100;
                                            $C101 = $B101*12;
                                            $B103 = $TotalLenderInterestIncomeFlip = ($Property->InterestPaymentsMade=="No" ? array_sum($B22)+array_sum($B32) : array_sum($B19)+array_sum($B29));
                                            $B104 = $FeesToLenderFlip = array_sum($B14)+array_sum($B15);
                                            $B105 = array_sum($B48);
                                            $B106 = ($B103+$B104+$B105);
                                            
                                            $E104 = $B106/$G71;
                                            $E105 = $E104/($MonthsFlipRehab+$MonthsToSell)*12;
                                            
                                            $C106 = array_sum($B100)-$B106;
                                            
                                            $FinancingCostFlip = ($FlipPoints+$FlipOtherFinancingCosts+$FlipLoanInterest+$FlipRehabInterest);
                                            ?>
                                            <p class="text-center"><strong>PROJECT FLIP CASH FLOW REPORT</strong></p><hr /><br />
                                            <div class="row">
                                                <div class="col-lg-6 text-center">
                                                    <p>
                                                        <?=$FCF[0]->PropertyName?><br>
                                                        <strong><?=$FCF[0]->PropertyStreetAddress?></strong><br>
                                                        <strong><?=$FCF[0]->PropertyCityTown?>, <?=$FCF[0]->PropertyStateProvince?>, <?=$FCF[0]->PropertyZipCode?>, <?=$FCF[0]->PropertyCountry?></strong>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6 text-center">
                                                    <p>
                                                        <strong><?=$FCF[0]->YourName?></strong><br>
                                                        <?=$FCF[0]->CompanyName?><br>
                                                        <?=$FCF[0]->PhoneNumber?>
                                                    </p>
                                                </div>
                                            </div><br />
                                            <div class="row">
                                                <div class="col-lg-12">
                                                	<div class="table-responsive">
                                                        <table class="table table-bordered text-center">
                                                            <tr>
                                                                <th width="10%" class="text-left">Month</th>
                                                                <th width="10%" class="text-center">0</th>
                                                                <?php 
                                                                for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <th width="10%" class="text-center"><?php echo $i; ?></th>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Purchase</td>
                                                                <td class="text-center"><?=number_format(-$FCF[0]->PurchasePrice)?></td>
                                                                <td colspan="<?=($MonthsFlipRehab+$MonthsToSell)?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Purchase Closing Costs</td>
                                                                <td class="text-center"><?=number_format(-$FCF[0]->ClosingCosts)?></td>
                                                                <td colspan="<?=($MonthsFlipRehab+$MonthsToSell)?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Orig/Disc Points and Loan Closing Cost</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($B14[$i]+$B15[$i]==0 ? "" : number_format(-($B14[$i]+$B15[$i])))?></td>
                                                                <?php } ?>
                                                            </tr> 
                                                            <tr>
                                                                <td class="text-left">Hosting Costs</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($B16[$i]==0 ? "" : number_format(-$B16[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Rehab Draws/Expenses</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($B23[$i]==0 ? "" : number_format(-($B23[$i])))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Intrest (Paid or Accured)</td>
                                                                <td class="text-left"></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($i>$MonthsFlipRehab+$MonthsToSell ? "" : number_format(-($B18[$i]+$B28[$i])))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Total cash Spend in Period</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($i>$MonthsFlipRehab+$MonthsToSell ? "" : number_format(-$B38[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Cumulative Cost Basis</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($i>$MonthsFlipRehab+$MonthsToSell ? "" : number_format(-$B33[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>       
                                                            <tr>
                                                                <td class="text-left">Sale Price</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($i==$MonthsFlipRehab+$MonthsToSell ? number_format($B43[$i]) : "")?></td>
                                                                <?php } ?>
                                                            </tr>                                                       
                                                            <tr>
                                                                <td class="text-left">Selling Costs</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($i==$MonthsFlipRehab+$MonthsToSell ? number_format(-$B44[$i]) : "")?></td>
                                                                <?php } ?>
                                                            </tr>       
                                                            <tr>
                                                                <td class="text-left">Profit Split to Lender</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($B48[$i]==0 ? "" : number_format($B48[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>  
                                                            <tr>
                                                                <td class="text-left">Flip Profit to Investor(Pre-Tax)</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($i==$MonthsFlipRehab+$MonthsToSell ? number_format($B49[$i]) : "")?></td>
                                                                <?php } ?>
                                                            </tr>   
                                                            <tr>
                                                                <td class="text-left">Total Cash Commited</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($i==$MonthsFlipRehab+$MonthsToSell ? number_format($FCF[0]->CashRequiredFlip) : "")?></td>
                                                                <?php } ?>
                                                            </tr>   
                                                            <tr>
                                                                <td class="text-left">Return on Cash Investment</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td><?=($i==$MonthsFlipRehab+$MonthsToSell ? ($FCF[0]->FlipROIAnnualized=='Infinite' ? 'Infinite' : round($FCF[0]->FlipROIAnnualized).'%') : "")?></td>
                                                                <?php } ?>
                                                            </tr> 
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>                                    
                                        
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['RefiCashFlow']) ) {
                                        $RCF = $print_data['SingleProposal']['RefiCashFlow']; ?>
                                        
                                        <div class="RefiCashFlow"><br />
                                            <?php 
                                            $Property = $RCF['Property'];
                                            $RefiBudget = $RCF['RefiBudget'];
                                            
                                            $MonthsRefiRehab = $Property->MonthsRefiRehab ? $Property->MonthsRefiRehab : 0;
                                            $MonthsToRefi = $Property->MonthsToRefi ? $Property->MonthsToRefi : 0;
                                            $RehabDiscountPoints = $Property->RehabDiscountPoints ? ($Property->RehabDiscountPoints/100) : 0;
                                            $ActualFinancedRent = $Property->ActualFinancedRent ? $Property->ActualFinancedRent : 0;
                                            $OtherPoints = $Property->OtherPoints ? ($Property->OtherPoints/100) : 0;
                                            $RefiHoldingCosts = $Property->RefiHoldingCosts ? $Property->RefiHoldingCosts : 0;
                                            $RehabInterestRate = $Property->RehabInterestRate ? ($Property->RehabInterestRate/100) : 0;
                                            $LumpSumRefiBudget = $Property->LumpSumRefiBudget ? $Property->LumpSumRefiBudget : 0;
                                            $DetailedRefiBudget = $Property->DetailedRefiBudget ? $Property->DetailedRefiBudget : 0; 
                                            $LenderSplit = $Property->LenderSplit ? ($Property->LenderSplit/100) : 0;
                                            
                                            $Row118Total = 0; $Row126Total = 0; $IRRReslt = 1;
                                            $MonthTotal = array();
                                            $MonthMatchArray = array();
                                            $RefiBudgetColumn = array(); $MonthPaidColumn = array();
                                            foreach ( $RefiBudget as $key => $refi ) {
                                                $RefiBudgetColumn[$key] = $refi->Budget;
                                                $MonthPaidColumn[$key] = $refi->MonthPaid;
                                            }
                                            
                                            $B2 = ($Property->FinancingUsed=="Financing" ? "Yes" : "No");
                                            $B3 = $Property->CapSelection;
                                            $B4 = $Property->FinancingCap ? ($Property->FinancingCap/100) : 0;
											
											$F2 = $Property->ARVFlip ? $Property->ARVFlip : 0;
											$F3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualFlipBudget ? $Property->ActualFlipBudget : 0);
											$F4 = ($Property->CapSelection=="ARV" ? $B4 * $F2 : $B4 * $F3);
											
											$B10 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
											$B11 = $PurchaseFinanced = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B10 : min($B10, $F4)) : 0);
                                            
                                            $I2 = $Property->ARVRent ? $Property->ARVRent : 0;
                                            $I3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualRefiBudget ? $Property->ActualRefiBudget : 0);
                                            $I4 = ($Property->CapSelection=="ARV" ? $B4 * $I2 : $B4 * $I3);
                                            
                                            // REFI
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B109[$i] = 1; // B109
                                                    $B110[$i] = ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B110
                                                    $B111[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B111
                                                } else {
                                                    $B109[$i] = ($i>$MonthsRefiRehab ? 0 : 1); // B109
                                                    $B110[$i] = ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B110
                                                    $B111[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B111
                                                }
                                            }
                                            
                                            $B112 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
                                            $B113 = $PurchaseFinancedRefi = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B112 : min($B112, $I4)) : 0);
                                            $B114 = $B112-$B113;
                                            $B115 = $Property->ClosingCosts ? $Property->ClosingCosts : 0;
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B116[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($RehabDiscountPoints)*$ActualFinancedRent : 0); // B116
                                                    $B117[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($OtherPoints)*$ActualFinancedRent : 0); // B117
                                                    $B118[$i] = 0; // B118
                                                    $Row118Total = $Row118Total + $B118[$i];
                                                    $B119[$i] = $PurchaseFinanced + ($B115 + $B118[$i]) * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0); // B119
                                                    $B120[$i] = 0; // B120
                                                    $B121[$i] = 0; // B121
                                                    $B122[$i] = 0; // B122
                                                    $B123[$i] = 0; // B123
                                                    $B124[$i] = 0; // B124
                                                    $B125[$i] = ($Property->RefiRehabDrawSelection=="Fund Rehab at Closing" ? ($Property->RefiRehabBudgetMethod=="Quick Lump Sum" ? $LumpSumRefiBudget : $DetailedRefiBudget) : 0); // B125
                                                    $B126[$i] = ($B3=="Cost" ? $B125[$i] * $B4 : max(0, min($I4, $B125[$i]))) * $B111[$i]; // B126
                                                    $Row126Total = $Row126Total + $B126[$i];
                                                    $B127[$i] = $B125[$i] - $B126[$i]; // B127
                                                    $B128[$i] = ($B125[$i]==0 ? 0 : $B125[$i]) * $B110[$i]; // B128
                                                    $B129[$i] = $B126[$i]; // B129
                                                    $B130[$i] = 0; // B130
                                                    $B131[$i] = 0; // B131
                                                    $B132[$i] = 0; // B132
                                                    $B133[$i] = 0; // B133
                                                    $B134[$i] = 0; // B134
                                                    $B135[$i] = $B112+$B115+$B116[$i]+$B117[$i]+$B125[$i]; // B135
                                                    $B136[$i] = $PurchaseFinancedRefi+($Row126Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])); // B136
                                                    $B137[$i] = $PurchaseFinancedRefi+$Property->ClosingCosts*($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))+($Row126Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])); // B137
                                                    $B138[$i] = $B114+($B115+$B118[$i])*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B120[$i]+$B127[$i]+$B131[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B116[$i]+$B117[$i]) : 0); // B138
                                                    $B139[$i] = $B135[$i]-($B137[$i]+$B138[$i]); // B139
                                                    $B140[$i] = $B114+$B115*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 :1))+$B116[$i]+$B117[$i]+$B118[$i]*($Property->FinanceClosingHolding=="Yes" ? 0 : 1)+$B121[$i]+$B127[$i]+$B131[$i]; // B140
                                                } else {
                                                    $B116[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($RehabDiscountPoints)*$ActualFinancedRent : 0); // B116
                                                    $B117[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($OtherPoints)*$ActualFinancedRent : 0); // B117				
                                                    $B118[$i] = $RefiHoldingCosts/($MonthsRefiRehab+$MonthsToRefi)*$B110[$i]; // B118
                                                    $Row118Total = $Row118Total + $B118[$i];
                                                    $B119[$i] = ($B119[$i-1] + $B118[$i] * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))*$B110[$i]; // B119
                                                    $B120[$i] = $RehabInterestRate*$B119[$i-1] / 12 * ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B120
                                                    $B121[$i] = $Property->InterestPaymentsMade=="Yes" ? $B120[$i] : 0; // B121
                                                    $B122[$i] = $B120[$i] - $B121[$i]; // B122
                                                    if ( $i == 1 ) {
                                                        $B123[$i] = $B122[$i] + $B123[$i-1]; // B123
                                                    } else {
                                                        $B123[$i] = ($B122[$i]+$B123[$i-1]+$B123[$i-1]*($RehabInterestRate/12))*$B110[$i]; // B123
                                                    }
                                                    $B124[$i] = $B123[$i] * ($i == ($MonthsRefiRehab + $MonthsToRefi) ? 1 : 0); // B124
                                                    
                                                    $ColU = ($i<=$MonthsRefiRehab ? 1 : 0);
                                                    $MonthMatch = 0;
                                                    for ( $j = 0; $j < count($MonthPaidColumn); $j++ ) {
                                                        if ( $MonthPaidColumn[$j] == $i ) {
                                                            $MonthMatch += $RefiBudgetColumn[$j];
                                                        }
                                                    }
                                                    $MonthMatchArray[$i] = $MonthMatch;
                                                    $B125[$i] = $Property->RefiRehabDrawSelection=="Fund Rehab at Closing" ? 0 : ($Property->RefiRehabBudgetMethod=="Detailed Input" ? $MonthMatchArray[$i] * $ColU : $LumpSumFlipBudget / $MonthsRefiRehab * $ColU); // B125
                                                    $B126[$i] = ($B3=="Cost" ? $B125[$i] * $B4 : max(0, min($I4-$B137[$i-1], $B125[$i]))) * $B111[$i]; // B126
                                                    $B127[$i] = $B125[$i] - $B126[$i]; // B127
                                                    $B128[$i] = ($B125[$i]==0 ? $B128[$i-1] : $B125[$i] + $B128[$i-1]) * $B110[$i]; // B128
                                                    $B129[$i] = ($B126[$i]==0 ? $B129[$i-1] : $B126[$i] + $B129[$i-1]) * $B110[$i]; // B129
                                                    $B130[$i] = $B129[$i-1] * $RehabInterestRate/12 * $B110[$i] * $B111[$i]; // B130
                                                    $B131[$i] = $Property->InterestPaymentsMade=="Yes" ? $B130[$i] : 0; // B131
                                                    $B132[$i] = $B130[$i] - $B131[$i]; // B132
                                                    if ( $i == 1 ) {
                                                        $B133[$i] = $B132[$i] + $B133[$i-1]; // B133
                                                    } else {
                                                        $B133[$i] = ($B132[$i] + $B133[$i-1] + $B133[$i-1] * ($RehabInterestRate/12)) * $B110[$i]; // B133
                                                    }
                                                    $B134[$i] = $B133[$i] * ($i==($MonthsRefiRehab+$MonthsToRefi) ? 1 : 0); // B134
                                                    $B135[$i] = ($B135[$i-1]+($B116[$i]+$B117[$i]+$B118[$i])+$B121[$i]+$B122[$i]+$B123[$i-1]*$RehabInterestRate/12+$B125[$i]+$B131[$i]+$B132[$i]+$B133[$i-1]*$RehabInterestRate/12)*$B110[$i]; // B135
                                                    $B136[$i] = ($PurchaseFinancedRefi+$B129[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])))*$B110[$i]; // B136
                                                    $B137[$i] = ($PurchaseFinancedRefi+($Property->ClosingCosts+($Row118Total))*($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))+$B129[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i]))+$B123[$i]+$B133[$i])*$B110[$i]; // B137
                                                    $B138[$i] = ($B118[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B121[$i]+$B127[$i]+$B131[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B116[$i]+$B117[$i]) : 0)+$B138[$i-1])*$B110[$i]; // B138
                                                    $B139[$i] = $B135[$i]-($B137[$i]+$B138[$i]); // B139
                                                    $B140[$i] = $B118[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B121[$i]+$B127[$i]+$B131[$i]; // B140
                                                }
                                            }
                                            
                                            $B141 = array_sum($B140)-max($B138);
                                            $B142 = max($B135);
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B144[$i] = 0; // B144
                                                    $B145[$i] = 0; // B145
                                                    $B145[$i] = 0; // B146
                                                    $B147[$i] = 0; // B147
                                                    $B149[$i] = 0; // B149
                                                    $B150[$i] = 0; // B150
                                                    $B151[$i] = 0; // B151
                                                    $B152[$i] = 0; // B152
                                                    $B153[$i] = 0; // B153
                                                    $B154[$i] = 0; // B154
                                                } else {
                                                    $B144[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->RefiAmount : 0); // B144
                                                    $B145[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? -($Property->RefiDiscountPoints/100)*$Property->RefiAmount : 0); // B145
                                                    $B146[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? -$B137[$i] : 0); // B146
                                                    $B147[$i] = ($B144[$i]+$B145[$i]+$B146[$i]); // B147
                                                    $B149[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? max(0, $B147[$i]-$Property->CashRequiredRent) : 0); // B149
                                                    $B150[$i] = 	($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->LenderSplit*($Property->FinancingUsed=="Financing" ? 1 : 0)*$B149[$i]*($Property->SplitWithLender=="Yes" ? 1 : 0) : 0); // B150
                                                    $B151[$i] = $B149[$i]-$B150[$i]; // B151
                                                    $B152[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "infinite" : $B149[$i]/$Property->CashRequiredRent/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B152
                                                    $B153[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? max(0, $B138[$i]-$B147[$i]) : 0); // B153
                                                    $B154[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->ARVRent-$Property->RefiAmount : 0); // B154
                                                }
                                            }
                                            
                                            $B158 = $Property->PurchasePrice;
                                            $B159 = $B115;
                                            $B160 = array_sum($B118);
                                            $B161 = array_sum($B116);
                                            $B162 = array_sum($B117);
                                            $B163 = array_sum($B125);
                                            $B164 = array_sum($B121)+array_sum($B124);
                                            $B165 = array_sum($B131)+array_sum($B134);
                                            $B167 = ($B158+$B159+$B160+$B161+$B162+$B163+$B164+$B165);
                                            
                                            $B169 = $RefiClosingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? $B115 : 0);
                                            $B170 = $RefiHoldingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? array_sum($B118) : 0);
                                            $B171 = $RentPoints = array_sum($B116);
                                            $B172 = $RentOtherFinancingCosts = array_sum($B117);
                                            $B173 = $RentLoanInterest = array_sum($B121)+array_sum($B124);
                                            $B174 = $RentRehabInterest = array_sum($B131)+array_sum($B134);
                                            $B175 = ($B171+$B172+$B173+$B174);
                                            
                                            $G170 = $PurchaseFinanced;
                                            $G171 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0));
                                            $G172 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? array_sum($B118) : 0));
                                            $G173 = ($Property->PointsClosingUpfront=="Paid Backend" ? array_sum($B116)+array_sum($B117) : 0)+($Property->InterestPaymentsMade=="No" ? (array_sum($B124)+array_sum($B134)) : 0);
                                            $G174 = array_sum($B126);
                                            $G175 = ($G170+$G171+$G172+$G173+$G174);
                                            $G177 = $FundedByLenderREfi = $G170+$G171+$G172+$G174;
                                            
                                            $B177 = $Property->TotalLoanRent;
                                            $B178 = $Property->CashRequiredRent;
                                            $B179 = ($B177+$B178);
                                            
                                            $D179 = $B142-$B179;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B181[$i] = 0; // B181
                                                    $B182[$i] = 0; // B182
                                                    $B184[$i] = 0; // B184
                                                    $B185[$i] = 0; // B185
                                                } else {
                                                    $B181[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "Infinite" : $B149[$i]/$Property->CashRequiredRent) : 0); // B181
                                                    $B182[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($B181[$i]=="Infinite" ? "Infinite" : $B181[$i]/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B182
                                                    $B184[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "Infinite" : $B151[$i]/$Property->CashRequiredRent) : 0); // B184
                                                    $B185[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($B184[$i]=="Infinite" ? "Infinite" : $B184[$i]/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B185
                                                }
                                            }
                                            
                                            // Cashflows to Lender
                                            $B190 = -$PurchaseFinancedRefi;
                                            $B191 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$Property->ClosingCosts : 0));
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B192[$i] = 0;
                                                    $B193[$i] = $B116[$i];
                                                    $B194[$i] = $B117[$i];
                                                    $B195[$i] = -$B126[$i];
                                                    $B196[$i] = 0;
                                                    $B197[$i] = 0;
                                                    $B198[$i] = 0;
                                                    $B199[$i] = 0;
                                                    $B200[$i] = $B137[$i];
                                                    $B201[$i] = 0;
                                                    $B202[$i] = 0;
                                                    
                                                    $B204[$i] = $B190+$B191+$B192[$i]+$B195[$i];
                                                    $B205[$i] = $B196[$i]+$B198[$i]+$B201[$i]+$B202[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B193[$i]+$B194[$i] : 0);
                                                    $B206[$i] = ($B204[$i]+$B205[$i]);
                                                } else {
                                                    $B192[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$B118[$i] : 0));
                                                    $B193[$i] = $B116[$i];
                                                    $B194[$i] = $B117[$i];
                                                    $B195[$i] = -$B126[$i];
                                                    $B196[$i] = $B121[$i];
                                                    $B197[$i] = $B122[$i];
                                                    $B198[$i] = $B131[$i];
                                                    $B199[$i] = $B132[$i];
                                                    $B200[$i] = $B137[$i];
                                                    $B201[$i] = -$B146[$i];
                                                    $B202[$i] = $B150[$i];
                                                    
                                                    $B204[$i] = $B192[$i]+$B195[$i];
                                                    $B205[$i] = $B196[$i]+$B198[$i]+$B201[$i]+$B202[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B193[$i]+$B194[$i] : 0);
                                                    $B206[$i] = ($B204[$i]+$B205[$i]);
                                                }
                                            }
                                            $B207 = IRR($B206, 0.01);
                                            for ( $i=0; $i<12; $i++ ) {
                                                $IRRReslt *= ($B207+1);
                                            }
                                            $B208 = ($IRRReslt-1)*100;
                                            $C207 = $B207*12;
                                            $B209 = $TotalLenderInterestIncomeRefi = ($Property->InterestPaymentsMade=="No" ? array_sum($B124)+array_sum($B134) : array_sum($B121)+array_sum($B131));
                                            $B210 = $FeesToLenderRefi = array_sum($B116)+array_sum($B117);
                                            $B211 = array_sum($B150);
                                            $B212 = ($B209+$B210+$B211);
                                            
                                            $E210 = $B212/$G177;
                                            $E211 = $E210/($MonthsRefiRehab+$MonthsToRefi)*12;
                                            
                                            $C212 = array_sum($B206)-$B212;
                                            
                                            $FinancingCostRent = $RentPoints+$RentOtherFinancingCosts+$RentLoanInterest+$RentRehabInterest;
                                            ?>
                                            <p class="text-center"><strong>PROJECT REFI CASH FLOW REPORT</strong></p><hr /><br />
                                            <div class="row">
                                                <div class="col-lg-6 text-center">
                                                    <p>
                                                        <?=$RCF[0]->PropertyName?><br>
                                                        <strong><?=$RCF[0]->PropertyStreetAddress?></strong><br>
                                                        <strong><?=$RCF[0]->PropertyCityTown?>, <?=$RCF[0]->PropertyStateProvince?>, <?=$RCF[0]->PropertyZipCode?>, <?=$RCF[0]->PropertyCountry?></strong>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6 text-center">
                                                    <p>
                                                        <strong><?=$RCF[0]->YourName?></strong><br>
                                                        <?=$RCF[0]->CompanyName?><br>
                                                        <?=$RCF[0]->PhoneNumber?>
                                                    </p>
                                                </div>
                                            </div><br />
                                            <div class="row">
                                                <div class="col-lg-12">
                                                	<div class="table-responsive">
                                                        <table class="table table-bordered text-center">
                                                            <tr>
                                                                <th width="10%" class="text-left">Month</th>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <th width="10%" class="text-center"><?php echo $i; ?></th>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Purchase</td>
                                                                <td class="text-center"><?=number_format(-$RCF[0]->PurchasePrice)?></td>
                                                                <td colspan="<?=($MonthsRefiRehab+$MonthsToRefi)?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Closing Cost</td>
                                                                <td class="text-center"><?=number_format(-$RCF[0]->ClosingCosts)?></td>
                                                                <td colspan="<?=($MonthsRefiRehab+$MonthsToRefi)?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Orig/Disc Points and Loan Closing Cost</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B116[$i]+$B117[$i]==0 ? "" : number_format(-($B116[$i]+$B117[$i])))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Hosting Costs</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B118[$i]==0 ? "" : number_format(-$B118[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>               
                                                            <tr>
                                                                <td class="text-left">Rehab Draws/Expenses</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B125[$i]==0 ? "" : number_format(-$B125[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>                                               
                                                            <tr>
                                                                <td class="text-left">Intrest (Paid or Accured)</td>
                                                                <td class="text-left"></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($i>$MonthsRefiRehab+$MonthsToRefi ? "" : number_format(-($B120[$i]+$B130[$i])))?></td>
                                                                <?php } ?>
                                                            </tr>                                               
                                                            <tr>
                                                                <td class="text-left">Total Cash Spend in Period</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) {  ?>
                                                                    <td><?=($i>$MonthsRefiRehab+$MonthsToRefi ? "" : number_format(-$B140[$i]))?></td>
                                                                <?php  } ?>
                                                            </tr>       
                                                            <tr>
                                                                <td class="text-left">Cumulative Cost Basis</td>
                                                                <?php  for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) {  ?>
                                                                    <td><?=($i>$MonthsRefiRehab+$MonthsToRefi ? "" : number_format(-$B135[$i]))?></td>
                                                                <?php  } ?>
                                                            </tr>       
                                                            <tr>
                                                                <td class="text-left">Refinance: New Loan Amount</td>
                                                                <td></td>
                                                                <?php  for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) {  ?>
                                                                    <td><?=($MonthsRefiRehab+$MonthsToRefi==$i ? number_format($B144[$i]) : "")?></td>
                                                                <?php  } ?>
                                                            </tr>                                                       
                                                            <tr>
                                                                <td class="text-left">Closing Costs on New Loan</td>
                                                                <td></td>
                                                                <?php  for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) {  ?>
                                                                    <td><?=($MonthsRefiRehab+$MonthsToRefi==$i ? number_format($B145[$i]) : "")?></td>
                                                                <?php  } ?>
                                                            </tr>       
                                                            <tr>
                                                                <td class="text-left">PayOff Existing Loan</td>
                                                                <td></td>
                                                                <?php  for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) {  ?>
                                                                    <td><?=($MonthsRefiRehab+$MonthsToRefi==$i ? number_format($B146[$i]) : "")?></td>
                                                                <?php  } ?>
                                                            </tr>   
                                                            <tr>
                                                                <td class="text-left">Cash Out at Refi</td>
                                                                <td></td>
                                                                <?php  for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) {  ?>
                                                                    <td><?=($MonthsRefiRehab+$MonthsToRefi==$i ? number_format($B147[$i]) : "")?></td>
                                                                <?php  } ?>
                                                            </tr>
                                                            <?php if ( $Property->SplitWithLender=="Yes" && $Property->FinancingUsed=="Financing" ) { ?>
                                                            <tr>
                                                                <td class="text-left"><?=$REF=($Property->SplitWithLender=="Yes" ? ($Property->FinancingUsed=="Financing" ? "Split to Lender from Refi Profits" : "") : "")?></td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($MonthsRefiRehab+$MonthsToRefi==$i ? ($REF=="" ? "" : number_format(max(0, $B150[$i]))) : "")?></td>
                                                                <?php  } ?>
                                                            </tr>
                                                            <?php } ?>
                                                            <tr>
                                                                <td class="text-left">Profit to Investor at Refi</td>
                                                                <td></td>
                                                                <?php  for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) {  ?>
                                                                    <td><?=($MonthsRefiRehab+$MonthsToRefi==$i ? number_format(max(0, $B151[$i])) : "")?></td>
                                                                <?php  } ?>
                                                            </tr> 
                                                            <tr>
                                                                <td class="text-left">Return on Cash Investment</td>
                                                                <td></td>
                                                                <?php  for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) {  ?>
                                                                    <td><?=($MonthsRefiRehab+$MonthsToRefi==$i ? ($B185[$i]=='Infinite' ? 'Infinite' : round($B185[$i]).'%') : "")?></td>
                                                                <?php  } ?>
                                                            </tr> 
                                                            <tr>
                                                                <td class="text-left">Cash Tied up in Deal</td>
                                                                <?php  for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($MonthsRefiRehab+$MonthsToRefi==$i ? number_format($B153[$i]) : "")?></td>
                                                                <?php  } ?>
                                                            </tr> 
                                                            <tr>
                                                                <td class="text-left">Equity Left in Deal</td>
                                                                <td></td>
                                                                <?php  for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($MonthsRefiRehab+$MonthsToRefi==$i ? number_format($B154[$i]) : "")?></td>
                                                                <?php  } ?>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['FlipFundingRequest']) ) {
                                        $FFR = $print_data['SingleProposal']['FlipFundingRequest']; ?>
                                        
                                        <div class="FlipFundingRequest"><br />
                                            <?php 
                                            $Property = $FFR->Property;
                                            $FlipBudget = $FFR->FlipBudget;
                                            
                                            $MonthsFlipRehab = $Property->MonthsFlipRehab ? $Property->MonthsFlipRehab : 0;
                                            $MonthsToSell = $Property->MonthsToSell ? $Property->MonthsToSell : 0;
                                            $RehabDiscountPoints = $Property->RehabDiscountPoints ? ($Property->RehabDiscountPoints/100) : 0;
                                            $ActualFinancedFlip = $Property->ActualFinancedFlip ? $Property->ActualFinancedFlip : 0;
                                            $OtherPoints = $Property->OtherPoints ? ($Property->OtherPoints/100) : 0;
                                            $FlipHoldingCosts = $Property->FlipHoldingCosts ? $Property->FlipHoldingCosts : 0;
                                            $RehabInterestRate = $Property->RehabInterestRate ? ($Property->RehabInterestRate/100) : 0;
                                            $LumpSumFlipBudget = $Property->LumpSumFlipBudget ? $Property->LumpSumFlipBudget : 0;
                                            $DetailedFlipBudget = $Property->DetailedFlipBudget ? $Property->DetailedFlipBudget : 0;
                                            $LenderSplit = $Property->LenderSplit ? ($Property->LenderSplit/100) : 0;
                                            
                                            $Row16Total = 0; $Row24Total = 0; $IRRReslt = 1;
                                            $MonthTotal = array();
                                            $MonthMatchArray = array();
                                            $FlipBudgetColumn = array(); $MonthPaidColumn = array();
                                            foreach ( $FlipBudget as $key => $flip ) {
                                                $FlipBudgetColumn[$key] = $flip->Budget;
                                                $MonthPaidColumn[$key] = $flip->MonthPaid;
                                            }
                                            
                                            $B2 = ($Property->FinancingUsed=="Financing" ? "Yes" : "No");
                                            $B3 = $Property->CapSelection;
                                            $B4 = $Property->FinancingCap ? ($Property->FinancingCap/100) : 0;
                                            
                                            $F2 = $Property->ARVFlip ? $Property->ARVFlip : 0;
                                            $F3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualFlipBudget ? $Property->ActualFlipBudget : 0);
                                            $F4 = ($Property->CapSelection=="ARV" ? $B4 * $F2 : $B4 * $F3);
                                            
                                            // FLIP
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B7[$i] = 1; // B7
                                                    $B8[$i] = ($i > ($MonthsFlipRehab + $MonthsToSell) ? 0 : 1); // B8
                                                    $B9[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B9
                                                } else {
                                                    $B7[$i] = ( $i > $MonthsFlipRehab ? 0 : 1); // B7
                                                    $B8[$i] = ($i > ($MonthsFlipRehab + $MonthsToSell) ? 0 : 1); // B8
                                                    $B9[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B9
                                                }
                                            }
                                            
                                            $B10 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
                                            $B11 = $PurchaseFinanced = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B10 : min($B10, $F4)) : 0);
                                            $B12 = $B10-$B11;
                                            $B13 = $Property->ClosingCosts ? $Property->ClosingCosts : 0;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B14[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab + $MonthsToSell) ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0)); // B14
                                                    $B15[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab+$MonthsToSell) ? ($OtherPoints)*$Property->ActualFinancedFlip : 0)); // B15
                                                    $B16[$i] = 0; // B16
                                                    $Row16Total = $Row16Total + $B16[$i];
                                                    $B17[$i] = $PurchaseFinanced + ($B13 + $B16[$i]) * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0); // B17
                                                    $B18[$i] = 0; // B18
                                                    $B19[$i] = 0; // B19
                                                    $B20[$i] = 0; // B20
                                                    $B21[$i] = 0; // B21
                                                    $B22[$i] = 0; // B22
                                                    $MonthTotal[$i] = ($Property->FlipRehabDrawSelection=="Fund Rehab at Closing" ? ($Property->FlipRehabBudgetMethod=="Quick Lump Sum" ? $LumpSumFlipBudget : $DetailedFlipBudget) : 0);
                                                    $B23[$i] = $MonthTotal[$i]; // B23
                                                    $B24[$i] = ($B3=="Cost" ? $MonthTotal[$i] * $B4 : max(0, min($F4, $MonthTotal[$i]))) * $B9[$i]; // B24
                                                    $Row24Total = $Row24Total + $B24[$i];
                                                    $B25[$i] = ($B23[$i] - $B24[$i]); // B25
                                                    $B26[$i] = ($B23[$i]==0 ? 0 : $B23[$i]) * $B8[$i]; // B26
                                                    $B27[$i] = $B24[$i]; // B27
                                                    $B28[$i] = 0; // B28
                                                    $B29[$i] = 0; // B29
                                                    $B30[$i] = 0; // B30
                                                    $B31[$i] = 0; // B31
                                                    $B32[$i] = 0; // B32
                                                    $B33[$i] = $B10+$B13+$B14[$i]+$B15[$i]+$B23[$i]; // B33
                                                    $B34[$i] = ($PurchaseFinanced + $B27[$i]) * $B8[$i]; // B34
                                                    $B35[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : $PurchaseFinanced+$Property->ClosingCosts*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+($Row24Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B14[$i]+$B15[$i]))); // B35
                                                    $B36[$i] = $B12+$Property->ClosingCosts*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B16[$i]+$B18[$i]+$B25[$i]+$B29[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B14[$i]+$B15[$i]) : 0); // B36
                                                    $B37[$i] = $B33[$i]-($B35[$i]+$B36[$i]); // B37
                                                    $B38[$i] = $B12+$B13*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B14[$i]+$B15[$i]+$B16[$i]+$B19[$i]+$B25[$i]+$B29[$i]; // B38
                                                } else {
                                                    $B14[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab + $MonthsToSell) ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0)); // B14
                                                    $B15[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab+$MonthsToSell) ? ($OtherPoints)*$ActualFinancedFlip : 0)); // B15
                                                    $B16[$i] = $FlipHoldingCosts/($MonthsFlipRehab+$MonthsToSell)*$B8[$i]; // B16
                                                    $Row16Total = $Row16Total + $B16[$i];
                                                    $B17[$i] = ($B17[$i-1] + $B16[$i] * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))*$B8[$i]; // B17
                                                    $B18[$i] = $RehabInterestRate * $B17[$i-1] / 12 * ($i>($MonthsFlipRehab+$MonthsToSell) ? 0 : 1); // B18
                                                    $B19[$i] = $Property->InterestPaymentsMade=="Yes" ? $B18[$i] : 0; // B19
                                                    $B20[$i] = $B18[$i] - $B19[$i]; // B20
                                                    if ( $i == 1 ) {
                                                        $B21[$i] = $B20[$i] + $B21[$i-1]; // B21
                                                    } else {
                                                        $B21[$i] = ($B20[$i]+$B21[$i-1]+$B21[$i-1]*($RehabInterestRate/12))*$B8[$i]; // B21
                                                    }
                                                    $B22[$i] = $B21[$i] * ($i == ($MonthsFlipRehab + $MonthsToSell) ? 1 : 0); // B22
                                                    $ColU = ($i<=$MonthsFlipRehab ? 1 : 0);
                                                    $MonthMatch = 0;
                                                    for ( $j = 0; $j < count($MonthPaidColumn); $j++ ) {
                                                        if ( $MonthPaidColumn[$j] == $i ) {
                                                            $MonthMatch += $FlipBudgetColumn[$j];
                                                        }
                                                    }
                                                    $MonthMatchArray[$i] = $MonthMatch;
                                                    $MonthTotal[$i] = $Property->FlipRehabDrawSelection=="Fund Rehab at Closing" ? 0 : ($Property->FlipRehabBudgetMethod=="Detailed Input" ? $MonthMatchArray[$i] * $ColU : $LumpSumFlipBudget / $MonthsFlipRehab * $ColU);
                                                    $B23[$i] = $MonthTotal[$i]; // B23
                                                    $B24[$i] = ($B3=="Cost" ? $MonthTotal[$i] * $B4 : max(0, min($F4-($B34[$i-1]), $B23[$i]))) * $B9[$i]; // B24
                                                    $Row24Total = $Row24Total + $B24[$i];
                                                    $B25[$i] = $B23[$i] - $B24[$i]; // B25
                                                    $B26[$i] = ($B23[$i]=="" ? $B26[$i-1] : $B23[$i] + $B26[$i-1]) * $B8[$i]; // B26
                                                    $B27[$i] = ($B24[$i]==0 ? $B27[$i-1] : $B24[$i] + $B27[$i-1]) * $B8[$i]; // B27
                                                    $B28[$i] = $B27[$i-1] * $RehabInterestRate/12 * $B8[$i] * $B9[$i]; // B28
                                                    $B29[$i] = $Property->InterestPaymentsMade=="Yes" ? $B28[$i] : 0; // B29
                                                    $B30[$i] = $B28[$i] - $B29[$i]; // B30
                                                    if ( $i == 1 ) {
                                                        $B31[$i] = $B30[$i] + $B31[$i-1]; // B31
                                                    } else {
                                                        $B31[$i] = ($B30[$i] + $B31[$i-1] + $B31[$i-1] * ($RehabInterestRate/12)) * $B8[$i]; // B31
                                                    }
                                                    $B32[$i] = $B31[$i] * ($i==($MonthsFlipRehab+$MonthsToSell) ? 1 : 0); // B32
                                                    $B33[$i] = ($B33[$i-1]+($B14[$i]+$B15[$i]+$B16[$i])+$B19[$i]+$B20[$i]+$B21[$i-1]*$RehabInterestRate/12+$B23[$i]+$B29[$i]+$B30[$i]+$B31[$i-1]*$RehabInterestRate/12)*$B8[$i]; // B33
                                                    $B34[$i] = ($PurchaseFinanced + $B27[$i]) * $B8[$i]; // B34
                                                    $B35[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($PurchaseFinanced+$Property->ClosingCosts*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+($Row16Total)*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+$B27[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B14[$i]+$B15[$i]))+$B21[$i]+$B31[$i])*$B8[$i]); // B35
                                                    $B36[$i] = ($B12+$B16[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B19[$i]+$B25[$i]+$B29[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B14[$i]+$B15[$i]) : 0)+$B36[$i-1])*$B8[$i]; // B36
                                                    $B37[$i] = $B33[$i]-($B35[$i]+$B36[$i]); // B37
                                                    $B38[$i] = $B16[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B19[$i]+$B25[$i]+$B29[$i]; // B38
                                                }
                                            }
                                            
                                            $B39 = array_sum($B38)-max($B36);
                                            $B40 = max($B33);
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B43[$i] = 0; // B43
                                                    $B44[$i] = 0; // B44
                                                    $B45[$i] = 0; // B45
                                                    $B46[$i] = 0; // B46
                                                    $B47[$i] = 0; // B47
                                                    $B48[$i] = 0; // B48
                                                    $B49[$i] = 0; // B49
                                                } else {
                                                    $B43[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $Property->ResalePrice : 0); // B43
                                                    $B44[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $Property->ResalePrice*($Property->CostOfSale/100) : 0); // B44
                                                    $B45[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? min($B35[$i], $B43[$i]-$B44[$i]) : 0); // B45
                                                    $B46[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? min($B36[$i], $B43[$i]-$B44[$i]-$B45[$i]) : 0); // B46
                                                    $B47[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $B43[$i]-$B44[$i]-$B45[$i]-$B46[$i] : 0); // B47
                                                    $B48[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $LenderSplit*($Property->FinancingUsed=="Financing" ? 1 : 0)*$B47[$i]*($Property->SplitWithLender=="Yes" ? 1 : 0) : 0); // B48
                                                    $B49[$i] = $B47[$i]-$B48[$i]; // B49
                                                }
                                            }
                                            
                                            $B52 = $Property->PurchasePrice;
                                            $B53 = $B13;
                                            $B54 = array_sum($B16);
                                            $B55 = array_sum($B14);
                                            $B56 = array_sum($B15);
                                            $B57 = array_sum($B23);
                                            $B58 = array_sum($B19)+array_sum($B22);
                                            $B59 = array_sum($B29)+array_sum($B32);
                                            $B61 = ($B52+$B53+$B54+$B55+$B56+$B57+$B58+$B59);
                                            
                                            $B63 = $PurchaseClosingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0);
                                            $B64 = $HoldingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? array_sum($B16) : 0);
                                            $B65 = $FlipPoints = array_sum($B14);
                                            $B66 = $FlipOtherFinancingCosts = array_sum($B15);
                                            $B67 = $FlipLoanInterest = array_sum($B19)+array_sum($B22);
                                            $B68 = $FlipRehabInterest = array_sum($B29)+array_sum($B32);
                                            $B69 = ($B63+$B64+$B65+$B66+$B67+$B68);
                                            
                                            $G64 = $PurchaseFinanced;
                                            $G65 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0));
                                            $G66 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? array_sum($B16) : 0));
                                            $G67 = ($Property->PointsClosingUpfront=="Paid Backend" ? array_sum($B14)+array_sum($B15) : 0)+($Property->InterestPaymentsMade=="No" ? (array_sum($B22)+array_sum($B32)) : 0);
                                            $G68 = array_sum($B24);
                                            $G69 = ($G64+$G65+$G66+$G67+$G68);
                                            $G71 = $FundedByLenderFlip = $G64+$G65+$G66+$G68;
                                            
                                            $B71 = $Property->TotalLoanFlip;
                                            $B72 = $Property->CashRequiredFlip;
                                            $B73 = ($B71+$B72);
                                            
                                            $D73 = $B40-$B73;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B75[$i] = 0; // B75
                                                    $B76[$i] = 0; // B76
                                                    $B78[$i] = 0; // B78
                                                    $B79[$i] = 0; // B79
                                                } else {
                                                    $B75[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($Property->CashRequiredFlip==0 ? "Infinite" : $B47[$i]/$Property->CashRequiredFlip) : 0); // B75
                                                    $B76[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($B75[$i]=="Infinite" ? "Infinite" : $B75[$i]/($MonthsFlipRehab+$MonthsToSell)*12) : 0); // B76
                                                    $B78[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($Property->CashRequiredFlip==0 ? "Infinite" : $B49[$i]/$Property->CashRequiredFlip) : 0); // B78
                                                    $B79[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($B78[$i]=="Infinite" ? "Inifinite" : $B78[$i]/($MonthsFlipRehab+$MonthsToSell)*12) : 0); // B79
                                                }
                                            }
                                            
                                            // Cashflows to Lender
                                            $B84 = -$PurchaseFinanced;
                                            $B85 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$Property->ClosingCosts : 0));
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B86[$i] = 0;
                                                    $B87[$i] = $B14[$i];
                                                    $B88[$i] = $B15[$i];
                                                    $B89[$i] = -$B24[$i];
                                                    $B90[$i] = 0;
                                                    $B91[$i] = 0;
                                                    $B92[$i] = 0;
                                                    $B93[$i] = 0;
                                                    $B94[$i] = $B35[$i];
                                                    $B95[$i] = 0;
                                                    $B96[$i] = 0;
                                                    
                                                    $B98[$i] = $B84+$B85+$B86[$i]+$B89[$i];
                                                    $B99[$i] = $B90[$i]+$B92[$i]+$B95[$i]+$B96[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B87[$i]+$B88[$i] : 0);
                                                    $B100[$i] = ($B98[$i]+$B99[$i]);
                                                } else {
                                                    $B86[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$B16[$i] : 0));
                                                    $B87[$i] = $B14[$i];
                                                    $B88[$i] = $B15[$i];
                                                    $B89[$i] = -$B24[$i];
                                                    $B90[$i] = $B19[$i];
                                                    $B91[$i] = $B20[$i];
                                                    $B92[$i] = $B29[$i];
                                                    $B93[$i] = $B30[$i];
                                                    $B94[$i] = $B35[$i];
                                                    $B95[$i] = $B45[$i];
                                                    $B96[$i] = $B48[$i];
                                                    
                                                    $B98[$i] = $B86[$i]+$B89[$i];
                                                    $B99[$i] = $B90[$i]+$B92[$i]+$B95[$i]+$B96[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B87[$i]+$B88[$i] : 0);
                                                    $B100[$i] = ($B98[$i]+$B99[$i]);
                                                }
                                            }
                                            $B101 = IRR($B100, 0.01);
                                            for ( $i=0; $i<12; $i++ ) {
                                                $IRRReslt *= ($B101+1);
                                            }
                                            $B102 = ($IRRReslt-1)*100;
                                            $C101 = $B101*12;
                                            $B103 = $TotalLenderInterestIncomeFlip = ($Property->InterestPaymentsMade=="No" ? array_sum($B22)+array_sum($B32) : array_sum($B19)+array_sum($B29));
                                            $B104 = $FeesToLenderFlip = array_sum($B14)+array_sum($B15);
                                            $B105 = array_sum($B48);
                                            $B106 = ($B103+$B104+$B105);
                                            
                                            $E104 = $B106/$G71;
                                            $E105 = $E104/($MonthsFlipRehab+$MonthsToSell)*12;
                                            
                                            $C106 = array_sum($B100)-$B106;
                                            
                                            $FinancingCostFlip = ($FlipPoints+$FlipOtherFinancingCosts+$FlipLoanInterest+$FlipRehabInterest);
                                            ?>
                                            <p class="text-center"><strong>Opportunity to Earn a 10%+ Secured Return!</strong></p>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <td>Property Street Address:</td>
                                                            <td class="text-center"><strong><?=$FFR->PropertyStreetAddress?></strong></td>
                                                            <td class="text-center"><strong><u>Presented By:</u></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Property City, State, Zip:</td>
                                                            <td class="text-center"><strong><?=$FFR->PropertyCityTown?>, <?=$FFR->PropertyStateProvince?>, <?=$FFR->PropertyZipCode?>, <?=$FFR->PropertyCountry?></strong></td>
                                                            <td class="text-center"><strong><?=$FFR->YourName?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bedrooms:</td>
                                                            <td class="text-center"><?=$FFR->Bedrooms?></td>
                                                            <td class="text-center"><?=$FFR->CompanyName?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Baths:</td>
                                                            <td class="text-center"><?=$FFR->Bathrooms?></td>
                                                            <td class="text-center"><?=$FFR->PhoneNumber?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Square Feet:</td>
                                                            <td class="text-center"><?=$FFR->SquareFeet?></td>
                                                            <td class="text-center"><?=$FFR->Website?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Year Built:</td>
                                                            <td class="text-center"><?=$FFR->YearBuilt?></td>
                                                            <td class="text-center"><?=$FFR->Email?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-lg-7">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <th colspan="2"><u>PURCHASE/LOAN ASSUMPTIONS</u></th>
                                                            <td class="text-right">% of ARV</td>
                                                        </tr>
                                                        <tr>
                                                            <td>After-Repair Value (ARV)</td>
                                                            <td class="text-right"><?=number_format($FFR->ARVFlip, 2)?></td>
                                                            <td class="text-right"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Purchase Price</td>
                                                            <td class="text-right"><?=number_format($FFR->PurchasePrice, 2)?></td>
                                                            <td class="text-right"><?=round(($FFR->ARVFlip == 0 ? 0 : round(($FFR->PurchasePrice/$FFR->ARVFlip)*100)))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Rehab Cost</td>
                                                            <td class="text-right"><?=number_format($FFR->ActualFlipBudget, 2)?></td>
                                                            <td class="text-right"><?=round(($FFR->ARVFlip == 0 ? 0 : round(($FFR->ActualFlipBudget/$FFR->ARVFlip)*100)))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Closing/Holding Costs</td>
                                                            <td class="text-right">
                                                                <?=($FFR->FinanceClosingHolding=="No" ? 0 : number_format($B53+$B54, 2))?>
                                                            </td>
                                                            <td class="text-right"><?=($FFR->FinanceClosingHolding=="No" ? 0 : round(($FFR->ARVFlip == 0 ? 0 : (($B53+$B54)/$FFR->ARVFlip)*100)))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Total Funds Needed:</strong></td>
                                                            <td class="text-right">
                                                                <strong>
                                                                    <?=number_format($FundedByLenderFlip, 2)?>
                                                                </strong>
                                                            </td>
                                                            <td class="text-right">
                                                                <strong>
                                                                    <?=round(($FFR->ARVFlip == 0 ? 0 : $FundedByLenderFlip/$FFR->ARVFlip*100))?>%
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Points/Interest (Deferred)</td>
                                                            <td class="text-right"><?=number_format($G67, 2)?></td>
                                                            <td class="text-right"><?=round(($FFR->ARVFlip == 0 ? 0 : ($G67/$FFR->ARVFlip)*100))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Loan Amount</td>
                                                            <td class="text-right"><?=number_format($FFR->TotalLoanFlip, 2)?></td>
                                                            <td class="text-right"><strong><?=round(($FFR->ARVFlip == 0 ? 0 : ($FFR->TotalLoanFlip/$FFR->ARVFlip)*100))?>%</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong>Points Offered to Lender</strong></td>
                                                            <td class="text-right">
                                                                <strong>
                                                                    <?=$FFR->RehabDiscountPoints > 0 ? round($FFR->RehabDiscountPoints, 1) . '%' : ""?>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong>Interest Rate Offered to Lender:</strong></td>
                                                            <td class="text-right"><strong><?=round($FFR->RehabInterestRate, 1)?>%</strong></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-lg-5">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <td colspan="2"><strong><u>Timeline Assumptions:</u></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Time to Complete Rehab:</td>
                                                            <td class="text-right"><?=$FFR->MonthsFlipRehab==1?$FFR->MonthsFlipRehab . ' Month':$FFR->MonthsFlipRehab . ' Months'?></td>
                                
                                                        </tr>
                                                        <tr>
                                                            <td> Time to Complete Sale:</td>
                                                            <td class="text-right"><?=$FFR->MonthsToSell==1?$FFR->MonthsToSell . ' Month':$FFR->MonthsToSell . ' Months'?></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Total Time:</td>
                                                            <td class="text-right"><?=($FFR->MonthsFlipRehab+$FFR->MonthsToSell)==1?($FFR->MonthsFlipRehab+$FFR->MonthsToSell) . ' Month':($FFR->MonthsFlipRehab+$FFR->MonthsToSell) . ' Months'?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <br><br><br><br><br><br><br><br> 
                                                                <strong><?=($Property->SplitWithLender=="Yes" ? "Profit Split to Lender:" : "")?></strong>
                                                            </td>
                                                            <td class="text-right"> 
                                                                <br><br><br><br><br><br><br><br> 
                                                                <strong><?=($Property->SplitWithLender=="Yes" ? round($Property->LenderSplit).'%' : "")?></strong>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-lg-12">
                                                    <table class="table" style="border:none;">
                                                        <tr>
                                                            <td width="20%">Property Description:</td>
                                                            <td><?=$FFR->PropertyDescription?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">Work Needed:</td>
                                                            <td><?=$FFR->WorkNeeded?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-lg-8 col-lg-offset-2">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <th colspan="2"><strong>PROJECTED FINANCIAL RESULTS FOR LENDER:</strong></th>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Interest Income</td>
                                                            <td class="text-right"><strong>$<?=number_format($TotalLenderInterestIncomeFlip, 0)?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Points, Fees</td>
                                                            <td class="text-right"><strong>$<?=number_format($FeesToLenderFlip, 0)?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Profit Split</td>
                                                            <td class="text-right"><?=($B105==0 ? "" : '$'.number_format($B105))?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Income to Lender</td>
                                                            <td class="text-right">
                                                                <strong>$<?=number_format($TotalLenderInterestIncomeFlip+$FeesToLenderFlip+$B105, 0)?></strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Cash-on-Cash Return (annualized)</td>
                                                            <td class="text-right">
                                                                <strong>
                                                                    <?=($FFR->FinancingUsed=="All Cash" ? "" : round($E105*100).'%')?>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Internal Rate of Return (annualized)</td>
                                                            <td class="text-right">
                                                                <strong><?=($FFR->FinancingUsed=="All Cash" ? "" : round($B102).'%')?>
                                                                </strong>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['RefiFundingRequest']) ) {
                                        $RFR = $print_data['SingleProposal']['RefiFundingRequest']; ?>
                                            
                                        <div class="RefiFundingRequest"><br />
                                            <?php 
                                            $Property = $RFR->Property;
                                            $RefiBudget = $RFR->RefiBudget;
                                            
                                            $MonthsRefiRehab = $Property->MonthsRefiRehab ? $Property->MonthsRefiRehab : 0;
                                            $MonthsToRefi = $Property->MonthsToRefi ? $Property->MonthsToRefi : 0;
                                            $RehabDiscountPoints = $Property->RehabDiscountPoints ? ($Property->RehabDiscountPoints/100) : 0;
                                            $ActualFinancedRent = $Property->ActualFinancedRent ? $Property->ActualFinancedRent : 0;
                                            $OtherPoints = $Property->OtherPoints ? ($Property->OtherPoints/100) : 0;
                                            $RefiHoldingCosts = $Property->RefiHoldingCosts ? $Property->RefiHoldingCosts : 0;
                                            $RehabInterestRate = $Property->RehabInterestRate ? ($Property->RehabInterestRate/100) : 0;
                                            $LumpSumRefiBudget = $Property->LumpSumRefiBudget ? $Property->LumpSumRefiBudget : 0;
                                            $DetailedRefiBudget = $Property->DetailedRefiBudget ? $Property->DetailedRefiBudget : 0; 
                                            $LenderSplit = $Property->LenderSplit ? ($Property->LenderSplit/100) : 0;
                                            
                                            $Row118Total = 0; $Row126Total = 0; $IRRReslt = 1;
                                            $MonthTotal = array();
                                            $MonthMatchArray = array();
                                            $RefiBudgetColumn = array(); $MonthPaidColumn = array();
                                            foreach ( $RefiBudget as $key => $refi ) {
                                                $RefiBudgetColumn[$key] = $refi->Budget;
                                                $MonthPaidColumn[$key] = $refi->MonthPaid;
                                            }
                                            
                                            $B2 = ($Property->FinancingUsed=="Financing" ? "Yes" : "No");
                                            $B3 = $Property->CapSelection;
                                            $B4 = $Property->FinancingCap ? ($Property->FinancingCap/100) : 0;
											
											$F2 = $Property->ARVFlip ? $Property->ARVFlip : 0;
											$F3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualFlipBudget ? $Property->ActualFlipBudget : 0);
											$F4 = ($Property->CapSelection=="ARV" ? $B4 * $F2 : $B4 * $F3);
											
											$B10 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
											$B11 = $PurchaseFinanced = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B10 : min($B10, $F4)) : 0);
                                            
                                            $I2 = $Property->ARVRent ? $Property->ARVRent : 0;
                                            $I3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualRefiBudget ? $Property->ActualRefiBudget : 0);
                                            $I4 = ($Property->CapSelection=="ARV" ? $B4 * $I2 : $B4 * $I3);
                                            
                                            // REFI
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B109[$i] = 1; // B109
                                                    $B110[$i] = ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B110
                                                    $B111[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B111
                                                } else {
                                                    $B109[$i] = ($i>$MonthsRefiRehab ? 0 : 1); // B109
                                                    $B110[$i] = ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B110
                                                    $B111[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B111
                                                }
                                            }
                                            
                                            $B112 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
                                            $B113 = $PurchaseFinancedRefi = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B112 : min($B112, $I4)) : 0);
                                            $B114 = $B112-$B113;
                                            $B115 = $Property->ClosingCosts ? $Property->ClosingCosts : 0;
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B116[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($RehabDiscountPoints)*$ActualFinancedRent : 0); // B116
                                                    $B117[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($OtherPoints)*$ActualFinancedRent : 0); // B117
                                                    $B118[$i] = 0; // B118
                                                    $Row118Total = $Row118Total + $B118[$i];
                                                    $B119[$i] = $PurchaseFinanced + ($B115 + $B118[$i]) * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0); // B119
                                                    $B120[$i] = 0; // B120
                                                    $B121[$i] = 0; // B121
                                                    $B122[$i] = 0; // B122
                                                    $B123[$i] = 0; // B123
                                                    $B124[$i] = 0; // B124
                                                    $B125[$i] = ($Property->RefiRehabDrawSelection=="Fund Rehab at Closing" ? ($Property->RefiRehabBudgetMethod=="Quick Lump Sum" ? $LumpSumRefiBudget : $DetailedRefiBudget) : 0); // B125
                                                    $B126[$i] = ($B3=="Cost" ? $B125[$i] * $B4 : max(0, min($I4, $B125[$i]))) * $B111[$i]; // B126
                                                    $Row126Total = $Row126Total + $B126[$i];
                                                    $B127[$i] = $B125[$i] - $B126[$i]; // B127
                                                    $B128[$i] = ($B125[$i]==0 ? 0 : $B125[$i]) * $B110[$i]; // B128
                                                    $B129[$i] = $B126[$i]; // B129
                                                    $B130[$i] = 0; // B130
                                                    $B131[$i] = 0; // B131
                                                    $B132[$i] = 0; // B132
                                                    $B133[$i] = 0; // B133
                                                    $B134[$i] = 0; // B134
                                                    $B135[$i] = $B112+$B115+$B116[$i]+$B117[$i]+$B125[$i]; // B135
                                                    $B136[$i] = $PurchaseFinancedRefi+($Row126Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])); // B136
                                                    $B137[$i] = $PurchaseFinancedRefi+$Property->ClosingCosts*($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))+($Row126Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])); // B137
                                                    $B138[$i] = $B114+($B115+$B118[$i])*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B120[$i]+$B127[$i]+$B131[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B116[$i]+$B117[$i]) : 0); // B138
                                                    $B139[$i] = $B135[$i]-($B137[$i]+$B138[$i]); // B139
                                                    $B140[$i] = $B114+$B115*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 :1))+$B116[$i]+$B117[$i]+$B118[$i]*($Property->FinanceClosingHolding=="Yes" ? 0 : 1)+$B121[$i]+$B127[$i]+$B131[$i]; // B140
                                                } else {
                                                    $B116[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($RehabDiscountPoints)*$ActualFinancedRent : 0); // B116
                                                    $B117[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($OtherPoints)*$ActualFinancedRent : 0); // B117				
                                                    $B118[$i] = $RefiHoldingCosts/($MonthsRefiRehab+$MonthsToRefi)*$B110[$i]; // B118
                                                    $Row118Total = $Row118Total + $B118[$i];
                                                    $B119[$i] = ($B119[$i-1] + $B118[$i] * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))*$B110[$i]; // B119
                                                    $B120[$i] = $RehabInterestRate*$B119[$i-1] / 12 * ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B120
                                                    $B121[$i] = $Property->InterestPaymentsMade=="Yes" ? $B120[$i] : 0; // B121
                                                    $B122[$i] = $B120[$i] - $B121[$i]; // B122
                                                    if ( $i == 1 ) {
                                                        $B123[$i] = $B122[$i] + $B123[$i-1]; // B123
                                                    } else {
                                                        $B123[$i] = ($B122[$i]+$B123[$i-1]+$B123[$i-1]*($RehabInterestRate/12))*$B110[$i]; // B123
                                                    }
                                                    $B124[$i] = $B123[$i] * ($i == ($MonthsRefiRehab + $MonthsToRefi) ? 1 : 0); // B124
                                                    
                                                    $ColU = ($i<=$MonthsRefiRehab ? 1 : 0);
                                                    $MonthMatch = 0;
                                                    for ( $j = 0; $j < count($MonthPaidColumn); $j++ ) {
                                                        if ( $MonthPaidColumn[$j] == $i ) {
                                                            $MonthMatch += $RefiBudgetColumn[$j];
                                                        }
                                                    }
                                                    $MonthMatchArray[$i] = $MonthMatch;
                                                    $B125[$i] = $Property->RefiRehabDrawSelection=="Fund Rehab at Closing" ? 0 : ($Property->RefiRehabBudgetMethod=="Detailed Input" ? $MonthMatchArray[$i] * $ColU : $LumpSumFlipBudget / $MonthsRefiRehab * $ColU); // B125
                                                    $B126[$i] = ($B3=="Cost" ? $B125[$i] * $B4 : max(0, min($I4-$B137[$i-1], $B125[$i]))) * $B111[$i]; // B126
                                                    $B127[$i] = $B125[$i] - $B126[$i]; // B127
                                                    $B128[$i] = ($B125[$i]==0 ? $B128[$i-1] : $B125[$i] + $B128[$i-1]) * $B110[$i]; // B128
                                                    $B129[$i] = ($B126[$i]==0 ? $B129[$i-1] : $B126[$i] + $B129[$i-1]) * $B110[$i]; // B129
                                                    $B130[$i] = $B129[$i-1] * $RehabInterestRate/12 * $B110[$i] * $B111[$i]; // B130
                                                    $B131[$i] = $Property->InterestPaymentsMade=="Yes" ? $B130[$i] : 0; // B131
                                                    $B132[$i] = $B130[$i] - $B131[$i]; // B132
                                                    if ( $i == 1 ) {
                                                        $B133[$i] = $B132[$i] + $B133[$i-1]; // B133
                                                    } else {
                                                        $B133[$i] = ($B132[$i] + $B133[$i-1] + $B133[$i-1] * ($RehabInterestRate/12)) * $B110[$i]; // B133
                                                    }
                                                    $B134[$i] = $B133[$i] * ($i==($MonthsRefiRehab+$MonthsToRefi) ? 1 : 0); // B134
                                                    $B135[$i] = ($B135[$i-1]+($B116[$i]+$B117[$i]+$B118[$i])+$B121[$i]+$B122[$i]+$B123[$i-1]*$RehabInterestRate/12+$B125[$i]+$B131[$i]+$B132[$i]+$B133[$i-1]*$RehabInterestRate/12)*$B110[$i]; // B135
                                                    $B136[$i] = ($PurchaseFinancedRefi+$B129[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])))*$B110[$i]; // B136
                                                    $B137[$i] = ($PurchaseFinancedRefi+($Property->ClosingCosts+($Row118Total))*($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))+$B129[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i]))+$B123[$i]+$B133[$i])*$B110[$i]; // B137
                                                    $B138[$i] = ($B118[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B121[$i]+$B127[$i]+$B131[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B116[$i]+$B117[$i]) : 0)+$B138[$i-1])*$B110[$i]; // B138
                                                    $B139[$i] = $B135[$i]-($B137[$i]+$B138[$i]); // B139
                                                    $B140[$i] = $B118[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B121[$i]+$B127[$i]+$B131[$i]; // B140
                                                }
                                            }
                                            
                                            $B141 = array_sum($B140)-max($B138);
                                            $B142 = max($B135);
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B144[$i] = 0; // B144
                                                    $B145[$i] = 0; // B145
                                                    $B145[$i] = 0; // B146
                                                    $B147[$i] = 0; // B147
                                                    $B149[$i] = 0; // B149
                                                    $B150[$i] = 0; // B150
                                                    $B151[$i] = 0; // B151
                                                    $B152[$i] = 0; // B152
                                                    $B153[$i] = 0; // B153
                                                    $B154[$i] = 0; // B154
                                                } else {
                                                    $B144[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->RefiAmount : 0); // B144
                                                    $B145[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? -($Property->RefiDiscountPoints/100)*$Property->RefiAmount : 0); // B145
                                                    $B146[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? -$B137[$i] : 0); // B146
                                                    $B147[$i] = ($B144[$i]+$B145[$i]+$B146[$i]); // B147
                                                    $B149[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? max(0, $B147[$i]-$Property->CashRequiredRent) : 0); // B149
                                                    $B150[$i] = 	($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->LenderSplit*($Property->FinancingUsed=="Financing" ? 1 : 0)*$B149[$i]*($Property->SplitWithLender=="Yes" ? 1 : 0) : 0); // B150
                                                    $B151[$i] = $B149[$i]-$B150[$i]; // B151
                                                    $B152[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "infinite" : $B149[$i]/$Property->CashRequiredRent/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B152
                                                    $B153[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? max(0, $B138[$i]-$B147[$i]) : 0); // B153
                                                    $B154[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->ARVRent-$Property->RefiAmount : 0); // B154
                                                }
                                            }
                                            
                                            $B158 = $Property->PurchasePrice;
                                            $B159 = $B115;
                                            $B160 = array_sum($B118);
                                            $B161 = array_sum($B116);
                                            $B162 = array_sum($B117);
                                            $B163 = array_sum($B125);
                                            $B164 = array_sum($B121)+array_sum($B124);
                                            $B165 = array_sum($B131)+array_sum($B134);
                                            $B167 = ($B158+$B159+$B160+$B161+$B162+$B163+$B164+$B165);
                                            
                                            $B169 = $RefiClosingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? $B115 : 0);
                                            $B170 = $RefiHoldingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? array_sum($B118) : 0);
                                            $B171 = $RentPoints = array_sum($B116);
                                            $B172 = $RentOtherFinancingCosts = array_sum($B117);
                                            $B173 = $RentLoanInterest = array_sum($B121)+array_sum($B124);
                                            $B174 = $RentRehabInterest = array_sum($B131)+array_sum($B134);
                                            $B175 = ($B171+$B172+$B173+$B174);
                                            
                                            $G170 = $PurchaseFinanced;
                                            $G171 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0));
                                            $G172 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? array_sum($B118) : 0));
                                            $G173 = ($Property->PointsClosingUpfront=="Paid Backend" ? array_sum($B116)+array_sum($B117) : 0)+($Property->InterestPaymentsMade=="No" ? (array_sum($B124)+array_sum($B134)) : 0);
                                            $G174 = array_sum($B126);
                                            $G175 = ($G170+$G171+$G172+$G173+$G174);
                                            $G177 = $FundedByLenderREfi = $G170+$G171+$G172+$G174;
                                            
                                            $B177 = $Property->TotalLoanRent;
                                            $B178 = $Property->CashRequiredRent;
                                            $B179 = ($B177+$B178);
                                            
                                            $D179 = $B142-$B179;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B181[$i] = 0; // B181
                                                    $B182[$i] = 0; // B182
                                                    $B184[$i] = 0; // B184
                                                    $B185[$i] = 0; // B185
                                                } else {
                                                    $B181[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "Infinite" : $B149[$i]/$Property->CashRequiredRent) : 0); // B181
                                                    $B182[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($B181[$i]=="Infinite" ? "Infinite" : $B181[$i]/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B182
                                                    $B184[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "Infinite" : $B151[$i]/$Property->CashRequiredRent) : 0); // B184
                                                    $B185[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($B184[$i]=="Infinite" ? "Infinite" : $B184[$i]/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B185
                                                }
                                            }
                                            
                                            // Cashflows to Lender
                                            $B190 = -$PurchaseFinancedRefi;
                                            $B191 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$Property->ClosingCosts : 0));
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B192[$i] = 0;
                                                    $B193[$i] = $B116[$i];
                                                    $B194[$i] = $B117[$i];
                                                    $B195[$i] = -$B126[$i];
                                                    $B196[$i] = 0;
                                                    $B197[$i] = 0;
                                                    $B198[$i] = 0;
                                                    $B199[$i] = 0;
                                                    $B200[$i] = $B137[$i];
                                                    $B201[$i] = 0;
                                                    $B202[$i] = 0;
                                                    
                                                    $B204[$i] = $B190+$B191+$B192[$i]+$B195[$i];
                                                    $B205[$i] = $B196[$i]+$B198[$i]+$B201[$i]+$B202[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B193[$i]+$B194[$i] : 0);
                                                    $B206[$i] = ($B204[$i]+$B205[$i]);
                                                } else {
                                                    $B192[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$B118[$i] : 0));
                                                    $B193[$i] = $B116[$i];
                                                    $B194[$i] = $B117[$i];
                                                    $B195[$i] = -$B126[$i];
                                                    $B196[$i] = $B121[$i];
                                                    $B197[$i] = $B122[$i];
                                                    $B198[$i] = $B131[$i];
                                                    $B199[$i] = $B132[$i];
                                                    $B200[$i] = $B137[$i];
                                                    $B201[$i] = -$B146[$i];
                                                    $B202[$i] = $B150[$i];
                                                    
                                                    $B204[$i] = $B192[$i]+$B195[$i];
                                                    $B205[$i] = $B196[$i]+$B198[$i]+$B201[$i]+$B202[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B193[$i]+$B194[$i] : 0);
                                                    $B206[$i] = ($B204[$i]+$B205[$i]);
                                                }
                                            }
                                            $B207 = IRR($B206, 0.01);
                                            for ( $i=0; $i<12; $i++ ) {
                                                $IRRReslt *= ($B207+1);
                                            }
                                            $B208 = ($IRRReslt-1)*100;
                                            $C207 = $B207*12;
                                            $B209 = $TotalLenderInterestIncomeRefi = ($Property->InterestPaymentsMade=="No" ? array_sum($B124)+array_sum($B134) : array_sum($B121)+array_sum($B131));
                                            $B210 = $FeesToLenderRefi = array_sum($B116)+array_sum($B117);
                                            $B211 = array_sum($B150);
                                            $B212 = ($B209+$B210+$B211);
                                            
                                            $E210 = $B212/$G177;
                                            $E211 = $E210/($MonthsRefiRehab+$MonthsToRefi)*12;
                                            
                                            $C212 = array_sum($B206)-$B212;
                                            
                                            $FinancingCostRent = $RentPoints+$RentOtherFinancingCosts+$RentLoanInterest+$RentRehabInterest; 
                                            ?>
                                            <p class="text-center"><strong>Opportunity to Earn a 10%+ Secured Return!</strong></p>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <td>Property Street Address:</td>
                                                            <td class="text-center"><strong><?=$RFR->PropertyStreetAddress?></strong></td>
                                                            <td class="text-center"><strong><u>Presented By:</u></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Property City, State, Zip:</td>
                                                            <td class="text-center"><strong><?=$RFR->PropertyCityTown?>, <?=$RFR->PropertyStateProvince?>, <?=$RFR->PropertyZipCode?>, <?=$RFR->PropertyCountry?></strong></td>
                                                            <td class="text-center"><strong><?=$RFR->YourName?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bedrooms:</td>
                                                            <td class="text-center"><?=$RFR->Bedrooms?></td>
                                                            <td class="text-center"><?=$RFR->CompanyName?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Baths:</td>
                                                            <td class="text-center"><?=$RFR->Bathrooms?></td>
                                                            <td class="text-center"><?=$RFR->PhoneNumber?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Square Feet:</td>
                                                            <td class="text-center"><?=$RFR->SquareFeet?></td>
                                                            <td class="text-center"><?=$RFR->Website?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Year Built:</td>
                                                            <td class="text-center"><?=$RFR->YearBuilt?></td>
                                                            <td class="text-center"><?=$RFR->Email?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-lg-7">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <th colspan="2"><u>PURCHASE/LOAN ASSUMPTIONS</u></th>
                                                            <td class="text-right">% of ARV</td>
                                                        </tr>
                                                        <tr>
                                                            <td>After-Repair Value (ARV)</td>
                                                            <td class="text-right"><?=number_format($RFR->ARVRent, 2)?></td>
                                                            <td class="text-right"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Purchase Price</td>
                                                            <td class="text-right"><?=number_format($RFR->PurchasePrice, 2)?></td>
                                                            <td class="text-right"><?=round(($RFR->ARVRent == 0 ? 0 : ($RFR->PurchasePrice/$RFR->ARVRent)*100))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Rehab Cost</td>
                                                            <td class="text-right"><?=number_format($RFR->ActualRefiBudget, 2)?></td>
                                                            <td class="text-right"><?=round(($RFR->ARVRent == 0 ? 0 : ($RFR->ActualRefiBudget/$RFR->ARVRent)*100))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Closing/Holding Costs</td>
                                                            <td class="text-right"><?=($RFR->FinanceClosingHolding=="No" ? 0 : number_format($B159+$B160, 2))?></td>
                                                            <td class="text-right"><?=round(($RFR->ARVRent == 0 ? 0 : ($B159+$B160/$RFR->ARVRent)*100))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td><strong>Total Funds Needed:</strong></td>
                                                            <td class="text-right"><strong><?=number_format($FundedByLenderREfi, 2)?></strong></td>
                                                            <td class="text-right"><strong><?=round(($RFR->ARVRent == 0 ? 0 : ($FundedByLenderREfi/$RFR->ARVRent)*100))?>%</strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Points/Interest (Deferred)</td>
                                                            <td class="text-right"><?=number_format($G173, 2)?></td>
                                                            <td class="text-right"><?=round(($RFR->ARVRent == 0 ? 0 : ($G173/$RFR->ARVRent)*100))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Loan Amount</td>
                                                            <td class="text-right"><?=number_format($RFR->TotalLoanRent, 2)?></td>
                                                            <td class="text-right"><?=round(($RFR->ARVRent == 0 ? 0 : ($RFR->TotalLoanRent/$RFR->ARVRent)*100))?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong>Points Offered to Lender</strong></td>
                                                            <td class="text-right"><strong><?=($RFR->RehabDiscountPoints>0 ? round($RFR->RehabDiscountPoints).'%' : "")?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"><strong>Interest Rate Offered to Lender:</strong></td>
                                                            <td class="text-right"><strong><?=round($RFR->RehabInterestRate)?>%</strong></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-lg-5">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <td colspan="2"><strong><u>Timeline Assumptions:</u></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Time to Complete Rehab:</td>
                                                            <td class="text-right"><?=($RFR->MonthsRefiRehab==1 ? $RFR->MonthsRefiRehab.' Month' : $RFR->MonthsRefiRehab.' Months')?></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Time to Complete Sale:</td>
                                                            <td class="text-right"><?=($RFR->MonthsToRefi==1 ? $RFR->MonthsToRefi.' Month' : $RFR->MonthsToRefi.' Months')?></td>
                                                        </tr>
                                                        <tr>
                                                            <td> Total Time:</td>
                                                            <td class="text-right"><?=(($RFR->MonthsRefiRehab+$RFR->MonthsToRefi)==1 ? ($RFR->MonthsRefiRehab+$RFR->MonthsToRefi).' Month' : ($RFR->MonthsRefiRehab+$RFR->MonthsToRefi).' Months')?></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2"></td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <br><br><br><br><br><br><br><br> 
                                                                <strong><?=($RFR->SplitWithLender=="Yes" ? "Profit Split to Lender:" : "")?></strong>
                                                            </td>
                                                            <td class="text-right">
                                                                <br><br><br><br><br><br><br><br> 
                                                                <strong><?=($RFR->SplitWithLender=="Yes" ? round($RFR->LenderSplit).'%' : "")?></strong>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-lg-12">
                                                    <table class="table" style="border:none;">
                                                        <tr>
                                                            <td width="20%">Property Description:</td>
                                                            <td><?=$RFR->PropertyDescription?></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20%">Work Needed:</td>
                                                            <td><?=$RFR->WorkNeeded?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                                <div class="col-lg-8 col-lg-offset-2">
                                                    <table class="table table-borderless table-bg" border="1" bordercolor="#ddd">
                                                        <tr>
                                                            <th colspan="2"><strong>PROJECTED FINANCIAL RESULTS FOR LENDER:</strong></th>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Interest Income</td>
                                                            <td class="text-right"><strong>$<?=number_format($TotalLenderInterestIncomeRefi, 0)?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Points, Fees</td>
                                                            <td class="text-right"><strong>$<?=number_format($FeesToLenderRefi, 0)?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Profit Split from Cash-Out Refi</td>
                                                            <td class="text-right"><?=($B211==0 ? "" : '$'.number_format($B211, 0))?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Income to Lender</td>
                                                            <td class="text-right">
                                                                <strong>$<?=number_format($TotalLenderInterestIncomeRefi+$FeesToLenderRefi+$B211, 0)?></strong>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>Cash-on-Cash Return (annualized)</td>
                                                            <td class="text-right"><strong><?=($RFR->FinancingUsed=="All Cash" ? "" : round($E211*100).'%')?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Internal Rate of Return (annualized)</td>
                                                            <td class="text-right"><strong><?=($RFR->FinancingUsed=="All Cash" ? "" : round($B208).'%')?></strong></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['FlipCashFlowLender']) ) {
                                        $FCL = $print_data['SingleProposal']['FlipCashFlowLender']; ?>
                                            
                                        <div class="FlipCashFlowLender"><br />
                                            <?php
                                            $Property = $FCL['Property'];
                                            $FlipBudget = $FCL['FlipBudget'];
                                            
                                            $MonthsFlipRehab = $Property->MonthsFlipRehab ? $Property->MonthsFlipRehab : 0;
                                            $MonthsToSell = $Property->MonthsToSell ? $Property->MonthsToSell : 0;
                                            $RehabDiscountPoints = $Property->RehabDiscountPoints ? ($Property->RehabDiscountPoints/100) : 0;
                                            $ActualFinancedFlip = $Property->ActualFinancedFlip ? $Property->ActualFinancedFlip : 0;
                                            $OtherPoints = $Property->OtherPoints ? ($Property->OtherPoints/100) : 0;
                                            $FlipHoldingCosts = $Property->FlipHoldingCosts ? $Property->FlipHoldingCosts : 0;
                                            $RehabInterestRate = $Property->RehabInterestRate ? ($Property->RehabInterestRate/100) : 0;
                                            $LumpSumFlipBudget = $Property->LumpSumFlipBudget ? $Property->LumpSumFlipBudget : 0;
                                            $DetailedFlipBudget = $Property->DetailedFlipBudget ? $Property->DetailedFlipBudget : 0;
                                            $LenderSplit = $Property->LenderSplit ? ($Property->LenderSplit/100) : 0;
                                            
                                            $Row16Total = 0; $Row24Total = 0; $IRRReslt = 1;
                                            $MonthTotal = array();
                                            $MonthMatchArray = array();
                                            $FlipBudgetColumn = array(); $MonthPaidColumn = array();
                                            foreach ( $FlipBudget as $key => $flip ) {
                                                $FlipBudgetColumn[$key] = $flip->Budget;
                                                $MonthPaidColumn[$key] = $flip->MonthPaid;
                                            }
                                            
                                            $B2 = ($Property->FinancingUsed=="Financing" ? "Yes" : "No");
                                            $B3 = $Property->CapSelection;
                                            $B4 = $Property->FinancingCap ? ($Property->FinancingCap/100) : 0;
                                            
                                            $F2 = $Property->ARVFlip ? $Property->ARVFlip : 0;
                                            $F3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualFlipBudget ? $Property->ActualFlipBudget : 0);
                                            $F4 = ($Property->CapSelection=="ARV" ? $B4 * $F2 : $B4 * $F3);
                                            
                                            // FLIP
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B7[$i] = 1; // B7
                                                    $B8[$i] = ($i > ($MonthsFlipRehab + $MonthsToSell) ? 0 : 1); // B8
                                                    $B9[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B9
                                                } else {
                                                    $B7[$i] = ( $i > $MonthsFlipRehab ? 0 : 1); // B7
                                                    $B8[$i] = ($i > ($MonthsFlipRehab + $MonthsToSell) ? 0 : 1); // B8
                                                    $B9[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B9
                                                }
                                            }
                                            
                                            $B10 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
                                            $B11 = $PurchaseFinanced = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B10 : min($B10, $F4)) : 0);
                                            $B12 = $B10-$B11;
                                            $B13 = $Property->ClosingCosts ? $Property->ClosingCosts : 0;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B14[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab + $MonthsToSell) ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0)); // B14
                                                    $B15[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab+$MonthsToSell) ? ($OtherPoints)*$Property->ActualFinancedFlip : 0)); // B15
                                                    $B16[$i] = 0; // B16
                                                    $Row16Total = $Row16Total + $B16[$i];
                                                    $B17[$i] = $PurchaseFinanced + ($B13 + $B16[$i]) * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0); // B17
                                                    $B18[$i] = 0; // B18
                                                    $B19[$i] = 0; // B19
                                                    $B20[$i] = 0; // B20
                                                    $B21[$i] = 0; // B21
                                                    $B22[$i] = 0; // B22
                                                    $MonthTotal[$i] = ($Property->FlipRehabDrawSelection=="Fund Rehab at Closing" ? ($Property->FlipRehabBudgetMethod=="Quick Lump Sum" ? $LumpSumFlipBudget : $DetailedFlipBudget) : 0);
                                                    $B23[$i] = $MonthTotal[$i]; // B23
                                                    $B24[$i] = ($B3=="Cost" ? $MonthTotal[$i] * $B4 : max(0, min($F4, $MonthTotal[$i]))) * $B9[$i]; // B24
                                                    $Row24Total = $Row24Total + $B24[$i];
                                                    $B25[$i] = ($B23[$i] - $B24[$i]); // B25
                                                    $B26[$i] = ($B23[$i]==0 ? 0 : $B23[$i]) * $B8[$i]; // B26
                                                    $B27[$i] = $B24[$i]; // B27
                                                    $B28[$i] = 0; // B28
                                                    $B29[$i] = 0; // B29
                                                    $B30[$i] = 0; // B30
                                                    $B31[$i] = 0; // B31
                                                    $B32[$i] = 0; // B32
                                                    $B33[$i] = $B10+$B13+$B14[$i]+$B15[$i]+$B23[$i]; // B33
                                                    $B34[$i] = ($PurchaseFinanced + $B27[$i]) * $B8[$i]; // B34
                                                    $B35[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : $PurchaseFinanced+$Property->ClosingCosts*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+($Row24Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B14[$i]+$B15[$i]))); // B35
                                                    $B36[$i] = $B12+$Property->ClosingCosts*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B16[$i]+$B18[$i]+$B25[$i]+$B29[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B14[$i]+$B15[$i]) : 0); // B36
                                                    $B37[$i] = $B33[$i]-($B35[$i]+$B36[$i]); // B37
                                                    $B38[$i] = $B12+$B13*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B14[$i]+$B15[$i]+$B16[$i]+$B19[$i]+$B25[$i]+$B29[$i]; // B38
                                                } else {
                                                    $B14[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab + $MonthsToSell) ? ($RehabDiscountPoints)*$Property->ActualFinancedFlip : 0)); // B14
                                                    $B15[$i] = ($Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedFlip : 0) : ($i==($MonthsFlipRehab+$MonthsToSell) ? ($OtherPoints)*$ActualFinancedFlip : 0)); // B15
                                                    $B16[$i] = $FlipHoldingCosts/($MonthsFlipRehab+$MonthsToSell)*$B8[$i]; // B16
                                                    $Row16Total = $Row16Total + $B16[$i];
                                                    $B17[$i] = ($B17[$i-1] + $B16[$i] * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))*$B8[$i]; // B17
                                                    $B18[$i] = $RehabInterestRate * $B17[$i-1] / 12 * ($i>($MonthsFlipRehab+$MonthsToSell) ? 0 : 1); // B18
                                                    $B19[$i] = $Property->InterestPaymentsMade=="Yes" ? $B18[$i] : 0; // B19
                                                    $B20[$i] = $B18[$i] - $B19[$i]; // B20
                                                    if ( $i == 1 ) {
                                                        $B21[$i] = $B20[$i] + $B21[$i-1]; // B21
                                                    } else {
                                                        $B21[$i] = ($B20[$i]+$B21[$i-1]+$B21[$i-1]*($RehabInterestRate/12))*$B8[$i]; // B21
                                                    }
                                                    $B22[$i] = $B21[$i] * ($i == ($MonthsFlipRehab + $MonthsToSell) ? 1 : 0); // B22
                                                    $ColU = ($i<=$MonthsFlipRehab ? 1 : 0);
                                                    $MonthMatch = 0;
                                                    for ( $j = 0; $j < count($MonthPaidColumn); $j++ ) {
                                                        if ( $MonthPaidColumn[$j] == $i ) {
                                                            $MonthMatch += $FlipBudgetColumn[$j];
                                                        }
                                                    }
                                                    $MonthMatchArray[$i] = $MonthMatch;
                                                    $MonthTotal[$i] = $Property->FlipRehabDrawSelection=="Fund Rehab at Closing" ? 0 : ($Property->FlipRehabBudgetMethod=="Detailed Input" ? $MonthMatchArray[$i] * $ColU : $LumpSumFlipBudget / $MonthsFlipRehab * $ColU);
                                                    $B23[$i] = $MonthTotal[$i]; // B23
                                                    $B24[$i] = ($B3=="Cost" ? $MonthTotal[$i] * $B4 : max(0, min($F4-($B34[$i-1]), $B23[$i]))) * $B9[$i]; // B24
                                                    $Row24Total = $Row24Total + $B24[$i];
                                                    $B25[$i] = $B23[$i] - $B24[$i]; // B25
                                                    $B26[$i] = ($B23[$i]=="" ? $B26[$i-1] : $B23[$i] + $B26[$i-1]) * $B8[$i]; // B26
                                                    $B27[$i] = ($B24[$i]==0 ? $B27[$i-1] : $B24[$i] + $B27[$i-1]) * $B8[$i]; // B27
                                                    $B28[$i] = $B27[$i-1] * $RehabInterestRate/12 * $B8[$i] * $B9[$i]; // B28
                                                    $B29[$i] = $Property->InterestPaymentsMade=="Yes" ? $B28[$i] : 0; // B29
                                                    $B30[$i] = $B28[$i] - $B29[$i]; // B30
                                                    if ( $i == 1 ) {
                                                        $B31[$i] = $B30[$i] + $B31[$i-1]; // B31
                                                    } else {
                                                        $B31[$i] = ($B30[$i] + $B31[$i-1] + $B31[$i-1] * ($RehabInterestRate/12)) * $B8[$i]; // B31
                                                    }
                                                    $B32[$i] = $B31[$i] * ($i==($MonthsFlipRehab+$MonthsToSell) ? 1 : 0); // B32
                                                    $B33[$i] = ($B33[$i-1]+($B14[$i]+$B15[$i]+$B16[$i])+$B19[$i]+$B20[$i]+$B21[$i-1]*$RehabInterestRate/12+$B23[$i]+$B29[$i]+$B30[$i]+$B31[$i-1]*$RehabInterestRate/12)*$B8[$i]; // B33
                                                    $B34[$i] = ($PurchaseFinanced + $B27[$i]) * $B8[$i]; // B34
                                                    $B35[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($PurchaseFinanced+$Property->ClosingCosts*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+($Row16Total)*($Property->FinanceClosingHolding=="Yes" ? 1 : 0)+$B27[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B14[$i]+$B15[$i]))+$B21[$i]+$B31[$i])*$B8[$i]); // B35
                                                    $B36[$i] = ($B12+$B16[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B19[$i]+$B25[$i]+$B29[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B14[$i]+$B15[$i]) : 0)+$B36[$i-1])*$B8[$i]; // B36
                                                    $B37[$i] = $B33[$i]-($B35[$i]+$B36[$i]); // B37
                                                    $B38[$i] = $B16[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B19[$i]+$B25[$i]+$B29[$i]; // B38
                                                }
                                            }
                                            
                                            $B39 = array_sum($B38)-max($B36);
                                            $B40 = max($B33);
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B43[$i] = 0; // B43
                                                    $B44[$i] = 0; // B44
                                                    $B45[$i] = 0; // B45
                                                    $B46[$i] = 0; // B46
                                                    $B47[$i] = 0; // B47
                                                    $B48[$i] = 0; // B48
                                                    $B49[$i] = 0; // B49
                                                } else {
                                                    $B43[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $Property->ResalePrice : 0); // B43
                                                    $B44[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $Property->ResalePrice*($Property->CostOfSale/100) : 0); // B44
                                                    $B45[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? min($B35[$i], $B43[$i]-$B44[$i]) : 0); // B45
                                                    $B46[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? min($B36[$i], $B43[$i]-$B44[$i]-$B45[$i]) : 0); // B46
                                                    $B47[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $B43[$i]-$B44[$i]-$B45[$i]-$B46[$i] : 0); // B47
                                                    $B48[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? $LenderSplit*($Property->FinancingUsed=="Financing" ? 1 : 0)*$B47[$i]*($Property->SplitWithLender=="Yes" ? 1 : 0) : 0); // B48
                                                    $B49[$i] = $B47[$i]-$B48[$i]; // B49
                                                }
                                            }
                                            
                                            $B52 = $Property->PurchasePrice;
                                            $B53 = $B13;
                                            $B54 = array_sum($B16);
                                            $B55 = array_sum($B14);
                                            $B56 = array_sum($B15);
                                            $B57 = array_sum($B23);
                                            $B58 = array_sum($B19)+array_sum($B22);
                                            $B59 = array_sum($B29)+array_sum($B32);
                                            $B61 = ($B52+$B53+$B54+$B55+$B56+$B57+$B58+$B59);
                                            
                                            $B63 = $PurchaseClosingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0);
                                            $B64 = $HoldingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? array_sum($B16) : 0);
                                            $B65 = $FlipPoints = array_sum($B14);
                                            $B66 = $FlipOtherFinancingCosts = array_sum($B15);
                                            $B67 = $FlipLoanInterest = array_sum($B19)+array_sum($B22);
                                            $B68 = $FlipRehabInterest = array_sum($B29)+array_sum($B32);
                                            $B69 = ($B63+$B64+$B65+$B66+$B67+$B68);
                                            
                                            $G64 = $PurchaseFinanced;
                                            $G65 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0));
                                            $G66 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? array_sum($B16) : 0));
                                            $G67 = ($Property->PointsClosingUpfront=="Paid Backend" ? array_sum($B14)+array_sum($B15) : 0)+($Property->InterestPaymentsMade=="No" ? (array_sum($B22)+array_sum($B32)) : 0);
                                            $G68 = array_sum($B24);
                                            $G69 = ($G64+$G65+$G66+$G67+$G68);
                                            $G71 = $FundedByLenderFlip = $G64+$G65+$G66+$G68;
                                            
                                            $B71 = $Property->TotalLoanFlip;
                                            $B72 = $Property->CashRequiredFlip;
                                            $B73 = ($B71+$B72);
                                            
                                            $D73 = $B40-$B73;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B75[$i] = 0; // B75
                                                    $B76[$i] = 0; // B76
                                                    $B78[$i] = 0; // B78
                                                    $B79[$i] = 0; // B79
                                                } else {
                                                    $B75[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($Property->CashRequiredFlip==0 ? "Infinite" : $B47[$i]/$Property->CashRequiredFlip) : 0); // B75
                                                    $B76[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($B75[$i]=="Infinite" ? "Infinite" : $B75[$i]/($MonthsFlipRehab+$MonthsToSell)*12) : 0); // B76
                                                    $B78[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($Property->CashRequiredFlip==0 ? "Infinite" : $B49[$i]/$Property->CashRequiredFlip) : 0); // B78
                                                    $B79[$i] = ($i==($MonthsFlipRehab+$MonthsToSell) ? ($B78[$i]=="Infinite" ? "Inifinite" : $B78[$i]/($MonthsFlipRehab+$MonthsToSell)*12) : 0); // B79
                                                }
                                            }
                                            
                                            // Cashflows to Lender
                                            $B84 = -$PurchaseFinanced;
                                            $B85 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$Property->ClosingCosts : 0));
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B86[$i] = 0;
                                                    $B87[$i] = $B14[$i];
                                                    $B88[$i] = $B15[$i];
                                                    $B89[$i] = -$B24[$i];
                                                    $B90[$i] = 0;
                                                    $B91[$i] = 0;
                                                    $B92[$i] = 0;
                                                    $B93[$i] = 0;
                                                    $B94[$i] = $B35[$i];
                                                    $B95[$i] = 0;
                                                    $B96[$i] = 0;
                                                    
                                                    $B98[$i] = $B84+$B85+$B86[$i]+$B89[$i];
                                                    $B99[$i] = $B90[$i]+$B92[$i]+$B95[$i]+$B96[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B87[$i]+$B88[$i] : 0);
                                                    $B100[$i] = ($B98[$i]+$B99[$i]);
                                                } else {
                                                    $B86[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$B16[$i] : 0));
                                                    $B87[$i] = $B14[$i];
                                                    $B88[$i] = $B15[$i];
                                                    $B89[$i] = -$B24[$i];
                                                    $B90[$i] = $B19[$i];
                                                    $B91[$i] = $B20[$i];
                                                    $B92[$i] = $B29[$i];
                                                    $B93[$i] = $B30[$i];
                                                    $B94[$i] = $B35[$i];
                                                    $B95[$i] = $B45[$i];
                                                    $B96[$i] = $B48[$i];
                                                    
                                                    $B98[$i] = $B86[$i]+$B89[$i];
                                                    $B99[$i] = $B90[$i]+$B92[$i]+$B95[$i]+$B96[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B87[$i]+$B88[$i] : 0);
                                                    $B100[$i] = ($B98[$i]+$B99[$i]);
                                                }
                                            }
                                            $B101 = IRR($B100, 0.01);
                                            for ( $i=0; $i<12; $i++ ) {
                                                $IRRReslt *= ($B101+1);
                                            }
                                            $B102 = ($IRRReslt-1)*100;
                                            $C101 = $B101*12;
                                            $B103 = $TotalLenderInterestIncomeFlip = ($Property->InterestPaymentsMade=="No" ? array_sum($B22)+array_sum($B32) : array_sum($B19)+array_sum($B29));
                                            $B104 = $FeesToLenderFlip = array_sum($B14)+array_sum($B15);
                                            $B105 = array_sum($B48);
                                            $B106 = ($B103+$B104+$B105);
                                            
                                            $E104 = $B106/$G71;
                                            $E105 = $E104/($MonthsFlipRehab+$MonthsToSell)*12;
                                            
                                            $C106 = array_sum($B100)-$B106;
                                            
                                            $FinancingCostFlip = ($FlipPoints+$FlipOtherFinancingCosts+$FlipLoanInterest+$FlipRehabInterest);
                                            ?>
                                            <p class="text-center"><strong>FLIP CASH FLOWS TO LENDER</strong></p><hr /><br />
                                            <div class="row">
                                                <div class="col-lg-6 text-center">
                                                    <p>
                                                        <?=$FCL[0]->PropertyName?><br>
                                                        <strong><?=$FCL[0]->PropertyStreetAddress?></strong><br>
                                                        <strong><?=$FCL[0]->PropertyCityTown?>, <?=$FCL[0]->PropertyStateProvince?>, <?=$FCL[0]->PropertyZipCode?>, <?=$FCL[0]->PropertyCountry?></strong>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6 text-center">
                                                    <p>
                                                        <strong><?=$FCL[0]->YourName?></strong><br>
                                                        <?=$FCL[0]->CompanyName?><br>
                                                        <?=$FCL[0]->PhoneNumber?>
                                                    </p>
                                                </div>
                                            </div><br />
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <td>Points Offered to Lender:</td>
                                                            <td class="text-right">
                                                                <?=($FCL[0]->RehabDiscountPoints>0 ? round($FCL[0]->RehabDiscountPoints).'%' : "");?>
                                                            </td>
                                                            <td>Total Interest Income</td>
                                                            <td class="text-right"><?=number_format($TotalLenderInterestIncomeFlip)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Interest Rate Offered to Lender:</td>
                                                            <td class="text-right"><?=round($FCL[0]->RehabInterestRate)?>%</td>
                                                            <td>Points, Fees</td>
                                                            <td class="text-right"><?=number_format($FeesToLenderFlip)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><?=($FCL[0]->SplitWithLender=="Yes" ? "Profit Split to Lender:" : "")?></td>
                                                            <td class="text-right"><?=($FCL[0]->SplitWithLender=='Yes' ? round($FCL[0]->LenderSplit).'%' : "")?></td>
                                                            <td><?=($FCL[0]->SplitWithLender=="Yes" ? ($B105!="" ? "Profit Split" : "") : "")?></td>
                                                            <td class="text-right"><?=($B105==0 ? "" : number_format($B105))?></td>
                                                        </tr>
                                
                                                        <tr>
                                                            <td>Total Amount Funded By Lender:</td>
                                                            <td class="text-right"><?=number_format($FundedByLenderFlip)?></td>
                                                            <td>Total</td>
                                                            <td class="text-right"><?=number_format($B106)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td rowspan="2">Total Loan Amount (inc deferred):</td>
                                                            <td rowspan="2" class="text-right">
                                                                <?=($FCL[0]->PointsClosingUpfront=="Paid Backend" ? number_format($FCL[0]->TotalLoanFlip) : ($FCL[0]->InterestPaymentsMade=="No" ? number_format($FCL[0]->TotalLoanFlip) : ""))?>
                                                            </td>
                                                            <td>Cash-on-Cash Return (annual)</td>
                                                            <td class="text-right"><?=round($E105*100)?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>IRR (annualized)</td>
                                                            <td class="text-right"><?=($FCL[0]->FinancingUsed=="All Cash" ? "" : round($B102).'%')?>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered text-center">
                                                            <tr>
                                                                <th width="10%" class="text-left">Month</th>
                                                                <th width="10%" class="text-center">0</th>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <th width="10%" class="text-center"><?=$i?></th>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Purchase</td>
                                                                <td class="text-center"><?=number_format(-$PurchaseFinanced)?></td>
                                                                <td colspan="<?=($MonthsFlipRehab+$MonthsToSell)?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Purchase Closing Costs Funded</td>
                                                                <td class="text-center"><?=number_format(-$FCL[0]->ClosingCosts)?></td>
                                                                <td colspan="<?=($MonthsFlipRehab+$MonthsToSell)?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Holding Costs Funded</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B86[$i]==0 ? "" : number_format($B86[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Origination/Discount Points</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B87[$i]+$B88[$i]==0 ? "" : number_format($B87[$i]+$B88[$i]))?></td>
                                                                <?php } ?>
                                                            </tr> 
                                                            <tr>
                                                                <td class="text-left">Rehab Draws</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B89[$i]==0 ? "" : number_format($B89[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Interest on Loan (Paid)</td>
                                                                <td class="text-left"></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B90[$i]==0 ? "" : number_format($B90[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Interest on Loan (Deferred)</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B91[$i]==0 ? "" : number_format($B91[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Interest on Draws (Paid)</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B92[$i]==0 ? "" : number_format($B92[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Interest on Draws (Deferred)</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B93[$i]==0 ? "" : number_format($B93[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Total Loan Balance</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B94[$i]==0 ? "" : number_format(-$B94[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Payback of the Loan</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($i==$MonthsFlipRehab+$MonthsToSell ? number_format($B95[$i]) : "")?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Profit Split</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($FCL[0]->SplitWithLender=="Yes" ? ($B96[$i]==0 ? "" : number_format($B48[$i])) : "")?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Cashflows Out</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B98[$i]==0 ? "" : number_format($B98[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Cashflows In</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B99[$i]==0 ? "" : number_format($B99[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Net</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsFlipRehab+$MonthsToSell); $i++ ) { ?>
                                                                    <td class="text-center"><?=($B100[$i]==0 ? "" : number_format($B100[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                                                    
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['RefiCashFlowLender']) ) {
                                        $RCL = $print_data['SingleProposal']['RefiCashFlowLender']; ?>
                                            
                                        <div class="RefiCashFlowLender"><br />
                                            <?php 
                                            $Property = $RCL['Property'];
                                            $RefiBudget = $RCL['RefiBudget'];
                                            
                                            $MonthsRefiRehab = $Property->MonthsRefiRehab ? $Property->MonthsRefiRehab : 0;
                                            $MonthsToRefi = $Property->MonthsToRefi ? $Property->MonthsToRefi : 0;
                                            $RehabDiscountPoints = $Property->RehabDiscountPoints ? ($Property->RehabDiscountPoints/100) : 0;
                                            $ActualFinancedRent = $Property->ActualFinancedRent ? $Property->ActualFinancedRent : 0;
                                            $OtherPoints = $Property->OtherPoints ? ($Property->OtherPoints/100) : 0;
                                            $RefiHoldingCosts = $Property->RefiHoldingCosts ? $Property->RefiHoldingCosts : 0;
                                            $RehabInterestRate = $Property->RehabInterestRate ? ($Property->RehabInterestRate/100) : 0;
                                            $LumpSumRefiBudget = $Property->LumpSumRefiBudget ? $Property->LumpSumRefiBudget : 0;
                                            $DetailedRefiBudget = $Property->DetailedRefiBudget ? $Property->DetailedRefiBudget : 0; 
                                            $LenderSplit = $Property->LenderSplit ? ($Property->LenderSplit/100) : 0;
                                            
                                            $Row118Total = 0; $Row126Total = 0; $IRRReslt = 1;
                                            $MonthTotal = array();
                                            $MonthMatchArray = array();
                                            $RefiBudgetColumn = array(); $MonthPaidColumn = array();
                                            foreach ( $RefiBudget as $key => $refi ) {
                                                $RefiBudgetColumn[$key] = $refi->Budget;
                                                $MonthPaidColumn[$key] = $refi->MonthPaid;
                                            }
                                            
                                            $B2 = ($Property->FinancingUsed=="Financing" ? "Yes" : "No");
                                            $B3 = $Property->CapSelection;
                                            $B4 = $Property->FinancingCap ? ($Property->FinancingCap/100) : 0;
											
											$F2 = $Property->ARVFlip ? $Property->ARVFlip : 0;
											$F3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualFlipBudget ? $Property->ActualFlipBudget : 0);
											$F4 = ($Property->CapSelection=="ARV" ? $B4 * $F2 : $B4 * $F3);
											
											$B10 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
											$B11 = $PurchaseFinanced = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B10 : min($B10, $F4)) : 0);
                                            
                                            $I2 = $Property->ARVRent ? $Property->ARVRent : 0;
                                            $I3 = ($Property->PurchasePrice ? $Property->PurchasePrice : 0) + ($Property->ActualRefiBudget ? $Property->ActualRefiBudget : 0);
                                            $I4 = ($Property->CapSelection=="ARV" ? $B4 * $I2 : $B4 * $I3);
                                            
                                            // REFI
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B109[$i] = 1; // B109
                                                    $B110[$i] = ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B110
                                                    $B111[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B111
                                                } else {
                                                    $B109[$i] = ($i>$MonthsRefiRehab ? 0 : 1); // B109
                                                    $B110[$i] = ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B110
                                                    $B111[$i] = ($Property->FinancingUsed=="Financing" ? 1 : 0); // B111
                                                }
                                            }
                                            
                                            $B112 = $Property->PurchasePrice ? $Property->PurchasePrice : 0;
                                            $B113 = $PurchaseFinancedRefi = ($B2=="Yes" ? ($B3=="Cost" ? $B4*$B112 : min($B112, $I4)) : 0);
                                            $B114 = $B112-$B113;
                                            $B115 = $Property->ClosingCosts ? $Property->ClosingCosts : 0;
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B116[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($RehabDiscountPoints)*$ActualFinancedRent : 0); // B116
                                                    $B117[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($OtherPoints)*$ActualFinancedRent : 0); // B117
                                                    $B118[$i] = 0; // B118
                                                    $Row118Total = $Row118Total + $B118[$i];
                                                    $B119[$i] = $PurchaseFinanced + ($B115 + $B118[$i]) * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0); // B119
                                                    $B120[$i] = 0; // B120
                                                    $B121[$i] = 0; // B121
                                                    $B122[$i] = 0; // B122
                                                    $B123[$i] = 0; // B123
                                                    $B124[$i] = 0; // B124
                                                    $B125[$i] = ($Property->RefiRehabDrawSelection=="Fund Rehab at Closing" ? ($Property->RefiRehabBudgetMethod=="Quick Lump Sum" ? $LumpSumRefiBudget : $DetailedRefiBudget) : 0); // B125
                                                    $B126[$i] = ($B3=="Cost" ? $B125[$i] * $B4 : max(0, min($I4, $B125[$i]))) * $B111[$i]; // B126
                                                    $Row126Total = $Row126Total + $B126[$i];
                                                    $B127[$i] = $B125[$i] - $B126[$i]; // B127
                                                    $B128[$i] = ($B125[$i]==0 ? 0 : $B125[$i]) * $B110[$i]; // B128
                                                    $B129[$i] = $B126[$i]; // B129
                                                    $B130[$i] = 0; // B130
                                                    $B131[$i] = 0; // B131
                                                    $B132[$i] = 0; // B132
                                                    $B133[$i] = 0; // B133
                                                    $B134[$i] = 0; // B134
                                                    $B135[$i] = $B112+$B115+$B116[$i]+$B117[$i]+$B125[$i]; // B135
                                                    $B136[$i] = $PurchaseFinancedRefi+($Row126Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])); // B136
                                                    $B137[$i] = $PurchaseFinancedRefi+$Property->ClosingCosts*($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))+($Row126Total)+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])); // B137
                                                    $B138[$i] = $B114+($B115+$B118[$i])*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B120[$i]+$B127[$i]+$B131[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B116[$i]+$B117[$i]) : 0); // B138
                                                    $B139[$i] = $B135[$i]-($B137[$i]+$B138[$i]); // B139
                                                    $B140[$i] = $B114+$B115*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 :1))+$B116[$i]+$B117[$i]+$B118[$i]*($Property->FinanceClosingHolding=="Yes" ? 0 : 1)+$B121[$i]+$B127[$i]+$B131[$i]; // B140
                                                } else {
                                                    $B116[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($RehabDiscountPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($RehabDiscountPoints)*$ActualFinancedRent : 0); // B116
                                                    $B117[$i] = $Property->PointsClosingUpfront=="Paid Upfront" ? ($i==0 ? ($OtherPoints)*$ActualFinancedRent : 0) : ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($OtherPoints)*$ActualFinancedRent : 0); // B117				
                                                    $B118[$i] = $RefiHoldingCosts/($MonthsRefiRehab+$MonthsToRefi)*$B110[$i]; // B118
                                                    $Row118Total = $Row118Total + $B118[$i];
                                                    $B119[$i] = ($B119[$i-1] + $B118[$i] * ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))*$B110[$i]; // B119
                                                    $B120[$i] = $RehabInterestRate*$B119[$i-1] / 12 * ($i>($MonthsRefiRehab+$MonthsToRefi) ? 0 : 1); // B120
                                                    $B121[$i] = $Property->InterestPaymentsMade=="Yes" ? $B120[$i] : 0; // B121
                                                    $B122[$i] = $B120[$i] - $B121[$i]; // B122
                                                    if ( $i == 1 ) {
                                                        $B123[$i] = $B122[$i] + $B123[$i-1]; // B123
                                                    } else {
                                                        $B123[$i] = ($B122[$i]+$B123[$i-1]+$B123[$i-1]*($RehabInterestRate/12))*$B110[$i]; // B123
                                                    }
                                                    $B124[$i] = $B123[$i] * ($i == ($MonthsRefiRehab + $MonthsToRefi) ? 1 : 0); // B124
                                                    
                                                    $ColU = ($i<=$MonthsRefiRehab ? 1 : 0);
                                                    $MonthMatch = 0;
                                                    for ( $j = 0; $j < count($MonthPaidColumn); $j++ ) {
                                                        if ( $MonthPaidColumn[$j] == $i ) {
                                                            $MonthMatch += $RefiBudgetColumn[$j];
                                                        }
                                                    }
                                                    $MonthMatchArray[$i] = $MonthMatch;
                                                    $B125[$i] = $Property->RefiRehabDrawSelection=="Fund Rehab at Closing" ? 0 : ($Property->RefiRehabBudgetMethod=="Detailed Input" ? $MonthMatchArray[$i] * $ColU : $LumpSumFlipBudget / $MonthsRefiRehab * $ColU); // B125
                                                    $B126[$i] = ($B3=="Cost" ? $B125[$i] * $B4 : max(0, min($I4-$B137[$i-1], $B125[$i]))) * $B111[$i]; // B126
                                                    $B127[$i] = $B125[$i] - $B126[$i]; // B127
                                                    $B128[$i] = ($B125[$i]==0 ? $B128[$i-1] : $B125[$i] + $B128[$i-1]) * $B110[$i]; // B128
                                                    $B129[$i] = ($B126[$i]==0 ? $B129[$i-1] : $B126[$i] + $B129[$i-1]) * $B110[$i]; // B129
                                                    $B130[$i] = $B129[$i-1] * $RehabInterestRate/12 * $B110[$i] * $B111[$i]; // B130
                                                    $B131[$i] = $Property->InterestPaymentsMade=="Yes" ? $B130[$i] : 0; // B131
                                                    $B132[$i] = $B130[$i] - $B131[$i]; // B132
                                                    if ( $i == 1 ) {
                                                        $B133[$i] = $B132[$i] + $B133[$i-1]; // B133
                                                    } else {
                                                        $B133[$i] = ($B132[$i] + $B133[$i-1] + $B133[$i-1] * ($RehabInterestRate/12)) * $B110[$i]; // B133
                                                    }
                                                    $B134[$i] = $B133[$i] * ($i==($MonthsRefiRehab+$MonthsToRefi) ? 1 : 0); // B134
                                                    $B135[$i] = ($B135[$i-1]+($B116[$i]+$B117[$i]+$B118[$i])+$B121[$i]+$B122[$i]+$B123[$i-1]*$RehabInterestRate/12+$B125[$i]+$B131[$i]+$B132[$i]+$B133[$i-1]*$RehabInterestRate/12)*$B110[$i]; // B135
                                                    $B136[$i] = ($PurchaseFinancedRefi+$B129[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i])))*$B110[$i]; // B136
                                                    $B137[$i] = ($PurchaseFinancedRefi+($Property->ClosingCosts+($Row118Total))*($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? 1 : 0))+$B129[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? 0 : ($B116[$i]+$B117[$i]))+$B123[$i]+$B133[$i])*$B110[$i]; // B137
                                                    $B138[$i] = ($B118[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B121[$i]+$B127[$i]+$B131[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? ($B116[$i]+$B117[$i]) : 0)+$B138[$i-1])*$B110[$i]; // B138
                                                    $B139[$i] = $B135[$i]-($B137[$i]+$B138[$i]); // B139
                                                    $B140[$i] = $B118[$i]*($Property->FinancingUsed=="All Cash" ? 1 : ($Property->FinanceClosingHolding=="Yes" ? 0 : 1))+$B121[$i]+$B127[$i]+$B131[$i]; // B140
                                                }
                                            }
                                            
                                            $B141 = array_sum($B140)-max($B138);
                                            $B142 = max($B135);
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B144[$i] = 0; // B144
                                                    $B145[$i] = 0; // B145
                                                    $B145[$i] = 0; // B146
                                                    $B147[$i] = 0; // B147
                                                    $B149[$i] = 0; // B149
                                                    $B150[$i] = 0; // B150
                                                    $B151[$i] = 0; // B151
                                                    $B152[$i] = 0; // B152
                                                    $B153[$i] = 0; // B153
                                                    $B154[$i] = 0; // B154
                                                } else {
                                                    $B144[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->RefiAmount : 0); // B144
                                                    $B145[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? -($Property->RefiDiscountPoints/100)*$Property->RefiAmount : 0); // B145
                                                    $B146[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? -$B137[$i] : 0); // B146
                                                    $B147[$i] = ($B144[$i]+$B145[$i]+$B146[$i]); // B147
                                                    $B149[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? max(0, $B147[$i]-$Property->CashRequiredRent) : 0); // B149
                                                    $B150[$i] = 	($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->LenderSplit*($Property->FinancingUsed=="Financing" ? 1 : 0)*$B149[$i]*($Property->SplitWithLender=="Yes" ? 1 : 0) : 0); // B150
                                                    $B151[$i] = $B149[$i]-$B150[$i]; // B151
                                                    $B152[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "infinite" : $B149[$i]/$Property->CashRequiredRent/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B152
                                                    $B153[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? max(0, $B138[$i]-$B147[$i]) : 0); // B153
                                                    $B154[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? $Property->ARVRent-$Property->RefiAmount : 0); // B154
                                                }
                                            }
                                            
                                            $B158 = $Property->PurchasePrice;
                                            $B159 = $B115;
                                            $B160 = array_sum($B118);
                                            $B161 = array_sum($B116);
                                            $B162 = array_sum($B117);
                                            $B163 = array_sum($B125);
                                            $B164 = array_sum($B121)+array_sum($B124);
                                            $B165 = array_sum($B131)+array_sum($B134);
                                            $B167 = ($B158+$B159+$B160+$B161+$B162+$B163+$B164+$B165);
                                            
                                            $B169 = $RefiClosingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? $B115 : 0);
                                            $B170 = $RefiHoldingCostsFinanced = ($Property->FinanceClosingHolding=="Yes" ? array_sum($B118) : 0);
                                            $B171 = $RentPoints = array_sum($B116);
                                            $B172 = $RentOtherFinancingCosts = array_sum($B117);
                                            $B173 = $RentLoanInterest = array_sum($B121)+array_sum($B124);
                                            $B174 = $RentRehabInterest = array_sum($B131)+array_sum($B134);
                                            $B175 = ($B171+$B172+$B173+$B174);
                                            
                                            $G170 = $PurchaseFinanced;
                                            $G171 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? $Property->ClosingCosts : 0));
                                            $G172 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? array_sum($B118) : 0));
                                            $G173 = ($Property->PointsClosingUpfront=="Paid Backend" ? array_sum($B116)+array_sum($B117) : 0)+($Property->InterestPaymentsMade=="No" ? (array_sum($B124)+array_sum($B134)) : 0);
                                            $G174 = array_sum($B126);
                                            $G175 = ($G170+$G171+$G172+$G173+$G174);
                                            $G177 = $FundedByLenderREfi = $G170+$G171+$G172+$G174;
                                            
                                            $B177 = $Property->TotalLoanRent;
                                            $B178 = $Property->CashRequiredRent;
                                            $B179 = ($B177+$B178);
                                            
                                            $D179 = $B142-$B179;
                                            
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B181[$i] = 0; // B181
                                                    $B182[$i] = 0; // B182
                                                    $B184[$i] = 0; // B184
                                                    $B185[$i] = 0; // B185
                                                } else {
                                                    $B181[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "Infinite" : $B149[$i]/$Property->CashRequiredRent) : 0); // B181
                                                    $B182[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($B181[$i]=="Infinite" ? "Infinite" : $B181[$i]/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B182
                                                    $B184[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($Property->CashRequiredRent==0 ? "Infinite" : $B151[$i]/$Property->CashRequiredRent) : 0); // B184
                                                    $B185[$i] = ($i==($MonthsRefiRehab+$MonthsToRefi) ? ($B184[$i]=="Infinite" ? "Infinite" : $B184[$i]/($MonthsRefiRehab+$MonthsToRefi)*12) : 0); // B185
                                                }
                                            }
                                            
                                            // Cashflows to Lender
                                            $B190 = -$PurchaseFinancedRefi;
                                            $B191 = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$Property->ClosingCosts : 0));
                                            for ( $i = 0; $i <= 24; $i++ ) {
                                                if ( $i == 0 ) {
                                                    $B192[$i] = 0;
                                                    $B193[$i] = $B116[$i];
                                                    $B194[$i] = $B117[$i];
                                                    $B195[$i] = -$B126[$i];
                                                    $B196[$i] = 0;
                                                    $B197[$i] = 0;
                                                    $B198[$i] = 0;
                                                    $B199[$i] = 0;
                                                    $B200[$i] = $B137[$i];
                                                    $B201[$i] = 0;
                                                    $B202[$i] = 0;
                                                    
                                                    $B204[$i] = $B190+$B191+$B192[$i]+$B195[$i];
                                                    $B205[$i] = $B196[$i]+$B198[$i]+$B201[$i]+$B202[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B193[$i]+$B194[$i] : 0);
                                                    $B206[$i] = ($B204[$i]+$B205[$i]);
                                                } else {
                                                    $B192[$i] = ($Property->FinancingUsed=="All Cash" ? 0 : ($Property->FinanceClosingHolding=="Yes" ? -$B118[$i] : 0));
                                                    $B193[$i] = $B116[$i];
                                                    $B194[$i] = $B117[$i];
                                                    $B195[$i] = -$B126[$i];
                                                    $B196[$i] = $B121[$i];
                                                    $B197[$i] = $B122[$i];
                                                    $B198[$i] = $B131[$i];
                                                    $B199[$i] = $B132[$i];
                                                    $B200[$i] = $B137[$i];
                                                    $B201[$i] = -$B146[$i];
                                                    $B202[$i] = $B150[$i];
                                                    
                                                    $B204[$i] = $B192[$i]+$B195[$i];
                                                    $B205[$i] = $B196[$i]+$B198[$i]+$B201[$i]+$B202[$i]+($Property->PointsClosingUpfront=="Paid Upfront" ? $B193[$i]+$B194[$i] : 0);
                                                    $B206[$i] = ($B204[$i]+$B205[$i]);
                                                }
                                            }
                                            $B207 = IRR($B206, 0.01);
                                            for ( $i=0; $i<12; $i++ ) {
                                                $IRRReslt *= ($B207+1);
                                            }
                                            $B208 = ($IRRReslt-1)*100;
                                            $C207 = $B207*12;
                                            $B209 = $TotalLenderInterestIncomeRefi = ($Property->InterestPaymentsMade=="No" ? array_sum($B124)+array_sum($B134) : array_sum($B121)+array_sum($B131));
                                            $B210 = $FeesToLenderRefi = array_sum($B116)+array_sum($B117);
                                            $B211 = array_sum($B150);
                                            $B212 = ($B209+$B210+$B211);
                                            
                                            $E210 = $B212/$G177;
                                            $E211 = $E210/($MonthsRefiRehab+$MonthsToRefi)*12;
                                            
                                            $C212 = array_sum($B206)-$B212;
                                            
                                            $FinancingCostRent = $RentPoints+$RentOtherFinancingCosts+$RentLoanInterest+$RentRehabInterest;
                                            ?>
                                            <p class="text-center"><strong>REFI CASH FLOWS TO LENDER</strong></p><hr /><br />
                                            <div class="row">
                                                <div class="col-lg-6 text-center">
                                                    <p>
                                                        <?=$RCL[0]->PropertyName?><br>
                                                        <strong><?=$RCL[0]->PropertyStreetAddress?></strong><br>
                                                        <strong><?=$RCL[0]->PropertyCityTown?>, <?=$RCL[0]->PropertyStateProvince?>, <?=$RCL[0]->PropertyZipCode?>, <?=$RCL[0]->PropertyCountry?></strong>
                                                    </p>
                                                </div>
                                                <div class="col-lg-6 text-center">
                                                    <p>
                                                        <strong><?=$RCL[0]->YourName?></strong><br>
                                                        <?=$RCL[0]->CompanyName?><br>
                                                        <?=$RCL[0]->PhoneNumber?>
                                                    </p>
                                                </div>
                                            </div><br />
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <td>Points Offered to Lender:</td>
                                                            <td class="text-right"><?=($RCL[0]->RehabDiscountPoints>0 ? round($RCL[0]->RehabDiscountPoints).'%' : "")?></td>
                                                            <td>Total Interest Income</td>
                                                            <td class="text-right"><?=number_format($TotalLenderInterestIncomeRefi)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Interest Rate Offered to Lender:</td>
                                                            <td class="text-right"><?=round($RCL[0]->RehabInterestRate)?>%</td>
                                                            <td>Points, Fees</td>
                                                            <td class="text-right"><?=number_format($FeesToLenderRefi)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td><?=($RCL[0]->SplitWithLender=="Yes" ? "Profit Split to Lender:" : "")?></td>
                                                            <td class="text-right"><?=($RCL[0]->SplitWithLender=="Yes" ? round($RCL[0]->LenderSplit).'%' : "")?></td>
                                                            <td><?=($RCL[0]->SplitWithLender=="Yes" ? ($B211!="" ? "Profit Split" : "") : "")?></td>
                                                            <td class="text-right"><?=($B211==0 ? "" : number_format($B211))?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Total Amount Funded By Lender:</td>
                                                            <td class="text-right"><?=number_format($FundedByLenderREfi)?></td>
                                                            <td>Total</td>
                                                            <td class="text-right"><?=number_format($B212)?></td>
                                                        </tr>
                                                        <tr>
                                                            <td rowspan="2">Total Loan Amount (inc deferred):</td>
                                                            <td rowspan="2" class="text-right"><?=($RCL[0]->PointsClosingUpfront=="Paid Backend" ? number_format($RCL[0]->TotalLoanRent) : ($RCL[0]->InterestPaymentsMade=="No" ? number_format($RCL[0]->TotalLoanRent) : ""))?></td>
                                                            <td>Cash-on-Cash Return (annual)</td>
                                                            <td class="text-right"><?=round($E211*100)?>%</td>
                                                        </tr>
                                                        <tr>
                                                            <td>IRR (annualized)</td>
                                                            <td class="text-right"><?=($RCL[0]->FinancingUsed=="All Cash" ? "" : round($B208).'%')?></td>
                                                        </tr>
                                                    </table>
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered text-center">
                                                            <tr>
                                                                <th width="10%" class="text-left">Month</th>
                                                                <th width="10%" class="text-center">0</th>
                                                                <?php for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <th width="10%" class="text-center"><?=$i?></th>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Purchase</td>
                                                                <td class="text-center"><?=number_format(-$PurchaseFinancedRefi)?></td>
                                                                <td colspan="<?=($MonthsRefiRehab+$MonthsToRefi)?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Purchase Closing Costs Funded</td>
                                                                <td class="text-center"><?=number_format(-$RCL[0]->ClosingCosts)?></td>
                                                                <td colspan="<?=($MonthsRefiRehab+$MonthsToRefi)?>"></td>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Holding Costs Funded</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B192[$i]==0 ? "" : number_format($B192[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Origination/Discount Points</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B193[$i]+$B194[$i]==0 ? "" : number_format($B193[$i]+$B194[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Rehab Draws</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B195[$i]==0 ? "" : number_format($B195[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Interest on Loan (Paid)</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B196[$i]==0 ? "" : number_format($B196[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Interest on Loan (Deferred)</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B197[$i]==0 ? "" : number_format($B197[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Interest on Draws (Paid)</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B198[$i]==0 ? "" : number_format($B198[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Interest on Draws (Deferred)</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B199[$i]==0 ? "" : number_format($B199[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Total Loan Balance</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B200[$i]==0 ? "" : number_format(-$B200[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Payback of the Loan</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($i==$MonthsRefiRehab+$MonthsToRefi ? number_format($B201[$i]) : "")?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Profit Split from Cash-Out Refi</td>
                                                                <td></td>
                                                                <?php for ( $i = 1; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($RCL[0]->SplitWithLender=="Yes" ? ($B202[$i]==0 ? "" : number_format($B202[$i])) : "")?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Cashflows Out</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B204[$i]==0 ? "" : number_format($B204[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Cashflows In</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B205[$i]==0 ? "" : number_format($B205[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                            <tr>
                                                                <td class="text-left">Net</td>
                                                                <?php for ( $i = 0; $i <= ($MonthsRefiRehab+$MonthsToRefi); $i++ ) { ?>
                                                                    <td><?=($B206[$i]==0 ? "" : number_format($B206[$i]))?></td>
                                                                <?php } ?>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['CompsReport']) ) {
                                        $CS = $print_data['SingleProposal']['CompsReport']; ?>
                                            
                                        <div class="CompsReport"><br />
                                            <p class="text-center"><strong>Comparable Sales Report For <?=$CS[0]->PropertyStreetAddress?></strong></p><hr />
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <table class="table table-borderless" style="border:none;">
                                                        <tr>
                                                            <td>Property Street Address:</td>
                                                            <td class="text-center"><strong><?=$CS[0]->PropertyStreetAddress?></strong></td>
                                                            <td class="text-center"><strong><u>Presented By:</u></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Property City, State, Zip:</td>
                                                            <td class="text-center"><strong><?=$CS[0]->PropertyCityTown?>, <?=$CS[0]->PropertyStateProvince?>, <?=$CS[0]->PropertyZipCode?>, <?=$CS[0]->PropertyCountry?></strong></td>
                                                            <td class="text-center"><strong><?=$CS[0]->YourName?></strong></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Bedrooms:</td>
                                                            <td class="text-center"><?=$CS[0]->Bedrooms?></td>
                                                            <td class="text-center"><?=$CS[0]->CompanyName?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Baths:</td>
                                                            <td class="text-center"><?=$CS[0]->Bathrooms?></td>
                                                            <td class="text-center"><?=$CS[0]->PhoneNumber?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Square Feet:</td>
                                                            <td class="text-center"><?=$CS[0]->SquareFeet?></td>
                                                            <td class="text-center"><?=$CS[0]->Website?></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Year Built:</td>
                                                            <td class="text-center"><?=$CS[0]->YearBuilt?></td>
                                                            <td class="text-center"><?=$CS[0]->Email?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <table class="table table-bordered">
                                                        <tr>
                                                            <th></th>
                                                            <th>Address</th>
                                                            <th>Beds</th>
                                                            <th>Baths</th>
                                                            <th>Sq Ft</th>
                                                            <th>Date Sold</th>
                                                            <th>Sales Price</th>
                                                            <th>Notes</th>
                                                        </tr>
                                                        <?php for ( $i = 0; $i < count($CS); $i++ ) { ?>
                                                            <tr>
                                                                <td><?=$i+1?></td>
                                                                <td><?=$CS[$i]->SalesAddress?></td>
                                                                <td><?=$CS[$i]->Beds?></td>
                                                                <td><?=$CS[$i]->Baths?></td>
                                                                <td><?=$CS[$i]->SqFt?></td>
                                                                <td><?=$CS[$i]->DateSold?></td>
                                                                <td><?=$CS[$i]->SalesPrice?></td>
                                                                <td><?=$CS[$i]->Notes?></td>
                                                            </tr>
                                                        <?php } ?>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                        
                                    <?php } ?>
                                
									<?php if ( isset($print_data['SingleProposal']['PicsPage']) ) {
                                        $AP = $print_data['SingleProposal']['PicsPage']; ?>
                                        
                                        <?php if ( !empty($AP->Photos) ) { ?>
                                        
                                            <div class="PicsPage"><br />
                                                <p class="text-center"><strong><?php echo $AP->PropertyStreetAddress . ", " . $AP->PropertyCityTown; ?>, <?=$AP->PropertyStateProvince?>, <?=$AP->PropertyZipCode?>, <?=$AP->PropertyCountry?></strong></p><hr /><br />
                                                <div class="row">
                                                    <?php 
                                                    $photos = explode('|', $AP->Photos);
                                                    for ( $i = 0; $i < count($photos); $i++ ) { ?>
                                                        
                                                        <div class="col-lg-4 text-center">
                                                            <img src="<?=$photos[$i]?>" width="100%" class="thumbnail" alt="" />
                                                        </div>
                                                        
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            
                                        <?php } ?>
                                    <?php } ?>
                            		<div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</section>