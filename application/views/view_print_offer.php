<div class="MaxOffers">
    <h3 class="text-center"><strong>MAX OFFER REPORT</strong></h3><hr />
    <div class="row">
	    <div class="col-lg-6 col-lg-offset-3">
            <table class="table table-bordered">
                <tr>
                    <td>&nbsp;</td>
                    <td class="text-center"><strong>Scenario/Property</strong></td>
                </tr>
                <tr>
                    <td>Address/Name:</td>
                    <td class="text-center"><?=$print_data['MaxOffers']->address_name?></td>
                </tr>
                <tr>
                    <td>ARV (After-Repair Value):</td>
                    <td class="text-center">$<?=number_format($print_data['MaxOffers']->avr, 2)?></td>
                </tr>
                <tr>
                    <td>Max % of ARV:</td>
                    <td class="text-center"><?=round($print_data['MaxOffers']->max_avr_percent)?>%</td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td>Repairs:</td>
                    <td class="text-center">$<?=number_format($print_data['MaxOffers']->repairs, 2)?></td>
                </tr>
                <tr>
                    <td>Closing Costs (%):</td>
                    <td class="text-center"><?=round($print_data['MaxOffers']->closing_costs)?>%</td>
                </tr>
                <tr>
                    <td>Holding Costs:</td>
                    <td class="text-center">$<?=number_format($print_data['MaxOffers']->holding_costs, 2)?></td>
                </tr>
                <tr>
                    <td>Other Expenses:</td>
                    <td class="text-center">$<?=number_format($print_data['MaxOffers']->other_expenses, 2)?></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td>Wholesale Profit:</td>
                    <td class="text-center">$<?=number_format($print_data['MaxOffers']->wholesale_profit, 2)?></td>
                </tr>
                <tr>
                    <td>Max Offer:</td>
                    <td class="text-center">$<?=number_format($print_data['MaxOffers']->max_offer, 2)?></td>
                </tr>
                <tr>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="2"><strong><u>Rental Rate of Return</u></strong></td>
                </tr>
                <tr>
                    <td>End-Buyer Cost Basis:</td>
                    <td class="text-center">$<?=number_format($print_data['MaxOffers']->end_buyer_cost_basis, 2)?></td>
                </tr>
                <tr>
                    <td>Projected Monthly Income:</td>
                    <td class="text-center">$<?=number_format($print_data['MaxOffers']->projected_monthly_income, 2)?></td>
                </tr>
                <tr>
                    <td>Projected Monthly Expenses:</td>
                    <td class="text-center">$<?=number_format($print_data['MaxOffers']->projected_monthly_expenses, 2)?></td>
                </tr>
                <tr>
                    <td>Cap Rate (Rate of Return):</td>
                    <td class="text-center"><?=round($print_data['MaxOffers']->cap_rate)?>%</td>
                </tr>
            </table>
        </div>
    </div>
</div>