<?php if ( $error ) { ?><style>.edit-field{display:block !important;}.profile-data{display:none !important;}</style><?php } ?>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
		                <h2>My Account</h2>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    	<?php if ( $user->user_type == 'investor' ) { ?>
							<?php if ( !$profile ) { ?>
                                <!-- No action needed -->
                            <?php } elseif ( !$portfolio ) { ?>
                                <a href="<?=base_url('pages/new-portfolio')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to create your first portfolio."><i class="fa fa-pencil-square-o"></i> Create Portfolio</a>
                            <?php } elseif ( !$proposal ) { ?>
                                <a href="<?=base_url('pages/proposal-writer')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to post your first proposal."><i class="fa fa-pencil-square-o"></i> Create Proposal</a>
                            <?php } else { ?>
                                <!-- No action needed -->
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                
                <div class="page-content">
                	<?php if ( $user->user_type == 'investor' ) { ?>
						<?php if ( !$profile ) { ?>
                            <div class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-info-circle"></i>&nbsp;&nbsp;Please update your profile details below to get the access.&nbsp;&nbsp;&nbsp; 
                                <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Video Tutorial">
                                	<span data-target="#profile_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                </a>
                            </div>
                        <?php } elseif ( !$portfolio ) { ?>
                            <div class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-info-circle"></i>&nbsp;&nbsp;Now you have to create your portfolio. For creating your first portfolio click the above '<a href="<?=base_url('pages/new-portfolio')?>/"><u>Create Portfolio</u></a>' botton.&nbsp;&nbsp;&nbsp;
                                <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Video Tutorial">
                                	<span data-target="#portfolio_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                </a>
                            </div>
                        <?php } elseif ( !$proposal ) { ?>
                            <div class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-info-circle"></i>&nbsp;&nbsp;Now you have to create your proposal. For creating your first proposal click the above '<a href="<?=base_url('pages/proposal-writer')?>/"><u>Create Proposal</u></a>' botton.&nbsp;&nbsp;&nbsp;
                                <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Video Tutorial">
                                	<span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                </a>
                            </div>
                        <?php } else { ?>
                            <!-- No action needed -->
                        <?php } ?>
                    <?php } ?>
                    
					<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-user"></i> Account Settings</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#account_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                    <h3 class="box-title pull-right">
                                        <?php if ( $user && !$error ) { ?>
                                            <a id="cancel_profile" class="hide action-btn" data-toggle="tooltip" data-placement="top" title="Cancel">
                                                <i class="fa fa-close"></i> Cancel
                                            </a>
                                            <a id="edit_profile" class="action-btn" data-toggle="tooltip" data-placement="top" title="Edit Account">
                                                <i class="fa fa-pencil-square-o"></i> Edit
                                            </a>
                                        <?php } ?>
                                    </h3>
                                </div>
                                <div class="box-body">
									<?=form_open_multipart(base_url('pages/accounts') . '/', array('autocomplete' => 'off'))?>
                            			<div class="row">
                                        	<?php if ( $success ) { ?>
                                            	<div class="col-xs-12">
                                                    <div class="alert alert-success" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <i class="fa fa-info-circle"></i> <?=$success?>
                                                    </div>
                                                </div>
                                            <?php } elseif ( $error ) { ?>
                                            	<div class="col-xs-12">
                                                    <div class="alert alert-danger" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <i class="fa fa-info-circle"></i> Please correct the following field(s) below.
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="col-xs-3 profile-pic">
                                            	<div class="pic-inner <?=($image_error ? ' InputError' : '')?>">
	                                                <img class="thumbnail" id="profile_pic" src="<?=$user->picture_medium ? $user->picture_medium : base_url('assets/images/default-pic.jpg')?>" alt="picture" />
                                                    <div class="edit-profile">
                                                        <div class="edit-field" <?=($user->picture_medium) ? 'style="display:none;"' : 'style="display:block;"'?>>
                                                            <?php
                                                            $picture = array(
                                                                'name'  => 'picture',
                                                                'id'    => 'picture',
                                                                'class'	=> 'form-control file-input',
                                                                'title' => 'Select profile image',
                                                            ); ?>
                                                            <?=form_upload($picture)?>
                                                            <a href="#" class="edit-pic"><i class="fa fa-pencil-square-o"></i> Edit Picture or Logo</a><br />
                                                            <font size="1">File formats gif, jpg, png, bmp | Max file size 1 MB</font>
                                                        </div>
                                                        <?=($image_error) ? '<span class="error">' . $image_error . '</span>' : ''?>
                                                    </div>
                                                </div>
                                                <hr /><br />
                                                <div class="profile-scale text-left">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">Profile Strength</div>
                                                        <div class="panel-body">
                                                            <p><?=$completeness?>% Profile Completeness</p>
                                                            <?php 
                                                            if ( $completeness >= 75 && $completeness <= 100 ) {
                                                                $class = 'progress-bar-success';
                                                            } elseif ( $completeness >= 50 && $completeness < 75 ) {
                                                                $class = 'progress-bar-info';
                                                            } elseif ( $completeness >= 25 && $completeness < 50 ) {
                                                                $class = 'progress-bar-warning';
                                                            } elseif ( $completeness >= 0 && $completeness < 25 ) {
                                                                $class = 'progress-bar-danger';
                                                            }
                                                            ?>
                                                            <div class="progress">
                                                                <div class="progress-bar <?=$class?>" role="progressbar" aria-valuenow="<?=$completeness?>" aria-valuemin="0" aria-valuemax="100" style="width:<?=$completeness?>%;"><?=$completeness?>%</div>
                                                            </div><br />
                                                            <p><strong><u>Complete Your Profile</u></strong></p>
                                                            <?php if ( $completeness < 100 ) { ?>
                                                                <ul class="quick-complete">
                                                                    <?php if ( $user->user_type == 'investor' ) { ?>
                                                                        <?php if ( $user->dob == 'Not updated' || $user->phone == 'Not updated' || $user->street == 'Not updated' || $user->town == 'Not updated' || $user->state == 'Not updated' || $user->zip == 'Not updated' || $user->country == 'Not updated' || $user->linkedin_url == 'Not updated' || $user->facebook_url == 'Not updated' || $user->twitter_url == 'Not updated' ) { ?>
                                                                            <li><a href="<?=base_url('pages/accounts')?>/">Update Your Account</a></li>
                                                                        <?php } ?>
                                                                        <?php if ( strpos($user->picture, 'default-pic.jpg') !== false ) { ?>
                                                                            <li><a href="#">Upload Profile Picture</a></li>
                                                                        <?php } ?>
                                                                        <?php if ( $profile == false ) { ?>
                                                                            <li><a href="<?=base_url('pages/profile')?>/">Update Your Profile</a></li>
                                                                        <?php } ?>                                                    
                                                                        <?php if ( $portfolio == false ) { ?>
                                                                            <li><a href="<?=base_url('pages/new-portfolio')?>/">Create Your Portfolio</a></li>
                                                                        <?php } ?>
                                                                        <?php if ( $proposal == false ) { ?>
                                                                            <li><a href="<?=base_url('pages/proposal-writer')?>/">Post Your Proposal</a></li>
                                                                        <?php } ?>
                                                                    <?php } elseif ( $user->user_type == 'cash_lender' ) { ?>
                                                                        <?php if ( $user->dob == 'Not updated' || $user->phone == 'Not updated' || $user->street == 'Not updated' || $user->town == 'Not updated' || $user->state == 'Not updated' || $user->zip == 'Not updated' || $user->country == 'Not updated' || $user->linkedin_url == 'Not updated' || $user->facebook_url == 'Not updated' || $user->twitter_url == 'Not updated' ) { ?>
                                                                            <li><a href="<?=base_url('pages/accounts')?>/">Update Your Account</a></li>
                                                                        <?php } ?>
                                                                        <?php if ( strpos($user->picture, 'default-pic.jpg') !== false ) { ?>
                                                                            <li><a href="#">Upload Profile Picture</a></li>
                                                                        <?php } ?>
                                                                        <?php if ( $profile == false ) { ?>
                                                                            <li><a href="<?=base_url('pages/profile')?>/">Update Your Profile</a></li>
                                                                        <?php } ?>                                                    
                                                                    <?php } ?>
                                                                </ul>
                                                            <?php } elseif ( $completeness == 100 ) { ?>
                                                                <p>Your Profile is 100% Complete.</p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>                                    
                                            </div>
                                            <div class="col-xs-9">
                                                <div class="fields-panel">
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('fname'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Name: <span class="required">*</span> </label>
                                                            <?php 
                                                            $fname = array(
                                                                'name'  => 'fname',
                                                                'id'    => 'fname',
                                                                'value' => isset($user->fname) ? $user->fname : set_value('fname'),
                                                                'class'	=> 'form-control',
                                                            ); ?>
                                                            <?=form_input($fname)?>
                                                            <?=form_error('fname', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Name: </label>
                                                            <p><?=$user->fname?></p>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('dob'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Date of Birth: <span class="required">*</span> </label>
                                                            <?php 
                                                            $dob = array(
                                                                'name'  => 'dob',
                                                                'id'    => 'dob',
                                                                'value' => isset($user->dob) ? $user->dob : set_value('dob'),
                                                                'class'	=> 'form-control',
                                                                'data-provide' => 'datepicker',
                                                                'data-date-format' => 'mm-dd-yyyy',
                                                            ); ?>
                                                            <div class="input-group" id="datetimepicker7">
                                                                <span class="input-group-addon datepickerbutton">
                                                                    <i class="glyphicon glyphicon-calendar"></i>
                                                                </span>
                                                                <?=form_input($dob)?>
                                                            </div>
                                                            <?=form_error('dob', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Date of Birth: </label>
                                                            <p><?=$user->dob?></p>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('email'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Email Address: <em class="small">You can not change your email address.</em></label>
                                                            <?php 
                                                            $email = array(
                                                                'name'  => 'email',
                                                                'id'    => 'email',
                                                                'value' => isset($user->email) ? $user->email : set_value('email'),
                                                                'class'	=> 'form-control',
                                                                'disabled' => 'true',
                                                            ); ?>
                                                            <?=form_input($email)?>
                                                            <?=form_error('email', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Email Address: <em class="small">You cannot change your email address.</em></label>
                                                            <p><?=$user->email?></p>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('password'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Password: 
                                                                <?php if ( $user->password == 'linkedin' ) { ?>
                                                                    <em class="small">You cannot change or use your password. Your profile is registered by Linkedin.</em>
                                                                <?php } else { ?>
                                                                    <em class="small">If you would like to change the password type a new one. Otherwise leave this blank.</em>
                                                                <?php } ?>
                                                            </label>
                                                            <?php 
                                                            if ( $user->password == 'linkedin' ) {
                                                                $password = array(
                                                                    'name'  => 'password_linkedin',
                                                                    'id'    => 'password_linkedin',
                                                                    'value' => 'Your account is registered by Linkedin, so you have no password.',
                                                                    'class'	=> 'form-control',
                                                                    'disabled' => 'true',
                                                                ); ?>
                                                                <?=form_input($password)?>
                                                            <?php
                                                            } else {
                                                                $password = array(
                                                                    'name'  => 'password',
                                                                    'id'    => 'password',
                                                                    'class'	=> 'form-control',
                                                                );
                                                                $passwordC = array(
                                                                    'name'  => 'passwordC',
                                                                    'id'    => 'passwordC',
                                                                    'class'	=> 'form-control',
                                                                ); ?>
                                                                <?=form_password($password)?>
                                                                <label>Retype Password: </label>
                                                                <?=form_password($passwordC)?>
                                                                <?=form_error('password', '<span class="error">', '</span>')?>
                                                            <?php 
                                                            } ?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Password: 
                                                                <?php if ( $user->password == 'linkedin' ) { ?>
                                                                    <em class="small">You cannot change or use your password. Your profile is registered by Linkedin.</em>
                                                                <?php } else { ?>
                                                                    <em class="small">If you would like to change the password type a new one. Otherwise leave this blank.</em>
                                                                <?php } ?>
                                                            </label>
                                                            <?php if ( $user->password == 'linkedin' ) { ?>
                                                                <p>Your account is registered by Linkedin, so you have no password.</p>
                                                            <?php } else { ?>
                                                                <p>XXXXXXXXXXXX</p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('phone'); ?>
                                                        <div class="edit-field<?=((!empty($err) || $phone_number_error) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Phone Number: </label>
                                                            <?php 
                                                            $phone = array(
                                                                'name'  => 'phone',
                                                                'id'    => 'phone',
                                                                'value' => isset($user->phone) ? $user->phone : set_value('phone'),
                                                                'class'	=> 'form-control',
                                                            ); ?>
                                                            <?=form_input($phone)?>
                                                            <?=form_error('phone', '<span class="error">', '</span>')?>
                                                            <?=($phone_number_error) ? '<span class="error">' . $phone_number_error . '</span>' : ''?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Phone Number: </label>
                                                            <p><?=$user->phone?></p>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('street'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Street: <span class="required">*</span> </label>
                                                            <?php 
                                                            $street = array(
                                                                'name'  => 'street',
                                                                'id'    => 'street',
                                                                'value' => isset($user->street) ? $user->street : set_value('street'),
                                                                'class'	=> 'form-control',
                                                            ); ?>
                                                            <?=form_input($street)?>
                                                            <?=form_error('street', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Street: </label>
                                                            <p><?=$user->street?></p>
                                                        </div>
                                                    </div>
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('town'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Town: <span class="required">*</span> </label>
                                                            <?php 
                                                            $town = array(
                                                                'name'  => 'town',
                                                                'id'    => 'town',
                                                                'value' => isset($user->town) ? $user->town : set_value('town'),
                                                                'class'	=> 'form-control',
                                                            ); ?>
                                                            <?=form_input($town)?>
                                                            <?=form_error('town', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Town: </label>
                                                            <p><?=$user->town?></p>
                                                        </div>
                                                    </div>
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('state'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>State: <span class="required">*</span> </label>
                                                            <?php 
                                                            $state = array(
                                                                'name'  => 'state',
                                                                'id'    => 'state',
                                                                'value' => isset($user->state) ? $user->state : set_value('state'),
                                                                'class'	=> 'form-control',
                                                            ); ?>
                                                            <?=form_input($state)?>
                                                            <?=form_error('state', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>State: </label>
                                                            <p><?=$user->state?></p>
                                                        </div>
                                                    </div>
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('zip'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>ZIP: <span class="required">*</span> </label>
                                                            <?php 
                                                            $zip = array(
                                                                'name'  => 'zip',
                                                                'id'    => 'zip',
                                                                'value' => isset($user->zip) ? $user->zip : set_value('zip'),
                                                                'class'	=> 'form-control',
                                                            ); ?>
                                                            <?=form_input($zip)?>
                                                            <?=form_error('zip', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>ZIP: </label>
                                                            <p><?=$user->zip?></p>
                                                        </div>
                                                    </div>
                                                    
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('country'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Country: <span class="required">*</span> </label>
                                                            <?=country_select('country', 'country', 'form-control', $user->country, array('US', 'CA'), '')?>
                                                            <?=form_error('country', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Country: </label>
                                                            <p><?=$countries[$user->country]?></p>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('linkedin_url'); ?>
                                                        <div class="edit-field<?=((!empty($err) || $linked_in_url_error) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Linkedin URL: </label>
                                                            <?php 
                                                            $linkedin_url = array(
                                                                'name'  => 'linkedin_url',
                                                                'id'    => 'linkedin_url',
                                                                'value' => isset($user->linkedin_url) ? $user->linkedin_url : set_value('linkedin_url'),
                                                                'class'	=> 'form-control',
                                                            ); ?>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">www.</div>
                                                                <?=form_input($linkedin_url)?>																				
                                                            </div>
                                                            <?=form_error('linkedin_url', '<span class="error">', '</span>')?>
                                                            <?=($linked_in_url_error) ? '<span class="error">' . $linked_in_url_error . '</span>' : ''?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Linkedin URL: </label>
                                                            <p>
																<?php if($user->linkedin_url!="Not updated"){ ?>
                                                                	<a href="https://www.<?=$user->linkedin_url?>" target="_blank">
																		www.<?=$user->linkedin_url?>
                                                                    </a>
																<?php } else {
																	echo $user->linkedin_url;
																} ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('facebook_url'); ?>
                                                        <div class="edit-field<?=((!empty($err) || $facebook_url_error) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Facebook URL: </label>
                                                            <?php 
                                                            $facebook_url = array(
                                                                'name'  => 'facebook_url',
                                                                'id'    => 'facebook_url',
                                                                'value' => isset($user->facebook_url) ? $user->facebook_url : set_value('facebook_url'),
                                                                'class'	=> 'form-control',
                                                            ); ?>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">www.</div>
                                                                <?=form_input($facebook_url)?>																				
                                                            </div>
                                                            <?=form_error('facebook_url', '<span class="error">', '</span>')?>
                                                            <?=($facebook_url_error) ? '<span class="error">' . $facebook_url_error . '</span>' : ''?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Facebook URL: </label>
                                                            <p>
																<?php if($user->facebook_url!="Not updated"){  ?>
                                                            		<a href="https://www.<?=$user->facebook_url?>" target="_blank">
																		www.<?=$user->facebook_url?>
                                                                    </a>
																<?php } else {
																	echo $user->facebook_url;
																} ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('twitter_url'); ?>
                                                        <div class="edit-field<?=((!empty($err) || $twitter_url_error) ? ' InputError' : '')?>" style="display:none;">
                                                            <label>Twitter URL: </label>
                                                            <?php 
                                                            $twitter_url = array(
                                                                'name'  => 'twitter_url',
                                                                'id'    => 'twitter_url',
                                                                'value' => isset($user->twitter_url) ? $user->twitter_url : set_value('twitter_url'),
                                                                'class'	=> 'form-control',
                                                            ); ?>
                                                            <div class="input-group">
                                                                <div class="input-group-addon">www.</div>
                                                                <?=form_input($twitter_url)?>																				
                                                            </div>
                                                            <?=form_error('twitter_url', '<span class="error">', '</span>')?>
                                                            <?=($twitter_url_error) ? '<span class="error">' . $twitter_url_error . '</span>' : ''?>
                                                        </div>
                                                        <div class="profile-data">
                                                            <label>Twitter URL: </label>
                                                            <p>
																<?php if($user->twitter_url!="Not updated"){ ?>
                                                                	<a href="https://www.<?=$user->twitter_url?>" target="_blank">
																		www.<?=$user->twitter_url?>
                                                                    </a>
																<?php } else {
																	echo $user->twitter_url; 
																} ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                        <div class="edit-field" style="display:none;">
                                                            <?php
                                                            $data = array(
                                                                'name' 		=> 'update_profile',
                                                                'id' 		=> 'update_profile',
                                                                'value' 	=> 'true',
                                                                'type' 		=> 'submit',
                                                                'content' 	=> 'Submit',
                                                                'class'		=> 'btn btn-primary submit_profile',
                                                            ); ?>					
                                                            <?=form_button($data)?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?=form_close()?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>