<style type="text/css">
h3, h4 {
	margin-top:0;
}
.table {
	font-size:12px;
}
.table.table-bg {
	background:#F7F4F4;
}
.table.table-bordered td, .table.table-bordered th {
	border: 1px solid #ddd !important;
} 
</style>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <h2>OFFER PRICE</h2>
                    </div>
                </div>
                
                <div class="page-content">
                	<div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-file-text-o"></i> Max Offer Price Report</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#portfolio_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
                                    <div class="col-lg-8 col-lg-offset-2">
                                        <div class="row">
                                            <table class="table table-bordered">
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="text-center"><strong>Scenario/Property</strong></td>
                                                </tr>
                                                <tr>
                                                    <td>Address/Name:</td>
                                                    <td class="text-center"><?=$print_data['SingleOffer']->address_name?></td>
                                                </tr>
                                                <tr>
                                                    <td>ARV (After-Repair Value):</td>
                                                    <td class="text-center">$<?=number_format($print_data['SingleOffer']->avr, 2)?></td>
                                                </tr>
                                                <tr>
                                                    <td>Max % of ARV:</td>
                                                    <td class="text-center"><?=round($print_data['SingleOffer']->max_avr_percent)?>%</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>Repairs:</td>
                                                    <td class="text-center">$<?=number_format($print_data['SingleOffer']->repairs, 2)?></td>
                                                </tr>
                                                <tr>
                                                    <td>Closing Costs (%):</td>
                                                    <td class="text-center"><?=round($print_data['SingleOffer']->closing_costs)?>%</td>
                                                </tr>
                                                <tr>
                                                    <td>Holding Costs:</td>
                                                    <td class="text-center">$<?=number_format($print_data['SingleOffer']->holding_costs, 2)?></td>
                                                </tr>
                                                <tr>
                                                    <td>Other Expenses:</td>
                                                    <td class="text-center">$<?=number_format($print_data['SingleOffer']->other_expenses, 2)?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td>Wholesale Profit:</td>
                                                    <td class="text-center">$<?=number_format($print_data['SingleOffer']->wholesale_profit, 2)?></td>
                                                </tr>
                                                <tr>
                                                    <td>Max Offer:</td>
                                                    <td class="text-center">$<?=number_format($print_data['SingleOffer']->max_offer, 2)?></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2"><strong><u>Rental Rate of Return</u></strong></td>
                                                </tr>
                                                <tr>
                                                    <td>End-Buyer Cost Basis:</td>
                                                    <td class="text-center">$<?=number_format($print_data['SingleOffer']->end_buyer_cost_basis, 2)?></td>
                                                </tr>
                                                <tr>
                                                    <td>Projected Monthly Income:</td>
                                                    <td class="text-center">$<?=number_format($print_data['SingleOffer']->projected_monthly_income, 2)?></td>
                                                </tr>
                                                <tr>
                                                    <td>Projected Monthly Expenses:</td>
                                                    <td class="text-center">$<?=number_format($print_data['SingleOffer']->projected_monthly_expenses, 2)?></td>
                                                </tr>
                                                <tr>
                                                    <td>Cap Rate (Rate of Return):</td>
                                                    <td class="text-center"><?=round($print_data['SingleOffer']->cap_rate)?>%</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                   	<div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </aside>
        </div>
    </div>
</section>