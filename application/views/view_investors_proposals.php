<section id="wrapper">
    <div class="container-fluid">
        <div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>Proposals in Underwriting</h2>
                    </div>
                </div>
                
                <div class="page-content">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-file-picture-o"></i> Porposal Lists</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <p id="search_filter"><i class="fa fa-filter"></i> Filter</p>
                                    </h3>
                                </div>
                                <div class="box-body">
                                    <div class="container-fluid filter-form" style="display:none;" id="filter_body">
                                        <form method="post" class="form-horizontal" id="proposal_filter_form">
                                            <div class="col-xs-6">
                                                <div class="form-group">
                                                    <label for="InvestorLocation" class="control-label col-xs-6">Location of Real Estate Investor</label>
                                                    <div class="col-xs-6">
                                                        <input type="text" name="InvestorLocation" id="InvestorLocation" placeholder="Location" value="<?=$InvestorLocation ? $InvestorLocation : ''?>" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="LoanValue" class="control-label col-xs-6">Loan to value ($0 - $1000)</label>
                                                    <div class="col-xs-6">
                                                        <p class="price-range-slider">
                                                            <input id="LoanValue" name="LoanValue" type="text" class="filter-price-range" data-slider-min="0" data-slider-max="1000" data-slider-step="1" data-slider-value="[<?=$LoanValue ? $LoanValue : '0,250'?>]"/>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                            	<div class="form-group">
                                                    <label for="PropertyLocation" class="control-label col-xs-6">Location of Property</label>
                                                    <div class="col-xs-6">
                                                        <input type="text" name="PropertyLocation" id="PropertyLocation" placeholder="Location" value="<?=$PropertyLocation ? $PropertyLocation : ''?>" class="form-control" />
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="RepairValue" class="control-label col-xs-6">After repair value ($0 - $1000)</label>
                                                    <div class="col-xs-6">
                                                        <p class="price-range-slider">
                                                            <input id="RepairValue" name="RepairValue" type="text" class="filter-price-range" data-slider-min="0" data-slider-max="1000" data-slider-step="1" data-slider-value="[<?=$RepairValue ? $RepairValue : '0,500'?>]"/>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-12">
                                                <div class="form-group text-center">
                                                    <button type="submit" class="btn btn-primary" name="filter_submit" id="filter_submit">
                                                        &nbsp;&nbsp;Search&nbsp;&nbsp;
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="relative" id="proposal_results">
                                        <div class="filter-loader"></div>
                                        <?php if ( $proposals ) { $i = 0; ?>
                                            <?php foreach ( $proposals as $k => $port ) {
                                                if ( $i % 2 == 0 ) { $bg = '#F8F8F8'; } else { $bg = ''; }
                                                if ( isset($port->InfoID) ) { ?>
                                                
                                                    <div class="container-fluid portfolio-row" style="background-color:<?php echo $bg; ?>;">
                                                        <div class="col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-5">
                                                                    <p><strong><?=$port->PropertyName?></strong></p>
                                                                    
																<p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?=$port->PropertyStateProvince?></p>
																<p>Total Loan Amount &nbsp;&nbsp;<i class="fa fa-dollar"></i>
															<?=($port->FlipOrRefi == "Flip"? $port->TotalLoanFlip : $port->TotalLoanRent)?></p>
                                                                    <p>
                                                                        <a href="<?=base_url('pages/proposal/' . $port->PropertyID . '/' . strtolower(str_ireplace(' ', '-', $port->PropertyName)))?>/"><i class="fa fa-caret-square-o-right"></i> Read More</a>
                                                                    </p>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <p><strong>Description</strong></p>
                                                                    <p><?=$port->PropertyDescription?></p>
                                                                </div>
                                                                <div class="col-xs-1 text-right">
                                                                    <a href="<?=base_url('pages/profiles/' . $port->UserID . '/' . strtolower(str_ireplace(' ', '-', $port->fname)))?>/"><img style="border:2px solid #6B6B6B;" class="img-circle" width="50" height="50" src="<?=isset($port->picture_small) ? $port->picture_small : base_url('assets/images/default-pic-small.jpg')?>" alt="<?=$port->fname?>" title="Posted by <?=$port->fname?>" /></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="container-fluid portfolio-row" style="background-color:<?php echo $bg; ?>;">
                                                        <div class="col-xs-12">
                                                            <div class="row">
                                                                <div class="col-xs-5">
                                                                    <p><strong><?=$port->PropertyName?></strong></p>
                                                                    <p><i class="fa fa-map-marker"></i>&nbsp;&nbsp;<?=$port->PropertyStreetAddress?>, <?=$port->PropertyCityTown?>, <?=$port->PropertyStateProvince?>, <?=$port->PropertyZipCode?>, <?=$port->PropertyCountry?></p>
                                                                    <p>
                                                                        <a href="<?=base_url('pages/rental/' . $port->PropertyID . '/' . strtolower(str_ireplace(' ', '-', $port->PropertyName)))?>/"><i class="fa fa-caret-square-o-right"></i> Read More</a>
                                                                    </p>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <!--<p><strong>Description</strong></p>
                                                                    <p><?=$port->PropertyDescription?></p>-->
                                                                </div>
                                                                <div class="col-xs-1 text-right">
                                                                    <a href="<?=base_url('pages/profiles/' . $port->UserID . '/' . strtolower(str_ireplace(' ', '-', $port->fname)))?>/"><img style="border:2px solid #6B6B6B;" class="img-circle" width="50" height="50" src="<?=isset($port->picture_small) ? $port->picture_small : base_url('assets/images/default-pic-small.jpg')?>" alt="<?=$port->fname?>" title="Posted by <?=$port->fname?>" /></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } ?>                                            
                                            <?php $i++; } ?>
                                        <?php } else { ?>
                                            <p>No proposals are available.</p>
                                        <?php } ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>