<header id="header">
	<div class="container">
    	<div class="row">
			<nav class="navbar">
            	<div class="container-fluid">
                	<div class="navbar-header">
                    	<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        	<span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <h1 class="home-logo">
                        	<a class="navbar-brand" href="<?=base_url()?>">
                        		<img src="<?=base_url('assets/images/logo.png')?>" alt="CBList" width="200" />
                        	</a>
                        </h1>
                    </div>
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                        	<li <?php if($this->uri->segment(2) == ''):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url()?>">Home</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'aboutus'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/aboutus')?>/">About Us</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'blog'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/blog')?>/">Blog</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'pricing'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/pricing')?>/">Pricing</a>
                            </li>
                            <li <?php if($this->uri->segment(2) == 'features'):?> class="active" <?php else:?> <?php endif;?>>
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </li>
                            <?php if ( $user_id ) { ?>
                            	<li>
                                    <a href="<?=base_url('pages/profile')?>/">Profile</a>
                                </li>
                                <li>
                                    <a href="<?=base_url('pages/logout')?>/">Logout</a>
                                </li>
                            <?php } else { ?>
                                <li <?php if($this->uri->segment(2) == 'investors-login'):?> class="active" <?php else:?> <?php endif;?>>
                                    <a href="<?=base_url('pages/investors-login')?>/">Investors Login</a>
                                </li>
                                <li <?php if($this->uri->segment(2) == 'cashlenders-login'):?> class="active" <?php else:?> <?php endif;?>>
                                    <a href="<?=base_url('pages/cashlenders-login')?>/">Cash Lenders Login</a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </div>
</header>

<section class="container">
	<div class="row">
        <div class="col-xs-12 text-center">
            <div class="row">
                <div class="logo">
                    <h1>OPPS, Error 404!</h1>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-2"></div>
                <div class="col-xs-8">
                	<form method="post">
                        <div class="input-group">
                            <input type="text" placeholder="Write Your Message" class="form-control" name="message" required />
                            <span class="input-group-btn">
                                <button class="btn btn-danger" name="submit" type="submit">Contact Support</button>
                            </span>
                        </div>
                        <?php if ( $success ) { ?>
                        	<div class="alert alert-success" role="alert" style="margin-top:10px;">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-info-circle"></i> <?=$success?>
                            </div>
                        <?php } elseif ( $error ) { ?>
                        	<div class="alert alert-danger" role="alert" style="margin-top:10px;">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-info-circle"></i> <?=$error?>
                            </div>
                        <?php } ?>
                    </form>
                </div>
                <div class="col-xs-2"></div>
                <div class="clearfix"></div>
                <br /><br />
                <p class="text-muted">We are Sorry! <br />There is some error here, Please try later.</p>
                <div class="clearfix"></div>
                <br /><br />
                <div class="col-xs-3"></div>
                <div class="col-xs-6">
                    <div class="btn-group btn-group-justified">
                    	<?php if ( $user_id ) { ?>
							<?php if ( $user->user_type == 'investor' ) { ?>
                                <?php if ( $profile && $portfolio ) { ?>
                                    <a href="<?=base_url('pages/dashboard')?>/" class="btn btn-primary">Back to Dashboard</a>
                                <?php } else { ?>
                                    <a href="<?=base_url('pages/profile')?>/" class="btn btn-primary">Back to Profile</a>
                                <?php } ?>
                            <?php } else { ?>
                                <a href="<?=base_url('pages/profile')?>/" class="btn btn-primary">Back to Profile</a>
                            <?php } ?>
	                        <a href="<?=base_url()?>" class="btn btn-success">Back to Home</a> 
                        <?php } else { ?>
                        	<a href="<?=base_url()?>" class="btn btn-success">Return to Home Page</a> 
                        <?php } ?>
                    </div>
                </div>
                <div class="col-xs-3"></div>
            </div><br /><br /><br />
        </div>
    </div>
</section>

<footer>
	<div class="container">
        <div class="row">
            <div class="col-xs-6">
                <p class="text-left">
                    <a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                    <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                    <a href="<?=base_url('pages/features')?>/">Features</a>
                </p>
            </div>
            <div class="col-xs-6">
                <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
            </div>
        </div>
	</div>
</footer>