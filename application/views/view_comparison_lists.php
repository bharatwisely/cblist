<style type="text/css">.portfolio-desc-sec p{margin:0;}.property-list{float:left;width:20%;}.property-info{float:left;width:100%;}</style>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
		                <h2>Rental Property Comparison Analyser</h2>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    	<?php if ( !$proposal ) { ?>
                        	<a href="<?=base_url('pages/proposal-writer')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to post your first proposal."><i class="fa fa-plus-square-o"></i> Create Proposal</a>
                        <?php } else { ?>
                        	<!-- No action needed -->
                        <?php } ?>
                    </div>
                </div>
                
                <div class="page-content">
                	<?php if ( !$proposal ) { ?>
                        <div class="alert alert-warning" role="alert">
                        	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-info-circle"></i> Now you have to create your proposals. For creating your first proposal click the above '<a href="<?=base_url('pages/proposal-writer')?>/"><u>Create Proposal</u></a>' botton.&nbsp;&nbsp;&nbsp; 
                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Proposal Tutorial">
                                <span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                            </a>
                        </div>
                    <?php } else { ?>
                        <!-- No action needed -->
                    <?php } ?>
                    
                    <div class="row">
                        <div class="col-xs-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-file-text-o"></i> Rental Property Comparison Analyser List</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#comparison_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                    <h3 class="box-title pull-right">
                                        <a href="<?=base_url('pages/comparison-analyser')?>/" class="action-btn" data-toggle="tooltip" data-placement="top" title="New Comparison">
                                        	<i class="fa fa-plus-square-o"></i> New
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
									<?php if ( $this->session->userdata('comparison_deleted') ) { ?>
                                        <div class="alert alert-success" role="alert" style="margin-bottom:10px;">
                                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-info-circle"></i> Property Comparison deleted!
                                        </div>
                                    <?php } ?>
									<?php if ( $comparison ) { $i = 0; ?>
                                        <?php foreach ( $comparison as $public ) {
                                            if ( $i % 2 == 0 ) { $bg = '#F8F8F8'; } else { $bg = ''; }
											if ( $public->ProposalStatus == 'publish' ) { ?>
                                                <div class="container-fluid proposal-row" style="background-color:<?php echo $bg; ?>;">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="portfolio-desc-sec">
                                                                <div class="col-xs-9">
                                                                    <p class="property-list">
                                                                        <span class="property-info"><?=!empty($public->P1Address) ? $public->P1Address : ''?></span>
                                                                        <span class="property-info">Price: 
                                                                        	<strong><?=!empty($public->P1Price) ? '$' . number_format($public->P1Price, 2) : ''?></strong>
                                                                        </span>
                                                                        <span class="property-info">REQ: 
                                                                        	<strong>??</strong>
                                                                        </span>
                                                                        <span class="property-info">NOI: 
                                                                        	<strong><?=!empty($public->P1NOI) ? '$' . number_format($public->P1NOI, 2) : ''?></strong>
                                                                        </span>
                                                                    </p>
                                                                    <p class="property-list">
                                                                        <span class="property-info"><?=!empty($public->P2Address) ? $public->P2Address : ''?></span>
                                                                        <span class="property-info">Price: 
                                                                        	<strong><?=!empty($public->P2Price) ? '$' . number_format($public->P2Price, 2) : ''?></strong>
                                                                        </span>
                                                                        <span class="property-info">REQ: 
                                                                        	<strong>??</strong>
                                                                        </span>
                                                                        <span class="property-info">NOI: 
                                                                        	<strong><?=!empty($public->P2NOI) ? '$' . number_format($public->P2NOI, 2) : ''?></strong>
                                                                        </span>
                                                                    </p>
                                                                    <p class="property-list">
                                                                        <span class="property-info"><?=!empty($public->P3Address) ? $public->P3Address : ''?></span>
                                                                        <span class="property-info">Price: 
                                                                        	<strong><?=!empty($public->P3Price) ? '$' . number_format($public->P3Price, 2) : ''?></strong>
                                                                        </span>
                                                                        <span class="property-info">REQ: 
                                                                        	<strong>??</strong>
                                                                        </span>
                                                                        <span class="property-info">NOI: 
                                                                        	<strong><?=!empty($public->P3NOI) ? '$' . number_format($public->P3NOI, 2) : ''?></strong>
                                                                        </span>
                                                                    </p>
                                                                    <p class="property-list">
                                                                    	<span class="property-info"><?=!empty($public->P4Address) ? $public->P4Address : ''?></span>
                                                                        <span class="property-info">Price: 
                                                                        	<strong><?=!empty($public->P4Price) ? '$' . number_format($public->P4Price, 2) : ''?></strong>
                                                                        </span>
                                                                        <span class="property-info">REQ: 
                                                                        	<strong>??</strong>
                                                                        </span>
                                                                        <span class="property-info">NOI: 
                                                                        	<strong><?=!empty($public->P4NOI) ? '$' . number_format($public->P4NOI, 2) : ''?></strong>
                                                                        </span>
                                                                    </p>
                                                                    <p class="property-list">
                                                                    	<span class="property-info"><?=!empty($public->P5Address) ? $public->P5Address : ''?></span>
                                                                        <span class="property-info">Price: 
                                                                        	<strong><?=!empty($public->P5Price) ? '$' . number_format($public->P5Price, 2) : ''?></strong>
                                                                        </span>
                                                                        <span class="property-info">REQ: 
                                                                        	<strong>??</strong>
                                                                        </span>
                                                                        <span class="property-info">NOI: 
                                                                        	<strong><?=!empty($public->P5NOI) ?  '$' . number_format($public->P5NOI, 2) : ''?></strong>
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <?php $propertyId = $public->PropertyID; ?>
                                                                <div class="col-xs-3 text-right">
                                                                    <a class="action-btn" href="<?=base_url('pages/edit-comparison/' . $propertyId . '/Analyser')?>/" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                        <button type="button" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
                                                                    </a>
                                                                    <a class="action-btn" href="<?=base_url('pages/print-report/' . $propertyId . '/Analyser')?>/" data-toggle="tooltip" data-placement="top" title="Reports" target="_blank">
                                                                        <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></button>
                                                                    </a>
                                                                    <a class="action-btn" onclick="return confirm('Are you want to delete this comparison result?');" href="<?=base_url('pages/delete-comparison/' . $propertyId . '/Analyser')?>/" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                        <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php } else { ?>
												<div class="container-fluid proposal-row" style="background-color:<?php echo $bg; ?>;">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="portfolio-desc-sec">
                                                                <div class="col-xs-9">
                                                                    <p class="property-list">
                                                                        <span class="property-info"><?=!empty($public->P1Address) ? $public->P1Address : ''?></span>
                                                                        <span class="property-info">Price: 
                                                                        	<strong><?=!empty($public->P1Price) ? '$' . number_format($public->P1Price, 2) : ''?></strong>
                                                                        </span>
                                                                        <span class="property-info">REQ: 
                                                                        	<strong>??</strong>
                                                                        </span>
                                                                        <span class="property-info">NOI: 
                                                                        	<strong><?=!empty($public->P1NOI) ? '$' . number_format($public->P1NOI, 2) : ''?></strong>
                                                                        </span>
                                                                    </p>
                                                                    <p class="property-list">
                                                                        <span class="property-info"><?=!empty($public->P2Address) ? $public->P2Address : ''?></span>
                                                                        <span class="property-info">Price: 
                                                                        	<strong><?=!empty($public->P2Price) ? '$' . number_format($public->P2Price, 2) : ''?></strong>
                                                                        </span>
                                                                        <span class="property-info">REQ: 
                                                                        	<strong>??</strong>
                                                                        </span>
                                                                        <span class="property-info">NOI: 
                                                                        	<strong><?=!empty($public->P2NOI) ? '$' . number_format($public->P2NOI, 2) : ''?></strong>
                                                                        </span>
                                                                    </p>
                                                                    <p class="property-list">
                                                                        <span class="property-info"><?=!empty($public->P3Address) ? $public->P3Address : ''?></span>
                                                                        <span class="property-info">Price: 
                                                                        	<strong><?=!empty($public->P3Price) ? '$' . number_format($public->P3Price, 2) : ''?></strong>
                                                                        </span>
                                                                        <span class="property-info">REQ: 
                                                                        	<strong>??</strong>
                                                                        </span>
                                                                        <span class="property-info">NOI: 
                                                                        	<strong><?=!empty($public->P3NOI) ? '$' . number_format($public->P3NOI, 2) : ''?></strong>
                                                                        </span>
                                                                    </p>
                                                                    <p class="property-list">
                                                                        <span class="property-info"><?=!empty($public->P4Address) ? $public->P4Address : ''?></span>
                                                                        <span class="property-info">Price: 
                                                                        	<strong><?=!empty($public->P4Price) ? '$' . number_format($public->P4Price, 2) : ''?></strong>
                                                                        </span>
                                                                        <span class="property-info">REQ: 
                                                                        	<strong>??</strong>
                                                                        </span>
                                                                        <span class="property-info">NOI: 
                                                                        	<strong><?=!empty($public->P4NOI) ? '$' . number_format($public->P4NOI, 2) : ''?></strong>
                                                                        </span>
                                                                    </p>
                                                                    <p class="property-list">
                                                                        <span class="property-info"><?=!empty($public->P5Address) ? $public->P5Address : ''?></span>
                                                                        <span class="property-info">Price: 
                                                                        	<strong><?=!empty($public->P5Price) ? '$' . number_format($public->P5Price, 2) : ''?></strong>
                                                                        </span>
                                                                        <span class="property-info">REQ: 
                                                                        	<strong>??</strong>
                                                                        </span>
                                                                        <span class="property-info">NOI: 
                                                                        	<strong><?=!empty($public->P5NOI) ? '$' . number_format($public->P5NOI, 2) : ''?></strong>
                                                                        </span>
                                                                    </p>
                                                                </div>
                                                                <?php $propertyId = $public->PropertyID; ?>
                                                                <div class="col-xs-3 text-right">
                                                                	<a class="label label-primary" style="margin:0 1px;display:inline-block">Draft Proposal</a>
                                                                    <a class="action-btn" href="<?=base_url('pages/edit-comparison/' . $propertyId . '/Analyser')?>/" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                        <button type="button" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
                                                                    </a>
                                                                    <a class="action-btn" onclick="return confirm('Are you want to delete this comparison result?');" href="<?=base_url('pages/delete-comparison/' . $propertyId . '/Analyser')?>/" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                        <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
										<?php } $i++; } ?>
                                    <?php } else { ?>
                                        <p>You have not compare any property yet. User comparison analyser to compare your properties from <a href="<?=base_url('pages/comparison-analyser')?>/">here</a>.</p>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>