<style type="text/css">
.fields-panel h3{font-size:18px;text-align:center;}
.fields-panel h4{text-align:center;}
.assump-box{background:transparent;padding:10px 15px;margin-bottom:10px;}
.assump-box-child{background:none;z-index:1;position:relative;}
.panel-field{margin-bottom:5px;}
.panel-field button {font-size: 14px;font-weight: normal;margin-bottom: 0;white-space: normal;}
.panel-field input, .panel-field select{margin-bottom:0;padding:5px;border:none;font-size:16px;border:1px solid #007C30;background:#CEEAD9;box-shadow:none !important;-moz-box-shadow:none !important;-webkit-box-shadow:none !important;}
.panel-field input[readonly]{color:#000;}
.panel-field label{margin-bottom:0;font-weight:normal;font-size:18px;color:#222;}
.form-inline .panel-field .input-group{width:100%;}
.form-inline .panel-field .form-control{width:100%;box-shadow:none;}
.form-inline .panel-field .input-group .input-group-addon{width:20px;}
hr{margin-bottom:10px;border-top:1px solid #ccc;}
.input-group-addon {background-color:#007C30;border:1px solid #007C30;border-radius:0;padding:5px;color:#fff;}
.input-group .form-control {border:1px solid #007C30;}

.panel-field:nth-child(odd) .form-control {background-color:#c6eefd;border:1px solid #2a6c84;}
.panel-field:nth-child(even) .form-control {background:#ceead9;border:1px solid #007c30;}
.panel-field:nth-child(odd) .input-group-addon {background-color:#2a6c84;border:1px solid #2a6c84;}
.panel-field:nth-child(even) .input-group-addon {background-color:#007c30;border:1px solid #007c30;}
.topButtons{top:-55px;}
</style>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <h2>Offer Price Calculator</h2>
                    </div>
                </div>
                
                <div class="page-content">
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-calculator"></i> Max Offer Calculator</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#calculator_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                    <h3 class="box-title col-xs-6 col-xs-offset-2">
                                        <div class="progress">
                                            <div class="progress-bar" data-transitiongoal="0"></div>
                                        </div>
                                    </h3>
                                </div>
                                <div class="box-body">
                                	<div class="offer-calc-page">
                                        <div class="col-xs-12">
                                            <?php if ( $success ) { ?>
                                                <div class="alert alert-success" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                    <i class="fa fa-info-circle"></i> <?=$success?>
                                                </div>
                                            <?php } ?>
                                            
                                            <input type="hidden" id="page_type" value="MAX OFFER" />
                                            <div class="fields-panel flex-box offer-calculator-screen">
                                                <div class="col-xs-8 col-xs-offset-2 flex-box-child">
                                                    <div class="col-xs-12 assump-box" id="OfferCalculator">
                                                    
                                                        <div class="full-section-box">
                                                            <div class="panel-field">
                                                                <h3>SCENARIO/PROPERTY</h3><hr />
                                                            </div>
                                                            <div class="assump-box-child"><!-- STEP (1) -->
                                                            
                                                                <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                                <?=form_open(base_url('pages/offer-calculator') . '/', $attributes)?>
                                                                
                                                                    <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                    <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                    
                                                                    <div class="panel-field topButtons">
                                                                        <img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide loading" />
                                                                        <button type="button" class="btn btn-sm btn-primary on_save_event trigger-bar" data-rel="100">SAVE</button>
                                                                    </div><!-- PRINT REPORT Button -->
                                                                    
                                                                    <div class="panel-field" id="address_name_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">Address/Name:</label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "address_name",
                                                                                    'id'    => "address_name",
                                                                                    'value' => ($reset) ? "" : set_value("address_name"),
                                                                                    'class'  => 'form-control',
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"This is optional - enter the name of the property in question or the address.  You can alternatively enter the name of scenario in question.",
                                                                                ); ?>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Address/Name -->
                                                                    <div class="panel-field" id="avr_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">ARV (After-Repair Value):</label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "avr",
                                                                                    'id'    => "avr",
                                                                                    'value' => ($reset) ? "" : set_value("avr"),
                                                                                    'class'  => "form-control calc_input number-field NumberOnly",
                                                                                    "data-popover" => "true",
                                                                                    "data-content" => "ARV stands for 'After-Repair Value'.  This is the projected value/appraisal of the property after all renovations are completed by you or your end-buyer",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- ARV (After-Repair Value) -->
                                                                    <div class="panel-field" id="max_avr_percent_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">Max % of ARV:</label>
                                                                                <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "max_avr_percent",
                                                                                    'id'    => "max_avr_percent",
                                                                                    'value' => 70,
                                                                                    'class'  => "form-control calc_input number-field NumberOnly percentageField",
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"Enter the typical max % of ARV that a lender will loan on for this property.  This used to calculate the max that your wholesale buyer can pay after  after all repairs, closing/holding costs, other expenses and wholesale fee"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <hr />
                                                                    </div><!-- Max % of ARV -->
                
                                                                    <div class="panel-field" id="repairs_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">Repairs:</label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "repairs",
                                                                                    'id'    => "repairs",
                                                                                    'value' => ($reset) ? "" : set_value("repairs"),
                                                                                    'class'  => "form-control calc_input number-field NumberOnly",
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"Projected repair budget for the property.  Be mindful of that this is the one of the most important and risky drivers of profit in a deal.",
                                                                                ); ?>
                                                                                 <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                 </div>                                                          
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Repairs -->
                                                                    <div class="panel-field" id="closing_costs_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">Closing Costs (%):</label>	
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "closing_costs",
                                                                                    'id'    => "closing_costs",
                                                                                    'value' => ($reset) ? "" : set_value("closing_costs"),
                                                                                    'class'  => "form-control calc_input number-field NumberOnly percentageField",
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"Projected closing costs for you (if you are planning to buy and rehab) or for your wholesale buyer.  Enter this as % of purchase price (Offer + Wholesale Fee, if any)"
                                                                                ); ?>
                                                                                 <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                 </div>    
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Closing Costs (%) -->
                                                                    <div class="panel-field" id="holding_costs_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">Holding Costs: </label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "holding_costs",
                                                                                    'id'    => "holding_costs",
                                                                                    'value' => ($reset) ? "" : set_value("holding_costs"),
                                                                                    'class'  => "form-control calc_input number-field NumberOnly",
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"Projected holding costs for you or your wholesale buyer during renovations and the time it takes to sell the property",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>                     
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Closing Costs (%) -->
                                                                    <div class="panel-field" id="other_expenses_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">Other Expenses:</label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "other_expenses",
                                                                                    'id'    => "other_expenses",
                                                                                    'value' => ($reset) ? "" : set_value("other_expenses"),
                                                                                    'class'  => "form-control calc_input number-field NumberOnly",
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"Any additional expenses you can foresee"
                                                                                ); ?>
                                                                                 <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                 </div>                     
                                                                            </div>
                                                                        </div>
                                                                        <hr />
                                                                    </div><!-- Other Expenses -->
                
                                                                    <div class="panel-field" id="wholesale_profit_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">Wholesale Profit:</label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "wholesale_profit",
                                                                                    'id'    => "wholesale_profit",
                                                                                    'value' => ($reset) ? "" : set_value("wholesale_profit"),
                                                                                    'class'  => "form-control calc_input number-field NumberOnly",
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"Your desired wholesale profit.  If you are buying the property for yourself to rehab, leave this blank",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>                     
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Wholesale Profit -->
                                                                    <div class="panel-field" id="max_offer_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8"><strong>Max Offer</strong></label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "max_offer",
                                                                                    'id'    => "max_offer",
                                                                                    'value' => ($reset) ? "" : set_value("max_offer"),
                                                                                    'class'  => "form-control calc_input number-field",
                                                                                    'readonly' => 'readonly',
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"This is the maximum offer you can make given your inputs above",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>                     
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="panel-field text-right">
                                                                        <input type="hidden" name="CreatedOn" value="<?=date('Y-m-d H:i:s')?>" />
                                                                        <input type="hidden" name="TableName" value="max_offer" />
                                                                    </div>
                                                                
                                                                <?=form_close()?>
                                                                
                                                                <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                                <?=form_open(base_url('pages/offer-calculator') . '/', $attributes)?>
                                                                
                                                                    <hr />
                                                                    <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                    <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                    
																	<div class="panel-field" >
																	<div class="row">
																	<label class="col-xs-8"><strong>Rental Scenario</strong></label>
																	</div>
																	</div>
                                                                    <div class="panel-field" id="end_buyer_cost_basis_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">End-Buyer Cost Basis:</label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "end_buyer_cost_basis",
                                                                                    'id'    => "end_buyer_cost_basis",
                                                                                    'value' => ($reset) ? "" : set_value("end_buyer_cost_basis"),
                                                                                    'class'  => "form-control calc_input number-field",
                                                                                    'readonly' => 'readonly',
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"Tis is a calculated amount based on the ARV and the Max % of ARV entered above.",
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>                     
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- End-Buyer Cost Basis -->
                                                                    <div class="panel-field" id="projected_monthly_income_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">Projected Monthly Income:</label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "projected_monthly_income",
                                                                                    'id'    => "projected_monthly_income",
                                                                                    'value' => ($reset) ? "" : set_value("projected_monthly_income"),
                                                                                    'class'  => "form-control calc_input number-field NumberOnly",
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"Enter projected monthly rental income of the property"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>                     
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Projected Monthly Income -->
                                                                    <div class="panel-field" id="projected_monthly_expenses_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">Projected Monthly Expenses:</label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "projected_monthly_expenses",
                                                                                    'id'    => "projected_monthly_expenses",
                                                                                    'value' => ($reset) ? "" : set_value("projected_monthly_expenses"),
                                                                                    'class'  => "form-control calc_input number-field NumberOnly",
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"Enter projected monthly operating expenses including property taxes, insurance, maintenance, etc.  If not sure, you can use the 50% rule for ballpark (50% * Income)"
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <div class="input-group-addon">$</div>
                                                                                    <?=form_input($field)?>
                                                                                </div>                     
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Projected Monthly Expenses: -->
                                                                    <div class="panel-field" id="cap_rate_row">
                                                                        <div class="row">
                                                                            <label class="col-xs-8">Cap Rate (Rate of Return):</label>
                                                                            <div class="col-xs-4">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "cap_rate",
                                                                                    'id'    => "cap_rate",
                                                                                    'value' => ($reset) ? "" : set_value("cap_rate"),
                                                                                    'class'  => "form-control calc_input number-field percentageField",
                                                                                    'readonly' => 'readonly',
                                                                                    "data-popover"=>"true",
                                                                                    "data-content"=>"This will show you the Capitalization Rate of the property, which for an all-cash deal is the same thing as Cash-on-Cash Return."
                                                                                ); ?>
                                                                                <div class="input-group">
                                                                                    <?=form_input($field)?>
                                                                                    <div class="input-group-addon">%</div>
                                                                                </div>                     
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Cap Rate (Rate of Return): -->
                                                                    
                                                                    <div class="panel-field text-right">
                                                                        <hr />
                                                                        <img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide loading" />
                                                                        <button type="button" class="btn btn-sm btn-primary on_save_event trigger-bar" data-rel="100">SAVE</button>
                                                                        <input type="hidden" name="CreatedOn" value="<?=date('Y-m-d H:i:s')?>" />
                                                                        <input type="hidden" name="TableName" value="offer_rate" />
                                                                    </div><!-- PRINT REPORT Button -->
                                                                    
                                                                <?=form_close()?>
                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>