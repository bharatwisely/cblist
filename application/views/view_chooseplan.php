<input type="hidden" id="paymentchoose_plan" value="true" />
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
		                <h2>Choose Plan</h2>
                    </div>
                </div>
            	<div class="page-content">
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-user"></i> Plans</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#payment_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
                                	<div id="response_demo" style="display:none;"></div>
                        			<?=form_open(base_url('pages/payment') . '/', array('autocomplete' => 'off', 'class' => 'form-inline choosePlan-form'))?>
                                        <div class="fields-panel">
                                            <div class="panel-field">
                                                <div class="edit-field choosePlan-edit row">
                                                    <div class="col-xs-10 col-xs-offset-2">
                                                        <p>Please choose a yearly plan below: </p>
                                                    </div>
                                                    
                                                    <div class="col-xs-4 col-xs-offset-2">
                                                        <?php 
                                                        $min_amount = array(
                                                            'name'  => 'choose_amount',
                                                            'id'    => 'min_amount',
                                                            'value' => 97,
                                                            'class'	=> 'form-control',
                                                        ); 
                                                        ?>
                                                        <?php if($user->paidtype == "N") {?>
                                                        <span id="upgrade_payment">
                                                            <p class="pull-left">
                                                                <?=form_radio($min_amount,'min_amount',set_radio('choose_amount', '97', TRUE)); ?>
                                                                <label for="min_amount" class="">$ 97/Year</label>&nbsp;
                                                            </p>
                                                            <?php
                                                            $max_amount = array(
                                                                'name'  => 'choose_amount',
                                                                'id'    => 'max_amount',
                                                                'value' => 297,
                                                                'class'	=> 'form-control',
                                                            );												
                                                            ?>
                                                            <p class="pull-left">
                                                                <?=form_radio($max_amount,'max_amount',set_radio('choose_amount', '297'));?>
                                                                <label for="max_amount" class="">$ 297/Year</label>
                                                            </p>
                                                        </span>
                                                        <?php } elseif($user->paidtype == "M") {?>
                                                            <span id="upgraded_payment_display">
                                                                <label>You are already a paid user with our Basic Package ($97). As your request for upgrade account to Premium Package, the price will be $297</label>
                                                                <input type="hidden" name="choose_amount" value="297" />
                                                            </span>
                                                        <?php } ?>
                                                        <div class="clearfix"></div>
                                                        <p>
                                                            <?php
                                                            $data = array(
                                                                'name' 		=> 'submit_plan',
                                                                'id' 		=> 'submit_plan',
                                                                'value' 	=> 'true',
                                                                'content' 	=> 'Submit',
                                                                'class'		=> 'btn btn-primary submit_plan',
                                                                'onclick'   => 'paypal_pay()'
                                                            ); ?>					
                                                            <?=form_button($data)?>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
			                        <?=form_close()?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
					</div>
				</div>  
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>