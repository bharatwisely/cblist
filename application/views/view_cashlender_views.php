<style type="text/css">.portfolio-row p{margin:0;}</style>
<section id="wrapper">
    <div class="container-fluid">
        <div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
                <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <h2>Leadbox</h2>
                    </div>
                </div>
                
                <div class="page-content">
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-briefcase"></i> Leadbox</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#portfolio_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
                                    <div id="cashlender_list">
                                        <?php if ( $cashlender_proposal_views ) { $i = 0; ?>
                                            <?php foreach ( $cashlender_proposal_views as $k => $port ) {
                                                if ( $i % 2 == 0 ) { $bg = '#F8F8F8'; } else { $bg = ''; } ?>
                                                <div class="container-fluid portfolio-row" style="background-color:<?php echo $bg; ?>;">
                                                    <div class="col-xs-10">
                                                        <p><strong><?=$port->fname?></strong></p>
                                                    </div>
                                                    <div class="col-xs-2 text-right">
                                                        <a class="action-btn csv-downloader" href="<?=$port->download_link?>" data-toggle="tooltip" data-placement="left" title="Download (<?=$port->download_count?>)">
                                                            <button type="button" class="btn btn-xs btn-primary">
                                                                <i class="fa fa-download"></i>
                                                            </button>
                                                        </a>
                                                    </div>
                                                </div>
                                            <?php $i++; } ?>
                                        <?php } else { ?>
                                            <p>No leads are available in your leadbox.</p>
                                        <?php } ?>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>