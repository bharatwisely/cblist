<style>.box .box-body {min-height:200px !important;}</style>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
		                <h2>Dashboard</h2>
                    </div>
                </div>
                <div class="page-content">
                    <div class="row">

                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-file-text-o"></i> PROPOSALS</h3>
                                    <h3 class="box-title pull-right">
                                        <a href="<?=base_url('pages/proposal-writer')?>/" class="action-btn" data-toggle="tooltip" data-placement="top" title="New Proposal">
                                        	<i class="fa fa-plus-square-o"></i> New
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
                                	<div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover datatables">
                                            <thead>
                                                <tr>
                                                    <th width="15%"><p>Proposal #</p></th>
                                                    <th width="45%"><p>Property Name</p></th>
                                                    <th width="20%"><p>Created On</p></th>
                                                    <th width="20%" class="text-center"><p>Actions</p></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($proposal_completed as $key => $proposal) { ?>
                                                    <?php $propertyId = $proposal->PropertyID; ?>
                                                    <tr>
                                                        <td><p><?=$proposal->PropertyID?></p></td>
                                                        <td>
                                                            <p>
                                                                <a class="action-btn" data-toggle="tooltip" data-placement="top" href="<?=base_url("pages/print-report/" . $propertyId . "/SingleProposal")?>/" title="Click to view">
                                                                    <?=$proposal->PropertyName?>
                                                                </a>
                                                            </p>
                                                        </td>
                                                        <td><p><?=$proposal->CreatedOn?></p></td>
                                                        <td class="text-center">
                                                        	<input type="checkbox" <?=($proposal->FlipOrRefi=='Flip'?'checked':'')?> data-size="mini" class="switch OptionRefiFlip" data-on-text="Flip" data-off-text="Refi" data-off-color="info" data-id="<?=$propertyId?>" />
                                                            <a class="action-btn" data-toggle="tooltip" data-placement="top" href="<?=base_url("pages/submit-proposal/" . $propertyId . "/proposals")?>/" title="<?=($proposal->Submitted=='Yes' ? 'Unsubmit' : 'Submit')?>">
                                                                <button type="button" class="btn btn-xs btn-warning">
                                                                    <?=($proposal->Submitted=='Yes' ? '<i class="fa fa-unlink"></i>' : '<i class="fa fa-link"></i>')?>
                                                                </button>
                                                            </a>
                                                            <a class="action-btn" data-toggle="tooltip" data-placement="top" href="<?=base_url("pages/edit-proposal/" . $propertyId . "/proposals")?>/" title="Edit">
                                                                <button type="button" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
                                                            </a>
                                                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Reports">
                                                                <button onclick="open_print_proposal('<?=$propertyId?>', 'print')" type="button" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></button>
                                                            </a>
                                                            <a class="action-btn" onclick="return confirm('Are you want to delete this proposal?');" href="<?=base_url('pages/delete-proposal/' . $propertyId . '/proposals')?>/" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-calculator"></i> RENTALS</h3>
                                    <h3 class="box-title pull-right">
                                        <a href="<?=base_url('pages/rental-calculation')?>/" class="action-btn" data-toggle="tooltip" data-placement="top" title="New Calculation">
                                        	<i class="fa fa-plus-square-o"></i> New
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
                                	<div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover datatables">
                                            <thead>
                                                <tr>
                                                    <th width="15%"><p>Proposal #</p></th>
                                                    <th width="45%"><p>Property Name</p></th>
                                                    <th width="20%"><p>Created On</p></th>
                                                    <th width="20%" class="text-center"><p>Actions</p></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($rental_completed as $key => $rental) { ?>
                                                    <?php $propertyId = $rental->PropertyID; ?>
                                                    <tr>
                                                        <td><p><?=$rental->PropertyID?></p></td>
                                                        <td>
                                                            <p>
                                                                <a class="action-btn" href="<?=base_url("pages/print-report/" . $propertyId . "/SingleRental")?>/" data-toggle="tooltip" data-placement="top" title="Click to view">
                                                                    <?=$rental->PropertyName?>
                                                                </a>
                                                            </p>
                                                        </td>
                                                        <td><p><?=$rental->CreatedOn?></p></td>
                                                        <td class="text-center">
                                                            <a class="action-btn" data-toggle="tooltip" data-placement="top" href="<?=base_url("pages/submit-rental/" . $propertyId . "/rentals")?>/" title="<?=($rental->Submitted=='Yes' ? 'Unsubmit' : 'Submit')?>">
                                                                <button type="button" class="btn btn-xs btn-warning">
                                                                    <?=($rental->Submitted=='Yes' ? '<i class="fa fa-unlink"></i>' : '<i class="fa fa-link"></i>')?>
                                                                </button>
                                                            </a>
                                                            <a class="action-btn" data-toggle="tooltip" data-placement="top" href="<?=base_url("pages/edit-rental/" . $propertyId . "/rentals")?>/" title="Edit">
                                                                <button type="button" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
                                                            </a>
                                                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Reports">
                                                                <button onclick="open_print_rental('<?=$propertyId?>', 'print')" type="button" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></button>
                                                            </a>
                                                            <a class="action-btn" onclick="return confirm('Are you want to delete this rental proposal?');" href="<?=base_url('pages/delete-rental/' . $propertyId . '/rentals')?>/" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-calculator"></i> MAX OFFERS</h3>
                                    <h3 class="box-title pull-right">
                                        <a href="<?=base_url('pages/offer-calculator')?>/" class="action-btn" data-toggle="tooltip" data-placement="top" title="New Offer">
                                        	<i class="fa fa-plus-square-o"></i> New
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
                                	<div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover datatables">
                                            <thead>
                                                <tr>
                                                    <th width="15%"><p>Offer #</p></th>
                                                    <th width="45%"><p>Offer Name</p></th>
                                                    <th width="20%"><p>Created On</p></th>
                                                    <th width="20%" class="text-center"><p>Actions</p></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($offer_completed as $key => $offer) { ?>
                                                    <?php $propertyId = $offer->PropertyID; ?>
                                                    <tr>
                                                        <td><p><?=$offer->PropertyID?></p></td>
                                                        <td>
                                                            <p>
                                                                <a class="action-btn" data-toggle="tooltip" data-placement="top" href="<?=base_url("pages/print-report/" . $propertyId . "/SingleOffer")?>/" title="Click to view">
                                                                    <?=$offer->address_name?>
                                                                </a>
                                                            </p>
                                                        </td>
                                                        <td><p><?=$offer->CreatedOn?></p></td>
                                                        <td class="text-center">
                                                            <a class="action-btn" href="<?=base_url('pages/edit-offer/' . $propertyId . '/MaxOffers')?>/" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <button type="button" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
                                                            </a>
                                                            <a class="action-btn" href="<?=base_url('pages/print-report/' . $propertyId . '/MaxOffers')?>/" data-toggle="tooltip" data-placement="top" title="Reports" target="_blank">
                                                                <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></button>
                                                            </a>
                                                            <a class="action-btn" onclick="return confirm('Are you want to delete this offer?');" href="<?=base_url('pages/delete-offer/' . $propertyId . '/max-offers')?>/" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>                                

                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-calculator"></i> COMPARISONS</h3>
                                    <h3 class="box-title pull-right">
                                        <a href="<?=base_url('pages/comparison-analyser')?>/" class="action-btn" data-toggle="tooltip" data-placement="top" title="New Comparison">
                                        	<i class="fa fa-plus-square-o"></i> New
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
                                    <div class="table-responsive">
                                        <table class="table table-striped table-bordered table-hover datatables">
                                            <thead>
                                                <tr>
                                                    <th width="15%"><p>Comparison #</p></th>
                                                    <th width="45%"><p>Comparison Address</p></th>
                                                    <th width="20%"><p>Created On</p></th>
                                                    <th width="20%" class="text-center"><p>Actions</p></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($comp_completed as $key => $comp) {
													$propertyId = $comp->PropertyID; ?>
                                                    <tr>
                                                        <td><p><?=$comp->PropertyID?></p></td>
                                                        <td>
                                                            <p>
                                                                <a class="action-btn" href="#" data-toggle="tooltip" data-placement="top" title="Click to view">
                                                                    <?=!empty($comp->P1Address) ? $comp->P1Address . ' - ' : ''?>
                                                                    <?=!empty($comp->P2Address) ? $comp->P2Address . ' - ' : ''?>
                                                                    <?=!empty($comp->P3Address) ? $comp->P3Address . ' - ' : ''?>
                                                                    <?=!empty($comp->P4Address) ? $comp->P4Address . ' - ' : ''?>
                                                                    <?=!empty($comp->P5Address) ? $comp->P5Address . '' : ''?>
                                                                </a>
                                                            </p>
                                                        </td>
                                                        <td><p><?=$comp->CreatedOn?></p></td>
                                                        <td class="text-center">
                                                            <a class="action-btn" href="<?=base_url('pages/edit-comparison/' . $propertyId . '/Analyser')?>/" data-toggle="tooltip" data-placement="top" title="Edit">
                                                                <button type="button" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
                                                            </a>
                                                            <a class="action-btn" href="<?=base_url('pages/print-report/' . $propertyId . '/Analyser')?>/" data-toggle="tooltip" data-placement="top" title="Reports" target="_blank">
                                                                <button type="button" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></button>
                                                            </a>
                                                            <a class="action-btn" onclick="return confirm('Are you want to delete this comparison result?');" href="<?=base_url('pages/delete-comparison/' . $propertyId . '/Analyser')?>/" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                            
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>

<?php $this->load->view('template/report-modal'); ?>
<?php $this->load->view('template/rental-modal'); ?>
<?php $this->load->view('template/email-modal'); ?>