<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
		                <h2>Portfolio</h2>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    	<?php if ( !$portfolio ) { ?>
                        	<!-- No action needed -->
                        <?php } elseif ( !$proposal ) { ?>
                        	<a href="<?=base_url('pages/proposal-writer')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to post your first proposal."><i class="fa fa-pencil-square-o"></i> Create Proposal</a>
                        <?php } else { ?>
                        	<!-- No action needed -->
                        <?php } ?>
                    </div>
                </div>
                
                <div class="page-content">
                	<?php if ( !$portfolio ) { ?>
                        <div class="alert alert-warning" role="alert">
                        	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-info-circle"></i> Please create your first portfolio below to get the access.&nbsp;&nbsp;&nbsp; 
                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Portfolio Tutorial">
                                <span data-target="#portfolio_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                            </a>
                        </div>
                    <?php } elseif ( !$proposal ) { ?>
                        <div class="alert alert-warning" role="alert">
                        	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-info-circle"></i> Now you have to create your proposal. For creating your first proposal click the above '<a href="<?=base_url('pages/proposal-public')?>/"><u>Create Proposal</u></a>' botton.&nbsp;&nbsp;&nbsp; 
                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Proposal Tutorial">
                                <span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                            </a>
                        </div>
                    <?php } else { ?>
                        <!-- No action needed -->
                    <?php } ?>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-paint-brush"></i> Edit Your Portfolio</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#portfolio_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
									<?=form_open_multipart(base_url('pages/edit-portfolio/' . $single->id) . '/portfolio/')?>
                                        <div class="row">
                                        	<?php if ( $success ) { ?>
                                            	<div class="col-xs-12">
                                                    <div class="alert alert-success" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <i class="fa fa-info-circle"></i> <?=$success?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="col-xs-3">
                                                <div class="portfolio-pic">
                                                	<div class="pic-inner<?=($image_error ? ' InputError' : '')?>">
														<?php if ( $single->images ) { ?>
                                                            <?=form_hidden('all_pic', $single->images)?>
                                                            <?=form_hidden('del_pic', '')?>
                                                            <?php $images = explode('|', $single->images); $i = 0; ?>
                                                            <?php foreach ( $images as $img ) { ?>
                                                                
                                                                <div class="image-upload">
                                                                    <?php if ( $i != 0 ) { ?>
                                                                        <i id="<?=$img?>" title="Remove this image" class="fa fa-close portfolio-close"></i>
                                                                    <?php } ?>
                                                                    <?php
                                                                    $picture = array(
                                                                        'name'  	=> 'portfolio_pic[]',
                                                                        'id'    	=> $img,
                                                                        'class'		=> 'form-control file-portfolio',
                                                                    ); ?>
                                                                    <img src="<?=$img?>" alt="pic" class="portfolio-img thumbnail portfolio_img_upload" title="Change the image" />
                                                                    <?=form_upload($picture)?>
                                                                    <hr />
                                                                </div>
                                                                
                                                            <?php $i++;
                                                            } ?>
                                                        <?php } else { ?>
                                                        
                                                            <div class="image-upload">
                                                                <?php
                                                                $picture = array(
                                                                    'name'  	=> 'portfolio_pic[]',
                                                                    'id'    	=> 'portfolio_pic',
                                                                    'class'		=> 'form-control file-portfolio',
                                                                ); ?>
                                                                <img src="<?=base_url('assets/images/uploadimg.jpg')?>" alt="pic" class="portfolio-img thumbnail portfolio_img_upload" title="Select portfolio images" />
                                                                <?=form_upload($picture)?>
                                                            </div>
                                                        
                                                    	<?php } ?>
                                                        <?=($image_error) ? '<span class="error">' . $image_error . '</span>' : ''?>
                                                    </div>
                                                </div>
                                                <div class="action-btn">
                                                    <p class="small">
                                                        You can upload multiple images<br />
                                                        <font size="1">File formats gif, jpg, png, bmp | Max file size 2 MB</font>
                                                    </p>
                                                    <p><a href="#" class="btn add_more"><i class="fa fa-plus-square-o"></i> Add new</a></p>
                                                </div>
                                            </div>
                                            <div class="col-xs-9">
                                                <div class="fields-panel">
                                                    <div class="panel-field<?=(!empty(form_error('property_name')) ? ' InputError' : '')?>">
                                                        <label>Property Name: </label>
                                                        <?php 
                                                        $property_name = array(
                                                            'name'  => 'property_name',
                                                            'id'    => 'property_name',
                                                            'value' => $single->property_name,
                                                            'class'	=> 'form-control',		
                                                        ); ?>
                                                        <?=form_input($property_name)?>
                                                        <?=form_error('property_name', '<span class="error">', '</span>')?>
                                                    </div>
                                                    
                                                    <div class="panel-field<?=(!empty(form_error('property_add')) ? ' InputError' : '')?>">
                                                        <label>Write your property location: </label>
                                                        <?php 
                                                        $property_add = array(
                                                            'name'  => 'property_add',
                                                            'id'    => 'property_add',
                                                            'placeholder' => '',
                                                            'value' => $single->location,
                                                            'class'	=> 'form-control map-field',
                                                        ); ?>
                                                        <?=form_input($property_add)?>
                                                        <div style="height:200px;" id="map-canvas"></div>
                                                        <?=form_error('property_add', '<span class="error">', '</span>')?>
                                                    </div>
                                                    
                                                    <div class="panel-field<?=(!empty(form_error('youtube_link')) ? ' InputError' : '')?>">
                                                        <label>Paste a YouTube link of property you have developed: (Optional)</label>
                                                        <?php 
                                                        $youtube_link = array(
                                                            'name'  => 'youtube_link',
                                                            'id'    => 'youtube_link',
                                                            'value' => $single->youtube_link,
                                                            'class'	=> 'form-control',		
                                                        ); ?>
                                                        <?=form_input($youtube_link)?>
                                                        <?=form_error('youtube_link', '<span class="error">', '</span>')?>
                                                    </div>
                                                    
                                                    <div class="panel-field<?=(!empty(form_error('info_one')) ? ' InputError' : '')?>">
                                                        <label>Property information: </label>
                                                        <?php 
                                                        $info_one = array(
                                                            'name'  => 'info_one',
                                                            'id'    => 'info_one',
                                                            'value' => $single->info_one,
                                                            'class'	=> 'form-control',
                                                            'rows'	=> 6,
                                                        ); ?>
                                                        <?=form_textarea($info_one)?>
                                                        <?=form_error('info_one', '<span class="error">', '</span>')?>
                                                    </div>
                                                    
                                                    <div class="panel-field<?=(!empty(form_error('info_two')) ? ' InputError' : '')?>">
                                                        <label>Additional property information: (Optional)</label>
                                                        <?php 
                                                        $info_two = array(
                                                            'name'  => 'info_two',
                                                            'id'    => 'info_two',
                                                            'value' => $single->info_two,
                                                            'class'	=> 'form-control',
                                                            'rows'	=> 5,
                                                        ); ?>
                                                        <?=form_textarea($info_two)?>
                                                        <?=form_error('info_two', '<span class="error">', '</span>')?>
                                                    </div>
                                                    <div class="panel-field">
                                                        <?php
                                                        $data = array(
                                                            'name' 		=> 'add_portfolio',
                                                            'id' 		=> 'add_portfolio',
                                                            'value' 	=> 'true',
                                                            'type' 		=> 'submit',
                                                            'content' 	=> 'Submit',
                                                            'class'		=> 'btn btn-primary portfolio_submit',
                                                        ); ?>					
                                                        <?= form_button($data) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    <?=form_close()?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>