<?php if ( $error ) { ?><style>.edit-field{display:block !important;}.profile-data{display:none !important;}</style><?php } ?>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
		                <h2>My <?=($user->user_type=='cash_lender' ? 'Lender' : '')?> Profile</h2>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    	<?php if ( $user->user_type == 'investor' ) { ?>
							<?php if ( !$profile ) { ?>
                                <!-- No action needed -->
                            <?php } elseif ( !$portfolio ) { ?>
                                <a href="<?=base_url('pages/new-portfolio')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to create your first portfolio."><i class="fa fa-plus-square-o"></i> Create Portfolio</a>
                            <?php } elseif ( !$proposal ) { ?>
                                <a href="<?=base_url('pages/proposal-writer')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to post your first proposal."><i class="fa fa-plus-square-o"></i> Create Proposal</a>
                            <?php } else { ?>
                                <!-- No action needed -->
                            <?php } ?>
                        <?php } ?>
                    </div>
                </div>
                
                <div class="page-content">
                	<?php if ( $user->user_type == 'investor' ) { ?>
						<?php if ( !$profile ) { ?>
                            <div class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-info-circle"></i>&nbsp;&nbsp;Please update your profile details below to get the access.&nbsp;&nbsp;&nbsp; 
                                <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Profile Tutorial">
                                	<span data-target="#profile_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                </a>
                            </div>
                        <?php } elseif ( !$portfolio ) { ?>
                            <div class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-info-circle"></i>&nbsp;&nbsp;Now you have to create your portfolio. For creating your first portfolio click the above '<a href="<?=base_url('pages/new-portfolio')?>/"><u>Create Portfolio</u></a>' botton.&nbsp;&nbsp;&nbsp;
                                <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Portfolio Tutorial">
                                	<span data-target="#portfolio_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                </a>
                            </div>
                        <?php } elseif ( !$proposal ) { ?>
                            <div class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-info-circle"></i>&nbsp;&nbsp;Now you have to create your proposal. For creating your first proposal click the above '<a href="<?=base_url('pages/proposal-writer')?>/"><u>Create Proposal</u></a>' botton.&nbsp;&nbsp;&nbsp;
                                <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Proposal Tutorial">
                                	<span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                </a>
                            </div>
                        <?php } else { ?>
                            <!-- No action needed -->
                        <?php } ?>
                    <?php } elseif ( $user->user_type == 'cash_lender' ) { ?>
                    	<?php if ( !$profile ) { ?>
                            <div class="alert alert-warning" role="alert">
                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                <i class="fa fa-info-circle"></i> Please update your profile details below to get the access.&nbsp;&nbsp;&nbsp; 
                                <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Profile Tutorial">
                                	<span data-target="#profile_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                </a>
                            </div>
                        <?php } else { ?>
                            <!-- No action needed -->
                        <?php } ?>
                    <?php } ?>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-user"></i> Profile Settings</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#profile_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                    <h3 class="box-title pull-right">
                                        <?php if ( $profile ) { ?>
                                            <a id="cancel_profile" class="hide action-btn" data-toggle="tooltip" data-placement="top" title="Cancel">
                                                <i class="fa fa-close"></i> Cancel
                                            </a>
                                            <a id="edit_profile" class="action-btn" data-toggle="tooltip" data-placement="top" title="Edit Profile">
                                                <i class="fa fa-pencil-square-o"></i> Edit
                                            </a>
                                        <?php } ?>
                                    </h3>
                                </div>
                                <div class="box-body">
			                        <?=form_open_multipart(base_url('pages/profile') . '/')?>
                                    	<div class="row">
                                        	<?php if ( $success ) { ?>
                                            	<div class="col-xs-12">
                                                    <div class="alert alert-success" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <i class="fa fa-info-circle"></i> <?=$success?>
                                                    </div>
                                                </div>
                                            <?php } elseif ( $error ) { ?>
                                                <div class="col-xs-12">
                                                    <div class="alert alert-danger" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <i class="fa fa-info-circle"></i> Please correct the following field(s) below.
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="col-xs-3 profile-pic">
                                            	<div class="pic-inner <?=($image_error ? ' InputError' : '')?>">
	                                                <img class="thumbnail" id="profile_pic" src="<?=$user->picture_medium ? $user->picture_medium : base_url('assets/images/default-pic.jpg')?>" alt="" />
                                                    <div class="edit-profile">
                                                        <div class="edit-field" <?=($profile) ? 'style="display:none;"' : 'style="display:block;"'?>>
                                                            <?php
                                                            $picture = array(
                                                                'name'  => 'picture',
                                                                'id'    => 'picture',
                                                                'class'	=> 'form-control file-input',
                                                                'title' => 'Select profile image',
                                                            ); ?>
                                                            <?=form_upload($picture)?>
                                                            <a href="#" class="edit-pic"><i class="fa fa-pencil-square-o"></i> Edit Picture or Logo</a><br />
                                                            <font size="1">File formats gif, jpg, png, bmp | Max file size 1 MB</font>
                                                        </div>
                                                        <?=($image_error) ? '<span class="error">' . $image_error . '</span>' : ''?>
                                                    </div>
                                                </div>
                                                <hr /><br />
                                                <div class="profile-scale text-left">
                                                    <div class="panel panel-default">
                                                        <div class="panel-heading">Profile Strength</div>
                                                        <div class="panel-body">
                                                            <p><?=$completeness?>% Profile Completeness</p>
                                                            <?php 
                                                            if ( $completeness >= 75 && $completeness <= 100 ) {
                                                                $class = 'progress-bar-success';
                                                            } elseif ( $completeness >= 50 && $completeness < 75 ) {
                                                                $class = 'progress-bar-info';
                                                            } elseif ( $completeness >= 25 && $completeness < 50 ) {
                                                                $class = 'progress-bar-warning';
                                                            } elseif ( $completeness >= 0 && $completeness < 25 ) {
                                                                $class = 'progress-bar-danger';
                                                            }
                                                            ?>
                                                            <div class="progress">
                                                                <div class="progress-bar <?=$class?>" role="progressbar" aria-valuenow="<?=$completeness?>" aria-valuemin="0" aria-valuemax="100" style="width:<?=$completeness?>%;"><?=$completeness?>%</div>
                                                            </div><br />
                                                            <p><strong><u>Complete Your Profile</u></strong></p>
                                                            <?php if ( $completeness < 100 ) { ?>
                                                                <ul class="quick-complete">
                                                                    <?php if ( $user->user_type == 'investor' ) { ?>
                                                                        <?php if ( $user->dob == 'Not updated' || $user->phone == 'Not updated' || $user->street == 'Not updated' || $user->town == 'Not updated' || $user->state == 'Not updated' || $user->zip == 'Not updated' || $user->country == 'Not updated' || $user->linkedin_url == 'Not updated' || $user->facebook_url == 'Not updated' || $user->twitter_url == 'Not updated' ) { ?>
                                                                            <li><a href="<?=base_url('pages/accounts')?>/">Update Your Account</a></li>
                                                                        <?php } ?>
                                                                        <?php if ( strpos($user->picture, 'default-pic.jpg') !== false ) { ?>
                                                                            <li><a href="#">Upload Profile Picture</a></li>
                                                                     <?php } ?>
                                                                        <?php if ( $profile == false ) { ?>
                                                                            <li><a href="<?=base_url('pages/profile')?>/">Update Your Profile</a></li>
                                                                        <?php } ?>                                                    
                                                                        <?php if ( $portfolio == false ) { ?>
                                                                            <li><a href="<?=base_url('pages/new-portfolio')?>/">Create Your Portfolio</a></li>
                                                                        <?php } ?>
                                                                        <?php if ( $proposal == false ) { ?>
                                                                            <li><a href="<?=base_url('pages/proposal-writer')?>/">Post Your Proposal</a></li>
                                                                        <?php } ?>
                                                                        <?php if ( !$profile->youtube_link ) { ?>
                                                                            <li><a href="<?=base_url('pages/profile')?>/">Portfolio video link</a></li>
                                                                        <?php } ?>
                                                                    <?php } elseif ( $user->user_type == 'cash_lender' ) { ?>
                                                                        <?php if ( $user->dob == 'Not updated' || $user->phone == 'Not updated' || $user->street == 'Not updated' || $user->town == 'Not updated' || $user->state == 'Not updated' || $user->zip == 'Not updated' || $user->country == 'Not updated' || $user->linkedin_url == 'Not updated' || $user->facebook_url == 'Not updated' || $user->twitter_url == 'Not updated' ) { ?>
                                                                            <li><a href="<?=base_url('pages/accounts')?>/">Update Your Account</a></li>
                                                                        <?php } ?>
                                                                        <?php if ( strpos($user->picture, 'default-pic.jpg') !== false ) { ?>
                                                                            <li><a href="#">Upload Profile Picture</a></li>
                                                                        <?php } ?>
                                                                        <?php if ( $profile == false ) { ?>
                                                                            <li><a href="<?=base_url('pages/profile')?>/">Update Your Profile</a></li>
                                                                        <?php } ?>                                                    
                                                                    <?php } ?>
                                                                </ul>
                                                            <?php } elseif ( $completeness == 100 ) { ?>
                                                                <p>Your Profile is 100% Complete.</p>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-9">
                                                <div class="fields-panel">
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('sales_pitch'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" <?=($profile) ? 'style="display:none;"' : 'style="display:block;"'?>>
                                                            <label>
                                                                <?php if ( $user->user_type == 'investor' ) { ?>
                                                                    Your 500 chars sales pitch for Cash Lenders: <span class="required">*</span>
                                                                <?php } elseif ( $user->user_type == 'cash_lender' ) { ?>
                                                                    Your 500 chars pitch for Real Estate Investors: <span class="required">*</span> 
                                                                <?php } ?>
                                                                <span class="pull-right" id="pitch_counter"></span>
                                                            </label>
                                                            <?php 
                                                            $sales_pitch = array(
                                                                'name'  => 'sales_pitch',
                                                                'id'    => 'sales_pitch',
                                                                'value' => isset($profile->sales_pitch) ? $profile->sales_pitch : set_value('sales_pitch'),
                                                                'class'	=> 'form-control',
                                                                'rows'	=> 5,
                                                            ); ?>
                                                            <?=form_textarea($sales_pitch)?>
                                                            <?=form_error('sales_pitch', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <?php if ( $profile ) { ?>
                                                            <div class="profile-data" <?=($profile) ? 'style="display:block;"' : 'style="display:none;"'?>>
                                                                <label>
                                                                    <?php if ( $user->user_type == 'investor' ) { ?>
                                                                        Sales pitch for cash buyers:
                                                                    <?php } elseif ( $user->user_type == 'cash_lender' ) { ?>
                                                                        Pitch for investors:
                                                                    <?php } ?>
                                                                </label>
                                                                <p><?=$profile->sales_pitch?></p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('about_you'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" <?=($profile) ? 'style="display:none;"' : 'style="display:block;"'?>>
                                                            <label>
                                                                Write a few words about yourself: <span class="required">*</span> 
                                                                <span class="pull-right" id="about_counter"></span>
                                                            </label>
                                                            <?php 
                                                            $about_you = array(
                                                                'name'  => 'about_you',
                                                                'id'    => 'about_you',
                                                                'value' => isset($profile->about_you) ? $profile->about_you : set_value('about_you'),
                                                                'class'	=> 'form-control',
                                                                'rows'	=> 5,
                                                            ); ?>
                                                            <?=form_textarea($about_you)?>
                                                            <?=form_error('about_you', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <?php if ( $profile ) { ?>
                                                            <div class="profile-data" <?=($profile) ? 'style="display:block;"' : 'style="display:none;"'?>>
                                                                <label>About yourself: </label>
                                                                <p><?=$profile->about_you?></p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    
                                                    <div class="panel-field">
                                                    	<?php $err = form_error('about_investment'); ?>
                                                        <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" <?=($profile) ? 'style="display:none;"' : 'style="display:block;"'?>>
                                                            <label>
                                                                Write a few words about your investments philosophy, type of work you do: <span class="required">*</span> 
                                                                <span class="pull-right" id="investment_counter"></span>
                                                            </label>
                                                            <?php 
                                                            $about_investment = array(
                                                                'name'  => 'about_investment',
                                                                'id'    => 'about_investment',
                                                                'value' => isset($profile->about_investment) ? $profile->about_investment : set_value('about_investment'),
                                                                'class'	=> 'form-control',
                                                                'rows'	=> 5,
                                                            ); ?>
                                                            <?=form_textarea($about_investment)?>
                                                            <?=form_error('about_investment', '<span class="error">', '</span>')?>
                                                        </div>
                                                        <?php if ( $profile ) { ?>
                                                            <div class="profile-data" <?=($profile) ? 'style="display:block;"' : 'style="display:none;"'?>>
                                                                <label>About your investments: </label>
                                                                <p><?=$profile->about_investment?></p>
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                    
                                                    <?php if ( $user->user_type == 'investor' ) { ?>
                                                        <div class="panel-field">
                                                            <?php $err = form_error('youtube_link'); ?>
                                                            <div class="edit-field<?=(!empty($err) ? ' InputError' : '')?>" <?=($profile) ? 'style="display:none;"' : 'style="display:block;"'?>>
                                                                <label>Paste an unlisted YouTube link of a video where you share about who you are and why should cash buyers invest in your portfolio: 
                                                                <a class="pop-over" data-container="body" data-toggle="popover" data-placement="top" data-trigger="hover" data-content="Paste an unlisted YouTube link of a video where you share about who you are and why should cash buyers invest in your portfolio."><i class="fa fa-question-circle"></i></a></label>
                                                                <?php 
                                                                $youtube_link = array(
                                                                    'name'  => 'youtube_link',
                                                                    'id'    => 'youtube_link',
                                                                    'value' => isset($profile->youtube_link) ? $profile->youtube_link : set_value('youtube_link'),
                                                                    'class'	=> 'form-control',
                                                                ); ?>
                                                                <?=form_input($youtube_link)?>
                                                                <?=form_error('youtube_link', '<span class="error">', '</span>')?>
                                                            </div>
                                                            <?php if ( $profile->youtube_link ) { ?>
                                                                <div class="profile-data" <?=($profile) ? 'style="display:block;"' : 'style="display:none;"'?>>
                                                                    <label>YouTube video: </label>
                                                                    <div id="profile_video"></div>
                                                                    <script type="text/javascript">
                                                                    jwplayer("profile_video").setup({
                                                                        file: "<?=$profile->youtube_link?>",
                                                                        width: "100%",
                                                                        height: 450,
                                                                    });
                                                                    </script>
                                                                </div>
                                                            <?php } ?>
                                                        </div>
                                                    <?php } ?>
                                                    
                                                    <div class="panel-field">
                                                        <div class="edit-field" <?=($profile) ? 'style="display:none;"' : 'style="display:block;"'?>>
                                                            <?php
                                                            $data = array(
                                                                'name' 		=> 'update_profile',
                                                                'id' 		=> 'update_profile',
                                                                'value' 	=> 'true',
                                                                'type' 		=> 'submit',
                                                                'content' 	=> 'Submit',
                                                                'class'		=> 'btn btn-primary submit_profile',
                                                            ); ?>					
                                                            <?=form_button($data)?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
			                        <?=form_close()?>
                                	<div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>