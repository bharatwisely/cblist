<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
		                <h2>Portfolio</h2>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    	<?php if ( !$portfolio ) { ?>
                        	<!-- No action needed -->
                        <?php } elseif ( !$proposal ) { ?>
                        	<a href="<?=base_url('pages/proposal-writer')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to post your first proposal."><i class="fa fa-pencil-square-o"></i> Create Proposal</a>
                        <?php } else { ?>
                        	<!-- No action needed -->
                        <?php } ?>
                    </div>
                </div>
                <div class="page-content">
                	<?php if ( !$portfolio ) { ?>
                        <div class="alert alert-warning" role="alert">
                        	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-info-circle"></i> Please create your first portfolio below to get the access.&nbsp;&nbsp;&nbsp; 
                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Portfolio Tutorial">
                                <span data-target="#portfolio_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                            </a>
                        </div>
                    <?php } elseif ( !$proposal ) { ?>
                        <div class="alert alert-warning" role="alert">
                        	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-info-circle"></i> Now you have to create your proposal. For creating your first proposal click the above '<a href="<?=base_url('pages/proposal-writer')?>/"><u>Create Proposal</u></a>' botton.&nbsp;&nbsp;&nbsp;
                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Proposal Tutorial">
                                <span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                            </a>
                        </div>
                    <?php } else { ?>
                        <!-- No action needed -->
                    <?php } ?>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-paint-brush"></i> Add Your Portfolio</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#portfolio_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                </div>
                                <div class="box-body">
									<?=form_open_multipart(base_url('pages/new-portfolio') . '/')?>
                                    	<div class="row">
                                        	<?php if ( $success ) { ?>
                                            	<div class="col-xs-12">
                                                    <div class="alert alert-success" role="alert">
                                                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                        <i class="fa fa-info-circle"></i> <?=$success?>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                            <div class="col-xs-3">
                                                <div class="portfolio-pic">
                                                	<div class="pic-inner <?=($image_error ? ' InputError' : '')?>">
                                                        <?php
                                                        $picture = array(
                                                            'name'  	=> 'portfolio_pic[]',
                                                            'id'    	=> 'portfolio_pic',
                                                            'class'		=> 'form-control file-portfolio',
                                                        ); ?>
                                                        <div class="image-upload">
                                                            <img src="<?=base_url('assets/images/uploadimg.jpg')?>" alt="pic" class="portfolio-img thumbnail portfolio_img_upload" title="Select portfolio images" />
                                                            <?=form_upload($picture)?>
                                                        </div>
                                                        <?=($image_error) ? '<br /><span class="error">' . $image_error . '</span>' : ''?>
                                                    </div>
                                                    <hr />
                                                </div>
                                                <div class="pic-action-btn">
                                                    <p class="small">
                                                        You can upload multiple images<br />
                                                        <font size="1">File formats gif, jpg, png, bmp | Max file size 2 MB</font>
                                                    </p>
                                                    <p><a href="#" class="btn add_more"><i class="fa fa-plus-square-o"></i> Add another</a></p>
                                                </div>
                                            </div>
                                            <div class="col-xs-9">
                                                <div class="fields-panel">
                                                    <?php $err = form_error('property_name'); ?>
                                                    <div class="panel-field<?=(!empty($err) ? ' InputError' : '')?>">
                                                        <label>Property Name: </label>
                                                        <?php 
                                                        $property_name = array(
                                                            'name'  => 'property_name',
                                                            'id'    => 'property_name',
                                                            'value' => ($reset) ? "" : set_value('property_name'),
                                                            'class'	=> 'form-control',		
                                                        ); ?>
                                                        <?=form_input($property_name)?>
                                                        <?=form_error('property_name', '<span class="error">', '</span>')?>
                                                    </div>
                                                    <?php $err = form_error('property_add'); ?>
                                                    <div class="panel-field<?=(!empty($err) ? ' InputError' : '')?>">
                                                        <label>Write your property location: </label>
                                                        <?php 
                                                        $property_add = array(
                                                            'name'  => 'property_add',
                                                            'id'    => 'property_address',
                                                            'placeholder' => '',
                                                            'value' => ($reset) ? "" : set_value('property_add'),
                                                            'class'	=> 'form-control map-field',
                                                        ); ?>
                                                        <?=form_input($property_add)?>
                                                        <div style="height:200px;" id="map-canvas"></div>
                                                        <?=form_error('property_add', '<span class="error">', '</span>')?>
                                                    </div>
                                                    <?php $err = form_error('youtube_link'); ?>
                                                    <div class="panel-field<?=(!empty($e) ? ' InputError' : '')?>">
                                                        <label>Paste a YouTube link of property you have developed: (Optional)</label>
                                                        <?php 
                                                        $youtube_link = array(
                                                            'name'  => 'youtube_link',
                                                            'id'    => 'youtube_link',
                                                            'value' => ($reset) ? "" : set_value('youtube_link'),
                                                            'class'	=> 'form-control',		
                                                        ); ?>
                                                        <?=form_input($youtube_link)?>
                                                        <?=form_error('youtube_link', '<span class="error">', '</span>')?>
                                                    </div>
                                                    <?php $err = form_error('info_one'); ?>
                                                    <div class="panel-field<?=(!empty($err) ? ' InputError' : '')?>">
                                                        <label>Property information: <small class="pull-right">*Enter property information minimum 32 characters</small></label>
                                                        <?php 
                                                        $info_one = array(
                                                            'name'  => 'info_one',
                                                            'id'    => 'info_one',
                                                            'value' => ($reset) ? "" : set_value('info_one'),
                                                            'class'	=> 'form-control',
                                                            'rows'	=> 6,
                                                        ); ?>
                                                        <?=form_textarea($info_one)?>
                                                        <?=form_error('info_one', '<span class="error">', '</span>')?>
                                                    </div>
                                                    <?php $err = form_error('info_two'); ?>
                                                    <div class="panel-field<?=(!empty($err) ? ' InputError' : '')?>">
                                                        <label>Additional property information: (Optional)</label>
                                                        <?php 
                                                        $info_two = array(
                                                            'name'  => 'info_two',
                                                            'id'    => 'info_two',
                                                            'value' => ($reset) ? "" : set_value('info_two'),
                                                            'class'	=> 'form-control',
                                                            'rows'	=> 5,
                                                        ); ?>
                                                        <?=form_textarea($info_two)?>
                                                        <?=form_error('info_two', '<span class="error">', '</span>')?>
                                                    </div>
                                                    <div class="panel-field">
                                                        <?php
                                                        $data = array(
                                                            'name' 		=> 'add_portfolio',
                                                            'id' 		=> 'add_portfolio',
                                                            'value' 	=> 'true',
                                                            'type' 		=> 'submit',
                                                            'content' 	=> 'Submit',
                                                            'class'		=> 'btn btn-primary portfolio_submit',
                                                        ); ?>					
                                                        <?=form_button($data) ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
			                        <?=form_close()?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>