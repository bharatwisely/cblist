<style type="text/css">.portfolio-desc-sec p{margin:0;}</style>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-10 col-md-10 col-sm-6 col-xs-12">
		                <h2>Proposals</h2>
                    </div>
                    <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    	<?php if ( !$proposal ) { ?>
                        	<a href="<?=base_url('pages/proposal-writer')?>/" tabindex="0" class="btn btn-block add-proposal" data-container="body" data-toggle="popover" data-placement="left" data-trigger="hover" data-content="Click here to post your first proposal."><i class="fa fa-plus-square-o"></i> Create Proposal</a>
                        <?php } else { ?>
                        	<!-- No action needed -->
                        <?php } ?>
                    </div>
                </div>
                
                <div class="page-content">
                	<?php if ( !$proposal ) { ?>
                        <div class="alert alert-warning" role="alert">
                        	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-info-circle"></i> Now you have to create your proposals. For creating your first proposal click the above '<a href="<?=base_url('pages/proposal-writer')?>/"><u>Create Proposal</u></a>' botton.&nbsp;&nbsp;&nbsp; 
                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Proposal Tutorial">
                                <span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                            </a>
                        </div>
                    <?php } else { ?>
                        <!-- No action needed -->
                    <?php } ?>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-file-text-o"></i> Proposals</h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                    <h3 class="box-title pull-right">
                                        <a href="<?=base_url('pages/proposal-writer')?>/" class="action-btn" data-toggle="tooltip" data-placement="top" title="New Proposal">
                                        	<i class="fa fa-plus-square-o"></i> New
                                        </a>
                                    </h3>
                                    <h3 class="box-title pull-right">
                                    	<span data-toggle="tooltip" data-placement="top" title="New from Max Offer">
                                            <a href="#" data-target="#MaxOfferModal" data-toggle="modal" class="action-btn">
                                                <i class="fa fa-plus-square-o"></i> New From Max Offer
                                            </a>
                                        </span>
                                    </h3>
                                </div>
                                <div class="box-body">
									<?php if ( $this->session->userdata('proposals_deleted') ) { ?>
                                        <div class="alert alert-success" role="alert" style="margin-bottom:10px;">
                                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-info-circle"></i> Selected proposal is successfully deleted!
                                        </div>
                                    <?php } ?>
                                    <?php if ( $this->session->userdata('proposal_submitted') == 'Yes' ) { ?>
                                        <div class="alert alert-success" role="alert" style="margin-bottom:10px;">
                                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-info-circle"></i> Selected proposal is successfully submitted to network!
                                        </div>
                                    <?php } elseif ( $this->session->userdata('proposal_submitted') == 'No' ) { ?>
                                        <div class="alert alert-info" role="alert" style="margin-bottom:10px;">
                                            <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <i class="fa fa-info-circle"></i> Selected proposal is not showing to network now!
                                        </div>
                                    <?php } ?>
									<?php if ( $proposal ) { $i = 0; ?>
                                        <?php foreach ( $proposal as $public ) {
                                            if ( $i % 2 == 0 ) { $bg = '#F8F8F8'; } else { $bg = ''; }
											if ( $public->ProposalStatus == 'publish' ) { ?>
                                                <div class="container-fluid proposal-row" style="background-color:<?php echo $bg; ?>;">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="portfolio-desc-sec">
                                                                <div class="col-xs-3">
                                                                    <p><strong><?=$public->PropertyName?></strong></p>
                                                                </div>
                                                                <div class="col-xs-5">
                                                                    <p>
                                                                        <i class="fa fa-map-marker"></i>&nbsp;&nbsp;
                                                                        <?=$public->PropertyStreetAddress?>, 
                                                                        <?=$public->PropertyCityTown?>, 
                                                                        <?=$public->PropertyStateProvince?>, 
                                                                        <?=$public->PropertyZipCode?>, 
                                                                        <?=$countries[$public->PropertyCountry]?>
                                                                    </p>
                                                                </div>
                                                                <?php $propertyId = $public->PropertyID; ?>
                                                                <div class="col-xs-4 text-right">
                                                                	<input type="checkbox" <?=($public->FlipOrRefi=='Flip'?'checked':'')?> data-size="mini" class="switch OptionRefiFlip" data-on-text="Flip" data-off-text="Refi" data-off-color="info" data-id="<?=$propertyId?>" />
                                                                    <a class="action-btn" data-toggle="tooltip" data-placement="top" href="<?=base_url("pages/submit-proposal/" . $propertyId . "/proposals")?>/" title="Network">
                                                                        <button type="button" class="btn btn-xs btn-warning">
                                                                            <?=($public->Submitted=='Yes' ? '<i class="fa fa-unlink"></i> Unsubmit' : '&nbsp;&nbsp;<i class="fa fa-link"></i> Submit&nbsp;&nbsp;&nbsp;')?>
                                                                        </button>
                                                                    </a>
                                                                    <a class="action-btn" data-toggle="tooltip" data-placement="top" href="<?=base_url("pages/edit-proposal/" . $propertyId . "/proposals")?>/" title="Edit">
                                                                        <button type="button" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
                                                                    </a>
                                                                    <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Reports">
                                                                        <button onclick="open_print_proposal('<?=$propertyId?>', 'print')" type="button" class="btn btn-xs btn-primary"><i class="fa fa-print"></i></button>
                                                                    </a>
                                                                    <a class="action-btn" onclick="return confirm('Are you want to delete this proposal?');" href="<?=base_url('pages/delete-proposal/' . $propertyId . '/proposals')?>/" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                        <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php } else { ?>
												<div class="container-fluid proposal-row" style="background-color:<?php echo $bg; ?>;">
                                                    <div class="col-xs-12">
                                                        <div class="row">
                                                            <div class="portfolio-desc-sec">
                                                                <div class="col-xs-3">
                                                                    <p><strong><?=($public->PropertyName ? $public->PropertyName : 'Property Name')?></strong></p>
                                                                </div>
                                                                <div class="col-xs-6">
                                                                    <p>
                                                                        <i class="fa fa-map-marker"></i>&nbsp;&nbsp;
                                                                        <?=($public->PropertyStreetAddress ? $public->PropertyStreetAddress : 'Street Address')?>, 
                                                                        <?=($public->PropertyCityTown ? $public->PropertyCityTown : 'City/Town')?>, 
                                                                        <?=($public->PropertyStateProvince ? $public->PropertyStateProvince : 'State/Province')?>, 
                                                                        <?=($public->PropertyZipCode ? $public->PropertyZipCode : 'ZIP Code')?>, 
                                                                        <?=($countries[$public->PropertyCountry] ? $countries[$public->PropertyCountry] : 'Country')?>
                                                                    </p>
                                                                </div>
                                                                <?php $propertyId = $public->PropertyID; ?>
                                                                <div class="col-xs-3 text-right">
                                                                    <a class="label label-primary" style="margin:0 1px;display:inline-block">Draft Proposal</a>
                                                                    <a class="action-btn" data-toggle="tooltip" data-placement="top" href="<?=base_url("pages/edit-proposal/" . $propertyId . "/proposals")?>/" title="Edit">
                                                                        <button type="button" class="btn btn-xs btn-default"><i class="fa fa-pencil"></i></button>
                                                                    </a>
                                                                    <a class="action-btn" onclick="return confirm('Are you want to delete this proposal?');" href="<?=base_url('pages/delete-proposal/' . $propertyId . '/proposals')?>/" data-toggle="tooltip" data-placement="top" title="Delete">
                                                                        <button type="button" class="btn btn-xs btn-danger"><i class="fa fa-trash"></i></button>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
										<?php } $i++; } ?>
                                    <?php } else { ?>
                                        <p>You have not post any proposals yet. Post your first proposal from <a href="<?=base_url('pages/proposal-writer')?>/">here</a>.</p>
                                    <?php } ?>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>

<!-- Max Offer Modal -->
<div class="modal fade" id="MaxOfferModal" tabindex="-1" role="dialog" aria-labelledby="MaxOfferModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="MaxOfferModalLabel"><i class="fa fa-info-circle"></i> List Of Max Offers</h4>
            </div>
            <div class="modal-body">
                <?php if ( $offers ) { $i = 0; ?>
                	<p>Please select Max Offer to create a new proposal.</p><hr />
					<?php foreach ( $offers as $public ) {
                        if ( $i % 2 == 0 ) { $bg = '#F8F8F8'; } else { $bg = ''; } ?>
                        <div class="container-fluid proposal-row" style="background-color:<?php echo $bg; ?>;">
                            <div class="col-xs-12">
                                <div class="row">
                                    <div class="portfolio-desc-sec">
                                        <div class="col-xs-5">
                                            <p><strong><?=$public->address_name?></strong></p>
                                        </div>
                                        <div class="col-xs-6">
                                            <p><strong>$<?=number_format($public->max_offer, 2)?></strong></p>
                                        </div>
                                        <?php $propertyId = $public->PropertyID; ?>
                                        <div class="col-xs-1 text-right">
                                            <a class="action-btn" href="<?=base_url('pages/offer-to-proposal/' . $propertyId . '/proposal')?>/" data-toggle="tooltip" data-placement="top" title="Choose">
                                                <button type="button" class="btn btn-xs btn-success"><i class="fa fa-check"></i></button>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php $i++; } ?>
                <?php } else { ?>
                    <p>You have not calculate any max offer yet. Calculate your first max offer from <a href="<?=base_url('pages/offer-calculator')?>/">here</a>.</p>
                <?php } ?>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('template/report-modal'); ?>
<?php $this->load->view('template/email-modal'); ?>