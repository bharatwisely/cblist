<style type="text/css">
.fields-panel h3{font-size:18px;text-align:center;}
.fields-panel h4{text-align:center;}
.assump-box{background:transparent;padding:10px 15px;margin-bottom:10px;}
.assump-box-child{background:none;z-index:1;position:relative;}
.panel-field input, .panel-field select{margin-bottom:0;padding:5px;border:none;font-size:16px;border:1px solid #007C30;background:#CEEAD9;box-shadow:none !important;-moz-box-shadow:none !important;-webkit-box-shadow:none !important;}
.assump-box .panel-field button{margin-bottom:0;font-weight:normal;font-size:14px;white-space:normal;}
#PropertyClosingDate{background:#CEEAD9;}
.panel-field input[readonly]{color:#000;}
.panel-field label{margin-bottom:0;font-weight:normal;font-size:18px;height:auto;color:#222;}
.form-inline .panel-field .input-group{width:100%;}
.form-inline .panel-field .form-control{width:100%;box-shadow:none;}
.form-inline .panel-field .input-group .input-group-addon{width:20px;}
hr{margin-bottom:10px;border-top:1px solid #ccc;}

#MoreOptionModal .table-responsive {overflow:auto;}
#MoreOptionModal .modal-content {width:1000px;font-size:16px;}
#MoreOptionModal .input-group .form-control {width:62px !important;}
#MoreOptionModal .form-control {width:85px !important;height:25px !important;}
#MoreOptionModal table {font-size:10px;font-size:13px;background:#fff;}
#MoreOptionModal .table > thead > tr > th, #MoreOptionModal .table > tfoot > tr > th, #MoreOptionModal .table > thead > tr > td, #MoreOptionModal .table > tbody > tr > td, #MoreOptionModal .table > tfoot > tr > td, #MoreOptionModal .table > tbody > tr > th {vertical-align:middle;padding:0;line-height:1;border:1px solid #08652C;}
#MoreOptionModal .table > tbody > tr > th {padding:4px;}
#MoreOptionModal .table > thead > tr > th input, #MoreOptionModal .table > tfoot > tr > th input, #MoreOptionModal .table > thead > tr > td input, #MoreOptionModal .table > tbody > tr > td input, #MoreOptionModal .table > tfoot > tr > td input, #MoreOptionModal .table > thead > tr > th select, #MoreOptionModal .table > tfoot > tr > th select, #MoreOptionModal .table > thead > tr > td select, #MoreOptionModal .table > tbody > tr > td select, #MoreOptionModal .table > tfoot > tr > td select {padding:3px;text-align:left;font-size:10px;min-width:100%;margin:0;border:none;box-shadow:none;background:#ceead9;color:#000;height:26px;}

#ManualGrowthRateModal {z-index:99999;}
#ManualGrowthRateModal .table-responsive {overflow:auto;}
#ManualGrowthRateModal .modal-content {width:1000px;font-size:16px;}
#ManualGrowthRateModal .input-group .form-control {width:62px !important;}
#ManualGrowthRateModal .form-control {width:85px !important;height:25px !important;}
#ManualGrowthRateModal table {font-size:10px;font-size:13px;background:#fff;}
#ManualGrowthRateModal .table > thead > tr > th, #ManualGrowthRateModal .table > tfoot > tr > th, #ManualGrowthRateModal .table > thead > tr > td, #ManualGrowthRateModal .table > tbody > tr > td, #ManualGrowthRateModal .table > tfoot > tr > td, #ManualGrowthRateModal .table > tbody > tr > th {vertical-align:middle;padding:0;line-height:1;border:1px solid #08652C;}
#ManualGrowthRateModal .table > tbody > tr > th {padding:4px;}
#ManualGrowthRateModal .table > thead > tr > th input, #ManualGrowthRateModal .table > tfoot > tr > th input, #ManualGrowthRateModal .table > thead > tr > td input, #ManualGrowthRateModal .table > tbody > tr > td input, #ManualGrowthRateModal .table > tfoot > tr > td input, #ManualGrowthRateModal .table > thead > tr > th select, #ManualGrowthRateModal .table > tfoot > tr > th select, #ManualGrowthRateModal .table > thead > tr > td select, #ManualGrowthRateModal .table > tbody > tr > td select, #ManualGrowthRateModal .table > tfoot > tr > td select {padding:3px;text-align:left;font-size:10px;min-width:100%;margin:0;border:none;box-shadow:none;background:#ceead9;color:#000;height:26px;}

.input-group-addon {background-color:#007C30;border:1px solid #007C30;border-radius:0;padding:5px;color:#fff;}
.input-group .form-control {border:1px solid #007C30;}
#Section1OptImcome .input-group { margin:0 -10px; }
#Section1OptImcome .panel-field { margin-right:10px; }
#Section2OptExpense .input-group { margin:0 -10px; }
#Section2OptExpense .panel-field { margin-right:10px; }

/*.panel-field:nth-child(odd) .form-control {background-color:#c6eefd;border:1px solid #2a6c84;}
.panel-field:nth-child(even) .form-control {background:#ceead9;border:1px solid #007c30;}
.panel-field:nth-child(odd) .input-group-addon {background-color:#2a6c84;border:1px solid #2a6c84;}
.panel-field:nth-child(even) .input-group-addon {background-color:#007c30;border:1px solid #007c30;}*/
.panel-field .form-control[readonly="readonly"] {background-color:#fff;}

#MoreOptionModal .form-control {background:#ceead9;border:1px solid #007c30;}
#MoreOptionModal table tr:nth-child(odd) .form-control {background:#c6eefd;	border:none;}
#MoreOptionModal table tr:nth-child(even) .form-control {background:#ceead9;border:none;}
#MoreOptionModal table tr:nth-child(odd) .input-group-addon {background:#2a6c84;border:1px solid #2a6c84;}
#MoreOptionModal table tr:nth-child(even) .input-group-addon {background:#007c30;border:1px solid #007c30;}

#ManualGrowthRateModal .form-control {background:#ceead9;border:1px solid #007c30;}
#ManualGrowthRateModal table tr:nth-child(odd) .form-control {background:#c6eefd;	border:none;}
#ManualGrowthRateModal table tr:nth-child(even) .form-control {background:#ceead9;border:none;}
#ManualGrowthRateModal table tr:nth-child(odd) .input-group-addon {background:#2a6c84;border:1px solid #2a6c84;}
#ManualGrowthRateModal table tr:nth-child(even) .input-group-addon {background:#007c30;border:1px solid #007c30;}

.topButtons{ top:-55px;}
</style>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-5 col-md-4 col-sm-5 col-xs-10">
		                <h2>Rental Property Evaluator</h2>
                    </div>
                   	<div class="col-lg-7 col-md-6 col-sm-5 col-xs-10">
                    	<div class="quick-tags"></div>
                    </div>
                </div>
                
                <div class="page-content">
                	<div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-calculator"></i> Rental Calculator</h3>
                                    <h3 class="box-title col-xs-6 col-xs-offset-3">
                                        <div class="progress">
                                            <div class="progress-bar" data-transitiongoal="0"></div>
                                        </div>
                                    </h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#rental_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                    <h3 class="box-title pull-right" style="padding:5px 10px;">
                                        <img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide draft-loading" />
                                        <a class="btn btn-primary btn-sm" id="SaveAsDraft">Save As Draft</a>
                                    </h3>
                                </div>
                                <div class="box-body">
                                	<div class="offer-calc-page">
										<?php if ( $success ) { ?>
                                            <div class="alert alert-success" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-info-circle"></i> <?=$success?>
                                            </div>
                                        <?php } ?>

                                        <input type="hidden" id="page_type" value="RENTAL" />
                                        <input type="hidden" id="PageStatus" value="UPDATE" />
                                        <div class="fields-panel flex-box offer-calculator-screen">
                                            <div class="col-xs-12 flex-box-child">
                                                <div class="col-xs-12 assump-box" id="RentalValuator">
        
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>PROPERTY INFORMATION</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child">
                                                        
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                                                                
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep1 on_next_section" data-rel="10">NEXT</button>
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                                <div class="panel-field" id="PropertyNameRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property Name</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyName",
                                                                                'id'    => "PropertyName",
                                                                                'value' => $fields->PropertyName,
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Name -->
                                                                <div class="panel-field" id="PropertyStreetAddressRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property Street Address</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyStreetAddress",
                                                                                'id'    => "PropertyStreetAddress",
                                                                                'value' => $fields->PropertyStreetAddress,
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Street Address -->
                                                                <div class="panel-field" id="PropertyCityTownRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property City/Town</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyCityTown",
                                                                                'id'    => "PropertyCityTown",
                                                                                'value' => $fields->PropertyCityTown,
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property City -->
                                                                <div class="panel-field" id="PropertyStateProvinceRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property State/Province</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyStateProvince",
                                                                                'id'    => "PropertyStateProvince",
                                                                                'value' => $fields->PropertyStateProvince,
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property State -->
                                                                <div class="panel-field" id="PropertyZipCodeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property ZIP Code</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyZipCode",
                                                                                'id'    => "PropertyZipCode",
                                                                                'value' => $fields->PropertyZipCode,
                                                                                'class'	=> 'form-control NumberOnly Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Zip -->
                                                                <div class="panel-field" id="PropertyCountryRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property Country</label>
                                                                        <div class="col-xs-6">
                                                                        	<?=country_select('PropertyCountry', 'PropertyCountry', 'form-control Required', $fields->PropertyCountry, array('US', 'CA'), '')?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Country -->
                                                                <div class="panel-field" id="PropertyClosingDateRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property Closing Date</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyClosingDate",
                                                                                'id'    => "PropertyClosingDate",
                                                                                'value' => $fields->PropertyClosingDate,
                                                                                'class'	=> 'form-control DateClosing Required',
                                                                                'data-provide' => 'datepicker',
                                                                                'data-date-format' => 'mm-dd-yyyy',
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div><hr /><br />
                                                                </div><!-- Property Closing Date -->
                                                                
                                                                <div class="panel-field" id="YourNameRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Your Name</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "YourName",
                                                                                'id'    => "YourName",
                                                                                'value' => $fields->YourName,
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Your Name -->
                                                                <div class="panel-field" id="CompanyNameRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Company Name</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CompanyName",
                                                                                'id'    => "CompanyName",
                                                                                'value' => $fields->CompanyName,
                                                                                'class'	=> 'form-control',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Company Name -->
                                                                
                                                                <div class="panel-field" id="CompanyStreetRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Street Address</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CompanyStreet",
                                                                                'id'    => "CompanyStreet",
                                                                                'value' => ($fields->CompanyStreet ? $fields->CompanyStreet : $user->street),
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Comapny Street Address -->
                                                                <div class="panel-field" id="CompanyCityRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Town/City</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CompanyCity",
                                                                                'id'    => "CompanyCity",
                                                                                'value' => ($fields->CompanyCity ? $fields->CompanyCity : $user->town),
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Company City -->
                                                                <div class="panel-field" id="CompanyStateRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">State/Province</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CompanyState",
                                                                                'id'    => "CompanyState",
                                                                                'value' => ($fields->CompanyState ? $fields->CompanyState : $user->state),
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Company State -->
                                                                <div class="panel-field" id="CompanyZipRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">ZIP Code</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CompanyZip",
                                                                                'id'    => "CompanyZip",
                                                                                'value' => ($fields->CompanyZip ? $fields->CompanyZip : $user->zip),
                                                                                'class'	=> 'form-control NumberOnly Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Company Zip -->
																<div class="panel-field" id="CompanyCountryRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Country Country</label>
                                                                        <div class="col-xs-6">
                                                                        	<?=country_select('CompanyCountry', 'CompanyCountry', 'form-control Required', ($fields->CompanyCountry ? $fields->CompanyCountry : $user->country), array('US', 'CA'), '')?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Company Country -->
                                                                <div class="panel-field" id="PhoneNumberRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Phone Number</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PhoneNumber",
                                                                                'id'    => "PhoneNumber",
                                                                                'value' => $fields->PhoneNumber,
                                                                                'class'	=> 'form-control NumberOnly Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Phone Number -->
                                                                <div class="panel-field" id="EmailRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Email</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Email",
                                                                                'id'    => "Email",
                                                                                'value' => $fields->Email,
                                                                                'class'	=> 'form-control EmailOnly Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Email -->
                                                                <div class="panel-field" id="FacebookURLRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Facebook URL</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FacebookURL",
                                                                                'id'    => "FacebookURL",
                                                                                'value' => ($fields->FacebookURL ? $fields->FacebookURL : $user->facebook_url),
                                                                                'class'	=> 'form-control WebsiteOnly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">www.</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- FacebookURL -->
                                                                <div class="panel-field" id="LinkedinURLRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Linkedin URL</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "LinkedinURL",
                                                                                'id'    => "LinkedinURL",
                                                                                'value' => ($fields->LinkedinURL ? $fields->LinkedinURL : $user->linkedin_url),
                                                                                'class'	=> 'form-control WebsiteOnly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">www.</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- LinkedinURL -->
                                                                <div class="panel-field" id="TwitterURLRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Twitter URL</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TwitterURL",
                                                                                'id'    => "TwitterURL",
                                                                                'value' => ($fields->TwitterURL ? $fields->TwitterURL : $user->twitter_url),
                                                                                'class'	=> 'form-control WebsiteOnly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">www.</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- TwitterURL -->
                                                                <div class="panel-field" id="WebsiteRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Website</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Website",
                                                                                'id'    => "Website",
                                                                                'value' => $fields->Website,
                                                                                'class'	=> 'form-control WebsiteOnly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">www.</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Website -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep1 on_next_section" data-rel="10">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" id="ProposalStatus" name="ProposalStatus" value="<?=$fields->ProposalStatus?>" />
                                                                    <input type="hidden" name="TableName" value="rental_property_information" />
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP 1: PROPERTY INFORMATION -->
                                                    
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>OPERATING INCOME RENT</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child" id="Section1OptImcome"><!-- STEP (1) -->
                                                        
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons" style="margin-right:0;">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="0">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep2 on_next_section" data-rel="20">NEXT</button>
                                                                </div><!-- Next Button -->
                                                                
                                                                <div class="panel-field" id="OperatingIncomeHeader" style="margin-right:0;">
                                                                    <div class="row">
                                                                        <label class="col-xs-1 text-center">Unit Ref #</label>
                                                                        <label class="col-xs-1 text-center"># of units</label>
                                                                        <label class="col-xs-2 text-center">Unit type</label>
                                                                        <label class="col-xs-1 text-center">Square Ft.</label>
                                                                        <label class="col-xs-2 text-center">Monthly Rent Per Unit</label>
                                                                        <label class="col-xs-2 text-center">Annual Rent</label>
                                                                        <label class="col-xs-1 text-center"></label>
                                                                        <label class="col-xs-1 text-center" style="padding-left:0;">Rent<br />SqFt</label>
                                                                        <label class="col-xs-1 text-center" style="padding-left:0">
                                                                        	<button type="button" class="btn btn-md btn-default btn-block" data-target="#annual_growth_rate" data-toggle="modal">Growth<br />Rate</button>
                                                                        </label>
                                                                    </div><hr />
                                                                </div><!-- Operating Income Header -->
                                                                <div class="panel-field" id="OperatingIncomeRow_1">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">1</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_1",
                                                                                'id'    => "PropertyNoOfUnit_1",
                                                                                'value' => $fields->PropertyNoOfUnit_1,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_1" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_1', $options, $fields->PropertyUnitType_1, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_1",
                                                                                'id'    => "PropertySquareFeet_1",
                                                                                'value' => $fields->PropertySquareFeet_1,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_1",
                                                                                'id'    => "PropertyMonthlyRent_1",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_1, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_1",
                                                                                'id'    => "PropertyAnnualRent_1",
                                                                                'value' => number_format($fields->PropertyAnnualRent_1, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_1",
                                                                                'id'    => "PropertyPercentage_1",
                                                                                'value' => round($fields->PropertyPercentage_1),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_1",
                                                                                'id'    => "PropertyRentSqFtPercentage_1",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_1, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?> 
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_1",
                                                                                'id'    => "PropertyAnnualGrowthRate_1",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_1),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_1 -->                                                        
                                                                <div class="panel-field" id="OperatingIncomeRow_2">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">2</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_2",
                                                                                'id'    => "PropertyNoOfUnit_2",
                                                                                'value' => $fields->PropertyNoOfUnit_2,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_2" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_2', $options, $fields->PropertyUnitType_2, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_2",
                                                                                'id'    => "PropertySquareFeet_2",
                                                                                'value' => $fields->PropertySquareFeet_2,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_2",
                                                                                'id'    => "PropertyMonthlyRent_2",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_2, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_2",
                                                                                'id'    => "PropertyAnnualRent_2",
                                                                                'value' => number_format($fields->PropertyAnnualRent_2, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_2",
                                                                                'id'    => "PropertyPercentage_2",
                                                                                'value' => round($fields->PropertyPercentage_2),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_2",
                                                                                'id'    => "PropertyRentSqFtPercentage_2",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_2, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_2",
                                                                                'id'    => "PropertyAnnualGrowthRate_2",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_2),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_2 -->                                                        
                                                                <div class="panel-field" id="OperatingIncomeRow_3">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">3</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_3",
                                                                                'id'    => "PropertyNoOfUnit_3",
                                                                                'value' => $fields->PropertyNoOfUnit_3,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_3" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_3', $options, $fields->PropertyUnitType_3, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_3",
                                                                                'id'    => "PropertySquareFeet_3",
                                                                                'value' => $fields->PropertySquareFeet_3,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_3",
                                                                                'id'    => "PropertyMonthlyRent_3",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_3, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_3",
                                                                                'id'    => "PropertyAnnualRent_3",
                                                                                'value' => number_format($fields->PropertyAnnualRent_3, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_3",
                                                                                'id'    => "PropertyPercentage_3",
                                                                                'value' => round($fields->PropertyPercentage_3),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_3",
                                                                                'id'    => "PropertyRentSqFtPercentage_3",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_3, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_3",
                                                                                'id'    => "PropertyAnnualGrowthRate_3",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_3),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_3 -->                                                        
                                                                <div class="panel-field" id="OperatingIncomeRow_4">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">4</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_4",
                                                                                'id'    => "PropertyNoOfUnit_4",
                                                                                'value' => $fields->PropertyNoOfUnit_4,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_4" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_4', $options, $fields->PropertyUnitType_4, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_4",
                                                                                'id'    => "PropertySquareFeet_4",
                                                                                'value' => $fields->PropertySquareFeet_4,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_4",
                                                                                'id'    => "PropertyMonthlyRent_4",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_4, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_4",
                                                                                'id'    => "PropertyAnnualRent_4",
                                                                                'value' => number_format($fields->PropertyAnnualRent_4, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_4",
                                                                                'id'    => "PropertyPercentage_4",
                                                                                'value' => round($fields->PropertyPercentage_4),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_4",
                                                                                'id'    => "PropertyRentSqFtPercentage_4",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_4, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																			 <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_4",
                                                                                'id'    => "PropertyAnnualGrowthRate_4",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_4),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_4 -->                                                        
                                                                <div class="panel-field" id="OperatingIncomeRow_5">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">5</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_5",
                                                                                'id'    => "PropertyNoOfUnit_5",
                                                                                'value' => $fields->PropertyNoOfUnit_5,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                           <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_5" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_5', $options, $fields->PropertyUnitType_5, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_5",
                                                                                'id'    => "PropertySquareFeet_5",
                                                                                'value' => $fields->PropertySquareFeet_5,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_5",
                                                                                'id'    => "PropertyMonthlyRent_5",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_5, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_5",
                                                                                'id'    => "PropertyAnnualRent_5",
                                                                                'value' => number_format($fields->PropertyAnnualRent_5, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_5",
                                                                                'id'    => "PropertyPercentage_5",
                                                                                'value' => round($fields->PropertyPercentage_5),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_5",
                                                                                'id'    => "PropertyRentSqFtPercentage_5",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_5, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																			 <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_5",
                                                                                'id'    => "PropertyAnnualGrowthRate_5",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_5),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_5 -->                                                                
                                                                <div class="panel-field" id="OperatingIncomeRow_6">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">6</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_6",
                                                                                'id'    => "PropertyNoOfUnit_6",
                                                                                'value' => $fields->PropertyNoOfUnit_6,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                           <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_6" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_6', $options, $fields->PropertyUnitType_6, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_6",
                                                                                'id'    => "PropertySquareFeet_6",
                                                                                'value' => $fields->PropertySquareFeet_6,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_6",
                                                                                'id'    => "PropertyMonthlyRent_6",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_6, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_6",
                                                                                'id'    => "PropertyAnnualRent_6",
                                                                                'value' => number_format($fields->PropertyAnnualRent_6, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_6",
                                                                                'id'    => "PropertyPercentage_6",
                                                                                'value' => round($fields->PropertyPercentage_6),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_6",
                                                                                'id'    => "PropertyRentSqFtPercentage_6",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_6, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																			<div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_6",
                                                                                'id'    => "PropertyAnnualGrowthRate_6",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_6),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_6 -->                                                               
                                                                <div class="panel-field" id="OperatingIncomeRow_7">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">7</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_7",
                                                                                'id'    => "PropertyNoOfUnit_7",
                                                                                'value' => $fields->PropertyNoOfUnit_7,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                           <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_7" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_7', $options, $fields->PropertyUnitType_7, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_7",
                                                                                'id'    => "PropertySquareFeet_7",
                                                                                'value' => $fields->PropertySquareFeet_7,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_7",
                                                                                'id'    => "PropertyMonthlyRent_7",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_7, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_7",
                                                                                'id'    => "PropertyAnnualRent_7",
                                                                                'value' => number_format($fields->PropertyAnnualRent_7, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_7",
                                                                                'id'    => "PropertyPercentage_7",
                                                                                'value' => round($fields->PropertyPercentage_7),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_7",
                                                                                'id'    => "PropertyRentSqFtPercentage_7",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_7, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																			<div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_7",
                                                                                'id'    => "PropertyAnnualGrowthRate_7",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_7),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_7 -->                                                                
                                                                <div class="panel-field" id="OperatingIncomeRow_8">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">8</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_8",
                                                                                'id'    => "PropertyNoOfUnit_8",
                                                                                'value' => $fields->PropertyNoOfUnit_8,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                           <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_8" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_8', $options, $fields->PropertyUnitType_8, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_8",
                                                                                'id'    => "PropertySquareFeet_8",
                                                                                'value' => $fields->PropertySquareFeet_8,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_8",
                                                                                'id'    => "PropertyMonthlyRent_8",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_8, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_8",
                                                                                'id'    => "PropertyAnnualRent_8",
                                                                                'value' => number_format($fields->PropertyAnnualRent_8, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_8",
                                                                                'id'    => "PropertyPercentage_8",
                                                                                'value' => round($fields->PropertyPercentage_8),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_8",
                                                                                'id'    => "PropertyRentSqFtPercentage_8",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_8, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																			<div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_8",
                                                                                'id'    => "PropertyAnnualGrowthRate_8",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_8),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_8 -->                                                                
                                                                <div class="panel-field" id="OperatingIncomeRow_9">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">9</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_9",
                                                                                'id'    => "PropertyNoOfUnit_9",
                                                                                'value' => $fields->PropertyNoOfUnit_9,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_9" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_9', $options, $fields->PropertyUnitType_9, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_9",
                                                                                'id'    => "PropertySquareFeet_9",
                                                                                'value' => $fields->PropertySquareFeet_9,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_9",
                                                                                'id'    => "PropertyMonthlyRent_9",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_9, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_9",
                                                                                'id'    => "PropertyAnnualRent_9",
                                                                                'value' => number_format($fields->PropertyAnnualRent_9, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_9",
                                                                                'id'    => "PropertyPercentage_9",
                                                                                'value' => round($fields->PropertyPercentage_9),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_9",
                                                                                'id'    => "PropertyRentSqFtPercentage_9",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_9, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																			<div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                                
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_9",
                                                                                'id'    => "PropertyAnnualGrowthRate_9",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_9),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_9 -->                                                                
                                                                <div class="panel-field" id="OperatingIncomeRow_10">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">10</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_10",
                                                                                'id'    => "PropertyNoOfUnit_10",
                                                                                'value' => $fields->PropertyNoOfUnit_10,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_10" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_10', $options, $fields->PropertyUnitType_10, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_10",
                                                                                'id'    => "PropertySquareFeet_10",
                                                                                'value' => $fields->PropertySquareFeet_10,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_10",
                                                                                'id'    => "PropertyMonthlyRent_10",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_10, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_10",
                                                                                'id'    => "PropertyAnnualRent_10",
                                                                                'value' => number_format($fields->PropertyAnnualRent_10, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_10",
                                                                                'id'    => "PropertyPercentage_10",
                                                                                'value' => round($fields->PropertyPercentage_10),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_10",
                                                                                'id'    => "PropertyRentSqFtPercentage_10",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_10, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_10",
                                                                                'id'    => "PropertyAnnualGrowthRate_10",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_10),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_10 -->                                                                
                                                                <div class="panel-field" id="OperatingIncomeRow_11">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">11</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_11",
                                                                                'id'    => "PropertyNoOfUnit_11",
                                                                                'value' => $fields->PropertyNoOfUnit_11,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                           <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_11" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_11', $options, $fields->PropertyUnitType_11, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_11",
                                                                                'id'    => "PropertySquareFeet_11",
                                                                                'value' => $fields->PropertySquareFeet_11,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_11",
                                                                                'id'    => "PropertyMonthlyRent_11",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_11, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_11",
                                                                                'id'    => "PropertyAnnualRent_11",
                                                                                'value' => number_format($fields->PropertyAnnualRent_11, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_11",
                                                                                'id'    => "PropertyPercentage_11",
                                                                                'value' => round($fields->PropertyPercentage_11),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_11",
                                                                                'id'    => "PropertyRentSqFtPercentage_11",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_11, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																			<div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_11",
                                                                                'id'    => "PropertyAnnualGrowthRate_11",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_11),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_11 -->                                                                
                                                                <div class="panel-field" id="OperatingIncomeRow_12">
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">12</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_12",
                                                                                'id'    => "PropertyNoOfUnit_12",
                                                                                'value' => $fields->PropertyNoOfUnit_12,
                                                                                'class' => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                           <?php 
                                                                            $options = array(1 => "1 Br", 2=> "2 Br", 3 => "3 Br", 4 => "4 Br", 5 => "5 Br", 6 => "Studio", 7 => "Storage", 8 => "Garage", 9 => "Retail Store");
                                                                            $attrb = 'id="PropertyUnitType_12" class="form-control Required" data-popover="true" data-content=""'; ?>
																			<?=form_dropdown('PropertyUnitType_12', $options, $fields->PropertyUnitType_12, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_12",
                                                                                'id'    => "PropertySquareFeet_12",
                                                                                'value' => $fields->PropertySquareFeet_12,
                                                                                'class'  => 'form-control NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyMonthlyRent_12",
                                                                                'id'    => "PropertyMonthlyRent_12",
                                                                                'value' => number_format($fields->PropertyMonthlyRent_12, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>																				
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualRent_12",
                                                                                'id'    => "PropertyAnnualRent_12",
                                                                                'value' => number_format($fields->PropertyAnnualRent_12, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyPercentage_12",
                                                                                'id'    => "PropertyPercentage_12",
                                                                                'value' => round($fields->PropertyPercentage_12),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyRentSqFtPercentage_12",
                                                                                'id'    => "PropertyRentSqFtPercentage_12",
                                                                                'value' => number_format($fields->PropertyRentSqFtPercentage_12, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																			<div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                               
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyAnnualGrowthRate_12",
                                                                                'id'    => "PropertyAnnualGrowthRate_12",
                                                                                'value' => round($fields->PropertyAnnualGrowthRate_12),
                                                                                'class'  => 'form-control number-field NumberOnly growth_check percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
																				'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingIncomeRow_12 -->
                                                                
                                                                <div class="panel-field" id="OperatingIncomeRow_Total" style="margin-right:0;">
                                                                	<hr/>
                                                                    <div class="row">
                                                                        <div class="col-xs-1 text-center">Total</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyNoOfUnit_Total",
                                                                                'id'    => "PropertyNoOfUnit_Total",
                                                                                'value' => $fields->PropertyNoOfUnit_Total,
                                                                                'class'  => 'form-control',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
																				<?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2"></div>
                                                                        <div class="col-xs-1" style="padding:0 10px 0 0;">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertySquareFeet_Total",
                                                                                'id'    => "PropertySquareFeet_Total",
                                                                                'value' => $fields->PropertySquareFeet_Total,
                                                                                'class'  => 'form-control',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
																			<?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-7"></div>
                                                                    </div><hr/>
                                                                </div><!-- OperatingIncomeRow_Total -->
                                                                
                                                                <div class="panel-field" id="GrossScheduledIncomeRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">Gross Scheduled Income</div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "GrossScheduledIncomeMonthly",
                                                                                'id'    => "GrossScheduledIncomeMonthly",
                                                                                'value' => number_format($fields->GrossScheduledIncomeMonthly, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "GrossScheduledIncomeAnnual",
                                                                                'id'    => "GrossScheduledIncomeAnnual",
                                                                                'value' => number_format($fields->GrossScheduledIncomeAnnual, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "GrossScheduledIncomePercentage",
                                                                                'id'    => "GrossScheduledIncomePercentage",
                                                                                'value' => round($fields->GrossScheduledIncomePercentage),
                                                                                'class'  => 'form-control number-field percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2"></div>
                                                                    </div>
                                                                </div><!-- GrossScheduledIncomeRow -->
                                                                <div class="panel-field" id="VacancyLossRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-3">VACANCY LOSS</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "VacancyLossPercentage",
                                                                                'id'    => "VacancyLossPercentage",
                                                                                'value' => round($fields->VacancyLossPercentage),
                                                                                'class'  => 'form-control number-field NumberOnly percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "VacancyLossMonthly",
                                                                                'id'    => "VacancyLossMonthly",
                                                                                'value' => number_format($fields->VacancyLossMonthly, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "VacancyLossAnnual",
                                                                                'id'    => "VacancyLossAnnual",
                                                                                'value' => number_format($fields->VacancyLossAnnual, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2"></div>
                                                                    </div>
                                                                </div><!-- VacancyLossRow -->                                                        
                                                                <div class="panel-field" id="ConcessionsRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-3">CONCESSIONS</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ConcessionsParcentage",
                                                                                'id'    => "ConcessionsParcentage",
                                                                                'value' => round($fields->ConcessionsParcentage),
                                                                                'class'  => 'form-control number-field NumberOnly percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ConcessionsMonthly",
                                                                                'id'    => "ConcessionsMonthly",
                                                                                'value' => number_format($fields->ConcessionsMonthly, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ConcessionsAnnual",
                                                                                'id'    => "ConcessionsAnnual",
                                                                                'value' => number_format($fields->ConcessionsAnnual, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2"></div>
                                                                    </div>
                                                                </div><!-- ConcessionsRow -->                                                        
                                                                <div class="panel-field" id="ManagementFeeRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-3">MANAGEMENT FEE</div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ManagementFeePercentage",
                                                                                'id'    => "ManagementFeePercentage",
                                                                                'value' => round($fields->ManagementFeePercentage),
                                                                                'class'  => 'form-control number-field NumberOnly percentageField',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ManagementFeeMonthly",
                                                                                'id'    => "ManagementFeeMonthly",
                                                                                'value' => number_format($fields->ManagementFeeMonthly, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ManagementFeeAnnual",
                                                                                'id'    => "ManagementFeeAnnual",
                                                                                'value' => number_format($fields->ManagementFeeAnnual, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2"></div>
                                                                    </div>
                                                                </div><!-- ManagementFeeRow -->
                                                                
                                                                <div class="panel-field" id="OtherIncomeRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">Other Income</div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OtherIncomeMonthly",
                                                                                'id'    => "OtherIncomeMonthly",
                                                                                'value' => number_format($fields->OtherIncomeMonthly, 2, '.', ''),
                                                                                'class'  => 'form-control number-field NumberOnly',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OtherIncomeAnnual",
                                                                                'id'    => "OtherIncomeAnnual",
                                                                                'value' => number_format($fields->OtherIncomeAnnual, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2"></div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OtherIncomeAnnualGrowthRate",
                                                                                'id'    => "OtherIncomeAnnualGrowthRate",
                                                                                'value' => round($fields->OtherIncomeAnnualGrowthRate),
                                                                                'class'  => 'form-control number-field NumberOnly percentageField',
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OtherIncomeRow -->
                                                                <div class="panel-field" id="GrossOperatingIncomeRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">Gross Operating Income (Effective Gross Income)</div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingIncomeMonthlyTotal",
                                                                                'id'    => "OperatingIncomeMonthlyTotal",
                                                                                'value' => number_format($fields->OperatingIncomeMonthlyTotal, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingIncomeAnnualTotal",
                                                                                'id'    => "OperatingIncomeAnnualTotal",
                                                                                'value' => number_format($fields->OperatingIncomeAnnualTotal, 2, '.', ''),
                                                                                'class'  => 'form-control number-field',
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3"></div>
                                                                    </div>
                                                                </div><!-- GrossOperatingIncomeRow -->
                                                                
                                                                <div class="panel-field text-right" style="margin-right:0;">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="0">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep2 on_next_section" data-rel="20">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="rental_operating_income" />
                                                                </div><!-- Next Button -->
                                                            
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP 2: SECTION 1: OPERATING INCOME (RENT) -->
                                                        
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>OPERATING EXPENSES</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child" id="Section2OptExpense"><!-- STEP (2) -->
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons" style="margin-right:0;">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="10">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep3 on_next_section" data-rel="25">NEXT</button>
                                                                </div><!-- BACK NEXT Button -->
                                                                
                                                                <div class="panel-field" id="OperatingExpenseHeader" style="margin-right:0;">
                                                                    <div class="row">
                                                                        <label class="col-xs-4"></label>
                                                                        <label class="col-xs-3 text-center">Monthly</label>
                                                                        <label class="col-xs-2 text-center">Annual</label>
                                                                        <label class="col-xs-2 text-center">% of Net Income</label>
                                                                        <label class="col-xs-1 text-center" style="padding-left:0">
                                                                        	<button type="button" class="btn btn-md btn-default btn-block" data-target="#expense_growth_rate" data-toggle="modal">Growth<br />Rate</button>
                                                                        </label>
                                                                    </div>
                                                                </div><!-- OperatingExpenseHeader -->
                                                                <div class="panel-field" id="OperatingExpenseRow_1">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs/Reserves' => 'Repairs/Reserves',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_1" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_1', $options, $fields->OperatingExpensesOption_1, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_1",
                                                                                'id'    => "OperatingExpenseMonthly_1",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_1, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_1",
                                                                                'id'    => "OperatingExpenseAnnual_1",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_1, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_1",
                                                                                'id'    => "OperatingExpensePercentage_1",
                                                                                'value' => round($fields->OperatingExpensePercentage_1),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_1",
                                                                                'id'    => "OperatingExpenseGrowthRate_1",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_1),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_1 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_2">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_2" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_2', $options, $fields->OperatingExpensesOption_2, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_2",
                                                                                'id'    => "OperatingExpenseMonthly_2",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_2, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_2",
                                                                                'id'    => "OperatingExpenseAnnual_2",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_2, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_2",
                                                                                'id'    => "OperatingExpensePercentage_2",
                                                                                'value' => round($fields->OperatingExpensePercentage_2),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_2",
                                                                                'id'    => "OperatingExpenseGrowthRate_2",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_2),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_2 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_3">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_3" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_3', $options, $fields->OperatingExpensesOption_3, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_3",
                                                                                'id'    => "OperatingExpenseMonthly_3",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_3, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_3",
                                                                                'id'    => "OperatingExpenseAnnual_3",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_3, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_3",
                                                                                'id'    => "OperatingExpensePercentage_3",
                                                                                'value' => round($fields->OperatingExpensePercentage_3),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_3",
                                                                                'id'    => "OperatingExpenseGrowthRate_3",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_3),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_3 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_4">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_4" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_4', $options, $fields->OperatingExpensesOption_4, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_4",
                                                                                'id'    => "OperatingExpenseMonthly_4",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_4, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_4",
                                                                                'id'    => "OperatingExpenseAnnual_4",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_4, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_4",
                                                                                'id'    => "OperatingExpensePercentage_4",
                                                                                'value' => round($fields->OperatingExpensePercentage_4),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_4",
                                                                                'id'    => "OperatingExpenseGrowthRate_4",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_4),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_4 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_5">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_5" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_5', $options, $fields->OperatingExpensesOption_5, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_5",
                                                                                'id'    => "OperatingExpenseMonthly_5",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_5, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_5",
                                                                                'id'    => "OperatingExpenseAnnual_5",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_5, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_5",
                                                                                'id'    => "OperatingExpensePercentage_5",
                                                                                'value' => round($fields->OperatingExpensePercentage_5),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_5",
                                                                                'id'    => "OperatingExpenseGrowthRate_5",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_5),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_5 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_6">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_6" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_6', $options, $fields->OperatingExpensesOption_6, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_6",
                                                                                'id'    => "OperatingExpenseMonthly_6",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_6, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_6",
                                                                                'id'    => "OperatingExpenseAnnual_6",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_6, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_6",
                                                                                'id'    => "OperatingExpensePercentage_6",
                                                                                'value' => round($fields->OperatingExpensePercentage_6),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_6",
                                                                                'id'    => "OperatingExpenseGrowthRate_6",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_6),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_6 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_7">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_7" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_7', $options, $fields->OperatingExpensesOption_7, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_7",
                                                                                'id'    => "OperatingExpenseMonthly_7",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_7, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_7",
                                                                                'id'    => "OperatingExpenseAnnual_7",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_7, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_7",
                                                                                'id'    => "OperatingExpensePercentage_7",
                                                                                'value' => round($fields->OperatingExpensePercentage_7),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_7",
                                                                                'id'    => "OperatingExpenseGrowthRate_7",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_7),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_7 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_8">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_8" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_8', $options, $fields->OperatingExpensesOption_8, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_8",
                                                                                'id'    => "OperatingExpenseMonthly_8",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_8, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_8",
                                                                                'id'    => "OperatingExpenseAnnual_8",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_8, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_8",
                                                                                'id'    => "OperatingExpensePercentage_8",
                                                                                'value' => round($fields->OperatingExpensePercentage_8),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_8",
                                                                                'id'    => "OperatingExpenseGrowthRate_8",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_8),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_8 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_9">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_9" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_9', $options, $fields->OperatingExpensesOption_9, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_9",
                                                                                'id'    => "OperatingExpenseMonthly_9",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_9, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_9",
                                                                                'id'    => "OperatingExpenseAnnual_9",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_9, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_9",
                                                                                'id'    => "OperatingExpensePercentage_9",
                                                                                'value' => round($fields->OperatingExpensePercentage_9),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_9",
                                                                                'id'    => "OperatingExpenseGrowthRate_9",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_9),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_9 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_10">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_10" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_10', $options, $fields->OperatingExpensesOption_10, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_10",
                                                                                'id'    => "OperatingExpenseMonthly_10",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_10, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_10",
                                                                                'id'    => "OperatingExpenseAnnual_10",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_10, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_10",
                                                                                'id'    => "OperatingExpensePercentage_10",
                                                                                'value' => round($fields->OperatingExpensePercentage_10),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_10",
                                                                                'id'    => "OperatingExpenseGrowthRate_10",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_10),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_10 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_11">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_11" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_11', $options, $fields->OperatingExpensesOption_11, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_11",
                                                                                'id'    => "OperatingExpenseMonthly_11",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_11, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_11",
                                                                                'id'    => "OperatingExpenseAnnual_11",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_11, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_11",
                                                                                'id'    => "OperatingExpensePercentage_11",
                                                                                'value' => round($fields->OperatingExpensePercentage_11),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_11",
                                                                                'id'    => "OperatingExpenseGrowthRate_11",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_11),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_11 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_12">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_12" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_12', $options, $fields->OperatingExpensesOption_12, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_12",
                                                                                'id'    => "OperatingExpenseMonthly_12",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_12, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_12",
                                                                                'id'    => "OperatingExpenseAnnual_12",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_12, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_12",
                                                                                'id'    => "OperatingExpensePercentage_12",
                                                                                'value' => round($fields->OperatingExpensePercentage_12),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_12",
                                                                                'id'    => "OperatingExpenseGrowthRate_12",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_12),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_12 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_13">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_13" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_13', $options, $fields->OperatingExpensesOption_13, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_13",
                                                                                'id'    => "OperatingExpenseMonthly_13",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_13, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_13",
                                                                                'id'    => "OperatingExpenseAnnual_13",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_13, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_13",
                                                                                'id'    => "OperatingExpensePercentage_13",
                                                                                'value' => round($fields->OperatingExpensePercentage_13),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_13",
                                                                                'id'    => "OperatingExpenseGrowthRate_13",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_13),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_13 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_14">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_14" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_14', $options, $fields->OperatingExpensesOption_14, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_14",
                                                                                'id'    => "OperatingExpenseMonthly_14",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_14, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_14",
                                                                                'id'    => "OperatingExpenseAnnual_14",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_14, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_14",
                                                                                'id'    => "OperatingExpensePercentage_14",
                                                                                'value' => round($fields->OperatingExpensePercentage_14),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_14",
                                                                                'id'    => "OperatingExpenseGrowthRate_14",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_14),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_14 -->
                                                                <div class="panel-field" id="OperatingExpenseRow_15">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                '' => '',
                                                                                'Accounting' => 'Accounting',
                                                                                'Advertising' => 'Advertising',
                                                                                'Bank Charges' => 'Bank Charges',
                                                                                'Condo Association Fees' => 'Condo Association Fees',
																				'Exterminator' => 'Exterminator',
                                                                                'Furniture Rental' => 'Furniture Rental',
																				'Garbage' => 'Garbage',
																				'General Admin' => 'General Admin',
                                                                                'Insurance' => 'Insurance',
                                                                                'Janitorial' => 'Janitorial',
                                                                                'Landscape' => 'Landscape',
                                                                                'Legal' => 'Legal',
                                                                                'Licenses' => 'Licenses',
																				'Management' => 'Management',
                                                                                'Miscellanious' => 'Miscellanious',
                                                                                'Other' => 'Other',
                                                                                'Phone' => 'Phone',
                                                                                'Referalls or commissions' => 'Referalls or commissions',
                                                                                'Repairs and Maintenance' => 'Repairs and Maintenance',
                                                                                'Resident Superintendent' => 'Resident Superintendent',
                                                                                'Salaries & Related' => 'Salaries & Related',
																				'Sewer Expense' => 'Sewer Expense', 
                                                                                'Snow Removal' => 'Snow Removal',
                                                                                'Super' => 'Super',
                                                                                'Supplies Office' => 'Supplies Office',																				
                                                                                'Taxes - Other' => 'Taxes - Other',
                                                                                'Taxes - Payroll' => 'Taxes - Payroll',
                                                                                'Taxes' => 'Taxes',
                                                                                'Warranties' => 'Warranties',
                                                                            );
                                                                            $attrb = 'id="OperatingExpensesOption_15" class="form-control"'; ?>
                                                                            <?=form_dropdown('OperatingExpensesOption_15', $options, $fields->OperatingExpensesOption_15, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseMonthly_15",
                                                                                'id'    => "OperatingExpenseMonthly_15",
                                                                                'value' => number_format($fields->OperatingExpenseMonthly_15, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseAnnual_15",
                                                                                'id'    => "OperatingExpenseAnnual_15",
                                                                                'value' => number_format($fields->OperatingExpenseAnnual_15, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensePercentage_15",
                                                                                'id'    => "OperatingExpensePercentage_15",
                                                                                'value' => round($fields->OperatingExpensePercentage_15),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGrowthRate_15",
                                                                                'id'    => "OperatingExpenseGrowthRate_15",
                                                                                'value' => round($fields->OperatingExpenseGrowthRate_15),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseRow_15 -->
                                                                
                                                                <div class="panel-field" id="UtilitiesRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-12"><strong>Utilities:</strong></div>
                                                                    </div>
                                                                </div><!-- Utilities -->
                                                                <div class="panel-field" id="OperatingExpenseWaterSewerRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">&nbsp;&nbsp;&nbsp;Water/Sewer</div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseWaterSewerMonthly",
                                                                                'id'    => "OperatingExpenseWaterSewerMonthly",
                                                                                'value' => number_format($fields->OperatingExpenseWaterSewerMonthly, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseWaterSewerAnnual",
                                                                                'id'    => "OperatingExpenseWaterSewerAnnual",
                                                                                'value' => number_format($fields->OperatingExpenseWaterSewerAnnual, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseWaterSewerPercentage",
                                                                                'id'    => "OperatingExpenseWaterSewerPercentage",
                                                                                'value' => round($fields->OperatingExpenseWaterSewerPercentage),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseWaterSewerGrowthRate",
                                                                                'id'    => "OperatingExpenseWaterSewerGrowthRate",
                                                                                'value' => round($fields->OperatingExpenseWaterSewerGrowthRate),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!--    Water/Sewer -->
                                                                <div class="panel-field" id="OperatingExpenseElectricityRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">&nbsp;&nbsp;&nbsp;Electricity</div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseElectricityMonthly",
                                                                                'id'    => "OperatingExpenseElectricityMonthly",
                                                                                'value' => number_format($fields->OperatingExpenseElectricityMonthly, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseElectricityAnnual",
                                                                                'id'    => "OperatingExpenseElectricityAnnual",
                                                                                'value' => number_format($fields->OperatingExpenseElectricityAnnual, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseElectricityPercentage",
                                                                                'id'    => "OperatingExpenseElectricityPercentage",
                                                                                'value' => round($fields->OperatingExpenseElectricityPercentage),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseElectricityGrowthRate",
                                                                                'id'    => "OperatingExpenseElectricityGrowthRate",
                                                                                'value' => round($fields->OperatingExpenseElectricityGrowthRate),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!--    Electricity -->
                                                                <div class="panel-field" id="OperatingExpenseGasRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">&nbsp;&nbsp;&nbsp;Gas</div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGasMonthly",
                                                                                'id'    => "OperatingExpenseGasMonthly",
                                                                                'value' => number_format($fields->OperatingExpenseGasMonthly, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGasAnnual",
                                                                                'id'    => "OperatingExpenseGasAnnual",
                                                                                'value' => number_format($fields->OperatingExpenseGasAnnual, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGasPercentage",
                                                                                'id'    => "OperatingExpenseGasPercentage",
                                                                                'value' => round($fields->OperatingExpenseGasPercentage),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseGasGrowthRate",
                                                                                'id'    => "OperatingExpenseGasGrowthRate",
                                                                                'value' => round($fields->OperatingExpenseGasGrowthRate),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!--    Gas -->
                                                                <div class="panel-field" id="OperatingExpenseFuelOilRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">&nbsp;&nbsp;&nbsp;Fuel Oil</div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseFuelOilMonthly",
                                                                                'id'    => "OperatingExpenseFuelOilMonthly",
                                                                                'value' => number_format($fields->OperatingExpenseFuelOilMonthly, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseFuelOilAnnual",
                                                                                'id'    => "OperatingExpenseFuelOilAnnual",
                                                                                'value' => number_format($fields->OperatingExpenseFuelOilAnnual, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseFuelOilPercentage",
                                                                                'id'    => "OperatingExpenseFuelOilPercentage",
                                                                                'value' => round($fields->OperatingExpenseFuelOilPercentage),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseFuelOilGrowthRate",
                                                                                'id'    => "OperatingExpenseFuelOilGrowthRate",
                                                                                'value' => round($fields->OperatingExpenseFuelOilGrowthRate),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!--    Fuel Oil -->
                                                                <div class="panel-field" id="OperatingExpenseOtherUtilitiesRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">&nbsp;&nbsp;&nbsp;Other Utilities</div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseOtherUtilitiesMonthly",
                                                                                'id'    => "OperatingExpenseOtherUtilitiesMonthly",
                                                                                'value' => number_format($fields->OperatingExpenseOtherUtilitiesMonthly, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseOtherUtilitiesAnnual",
                                                                                'id'    => "OperatingExpenseOtherUtilitiesAnnual",
                                                                                'value' => number_format($fields->OperatingExpenseOtherUtilitiesAnnual, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseOtherUtilitiesPercentage",
                                                                                'id'    => "OperatingExpenseOtherUtilitiesPercentage",
                                                                                'value' => round($fields->OperatingExpenseOtherUtilitiesPercentage),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseOtherUtilitiesGrowthRate",
                                                                                'id'    => "OperatingExpenseOtherUtilitiesGrowthRate",
                                                                                'value' => round($fields->OperatingExpenseOtherUtilitiesGrowthRate),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!--    Other Utilities -->
                                                                <div class="panel-field" id="OperatingExpenseReservesRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4">Reserves</div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseReservesMonthly",
                                                                                'id'    => "OperatingExpenseReservesMonthly",
                                                                                'value' => number_format($fields->OperatingExpenseReservesMonthly, 2, '.', ''),
                                                                                'class'  => "form-control NumberOnly number-field",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseReservesAnnual",
                                                                                'id'    => "OperatingExpenseReservesAnnual",
                                                                                'value' => number_format($fields->OperatingExpenseReservesAnnual, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseReservesPercentage",
                                                                                'id'    => "OperatingExpenseReservesPercentage",
                                                                                'value' => round($fields->OperatingExpenseReservesPercentage),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseReservesGrowthRate",
                                                                                'id'    => "OperatingExpenseReservesGrowthRate",
                                                                                'value' => round($fields->OperatingExpenseReservesGrowthRate),
                                                                                'class'  => "form-control NumberOnly expense_check percentageField",
																				"readonly" => "readonly",
                                                                                "data-popover" => "true",
                                                                                "data-content" => ""
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!--    Reserves -->
                                                                
                                                                <div class="panel-field" id="OperatingExpenseTotalRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4"><strong>Total Operating expenses</strong></div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseTotalMonthly",
                                                                                'id'    => "OperatingExpenseTotalMonthly",
                                                                                'value' => number_format($fields->OperatingExpenseTotalMonthly, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseTotalAnnual",
                                                                                'id'    => "OperatingExpenseTotalAnnual",
                                                                                'value' => number_format($fields->OperatingExpenseTotalAnnual, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenseTotalPercentage",
                                                                                'id'    => "OperatingExpenseTotalPercentage",
                                                                                'value' => round($fields->OperatingExpenseTotalPercentage),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                    </div>
                                                                </div><!-- OperatingExpenseTotal -->
                                                                <div class="panel-field hide" id="OperatingExpensesIncomeRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4"><strong>% Operating expenses to Income</strong></div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpensesIncomeMonthly",
                                                                                'id'    => "OperatingExpensesIncomeMonthly",
                                                                                'value' => round($fields->OperatingExpensesIncomeMonthly),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-5"></div>
                                                                    </div>
                                                                </div><!-- OperatingExpensesIncome -->
                                                                <div class="panel-field" id="NetOperatingIncomeRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-4"><strong>Net Operating income</strong></div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "NetOperatingIncomeMonthly",
                                                                                'id'    => "NetOperatingIncomeMonthly",
                                                                                'value' => number_format($fields->NetOperatingIncomeMonthly, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "NetOperatingIncomeAnnual",
                                                                                'id'    => "NetOperatingIncomeAnnual",
                                                                                'value' => number_format($fields->NetOperatingIncomeAnnual, 2, '.', ''),
                                                                                'class'  => "form-control",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "NetOperatingIncomePercentage",
                                                                                'id'    => "NetOperatingIncomePercentage",
                                                                                'value' => round($fields->NetOperatingIncomePercentage),
                                                                                'class'  => "form-control percentageField",
                                                                                "data-popover" => "true",
                                                                                "data-content" => "",
                                                                                "readonly" => "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-1"></div>
                                                                    </div>
                                                                </div><!-- NetOperatingIncome -->
                                                                
                                                                <div class="panel-field text-right" style="margin-right:0;">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="10">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep3 on_next_section" data-rel="25">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="rental_operating_expenses" />
                                                                </div><!-- BACK NEXT Button -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>    
                                                        <div class="clearfix"></div>      
                                                    </div><!-- STEP 3: SECTION 2: OPERATING EXPENSES -->
                                                    
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>PURCHASE PRICE, HOLDING PERIOD & IRR ASSUMPTIONS</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child"><!-- STEP (3) -->
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="20">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep4 on_next_section" data-rel="30">NEXT</button>
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                                <div class="panel-field" id="PurchasePriceRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-9"><strong>Purchase Price</strong></label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PurchasePrice",
                                                                                'id'    => "PurchasePrice",
                                                                                'value' => number_format($fields->PurchasePrice, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- PurchasePrice -->
                                                                <div class="panel-field" id="OverrideInitialValueRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">
                                                                        	Override Initial Market Value?
                                                                            <a href="#" data-target="#MarketValueModal" data-toggle="modal"><i class="fa fa-info-circle"></i></a>
                                                                            <a href="#"><i class="fa fa-volume-up"></i></a>
                                                                        </label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes' => 'Yes',
                                                                                'No'  => 'No',
                                                                            );
                                                                            $attrb = 'id="OverrideInitialValue" class="form-control"'; ?>
                                                                            <?=form_dropdown('OverrideInitialValue', $options, $fields->OverrideInitialValue, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "InitialMarketValue",
                                                                                'id'    => "InitialMarketValue",
                                                                                'value' => number_format($fields->InitialMarketValue, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                "readonly" => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OverrideInitialValue -->
                                                                <div class="panel-field" id="FirstYearMonthlyCashflowRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-9">1st Year Monthly Cashflow (Pre-Tax)</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FirstYearMonthlyCashflow",
                                                                                'id'    => "FirstYearMonthlyCashflow",
                                                                                'value' => number_format($fields->FirstYearMonthlyCashflow, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- YearMonthlyCashflow -->
                                                                <div class="panel-field" id="FirstYearAnnualCOCReturnRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-9">1st Year Annual Cash on Cash Return (Pre-Tax)</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FirstYearAnnualCOCReturn",
                                                                                'id'    => "FirstYearAnnualCOCReturn",
                                                                                'value' => round($fields->FirstYearAnnualCOCReturn),
                                                                                'class'	=> 'form-control number-field percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- 1YearAnnualCOCReturn -->
                                                                <div class="panel-field hide" id="RentGrowthOverideRow">
                                                                    <div class="row">
                                                                        <label class="col-lg-9">Same Rent Growth Rate for Each Unit Type?</label>
                                                                        <div class="col-lg-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes' => 'Yes',
                                                                                'No'  => 'No',
                                                                            );
                                                                            $attrb = 'id="RentGrowthOveride" class="form-control"'; ?>
                                                                            <?=form_dropdown('RentGrowthOveride', $options, $fields->RentGrowthOveride, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- RentGrowthOveride -->
                                                                <div class="panel-field hide" id="RentGrowthRateRow">
                                                                    <div class="row">
                                                                        <label class="col-lg-9">Annual Growth Rate for all Apartments</label>
                                                                        <div class="col-lg-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "RentGrowthRate",
                                                                                'id'    => "RentGrowthRate",
                                                                                'value' => $fields->RentGrowthRate,
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- RentGrowthRate -->
                                                                <div class="panel-field hide" id="OpExpensesGrowthOverrideRow">
                                                                    <div class="row">
                                                                        <label class="col-lg-9">Same Growth Rate for Each Op Expense?</label>
                                                                        <div class="col-lg-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes' => 'Yes',
                                                                                'No'  => 'No',
                                                                            );
                                                                            $attrb = 'id="OpExpensesGrowthOverride" class="form-control"'; ?>
                                                                            <?=form_dropdown('OpExpensesGrowthOverride', $options, $fields->OpExpensesGrowthOverride, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OpExpensesGrowthOverride -->
                                                                <div class="panel-field hide" id="OpExpensesGrowthRateRow">
                                                                    <div class="row">
                                                                        <label class="col-lg-9">Annual Growth Rate for all Apartments</label>
                                                                        <div class="col-lg-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OpExpensesGrowthRate",
                                                                                'id'    => "OpExpensesGrowthRate",
                                                                                'value' => $fields->OpExpensesGrowthRate,
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OpExpensesGrowthRate -->  
                                                                
                                                                <div class="panel-field text-right">
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="rental_price_assumptions" />
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field" id="YearSaleRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-9">Holding Period (in Years)</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24, 25 => 25, 26 => 26, 27 => 27, 28 => 28, 29 => 29, 30 => 30, 31 => 31, 32 => 32, 33 => 33, 34 => 34, 35 => 35, 36 => 36, 37 => 37, 38 => 38, 39 => 39, 40 => 40);
                                                                            $attrb = 'id="YearSale" class="form-control" data-popover="true" data-content=""'; ?>
                                                                            <?=form_dropdown('YearSale', $options, $fields->YearSale, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- YearSale -->
                                                                <div class="panel-field" id="ResaleRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-9">Type of Future Market Value Calculation:</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Set Cap Rate' => 'Set Cap Rate',
                                                                                'Assume Annual Appreciation%'  => 'Assume Annual Appreciation%',
                                                                            );
                                                                            $attrb = 'id="Resale" class="form-control"'; ?>
                                                                            <?=form_dropdown('Resale', $options, $fields->Resale, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Resale -->
                                                                <div class="panel-field" id="AppreciationRateRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-9">Resale Cap Rate Assumed As:</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "AppreciationRate",
                                                                                'id'    => "AppreciationRate",
                                                                                'value' => round($fields->AppreciationRate),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- AppreciationRate -->
                                                                <div class="panel-field" id="DiscRateRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-9">Discount Rate for Future Market Value</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "DiscRate",
                                                                                'id'    => "DiscRate",
                                                                                'value' => round($fields->DiscRate),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- DiscRate -->
                                                                
                                                                <div class="panel-field" id="MIRRCalculationRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-12"><strong>For MIRR Calculation:</strong></label>
                                                                    </div>
                                                                </div><!-- MIRRCalculation -->
                                                                <div class="panel-field" id="EquityInterestRatePaidRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-9">Interest Rate Paid for Equity</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "EquityInterestRatePaid",
                                                                                'id'    => "EquityInterestRatePaid",
                                                                                'value' => round($fields->EquityInterestRatePaid),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- EquityInterestRatePaid -->
                                                                <div class="panel-field" id="ReinvestmentRateReceivedRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-9">Reinvestment Rate Received</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ReinvestmentRateReceived",
                                                                                'id'    => "ReinvestmentRateReceived",
                                                                                'value' => round($fields->ReinvestmentRateReceived),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- ReinvestmentRateReceived -->
                                                                <div class="panel-field" id="PurchaseDateRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-9">Purchase Date (MM/DD/YYYY)</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PurchaseDate",
                                                                                'id'    => "PurchaseDate",
                                                                                'value' => $fields->PurchaseDate,
                                                                                'class'	=> 'form-control',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- PurchaseDate -->                                                        
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="25">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep5 on_next_section" data-rel="40">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="rental_holding_resale" />
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP 5: Holding period, Resale and IRR Assumptions: -->
                                                    
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>TAX ASSUMPTIONS</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child"><!-- STEP (3) -->
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="30">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep6 on_next_section" data-rel="50">NEXT</button>
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                                <div class="panel-field" id="DesignationRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Designation for Handling Passive Losses:</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Real Estate Professional'  => 'Real Estate Professional',
                                                                                'Passive Participant'  => 'Passive Participant',
                                                                                'Active Participant' => 'Active Participant',
                                                                            );
                                                                            $attrb = 'id="Designation" class="form-control"'; ?>
                                                                            <?=form_dropdown('Designation', $options, $fields->Designation, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Designation -->
																	<div class="panel-field <?php echo ($fields->Designation!='Active Participant' ? 'hide' : ''); ?>" id="GAIRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Gross Annual Income</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "GAI",
                                                                                'id'    => "GAI",
                                                                                'value' => number_format($fields->GAI, 2, '.', ''),
                                                                                'class'	=> 'form-control ClosingCostsInput number-field NumberOnly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3"></div>
                                                                    </div>
                                                                </div>
                                                                <!-- GAI -->
                                                                <div class="panel-field" id="DepYearsRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Depreciate Property Over(Years):</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                '27.5'  => '27.5',
                                                                                '39'  => '39',
                                                                            );
                                                                            $attrb = 'id="DepYears" class="form-control"'; ?>
                                                                            <?=form_dropdown('DepYears', $options, $fields->DepYears, $attrb)?>
                                                                        </div>
                                                                        <!--<div class="col-xs-3">Years</div>-->
                                                                    </div>
                                                                </div><!-- DepYears -->
                                                                <div class="panel-field" id="LandPercentageRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Assume Land to be a % of Total Value</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "LandPercentage",
                                                                                'id'    => "LandPercentage",
                                                                                'value' => round($fields->LandPercentage),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <!--<div class="col-xs-3">% of Total Value</div>-->
                                                                    </div>
                                                                </div><!-- LandPercentage -->
                                                                <div class="panel-field" id="TaxBracketRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Marginal Tax Bracket(Fed + State)</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TaxBracket",
                                                                                'id'    => "TaxBracket",
                                                                                'value' => round($fields->TaxBracket),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3"></div>
                                                                    </div>
                                                                </div><!-- TaxBracket -->
                                                                <div class="panel-field" id="CapitalGainsPercentageRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Capital Gains %</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CapitalGainsPercentage",
                                                                                'id'    => "CapitalGainsPercentage",
                                                                                'value' => round($fields->CapitalGainsPercentage),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3"></div>
                                                                    </div>
                                                                </div><!-- CapitalGainsPercentage -->
                                                                <div class="panel-field" id="CostOfSaleRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Closing Costs (as a % of Projected Sales Price)</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CostOfSale",
                                                                                'id'    => "CostOfSale",
                                                                                'value' => round($fields->CostOfSale),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3"></div>
                                                                    </div>
                                                                </div><!-- CostOfSale -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="30">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep6 on_next_section" data-rel="50">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="rental_tax_assumption" />
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>   
                                                        <div class="clearfix"></div>                                             
                                                    </div><!-- STEP 6: Tax Assumptions: -->
                                                    
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>FINANCING ANALYSIS</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child"><!-- STEP (3) -->
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="40">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep7 on_next_section" data-rel="60">NEXT</button>
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                                <div class="panel-field" id="UseFinancingRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Cash or Financing?</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes'  => 'Financing',
                                                                                'No'   => 'Cash',
                                                                            );
                                                                            $attrb = 'id="UseFinancing" class="form-control"'; ?>
                                                                            <?=form_dropdown('UseFinancing', $options, $fields->UseFinancing, $attrb)?>
                                                                        </div>
                                                                        <!--<div class="col-xs-3">* Select "Cash" if an all-cash purchase</div>-->
                                                                    </div>
                                                                </div><!-- UseFinancing -->
                                                                <div class="panel-field" id="DownPaymentPercentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Down Payment %</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "DownPaymentPercent",
                                                                                'id'    => "DownPaymentPercent",
                                                                                'value' => round($fields->DownPaymentPercent),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3"></div>
                                                                    </div>
                                                                </div><!-- DownPaymentPercent -->
                                                                <div class="panel-field" id="DownPaymentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Down Payment Amount</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "DownPayment",
                                                                                'id'    => "DownPayment",
                                                                                'value' => number_format($fields->DownPayment, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                                'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3"></div>
                                                                    </div>
                                                                </div><!-- DownPayment -->
                                                                
                                                                <div class="panel-field" id="MortgageRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6"></label>
                                                                        <div class="col-xs-3 text-center">1st Mortgage</div>
                                                                        <div class="col-xs-3 text-center organheading"><?php echo($fields->SecondMortgageUsed=='No' ? '' : '2nd Mortgage') ?></div>
                                                                    </div>
                                                                </div><!-- MortgageRow -->
                                                                <div class="panel-field" id="MortgageUsedRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Used?</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes'  => 'Yes',
                                                                                'No'   => 'No',
                                                                            );
                                                                            $attrb = 'id="FirstMortgageUsed" class="form-control"'; ?>
                                                                            <?=form_dropdown('FirstMortgageUsed', $options, $fields->FirstMortgageUsed, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes'  => 'Yes',
                                                                                'No'   => 'No',
                                                                            );
                                                                            $attrb = 'id="SecondMortgageUsed" class="form-control"'; ?>
                                                                            <?=form_dropdown('SecondMortgageUsed', $options, $fields->SecondMortgageUsed, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- MortgageUsed -->
                                                                <div class="panel-field" id="MortgageLoanTypeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Loan Type</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Fixed'  		=> 'Fixed',
                                                                                'Interest Only' => 'Interest Only',
                                                                            );
                                                                            $attrb = 'id="FirstMortgageLoanType" class="form-control"'; ?>
                                                                            <?=form_dropdown('FirstMortgageLoanType', $options, $fields->FirstMortgageLoanType, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Fixed'  		=> 'Fixed',
                                                                                'Interest Only' => 'Interest Only',
                                                                            );
                                                                            $attrb = 'id="SecondMortgageLoanType" class="form-control ' . ($fields->SecondMortgageUsed=='No' ? 'hide' : '') . '"'; ?>
                                                                            <?=form_dropdown('SecondMortgageLoanType', $options, $fields->SecondMortgageLoanType, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- MortgageLoanType -->
                                                                <div class="panel-field" id="MortgageTermRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Term (In years)</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FirstMortgageTerm",
                                                                                'id'    => "FirstMortgageTerm",
                                                                                'value' => $fields->FirstMortgageTerm,
                                                                                'class'	=> 'form-control NumberOnly',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                            ); ?>
																			<?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SecondMortgageTerm",
                                                                                'id'    => "SecondMortgageTerm",
                                                                                'value' => $fields->SecondMortgageTerm,
                                                                                'class'	=> 'form-control NumberOnly ' . ($fields->SecondMortgageUsed=='No' ? 'hide' : '') . '',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                            ); ?>
																			<?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- MortgageTerm -->
                                                                <div class="panel-field" id="MortgageLTVPercentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">LTV %</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FirstMortgageLTVPercent",
                                                                                'id'    => "FirstMortgageLTVPercent",
                                                                                'value' => round($fields->FirstMortgageLTVPercent),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SecondMortgageLTVPercent",
                                                                                'id'    => "SecondMortgageLTVPercent",
                                                                                'value' => round($fields->SecondMortgageLTVPercent),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                            ); ?>
                                                                            <div class="input-group <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- MortgageLTVPercent -->
                                                                <div class="panel-field" id="MortgageAmountRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Mortgage Amount</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FirstMortgageAmount",
                                                                                'id'    => "FirstMortgageAmount",
                                                                                'value' => number_format($fields->FirstMortgageAmount, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                                "readonly"	=> "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SecondMortgageAmount",
                                                                                'id'    => "SecondMortgageAmount",
                                                                                'value' => number_format($fields->SecondMortgageAmount, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                                "readonly"	=> "readonly"
                                                                            ); ?>
                                                                            <div class="input-group <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- MortgageAmount -->
                                                                <div class="panel-field" id="MortgageRateRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Interest Rate</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FirstMortgageRate",
                                                                                'id'    => "FirstMortgageRate",
                                                                                'value' => number_format($fields->FirstMortgageRate, 3, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SecondMortgageRate",
                                                                                'id'    => "SecondMortgageRate",
                                                                                'value' => number_format($fields->SecondMortgageRate, 3, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                            ); ?>
                                                                            <div class="input-group <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- MortgageRate -->                                                        
                                                                <div class="panel-field" id="MortgagePaymentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Payment</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FirstMortgagePayment",
                                                                                'id'    => "FirstMortgagePayment",
                                                                                'value' => number_format($fields->FirstMortgagePayment, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                                "readonly"	=> "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SecondMortgagePayment",
                                                                                'id'    => "SecondMortgagePayment",
                                                                                'value' => number_format($fields->SecondMortgagePayment, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                                "readonly"	=> "readonly"
                                                                            ); ?>
                                                                            <div class="input-group <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- MortgagePayment -->                                                        
                                                                <div class="panel-field" id="MortgagePaymentTotalRow">
                                                                    <div class="row <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">
                                                                        <label class="col-xs-6">Total Payment (1st and 2nd)</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FirstMortgagePaymentTotal",
                                                                                'id'    => "FirstMortgagePaymentTotal",
                                                                                'value' => number_format($fields->FirstMortgagePaymentTotal, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                                "readonly"	=> "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3"></div>
                                                                    </div>
                                                                </div><!-- MortgagePaymentTotal -->
                                                                
                                                                <div class="panel-field" id="TotalLoanAmountRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6"><span class="<?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">Total Loan Amount</span>&nbsp;</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TotalLoanAmount",
                                                                                'id'    => "TotalLoanAmount",
                                                                                'value' => number_format($fields->TotalLoanAmount, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                                "readonly"	=> "readonly"
                                                                            ); ?>
                                                                            <div class="input-group <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>&nbsp;
                                                                        </div>
                                                                        <div class="col-xs-3">
                                                                        	<button data-toggle="modal" data-target="#HelpfulCalcModal" class="btn btn-md btn-block btn-default" type="button">Points/Rate Break Even Calculator</button>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- TotalLoanAmount -->
                                                                <div class="panel-field" id="InterestRateRow">
                                                                    <div class="row <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">
                                                                        <label class="col-xs-6">Interest Rate (Blended, % per year)</label>
                                                                        <div class="col-xs-3">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "InterestRate",
                                                                                'id'    => "InterestRate",
                                                                                'value' => round($fields->InterestRate),
                                                                                'class'	=> 'form-control number-field percentageField',
                                                                                'data-popover' => 'true',
                                                                                'data-content' => "",
                                                                                "readonly"	=> "readonly"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-3"></div>
                                                                    </div>
                                                                </div><!-- InterestRate -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="40">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep7 on_next_section" data-rel="60">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="rental_financial_analysis" />
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>    
                                                        <div class="clearfix"></div>                                            
                                                    </div><!-- STEP 7: SECTION 3: FINANCING ANALYSIS -->
                                                    
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>CASH OUTLAY</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child"><!-- STEP (3) -->
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="50">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep8 on_next_section" data-rel="70">NEXT</button>
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                                <div class="panel-field" id="MortgageRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4"></label>
                                                                        <div class="col-xs-4 text-center">Mortgage 1</div>
                                                                        <div class="col-xs-4 text-center organheading"><?php echo($fields->SecondMortgageUsed=='No' ? '' : 'Mortgage 2'); ?></div>
                                                                    </div>
                                                                </div><!-- Mortgage -->
                                                                <div class="panel-field" id="RollupRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4">Roll-up Points into Mortgage?</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes'  => 'Yes',
                                                                                'No'   => 'No',
                                                                            );
                                                                            $attrb = 'id="RollupFirst" class="form-control"'; ?>
                                                                            <?=form_dropdown('RollupFirst', $options, $fields->RollupFirst, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes'  => 'Yes',
                                                                                'No'   => 'No',
                                                                            );
                                                                            $attrb = 'id="RollUpSecond" class="form-control"'; ?>
                                                                            <?=form_dropdown('RollUpSecond', $options, $fields->RollUpSecond, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Rollup -->
                                                                <div class="panel-field" id="InspectionAppraisalRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4">Inspection and Appraisal</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "InspectionAppraisal",
                                                                                'id'    => "InspectionAppraisal",
                                                                                'value' => number_format($fields->InspectionAppraisal, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4"></div>
                                                                    </div>
                                                                </div><!-- InspectionAppraisal -->
																<div class="panel-field">
                                                                    <div class="row">
                                                                        <label class="col-xs-4"></label>
                                                                        <div class="col-xs-4 text-center">Mortgage 1</div>
                                                                        <div class="col-xs-4 text-center organheading"><?php echo($fields->SecondMortgageUsed=='No' ? '' : 'Mortgage 2') ?></div>
                                                                    </div>
                                                                </div>
                                                                <div class="panel-field" id="OriginationFeeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4">Origination fee</label>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "MortgageOneOriginationFeePercent",
                                                                                'id'    => "MortgageOneOriginationFeePercent",
                                                                                'value' => round($fields->MortgageOneOriginationFeePercent),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "MortgageOneOriginationFee",
                                                                                'id'    => "MortgageOneOriginationFee",
                                                                                'value' => number_format($fields->MortgageOneOriginationFee, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                'readonly' => 'readonly'
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2 <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : '') ?>">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "MortgageTwoOriginationFeePercent",
                                                                                'id'    => "MortgageTwoOriginationFeePercent",
                                                                                'value' => round($fields->MortgageTwoOriginationFeePercent), 
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2 <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "MortgageTwoOriginationFee",
                                                                                'id'    => "MortgageTwoOriginationFee",
                                                                                'value' => number_format($fields->MortgageTwoOriginationFee, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                'readonly' => 'readonly'
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- OriginationFee -->
                                                                <div class="panel-field" id="DiscountFeeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4">Discount fee</label>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "MortgageOneDiscountFeePercent",
                                                                                'id'    => "MortgageOneDiscountFeePercent",
                                                                                'value' => round($fields->MortgageOneDiscountFeePercent),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "MortgageOneDiscountFee",
                                                                                'id'    => "MortgageOneDiscountFee",
                                                                                'value' => number_format($fields->MortgageOneDiscountFee, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                'readonly' => 'readonly'
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2 <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "MortgageTwoDiscountFeePercent",
                                                                                'id'    => "MortgageTwoDiscountFeePercent",
                                                                                'value' => round($fields->MortgageTwoDiscountFeePercent),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2 <?php echo ($fields->SecondMortgageUsed=='No' ? 'hide' : ''); ?>">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "MortgageTwoDiscountFee",
                                                                                'id'    => "MortgageTwoDiscountFee",
                                                                                'value' => number_format($fields->MortgageTwoDiscountFee, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                'readonly' => 'readonly'
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- DiscountFee -->
                                                                <div class="panel-field" id="ClosingCostsMiscFeeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2">Closing costs & misc. fees</label>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ClosingCostsMiscFeePercent",
                                                                                'id'    => "ClosingCostsMiscFeePercent",
                                                                                'value' => round($fields->ClosingCostsMiscFeePercent),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ClosingCostsMiscFee",
                                                                                'id'    => "ClosingCostsMiscFee",
                                                                                'value' => number_format($fields->ClosingCostsMiscFee, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                'readonly' => 'readonly'
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4"></div>
                                                                    </div>
                                                                </div><!-- ClosingCostsMiscFee -->
                                                                <div class="panel-field" id="TotalSettlementRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2"><strong>Total settlement</strong></label>
                                                                        <div class="col-xs-2"></div>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TotalSettlement",
                                                                                'id'    => "TotalSettlement",
                                                                                'value' => number_format($fields->TotalSettlement, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                'readonly' => 'readonly'
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4"></div>
                                                                    </div>
                                                                </div><!-- TotalSettlement -->
                                                                <div class="panel-field" id="CashbackFromSellerRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-2"><strong>Cash back from Seller</strong></label>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CashbackFromSellerPercent",
                                                                                'id'    => "CashbackFromSellerPercent",
                                                                                'value' => round($fields->CashbackFromSellerPercent),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CashbackFromSeller",
                                                                                'id'    => "CashbackFromSeller",
                                                                                'value' => number_format($fields->CashbackFromSeller, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                                'readonly' => 'readonly'
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4"></div>
                                                                    </div>
                                                                </div><!-- CashbackFromSeller -->
                                                                <div class="panel-field" id="InitialCapitalReservesRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4"><strong>Initial Capital Reserves and rate to accrue interest</strong></label>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "InitialCapitalReserves_1",
                                                                                'id'    => "InitialCapitalReserves_1",
                                                                                'value' => number_format($fields->InitialCapitalReserves_1, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-2">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "InitialCapitalReserves_2",
                                                                                'id'    => "InitialCapitalReserves_2",
                                                                                'value' => round($fields->InitialCapitalReserves_2),
                                                                                'class'	=> 'form-control number-field NumberOnly percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                        	<button type="button" class="btn btn-md btn-block btn-default" data-target="#HelpfulCalcModal1" data-toggle="modal">Capital Reserves Calculator</button>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- InitialCapitalReserves -->
                                                                <div class="panel-field" id="InitialCapitalImprovementsRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4"><strong>Initial Capital Improvements</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "InitialCapitalImprovements",
                                                                                'id'    => "InitialCapitalImprovements",
                                                                                'value' => number_format($fields->InitialCapitalImprovements, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                        	<button type="button" id="rentalPopup" class="btn btn-md btn-block btn-default" data-target="#MoreOptionModal" data-toggle="modal">Planned Capital Improvements</button>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- InitialCapitalImprovements -->
                                                                <div class="panel-field" id="FMVPropertyRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4"><strong>Add to FMV of Property?</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes' => 'Yes',
                                                                                'No'  => 'No',
                                                                            );
                                                                            $attrb = 'id="FMVProperty" class="form-control"'; ?>
                                                                            <?=form_dropdown('FMVProperty', $options, $fields->FMVProperty, $attrb)?>
                                                                        </div>
                                                                        <div class="col-xs-4"></div>
                                                                    </div>
                                                                </div><!-- FMVProperty -->
                                                                <div class="panel-field" id="FMVMultiplierRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4"><strong>FMV Multiplier</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FMVMultiplier",
                                                                                'id'    => "FMVMultiplier",
                                                                                'value' => $fields->FMVMultiplier,
                                                                                'class'	=> 'form-control number-field NumberOnly',
                                                                            ); ?>
																			<?=form_input($field)?>
                                                                        </div>
                                                                        <div class="col-xs-4"></div>
                                                                    </div>
                                                                </div><!-- FMVMultiplier -->
                                                                <div class="panel-field" id="FeeNameRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4"><strong>Additional Upfront Fee to Buyer (Optional)</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FeeName",
                                                                                'id'    => "FeeName",
                                                                                'value' => number_format($fields->FeeName, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4"></div>
                                                                    </div>
                                                                </div><!-- FeeName -->
                                                                <div class="panel-field" id="TotalCashOutlayRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-4"><strong>Total cash outlay</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TotalCashOutlay",
                                                                                'id'    => "TotalCashOutlay",
                                                                                'value' => number_format($fields->TotalCashOutlay, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-xs-4"></div>
                                                                    </div>
                                                                </div><!-- TotalCashOutlay -->

                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="50">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep8 on_next_section" data-rel="70">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="rental_cash_outlay" />
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>      
                                                        <div class="clearfix"></div>                                          
                                                    </div><!-- STEP 8: SECTION 4: CASH OUTLAY -->
                                                    
                                                    <div class="full-section-box">
                                                        <div class="panel-field">
                                                            <h3>HEADLINES</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child"><!-- STEP (3) -->
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-rental/' . $PropertyID . '/rentals') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="70">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep10 on_next_section" data-rel="90">NEXT</button>
                                                                </div><!-- PRINT REPORT Button -->
                                                    
                                                                <div class="panel-field" id="Headline1Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Headline</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Headline",
                                                                                'id'    => "Headline",
                                                                                'value' => $fields->Headline,
                                                                                'class'	=> 'form-control Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Headline 1 -->
                                                                <div class="panel-field" id="SubHeadline1Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Sub Headline 1</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SubHeadline1",
                                                                                'id'    => "SubHeadline1",
                                                                                'value' => $fields->SubHeadline1,
                                                                                'class'	=> 'form-control Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Sub Headline 1 -->
                                                                <div class="panel-field" id="SubHeadline2Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Sub Headline 2</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SubHeadline2",
                                                                                'id'    => "SubHeadline2",
                                                                                'value' => $fields->SubHeadline2,
                                                                                'class'	=> 'form-control Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Sub Headline 2 -->
                                                                <div class="panel-field" id="SubHeadline3Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Sub Headline 3</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SubHeadline3",
                                                                                'id'    => "SubHeadline3",
                                                                                'value' => $fields->SubHeadline3,
                                                                                'class'	=> 'form-control Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Sub Headline 3 -->
                                                                <div class="panel-field" id="SubHeadline4Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Sub Headline 4</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SubHeadline4",
                                                                                'id'    => "SubHeadline4",
                                                                                'value' => $fields->SubHeadline4,
                                                                                'class'	=> 'form-control Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Sub Headline 4 -->
                                                                <div class="panel-field" id="SubHeadline5Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Sub Headline 5</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SubHeadline5",
                                                                                'id'    => "SubHeadline5",
                                                                                'value' => $fields->SubHeadline5,
                                                                                'class'	=> 'form-control Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Sub Headline 5 -->
                                                                <div class="panel-field" id="UnitMixRow">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Unit Mix</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "UnitMix",
                                                                                'id'    => "UnitMix",
                                                                                'value' => $fields->UnitMix,
                                                                                'class'	=> 'form-control Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Unit Mix -->
                                                                <div class="panel-field" id="DescPropRow">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Description of Property</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "DescProp",
                                                                                'id'    => "DescProp",
                                                                                'value' => $fields->DescProp,
                                                                                'class'	=> 'form-control Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Desc Prop -->
                                                                <div class="panel-field" id="AreaNotesRow">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Notes on Area</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "AreaNotes",
                                                                                'id'    => "AreaNotes",
                                                                                'value' => $fields->AreaNotes,
                                                                                'class'	=> 'form-control Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Desc Prop -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="70">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary RentalStep10 on_next_section" data-rel="90">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="rental_headlines" />
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                            <?=form_close()?>
                                                        
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP 10: HEADLINES -->
                                                    
                                                    <div class="full-section-box"><!-- STEP (8) -->
                                                        <div class="panel-field">
                                                            <h3>PROPERTY PHOTOS</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child">
                                                        
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PictureForm'); ?>
                                                            <?=form_open_multipart(base_url('pages/ajax-image-upload' . '/' . $PropertyID . '/' . $CreatedOn . '/rental_property_photos/rental') . '/', $attributes)?>
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide loading" />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="80">BACK</button>
                                                                    <button type="submit" name="upload_btn" class="btn btn-md btn-primary RentalStep11 on_save_event" data-rel="100">SAVE</button>
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                                <div class="panel-field row" id="PreviewRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div id="PreviewResults">
                                                                            	<input type="hidden" name="OldPhotos" id="OldPhotos" value="<?php echo $fields->Photos ? $fields->Photos : ''; ?>" />
                                                                                <?php if ( !empty($fields->Photos) ) { ?>
                                                                                    <?php $images = explode('|', $fields->Photos); ?>
                                                                                    <?php $captions = explode('||||', $fields->Captions); ?>
                                                                                    <?php $cover = explode('|', $fields->Cover); ?>
                                                                                    <?php for ( $i = 0; $i < count($images); $i++ ) { ?>
                                                                                        <div class="col-sm-2 image-upload">
                                                                                        	<div class="Check_Cover">
                                                                                                <input type="radio" name="SetCover" class="radio SetCover" <?php echo $cover[$i]=='on' ? 'checked' : ''; ?> />
                                                                                                <label>Cover Photo</label>
                                                                                                <input type="hidden" class="CoverImages" name="CoversImages[]" value="<?php echo $cover[$i]=='on' ? 'on' : 'off'; ?>" />
                                                                                            </div>
                                                                                            <img class="thumbnail" src="<?php echo $images[$i]; ?>" width="100%" height="110" style="margin-bottom:5px;" />
                                                                                            <textarea name="Captions[]" class="form-control" rows="1" style="margin-bottom:10px;"><?php echo isset($captions[$i]) ? $captions[$i] : ''; ?></textarea>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                                <div id="added_preview_0" class="col-sm-2 image-upload">
                                                                                	<div class="Check_Cover">
                                                                                    	<input disabled type="radio" name="SetCover" class="radio SetCover" />
                                                                                        <label for="SetCover_0">Cover Photo</label>
                                                                                        <input disabled type="hidden" class="CoverImages" name="CoversImages[]" value="off" />
                                                                                    </div>
                                                                                    <a class="DeletePhotos" data-id="<?=$fields->RentalPhotoID?>" data-photo="<?php echo $images[$i]; ?>" data-table="rental_property_photos" onclick="DeletePhotos(this);"><i class="fa fa-times"></i></a>
                                                                                	<img id="Preview_Images_0" class="thumbnail" src="<?=base_url('assets/images/preview.jpg')?>" width="100%" height="110" style="margin-bottom:5px;" />
                                                                                    <textarea disabled name="Captions[]" id="Caption_0" class="form-control" rows="1" style="margin-bottom:10px;"></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Preview -->
                                                                
                                                                <div class="panel-field" id="Images0Row">
                                                                    <div class="row">
                                                                    	<div class="col-xs-6"></div>
                                                                        <div class="col-xs-6">
                                                                            <p><strong>If you want to change the images please upload, otherwise leave this field blank.</strong></p>
                                                                        </div>
                                                                        <label class="col-xs-6">Pictures</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Images[0]",
                                                                                'id'    => "Images_0",
                                                                                'class'	=> 'form-control images',
																				'onChange' => 'ValidateFileUpload(this.id)'
                                                                            ); ?>
                                                                            <?=form_upload($field)?>
                                                                            <font size="1">Allowed file types: GIF, JPG, JPEG, PNG, BMP</font>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Images -->
                                                                
                                                                <div id="more_image"></div><!-- More Images -->
        
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <div class="pull-left">
	                                                                    <a id="last_image" rel="<?=(!empty($fields->Photos) ? count($images) : 1)?>"></a>
    	                                                                <a id="add_new_image" class="action-btn" title="Add New" data-placement="top" data-toggle="tooltip">
        	                                                                <i class="fa fa-plus-circle"></i>
            	                                                        </a>
                	                                                    <a id="remove_image" class="action-btn" data-toggle="tooltip" data-placement="top" title="Remove" style="display:none">
  	                                                                      <i class="fa fa-minus-circle"></i>
    	                                                                </a>
                                                                    </div>
                                                                    <img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide loading" />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="80">BACK</button>
                                                                    <button type="submit" name="upload_btn" class="btn btn-md btn-primary RentalStep10 on_save_event" data-rel="100">SAVE</button>
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP 11: ADDITIONAL PICTURES -->
                                                    
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>

<?php $this->load->view('template/manual-growth-rate-modal'); ?>
<?php $this->load->view('template/help-calc-modal-edit'); ?>
<?php $this->load->view('template/help-calc-modal1-edit'); ?>
<?php $this->load->view('template/advanced-option-modal'); ?>