<section id="signup">
	<div class="container">
    	<div class="row">
        	<div class="col-xs-6 col-xs-offset-3">
                <div class="signup-box">
                    <h1><a class="home" href="<?=base_url()?>"><i class="fa fa-home"></i></a><span class="sign-title">Cashlenders Signin</span></h1>
                    <a href="<?=base_url()?>">
                    	<img src="<?=base_url('assets/images/logo.png')?>" alt="cblist" width="150" />
                    </a>
                    <div class="signup-body">
                        <?=form_open(base_url('pages/cashlenders-login') . '/')?>
                            <?php if ( $success ) { ?>
                                <div class="alert alert-success" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <i class="fa fa-info-circle"></i> <?=$success?>
                                </div>
                            <?php } else if ( $error ) { ?>
                                <div class="alert alert-danger" role="alert">
                                    <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <i class="fa fa-info-circle"></i> <?=$error?>
                                </div>
                            <?php } ?>
                            <p>
                                <?php 
                                $field = array(
                                    'name'        => 'email',
                                    'id'          => 'email',
                                    'placeholder' => 'Enter Your Email',
                                    'value'       => ($reset) ? "" : set_value('email'),
                                ); ?>
                                <?=form_input($field)?>
                                <?=form_error('email', '<span class="error">', '</span>')?>
                            </p>
                            <p>
                                <?php
                                $field = array(
                                    'name'        => 'password',
                                    'id'          => 'password',
                                    'placeholder' => 'Enter Your Password',
                                ); ?>
                                <?=form_password($field)?>
                                <?=form_error('password', '<span class="error">', '</span>')?>
                            </p>
                            <p>
                                <?php
                                $field = array(
                                    'name' 		=> 'email_submit',
                                    'id' 		=> 'email_submit',
                                    'value' 	=> 'true',
                                    'type' 		=> 'submit',
                                    'content' 	=> '<i class="fa fa-sign-in"></i> Log in',
                                    'class'		=> 'register',
                                ); ?>					
                                <?= form_button($field) ?>
                            </p>
                            <p class="text-center" style="height:20px;">
                            	<a href="<?=base_url('pages/forgot-password')?>/"><i class="fa fa-info-circle"></i> Forgot Password?</a>
                            </p>
                            <p class="text-center" style="height:20px;">
                                <a href="<?=base_url('pages/cashlenders-signup')?>/"><i class="fa fa-user"></i> Register as a Cashlender</a>
                            </p>
                        <?=form_close()?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>