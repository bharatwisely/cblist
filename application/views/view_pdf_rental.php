<?php 
if ( isset($print_data['RentalCoverPage']) ) {
	$DATA = $print_data['RentalCoverPage'];
} elseif ( isset($print_data['SinglePagePropertySummery']) ) {
	$DATA = $print_data['SinglePagePropertySummery'];
} elseif ( isset($print_data['RentalAdditionalPics']) ) {
	$DATA = $print_data['RentalAdditionalPics'];
} elseif ( isset($print_data['AnnualPropertyOperating']) ) {
	$DATA = $print_data['AnnualPropertyOperating'];
} elseif ( isset($print_data['RentReport']) ) {
	$DATA = $print_data['RentReport'];
} elseif ( isset($print_data['OperatingExpensesReport']) ) {
	$DATA = $print_data['OperatingExpensesReport'];
} elseif ( isset($print_data['IncomeStatement']) ) {
	$DATA = $print_data['IncomeStatement'];
} elseif ( isset($print_data['RentalCashFlowReport']) ) {
	$DATA = $print_data['RentalCashFlowReport'];
} elseif ( isset($print_data['DiscountCashFlowAnalysis']) ) {
	$DATA = $print_data['DiscountCashFlowAnalysis'];
} elseif ( isset($print_data['LenderSummary']) ) {
	$DATA = $print_data['LenderSummary'];
} elseif ( isset($print_data['RentalRefiAnalysis']) ) {
	$DATA = $print_data['RentalRefiAnalysis'];
} elseif ( isset($print_data['FullTaxTreatment']) ) {
	$DATA = $print_data['FullTaxTreatment'];
} elseif ( isset($print_data['PropertyValueEvolution']) ) {
	$DATA = $print_data['PropertyValueEvolution'];
} elseif ( isset($print_data['FinancialRatiosReport']) ) {
	$DATA = $print_data['FinancialRatiosReport'];    
}

/** ------------------------------------------------------- D16 ------------------------------------------------------- **/
$Incomes = array();
for ( $i=1; $i<=12; $i++ ) {
	$PropertyAnnualRent = 'PropertyAnnualRent_' . $i;
	$PropertyAnnualGrowthRate = 'PropertyAnnualGrowthRate_' . $i;
	$IncomeRate = $DATA->$PropertyAnnualRent;
	for ( $n=1; $n<=$DATA->YearSale; $n++ ) {
		if ( $n == 1 ) {
			$IncomeRate = $IncomeRate;
		} else {
			if ( $DATA->RentGrowthOveride == "No" ) {
				$IncomeRate = $IncomeRate + (($IncomeRate * $DATA->$PropertyAnnualGrowthRate)/100);
			} else {
				$IncomeRate = $IncomeRate + (($IncomeRate * $DATA->RentGrowthRate)/100);
			}
		}
		$Incomes[$i][$n] = round($IncomeRate, 2);
	}
}
foreach ( $Incomes as $k => $SubInArray ) {
	foreach ( $SubInArray as $id => $value ) {
		if ( !isset($D16[$id]) ) {
			$D16[$id] = 0;
		}
		$D16[$id] += round($value, 2);
	}
}
/** ------------------------------------------------------- D16 ------------------------------------------------------- **/

/** ------------------------------------------------------- D22 ------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$OverrideVacancyRates = 'OverrideVacancyRates_Year_' . $i;
	$OverrideConcessions = 'OverrideConcessions_Year_' . $i;
	$D18[$i] = round($DATA->OverrideVacancyRates=="No" ? ($D16[$i]*$DATA->VacancyLossPercentage)/100 : ($D16[$i]*$DATA->$OverrideVacancyRates)/100, 2);
	$D19[$i] = round($DATA->OverrideConcessions=="No" ? ($D16[$i]*$DATA->ConcessionsParcentage)/100 : ($D16[$i]*$DATA->$OverrideConcessions)/100, 2);
	$D20[$i] = round(($D16[$i]*$DATA->ManagementFeePercentage)/100, 2);
	if ( $i == 1 ) {
		$D21[$i] = $DATA->OtherIncomeAnnual;
	} else {
		$D21[$i] = round($D21[$i-1] * (1+($DATA->OtherIncomeAnnualGrowthRate/100))*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1), 2);
	}
	$D22[$i] = $D16[$i]-($D18[$i]+$D19[$i]+$D20[$i])+$D21[$i];
}
/** ------------------------------------------------------- D22 ------------------------------------------------------- **/

/** ------------------------------------------------------- D46------------------------------------------------------- **/
$ExpenseReservesAnnual = 'OperatingExpenseReservesAnnual';
$ExpenseReservesGrowthRate = 'OperatingExpenseReservesGrowthRate';
$ReservesRate = $DATA->$ExpenseReservesAnnual;
for ( $n=1; $n<=$DATA->YearSale; $n++ ) {
	if ( $n == 1 ) {
		$ReservesRate = $ReservesRate;
	} else {
		if ( $DATA->RentGrowthOveride == "No" ) {
			$ReservesRate = $ReservesRate + (($ReservesRate * $DATA->$ExpenseReservesGrowthRate)/100);
		} else {
			$ReservesRate = $ReservesRate + (($ReservesRate * $DATA->RentGrowthRate)/100);
		}
	}
	$Reserves[$i][$n] = $ReservesRate;
}
foreach ( $Reserves as $k => $ReservesArray ) {
	foreach ( $ReservesArray as $id => $value ) {
		if ( !isset($D46[$id]) ) {
			$D46[$id] = 0;
		}
		$D46[$id] += round($value, 2);
	}
}
/** ------------------------------------------------------- D46------------------------------------------------------- **/

/** ------------------------------------------------------- D47 ------------------------------------------------------- **/
$Expenses = array();
for ( $i=1; $i<=21; $i++ ) {
	if ( $i <= 15 ) {
		$OperatingExpenseAnnual = 'OperatingExpenseAnnual_'.$i;
		$OperatingExpenseGrowthRate = 'OperatingExpenseGrowthRate_'.$i;
	} elseif ( $i == 16 ) {
		$OperatingExpenseAnnual = 'OperatingExpenseWaterSewerAnnual';
		$OperatingExpenseGrowthRate = 'OperatingExpenseWaterSewerGrowthRate';
	} elseif ( $i == 17 ) {
		$OperatingExpenseAnnual = 'OperatingExpenseElectricityAnnual';
		$OperatingExpenseGrowthRate = 'OperatingExpenseElectricityGrowthRate';
	} elseif ( $i == 18 ) {
		$OperatingExpenseAnnual = 'OperatingExpenseGasAnnual';
		$OperatingExpenseGrowthRate = 'OperatingExpenseGasGrowthRate';
	} elseif ( $i == 19 ) {
		$OperatingExpenseAnnual = 'OperatingExpenseFuelOilAnnual';
		$OperatingExpenseGrowthRate = 'OperatingExpenseFuelOilGrowthRate';
	} elseif ( $i == 20 ) {
		$OperatingExpenseAnnual = 'OperatingExpenseOtherUtilitiesAnnual';
		$OperatingExpenseGrowthRate = 'OperatingExpenseOtherUtilitiesGrowthRate';
	} elseif ( $i == 21 ) {
		$OperatingExpenseAnnual = 'OperatingExpenseReservesAnnual';
		$OperatingExpenseGrowthRate = 'OperatingExpenseReservesGrowthRate';
	}
	$ExpenseRate = $DATA->$OperatingExpenseAnnual;
	for ( $n=1; $n<=$DATA->YearSale; $n++ ) {
		if ( $n == 1 ) {
			$ExpenseRate = $ExpenseRate;
		} else {
			$ExpenseRate = $ExpenseRate * (1 + ($DATA->OpExpensesGrowthOverride=="No" ? ($DATA->$OperatingExpenseGrowthRate/100) : ($DATA->OpExpensesGrowthRate/100))) * (($DATA->YearSale+1==$n || $DATA->YearSale+1<$n) ? 0 : 1);
		}
		$Expenses[$i][$n] = $ExpenseRate;
	}
}
foreach ( $Expenses as $k => $SubExArray ) {
	foreach ( $SubExArray as $id => $value ) {
		if ( !isset($D47[$id]) ) {
			$D47[$id] = 0;
		}
		$D47[$id] += round($value, 2);
	}
}
/** ------------------------------------------------------- D47 ------------------------------------------------------- **/

/** ------------------------------------------------------- D49 ------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D49[$i] = $D22[$i]-$D47[$i];
}
/** ------------------------------------------------------- D49 ------------------------------------------------------- **/

/** ----------------------------------------------------- D54/D55 ----------------------------------------------------- **/
for ( $i=1; $i<=481; $i++ ) {
	if ( $i == 1 ) {
		$B7[$i] = round($DATA->FirstMortgageAmount, 2);
		$C7[$i] = round(($B7[$i] > 1 ? $DATA->FirstMortgagePayment : 0), 2);
		$D7[$i] = round((($DATA->FirstMortgageRate/100)/12)*$B7[$i], 2);
		$E7[$i] = round(-$C7[$i]-$D7[$i], 2);
	} else {
		$B7[$i] = round(max($B7[$i-1]-$E7[$i-1], 0), 2);
		$C7[$i] = round(($B7[$i] > 1 ? $DATA->FirstMortgagePayment : 0), 2);
		$D7[$i] = round((($DATA->FirstMortgageRate/100)/12)*$B7[$i], 2);
		$E7[$i] = round(-$C7[$i]-$D7[$i], 2);
	}
}
for ( $i=12; $i<=481; $i=$i+12 ) {
	$F18[$i] = $D7[$i-11]+$D7[$i-10]+$D7[$i-9]+$D7[$i-8]+$D7[$i-7]+$D7[$i-6]+$D7[$i-5]+$D7[$i-4]+$D7[$i-3]+$D7[$i-2]+$D7[$i-1]+$D7[$i];
	$G18[$i] = $E7[$i-11]+$E7[$i-10]+$E7[$i-9]+$E7[$i-8]+$E7[$i-7]+$E7[$i-6]+$E7[$i-5]+$E7[$i-4]+$E7[$i-3]+$E7[$i-2]+$E7[$i-1]+$E7[$i];
}
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D54[$i] = round($G18[$i*12], 2);
	$D55[$i] = round($F18[$i*12], 2);
}
/** ----------------------------------------------------- D54/D55 ----------------------------------------------------- **/

/** ----------------------------------------------------- D56/D57 ----------------------------------------------------- **/
for ( $i=1; $i<=481; $i++ ) {
	if ( $i == 1 ) {
		$J7[$i] = round($DATA->SecondMortgageAmount, 2);
		$K7[$i] = round(($J7[$i] > 1 ? $DATA->SecondMortgagePayment : 0), 2);
		$L7[$i] = round((($DATA->SecondMortgageRate/100)/12)*$J7[$i], 2);
		$M7[$i] = round(-$K7[$i]-$L7[$i], 2);
	} else {
		$J7[$i] = round(max($J7[$i-1]-$M7[$i-1], 0), 2);
		$K7[$i] = round(($J7[$i] > 1 ? $DATA->SecondMortgagePayment : 0), 2);
		$L7[$i] = round((($DATA->SecondMortgageRate/100)/12)*$J7[$i], 2);
		$M7[$i] = round(-$K7[$i]-$L7[$i], 2);
	}
}
for ( $i=12; $i<=481; $i=$i+12 ) {
	$N18[$i] = $L7[$i-11]+$L7[$i-10]+$L7[$i-9]+$L7[$i-8]+$L7[$i-7]+$L7[$i-6]+$L7[$i-5]+$L7[$i-4]+$L7[$i-3]+$L7[$i-2]+$L7[$i-1]+$L7[$i];
	$O18[$i] = $M7[$i-11]+$M7[$i-10]+$M7[$i-9]+$M7[$i-8]+$M7[$i-7]+$M7[$i-6]+$M7[$i-5]+$M7[$i-4]+$M7[$i-3]+$M7[$i-2]+$M7[$i-1]+$M7[$i];
}
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D56[$i] = round($O18[$i*12], 2);
	$D57[$i] = round($N18[$i*12], 2);
}
/** ----------------------------------------------------- D56/D57 ----------------------------------------------------- **/

/** ------------------------------------------------------- D58 ------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D58[$i] = round($D54[$i]+$D55[$i]+$D56[$i]+$D57[$i], 2);
}
/** ------------------------------------------------------- D58 ------------------------------------------------------- **/

/** ------------------------------------------------------- D59 ------------------------------------------------------- **/
$D59[0] = $DATA->TotalCashOutlay_0;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D59[$i] = round($D49[$i]-$D58[$i], 2);
}
/** ------------------------------------------------------- D59 ------------------------------------------------------- **/

/** ------------------------------------------------------- D61 ------------------------------------------------------- **/
$D61[0] = ($DATA->YearSale > 30 ? IRR($D59, 0.05) : "N/A");
/** ------------------------------------------------------- D61 ------------------------------------------------------- **/

/** ----------------------------------------------------- D63/D65 ------------------------------------------------------- **/
$D63[0] = $DATA->PurchasePrice*(1-($DATA->LandPercentage/100))+$DATA->InitialCapitalImprovements;
$D65[0] = 0;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	if ( $i == 1 ) {
		$D65[$i] = round(min($D63[0]/$DATA->DepYears/12*11.5, $D63[0])*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1), 2);
	} else {
		$D65[$i] = round(min($D63[0]/$DATA->DepYears, $D63[$i-1])*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1), 2);
	}
	$D63[$i] = round(max(0, $D63[$i-1]-$D65[$i])*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1), 2);
}
/** ----------------------------------------------------- D63/D65 ------------------------------------------------------- **/

/** ----------------------------------------------------- D64/D66 ------------------------------------------------------- **/
$AssetBase1=0;$AssetBase2=0;$AssetBase3=0;$AssetBase4=0;$AssetBase5=0;$AssetBase6=0;$AssetBase7=0;$AssetBase8=0;$AssetBase9=0;$AssetBase10=0;
$AssetBase11=0;$AssetBase12=0;$AssetBase13=0;$AssetBase14=0;$AssetBase15=0;$AssetBase16=0;$AssetBase17=0;$AssetBase18=0;$AssetBase19=0;$AssetBase20=0;
$AssetBase21=0;$AssetBase22=0;$AssetBase23=0;$AssetBase24=0;$AssetBase25=0;$AssetBase26=0;$AssetBase27=0;$AssetBase28=0;$AssetBase29=0;$AssetBase30=0;
for ( $i=1; $i<=60; $i++ ) {
	$AssetBaseArray1[$i]=0;$AssetBaseArray2[$i]=0; $AssetBaseArray3[$i]=0; $AssetBaseArray4[$i]=0; $AssetBaseArray5[$i]=0; 
	$AssetBaseArray6[$i]=0; $AssetBaseArray7[$i]=0; $AssetBaseArray8[$i]=0; $AssetBaseArray9[$i]=0; $AssetBaseArray10[$i]=0; 
	$AssetBaseArray11[$i]=0; $AssetBaseArray12[$i]=0; $AssetBaseArray13[$i]=0; $AssetBaseArray14[$i]=0; $AssetBaseArray15[$i]=0; 
	$AssetBaseArray16[$i]=0; $AssetBaseArray17[$i]=0; $AssetBaseArray18[$i]=0; $AssetBaseArray19[$i]=0; $AssetBaseArray20[$i]=0; 
	$AssetBaseArray21[$i]=0; $AssetBaseArray22[$i]=0; $AssetBaseArray23[$i]=0; $AssetBaseArray24[$i]=0; $AssetBaseArray25[$i]=0; 
	$AssetBaseArray26[$i]=0; $AssetBaseArray27[$i]=0; $AssetBaseArray28[$i]=0; $AssetBaseArray29[$i]=0; $AssetBaseArray30[$i]=0;
	$AssetDepArray1[$i]=0;$AssetDepArray2[$i]=0; $AssetDepArray3[$i]=0; $AssetDepArray4[$i]=0; $AssetDepArray5[$i]=0; 
	$AssetDepArray6[$i]=0; $AssetDepArray7[$i]=0; $AssetDepArray8[$i]=0; $AssetDepArray9[$i]=0; $AssetDepArray10[$i]=0; 
	$AssetDepArray11[$i]=0; $AssetDepArray12[$i]=0; $AssetDepArray13[$i]=0; $AssetDepArray14[$i]=0; $AssetDepArray15[$i]=0; 
	$AssetDepArray16[$i]=0; $AssetDepArray17[$i]=0; $AssetDepArray18[$i]=0; $AssetDepArray19[$i]=0; $AssetDepArray20[$i]=0; 
	$AssetDepArray21[$i]=0; $AssetDepArray22[$i]=0; $AssetDepArray23[$i]=0; $AssetDepArray24[$i]=0; $AssetDepArray25[$i]=0; 
	$AssetDepArray26[$i]=0; $AssetDepArray27[$i]=0; $AssetDepArray28[$i]=0; $AssetDepArray29[$i]=0; $AssetDepArray30[$i]=0;
}
$DeductBase1=0;$DeductBase2=0;$DeductBase3=0;$DeductBase4=0;$DeductBase5=0;$DeductBase6=0;$DeductBase7=0;$DeductBase8=0;$DeductBase9=0;$DeductBase10=0;
$DeductBase11=0;$DeductBase12=0;$DeductBase13=0;$DeductBase14=0;$DeductBase15=0;$DeductBase16=0;$DeductBase17=0;$DeductBase18=0;$DeductBase19=0;$DeductBase20=0;
$DeductBase21=0;$DeductBase22=0;$DeductBase23=0;$DeductBase24=0;$DeductBase25=0;$DeductBase26=0;$DeductBase27=0;$DeductBase28=0;$DeductBase29=0;$DeductBase30=0;
$DepYears = $DATA->DepYears;
$CIS1=$DATA->CapitalImprovementSchedule_1;$CIS2=$DATA->CapitalImprovementSchedule_2;$CIS3=$DATA->CapitalImprovementSchedule_3;
$CIS4=$DATA->CapitalImprovementSchedule_4;$CIS5=$DATA->CapitalImprovementSchedule_5;$CIS6=$DATA->CapitalImprovementSchedule_6;
$CIS7=$DATA->CapitalImprovementSchedule_7;$CIS8=$DATA->CapitalImprovementSchedule_8;$CIS9=$DATA->CapitalImprovementSchedule_9;
$CIS10=$DATA->CapitalImprovementSchedule_10;$CIS11=$DATA->CapitalImprovementSchedule_11;$CIS12=$DATA->CapitalImprovementSchedule_12;
$CIS13=$DATA->CapitalImprovementSchedule_13;$CIS14=$DATA->CapitalImprovementSchedule_14;$CIS15=$DATA->CapitalImprovementSchedule_15;
$CIS16=$DATA->CapitalImprovementSchedule_16;$CIS17=$DATA->CapitalImprovementSchedule_17;$CIS18=$DATA->CapitalImprovementSchedule_18;
$CIS19=$DATA->CapitalImprovementSchedule_19;$CIS20=$DATA->CapitalImprovementSchedule_20;$CIS21=$DATA->CapitalImprovementSchedule_21;
$CIS22=$DATA->CapitalImprovementSchedule_22;$CIS23=$DATA->CapitalImprovementSchedule_23;$CIS24=$DATA->CapitalImprovementSchedule_24;
$CIS25=$DATA->CapitalImprovementSchedule_25;$CIS26=$DATA->CapitalImprovementSchedule_26;$CIS27=$DATA->CapitalImprovementSchedule_27;
$CIS28=$DATA->CapitalImprovementSchedule_28;$CIS29=$DATA->CapitalImprovementSchedule_29;$CIS30=$DATA->CapitalImprovementSchedule_30;
for ( $i=1; $i<=30;$i++ ) {
	if ( $i == 1 ) {
		$AssetBase1=$CIS1;$AssetDep1=0; $AssetBase2=$CIS2;$AssetDep2=0; $AssetBase3=$CIS3;$AssetDep3=0;
		$AssetBase4=$CIS4;$AssetDep4=0; $AssetBase5=$CIS5;$AssetDep5=0; $AssetBase6=$CIS6;$AssetDep6=0;
		$AssetBase7=$CIS7;$AssetDep7=0; $AssetBase8=$CIS8;$AssetDep8=0; $AssetBase9=$CIS9;$AssetDep9=0;
		$AssetBase10=$CIS10;$AssetDep10=0; $AssetBase11=$CIS11;$AssetDep11=0; $AssetBase12=$CIS12;$AssetDep12=0;
		$AssetBase13=$CIS13;$AssetDep13=0; $AssetBase14=$CIS14;$AssetDep14=0; $AssetBase15=$CIS15;$AssetDep15=0;
		$AssetBase16=$CIS16;$AssetDep16=0; $AssetBase17=$CIS17;$AssetDep17=0; $AssetBase18=$CIS18;$AssetDep18=0;
		$AssetBase19=$CIS19;$AssetDep19=0; $AssetBase20=$CIS20;$AssetDep20=0; $AssetBase21=$CIS21;$AssetDep21=0;
		$AssetBase22=$CIS22;$AssetDep22=0; $AssetBase23=$CIS23;$AssetDep23=0; $AssetBase24=$CIS24;$AssetDep24=0;
		$AssetBase25=$CIS25;$AssetDep25=0; $AssetBase26=$CIS26;$AssetDep26=0; $AssetBase27=$CIS27;$AssetDep27=0;
		$AssetBase28=$CIS28;$AssetDep28=0; $AssetBase29=$CIS29;$AssetDep29=0; $AssetBase30=$CIS30;$AssetDep30=0;
	} else {
		$AssetBase1=$AssetBase1;$AssetDep1=min($CIS1/$DepYears, $AssetBase1); $AssetBase2=$AssetBase2;$AssetDep2=min($CIS2/$DepYears, $AssetBase2);
		$AssetBase3=$AssetBase3;$AssetDep3=min($CIS3/$DepYears, $AssetBase3); $AssetBase4=$AssetBase4;$AssetDep4=min($CIS4/$DepYears, $AssetBase4);
		$AssetBase5=$AssetBase5;$AssetDep5=min($CIS5/$DepYears, $AssetBase5); $AssetBase6=$AssetBase6;$AssetDep6=min($CIS6/$DepYears, $AssetBase6);
		$AssetBase7=$AssetBase7;$AssetDep7=min($CIS7/$DepYears, $AssetBase7); $AssetBase8=$AssetBase8;$AssetDep8=min($CIS8/$DepYears, $AssetBase8);
		$AssetBase9=$AssetBase9;$AssetDep9=min($CIS9/$DepYears, $AssetBase9); $AssetBase10=$AssetBase10;$AssetDep10=min($CIS10/$DepYears, $AssetBase10);
		$AssetBase11=$AssetBase11;$AssetDep11=min($CIS11/$DepYears, $AssetBase11); $AssetBase12=$AssetBase12;$AssetDep12=min($CIS12/$DepYears, $AssetBase12);
		$AssetBase13=$AssetBase13;$AssetDep13=min($CIS13/$DepYears, $AssetBase13); $AssetBase14=$AssetBase14;$AssetDep14=min($CIS14/$DepYears, $AssetBase14);
		$AssetBase15=$AssetBase15;$AssetDep15=min($CIS15/$DepYears, $AssetBase15); $AssetBase16=$AssetBase16;$AssetDep16=min($CIS16/$DepYears, $AssetBase16);
		$AssetBase17=$AssetBase17;$AssetDep17=min($CIS17/$DepYears, $AssetBase17); $AssetBase18=$AssetBase18;$AssetDep18=min($CIS18/$DepYears, $AssetBase18);
		$AssetBase19=$AssetBase19;$AssetDep19=min($CIS19/$DepYears, $AssetBase19); $AssetBase20=$AssetBase20;$AssetDep20=min($CIS20/$DepYears, $AssetBase20);
		$AssetBase21=$AssetBase21;$AssetDep21=min($CIS21/$DepYears, $AssetBase21); $AssetBase22=$AssetBase22;$AssetDep22=min($CIS22/$DepYears, $AssetBase22);
		$AssetBase23=$AssetBase23;$AssetDep23=min($CIS23/$DepYears, $AssetBase23); $AssetBase24=$AssetBase24;$AssetDep24=min($CIS24/$DepYears, $AssetBase24);
		$AssetBase25=$AssetBase25;$AssetDep25=min($CIS25/$DepYears, $AssetBase25); $AssetBase26=$AssetBase26;$AssetDep26=min($CIS26/$DepYears, $AssetBase26);
		$AssetBase27=$AssetBase27;$AssetDep27=min($CIS27/$DepYears, $AssetBase27); $AssetBase28=$AssetBase28;$AssetDep28=min($CIS28/$DepYears, $AssetBase28);
		$AssetBase29=$AssetBase29;$AssetDep29=min($CIS29/$DepYears, $AssetBase29); $AssetBase30=$AssetBase30;$AssetDep30=min($CIS30/$DepYears, $AssetBase30);
	}
	$AssetBase1=max(0, $AssetBase1-$AssetDep1);$AssetBaseArray1[$i]=$AssetBase1;
	$AssetBase2=max(0, $AssetBase2-$AssetDep2);$AssetBaseArray2[$i+1]=$AssetBase2;
	$AssetBase3=max(0, $AssetBase3-$AssetDep3);$AssetBaseArray3[$i+2]=$AssetBase3;
	$AssetBase4=max(0, $AssetBase4-$AssetDep4);$AssetBaseArray4[$i+3]=$AssetBase4;
	$AssetBase5=max(0, $AssetBase5-$AssetDep5);$AssetBaseArray5[$i+4]=$AssetBase5;
	$AssetBase6=max(0, $AssetBase6-$AssetDep6);$AssetBaseArray6[$i+5]=$AssetBase6;
	$AssetBase7=max(0, $AssetBase7-$AssetDep7);$AssetBaseArray7[$i+6]=$AssetBase7;
	$AssetBase8=max(0, $AssetBase8-$AssetDep8);$AssetBaseArray8[$i+7]=$AssetBase8;
	$AssetBase9=max(0, $AssetBase9-$AssetDep9);$AssetBaseArray9[$i+8]=$AssetBase9;
	$AssetBase10=max(0, $AssetBase10-$AssetDep10);$AssetBaseArray10[$i+9]=$AssetBase10;
	$AssetBase11=max(0, $AssetBase11-$AssetDep11);$AssetBaseArray11[$i+10]=$AssetBase11;
	$AssetBase12=max(0, $AssetBase12-$AssetDep12);$AssetBaseArray12[$i+11]=$AssetBase12;
	$AssetBase13=max(0, $AssetBase13-$AssetDep13);$AssetBaseArray13[$i+12]=$AssetBase13;
	$AssetBase14=max(0, $AssetBase14-$AssetDep14);$AssetBaseArray14[$i+13]=$AssetBase14;
	$AssetBase15=max(0, $AssetBase15-$AssetDep15);$AssetBaseArray15[$i+14]=$AssetBase15;
	$AssetBase16=max(0, $AssetBase16-$AssetDep16);$AssetBaseArray16[$i+15]=$AssetBase16;
	$AssetBase17=max(0, $AssetBase17-$AssetDep17);$AssetBaseArray17[$i+16]=$AssetBase17;
	$AssetBase18=max(0, $AssetBase18-$AssetDep18);$AssetBaseArray18[$i+17]=$AssetBase18;
	$AssetBase19=max(0, $AssetBase19-$AssetDep19);$AssetBaseArray19[$i+18]=$AssetBase19;
	$AssetBase20=max(0, $AssetBase20-$AssetDep20);$AssetBaseArray20[$i+19]=$AssetBase20;
	$AssetBase21=max(0, $AssetBase21-$AssetDep21);$AssetBaseArray21[$i+20]=$AssetBase21;
	$AssetBase22=max(0, $AssetBase22-$AssetDep22);$AssetBaseArray22[$i+21]=$AssetBase22;
	$AssetBase23=max(0, $AssetBase23-$AssetDep23);$AssetBaseArray23[$i+22]=$AssetBase23;
	$AssetBase24=max(0, $AssetBase24-$AssetDep24);$AssetBaseArray24[$i+23]=$AssetBase24;
	$AssetBase25=max(0, $AssetBase25-$AssetDep25);$AssetBaseArray25[$i+24]=$AssetBase25;
	$AssetBase26=max(0, $AssetBase26-$AssetDep26);$AssetBaseArray26[$i+25]=$AssetBase26;
	$AssetBase27=max(0, $AssetBase27-$AssetDep27);$AssetBaseArray27[$i+26]=$AssetBase27;
	$AssetBase28=max(0, $AssetBase28-$AssetDep28);$AssetBaseArray28[$i+27]=$AssetBase28;
	$AssetBase29=max(0, $AssetBase29-$AssetDep29);$AssetBaseArray29[$i+28]=$AssetBase29;
	$AssetBase30=max(0, $AssetBase30-$AssetDep30);$AssetBaseArray30[$i+29]=$AssetBase30;
	$AssetDepArray1[$i]=$AssetDep1; $AssetDepArray2[$i+1]=$AssetDep2; 
	$AssetDepArray3[$i+2]=$AssetDep3; $AssetDepArray4[$i+3]=$AssetDep4; 
	$AssetDepArray5[$i+4]=$AssetDep5; $AssetDepArray6[$i+5]=$AssetDep6; 
	$AssetDepArray7[$i+6]=$AssetDep7; $AssetDepArray8[$i+7]=$AssetDep8; 
	$AssetDepArray9[$i+8]=$AssetDep9; $AssetDepArray10[$i+9]=$AssetDep10; 
	$AssetDepArray11[$i+10]=$AssetDep11; $AssetDepArray12[$i+11]=$AssetDep12; 
	$AssetDepArray13[$i+12]=$AssetDep13; $AssetDepArray14[$i+13]=$AssetDep14; 
	$AssetDepArray15[$i+14]=$AssetDep15; $AssetDepArray16[$i+15]=$AssetDep16; 
	$AssetDepArray17[$i+16]=$AssetDep17; $AssetDepArray18[$i+17]=$AssetDep18; 
	$AssetDepArray19[$i+18]=$AssetDep19; $AssetDepArray20[$i+19]=$AssetDep20; 
	$AssetDepArray21[$i+20]=$AssetDep21; $AssetDepArray22[$i+21]=$AssetDep22; 
	$AssetDepArray23[$i+22]=$AssetDep23; $AssetDepArray24[$i+23]=$AssetDep24; 
	$AssetDepArray25[$i+24]=$AssetDep25; $AssetDepArray26[$i+25]=$AssetDep26; 
	$AssetDepArray27[$i+26]=$AssetDep27; $AssetDepArray28[$i+27]=$AssetDep28; 
	$AssetDepArray29[$i+28]=$AssetDep29; $AssetDepArray30[$i+29]=$AssetDep30; 
}
for ( $i=1; $i<=60; $i++ ) {
	if ( $i == 1 ) {
		$CBCI[$i] = $AssetBaseArray1[$i];
		$CD[$i] = $AssetDepArray1[$i];
	} else {
		$CBCI[$i] = $AssetBaseArray1[$i]+$AssetBaseArray2[$i]+$AssetBaseArray3[$i]+$AssetBaseArray4[$i]+$AssetBaseArray5[$i]+$AssetBaseArray6[$i]+$AssetBaseArray7[$i]+$AssetBaseArray8[$i]+$AssetBaseArray9[$i]+$AssetBaseArray10[$i]+$AssetBaseArray11[$i]+$AssetBaseArray12[$i]+$AssetBaseArray13[$i]+$AssetBaseArray14[$i]+$AssetBaseArray15[$i]+$AssetBaseArray16[$i]+$AssetBaseArray17[$i]+$AssetBaseArray18[$i]+$AssetBaseArray19[$i]+$AssetBaseArray20[$i]+$AssetBaseArray21[$i]+$AssetBaseArray22[$i]+$AssetBaseArray23[$i]+$AssetBaseArray24[$i]+$AssetBaseArray25[$i]+$AssetBaseArray26[$i]+$AssetBaseArray27[$i]+$AssetBaseArray28[$i]+$AssetBaseArray29[$i]+$AssetBaseArray30[$i];
		$CD[$i] = $AssetDepArray1[$i]+$AssetDepArray2[$i]+$AssetDepArray3[$i]+$AssetDepArray4[$i]+$AssetDepArray5[$i]+$AssetDepArray6[$i]+$AssetDepArray7[$i]+$AssetDepArray8[$i]+$AssetDepArray9[$i]+$AssetDepArray10[$i]+$AssetDepArray11[$i]+$AssetDepArray12[$i]+$AssetDepArray13[$i]+$AssetDepArray14[$i]+$AssetDepArray15[$i]+$AssetDepArray16[$i]+$AssetDepArray17[$i]+$AssetDepArray18[$i]+$AssetDepArray19[$i]+$AssetDepArray20[$i]+$AssetDepArray21[$i]+$AssetDepArray22[$i]+$AssetDepArray23[$i]+$AssetDepArray24[$i]+$AssetDepArray25[$i]+$AssetDepArray26[$i]+$AssetDepArray27[$i]+$AssetDepArray28[$i]+$AssetDepArray29[$i]+$AssetDepArray30[$i];
	}
}
$D66[0] = 0;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D64[$i] = $CBCI[$i];
	$D66[$i] = $CD[$i];
}
/** ----------------------------------------------------- D64/D66 ------------------------------------------------------- **/

/** ------------------------------------------------------- D67 --------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D67[$i] = $D49[$i]-$D55[$i]-$D57[$i]-$D65[$i]-$D66[$i];
}
/** ------------------------------------------------------- D67 --------------------------------------------------------- **/

/** ------------------------------------------------------- C69 --------------------------------------------------------- **/
$D69[0] = $DATA->Designation;
/** ------------------------------------------------------- C69 --------------------------------------------------------- **/

/** ------------------------------------------------------- C70 --------------------------------------------------------- **/
$D70[0] = $Maxwriteoff = ($DATA->Designation=="Real Estate Professional" ? "Unlimited" : ($DATA->Designation=="Passive Participant" ? 0 : ($DATA->GAI<100000 ? 25000 : ($DATA->GAI>150000 ? 0 : (1-($DATA->GAI-100000)/50000)*25000))));
/** ------------------------------------------------------- C70 --------------------------------------------------------- **/

/** ------------------------------------------------------- C71 --------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D71[$i] = ($DATA->Designation=="Real Estate Professional" ? min($D67[$i], 0) : ($DATA->Designation=="Passive Participant" ? 0 : min(0, max($D67[$i], -$Maxwriteoff))));
}
/** ------------------------------------------------------- C71 --------------------------------------------------------- **/

/** ------------------------------------------------------- C72 --------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D72[$i] = min(0, $D67[$i]-$D71[$i]);
}
/** ------------------------------------------------------- C72 --------------------------------------------------------- **/

/** ----------------------------------------------------- D73/D74 --------------------------------------------------------- **/
$D73[0] = 0;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D74[$i] = ($i==1 ? 0 : ($D67[$i]>0 ? min(-$D73[$i-1], $D67[$i]) : 0));
	$D73[$i] = ($DATA->YearSale==$i-1 ? $D73[$i-1]+$D72[$i]+$D74[$i]-$C94[$i] : $D73[$i-1]+$D72[$i]+$D74[$i]);
}
/** ----------------------------------------------------- D73/D74 --------------------------------------------------------- **/

/** ------------------------------------------------------- D76 ----------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D76[$i] = $D67[$i]-$D74[$i]-$D72[$i];
}
/** ------------------------------------------------------- D76 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D78 ----------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D78[$i] = round(($D76[$i]*$DATA->TaxBracket)/100, 2);
}
/** ------------------------------------------------------- D78 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D80 ----------------------------------------------------------- **/
$D80[0] = $D59[0];
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D80[$i] = $D59[$i]-$D78[$i];
}
/** ------------------------------------------------------- D80 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D149 ---------------------------------------------------------- **/
$D149[1] = ($DATA->OverrideInitialValue=="Yes" ? $DATA->InitialMarketValue-$DATA->PurchasePrice : $DATA->InitialCapitalImprovements*$DATA->FMVMultiplier);
/** ------------------------------------------------------- D149 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D85 ----------------------------------------------------------- **/
$D85[0] = $DATA->PurchasePrice+($DATA->Resale=="Assume Annual Appreciation%" ? $D149[1] : 0);
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
	$FairMarketValue = 'FairMarketValue_' . $i;
	$D85[$i] = round(($DATA->Resale=="Assume Annual Appreciation%" ? $D85[$i-1]*(1+($DATA->AppreciationRate/100))*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1)+($i<=30?$DATA->$CapitalImprovementSchedule:1)*$DATA->$FairMarketValue : ($D49[$i]/$DATA->AppreciationRate)*100), 2);
}
/** ------------------------------------------------------- D85 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D51 ------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D51[$i] = $D49[$i]+($DATA->YearSale==$i ? $D85[$i] : 0);
}
/** ------------------------------------------------------- D51 ------------------------------------------------------- **/

/** ------------------------------------------------------- D86 ----------------------------------------------------------- **/
$D86[0] = $DATA->FirstMortgageAmount;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D86[$i] = round($B7[$i*12+1]*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1));
}
/** ------------------------------------------------------- D86 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D87 ----------------------------------------------------------- **/
$D87[0] = $DATA->SecondMortgageAmount;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D87[$i] = round($J7[$i*12+1]*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1));
}
/** ------------------------------------------------------- D87 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D88 ----------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D88[$i] = ($DATA->YearSale==$i ? ($DATA->CostOfSale/100)*$D85[$i] : 0);
}
/** ------------------------------------------------------- D88 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D89 ----------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D89[$i] = ($DATA->YearSale==$i ? $D85[$i]-($D86[$i]+$D87[$i]+$D88[$i]) : 0);
}
/** ------------------------------------------------------- D89 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D91 ----------------------------------------------------------- **/
$D91[0] = $DATA->PurchasePrice+$DATA->InitialCapitalImprovements;
$D65Total = $D65[0]; $D66Total = $D66[0];
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
	$D65Total = $D65Total + $D65[$i];
	$D66Total = $D66Total + $D66[$i];
	$D91[$i] = ($DATA->YearSale>=$i ? $D91[0]-($D65Total)+($i <= 30 ? $DATA->$CapitalImprovementSchedule : 0)-($D66Total) : 0);
}
/** ------------------------------------------------------- D91 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D93 ----------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D93[$i] = ($DATA->YearSale==$i ? $D85[$i]-$D91[$i]-$D88[$i] : 0);
}
/** ------------------------------------------------------- D93 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D94 ----------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D94[$i] = ($DATA->YearSale==$i ? $D73[$i] : 0);
}
/** ------------------------------------------------------- D94 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D95 ----------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D95[$i] = ($DATA->YearSale==$i ? max(0, $D93[$i]+$D94[$i]) : 0);
}
/** ------------------------------------------------------- D95 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D96 ----------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D96[$i] = round(($DATA->YearSale==$i ? $D95[$i]*$DATA->CapitalGainsPercentage/100 : 0), 2);
}
/** ------------------------------------------------------- D96 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D98 ----------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D98[$i] = ($DATA->YearSale==$i ? min(0, $D93[$i]+$D94[$i]) : 0);
}
/** ------------------------------------------------------- D98 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D129 ---------------------------------------------------------- **/
$D129[0] = $DATA->InitialCapitalReserves_1;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D129[$i] = round(($DATA->YearSale>=$i ? $D129[$i-1]*(1+($DATA->InitialCapitalReserves_2/100))+$D46[$i] : 0), 2);
}
/** ------------------------------------------------------- D129 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D99 ----------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D99[$i] = ($DATA->YearSale==$i ? $D129[$i] : 0);
}
/** ------------------------------------------------------- D99 ----------------------------------------------------------- **/

/** ------------------------------------------------------- D100 ---------------------------------------------------------- **/
$D100[0] = $D80[0];
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
	$D100[$i] = ($DATA->YearSale==$i ? $D59[$i]+$D89[$i]+$D99[$i] : $D59[$i]-($i <= 30 ? $DATA->$CapitalImprovementSchedule : 0));
}
/** ------------------------------------------------------- D100 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D101 ---------------------------------------------------------- **/
$D101[0] = $D80[0];
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
	$D101[$i] = ($DATA->YearSale==$i ? $D80[$i]+$D89[$i]-$D96[$i]+$D99[$i] : $D80[$i]-($i <= 30 ? $DATA->$CapitalImprovementSchedule : 0));
}
/** ------------------------------------------------------- D101 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D102 ---------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D101Current[] = $D101[$i];
	$D102[$i] = ($i!=1 ? ($DATA->YearSale>=$i ? IRR($D101Current, 0.005)*100 : 0) : 0);
}
/** ------------------------------------------------------- D102 ---------------------------------------------------------- **/


/** ------------------------------------------------------- D114 ---------------------------------------------------------- **/
$D114[0] = $DATA->TotalCashOutlay_0;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$TotalCashOutlay = 'TotalCashOutlay_' . $i;
	$D114[$i] = ($DATA->YearSale>=$i ? $DATA->$TotalCashOutlay : 0);
}
/** ------------------------------------------------------- D114 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D116 ---------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D116[$i] = ($D114[$i]==0 ? "N/A" : ($DATA->YearSale>=$i ? round(-$D59[$i]/$D114[$i]*100, 2) : 0));
}
/** ------------------------------------------------------- D116 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D117 ---------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D117[$i] = ($D114[$i]==0 ? "N/A" : ($DATA->YearSale>=$i ? round(-$D80[$i]/$D114[$i]*100, 2) : 0));
}
/** ------------------------------------------------------- D117 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D118 ---------------------------------------------------------- **/
$D59Total = 0;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D59Total = $D59Total + $D59[$i];
	$D118[$i] = ($D114[$i]==0 ? "N/A" : ($DATA->YearSale>=$i ? round((-$D59Total-($DATA->YearSale==$i ? $D89[$i] : 0))/$D114[$i]*100, 2) : 0));
}
/** ------------------------------------------------------- D118 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D119 ---------------------------------------------------------- **/
$D101Total = 0;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D101Total = $D101Total + $D101[$i];
	$D119[$i] = ($D114[$i]==0 ? "N/A" : ($DATA->YearSale>=$i ? round(-($D101Total)/$D114[$i]*100, 2) : 0));
}
/** ------------------------------------------------------- D119 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D120 ---------------------------------------------------------- **/
$D120[0] = $D86[0]+$D87[0];
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D120[$i] = ($DATA->YearSale>=$i ? $D86[$i]+$D87[$i] : 0);
}
/** ------------------------------------------------------- D120 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D121 ---------------------------------------------------------- **/
$D121[0] = $D85[0]-$D120[0];
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D121[$i] = ($DATA->YearSale>=$i ? $D85[$i]-$D120[$i] : 0);
}
/** ------------------------------------------------------- D121 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D122 ---------------------------------------------------------- **/
$D122[0] = ($DATA->YearSale>=0 ? $D121[0]+$D129[0] : 0);
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D122[$i] = ($DATA->YearSale>=$i ? $D121[$i]+$D129[$i] : 0);
}
/** ------------------------------------------------------- D122 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D123 ---------------------------------------------------------- **/
$D123[0] = $D120[0]/$D85[0]*100;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D123[$i] = ($DATA->YearSale>=$i ? round($D120[$i]/$D85[$i]*100, 2) : 0);
}
/** ------------------------------------------------------- D123 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D124 ---------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D124[$i] = ($DATA->YearSale>=$i ? ($D58[$i]>0.01 ? round($D49[$i]/$D58[$i], 2) : "N/A") : 0);
}
/** ------------------------------------------------------- D124 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D125 ---------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D125[$i] = ($DATA->YearSale>=$i ? round($D59[$i]/$D122[$i]*100, 2) : 0);
}
/** ------------------------------------------------------- D125 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D126 ---------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D126[$i] = ($DATA->YearSale>=$i ? round($D80[$i]/$D122[$i]*100, 2) : 0);
}
/** ------------------------------------------------------- D126 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D127 ---------------------------------------------------------- **/
$D127[0] = ($DATA->TotalLoanAmount>1 ? round($DATA->NetOperatingIncomeAnnual/$DATA->TotalLoanAmount*100, 2) : "N/A");
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D127[$i] = ($DATA->YearSale>=$i ? ($D120[$i]>0.01 ? round($D49[$i]/$D120[$i]*100, 2) : "N/A") : 0);
}
/** ------------------------------------------------------- D127 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D131 ---------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D131[$i] = ($DATA->YearSale>=$i ? round($D49[$i]/($DATA->PurchasePrice+$DATA->InitialCapitalImprovements)*100, 2) : 0);
}
/** ------------------------------------------------------- D131 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D134 ---------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D134[$i] = ($DATA->YearSale>=$i ? $D55[$i]+$D57[$i] : 0);
}
/** ------------------------------------------------------- D134 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D135 ---------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D135[$i] = ($DATA->YearSale>=$i ? $D54[$i]+$D56[$i] : 0);
}
/** ------------------------------------------------------- D135 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D138 ---------------------------------------------------------- **/
$D138[0] = $D122[0];
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D138[$i] = ($DATA->YearSale>=$i ? round($D122[$i]) : 0);
}
/** ------------------------------------------------------- D138 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D139 ---------------------------------------------------------- **/
$D139[0] = $DATA->DownPayment;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D139[$i] = ($DATA->YearSale>=$i ? $DATA->DownPayment : 0);
}
/** ------------------------------------------------------- D139 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D140 ---------------------------------------------------------- **/
$D140[0] = $D129[0];
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D140[$i] = ($DATA->YearSale>=$i ? round($D129[$i]) : 0);
}
/** ------------------------------------------------------- D140 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D141 ---------------------------------------------------------- **/
$D141[0] = $D85[0]-$DATA->PurchasePrice;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D141[$i] = ($DATA->YearSale>=$i ? $D85[$i]-$DATA->PurchasePrice : 0);
}
/** ------------------------------------------------------- D141 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D142 ---------------------------------------------------------- **/
$D142[0] = -($D120[0]-$D120[0]);
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D142[$i] = ($DATA->YearSale>=$i ? -($D120[$i]-$D120[0]) : 0);
}
/** ------------------------------------------------------- D142 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D143 ---------------------------------------------------------- **/
$D143[0] = $D138[0]-($D139[0]+$D140[0]+$D141[0]+$D142[0]);
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D143[$i] = ($DATA->YearSale>=$i ? $D138[$i]-($D139[$i]+$D140[$i]+$D141[$i]+$D142[$i]) : 0);
}
/** ------------------------------------------------------- D143 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D145 ---------------------------------------------------------- **/
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$D145[$i] = ($DATA->YearSale>=$i ? round(($D58[$i]+$D47[$i])/$D22[$i]*100, 2) : 0);
}
/** ------------------------------------------------------- D145 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D146 ---------------------------------------------------------- **/
$D146[0] = -$DATA->TotalCashOutlay+$DATA->InitialCapitalReserves_1;
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
	$D146[$i] = ($DATA->YearSale>=$i ? $D146[$i-1]+$D100[$i]+($D129[$i]-$D129[$i-1])-($i <= 30 ? $DATA->$CapitalImprovementSchedule : 0)-($DATA->YearSale==$i ? $D99[$i] : 0) : 0);
}
/** ------------------------------------------------------- D146 ---------------------------------------------------------- **/

/** ------------------------------------------------------- D147 ---------------------------------------------------------- **/
$D147[0] = $D146[0];
for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
	$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
	$D147[$i] = ($DATA->YearSale>=$i ? $D147[$i-1]+$D101[$i]+($D129[$i]-$D129[$i-1])-($i <= 30 ? $DATA->$CapitalImprovementSchedule : 0)-($DATA->YearSale==$i ? $D99[$i] : 0) : 0);
}
/** ------------------------------------------------------- D147 ---------------------------------------------------------- **/ 

$CapRate = round($DATA->NetOperatingIncomeAnnual/($DATA->PurchasePrice+$DATA->InitialCapitalImprovements+$DATA->FeeName)*100, 2); 

$GRM = round(($DATA->PurchasePrice+$DATA->InitialCapitalImprovements+$DATA->FeeName)/$DATA->GrossScheduledIncomeAnnual, 2); 

$SqFeet = round(($DATA->PurchasePrice+$DATA->InitialCapitalImprovements+$DATA->FeeName)/$DATA->PropertySquareFeet_Total, 2); 

$DCR = round(($DATA->TotalLoanAmount==0 ? "N/A" : $DATA->NetOperatingIncomeAnnual/-($DATA->FirstMortgagePaymentTotal*12)), 2); 

$DebtYield = ($DATA->TotalLoanAmount>1 ? round($DATA->NetOperatingIncomeAnnual/$DATA->TotalLoanAmount*100, 2) : "N/A"); 

$PaybackPeriod = round($DATA->TotalCashOutlay/((($DATA->OperatingIncomeMonthlyTotal-$DATA->OperatingExpenseTotalMonthly)+$DATA->FirstMortgagePaymentTotal)*12), 2); 

$BreakEvenRatio = round(-(($DATA->FirstMortgagePaymentTotal*12)-$DATA->OperatingExpenseTotalAnnual)/$DATA->OperatingIncomeAnnualTotal*100, 2); 

$Unit = round(($DATA->PurchasePrice+$DATA->InitialCapitalImprovements+$DATA->FeeName)/$DATA->PropertyNoOfUnit_Total);

$SalePrice = $D85[$DATA->YearSale];

$PreTaxIRR = ($DATA->TotalCashOutlay==0 ? "N/A" : round(IRR($D100, 0.05)*100, 2));

$AfterTaxIRR = ($DATA->TotalCashOutlay==0 ? "N/A" : round(IRR($D101, 0.05)*100, 2));

$PreTaxCumCashCash = ($DATA->TotalCashOutlay==0 ? "N/A" : $D118[$DATA->YearSale]);

$CumCashCash = ($DATA->TotalCashOutlay==0 ? "N/A" : $D119[$DATA->YearSale]);

$InitialCashInvestment = -$DATA->TotalCashOutlay;

$CapitalImprovementOutlaysAfterYear = 0;
for ( $i=1; $i<=30; $i++ ) {
	$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
	$CapitalImprovementOutlaysAfterYear += $DATA->$CapitalImprovementSchedule;
}

$NewD80 = $D80;
array_splice($NewD80, 0, 1);
$CashReceivedOverHoldingPeriod = round(array_sum($NewD80));

$FirstMortgagePayoff = -$D86[$DATA->YearSale];

$SecondMortgagePayoff = -$D87[$DATA->YearSale];

$CostOfSale = round(-$D88[$DATA->YearSale]);

$CapitalGains = round(-$D96[$DATA->YearSale]);

$AfterTaxCash = round($SalePrice+$FirstMortgagePayoff+$SecondMortgagePayoff+$CostOfSale+$CapitalGains); ?>

<?php if ( isset($print_data['RentalCoverPage']) ) {
	$CP = $print_data['RentalCoverPage']; ?>
	
	<div class="CoverPage">
    	<div class="col-lg-12">
	    	<h2 class="text-center title-border"><strong><?=$CP->Headline?></strong></h2>
			<h3 class="text-center"><strong><?=$CP->PropertyName?></strong></h3>
			<h4 class="text-center"><?=$CP->PropertyStreetAddress?></h4>
            <h4 class="text-center">
                <?=$CP->PropertyCityTown?>, 
                <?=$CP->PropertyStateProvince?>, 
                <?=$CP->PropertyZipCode?>, 
                <?=$CP->PropertyCountry?>
            </h4>
        </div>
        <div class="clearfix"></div>
		<?php if ( $CP->Photos ) { ?>
            <div class="col-lg-12 text-center">
                <?php $photos = explode('|', $CP->Photos); ?>
                <?php $covers = explode('|', $CP->Cover); ?>
                <?php $cover = array_search('on', $covers); ?>
                <img src="<?=$photos[$cover]?>" alt="" class="img-thumbnail" />
                <?php $captions = explode('||||', $CP->Captions); ?>
                <p><?=$captions[$cover]?></p>
            </div>
		<?php } ?>
        <div class="clearfix"></div>
        <div class="col-lg-12 text-center">
            <?php if ( $CP->SubHeadline1 ) { ?> <p><?=$CP->SubHeadline1?></p> <?php } ?>
            <?php if ( $CP->SubHeadline2 ) { ?> <p><?=$CP->SubHeadline2?></p> <?php } ?>
            <?php if ( $CP->SubHeadline3 ) { ?> <p><?=$CP->SubHeadline3?></p> <?php } ?>
            <?php if ( $CP->SubHeadline4 ) { ?> <p><?=$CP->SubHeadline4?></p> <?php } ?>
            <?php if ( $CP->SubHeadline5 ) { ?> <p><?=$CP->SubHeadline5?></p> <?php } ?>
            <br><strong><u>Exclusively Presented By:</u></strong><br>
            <?=$CP->YourName?><br>
            <?=$CP->CompanyName?><br>
            <?=$CP->CompanyStreet?><br>
            <?=$CP->CompanyCity?><br>
            <?=$CP->PhoneNumber?><br>
            <?=$CP->Email?><br>
            <?=$CP->Website?>
        </div>
        <div class="clearfix"></div>
	</div>  <!-- COMPLETE/TESTED -->
	
<?php } elseif ( isset($print_data['SinglePagePropertySummery']) ) {
	$SPPS = $print_data['SinglePagePropertySummery']; ?>
	
	<div class="SinglePagePropertySummer">
    	<div class="col-lg-12">
	    	<h3 class="text-center title-border"><strong>Property Summary</strong></h3>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-5 text-center">
            <p><strong><?=$SPPS->PropertyName?></strong></p>
            <p><strong><?=$SPPS->PropertyStreetAddress; ?></strong></p>
            <p><strong><?=$SPPS->PropertyCityTown?>, <?=$SPPS->PropertyStateProvince?>, <?=$SPPS->PropertyZipCode?>, <?=$SPPS->PropertyCountry?></strong></p>
            <p>
                <?php if ( $SPPS->Photos ) { ?>
                    <?php $photos = explode('|', $SPPS->Photos);
					$covers = explode('|', $SPPS->Cover);
					$cover = array_search('on', $covers); ?>
                    <img src="<?=$photos[$cover]?>" width="52%" alt="" class="img-thumbnail" />
                    <?php $captions = explode('||||', $SPPS->Captions); ?>
                    <p><?=$captions[$cover]?></p>
                <?php } ?>
            </p>
        </div>
        <div class="col-lg-2 text-right">
            <p><strong>Prepared By:</strong></p>
            <p>&nbsp;</p><p>&nbsp;</p>
            <p><strong>Contact Info:</strong></p>
        </div>
        <div class="col-lg-5 text-center">
            <p><?=$SPPS->YourName?></p>
            <p><?=$SPPS->CompanyName?></p>
            <p>&nbsp;</p>
            <p><?=$SPPS->CompanyStreet?></p>
            <p><?=$SPPS->CompanyCity?></p>
            <p><?=$SPPS->PhoneNumber?></p>
            <p><?=$SPPS->Email?></p>
            <p><?=$SPPS->Website?></p>
            <p>&nbsp;</p>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Purchase Price/Offer Price</td>
                        <td class="text-right">$<?=number_format($SPPS->PurchasePrice)?></td>
                    </tr>
                    <tr>
                        <td>DownPayment</td>
                        <td class="text-right">$<?=number_format($SPPS->DownPayment)?></td>
                    </tr>
                    <tr>
                        <td>Initial Capital Improvements and Reserves</td>
                        <td class="text-right">$<?=number_format($SPPS->InitialCapitalImprovements+$SPPS->InitialCapitalReserves_1)?></td>
                    </tr>
                    <tr>
                        <td>Total Closing Cost</td>
                        <?php $closing_cost = -((-$SPPS->InspectionAppraisal)+(-$SPPS->MortgageOneOriginationFee*($SPPS->RollupFirst=="No"?1:0))+(-$SPPS->MortgageTwoOriginationFee*($SPPS->SecondMortgageUsed=='Yes' ? ($SPPS->RollUpSecond=="No"?1:0):0))+(-$SPPS->MortgageOneDiscountFee*($SPPS->RollupFirst=="No"?1:0))+(-$SPPS->MortgageTwoDiscountFee*($SPPS->SecondMortgageUsed=='Yes' ? ($SPPS->RollUpSecond=="No"?1:0) : 0))+(-$SPPS->ClosingCostsMiscFee))-($SPPS->CashbackFromSeller);?>
                        <td class="text-right">$<?=number_format($closing_cost, 0)?></td>	
                    </tr>
                    <tr>
                        <td>Additional Upfront to Buyer (Optional)</td>
                        <td class="text-right">$<?=number_format($SPPS->FeeName,0);?></td>
                    </tr>
                    <tr>
                        <td>Total Cash Investment</td>
                        <td class="text-right">$<?=number_format($SPPS->TotalCashOutlay, 0)?></td>
                    </tr>
                    
                    <tr class="top-bg-border">
                        <td>Current/First Year Gross Rent</td>
                        <td class="text-right">$<?=number_format($SPPS->GrossScheduledIncomeAnnual, 0)?></td>
                    </tr>
                    <tr>
                        <td>Current/First Year Operating Expenses</td>
                        <td class="text-right">$<?=number_format($SPPS->OperatingExpenseTotalAnnual, 0)?></td>
                    </tr>
                    <tr>
                        <td>Vacancy, Concessions, Management Fees</td>
                        <td class="text-right">$<?=number_format(($SPPS->VacancyLossAnnual+$SPPS->ConcessionsAnnual+$SPPS->ManagementFeeAnnual), 0)?></td>
                    </tr>
                    <tr>
                        <td>Current/First Year Net Operating Income</td>
                        <td class="text-right">$<?=number_format($SPPS->NetOperatingIncomeAnnual, 0)?></td>
                    </tr>

                    <tr class="top-bg-border">
                        <td>Projected Holding Period</td>
                        <td class="text-right"><?=number_format($SPPS->YearSale, 0)?> Years</td>
                    </tr>
                    <tr>
                        <td>Terminal Cap Rate Assumed</td>
                        <td class="text-right"><?=round($SPPS->AppreciationRate)?>%</td>
                    </tr>
                    <tr>
                        <td>Projected Resale Price</td>
                        <td class="text-right">$<?=number_format($SalePrice, 0)?></td>
                    </tr>
                    <tr>
                        <td>Projected Leveraged IRR of Investment *</td>
                        <td class="text-right">
                            <?=round($PreTaxIRR)?>%
                        </td>
                    </tr>
                    <tr>
                        <td>Projected Leveraged MIRR of Investment *</td>
                        <td class="text-right">
                            <?php 
                            $LoopCount = (count($D100) > 13 ? 13 : count($D100));
                            for ( $i = 0; $i < $LoopCount; $i++ ) {
                                $COArray[$i] = $D100[$i];
                            } ?>
                            <?php $PreTaxMIRR = MIRR($COArray, ($SPPS->EquityInterestRatePaid/100), ($SPPS->ReinvestmentRateReceived/100))*100?>
                            <?php echo ($SPPS->TotalCashOutlay==0 ? "N/A" : round($PreTaxMIRR).'%')?>
                        </td>
                    </tr>
                    <tr>
                        <td>Projected Cumulative Cash-on-Cash Return *</td>
                        <td class="text-right">
                            <?=round($PreTaxCumCashCash)?>%
                        </td>
                    </tr>
                    <tr>
                        <td>Capitalization Rate</td>
                        <td class="text-right">
                            <?=round($CapRate)?>%
                        </td>
                    </tr>
                    <tr>
                        <td>Gross Rent Multiplier</td>
                        <td class="text-right"><?=number_format($GRM, 2)?></td>
                    </tr>
                    <tr>
                        <td>$/Sq Foot</td>
                        <td class="text-right">$<?=number_format($SqFeet, 2)?></td>
                    </tr>
                    <tr>
                        <td>$/Unit</td>
                        <td class="text-right">$<?=number_format($Unit, 0)?></td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered text-center">
                <thead>
                    <tr>
                        <th class="text-left">Selected Projected Financial Measures</th>
                        <th class="text-center">Year 1</th>
                        <th class="text-center">Year 12</th>
                        <th class="text-center">Year 25</th>
                        <th class="text-center">Year 40</th>
                    </tr>
                    <tr>
                        <td class="text-left">Annual Cash-on-Cash Return *</td>
                        <td><?=($SPPS->YearSale >= 1 && $SPPS->YearSale < 40 ? $SPPS->TotalCashOutlay == 0 ? 'N/A' : round($D116[1]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 12 && $SPPS->YearSale < 40 ? $SPPS->TotalCashOutlay == 0 ? 'N/A' : round($D116[12]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 25 && $SPPS->YearSale < 40 ? $SPPS->TotalCashOutlay == 0 ? 'N/A' : round($D116[25]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale == 40 ? $SPPS->TotalCashOutlay == 0 ? 'N/A' : round($D116[40]).'%' : 'N/A')?></td>
                    </tr>
                    <tr>
                        <td class="text-left">Cumulative Cash-on-Cash Return (ROI) *</td>
                        <td><?=($SPPS->YearSale >= 1 && $SPPS->YearSale < 40 ? $SPPS->TotalCashOutlay == 0 ? 'N/A' : round($D118[1]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 12 && $SPPS->YearSale < 40 ? $SPPS->TotalCashOutlay == 0 ? 'N/A' : round($D118[12]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 25 && $SPPS->YearSale < 40 ? $SPPS->TotalCashOutlay == 0 ? 'N/A' : round($D118[25]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale == 40 ? $SPPS->TotalCashOutlay == 0 ? 'N/A' : round($D116[40]).'%' : 'N/A')?></td>
                    </tr>
                    <tr>
                        <td class="text-left">DCR</td>
                        <td><?=($SPPS->YearSale >= 1 && $SPPS->YearSale < 40 ? ($D124[1] != 'N/A' ? number_format($D124[1], 2) : 'N/A') : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 12 && $SPPS->YearSale < 40 ? ($D124[1] != 'N/A' ? number_format($D124[12], 2) : 'N/A') : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 25 && $SPPS->YearSale < 40 ? ($D124[1] != 'N/A' ? number_format($D124[25], 2) : 'N/A') : 'N/A')?></td>
                        <td><?=($SPPS->YearSale == 40 ? ($D124[1] != 'N/A' ? number_format($D124[40], 2) : 'N/A') : 'N/A')?></td>
                    </tr>
                    <tr>
                        <td class="text-left">LTV</td>
                        <td><?=($SPPS->YearSale >= 1 && $SPPS->YearSale < 40 ? round($D123[1]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 12 && $SPPS->YearSale < 40 ? round($D123[12]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 25 && $SPPS->YearSale < 40 ? round($D123[25]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale == 40 ? number_format($D123[40], 2) : 'N/A')?></td>
                    </tr>
                    <tr>
                        <td class="text-left">Return on Equity *</td>
                        <td><?=($SPPS->YearSale >= 1 && $SPPS->YearSale < 40 ? round($D125[1]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 12 && $SPPS->YearSale < 40 ? round($D125[12]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 25 && $SPPS->YearSale < 40 ? round($D125[25]).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale == 40 ? number_format($D125[40], 2) : 'N/A')?></td>
                    </tr>
                    <tr>
                        <td class="text-left">Economic Occupancy %</td>
                        <td><?=($SPPS->YearSale >= 1 && $SPPS->YearSale < 40 ? round((($D16[1]-$D18[1]-$D19[1])/$D16[1])*100).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 12 && $SPPS->YearSale < 40 ? round((($D16[12]-$D18[12]-$D19[12])/$D16[12])*100).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 25 && $SPPS->YearSale < 40 ? round((($D16[25]-$D18[25]-$D19[25])/$D16[25])*100).'%' : 'N/A')?></td>
                        <td><?=($SPPS->YearSale == 40 ? number_format((($D16[40]-$D18[40]-$D19[40])/$D16[40])*100, 2) : 'N/A')?></td>
                    </tr>
                    <tr>
                        <td class="text-left">Effective Rent per sq foot</td>
                        <td><?=($SPPS->YearSale >= 1 && $SPPS->YearSale < 40 ? '$'.number_format($D22[1]/$SPPS->PropertySquareFeet_Total, 2) : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 12 && $SPPS->YearSale < 40 ? '$'.number_format($D22[12]/$SPPS->PropertySquareFeet_Total, 2) : 'N/A')?></td>
                        <td><?=($SPPS->YearSale >= 25 && $SPPS->YearSale < 40 ? '$'.number_format($D22[25]/$SPPS->PropertySquareFeet_Total, 2) : 'N/A')?></td>
                        <td><?=($SPPS->YearSale == 40 ? '$'.number_format($D22[40]/$SPPS->PropertySquareFeet_Total, 2) : 'N/A')?></td>
                    </tr>
                    <tr>
                        <td class="text-left" colspan="5">* All figures pre-tax</td>
                    </tr>
                </thead>
             </table>
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <td class="text-right"><strong>Unit Mix : </strong></td>
                        <td><?=$SPPS->UnitMix?></td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Description of Property : </strong></td>
                        <td><?=$SPPS->DescProp?></td>
                    </tr>
                    <tr>
                        <td class="text-right"><strong>Notes on Area : </strong></td>
                        <td><?=$SPPS->AreaNotes?></td>
                    </tr>
                </thead>
             </table>
        </div>
        <div class="clearfix"></div>
	</div>  <!-- COMPLETE/TESTED -->

<?php } elseif ( isset($print_data['RentalAdditionalPics']) ) {
	$AP = $print_data['RentalAdditionalPics']; ?>
	
    <?php if ( !empty($AP->Photos) ) { ?>
        <div class="PicsPage">
            <div class="col-lg-6 text-center">
                <h4><strong><?php echo $AP->PropertyName; ?></strong></h4>
                <p><?php echo $AP->PropertyStreetAddress; ?></p>
                <p><?php echo $AP->PropertyCityTown; ?>, <?=$AP->PropertyStateProvince?>, <?=$AP->PropertyZipCode?>, <?=$AP->PropertyCountry?></p>
            </div>
            <div class="col-lg-6 text-center">
                <h4><strong><?php echo $AP->YourName; ?></strong></h4>
                <p><?php echo $AP->CompanyName; ?></p>
                <p><?php echo $AP->PhoneNumber; ?></p>
            </div>
            <div class="clearfix"></div>
            <?php 
            $photos = explode('|', $AP->Photos);
            $captions = explode('||||', $AP->Captions);
            for ( $i = 0; $i < count($photos); $i++ ) { ?>
                
                <div class="col-lg-3 text-center">
                    <img src="<?=$photos[$i]?>" class="thumbnail" width="100%" alt="" />
                    <p><?=$captions[$i]?></p>
                </div>
                
            <?php } ?>
            <div class="clearfix"></div>
        </div>  <!-- COMPLETE/TESTED -->
    <?php } ?>

<?php } elseif ( isset($print_data['AnnualPropertyOperating']) ) {
	$APO = $print_data['AnnualPropertyOperating']; ?>

	<div class="AnnualPropertyOperating">
		<div class="col-lg-6 text-center">
			<p><strong><?=$APO->PropertyName?></strong></p>
			<p><?=$APO->PropertyStreetAddress; ?></p>
			<p>
				<?=$APO->PropertyCityTown?>, 
				<?=$APO->PropertyStateProvince?>, 
				<?=$APO->PropertyZipCode?>, 
				<?=$APO->PropertyCountry?>
            </p>
		</div>
		<div class="col-lg-6 text-center">
			<p><strong><?=$APO->YourName?></strong></p>
			<p><?=$APO->CompanyName?></p>
			<p><?=$APO->PhoneNumber?></p>
		</div>
		<div class="clearfix"></div>
        <div class="col-lg-12">
			<h3 class="text-center title-border"><strong>Annual Property Operating Statement</strong></h3>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td width="50%">&nbsp;</td>
                        <td width="25%" class="text-center">Annual</td>
                        <td width="25%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td><strong>Gross Income</strong></td>
                        <td class="text-center">$<?=number_format($APO->GrossScheduledIncomeAnnual, 0)?></td>
                        <td class="text-right">% of GI</td>
                    </tr>
                    <tr>
                        <td> - Vacancy</td>
                        <td class="text-center">$<?=number_format($APO->VacancyLossAnnual, 0)?></td>
                        <td class="text-right"><?=round($APO->VacancyLossPercentage)?>%</td>
                    </tr>
                    <tr>
                        <td> - Concessions</td>
                        <td class="text-center">$<?=number_format($APO->ConcessionsAnnual, 0)?></td>
                        <td class="text-right"><?=round($APO->ConcessionsParcentage)?>%</td>
                    </tr>
                    <tr>
                        <td> - Management</td>
                        <td class="text-center">$<?=number_format($APO->ManagementFeeAnnual, 0)?></td>
                        <td class="text-right"><?=round($APO->ManagementFeePercentage)?>%</td>
                    </tr>

                    <tr>
                        <td width="50%">Effective Gross Income</td>
                        <td width="25%" class="text-center">
                            $<?=number_format(($APO->GrossScheduledIncomeAnnual - ($APO->VacancyLossAnnual+$APO->ConcessionsAnnual+$APO->ManagementFeeAnnual)), 0)?>
                        </td>
                        <td width="25%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td> Other Income</td>
                        <td class="text-center">$<?=number_format($APO->OtherIncomeAnnual, 0)?></td>
                        <td class="text-right"><?=round(($APO->OtherIncomeMonthly/$APO->GrossScheduledIncomeMonthly)*100)?>%</td>
                    </tr>

                    <tr class="bg-border">
                        <td width="50%">Gross Operating Income </td>
                        <td width="25%" class="text-center">$<?=number_format($APO->OperatingIncomeAnnualTotal, 0)?></td>
                        <td width="25%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Economic Occupancy %</td>
                        <?php $gross_sc_income = ($APO->OperatingIncomeAnnualTotal - ($APO->VacancyLossAnnual+$APO->ConcessionsAnnual))/$APO->OperatingIncomeAnnualTotal; ?>
                        <td class="text-center"><?=round(($gross_sc_income*100))."%"; ?></td>
                        <td>&nbsp;</td>
                    </tr>

                    <tr>
                        <td width="50%">Operating Expenses:</td>
                        <td width="25%">&nbsp;</td>
                        <td width="25%" class="text-right"><u>% of GOI</u></td>
                    </tr>
                    <tr>
                        <td>Accounting </td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_1, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_1)?>%</td>
                    </tr>
                    <tr>
                        <td>Advertising </td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_2, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_2)?>%</td>
                    </tr>
                    <tr>
                        <td>Insurance</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_3, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_3)?>%</td>
                    </tr>
                    <tr>
                        <td>Janitorial </td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_4, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_4)?>%</td>
                    </tr>
                    <tr>
                        <td>Legal</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_5, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_5)?>%</td>
                    </tr>
                    <tr>
                        <td>Licenses</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_6, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_6)?>%</td>
                    </tr>
                    <tr>
                        <td>Miscellaneous</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_7, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_7)?>%</td>
                    </tr>
                    <tr>
                        <td>Repairs and Maintenance</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_8, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_8)?>%</td>
                    </tr>
                    <tr>
                        <td>Supplies</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_9, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_9)?>%</td>
                    </tr>
                    <tr>
                        <td>Taxes-Property</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_10, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_10)?>%</td>
                    </tr>
                    <tr>
                        <td>Lawn Care</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseAnnual_11, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpensePercentage_11)?>%</td>
                    </tr>
                    <tr>
                        <td>Other</td>
                        <td class="text-center"><?=($APO->OperatingExpenseAnnual_12==0 ? '-' : '$' . number_format($APO->OperatingExpenseAnnual_12, 0))?></td>
                        <td class="text-right"><?=($APO->OperatingExpensePercentage_12==0 ? '-' : round($APO->OperatingExpensePercentage_12) . '%')?></td>
                    </tr>
                    <tr>
                        <td>Other</td>
                        <td class="text-center"><?=($APO->OperatingExpenseAnnual_13==0 ? '-' : '$' . number_format($APO->OperatingExpenseAnnual_13, 0))?></td>
                        <td class="text-right"><?=($APO->OperatingExpensePercentage_13==0 ? '-' : round($APO->OperatingExpensePercentage_13) . '%')?></td>
                    </tr>
                    <tr>
                        <td>Other</td>
                        <td class="text-center"><?=($APO->OperatingExpenseAnnual_14==0 ? '-' : '$' . number_format($APO->OperatingExpenseAnnual_14, 0))?></td>
                        <td class="text-right"><?=($APO->OperatingExpensePercentage_14==0 ? '-' : round($APO->OperatingExpensePercentage_14) . '%')?></td>
                    </tr>
                    <tr>
                        <td>Other</td>
                        <td class="text-center"><?=($APO->OperatingExpenseAnnual_15==0 ? '-' : '$' . number_format($APO->OperatingExpenseAnnual_15, 0))?></td>
                        <td class="text-right"><?=($APO->OperatingExpensePercentage_15==0 ? '-' : round($APO->OperatingExpensePercentage_15) . '%')?></td>
                    </tr>
                    <tr>
                        <td colspan="3">Utilities:</td>
                    </tr>
                    <tr>
                        <td> Water/Sewar</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseWaterSewerAnnual, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpenseWaterSewerPercentage)?>%</td>
                    </tr>
                    <tr>
                        <td> Electricity</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseElectricityAnnual, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpenseElectricityPercentage)?>%</td>
                    </tr>
                    <tr>
                        <td> Gas</td>
                        <td class="text-center">$<?=($APO->OperatingExpenseGasAnnual==0 ? '-' : number_format($APO->OperatingExpenseGasAnnual, 0))?></td>
                        <td class="text-right"><?=($APO->OperatingExpenseGasPercentage==0 ? '-' : round($APO->OperatingExpenseGasPercentage) . '%')?></td>
                    </tr>
                    <tr>
                        <td> Fuel Oil</td>
                        <td class="text-center">$<?=($APO->OperatingExpenseFuelOilAnnual==0 ? '-' : number_format($APO->OperatingExpenseFuelOilAnnual, 0))?></td>
                        <td class="text-right"><?=($APO->OperatingExpenseFuelOilPercentage==0 ? '-' : round($APO->OperatingExpenseFuelOilPercentage) . '%')?></td>
                    </tr>
                    <tr>
                        <td> Other Utilities</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseOtherUtilitiesAnnual, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpenseOtherUtilitiesPercentage)?>%</td>
                    </tr>
                    <tr>
                        <td> Reserves</td>
                        <td class="text-center">$<?=number_format($APO->OperatingExpenseReservesAnnual, 0)?></td>
                        <td class="text-right"><?=round($APO->OperatingExpenseReservesPercentage)?>%</td>
                    </tr>

                    <tr class="bg-border">
                        <td width="50%">Total Operating Expenses</td>
                        <td width="25%" class="text-center">$<?=number_format($APO->OperatingExpenseTotalAnnual, 0)?></td>
                        <td width="25%" class="text-right"><?=round($APO->OperatingExpenseTotalPercentage)?>%</td>
                    </tr>

                    <tr class="bg-border">
                        <td width="50%"><strong>Net Operating Income</strong></td>
                        <td width="25%" class="text-center">$<?=number_format($APO->NetOperatingIncomeAnnual, 0)?></td>
                        <td width="25%" class="text-right"><?=round($APO->NetOperatingIncomePercentage)?>%</td>
                    </tr>

                    <tr>
                        <td width="50%">Debt Service(1st Mortgage)</td>
                        <td width="25%" class="text-center">$<?=number_format($APO->FirstMortgagePayment*12, 0)?></td>
                        <td width="25%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td width="50%">Debt Service(2nd Mortgage)</td>
                        <td width="25%" class="text-center">$<?=number_format($APO->SecondMortgagePayment*12, 0)?></td>
                        <td width="25%">&nbsp;</td>
                    </tr>

                    <tr class="bg-border">
                        <td width="50%">Total Debt Service</td>
                        <td width="25%" class="text-center">$<?=number_format($APO->FirstMortgagePaymentTotal*12, 0)?></td>
                        <td width="25%">&nbsp;</td>
                    </tr>
                    <tr class="bg-border">
                        <td><strong>Cash Flow Before Taxes</strong></td>
                        <td class="text-center">
                        <?=number_format(($APO->NetOperatingIncomeAnnual-($APO->FirstMortgagePaymentTotal*12*-1)), 0)?>
                        <td>&nbsp;</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td width="75%">Price</td>
                        <td width="25%" class="text-right">$<?=number_format($APO->PurchasePrice, 0)?></td>
                    </tr>
                    <tr>
                        <td>Sq Feet</td>
                        <td class="text-right"><?=$APO->PropertySquareFeet_Total?></td>
                    </tr>
                    <tr>
                        <td># of Units</td>
                        <td class="text-right"><?=$APO->PropertyNoOfUnit_Total?></td>
                    </tr>
                    <tr>
                        <td>$/Unit</td>
                        <td class="text-right">$<?=number_format(($APO->PurchasePrice+$APO->InitialCapitalImprovements+$APO->FeeName)/$APO->PropertyNoOfUnit_Total, 0)?></td>
                    </tr>
                    
                    <tr class="top-bg-border">
                        <td width="75%">Capitalization Rate (Cap Rate)</td>
                        <td width="25%" class="text-right"><?=round($CapRate)?>%</td>
                    </tr>
                    <tr>
                        <td>Gross Rent Multiplier (GRM)</td>
                        <td class="text-right"><?=number_format(($APO->PurchasePrice+$APO->InitialCapitalImprovements+$APO->FeeName)/$APO->GrossScheduledIncomeAnnual, 2)?></td>
                    </tr>
                    <tr>
                        <td>Payback Period</td>
                        <td class="text-right"><?=$PaybackPeriod;?></td>
                    </tr>
                    <tr>
                        <td>Debt Coverage Ratio (DCR)</td>
                        <td class="text-right"><?php echo $DCR;?></td>
                    </tr>
                    
                    <tr class="top-bg-border">
                        <td width="75%">Downpayment</td>
                        <td width="25%" class="text-right">$<?=number_format($APO->DownPayment, 0)?></td>
                    </tr>
                    <tr>
                        <td>Initial Cap Improvements/Reserves</td>
                        <td class="text-right">$<?=number_format($APO->InitialCapitalImprovements+$APO->InitialCapitalReserves_1, 0)?></td>
                    </tr>
                    <tr>
                        <td>Total Closing Costs</td>
                        <td class="text-right">$<?=number_format($APO->TotalSettlement-$APO->CashbackFromSeller, 0)?></td>
                    </tr>
                    <tr>
                        <td>Additional Upfront Fee to Buyer (Optional)</td>
                        <td class="text-right">$<?=number_format($APO->FeeName, 0)?></td>
                    </tr>
                    <tr>
                        <td>Total Investment</td>
                        <td class="text-right">$<?=number_format($APO->TotalCashOutlay, 0)?></td>
                    </tr>
                    <tr>
                        <td>Pre-Tax Cash/Cash Return</td>
                        <td class="text-right">
                            <?php echo round((((($APO->OperatingIncomeMonthlyTotal-$APO->OperatingExpenseTotalMonthly)+$APO->FirstMortgagePaymentTotal)*12)/$APO->TotalCashOutlay)*100)."%"; ?>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
	</div>  <!-- COMPLETE/TESTED -->

<?php } elseif ( isset($print_data['RentReport']) ) {
	$RR = $print_data['RentReport']; ?>
	
	<div class="RentReport">
		<div class="col-lg-6 text-center">
			<p><strong><?=$RR->PropertyName?></strong></p>
			<p><?=$RR->PropertyStreetAddress?></p>
			<p>
				<?=$RR->PropertyCityTown?>, 
				<?=$RR->PropertyStateProvince?>, 
				<?=$RR->PropertyZipCode?>, 
				<?=$RR->PropertyCountry?>
            </p>
		</div>
		<div class="col-lg-6 text-center">
			<p><strong><?=$RR->YourName?></strong></p>
			<p><?=$RR->CompanyName?></p>
			<p><?=$RR->PhoneNumber?></p>
		</div>
		<div class="clearfix"></div>
        <div class="col-lg-12">
        	<h3 class="text-center title-border"><strong>Operating Income Report</strong></h3>
            <?php for ( $a = 0; $a < $RR->YearSale; $a = $a + 10 ) {
                if ( ($a + 10) <= $RR->YearSale && $a == 0 ) {
                    $loop = 10;
                } elseif ( ($a + 10) <= $RR->YearSale && $a == 10 ) {
                    $loop = 20;
                } elseif ( ($a + 10) <= $RR->YearSale && $a == 20 ) {
                    $loop = 30;
                } else {
                    $loop = $RR->YearSale;
                } ?>
                <?php if ( $loop <= 10 ) {
					$span = 10;
				} elseif ( $loop <= 20 ) {
					$span = 20;
				} elseif ( $loop <= 30 ) {
					$span = 30;
				} else {
					$span = 40;
				} ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="34%" colspan="6">&nbsp;</th>
                            <th width="6%" class="text-center">Year (End)<br><br></th>
                            <?php 
                            $chq_date = str_replace("-", "/", $RR->PurchaseDate);
                            $closing_date  = date("Y-m-d", strtotime($chq_date)); ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $RR->YearSale ) { ?>
	                                <th width="6%" class="text-center"><?=$i?><br><?=date('d-m-Y', strtotime("$closing_date + $i years"))?></th>
                                <?php } else { ?>
                                	<th width="6%" class="text-center">&nbsp;</th>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center">Unit Type #</td>
                            <td class="text-center"># of units</td>
                            <td class="text-center">Unit type</td>
                            <td class="text-center">Square Ft.</td>
                            <td class="text-center">Monthly Rent<br />Per Unit</td>
                            <td class="text-center">Annual Rent</td>
                            <td class="text-center">Annual Growth</td>
                            <td colspan="10">&nbsp;</td>
                        </tr>
                        <?php 
                        $Unit = 0;
                        $TotalAnnualRate = array();
                        for ( $i=1; $i<=12; $i++ ) { ?>
                            <tr>
                                <?php 
                                $UnitStr = 'PropertyNoOfUnit_' . $i;
                                $UnittypeStr = 'PropertyUnitType_' . $i;
                                if ( $RR->$UnittypeStr == '' ) {
                                    $RR->$UnittypeStr = '-';
                                } else {
                                    $RR->$UnittypeStr = $RR->$UnittypeStr;
                                }
                                $PropertySquareFeet = 'PropertySquareFeet_' . $i;
                                $PropertyMonthlyRent = 'PropertyMonthlyRent_' . $i;
                                $PropertyAnnualRent = 'PropertyAnnualRent_' . $i;
                                $PropertyAnnualGrowthRate = 'PropertyAnnualGrowthRate_' . $i;
                                $AnnualRate = $RR->$PropertyAnnualRent;
                                ?>
                                <td class="text-center"><?=$i?></td>
                                <td class="text-center"><?=number_format($RR->$UnitStr, 0)?></td>
                                <td class="text-center"><?=$RR->$UnittypeStr?></td>
                                <td class="text-center"><?=number_format($RR->$PropertySquareFeet, 0)?></td>
                                <td class="text-center"><?=($RR->$PropertyMonthlyRent ? number_format($RR->$PropertyMonthlyRent, 0) : '-')?></td>
                                <td class="text-center"><?=number_format($RR->$PropertyAnnualRent, 0)?></td>
                                <?php if ( $RR->RentGrowthOveride == "No" ) { ?>
                                    <td class="text-center"><?=round($RR->$PropertyAnnualGrowthRate)?>%</td>
                                <?php } else { ?>
                                    <td class="text-center"><?=round($RR->RentGrowthRate)?>%</td>
                                <?php } ?>
                                <?php 
                                for ( $j=$a+1; $j<=$span; $j++ ) {
									if ( $j <= $RR->YearSale ) {
										if ( $j==1 ) { ?>
											<td class="text-center"><?=number_format($AnnualRate, 0)?></td>
										<?php
										} else {
											if ( $RR->RentGrowthOveride == "No" ) { ?>
												<td class="text-center"><?=number_format($AnnualRate = $AnnualRate + (($AnnualRate * $RR->$PropertyAnnualGrowthRate)/100), 0)?></td>
											<?php
											} else { ?>
												<td class="text-center"><?=number_format($AnnualRate = $AnnualRate + (($AnnualRate * $RR->RentGrowthRate)/100), 0)?></td>
											<?php
											}
										}
									} else { ?>
                                    	<td class="text-center">&nbsp;</td>
                                    <?php 
									}
                                    $TotalAnnualRate[$i][$j] = $AnnualRate;
                                } ?>
                            </tr>
                        <?php 
                        } ?>
                        <tr>
                            <td colspan="3">Gross Scheduled Income</td>
                            <td class="text-center"><?=number_format($RR->PropertySquareFeet_Total, 0)?></td>
                            <td class="text-center"><?=number_format($RR->GrossScheduledIncomeMonthly, 0)?></td>
                            <td class="text-center"><?=number_format($RR->GrossScheduledIncomeAnnual, 0)?></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) { ?>
                                <td class="text-center"><?=isset($D16[$i])?number_format($D16[$i], 0):''?></td>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="6">Vacancy Rate<br>Vacancy Loss</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D18[$i]) ) { ?>
	                                <td class="text-center"><?=round($RR->VacancyLossPercentage)?>%<br><?=number_format($D18[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="6">Concessions Rate<br>Concessions Amount</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D19[$i]) ) { ?>
                                	<td class="text-center"><?=round($RR->ConcessionsParcentage)?>%<br><?=number_format($D19[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="6">Management Fee</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D20[$i]) ) { ?>
                                	<td class="text-center"><?=round($RR->ManagementFeePercentage)?>%<br><?=number_format($D20[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3">Other Income</td>
                            <td class="text-center"><?=number_format($RR->OtherIncomeMonthly, 0)?></td>
                            <td class="text-center"><?=number_format($RR->OtherIncomeAnnual, 0)?></td>
                            <td class="text-center"><?=round($RR->OtherIncomeAnnualGrowthRate)?>%</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D21[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D21[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="6">Gross Operating Income</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D22[$i]) ) { ?>
                                	<td class="text-center"><?=number_format($D22[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="6">Physical Occupancy %<br>Economic Occupancy %<br>Effective Rent per Square Foot</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D16[$i])&&isset($D18[$i])&&isset($D19[$i]) ) { ?>
                                    <td class="text-center">
                                        <?=round(100-$RR->VacancyLossPercentage)?>%<br>
                                        <?=round((($D16[$i]-$D18[$i]-$D19[$i])/$D16[$i])*100)?>%<br>
                                        $<?=number_format($D22[$i]/$RR->PropertySquareFeet_Total, 2)?>
                                    </td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="7">Total Growth in Gross Operating Income</td>
                            <?php for ( $i=$a+2; $i<=$span; $i++ ) {
								if ( isset($D22[$i]) ) { ?>
                                	<td class="text-center"><?=round((($D22[$i]/$D22[$i-1])-1)*100)?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
	</div>  <!-- COMPLETE/TESTED -->
	
<?php } elseif ( isset($print_data['OperatingExpensesReport']) ) {
	$OER = $print_data['OperatingExpensesReport']; ?>
	
	<div class="OperatingExpensesReport">
		<div class="col-lg-6 text-center">
			<p><strong><?=$OER->PropertyName?></strong></p>
			<p><?=$OER->PropertyStreetAddress?></p>
			<p>
				<?=$OER->PropertyCityTown?>, 
				<?=$OER->PropertyStateProvince?>, 
				<?=$OER->PropertyZipCode?>, 
				<?=$OER->PropertyCountry?>
            </p>
		</div>
		<div class="col-lg-6 text-center">
			<p><strong><?=$OER->YourName?></strong></p>
			<p><?=$OER->CompanyName?></p>
			<p><?=$OER->PhoneNumber?></p>
		</div>
		<div class="clearfix"></div>
        <div class="col-lg-12">
        	<h3 class="text-center title-border"><strong>Operating Expenses Report</strong></h3>
            <?php for ( $a = 0; $a < $OER->YearSale; $a = $a + 10 ) {
                if ( ($a + 10) <= $OER->YearSale && $a == 0 ) {
                    $loop = 10;
                } elseif ( ($a + 10) <= $OER->YearSale && $a == 10 ) {
                    $loop = 20;
                } elseif ( ($a + 10) <= $OER->YearSale && $a == 20 ) {
                    $loop = 30;
                } else {
                    $loop = $OER->YearSale;
                } ?>
                <?php if ( $loop <= 10 ) {
                    $span = 10;
                } elseif ( $loop <= 20 ) {
                    $span = 20;
                } elseif ( $loop <= 30 ) {
                    $span = 30;
                } else {
                    $span = 40;
                } ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="30%" colspan="3">&nbsp;</th>
                            <th width="10%">Year (End)<br><br></th>
                            <?php 
                            $chq_date = str_replace("-", "/", $OER->PurchaseDate);
                            $closing_date  = date("Y-m-d", strtotime($chq_date)); ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
	                                <th width="6%" class="text-center"><?=$i?><br><?=date('d-m-Y', strtotime("$closing_date + $i years"))?></th>
                                <?php } else { ?>
                                	<th width="6%" class="text-center">&nbsp;</th>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="text-center">Current Monthly</td>
                            <td class="text-center">Current Annually</td>
                            <td class="text-center">Annual Growth Rate (%)</td>
                            <td colspan="10">&nbsp;</td>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_1?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_1, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_1, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_1) . '%';
                                } ?>
                            </td>
                            <?php $Expenses1 = array(); ?>
                            <?php $Expense1 = $OER->OperatingExpenseAnnual_1; ?>
                            <?php for ( $i=$a+1; $i <=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense1, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense1 = $Expense1 + ($Expense1 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense1 = $Expense1 + ($Expense1 * $OER->OperatingExpenseGrowthRate_1) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense1, 0)?></td>
                                    <?php }
								} else { ?>
                                    <td class="text-center">&nbsp;</td>
                                <?php 
                                } ?>
                                <?php $Expenses1[$i] = $Expense1; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_2?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_2, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_2, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_2) . '%';
                                } ?>
                            </td>
                            <?php $Expenses2 = array(); ?>
                            <?php $Expense2 = $OER->OperatingExpenseAnnual_2; ?>
                            <?php for ( $i=$a+1; $i <=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense2, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense2 = $Expense2 + ($Expense2 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense2 = $Expense2 + ($Expense2 * $OER->OperatingExpenseGrowthRate_2) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense2, 0)?></td>
                                    <?php }
								} else { ?>
                                    <td class="text-center">&nbsp;</td>
                                <?php 
                                } ?>
                                <?php $Expenses2[$i] = $Expense2; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_3?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_3, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_3, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_3) . '%';
                                } ?>
                            </td>
                            <?php $Expenses3 = array(); ?>
                            <?php $Expense3 = $OER->OperatingExpenseAnnual_3; ?>
                            <?php for ( $i=$a+1; $i <=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense3, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense3 = $Expense3 + ($Expense3 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense3 = $Expense3 + ($Expense3 * $OER->OperatingExpenseGrowthRate_3) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense3, 0)?></td>
                                    <?php }
								} else { ?>
                                    <td class="text-center">&nbsp;</td>
                                <?php 
                                } ?>
                                <?php $Expenses3[$i] = $Expense3; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_4?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_4, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_4, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_4) . '%';
                                } ?>
                            </td>
                            <?php $Expenses4 = array(); ?>
                            <?php $Expense4 = $OER->OperatingExpenseAnnual_4; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense4, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense4 = $Expense4 + ($Expense4 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense4 = $Expense4 + ($Expense4 * $OER->OperatingExpenseGrowthRate_4) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense4, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses4[$i] = $Expense4; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_5?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_5, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_5, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_5) . '%';
                                } ?>
                            </td>
                            <?php $Expenses5 = array(); ?>
                            <?php $Expense5 = $OER->OperatingExpenseAnnual_5; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense5, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense5 = $Expense5 + ($Expense5 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense5 = $Expense5 + ($Expense5 * $OER->OperatingExpenseGrowthRate_5) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense5, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses5[$i] = $Expense5; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_6?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_6, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_6, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_6) . '%';
                                } ?>
                            </td>
                            <?php $Expenses6 = array(); ?>
                            <?php $Expense6 = $OER->OperatingExpenseAnnual_6; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense6, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense6 = $Expense6 + ($Expense6 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense6 = $Expense6 + ($Expense6 * $OER->OperatingExpenseGrowthRate_6) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense6, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses6[$i] = $Expense6; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_7?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_7, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_7, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_7) . '%';
                                } ?>
                            </td>
                            <?php $Expenses7 = array(); ?>
                            <?php $Expense7 = $OER->OperatingExpenseAnnual_7; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense7, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense7 = $Expense7 + ($Expense7 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense7 = $Expense7 + ($Expense7 * $OER->OperatingExpenseGrowthRate_7) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense7, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses7[$i] = $Expense7; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_8?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_8, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_8, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_8) . '%';
                                } ?>
                            </td>
                            <?php $Expenses8 = array(); ?>
                            <?php $Expense8 = $OER->OperatingExpenseAnnual_8; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense8, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense8 = $Expense8 + ($Expense8 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense8 = $Expense8 + ($Expense8 * $OER->OperatingExpenseGrowthRate_8) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense8, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses8[$i] = $Expense8; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_9?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_9, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_9, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_9) . '%';
                                } ?>
                            </td>
                            <?php $Expenses9 = array(); ?>
                            <?php $Expense9 = $OER->OperatingExpenseAnnual_9; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense9, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense9 = $Expense9 + ($Expense9 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense9 = $Expense9 + ($Expense9 * $OER->OperatingExpenseGrowthRate_9) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense9, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses9[$i] = $Expense9; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_10?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_10, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_10, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_10) . '%';
                                } ?>
                            </td>
                            <?php $Expenses10 = array(); ?>
                            <?php $Expense10 = $OER->OperatingExpenseAnnual_10; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense10, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense10 = $Expense10 + ($Expense10 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense10 = $Expense10 + ($Expense10 * $OER->OperatingExpenseGrowthRate_10) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense10, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses10[$i] = $Expense10; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_11?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_11, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_11, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_11) . '%';
                                } ?>
                            </td>
                            <?php $Expenses11 = array(); ?>
                            <?php $Expense11 = $OER->OperatingExpenseAnnual_11; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense11, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense11 = $Expense11 + ($Expense11 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense11 = $Expense11 + ($Expense11 * $OER->OperatingExpenseGrowthRate_11) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense11, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses11[$i] = $Expense11; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_12?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseMonthly_12, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseAnnual_12, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGrowthRate_12) . '%';
                                } ?>
                            </td>
                            <?php $Expenses12 = array(); ?>
                            <?php $Expense12 = $OER->OperatingExpenseAnnual_12; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense12, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense12 = $Expense12 + ($Expense12 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense12 = $Expense12 + ($Expense12 * $OER->OperatingExpenseGrowthRate_12) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense12, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses12[$i] = $Expense12; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_13?></td>
                            <td class="text-center"><?=($OER->OperatingExpenseMonthly_13 ? number_format($OER->OperatingExpenseMonthly_13, 0) : '-')?></td>
                            <td class="text-center"><?=($OER->OperatingExpenseAnnual_13 ? number_format($OER->OperatingExpenseAnnual_13, 0) : '-')?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo ($OER->OperatingExpenseMonthly_13 ? round($OER->OpExpensesGrowthRate) . '%' : '-');
                                } else {
                                    echo ($OER->OperatingExpenseMonthly_13 ? round($OER->OperatingExpenseGrowthRate_13) . '%' : '-');
                                } ?>
                            </td>
                            <?php $Expenses13 = array(); ?>
                            <?php $Expense13 = $OER->OperatingExpenseAnnual_13; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense13, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense13 = $Expense13 + ($Expense13 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense13 = $Expense13 + ($Expense13 * $OER->OperatingExpenseGrowthRate_13) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense13, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses13[$i] = $Expense13; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_14?></td>
                            <td class="text-center"><?=($OER->OperatingExpenseMonthly_14 ? number_format($OER->OperatingExpenseMonthly_14, 0) : '-')?></td>
                            <td class="text-center"><?=($OER->OperatingExpenseAnnual_14 ? number_format($OER->OperatingExpenseAnnual_14, 0) : '-')?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo ($OER->OperatingExpenseMonthly_14 ? round($OER->OpExpensesGrowthRate) . '%' : '-');
                                } else {
                                    echo ($OER->OperatingExpenseMonthly_14 ? round($OER->OperatingExpenseGrowthRate_14) . '%' : '-');
                                } ?>
                            </td>
                            <?php $Expenses14 = array(); ?>
                            <?php $Expense14 = $OER->OperatingExpenseAnnual_14; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense14, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense14 = $Expense14 + ($Expense14 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense14 = $Expense14 + ($Expense14 * $OER->OperatingExpenseGrowthRate_14) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense14, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses14[$i] = $Expense14; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><?=$OER->OperatingExpensesOption_15?></td>
                            <td class="text-center"><?=($OER->OperatingExpenseMonthly_15 ? number_format($OER->OperatingExpenseMonthly_15, 0) : '-')?></td>
                            <td class="text-center"><?=($OER->OperatingExpenseAnnual_15 ? number_format($OER->OperatingExpenseAnnual_15, 0) : '-')?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo ($OER->OperatingExpenseMonthly_15 ? round($OER->OpExpensesGrowthRate) . '%' : '-');
                                } else {
                                    echo ($OER->OperatingExpenseMonthly_15 ? round($OER->OperatingExpenseGrowthRate_15) . '%' : '-');
                                } ?>
                            </td>
                            <?php $Expenses15 = array(); ?>
                            <?php $Expense15 = $OER->OperatingExpenseAnnual_15; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($Expense15, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $Expense15 = $Expense15 + ($Expense15 * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $Expense15 = $Expense15 + ($Expense15 * $OER->OperatingExpenseGrowthRate_15) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($Expense15, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses15[$i] = $Expense15; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td colspan="14"><strong>Utilities:</strong></td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;Water/Sewer</td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseWaterSewerMonthly, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseWaterSewerAnnual, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseWaterSewerGrowthRate) . '%';
                                } ?>
                            </td>
                            <?php $Expenses16 = array(); ?>
                            <?php $ExpenseWaterSewer = $OER->OperatingExpenseWaterSewerAnnual; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($ExpenseWaterSewer, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $ExpenseWaterSewer = $ExpenseWaterSewer + ($ExpenseWaterSewer * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $ExpenseWaterSewer = $ExpenseWaterSewer + ($ExpenseWaterSewer * $OER->OperatingExpenseWaterSewerGrowthRate) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($ExpenseWaterSewer, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses16[$i] = $ExpenseWaterSewer; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;Electricity</td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseElectricityMonthly, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseElectricityAnnual, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseElectricityGrowthRate) . '%';
                                } ?>
                            </td>
                            <?php $Expenses17 = array(); ?>
                            <?php $ExpenseElectricity = $OER->OperatingExpenseElectricityAnnual; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($ExpenseElectricity, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $ExpenseElectricity = $ExpenseElectricity + ($ExpenseElectricity * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $ExpenseElectricity = $ExpenseElectricity + ($ExpenseElectricity * $OER->OperatingExpenseElectricityGrowthRate) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($ExpenseElectricity, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses17[$i] = $ExpenseElectricity; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;Gas</td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseGasMonthly, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseGasAnnual, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseGasGrowthRate) . '%';
                                } ?>
                            </td>
                            <?php $Expenses18 = array(); ?>
                            <?php $ExpenseGas = $OER->OperatingExpenseGasAnnual; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($ExpenseGas, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $ExpenseGas = $ExpenseGas + ($ExpenseGas * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $ExpenseGas = $ExpenseGas + ($ExpenseGas * $OER->OperatingExpenseGasGrowthRate) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($ExpenseGas, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses18[$i] = $ExpenseGas; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;Fuel Oil</td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseFuelOilMonthly, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseFuelOilAnnual, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseFuelOilGrowthRate) . '%';
                                } ?>
                            </td>
                            <?php $Expenses19 = array(); ?>
                            <?php $ExpenseFuelOil = $OER->OperatingExpenseFuelOilAnnual; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($ExpenseFuelOil, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $ExpenseFuelOil = $ExpenseFuelOil + ($ExpenseFuelOil * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $ExpenseFuelOil = $ExpenseFuelOil + ($ExpenseFuelOil * $OER->OperatingExpenseFuelOilGrowthRate) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($ExpenseFuelOil, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses19[$i] = $ExpenseFuelOil; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;Other Utilities</td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseOtherUtilitiesMonthly, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseOtherUtilitiesAnnual, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseOtherUtilitiesGrowthRate) . '%';
                                } ?>
                            </td>
                            <?php $Expenses20 = array(); ?>
                            <?php $ExpenseOtherUtilities = $OER->OperatingExpenseOtherUtilitiesAnnual; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($ExpenseOtherUtilities, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $ExpenseOtherUtilities = $ExpenseOtherUtilities + ($ExpenseOtherUtilities * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $ExpenseOtherUtilities = $ExpenseOtherUtilities + ($ExpenseOtherUtilities * $OER->OperatingExpenseOtherUtilitiesGrowthRate) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($ExpenseOtherUtilities, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses20[$i] = $ExpenseOtherUtilities; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;Reserves</td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseReservesMonthly, 0)?></td>
                            <td class="text-center"><?=number_format($OER->OperatingExpenseReservesAnnual, 0)?></td>
                            <td class="text-center">
                                <?php 
                                if ( $OER->OpExpensesGrowthOverride == 'Yes' ) {
                                    echo round($OER->OpExpensesGrowthRate) . '%';
                                } else {
                                    echo round($OER->OperatingExpenseReservesGrowthRate) . '%';
                                } ?>
                            </td>
                            <?php $Expenses21 = array(); ?>
                            <?php $ExpenseReserves = $OER->OperatingExpenseReservesAnnual; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $OER->YearSale ) { ?>
									<?php if ( $i == 1 ) { ?>
                                        <td class="text-center"><?=number_format($ExpenseReserves, 0)?></td>
                                    <?php } else { ?>
                                        <?php if ( $OER->OpExpensesGrowthOverride == 'Yes' ) { ?>
                                            <?php $ExpenseReserves = $ExpenseReserves + ($ExpenseReserves * $OER->OpExpensesGrowthRate) / 100; ?>
                                        <?php } else { ?>
                                            <?php $ExpenseReserves = $ExpenseReserves + ($ExpenseReserves * $OER->OperatingExpenseReservesGrowthRate) / 100; ?>
                                        <?php } ?>
                                        <td class="text-center"><?=number_format($ExpenseReserves, 0)?></td>
                                    <?php } ?>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                                <?php $Expenses21[$i] = $ExpenseReserves; ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td colspan="4" class="text-center">Total Gross Operating Expenses</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D47[$i]) ) { ?>
                                	<td class="text-center"><?php echo ($OER->YearSale>=$i ? number_format($D47[$i],0) : ""); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td colspan="14"></td>
                        </tr>
                        <tr>
                            <td colspan="5" class="text-center">Total Growth in Operating Expenses</td>
                            <?php for ( $i=$a+2; $i<=$span; $i++ ) {
								if ( isset($D47[$i]) ) { ?>
	                                <td class="text-center"><?php echo ($OER->YearSale >= $i ? round((($D47[$i]/$D47[$i-1])-1)*100).'%' : "");?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>                            
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
	</div>  <!-- COMPLETE/TESTED -->

<?php } elseif ( isset($print_data['IncomeStatement']) ) {
	$IS = $print_data['IncomeStatement']; ?>
	
	<div class="IncomeStatement">
		<div class="col-lg-6 text-center">
			<p><strong><?=$IS->PropertyName?></strong></p>
			<p><?=$IS->PropertyStreetAddress?></p>
			<p><?=$IS->PropertyCityTown?>, <?=$IS->PropertyStateProvince?>, <?=$IS->PropertyZipCode?>, <?=$IS->PropertyCountry?></p>
		</div>
		<div class="col-lg-6 text-center">
			<p><strong><?=$IS->YourName?></strong></p>
			<p><?=$IS->CompanyName?></p>
			<p><?=$IS->PhoneNumber?></p>
		</div>
		<div class="clearfix"></div>
        <div class="col-lg-12">
        	<h3 class="text-center title-border"><strong>Income Statement</strong></h3>
            <?php for ( $a = 0; $a < $IS->YearSale; $a = $a + 10 ) {
                if ( ($a + 10) <= $IS->YearSale && $a == 0 ) {
                    $loop = 10;
                } elseif ( ($a + 10) <= $IS->YearSale && $a == 10 ) {
                    $loop = 20;
                } elseif ( ($a + 10) <= $IS->YearSale && $a == 20 ) {
                    $loop = 30;
                } else {
                    $loop = $IS->YearSale;
                } ?>
                <?php if ( $loop <= 10 ) {
                    $span = 10;
                } elseif ( $loop <= 20 ) {
                    $span = 20;
                } elseif ( $loop <= 30 ) {
                    $span = 30;
                } else {
                    $span = 40;
                } ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="17.5%">Year (End)<br><br></th>
                            <th width="7.5%" class="text-center"><?=($a==0 ? '0<br>' . $IS->PurchaseDate : '&nbsp;<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;')?></th>
                            <?php 
                            $chq_date = str_replace("-", "/", $IS->PurchaseDate);
                            $closing_date  = date("Y-m-d", strtotime($chq_date));
                            for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $IS->YearSale ) { ?>
	                                <th width="7.5%" class="text-center"><?=$i?><br><?=date('d-m-Y', strtotime("$closing_date + $i years"))?></th>
                                <?php } else { ?>
                                	<th width="7.5%" class="text-center">&nbsp;</th>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Gross Scheduled Income</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D16[$i]) ) { ?>
                                	<td class="text-center"><?=number_format($D16[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>+ Other Income</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D21[$i]) ) { ?>
                                	<td class="text-center"><?=number_format($D21[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>- Vacancy Loss</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D18[$i]) ) { ?>
                                	<td class="text-center"><?=number_format(-$D18[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>- Concessions</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D19[$i]) ) { ?>
                                	<td class="text-center"><?=number_format(-$D19[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>- Management Fees</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D20[$i]) ) { ?>
	                                <td class="text-center"><?=number_format(-$D20[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Effective Gross Income</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D22[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D22[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>- Operating Expenses</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D47[$i]) ) { ?>
	                                <td class="text-center"><?=number_format(-$D47[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Net Operating Income</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D49[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D49[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>- Mortgage Interest</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D55[$i]) && isset($D57[$i]) ) { ?>
	                                <td class="text-center"><?=number_format(-($D55[$i]+$D57[$i]), 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>- Depreciation Building</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D65[$i]) ) { ?>
	                                <td class="text-center"><?=number_format(-$D65[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>- Depreciation Improvements</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D66[$i]) ) { ?>
	                                <td class="text-center"><?=number_format(-$D66[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?> 
                        </tr>
                        <tr>
                            <td><strong>Taxable Income (Before write-offs)</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D67[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D67[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;Losses Carried Over</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D72[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D72[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Taxable Income (After write-offs)</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D76[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D76[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
	</div> <!-- COMPLETE/TESTED -->

<?php } elseif ( isset($print_data['RentalCashFlowReport']) ) {
	$RCFR = $print_data['RentalCashFlowReport']; ?>
	
	<div class="RentalCashFlowReport">
		<div class="col-lg-6 text-center">
			<p><strong><?=$RCFR->PropertyName?></strong></p>
			<p><?=$RCFR->PropertyStreetAddress?></p>
			<p><?=$RCFR->PropertyCityTown?>, <?=$RCFR->PropertyStateProvince?>, <?=$RCFR->PropertyZipCode?>, <?=$RCFR->PropertyCountry?></p>
		</div>
		<div class="col-lg-6 text-center">
			<p><strong><?=$RCFR->YourName?></strong></p>
			<p><?=$RCFR->CompanyName?></p>
			<p><?=$RCFR->PhoneNumber?></p>
		</div>
		<div class="clearfix"></div>
        <div class="col-lg-12">
        	<h3 class="text-center title-border"><strong>Cashflow Report</strong></h3>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="2">INITIAL CAPITAL OUTLAY SUMMARY</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><strong>Down Payment</strong></td>
                        <td class="text-right">$<?=number_format(-$RCFR->DownPayment, 2)?></td>
                    </tr>
                    <tr>
                        <td>Inspection and Appraisal</td>
                        <td class="text-right">$<?=number_format(-$RCFR->InspectionAppraisal, 2)?></td>
                    </tr>
                    <tr>
                        <td>Origination fee - 1st Mortgage</td>
                        <td class="text-right">$<?=number_format(-$RCFR->MortgageOneOriginationFee*($RCFR->RollupFirst=="No"?1:0), 2)?></td>
                    </tr>
                    <tr>
                        <td>Origination fee - 2nd Mortgage</td>
                        <td class="text-right">$<?=number_format(-$RCFR->MortgageTwoOriginationFee*($RCFR->SecondMortgageUsed=='Yes' ? ($RCFR->RollUpSecond=="No"?1:0):0), 2)?></td>
                    </tr>
                    <tr>
                        <td>Discount fee - 1st Mortgage</td>
                        <td class="text-right">$<?=number_format(-$RCFR->MortgageOneDiscountFee*($RCFR->RollupFirst=="No"?1:0), 2)?></td>
                    </tr>
                    <tr>
                        <td>Discount fee - 2nd Mortgage</td>
                        <td class="text-right">$<?=number_format(-$RCFR->MortgageTwoDiscountFee*($RCFR->SecondMortgageUsed=='Yes' ? ($RCFR->RollUpSecond=="No"?1:0) : 0), 2)?></td>
                    </tr>
                    <tr>
                        <td>Closing costs & misc. fees</td>
                        <td class="text-right">$<?=number_format(-$RCFR->ClosingCostsMiscFee, 2)?></td>
                    </tr>
                    <tr>
                        <td><strong>Total Closing Costs (Not financed)</strong></td>
                        <td class="text-right">$<?=number_format((-$RCFR->InspectionAppraisal)+(-$RCFR->MortgageOneOriginationFee*($RCFR->RollupFirst=="No"?1:0))+(-$RCFR->MortgageTwoOriginationFee*($RCFR->SecondMortgageUsed=='Yes' ? ($RCFR->RollUpSecond=="No"?1:0):0))+(-$RCFR->MortgageOneDiscountFee*($RCFR->RollupFirst=="No"?1:0))+(-$RCFR->MortgageTwoDiscountFee*($RCFR->SecondMortgageUsed=='Yes' ? ($RCFR->RollUpSecond=="No"?1:0) : 0))+(-$RCFR->ClosingCostsMiscFee), 2)?></td>
                    </tr>
                    <tr>
                        <td><strong>Cash back from Seller</strong></td>
                        <td class="text-right">$<?=number_format($RCFR->CashbackFromSeller, 2)?></td>
                    </tr>
                    <tr>
                        <td><strong>Initial Capital Improvements</strong></td>
                        <td class="text-right">$<?=number_format(-$RCFR->InitialCapitalImprovements, 2)?></td>
                    </tr>
                    <tr>
                        <td><strong>Initial Capital Reserves Deposited</strong></td>
                        <td class="text-right">$<?=number_format(-$RCFR->InitialCapitalReserves_1, 2)?></td>
                    </tr>
                    <tr>
                        <td><strong>Additional Upfront Fee to Buyer (Optional)</strong></td>
                        <td class="text-right">$<?=number_format(-$RCFR->FeeName, 2)?></td>
                    </tr>
                    <tr>
                        <td><strong>TOTAL CASH OUTLAY</strong></td>
                        <td class="text-right">$<?=number_format((-$RCFR->DownPayment)+((-$RCFR->InspectionAppraisal)+(-$RCFR->MortgageOneOriginationFee*($RCFR->RollupFirst=="No"?1:0))+(-$RCFR->MortgageTwoOriginationFee*($RCFR->SecondMortgageUsed=='Yes' ? ($RCFR->RollUpSecond=="No"?1:0):0))+(-$RCFR->MortgageOneDiscountFee*($RCFR->RollupFirst=="No"?1:0))+(-$RCFR->MortgageTwoDiscountFee*($RCFR->SecondMortgageUsed=='Yes' ? ($RCFR->RollUpSecond=="No"?1:0) : 0))+(-$RCFR->ClosingCostsMiscFee))+($RCFR->CashbackFromSeller)+(-$RCFR->InitialCapitalImprovements)+(-$RCFR->InitialCapitalReserves_1)+(-$RCFR->FeeName), 2)?></td>
                    </tr>
                </tbody>
            </table>

            <?php for ( $a = 0; $a < $RCFR->YearSale; $a = $a + 10 ) {
                if ( ($a + 10) <= $RCFR->YearSale && $a == 0 ) {
                    $loop = 10;
                } elseif ( ($a + 10) <= $RCFR->YearSale && $a == 10 ) {
                    $loop = 20;
                } elseif ( ($a + 10) <= $RCFR->YearSale && $a == 20 ) {
                    $loop = 30;
                } else {
                    $loop = $RCFR->YearSale;
                } ?>
                <?php if ( $loop <= 10 ) {
                    $span = 10;
                } elseif ( $loop <= 20 ) {
                    $span = 20;
                } elseif ( $loop <= 30 ) {
                    $span = 30;
                } else {
                    $span = 40;
                } ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="17.5%">Year (End)<br><br></th>
                            <?php 
                            $chq_date = str_replace("-", "/", $RCFR->PurchaseDate);
                            $closing_date  = date("Y-m-d", strtotime($chq_date)); ?>
                            <?php if ( $a == 0 ) { ?>
                                <th width="7.5%" class="text-center">0<br><?=date('d-m-Y', strtotime("$closing_date + 0 years"))?></th>
                            <?php } else { ?>
                                <th width="7.5%" class="text-center">&nbsp;<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</th>
                            <?php } ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $RCFR->YearSale ) { ?>
	                                <th width="7.5%" class="text-center"><?=$i?><br><?=date('d-m-Y', strtotime("$closing_date + $i years"))?></th>
                                <?php } else { ?>
                                	<th width="7.5%" class="text-center">&nbsp;</th>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Initial Outlay</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=number_format(-$RCFR->TotalCashOutlay, 0)?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>
                            <td colspan="10">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>Additional Capital Improvements</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $RCFR->YearSale ) { ?>
	                                <?php $CapitalImprovementSchedule = 'CapitalImprovementSchedule_'.$i; ?>
    	                            <td class="text-center"><?=number_format(($i <= 30 ? $RCFR->$CapitalImprovementSchedule : 0), 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Total Cash Outlay</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=number_format(-$RCFR->TotalCashOutlay, 0)?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $RCFR->YearSale ) { ?>
									<?php $TotalCashOutlay = 'TotalCashOutlay_'.$i; ?>
                                    <td class="text-center"><?=number_format($RCFR->$TotalCashOutlay, 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>

                        <tr>
                            <td>Gross Operating Income</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D22[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D22[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Gross Operating Expenses</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D47[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D47[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Net Operating Income</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D49[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D49[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Debt Service</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D58[$i]) ) { ?>
	                                <td class="text-center"><?=number_format(-$D58[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Cashflow (pre-tax)</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D59[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D59[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Pre-Tax Cash-on-Cash Return</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D116[$i]) ) { ?>
	                                <td class="text-center"><?=round($D116[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Income Taxes (Credits)</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D78[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D78[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Cashflow (after-tax)</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D80[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D80[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>After-Tax Cash-on-Cash Return</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D117[$i]) ) { ?>
	                                <td class="text-center"><?=round($D117[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Proceeds of Sale (pre-tax)</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D89[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D89[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Capital Gains Taxes</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D96[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D96[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Proceeds of Sale (after-tax)</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D89[$i]) && isset($D96[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D89[$i]-$D96[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Total Cashflows (pre-tax)</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D59[$i]) && isset($D89[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D59[$i]+($RCFR->YearSale==$i ? $D89[$i] : 0), 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Cumulative Pre-Tax Cash-on-Cash Return (ROI)</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D118[$i]) ) { ?>
	                                <td class="text-center"><?=round($D118[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Total Cashflows (after-tax)</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D101[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D101[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Cumulative After-Tax Cash-on-Cash Return (ROI)</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D119[$i]) ) { ?>
	                                <td class="text-center"><?=round($D119[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Reserves Balance</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=number_format($D129[0], 0)?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D129[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D129[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Total Cash Position (inc Reserves) *</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=number_format($D146[0], 0)?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D146[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D146[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Total Equity Position (Inc Reserves)</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=number_format($D121[0]+$D129[0], 0)?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D121[$i]) && isset($D129[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D121[$i]+$D129[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Return on Equity (ROE) *</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D125[$i]) ) { ?>
	                                <td class="text-center"><?=round($D125[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>*pre-tax</td>
                            <td colspan="11">
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
	</div> <!-- COMPLETE/TESTED -->

<?php } elseif ( isset($print_data['DiscountCashFlowAnalysis']) ) {
	$DCFA = $print_data['DiscountCashFlowAnalysis']; ?>
	
	<div class="DiscountCashFlowAnalysis">
		<div class="col-lg-6 text-center">
			<p><strong><?=$DCFA->PropertyName?></strong></p>
			<p><?=$DCFA->PropertyStreetAddress?></p>
			<p><?=$DCFA->PropertyCityTown?>, <?=$DCFA->PropertyStateProvince?>, <?=$DCFA->PropertyZipCode?>, <?=$DCFA->PropertyCountry?></p>
		</div>
		<div class="col-lg-6 text-center">
			<p><strong><?=$DCFA->YourName?></strong></p>
			<p><?=$DCFA->CompanyName?></p>
			<p><?=$DCFA->PhoneNumber?></p>
		</div>
		<div class="clearfix"></div>
        <div class="col-lg-12">
        	<h3 class="text-center title-border"><strong>Discount Cash Flow Analysis And Valuator</strong></h3>
            <?php for ( $a = 0; $a < $DCFA->YearSale; $a = $a + 10 ) {
                if ( ($a + 10) <= $DCFA->YearSale && $a == 0 ) {
                    $loop = 10;
                } elseif ( ($a + 10) <= $DCFA->YearSale && $a == 10 ) {
                    $loop = 20;
                } elseif ( ($a + 10) <= $DCFA->YearSale && $a == 20 ) {
                    $loop = 30;
                } else {
                    $loop = $DCFA->YearSale;
                } ?>
                <?php if ( $loop <= 10 ) {
                    $span = 10;
                } elseif ( $loop <= 20 ) {
                    $span = 20;
                } elseif ( $loop <= 30 ) {
                    $span = 30;
                } else {
                    $span = 40;
                } ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="17.5%">Year (End)<br><br></th>
                            <?php 
                            $chq_date = str_replace("-", "/", $DCFA->PurchaseDate);
                            $closing_date  = date("Y-m-d", strtotime($chq_date)); ?>
                            <th width="7.5%" class="text-center">0<br><?=date('d-m-Y', strtotime("$closing_date + 0 years"))?></th>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $DCFA->YearSale ) { ?>
                                	<th width="7.5%" class="text-center"><?=$i?><br><?=date('d-m-Y', strtotime("$closing_date + $i years"))?></th>
                                <?php } else { ?>
                                	<th width="7.5%" class="text-center">&nbsp;</th>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td><strong>Cumulative Cash Outlay</strong></td>
                            <td class="text-center"><?=number_format($DCFA->TotalCashOutlay_0, 2)?></td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $DCFA->YearSale ) { ?>
									<?php $TotalCashOutlay = 'TotalCashOutlay_'.$i; ?>
                                    <td class="text-center"><?=number_format($DCFA->$TotalCashOutlay, 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Gross Operating Income</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D22[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D22[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Gross Operating Expenses</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D47[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D47[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Net Operating Income</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D49[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D49[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Mortgage Payments</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D58[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D58[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Pre-tax Cashflow</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D59[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D59[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Pre-Tax Cash-on-Cash Return</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $DCFA->YearSale ) { ?>
	                                <?php $TotalCashOutlay = 'TotalCashOutlay_'.$i; ?>
    	                            <td class="text-center"><?=round(($D59[$i]/-$DCFA->$TotalCashOutlay)*100)?>%</td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Income Taxes (Credits)</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D78[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D78[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>After-Tax Cashflow</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D80[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D80[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>  
                        <tr>
                            <td>After-Tax Cash-on-Cash Return</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $DCFA->YearSale ) { ?>
									<?php $TotalCashOutlay = 'TotalCashOutlay_'.$i; ?>
                                    <td class="text-center"><?=round(($D80[$i]/-$DCFA->$TotalCashOutlay)*100)?>%</td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>			
                        <tr>
                            <td>Sale Price</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D85[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D85[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Gross Sale Proceeds</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D89[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D89[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Capital Gains Taxes</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D96[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D96[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Reserves Release</td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D99[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D99[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Total After-Tax Cashflow (CFAT)</td>
                            <td class="text-center"><?php echo number_format(-$DCFA->TotalCashOutlay,0); ?></td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D101[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D101[$i], 0)?></td>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td colspan="12"><strong>(1) Present Value of Property based on NOI and Projected Resale Value</strong></td>	
                        </tr>
                        <tr>
                            <td><strong>Cashflow Stream</strong></td>
                            <td>&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $DCFA->YearSale ) { ?>
	                                <td class="text-center">$<?=number_format($D49[$i]+($DCFA->YearSale==$i ? $SalePrice : 0), 0)?></td>
    	                            <?php $Row27[] = $D49[$i]+($DCFA->YearSale==$i ? $SalePrice : 0); ?>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Present Value of Property (PV):</strong></td>
                            <td>&nbsp;</td>
                            <td class="text-center">
                                <?php $C28 = NPV(($DCFA->DiscRate/100), $Row27); ?>
                                $<?=number_format($C28, 0)?>
                            </td>
                            <td colspan="9"></td>
                        </tr>
                        <tr>
                            <td><strong>Net Present Value of Property</strong> (PV minus Purchase Price)</td>
                            <td>&nbsp;</td>
                            <td class="text-center">
                                <?php $C29 = $C28-$DCFA->PurchasePrice; ?>
                                $<?=number_format($C29, 0)?>
                            </td>
                            <td colspan="9">
                                <?=($C29<0 ? " NPV of Property is Negative. Required Rate of Return will NOT be Achieved" : " NPV of Property is Positive.  Required Rate of Return will be Exceeded")?>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="12"><strong>(2) Present Value of Cash Investment based on Pre-Tax Cash Flows</strong></td>	
                        </tr>
                        <tr>
                            <td><strong>Cashflow Stream</strong></td>
                            <td class="text-center"><strong><?php echo number_format(-$DCFA->TotalCashOutlay,0); ?></strong></td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D59[$i]) && isset($D89[$i]) && isset($D99[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D59[$i]+($DCFA->YearSale==$i ? $D89[$i]+$D99[$i] : 0), 0)?></td>
    	                            <?php $Row32[] = $D59[$i]+($DCFA->YearSale==$i ? $D89[$i]+$D99[$i] : 0); ?>
                                <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Present Value of Investment (PV):</strong></td>
                            <td>&nbsp;</td>
                            <td class="text-center">
                                <?php $C33 = NPV(($DCFA->DiscRate/100), $Row32); ?>
                                $<?=number_format($C33, 0)?>
                            </td>
                            <td colspan="9"></td>
                        </tr>
                        <tr>
                            <td><strong>Net Present Value of Investment</strong> (PV minus Investment)</td>
                            <td>&nbsp;</td>
                            <td class="text-center">
                                <?php $C34 = $C33+(-$DCFA->TotalCashOutlay); ?>
                                $<?=number_format($C34, 0)?>
                            </td>
                            <td colspan="9">
                                <?=($C34<0 ? " NPV of Investment is Negative. Required Rate of Return will NOT be Achieved" : " NPV of Investment is Positive.  Required Rate of Return will be Exceeded")?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Pre-Tax Leveraged IRR</strong></td>
                            <td>&nbsp;</td>
                            <td class="text-center">
                                <?php 
                                $Row32B[0] = -$DCFA->TotalCashOutlay;
                                for ( $i=1; $i<=$DCFA->YearSale; $i++ ) {
									if ( isset($D59[$i]) && isset($D89[$i]) && isset($D99[$i]) ) {
	                                    $Row32B[$i] = $D59[$i]+($DCFA->YearSale==$i ? $D89[$i]+$D99[$i] : 0);
									}
                                } ?>
                                <?=round(IRR($Row32B, 0.05)*100)?>%
                            </td>
                            <td colspan="9"></td>
                        </tr>
                        <tr>
                            <td><strong>Pre-Tax Leveraged MIRR</strong></td>
                            <td>&nbsp;</td>
                            <td class="text-center">
                                <?=round(MIRR($Row32B, ($DCFA->EquityInterestRatePaid/100), ($DCFA->ReinvestmentRateReceived/100))*100)?>%
                            </td>
                            <td colspan="9"></td>
                        </tr>
                        
                        <tr>
                            <td colspan="12"><strong>(3) Present Value of Cash Investment based on After-Tax Cash Flows</strong></td>	
                        </tr>
                        <tr>
                            <td><strong>Cashflow Stream</strong></td>
                            <td class="text-center"><strong><?php echo number_format(-$DCFA->TotalCashOutlay,0); ?></strong></td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D101[$i]) ) { ?>
	                                <td class="text-center">$<?=number_format($D101[$i], 0)?></td>
	                                <?php $Row39[] = $D101[$i]; ?>
	                            <?php } else { ?>
                            		<td class="text-center">&nbsp;</td>
                                <?php } ?>
							<?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Present Value of Investment (PV):</strong></td>
                            <td>&nbsp;</td>
                            <td class="text-center">
                                <?php $C40 = NPV(($DCFA->DiscRate/100), $Row39); ?>
                                $<?=number_format($C40, 0)?>
                            </td>
                            <td colspan="9"></td>
                        </tr>
                        <tr>
                            <td><strong>Net Present Value of Investment</strong> (PV minus Investment)</td>
                            <td>&nbsp;</td>
                            <td class="text-center">
                                <?php $C41 = $C40+(-$DCFA->TotalCashOutlay); ?>
                                $<?=number_format($C41, 0)?>
                            </td>
                            <td colspan="9">
                                <?=($C41<0 ? " NPV of Investment is Negative. Required Rate of Return will NOT be Achieved" : " NPV of Investment is Positive.  Required Rate of Return will be Exceeded")?>
                            </td>
                        </tr>
                        <tr>
                            <td><strong>Pre-Tax Leveraged IRR</strong></td>
                            <td>&nbsp;</td>
                            <td class="text-center">
                                <?php 
                                $Row39B[0] = -$DCFA->TotalCashOutlay;
                                for ( $i=1; $i<=$DCFA->YearSale; $i++ ) {
									if ( isset($D101[$i]) ) {
	                                    $Row39B[$i] = $D101[$i];
									}
                                } ?>
                                <?=round(IRR($Row39B, 0.05)*100)?>%
                            </td>
                            <td colspan="9"></td>
                        </tr>
                        <tr>
                            <td><strong>Pre-Tax Leveraged MIRR</strong></td>
                            <td>&nbsp;</td>
                            <td class="text-center">
                                <?=round(MIRR($Row39B, ($DCFA->EquityInterestRatePaid/100), ($DCFA->ReinvestmentRateReceived/100))*100)?>%
                            </td>
                            <td colspan="9"></td>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="2" class="text-center">ASSUMPTIONS</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Purchase Price</td>
                        <td class="text-center">$<?php echo number_format($DCFA->PurchasePrice, 2);?></td>
                    </tr>
                    <tr>
                        <td> Initial Capital Improvements</td>
                        <td class="text-center">$<?php echo number_format($DCFA->InitialCapitalImprovements, 2);?></td>
                    </tr>
                    <tr>
                        <td> Additional Upfront Fee to Buyer (Optional)</td>
                        <td class="text-center">$<?php echo number_format($DCFA->FeeName, 2);?></td>
                    </tr>
                    <tr>
                        <td> Total Cash Invested</td>
                        <td class="text-center">$<?php echo number_format($DCFA->TotalCashOutlay, 2);?></td>
                    </tr>
                    <tr>
                        <td> Holding Term</td>
                        <td class="text-center"><?php echo number_format($DCFA->YearSale)." Years";?></td>
                    </tr>
                    <tr>
                        <td> Discount Rate for DCF Valuation</td>
                        <td class="text-center"><?php echo round($DCFA->DiscRate)."%";?></td>
                    </tr>
                    <tr>
                        <td> Type of Resale Calculation</td>
                        <td class="text-center"><?php echo $DCFA->Resale;?></td>
                    </tr>
                    <tr>
                        <td> Resale Cap Rate Assumed</td>
                        <td class="text-center"><?php echo round($DCFA->AppreciationRate)."%";?></td>
                    </tr>
                    <tr>
                        <td> Annual Rent Growth  (1st-Year)</td>
                        <td class="text-center"><?=round((($D22[2]/$D22[1])-1)*100)?>%</td>
                    </tr>
                    <tr>
                        <td> Annual Operating Expenses Growth (1st Year)</td>
                        <td class="text-center"><?=round((($D47[2]/$D47[1])-1)*100)?>%</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
	</div> <!-- COMPLETE/TESTED -->

<?php } elseif ( isset($print_data['LenderSummary']) ) {
	$LS = $print_data['LenderSummary']; ?>
	
	<div class="LenderSummary">
		<div class="col-lg-6 text-center">
			<p><strong><?=$LS->PropertyName?></strong></p>
			<p><?=$LS->PropertyStreetAddress?></p>
			<p><?=$LS->PropertyCityTown?>, <?=$LS->PropertyStateProvince?>, <?=$LS->PropertyZipCode?>, <?=$LS->PropertyCountry?></p>
		</div>
		<div class="col-lg-6 text-center">
			<p><strong><?=$LS->YourName?></strong></p>
			<p><?=$LS->CompanyName?></p>
			<p><?=$LS->PhoneNumber?></p>
		</div>
		<div class="clearfix"></div>
        <div class="col-lg-12">
        	<h3 class="text-center title-border"><strong>Debt Coverage and LTV Projections</strong></h3>
            <?php for ( $a = 0; $a < $LS->YearSale; $a = $a + 10 ) {
                if ( ($a + 10) <= $LS->YearSale && $a == 0 ) {
                    $loop = 10;
                } elseif ( ($a + 10) <= $LS->YearSale && $a == 10 ) {
                    $loop = 20;
                } elseif ( ($a + 10) <= $LS->YearSale && $a == 20 ) {
                    $loop = 30;
                } else {
                    $loop = $LS->YearSale;
                } ?>
                <?php if ( $loop <= 10 ) {
                    $span = 10;
                } elseif ( $loop <= 20 ) {
                    $span = 20;
                } elseif ( $loop <= 30 ) {
                    $span = 30;
                } else {
                    $span = 40;
                } ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="17.5%">Year (End)<br><br></th>
                            <?php 
                            $chq_date = str_replace("-", "/", $LS->PurchaseDate);
                            $closing_date  = date("Y-m-d", strtotime($chq_date)); ?>
                            <?php if ( $a == 0 ) { ?>
                                <th width="7.5%" class="text-center">0<br><?=date('d-m-Y', strtotime("$closing_date + 0 years"))?></th>
                            <?php } else { ?>
                                <th width="7.5%" class="text-center">&nbsp;<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</th>
                            <?php } ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $LS->YearSale ) { ?>
	                                <th width="7.5%" class="text-center"><?=$i?><br><?=date('d-m-Y', strtotime("$closing_date + $i years"))?></th>
                                <?php } else { ?>
                                	<th width="7.5%" class="text-center">&nbsp;</th>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Gross Operating Income</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D22[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D22[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Gross Operating Expenses</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D47[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D47[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Net Operating Income</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D49[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D49[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Debt Service</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D58[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D58[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>DCR (Debt Coverage Ratio)</strong></td>
                            <td class="text-center">&nbsp;</td>
                            <?php $total_dept_initial = $D86[0] + $D87[0];
                            for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D49[$i]) && isset($D58[$i]) ) {
									$total_dept = $D86[$i] + $D87[$i];?>
									<td class="text-center"><strong><?=($LS->YearSale >= $i ?($i==1 ? ($total_dept_initial > 1 ? number_format($D49[$i]/$D58[$i],2) : "N/A") : ($total_dept > 1 ? number_format($D49[$i]/$D58[$i],2) : "N/A")) : "");?></strong></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Projected Property Value</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D85[$i]) ) { ?>
	                                <td class="text-center"><?=($LS->YearSale>=$i ? number_format($D85[$i],0) : "");?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Total Debt</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?php echo number_format(($D86[0]+$D87[0]), 0); ?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php 
                            for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D86[$i]) && isset($D87[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format(($D86[$i]+$D87[$i]),0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Total Equity</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?php echo ($D85[0] != 0 ? number_format($D85[0]-($D86[0]+$D87[0]),0) :""); ?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D85[$i]) && isset($D86[$i]) && isset($D87[$i]) ) { ?>
	                                <td class="text-center"><?php echo ($D85[$i] != 0 ? number_format($D85[$i]-($D86[$i]+$D87[$i]),0) :""); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>LTV (Loan to Value)</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?php echo ($LS->YearSale >= 0 ? round((($D86[0]+$D87[0])/$D85[0])*100)."%":""); ?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D85[$i]) && isset($D86[$i]) && isset($D87[$i]) ) { ?>
	                                <td class="text-center"><?php echo ($LS->YearSale >= $i ? round((($D86[$i]+$D87[$i])/$D85[$i])*100)."%":""); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Debt Yield</strong></td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><strong><?php echo $DebtYield ?></strong></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D49[$i]) && isset($D86[$i]) && isset($D87[$i]) ) { ?>
	                                <td class="text-center"><strong><?php echo ($D86[$i]+$D87[$i]>1 ? number_format($D49[$i]/($D86[$i]+$D87[$i])*100, 2) : 'N/A'); ?></strong></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-6">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td colspan="2">ASSUMPTIONS</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Purchase Price</td>
                        <td class="text-center"><?php echo '$'.number_format($LS->PurchasePrice, 2); ?></td>
                    </tr>
                    <tr>
                        <td>Initial Capital Improvements</td>
                        <td class="text-center"><?php echo '$'.number_format($LS->InitialCapitalImprovements, 2); ?></td>
                    </tr>
                    <tr>
                        <td>Initial Reserves Deposited</td>
                        <td class="text-center"><?php echo '$'.number_format($LS->InitialCapitalReserves_1, 2); ?></td>
                    </tr>
                    <tr>
                        <td>Holding Term</td>
                        <td class="text-center"><?php echo $LS->YearSale." Years" ?></td>
                    </tr>
                    <tr>
                        <td>Type of Property Value Calculation</td>
                        <td class="text-center"><?php echo $LS->Resale; ?></td>
                    </tr>
                    <tr>
                        <td>Resale Cap Rate Assumed</td>
                        <td class="text-center"><?php echo round($LS->AppreciationRate).'%'; ?></td>
                    </tr>
                    <tr>
                        <td>Annual Rent Growth  (Effective)</td>
                        <td class="text-center"><?php echo round((($D22[2]/$D22[1])-1)*100)."%"; ?></td>
                    </tr>
                    <tr>
                        <td>Annual Operating Expenses Growth</td>
                        <td class="text-center"> <?php echo round((($D47[2]/$D47[1])-1)*100)."%"; ?></td>
                    </tr>
                    <tr>
                        <td>Buy-in Cap Rate</td>
                        <td class="text-center"><?=round($CapRate)?>%</td>
                    </tr>
                    <tr>
                        <td>Buy-in GRM</td>
                        <td class="text-center"><?=number_format($GRM, 2)?></td>
                    </tr>
                    <tr>
                        <td>Buy-in $/Sq Ft</td>
                        <td class="text-center"><?=number_format($SqFeet, 2)?></td>
                    </tr>
                    <tr>
                        <td>Buy-in DCR</td>
                        <td class="text-center"><?=number_format($DCR, 2)?>
                        </td>
                    </tr>
                    <tr>
                        <td>Buy-in Debt Yield</td>
                        <td class="text-center"><?=($DebtYield != 'N/A' ? round($DebtYield) . '%' : 'N/A')?></td>
                    </tr>
                    <tr>
                        <td>Buy-in Cash/Cash (Pre-Tax)</td>
                        <td class="text-center"><?=round((($LS->NetOperatingIncomeMonthly+$LS->FirstMortgagePaymentTotal)*12)/$LS->TotalCashOutlay*100)?>%</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-6">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td>MORTGAGE ASSUMPTIONS:</td>
                        <td class="text-center">(Mortgage 1)</td>
                        <td class="text-center">(Mortgage 2)</td>
                    </tr>
                </thead>
                <thead>
                    <tr>
                        <td>Amount Borrowed</td>
                        <td class="text-center"><?php echo number_format($LS->FirstMortgageAmount, 2); ?></td>
                        <td class="text-center"><?php echo number_format($LS->SecondMortgageAmount, 2); ?></td>
                    </tr>
                    <tr>
                        <td>Initial LTV%</td>
                        <td class="text-center"><?php echo round($LS->FirstMortgageLTVPercent, 2); ?>%</td>
                        <td class="text-center"><?php echo round($LS->SecondMortgageLTVPercent, 2); ?>%</td>
                    </tr>
                    <tr>
                        <td>Type of Mortgage</td>
                        <td class="text-center"><?php echo $LS->FirstMortgageLoanType; ?></td>
                        <td class="text-center"><?php echo $LS->SecondMortgageLoanType; ?></td>
                    </tr>
                    <tr>
                        <td>Term</td>
                        <td class="text-center"><?php echo number_format($LS->FirstMortgageTerm, 2); ?></td>
                        <td class="text-center"><?php echo number_format($LS->SecondMortgageTerm, 2); ?></td>
                    </tr>
                    <tr>
                        <td>Interest Rate</td>
                        <td class="text-center"><?php echo round($LS->FirstMortgageRate); ?>%</td>
                        <td class="text-center"><?php echo round($LS->SecondMortgageRate); ?>%</td>
                    </tr>
                    <tr>
                        <td>Payment Amount (Monthly)</td>
                        <td class="text-center"><?php echo number_format($LS->FirstMortgagePayment, 2); ?></td>
                        <td class="text-center"><?php echo number_format($LS->SecondMortgagePayment, 2); ?></td>
                    </tr>
                </thead>
            </table>
        </div>
        <div class="clearfix"></div>
	</div> <!-- COMPLETE/TESTED -->

<?php } elseif ( isset($print_data['RentalRefiAnalysis']) ) {
	$RRA = $print_data['RentalRefiAnalysis']; ?>
	
	<div class="RentalRefiAnalysis">
        <div class="col-lg-6 text-center">
            <p><strong><?=$RRA->PropertyName?></strong></p>
            <p><?=$RRA->PropertyStreetAddress?></p>
            <p><?=$RRA->PropertyCityTown?>, <?=$RRA->PropertyStateProvince?>, <?=$RRA->PropertyZipCode?>, <?=$RRA->PropertyCountry?></p>
        </div>
        <div class="col-lg-6 text-center">
            <p><strong><?=$RRA->YourName?></strong></p>
            <p><?=$RRA->CompanyName?></p>
            <p><?=$RRA->PhoneNumber?></p>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12">
	        <h3 class="text-center title-border"><strong>REFINANCING ANALYSIS</strong></h3>
        </div>
		<div class="clearfix"></div>
        <div class="col-lg-6">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <td><strong>New Loan Assumptions</strong></td>
                        <td class="text-center"><strong>1st Mortgage</strong></td>
                        <td class="text-center"><strong>2nd Mortgage</strong></td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td width="50%">Used?</td>
                        <td class="text-center"><?=$this->session->userdata('FirstMortgageUsed')?></td>
                        <td class="text-center"><?=$this->session->userdata('SecondMortgageUsed')?></td>
                    </tr>
                    <tr>
                        <td width="50%">Loan Type</td>
                        <td class="text-center"><?=$this->session->userdata('FirstMortgageLoanType')?></td>
                        <td class="text-center"><?=$this->session->userdata('SecondMortgageLoanType')?></td>
                    </tr>
                    <tr>
                        <td width="50%">Term (In years)</td>
                        <td class="text-center"><?=$this->session->userdata('FirstMortgageTerm')?></td>
                        <td class="text-center"><?=$this->session->userdata('SecondMortgageTerm')?></td>
                    </tr>
                    <tr>
                        <td width="50%">LTV %</td>
                        <td class="text-center"><?=$this->session->userdata('FirstMortgageLTV')?>%</td>
                        <td class="text-center"><?=$this->session->userdata('SecondMortgageLTV')?>%</td>
                    </tr>
                    <tr>
                        <td width="50%">Rate</td>
                        <td class="text-center"><?=$this->session->userdata('FirstMortgageRate')?>%</td>
                        <td class="text-center"><?=$this->session->userdata('SecondMortgageRate')?>%</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td colspan="2">* Use the inputs below to project a future appraisal in order to determine in which year you'll be able to pull out your investment</td>
                    </tr>
                    <tr>
                        <td width="50%">Method for Forecasting Market Value:</td>
                        <td class="text-center"><?=$this->session->userdata('RefiResale')?></td>
                    </tr>
                    <tr>
                        <td width="50%">Terminal Cap Rate for Appraisal</td>
                        <td class="text-center"><?=$this->session->userdata('TerminalCapRate')?>%</td>
                        <input type="hidden" name="NewLoanAmount" id="NewLoanAmount" value="<?=$this->session->userdata('NewLoanAmount')?>" />
                    </tr>
                    <tr>
                        <td>Closing costs (% of new loan)</td>
                        <td class="text-center"><?=$this->session->userdata('ClosingCostNewRate')?>%</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12"><?=$this->session->userdata('RefinancingAnalysisHTML')?></div>
        <div class="clearfix"></div>
	</div> <!-- COMPLETE/TESTED -->

<?php } elseif ( isset($print_data['FullTaxTreatment']) ) {
	$FTT = $print_data['FullTaxTreatment']; ?>
	
	<div class="FullTaxTreatment">
		<div class="col-lg-6 text-center">
			<p><strong><?=$FTT->PropertyName?></strong></p>
			<p><?=$FTT->PropertyStreetAddress?></p>
			<p><?=$FTT->PropertyCityTown?>, <?=$FTT->PropertyStateProvince?>, <?=$FTT->PropertyZipCode?>, <?=$FTT->PropertyCountry?></p>
		</div>
		<div class="col-lg-6 text-center">
			<p><strong><?=$FTT->YourName?></strong></p>
			<p><?=$FTT->CompanyName?></p>
			<p><?=$FTT->PhoneNumber?></p>
		</div>
		<div class="clearfix"></div>
        <div class="col-lg-12">
        	<h3 class="text-center title-border"><strong>TAX TREATMENT AND SHELTER REPORT</strong></h3>
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Years to Depreciate Property:</td>
                        <td class="text-center"><?php echo $FTT->DepYears; ?></td>
                    </tr>
                    <tr>
                        <td>Land's % of Value</td>
                        <td class="text-center"><?php echo round($FTT->LandPercentage); ?>%</td>
                    </tr>
                    <tr>
                        <td>Income Tax Bracket %</td>
                        <td class="text-center"><?php echo round($FTT->TaxBracket); ?>%</td>
                    </tr>
                    <tr>
                        <td>Capital Gains %</td>
                        <td class="text-center"><?php echo round($FTT->CapitalGainsPercentage); ?>%</td>
                    </tr>
                </tbody>
            </table>
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Designation for Handling Passive Losses:</td>
                        <td class="text-center"><?php echo $FTT->Designation; ?></td>
                    </tr>
                    <tr>
                        <td>Maximum Annual Write-off vs. Non-Passive Income</td>
                        <td class="text-center"><?php echo number_format($Maxwriteoff, 2); ?></td>
                    </tr>
                </tbody>
            </table>
            <?php for ( $a = 0; $a < $FTT->YearSale; $a = $a + 10 ) {
                if ( ($a + 10) <= $FTT->YearSale && $a == 0 ) {
                    $loop = 10;
                } elseif ( ($a + 10) <= $FTT->YearSale && $a == 10 ) {
                    $loop = 20;
                } elseif ( ($a + 10) <= $FTT->YearSale && $a == 20 ) {
                    $loop = 30;
                } else {
                    $loop = $FTT->YearSale;
                } ?>
                <?php if ( $loop <= 10 ) {
                    $span = 10;
                } elseif ( $loop <= 20 ) {
                    $span = 20;
                } elseif ( $loop <= 30 ) {
                    $span = 30;
                } else {
                    $span = 40;
                } ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="17.5%">Year (End)<br><br></th>
                            <?php 
                            $chq_date = str_replace("-", "/", $FTT->PurchaseDate);
                            $closing_date  = date("Y-m-d", strtotime($chq_date)); ?>
                            <?php if ( $a == 0 ) { ?>
                                <th width="7.5%" class="text-center">0<br><?=date('d-m-Y', strtotime("$closing_date + 0 years"))?></th>
                            <?php } else { ?>
                                <th width="7.5%" class="text-center">&nbsp;<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</th>
                            <?php } ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $FTT->YearSale ) { ?>
	                                <th width="7.5%" class="text-center"><?=$i?><br><?=date('d-m-Y', strtotime("$closing_date + $i years"))?></th>
                                <?php } else { ?>
                                	<th width="7.5%" class="text-center">&nbsp;</th>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Net Operating Income (NOI) (A)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D49[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D49[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Mortgage Interest Paid (B)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D55[$i]) && isset($D57[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D55[$i]+$D57[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Mortgage Principal Paid (C)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D54[$i]) && isset($D56[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D54[$i]+$D56[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Pre-Tax Cashflow A-B-C</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D59[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D59[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Depreciable Basis (Original)</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?php echo number_format($D63[0], 0); ?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D63[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D63[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Depreciable Basis (Cap Improvements)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D64[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D64[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Depreciation (Original)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D65[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D65[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Depreciation (Cap Improvements)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D66[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D66[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;Cumulative Depreciation (D)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php $C29[0] = 0; ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($C29[$i-1]) && isset($D65[$i]) && isset($D66[$i]) ) { ?>
	                                <?php $C29[$i] = $C29[$i-1]+($D65[$i]+$D66[$i]); ?>
    	                            <td class="text-center"><?php echo number_format($C29[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Taxable Income (before Write-Offs) A-B-D</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D67[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D67[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Amount written off against Income this year</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D71[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D71[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Losses Carried Over</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D72[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D72[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Cumulative Losses Carried Over</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D73[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D73[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td colspan="12">Losses Previously Carried Over</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;used to Offset This Year's Income</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D74[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D74[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Taxable Income (Loss) (After Write-offs)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D76[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D76[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Income Taxes (Credits) (E)</strong></td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D78[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D78[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Projected Sale Price</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D85[$i]) ) { ?>
	                                <td class="text-center"><?php echo ($FTT->YearSale==$i ? number_format($D85[$i], 0) : 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Adjusted Basis</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?php echo number_format($D91[0], 0); ?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D91[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D91[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Projected Cost of Sale</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D88[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D88[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Projected Gain on Sale (before write-offs)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D93[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D93[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Accumulated Passive Losses</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D94[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D94[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Projected Gain on Sale (after write-offs)</strong></td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D95[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D95[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Capital Gains Taxes</strong></td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D96[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D96[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td><strong>Total Taxes (Credits)</strong></td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D78[$i]) && isset($D96[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format(($i==1 ? (TRUE ? $D78[$i] : ($FTT->YearSale==$i ? $D78[$i]+$D96[$i] : 0)) : ($FTT->YearSale>$i ? $D78[$i] : ($FTT->YearSale==$i ? $D78[$i]+$D96[$i] : 0))), 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>                            
                        <tr>
                            <td><strong>Cashflow After Taxes A-B-C-E</strong></td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D101[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D101[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td colspan="12">Remaining Accumulated Losses to</td>
                        </tr>
                        <tr>
                            <td>&nbsp;&nbsp;&nbsp;be carried over to other properties</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D98[$i]) ) { ?>
	                                <td class="text-center"><?php echo number_format($D98[$i], 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
	</div> <!-- COMPLETE/TESTED -->

<?php } elseif ( isset($print_data['PropertyValueEvolution']) ) {
	$PVE = $print_data['PropertyValueEvolution']; ?>
	
	<div class="PropertyValueEvolution">
		<div class="col-lg-6 text-center">
			<p><strong><?=$PVE->PropertyName?></strong></p>
			<p><?=$PVE->PropertyStreetAddress?></p>
			<p><?=$PVE->PropertyCityTown?>, <?=$PVE->PropertyStateProvince?>, <?=$PVE->PropertyZipCode?>, <?=$PVE->PropertyCountry?></p>
		</div>
		<div class="col-lg-6 text-center">
			<p><strong><?=$PVE->YourName?></strong></p>
			<p><?=$PVE->CompanyName?></p>
			<p><?=$PVE->PhoneNumber?></p>
		</div>
		<div class="clearfix"></div>
        <div class="col-lg-12">
			<h3 class="text-center title-border"><strong>PROPERTY VALUE AND DISPOSITION REPORT</strong></h3>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Purchase Price</td>
                        <td class="text-center">$<?=number_format($PVE->PurchasePrice, 2)?></td>
                    </tr>
                    <tr>
                        <td>Initial Capital Improvements</td>
                        <td class="text-center">$<?=number_format($PVE->InitialCapitalImprovements, 2)?></td>
                    </tr>
                    <tr>
                        <td><?=($PVE->Resale=="Set Cap Rate" ? "" : ($PVE->InitialCapitalImprovements>0 ? "Capital Improvements Add to FMV?" : ""))?></td>
                        <td class="text-center">
                            <?=($PVE->InitialCapitalImprovements>0 ? ($PVE->Resale=="Assume Annual Appreciation%" ? $PVE->FMVProperty : "") : "")?>
                        </td>
                    </tr>
                    <tr>
                        <td><?=($PVE->InitialCapitalImprovements>0 ? ($PVE->Resale=="Set Cap Rate" ? "" : "Fair Market Value Multiplier on Initial Repairs") : "")?></td>
                        <td class="text-center">
                            <?=($PVE->InitialCapitalImprovements>0 ? ($PVE->Resale=="Assume Annual Appreciation%" ? $PVE->FMVMultiplier : "") : "")?>
                        </td>
                    </tr>
                    <tr>
                        <td>Assumed Value of Property in year 0</td>
                        <td class="text-center">$<?=number_format($D85[0], 2)?></td>
                    </tr>
                    <tr>
                        <td>Holding Period</td>
                        <td class="text-center"><?=$PVE->YearSale?> Years</td>
                    </tr>
                    <tr>
                        <td>Type of Market Value/Resale Value Calculation</td>
                        <td class="text-center"><?=$PVE->Resale?></td>
                    </tr>
                    <tr>
                        <td>Resale/Terminal Cap Rate</td>
                        <td class="text-center"><?=round($PVE->AppreciationRate)?>%</td>
                    </tr>
                    <tr>
                        <td>Sale Price/Terminal Value</td>
                        <td class="text-center">$<?=number_format($SalePrice, 2)?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-lg-6">
            <table class="table table-bordered">
                <tbody>
                    <tr>
                        <td>Sales Price	</td>
                        <td class="text-center">$<?=number_format($SalePrice, 2)?></td>
                    </tr>
                    <tr>
                        <td>1st Mortgage Payoff	</td>
                        <td class="text-center">$<?=number_format($FirstMortgagePayoff, 2)?></td>
                    </tr>
                    <tr>
                        <td>2nd Mortgage Payoff</td>
                        <td class="text-center">$<?=number_format($SecondMortgagePayoff, 2)?></td>
                    </tr>
                    <tr>
                        <td>Cost of Sale</td>
                        <td class="text-center">$<?=number_format($CostOfSale, 2)?></td>
                    </tr>
                    <tr>
                        <td>Pre-Tax Cash</td>
                        <td class="text-center">$<?=number_format($D89[$PVE->YearSale], 2)?></td>
                    </tr>
                    <tr>
                        <td>Capital Gains</td>
                        <td class="text-center">$<?=number_format($CapitalGains, 2)?></td>
                    </tr>
                    <tr>
                        <td>After-Tax Cash</td>
                        <td class="text-center">$<?=number_format($AfterTaxCash, 2)?></td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="clearfix"></div>
        <div class="col-lg-12">
            <?php for ( $a = 0; $a < $PVE->YearSale; $a = $a + 10 ) {
                if ( ($a + 10) <= $PVE->YearSale && $a == 0 ) {
                    $loop = 10;
                } elseif ( ($a + 10) <= $PVE->YearSale && $a == 10 ) {
                    $loop = 20;
                } elseif ( ($a + 10) <= $PVE->YearSale && $a == 20 ) {
                    $loop = 30;
                } else {
                    $loop = $PVE->YearSale;
                } ?>
                <?php if ( $loop <= 10 ) {
                    $span = 10;
                } elseif ( $loop <= 20 ) {
                    $span = 20;
                } elseif ( $loop <= 30 ) {
                    $span = 30;
                } else {
                    $span = 40;
                } ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="17.5%">Year (End)<br><br></th>
                            <?php 
                            $chq_date = str_replace("-", "/", $PVE->PurchaseDate);
                            $closing_date  = date("Y-m-d", strtotime($chq_date)); ?>
                            <?php if ( $a == 0 ) { ?>
                                <th width="7.5%" class="text-center">0<br><?=date('d-m-Y', strtotime("$closing_date + 0 years"))?></th>
                            <?php } else { ?>
                                <th width="7.5%" class="text-center">&nbsp;<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</th>
                            <?php } ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $PVE->YearSale ) { ?>
	                                <th width="7.5%" class="text-center"><?=$i?><br><?=date('d-m-Y', strtotime("$closing_date + $i years"))?></th>
                                <?php } else { ?>
                                	<th width="7.5%" class="text-center">&nbsp;</th>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td colspan="12"><strong>Calculation of Market Value</strong></td>
                        </tr>
                        <tr>
                            <td>Capital Improvements</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $PVE->YearSale ) {
	                                $CapitalImprovementSchedule = 'CapitalImprovementSchedule_'.$i; ?>
    	                            <td class="text-center"><?php echo number_format(($i <= 30 ? $PVE->$CapitalImprovementSchedule : 0), 0); ?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>FMV Multiplier</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $PVE->YearSale ) { ?>
									<?php
                                    if ( $i <= 30 ) {
                                        $CapitalImprovementSchedule = 'CapitalImprovementSchedule_'.$i; 
                                    }
                                    if ( isset($PVE->$CapitalImprovementSchedule) ) {
                                        $FairMarketValue = 'FairMarketValue_' . $i;
                                        $FairMarketValueresult = number_format($PVE->$FairMarketValue, 0);
                                    } else {
                                        $FairMarketValueresult = "";
                                    } ?>
                                    <td class="text-center"><?=$FairMarketValueresult?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Net Operating Income (NOI)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D49[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D49[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Market Value</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=number_format($D85[0], 0)?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D85[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D85[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td colspan="12"><strong>Calculation of Proceeds of Sale</strong></td>
                        </tr>
                        <tr>
                            <td>1st Mortgage Balance</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D86[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D86[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>2nd Mortgage Balance</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D87[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D87[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Total Debt</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=number_format($PVE->FirstMortgageAmount+$PVE->SecondMortgageAmount, 0)?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                           <?php for ( $i=$a+1; $i<=$span; $i++ ) {
							   if ( isset($D86[$i]) && isset($D87[$i]) ) { ?>
	                               <td class="text-center"><?=number_format($D86[$i]+$D87[$i], 0)?></td>
                               <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                           <?php } ?>
                        </tr>
                        <tr>
                            <td>Cost of Sale</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D88[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D88[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Cash at Sale</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D89[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D89[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td colspan="12"><strong>Calculation of Capital Gains Taxes</strong></td>
                        </tr>
                        <tr>
                            <td>Adjusted Basis</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D91[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D91[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Accumulated Passive Losses</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D94[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D94[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Gain on Sale (after write-offs)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D95[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D95[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Capital Gains Taxes</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D96[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D96[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>After-tax Cash Proceeds of Sale</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D89[$i]) && isset($D96[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D89[$i]-$D96[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td colspan="12"><strong>Breakdown of Total Equity:</strong></td>
                        </tr>
                        <tr>
                            <td>  Original DownPayment</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=number_format($D139[0], 0)?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D139[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D139[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>  Reserves</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=number_format($D140[0], 0)?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D140[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D140[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>  Principal Paydown on Debt</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D142[$i]) ) { ?>
		                            <td class="text-center"><?=number_format($D142[$i],0)?></td> 	
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>  Appreciation</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D141[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D141[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>  Total Equity with Reserves</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=number_format($D138[0], 0)?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D138[$i]) ) { ?>
	                                <td class="text-center"><?=number_format($D138[$i], 0)?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
	</div> <!-- COMPLETE/TESTED -->

<?php } elseif ( isset($print_data['FinancialRatiosReport']) ) {
	$FRR = $print_data['FinancialRatiosReport']; ?>
	
	<div class="FinancialRatiosReport">
		<div class="col-lg-6 text-center">
			<p><strong><?=$FRR->PropertyName?></strong></p>
			<p><?=$FRR->PropertyStreetAddress?></p>
			<p><?=$FRR->PropertyCityTown?>, <?=$FRR->PropertyStateProvince?>, <?=$FRR->PropertyZipCode?>, <?=$FRR->PropertyCountry?></p>
		</div>
		<div class="col-lg-6 text-center">
			<p><strong><?=$FRR->YourName?></strong></p>
			<p><?=$FRR->CompanyName?></p>
			<p><?=$FRR->PhoneNumber?></p>
		</div>
		<div class="clearfix"></div>
        <div class="col-lg-12">
            <?php for ( $a = 0; $a < $FRR->YearSale; $a = $a + 10 ) {
                if ( ($a + 10) <= $FRR->YearSale && $a == 0 ) {
                    $loop = 10;
                } elseif ( ($a + 10) <= $FRR->YearSale && $a == 10 ) {
                    $loop = 20;
                } elseif ( ($a + 10) <= $FRR->YearSale && $a == 20 ) {
                    $loop = 30;
                } else {
                    $loop = $FRR->YearSale;
                } ?>
                <?php if ( $loop <= 10 ) {
                    $span = 10;
                } elseif ( $loop <= 20 ) {
                    $span = 20;
                } elseif ( $loop <= 30 ) {
                    $span = 30;
                } else {
                    $span = 40;
                } ?>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th width="17.5%">Year (End)<br><br></th>
                            <?php 
                            $chq_date = str_replace("-", "/", $FRR->PurchaseDate);
                            $closing_date  = date("Y-m-d", strtotime($chq_date)); ?>
                            <?php if ( $a == 0 ) { ?>
                                <th width="7.5%" class="text-center">0<br><?=date('d-m-Y', strtotime("$closing_date + 0 years"))?></th>
                            <?php } else { ?>
                                <th width="7.5%" class="text-center">&nbsp;<br>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</th>
                            <?php } ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( $i <= $FRR->YearSale ) { ?>
	                                <th width="7.5%" class="text-center"><?=$i?><br><?=date('d-m-Y', strtotime("$closing_date + $i years"))?></th>
                                <?php } else { ?>
                                	<th width="7.5%" class="text-center">&nbsp;</th>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Pre-Tax Cash on Cash Return</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D116[$i]) ) { ?>
	                                <td class="text-center"><?=round($D116[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>After-Tax Cash on Cash Return</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D117[$i]) ) { ?>
	                                <td class="text-center"><?=round($D117[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Cumulative Cash-on-Cash Return (ROI) Pre-Tax</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D118[$i]) ) { ?>
	                                <td class="text-center"><?=round($D118[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Cumulative Cash-on-Cash Return (ROI) After-Tax</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D119[$i]) ) { ?>
	                                <td class="text-center"><?=round($D119[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>LTV (Loan to Value)</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?=round($D123[0])?>%</td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D123[$i]) ) { ?>
	                                <td class="text-center"><?=round($D123[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>DCR (Debt Coverage Ratio)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D124[$i]) ) { ?>
	                                <td class="text-center"><?=($D124[$i]!='N/A' ? number_format($D124[$i], 2) : 'N/A')?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Debt Yield</td>
                            <?php if ( $a == 0 ) { ?>
                                <td class="text-center"><?php echo ($DebtYield != 'N/A' ? $DebtYield ."%" : 'N/A');?></td>
                            <?php } else { ?>
                                <td class="text-center">&nbsp;</td>
                            <?php } ?>                                
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D86[$i]) && isset($D87[$i]) && isset($D49[$i]) ) { ?>
	                                <td class="text-center"><?=($D86[$i]+$D87[$i]>1 ? round($D49[$i]/($D86[$i]+$D87[$i])*100)."%" : 'N/A')?></td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Break-Even Ratio</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D145[$i]) ) { ?>
	                                <td class="text-center"><?=round($D145[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Return on Equity (CFAT/Total Equity) (Pre-Tax)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D125[$i]) ) { ?>
	                                <td class="text-center"><?=round($D125[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Return on Equity (CFAT/Total Equity) (After-Tax)</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D126[$i]) ) { ?>
	                                <td class="text-center"><?=round($D126[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                        <tr>
                            <td>Cap Rate based on original cost and current NOI</td>
                            <td class="text-center">&nbsp;</td>
                            <?php for ( $i=$a+1; $i<=$span; $i++ ) {
								if ( isset($D131[$i]) ) { ?>
	                                <td class="text-center"><?=round($D131[$i])?>%</td>
                                <?php } else { ?>
                                	<td class="text-center">&nbsp;</td>
                                <?php } ?>
                            <?php } ?>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
        </div>
        <div class="clearfix"></div>
	</div> <!-- COMPLETE/TESTED -->

<?php } ?> 