<style type="text/css">
.fields-panel h3{font-size:18px;text-align:center;}
.fields-panel h4{text-align:center;}
.assump-box{background-color:transparent;padding:10px 15px;margin-bottom:10px;}
.assump-box .assump-box-child{background:none;z-index:1;position:relative;}
.assump-box .panel-field{margin-bottom:10px;}
.modal-body .assump-box .panel-field{margin-bottom:5px;float:left;width:100%;}
.assump-box .panel-field input, .assump-box .panel-field select{margin-bottom:0;padding:5px;font-size:16px;border:1px solid #34a15e;background:#ceead9;box-shadow:none;color:#000;}
.assump-box .panel-field input[readonly].DateSoldField{background:#ceead9;}
.assump-box .panel-field textarea{margin-bottom:0;padding:5px;font-size:16px;border:1px solid #34a15e;background:#ceead9;box-shadow:none;color:#000;}
.assump-box .panel-field input[readonly]{color:#000;}
.assump-box .panel-field button{margin-bottom:0;font-weight:normal;font-size:14px;white-space:normal;}
.assump-box .panel-field button[disabled] {background:#007C30;}
.panel-field .form-inline .panel-field .input-group{width:100%;}
.panel-field .form-inline .panel-field .form-control{width:100%;box-shadow:none;}
.panel-field .form-inline .panel-field .input-group .input-group-addon{width:20px;border-radius:0;}
.assump-box .panel-field label{margin-bottom:0;font-weight:normal;font-size:18px;color:#222;}
.assump-box .panel-field label.text-muted{color:#555;}
hr{margin-bottom:10px;border-top:1px solid #ccc;}
#FlipRehubModal .modal-content, #RefiRehubModal .modal-content{width:1050px;}
#FlipRehubModal .modal-content .assump-box, #RefiRehubModal .modal-content .assump-box{padding:0 15px;margin:0;}
#FlipRehubModal .input-group .input-group-addon, 
#RefiRehubModal .input-group .input-group-addon{border-radius:0;padding:2px 5px;font-size:14px;}
#FlipRehubModal table, #RefiRehubModal table {font-size:14px;}
#OperatingIncomeModal table, #OperatingExpensesModal table{font-size:14px;background:#fff;}
#FlipRehubModal .table > thead > tr > th, #RefiRehubModal .table > thead > tr > th, #OperatingIncomeModal .table > thead > tr > th, #OperatingExpensesModal .table > thead > tr > th, #FlipRehubModal .table > tbody > tr > th, #RefiRehubModal .table > tbody > tr > th, #OperatingIncomeModal .table > tbody > tr > th, #OperatingExpensesModal .table > tbody > tr > th, #FlipRehubModal .table > tfoot > tr > th, #RefiRehubModal .table > tfoot > tr > th, #OperatingIncomeModal .table > tfoot > tr > th, #OperatingExpensesModal .table > tfoot > tr > th, #FlipRehubModal .table > thead > tr > td, #RefiRehubModal .table > thead > tr > td, #OperatingIncomeModal .table > thead > tr > td, #OperatingExpensesModal .table > thead > tr > td, #FlipRehubModal .table > tbody > tr > td, #RefiRehubModal .table > tbody > tr > td, #OperatingIncomeModal .table > tbody > tr > td, #OperatingExpensesModal .table > tbody > tr > td, #FlipRehubModal .table > tfoot > tr > td, #RefiRehubModal .table > tfoot > tr > td, #OperatingIncomeModal .table > tfoot > tr > td, #OperatingExpensesModal .table > tfoot > tr > td {vertical-align:middle;padding:0;line-height:1;border:1px solid #08652C;}
#FlipRehubModal .table > thead > tr > th:nth-child(1), #RefiRehubModal .table > thead > tr > th:nth-child(1), #FlipRehubModal .table > tbody > tr > th:nth-child(1), #RefiRehubModal .table > tbody > tr > th:nth-child(1), #FlipRehubModal .table > tfoot > tr > th:nth-child(1), #RefiRehubModal .table > tfoot > tr > th:nth-child(1), #FlipRehubModal .table > thead > tr > td:nth-child(1), #RefiRehubModal .table > thead > tr > td:nth-child(1), #FlipRehubModal .table > tbody > tr > td:nth-child(1), #RefiRehubModal .table > tbody > tr > td:nth-child(1), #FlipRehubModal .table > tfoot > tr > td:nth-child(1), #RefiRehubModal .table > tfoot > tr > td:nth-child(1) {width:150px;padding:0 5px;line-height: 1.25;}
#FlipRehubModal .table > thead > tr > th:nth-child(11), #RefiRehubModal .table > thead > tr > th:nth-child(11), #FlipRehubModal .table > tbody > tr > th:nth-child(11), #RefiRehubModal .table > tbody > tr > th:nth-child(11), #FlipRehubModal .table > tfoot > tr > th:nth-child(11), #RefiRehubModal .table > tfoot > tr > th:nth-child(11), #FlipRehubModal .table > thead > tr > td:nth-child(11), #RefiRehubModal .table > thead > tr > td:nth-child(11), #FlipRehubModal .table > tbody > tr > td:nth-child(11), #RefiRehubModal .table > tbody > tr > td:nth-child(11), #FlipRehubModal .table > tfoot > tr > td:nth-child(11), #RefiRehubModal .table > tfoot > tr > td:nth-child(11) {color:#f00;font-size:10px;}
#OperatingIncomeModal .table > tbody > tr > td, #OperatingExpensesModal .table > tbody > tr > td{text-align:center;}
#OperatingIncomeModal .table > tfoot > tr > td, #OperatingExpensesModal .table > tfoot > tr > td{padding:0;text-align:left;}
#FlipRehubModal .table > thead > tr > th, #OperatingIncomeModal .table > thead > tr > th, #OperatingExpensesModal .table > thead > tr > th, #RefiRehubModal .table > thead > tr > th, #OperatingIncomeModal .table > thead > tr > th, #OperatingExpensesModal .table > thead > tr > th {text-align:center;padding:5px 0;}
#FlipRehubModal .table > tbody > tr > td[colspan="11"], #RefiRehubModal .table > tbody > tr > td[colspan="11"]{padding:5px 0;}
#FlipRehubModal table input.form-control, #FlipRehubModal table select.form-control, #RefiRehubModal table input.form-control, #RefiRehubModal table select.form-control {padding:3px;text-align:left;font-size:14px;min-width:48px;margin:0;border:none;box-shadow:none;background:#ceead9;color:#000;height:35px;}
#FlipRehubModal table input.form-control[readonly], #FlipRehubModal table select.form-control[readonly], #RefiRehubModal table input.form-control[readonly], #RefiRehubModal table select.form-control[readonly] {background:#fff;color:#000;}
#OperatingIncomeModal table input.form-control, #OperatingExpensesModal table input.form-control,#OperatingIncomeModal table select.form-control, #OperatingExpensesModal table select.form-control {padding:3px 5px;text-align:left;font-size:14px;min-width:48px;margin:0;border:none;box-shadow:none;background:#ceead9;color:#000;height:26px;}
#OperatingIncomeModal table input.form-control[readonly], #OperatingExpensesModal table input.form-control[readonly],#OperatingIncomeModal table select.form-control[readonly], #OperatingExpensesModal table select.form-control[readonly] {background:#fff;color:#000;}
#FlipRehubModal table.right-table, #RefiRehubModal table.right-table{background:#fff;text-align:center;}
#FlipRehubModal .table.right-table > thead > tr > th, #RefiRehubModal .table.right-table > thead > tr > th, #FlipRehubModal .table.right-table > tbody > tr > th, #RefiRehubModal .table.right-table > tbody > tr > th, #FlipRehubModal .table.right-table > tfoot > tr > th, #RefiRehubModal .table.right-table > tfoot > tr > th, #FlipRehubModal .table.right-table > thead > tr > td, #RefiRehubModal .table.right-table > thead > tr > td, #FlipRehubModal .table.right-table > tbody > tr > td, #RefiRehubModal .table.right-table > tbody > tr > td, #FlipRehubModal .table.right-table > tfoot > tr > td, #RefiRehubModal .table.right-table > tfoot > tr > td {vertical-align:middle;padding:5px 0;text-align:center;}
.NoOfUnits_input { text-align:center !important; }
.input-group-addon {background-color:#007C30;border:1px solid #007C30;border-radius:0;padding:5px;color:#fff;}
.input-group .form-control {border:1px solid #007C30;}
.FlipBudgetColumn, .RefiBudgetColumn {font-weight:bold;}

.panel-field .form-control[readonly="readonly"] {background-color:#fff;}

#FlipRehubModal .form-control, #RefiRehubModal .form-control, #OperatingIncomeModal .form-control, #OperatingExpensesModal .form-control {background:#ceead9;border:1px solid #007c30;}
#FlipRehubModal table tr:nth-child(odd) .form-control, #RefiRehubModal table tr:nth-child(odd) .form-control, #OperatingIncomeModal table tr:nth-child(odd) .form-control, #OperatingExpensesModal table tr:nth-child(odd) .form-control {background:#c6eefd;border:none;}
#FlipRehubModal table tr:nth-child(even) .form-control, #RefiRehubModal table tr:nth-child(even) .form-control, #OperatingIncomeModal table tr:nth-child(even) .form-control, #OperatingExpensesModal table tr:nth-child(even) .form-control {background:#ceead9;border:none;}
#FlipRehubModal table tr:nth-child(odd) .input-group-addon, #RefiRehubModal table tr:nth-child(odd) .input-group-addon, #OperatingIncomeModal table tr:nth-child(odd) .input-group-addon, #OperatingExpensesModal table tr:nth-child(odd) .input-group-addon {background:#2a6c84;border:1px solid #2a6c84;}
#FlipRehubModal table tr:nth-child(even) .input-group-addon, #RefiRehubModal table tr:nth-child(even) .input-group-addon, #OperatingIncomeModal table tr:nth-child(even) .input-group-addon, #OperatingExpensesModal table tr:nth-child(even) .input-group-addon {background:#007c30;border:1px solid #007c30;}
</style>
<section id="wrapper">
	<div class="container-fluid">
    	<div class="row">
            <?php $this->load->view('template/left-panel'); ?>
            <aside class="col-lg-10 col-md-10 col-sm-12 col-xs-12 right-panel">
            	<div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 page-title row">
                	<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
		                <h2><span id="PropertyNameTitle"><?=($fields->PropertyName ? '' . $fields->PropertyName : '')?></span></h2>
                    </div>
                   	<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
						<div class="nano quick-tags-nano">
	                    	<div class="quick-tags nano-content"></div>
                        </div>
                    </div>
                </div>
                
                <div class="page-content">
                	<?php if ( !$proposal ) { ?>
                        <div class="alert alert-warning" role="alert">
                        	<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                            <i class="fa fa-info-circle"></i> Now you have to create your proposals. For creating your first proposal click the above '<a href="<?=base_url('pages/proposal-writer')?>/"><u>Create Proposal</u></a>' botton.&nbsp;&nbsp;&nbsp; 
                            <a class="action-btn" data-toggle="tooltip" data-placement="top" title="Proposal Tutorial">
                                <span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                            </a>
                        </div>
                    <?php } else { ?>
                        <!-- No action needed -->
                    <?php } ?>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="box box-green">
                                <div class="box-header">
                                    <h3 class="box-title"><i class="fa fa-calculator"></i> Proposal Writer</h3>
                                    <h3 class="box-title col-xs-6 col-xs-offset-3">
                                        <div class="progress">
                                            <div class="progress-bar" data-transitiongoal="0"></div>
                                        </div>
                                    </h3>
                                    <h3 class="box-title pull-right need-help">
                                        <a class="action-btn" data-toggle="tooltip" data-placement="top" title="FAQ">
                                            <span data-target="#proposal_help_video" data-toggle="modal">Need help? <i class="fa fa-video-camera"></i></span>
                                        </a>
                                    </h3>
                                    <?php if ( $fields->ProposalStatus == 'draft' ) { ?>
                                        <h3 class="box-title pull-right" style="padding:5px 10px;">
                                            <img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide draft-loading" />
                                            <a class="btn btn-primary btn-sm" id="SaveAsDraft">Save As Draft</a>
                                        </h3>
                                    <?php } ?>
                                </div>
                                <div class="box-body">
                    				<div class="offer-calc-page">
										<?php if ( $success ) { ?>
                                            <div class="alert alert-success" role="alert">
                                                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <i class="fa fa-info-circle"></i> <?=$success?>
                                            </div>
                                        <?php } ?>
                                        
                                        <input type="hidden" id="page_type" value="PROPOSAL" />
                                        <input type="hidden" id="PageStatus" value="UPDATE" />
                                        <div class="fields-panel flex-box offer-calculator-screen RehabForm">
                                        	<div class="col-xs-2">
                                            	<div class="row quick-address"></div>
                                            </div>
                                            <div class="col-xs-8 flex-box-child">
                                                <div class="col-xs-12 assump-box" id="RehubValuator">
                                                    
                                                    <!-- PROPERTY INFORMATION -->
                                                    <div class="full-section-box"><!-- STEP (1) -->
                                                        <div class="panel-field">
                                                            <h3>PROPERTY INFORMATION</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child">
                                                        
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                                                                
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step1Button" data-rel="12.50">NEXT</button>
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                                <div class="panel-field" id="PropertyNameRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property Name</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyName",
                                                                                'id'    => "PropertyName",
                                                                                'value' => $fields->PropertyName,
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Name -->
                                                                <div class="panel-field" id="PropertyStreetAddressRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property Street Address</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyStreetAddress",
                                                                                'id'    => "PropertyStreetAddress",
                                                                                'value' => $fields->PropertyStreetAddress,
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Street Address -->
                                                                <div class="panel-field" id="PropertyCityTownRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property City/Town</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyCityTown",
                                                                                'id'    => "PropertyCityTown",
                                                                                'value' => $fields->PropertyCityTown,
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property City -->
                                                                <div class="panel-field" id="PropertyStateProvinceRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property State/Province</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyStateProvince",
                                                                                'id'    => "PropertyStateProvince",
                                                                                'value' => $fields->PropertyStateProvince,
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property State -->
                                                                <div class="panel-field" id="PropertyZipCodeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property ZIP Code</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyZipCode",
                                                                                'id'    => "PropertyZipCode",
                                                                                'value' => $fields->PropertyZipCode,
                                                                                'class'	=> 'form-control NumberOnly Required NoPreZeroWithoutDecimal',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Zip -->
                                                                <div class="panel-field" id="PropertyCountryRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property Country</label>
                                                                        <div class="col-xs-6">
                                                                            <?=country_select('PropertyCountry', 'PropertyCountry', 'form-control Required', $fields->PropertyCountry, array('US', 'CA'), '')?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Country -->
                                                                <div class="panel-field" id="PropertyDescriptionRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property Description</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyDescription",
                                                                                'id'    => "PropertyDescription",
                                                                                'value' => $fields->PropertyDescription,
                                                                                'class'	=> 'form-control Required',
                                                                                'rows'	=> 3
                                                                            ); ?>
                                                                            <?=form_textarea($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property Description -->
                                                                <div class="panel-field" id="WorkNeededRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Work Needed</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "WorkNeeded",
                                                                                'id'    => "WorkNeeded",
                                                                                'value' => $fields->WorkNeeded,
                                                                                'class'	=> 'form-control Required',
                                                                                'rows'  => 3
                                                                            ); ?>
                                                                            <?=form_textarea($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Work Needed -->
                                                                <div class="panel-field" id="PropertyFeaturesRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Property Features</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PropertyFeatures",
                                                                                'id'    => "PropertyFeatures",
                                                                                'value' => $fields->PropertyFeatures,
                                                                                'class'	=> 'form-control Required',
                                                                                'rows'	=> 3
                                                                            ); ?>
                                                                            <?=form_textarea($field)?>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Property Features -->
                                                                <div class="panel-field" id="BedroomsRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Bedrooms</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Bedrooms",
                                                                                'id'    => "Bedrooms",
                                                                                'value' => $fields->Bedrooms,
                                                                                'class'	=> 'form-control NumberOnly Required NoPreZeroWithoutDecimal',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Bedrooms -->
                                                                <div class="panel-field" id="BathroomsRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Bathrooms</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Bathrooms",
                                                                                'id'    => "Bathrooms",
                                                                                'value' => $fields->Bathrooms,
                                                                                'class'	=> 'form-control NumberOnly Required NoPreZeroWithoutDecimal',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Bathrooms -->
                                                                <div class="panel-field" id="SquareFeetRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Square Feet</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SquareFeet",
                                                                                'id'    => "SquareFeet",
                                                                                'value' => $fields->SquareFeet,
                                                                                'class'	=> 'form-control NumberOnly Required NoPreZeroWithoutDecimal',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Square Feet -->
                                                                <div class="panel-field" id="YearBuiltRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Year Built</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "YearBuilt",
                                                                                'id'    => "YearBuilt",
                                                                                'value' => $fields->YearBuilt,
                                                                                'class'	=> 'form-control NumberOnly YearRange Required NoPreZeroWithoutDecimal',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Year Built -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step1Button" data-rel="12.50">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" id="ProposalStatus" name="ProposalStatus" value="<?=$fields->ProposalStatus?>" />
                                                                    <input type="hidden" name="TableName" value="property_information" />
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP (1) -->
                                                    
                                                    <!-- PERSONAL INFORMATION -->
                                                    <div class="full-section-box"><!-- STEP (2) -->
                                                        <div class="panel-field">
                                                            <h3>1)PERSONAL INFORMATION</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child">
                                                        
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                                                                
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="0">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step2Button" data-rel="25">NEXT</button>
                                                                </div><!-- Next/Prev Buttons -->
                                                            
                                                                <div class="panel-field" id="YourNameRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Your Name</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "YourName",
                                                                                'id'    => "YourName",
                                                                                'value' => $fields->YourName,
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Your Name -->
                                                                <div class="panel-field" id="CompanyNameRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Company Name</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CompanyName",
                                                                                'id'    => "CompanyName",
                                                                                'value' => $fields->CompanyName,
                                                                                'class'	=> 'form-control',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Company Name -->
                                                                <div class="panel-field" id="CompanyStreetRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Street Address</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CompanyStreet",
                                                                                'id'    => "CompanyStreet",
                                                                                'value' => ($fields->CompanyStreet ? $fields->CompanyStreet : $user->street),
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Comapny Street Address -->
                                                                <div class="panel-field" id="CompanyCityRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Town/City</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CompanyCity",
                                                                                'id'    => "CompanyCity",
                                                                                'value' => ($fields->CompanyCity ? $fields->CompanyCity : $user->town),
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Company City -->
                                                                <div class="panel-field" id="CompanyStateRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">State/Province</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CompanyState",
                                                                                'id'    => "CompanyState",
                                                                                'value' => ($fields->CompanyState ? $fields->CompanyState : $user->state),
                                                                                'class'	=> 'form-control Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Company State -->
                                                                <div class="panel-field" id="CompanyZipRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">ZIP Code</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CompanyZip",
                                                                                'id'    => "CompanyZip",
                                                                                'value' => ($fields->CompanyZip ? $fields->CompanyZip : $user->zip),
                                                                                'class'	=> 'form-control NumberOnly Required NoPreZeroWithoutDecimal',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Company Zip -->
                                                                <div class="panel-field" id="CompanyCountryRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Country Country</label>
                                                                        <div class="col-xs-6">
                                                                            <?=country_select('CompanyCountry', 'CompanyCountry', 'form-control Required', ($fields->CompanyCountry ? $fields->CompanyCountry : $user->country), array('US', 'CA'), '')?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Company Country -->
                                                                <div class="panel-field" id="PhoneNumberRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Phone Number</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PhoneNumber",
                                                                                'id'    => "PhoneNumber",
                                                                                'value' => $fields->PhoneNumber,
                                                                                'class'	=> 'form-control NumberOnly Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Phone Number -->
                                                                <div class="panel-field" id="EmailRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Email</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Email",
                                                                                'id'    => "Email",
                                                                                'value' => $fields->Email,
                                                                                'class'	=> 'form-control EmailOnly Required',
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Email -->
                                                                <div class="panel-field" id="FacebookURLRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Facebook URL</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FacebookURL",
                                                                                'id'    => "FacebookURL",
                                                                                'value' => (isset($fields->FacebookURL) ? $fields->FacebookURL : $user->facebook_url),
                                                                                'class'	=> 'form-control WebsiteOnly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                            	<div class="input-group-addon">www.</div>
		                                                                        <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- FacebookURL -->
                                                                <div class="panel-field" id="LinkedinURLRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Linkedin URL</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "LinkedinURL",
                                                                                'id'    => "LinkedinURL",
                                                                                'value' => (isset($fields->LinkedinURL) ? $fields->LinkedinURL : $user->linkedin_url),
                                                                                'class'	=> 'form-control WebsiteOnly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                            	<div class="input-group-addon">www.</div>
		                                                                        <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- LinkedinURL -->
                                                                <div class="panel-field" id="TwitterURLRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Twitter URL</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TwitterURL",
                                                                                'id'    => "TwitterURL",
                                                                                'value' => (isset($fields->TwitterURL) ? $fields->TwitterURL : $user->twitter_url),
                                                                                'class'	=> 'form-control WebsiteOnly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                            	<div class="input-group-addon">www.</div>
		                                                                        <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- TwitterURL -->  
                                                                <div class="panel-field" id="WebsiteRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-6">Website</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Website",
                                                                                'id'    => "Website",
                                                                                'value' => $fields->Website,
                                                                                'class'	=> 'form-control WebsiteOnly',
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                            	<div class="input-group-addon">www.</div>
		                                                                        <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Website -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="0">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step2Button" data-rel="25">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="personal_information" />
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP (2) -->
                                                    
                                                    <!-- ASSUMPTIONS PANEL STEP (1) -->
                                                    <div class="full-section-box"><!-- STEP (3) -->
                                                        <div class="panel-field">
                                                            <h3>2) PURCHASE ASSUMPTIONS PANEL</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child">
                                                        
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                                                            
                                                               
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="12.50">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step3Button" data-rel="37.50">NEXT</button>
                                                                </div><!-- Next/Prev Buttons -->
                                                            
                                                                <div class="panel-field" id="PurchasePriceRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Purchase Price</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PurchasePrice",
                                                                                'id'    => "PurchasePrice",
                                                                                'value' => number_format($fields->PurchasePrice, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"This is the contract purchase price at which you will acquire the property",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Purchase Price -->
                                                                <div class="panel-field" id="ClosingCostsRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-8">
                                                                            <button type="button" class="btn btn-default" data-target="#ClosingCostsModal" data-toggle="modal">
                                                                                Click here to Enter Closing Costs
                                                                            </button>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ClosingCosts",
                                                                                'id'    => "ClosingCosts",
                                                                                'value' => number_format($fields->ClosingCosts, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field Required',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Click on the button to be directed to a screen where you can enter the total closing costs for the purchase. You have the option to enter the total closing costs in one lump sum or simple add the costs for each part of the closing costs. Note:  All points/fees associated with a  loan are entered separately",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div id="closing-tags"></div>
                                                                    </div>
                                                                </div><!-- Click here to Enter Closing Costs -->
                                                                <div class="panel-field" id="HoldingCostsRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-8">
                                                                            <button type="button" class="btn btn-default" data-target="#HoldingCostsModal" data-toggle="modal">
                                                                                Click here to Enter Holding Costs
                                                                            </button>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "HoldingCosts",
                                                                                'id'    => "HoldingCosts",
                                                                                'value' => ($fields->HoldingCostsOption=='Detailed Input' ? number_format($fields->HoldingCosts, 2, '.', '').'/month' : number_format($fields->HoldingCosts, 2, '.', '')),
                                                                                'class'	=> 'form-control number-field Required',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Click on the button to be directed to a screen where you can enter the total projected holding costs for the duration of rehab. Again her you can enter a detailed line up tally of cost or simple use a lump sum amount.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                        <div id="holding-tags"></div>
                                                                    </div>
                                                                </div><!-- Click here to Enter Holding Costs -->
                                                                <div class="panel-field" id="FinanceClosingHoldingRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Include Closings/Holding Costs in the loan?</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes'  => 'Yes',
                                                                                'No'   => 'No',
                                                                            );
                                                                            $attrb = 'id="FinanceClosingHolding" class="form-control" data-popover="true" data-content="Select weather the lender will fund your purchase closing costs and holdings costs for the project"'; ?>
                                                                            <?=form_dropdown('FinanceClosingHolding', $options, $fields->FinanceClosingHolding, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Include Closings/Holding Costs in the loan? -->
        
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="12.50">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step3Button trigger-bar" data-rel="37.50">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="purchase_assumption" />
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP (3) -->
                                                    
                                                    <!-- ASSUMPTIONS PANEL STEP (2) -->
                                                    <div class="full-section-box"><!-- STEP (4) -->
                                                        <div class="panel-field">
                                                            <h3>3) FINANCING ASSUMPTIONS PANEL</h3><hr />
                                                        </div>                                                
                                                        <div class="assump-box-child">
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                                                                
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="25">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step4Button trigger-bar" data-rel="50">NEXT</button>
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                                <div class="panel-field" id="FinancingUsedRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Financing Used or All-Cash?</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Financing'  => 'Financing',
                                                                                'All Cash'   => 'All Cash',
                                                                            );
                                                                            $attrb = 'id="FinancingUsed" class="form-control" data-popover="true" data-content="Are you using any kind of financing for this purchase and renovation or all-cash?"'; ?>
                                                                            <?=form_dropdown('FinancingUsed', $options, $fields->FinancingUsed, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Financing Used or All-Cash? -->
                                                                <div class="panel-field" id="CapSelectionRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Lender caps ARV or Cost of Project?</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                'ARV'  	=> 'ARV',
                                                                                'Cost'	=> 'Cost',
                                                                            );
                                                                            $attrb = 'id="CapSelection" class="form-control" data-popover="true" data-content="Does your lender cap how much they will lend based on the total Cost of the project or the % of After-Repair Value?  If doing all-cash, ignore this."'; ?>
                                                                            <?=form_dropdown('CapSelection', $options, $fields->CapSelection, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Lender caps ARV or Cost of Project? -->
                                                                <div class="panel-field" id="FinancingCapRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Max % of ARV to be financed</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FinancingCap",
                                                                                'id'    => "FinancingCap",
                                                                                'value' => round($fields->FinancingCap),
                                                                                'class'	=> 'form-control number-field NumberOnly' . ($fields->FinancingUsed=='Financing' ? 'Required' : '') . ' percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Max % of Cost or ARV that your lender will finance.  For example, certain lenders will lend a maximum of 70% of cost. The remaining funding must come from your cash or other sources",
                                                                                'maxlength' => 3,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Max % of ARV to be financed -->
                                                                <div class="panel-field" id="RehabDiscountPointsRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Origination/Discount Points</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "RehabDiscountPoints",
                                                                                'id'    => "RehabDiscountPoints",
                                                                                'value' => round($fields->RehabDiscountPoints),
                                                                                'class'	=> 'form-control number-field NumberOnly' . ($fields->FinancingUsed=='Financing' ? 'Required' : '') . ' percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"These are the closing costs associated with the initial acquisition/renovation loan:  Origination Points (as % of loan amount) and Discount Points",
                                                                                'maxlength' => 3,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Origination/Discount Points -->
                                                                <div class="panel-field" id="OtherPointsRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Other Closing Costs Paid to Lender</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OtherPoints",
                                                                                'id'    => "OtherPoints",
                                                                                'value' => round($fields->OtherPoints),
                                                                                'class'	=> 'form-control number-field NumberOnly' . ($fields->FinancingUsed=='Financing' ? 'Required' : '') . ' percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Enter any other closings costs associated with the loan here as a % of the Total Loan Amount.  This amount is an estimate using a percentage to account for appraisals, inspections , tank sweeps and other due diligence cost that the lender will incur and pass on to you to pay.",
                                                                                'maxlength' => 3,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Other Closing Costs Paid to Lender -->
                                                                <div class="panel-field" id="PointsClosingUpfrontRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Points and Closings Costs Upfront or Back-End?</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Paid Upfront'  => 'Paid Upfront',
                                                                                'Paid Backend'   => 'Paid Backend',
                                                                            );
                                                                            $attrb = 'id="PointsClosingUpfront" class="form-control" data-popover="true" data-content="Will you pay loan closing costs and points at the time of closing or is it rolled into the loan amount, to be paid back at loan payoff?  This is common with hard money lenders and can be done when there is room in the deal.  Which is another way of say they will add the point into the loan if it doesn\'t to cause the loan ratios to go over the lender lending parameters."'; ?>
                                                                            <?=form_dropdown('PointsClosingUpfront', $options, $fields->PointsClosingUpfront, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Points and Closings Costs Upfront or Back-End? -->
                                                                <div class="panel-field" id="RehabInterestRateRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Interest Rate</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "RehabInterestRate",
                                                                                'id'    => "RehabInterestRate",
                                                                                'value' => round($fields->RehabInterestRate),
                                                                                'class'	=> 'form-control number-field NumberOnly' . ($fields->FinancingUsed=='Financing' ? 'Required' : '') . ' percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Enter the interest rate on your rehab/renovation loan here",
                                                                                'maxlength' => 5,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Interest Rate -->
                                                                <div class="panel-field" id="InterestPaymentsMadeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Interest Payments During Rehab?</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes'  => 'Yes',
                                                                                'No'   => 'No',
                                                                            );
                                                                            $attrb = 'id="InterestPaymentsMade" class="form-control" data-popover="true" data-content="Will your lender require interest payments during the construction period or do the interest payments get rolled into your mortgage balance to be paid back at the end?  What is typical is that lenders will require at least interest only payments, but hey maybe the lender will hold and take it in the backend. Of the loan that is :)."'; ?>
                                                                            <?=form_dropdown('InterestPaymentsMade', $options, $fields->InterestPaymentsMade, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Interest Payments During Rehab? -->
                                                                <div class="panel-field" id="SplitWithLenderRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Split Back-End Profits with Lender?</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(
                                                                                'Yes'  => 'Yes',
                                                                                'No'   => 'No',
                                                                            );
                                                                            $attrb = 'id="SplitWithLender" class="form-control" data-popover="true" data-content="Select if you are offering your Private Lender a split of your flip profits or any profits you will make from a cash-out refi.  This assumes that you first repay the loan, then recoup any of your own expenses, after which you give a certain % of net profits left over to your Private Lender.   This commonly called JVing or Equity participation and a host of other jargon.   You may choose to offer a split if you need to "sweeten" the deal for the lender."'; ?>
                                                                            <?=form_dropdown('SplitWithLender', $options, $fields->SplitWithLender, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Split Back-End Profits with Lender? -->
                                                                <div class="panel-field" id="LenderSplitRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">What % of Pre-Tax Profits Does Lender Get?</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "LenderSplit",
                                                                                'id'    => "LenderSplit",
                                                                                'value' => round($fields->LenderSplit),
                                                                                'class'	=> 'form-control number-field NumberOnly' . ($fields->FinancingUsed=='Financing' ? 'Required' : '') . ' percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Indicate the % of net profits from a Rehab/Flip or from a cash-out refi you plan to offer to your Private Lender.",
                                                                                'maxlength' => 3,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- What % of Pre-Tax Profits Does Lender Get? -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="25">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step4Button" data-rel="50">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="financing_assumption" />
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP (4) -->
                                                    
                                                    <!-- FLIP ANALYSIS -->
                                                    <div class="full-section-box"><!-- STEP (5) -->
                                                        <div class="panel-field">
                                                            <h3>4) REHAB/FLIP ANALYSIS</h3><hr />
                                                        </div>                                                
                                                        <div class="assump-box-child">

                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                                                                
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="37.50">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step5Button" data-rel="62.50">NEXT</button>
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                                <div class="panel-field" id="MonthsFlipRehabRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Months to Rehab</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                                            $attrb = 'id="MonthsFlipRehab" class="form-control" data-popover="true" data-content="How many months will the actual renovation of the property take place?    Include renovation time only, not time to sell, we will ask for that later."'; ?>
                                                                            <?=form_dropdown('MonthsFlipRehab', $options, $fields->MonthsFlipRehab, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Projected Rehab Period (Months) -->
                                                                <div class="panel-field" id="MonthsToSellRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Months to Close After Rehab</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                                            $attrb = 'id="MonthsToSell" class="form-control" data-popover="true" data-content="How many months do you project for the property to sell after the renovations are completed?"'; ?>
                                                                            <?=form_dropdown('MonthsToSell', $options, $fields->MonthsToSell, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Months to Complete Sale (After Rehab) -->
                                                                <div class="panel-field" id="ActualFlipBudgetRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-8">
                                                                            <input type="hidden" name="FlipHoldingCosts" id="FlipHoldingCosts" />
                                                                            <button type="button" class="btn btn-default" data-target="#FlipRehubModal" data-toggle="modal">
                                                                                REHAB BUDGET (Click here to enter)
                                                                            </button>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ActualFlipBudget",
                                                                                'id'    => "ActualFlipBudget",
                                                                                'value' => number_format($fields->ActualFlipBudget, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field Required',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Click on this button to enter the projected rehab budget required to flip the property.  You have the option of entering \"lump sum budget\" or \"detailed\"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- REHAB BUDGET (Click here to enter) -->
                                                                <div class="panel-field" id="ARVFlipRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">After-Repair Value (ARV) for FLIP</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ARVFlip",
                                                                                'id'    => "ARVFlip",
                                                                                'value' => number_format($fields->ARVFlip, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"ARV or After-Repair-Value is the projected market value of the property after all the renovations are complete.  This is a critical number to know for your deal and remember most lenders will check this number with an appraisal.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- After-Repair Value (ARV) for Flip -->
                                                                <div class="panel-field" id="TotalCapitalNeededFlipRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8 text-muted">Total Capital Needed</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TotalCapitalNeededFlip",
                                                                                'id'    => "TotalCapitalNeededFlip",
                                                                                'value' => number_format($fields->TotalCapitalNeededFlip, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Relax this is calculated.Total capital required to do the project,not including costs associated with financing.  Includes:  purchase price, non-financing closing costs, holding costs and rehab budget. ",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Total Capital Needed -->
                                                                <div class="panel-field" id="MaxFinancedFlipRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8 text-muted">Max $ that can be financed</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "MaxFinancedFlip",
                                                                                'id'    => "MaxFinancedFlip",
                                                                                'value' => number_format($fields->MaxFinancedFlip, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"This number reflects the maximum that your lender will finance based on your inputs:  ARV or cost and the lender cap. ",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Max $ that can be financed -->
                                                                <div class="panel-field" id="ActualFinancedFlipRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8 text-muted">Actual to be financed (not inc. closing/holding)</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ActualFinancedFlip",
                                                                                'id'    => "ActualFinancedFlip",
                                                                                'value' => number_format($fields->ActualFinancedFlip, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"This will indicate the actual amount that you will receive from your lender.   Back-end points and interest may be added to this. ",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Actual to be financed (not inc. closing/holding) -->
                                                                <div class="panel-field" id="PointInterestRolledUpFlipRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8 text-muted">Closing/Holding Costs/Interest Added to Loan</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PointInterestRolledUpFlip",
                                                                                'id'    => "PointInterestRolledUpFlip",
                                                                                'value' => number_format($fields->PointInterestRolledUpFlip, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Total amount of points and interest added to the loan amount to be paid off at loan maturity/balloon.  Again relax we calculated this for you."
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Closing/Holding Costs/Interest Added to Loan -->
                                                                <div class="panel-field" id="TotalLoanFlipRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Total Loan Amount</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TotalLoanFlip",
                                                                                'id'    => "TotalLoanFlip",
                                                                                'value' => number_format($fields->TotalLoanFlip, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"This amount is the sum of the 2 amounts immediately above this.  This will reflect the total loan amount due to be paid off at maturity.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Total Loan Amount -->
                                                                <div class="panel-field" id="CashRequiredFlipRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Cash Required (over life of project)</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CashRequiredFlip",
                                                                                'id'    => "CashRequiredFlip",
                                                                                'value' => number_format($fields->CashRequiredFlip, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Amount of total cash you will need over the life of the project, in addition to financing.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Cash Required (over life of project) -->
                                                                
                                                                <div class="panel-field" id="TotalCostBasisFlipRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Total All-in Cost at end of Rehab</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TotalCostBasisFlip",
                                                                                'id'    => "TotalCostBasisFlip",
                                                                                'value' => number_format($fields->TotalCostBasisFlip, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"This is the total all-in cost basis of the project, including acquisition, closing costs, holding costs, rehab, origination and discount points and interest on the loan.  The whole enchilada.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Total All-in Cost at end of Rehab -->
                                                                <div class="panel-field" id="AVRPercentageRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">% of ARV</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "AVRPercentage",
                                                                                'id'    => "AVRPercentage",
                                                                                'value' => round($fields->AVRPercentage),
                                                                                'class'	=> 'form-control number-field percentageField',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>" This is a good sanity check for the viability of your project.  It is calculated based on the Total cost basis of the project divided by the ARV .",
                                                                                'maxlength' => 3,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- % of ARV -->
                                                                <div class="panel-field" id="ResalePriceRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Projected Resale Price</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ResalePrice",
                                                                                'id'    => "ResalePrice",
                                                                                'value' => number_format($fields->ResalePrice, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Enter your projected resale price here.  This may or may not be the same as ARV!  For example, this may be lower than ARV if you want to sell quickly. ",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Projected Resale Price -->
                                                                <div class="panel-field" id="CostOfSaleRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Projected Cost of Sale (% of Sale Price)</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CostOfSale",
                                                                                'id'    => "CostOfSale",
                                                                                'value' => round($fields->CostOfSale),
                                                                                'class'	=> 'form-control number-field NumberOnly Required percentageField',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Commission you expect to pay on the sale (buyer's agent + seller's agent).  This should also include any closing costs paid by you.  Include the percentage for real estate commission plus  a percentage estimate for other closing costs.  I like to add 1.5 %.",
                                                                                'maxlength' => 3,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Projected Cost of Sale (% of Sale Price) -->
                                                                <div class="panel-field" id="FlipProfitRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Projected Profit (after any lender split)</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FlipProfit",
                                                                                'id'    => "FlipProfit",
                                                                                'value' => number_format($fields->FlipProfit, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"This will be your total projected profit from the flip",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <strong><?=form_input($field)?></strong>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Flip Profit -->
                                                                <div class="panel-field" id="FlipROIRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>ROI (Return on CASH invested)</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FlipROI",
                                                                                'id'    => "FlipROI",
                                                                                'value' => ($fields->FlipROI=='Infinite' ? 'Infinite' : round($fields->FlipROI)),
                                                                                'class'	=> 'form-control number-field percentageField',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"Return on Investment.  It is calculated as a percentage of Profit for the cash invested. If you did not have to put up any of your own cash, your ROI will be infinite!",
                                                                                'maxlength' => 3,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <strong><?=form_input($field)?></strong>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Flip ROI -->
                                                                <div class="panel-field" id="FlipROIAnnualizedRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>ROI (Annualized)</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "FlipROIAnnualized",
                                                                                'id'    => "FlipROIAnnualized",
                                                                                'value' => ($fields->FlipROIAnnualized=='Infinite' ? 'Infinite' : round($fields->FlipROIAnnualized)),
                                                                                'class'	=> 'form-control number-field percentageField',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"ROI Annualized - this will depend on how long it takes you to  Renovate and Sell and get to the closing table.",
                                                                                'maxlength' => 3,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <strong><?=form_input($field)?></strong>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Flip ROI Annualized -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="37.50">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step5Button" data-rel="62.50">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="flip_analysis" />
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP (5) -->
        
                                                    <!-- REFI ANALYSIS -->
                                                    <div class="full-section-box"><!-- STEP (6) -->
                                                        <div class="panel-field">
                                                            <h3>REHAB/REFI and HOLD ANALYSIS</h3><hr />
                                                        </div>                                                
                                                        <div class="assump-box-child">

                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                                                                
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="50">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step6Button" data-rel="75">NEXT</button>
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                                <div class="panel-field" id="MonthsRefiRehabRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Projected Rehab Period (Months)</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                                            $attrb = 'id="MonthsRefiRehab" class="form-control" data-popover="true" data-content="How many months  will the actual renovation of the property take place?  Include renovation time only, not time to sell, we will ask for that later."'; ?>
                                                                            <?=form_dropdown('MonthsRefiRehab', $options, $fields->MonthsRefiRehab, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Projected Rehab Period (Months) -->
                                                                <div class="panel-field" id="ActualRefiBudgetRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-8">
                                                                            <input type="hidden" name="RefiHoldingCosts" id="RefiHoldingCosts" />
                                                                            <button type="button" class="btn btn-default" data-target="#RefiRehubModal" data-toggle="modal">
                                                                                REHAB BUDGET (Click here to enter)
                                                                            </button>
                                                                        </div>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ActualRefiBudget",
                                                                                'id'    => "ActualRefiBudget",
                                                                                'value' => number_format($fields->ActualRefiBudget, 2, '.', ''),
                                                                                'class'	=> 'form-control Required',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"Click on this button to enter the projected rehab budget required to rent the property. You have the option of entering \"lump sum budget\" or \"detailed\"",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Actual Refi Rehab -->
                                                                <div class="panel-field" id="ARVRentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">After-Repair Value (ARV) for RENT</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ARVRent",
                                                                                'id'    => "ARVRent",
                                                                                'value' => number_format($fields->ARVRent, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field NumberOnly Required',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"ARV or After-Repair-Value is the projected market value of the property after all the renovations are complete.   This may be different from \"flip ARV\" because scope of work may be different",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- After-Repair Value (ARV) for RENT -->
                                                                <div class="panel-field" id="MonthsToRefiRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Months to Close on Refi (After Rehab)</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array(1 => 1, 2=> 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 21 => 21, 22 => 22, 23 => 23, 24 => 24);
                                                                            $attrb = 'id="MonthsToRefi" class="form-control" data-popover="true" data-content="How many months do you project it will take to close on the refi after the renovations are completed?"'; ?>
                                                                            <?=form_dropdown('MonthsToRefi', $options, $fields->MonthsToRefi, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Months to Close on Refi (After Rehab) -->
                                                                <div class="panel-field" id="TotalCapitalNeededRentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8 text-muted">Total Capital Needed</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TotalCapitalNeededRent",
                                                                                'id'    => "TotalCapitalNeededRent",
                                                                                'value' => number_format($fields->TotalCapitalNeededRent, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"Total capital required to do the project, not including costs associated with financing.  Includes:  purchase price, non-financing closing costs, holding costs and rehab budget.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Total Capital Needed -->
                                                                <div class="panel-field" id="MaxFinancedRefiRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8 text-muted">Max $ that can be financed</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "MaxFinancedRefi",
                                                                                'id'    => "MaxFinancedRefi",
                                                                                'value' => number_format($fields->MaxFinancedRefi, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"Relax this is calculated for you. This number reflects the maximum that your lender will finance based on your inputs:  ARV or cost and the lender cap. ",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Max $ that can be financed -->
                                                                <div class="panel-field" id="ActualFinancedRentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8 text-muted">Actual to be financed (not inc. closing/holding)</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "ActualFinancedRent",
                                                                                'id'    => "ActualFinancedRent",
                                                                                'value' => number_format($fields->ActualFinancedRent, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"This will indicate the actual amount that you will receive from your lender.   Back-end points and interest may be added to this depending on how you set up the loan."
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Actual to be financed (not inc. closing/holding) -->
                                                                <div class="panel-field" id="PointsInterestRolledupRentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8 text-muted">Closing/Holding Costs/Interest Added to Loan</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PointsInterestRolledupRent",
                                                                                'id'    => "PointsInterestRolledupRent",
                                                                                'value' => number_format($fields->PointsInterestRolledupRent, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"Total amount of points and interest added to the loan amount to be paid off at loan maturity/balloon",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Closing/Holding Costs/Interest Added to Loan -->                                                        
                                                                <div class="panel-field" id="TotalLoanRentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Total Loan Amount</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TotalLoanRent",
                                                                                'id'    => "TotalLoanRent",
                                                                                'value' => number_format($fields->TotalLoanRent, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"This equals the 2 fields immediately above this field and reflects the total loan amount due to be paid off at maturity.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Total Loan Amount -->
                                                                <div class="panel-field" id="CashRequiredRentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Cash Required</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CashRequiredRent",
                                                                                'id'    => "CashRequiredRent",
                                                                                'value' => number_format($fields->CashRequiredRent, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"The  total cash you will need over the life of the project, in addition to financing.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Cash Required -->                                                        
                                                                <div class="panel-field" id="TotalCostBasisRentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Total All-in Cost at end of Rehab</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "TotalCostBasisRent",
                                                                                'id'    => "TotalCostBasisRent",
                                                                                'value' => number_format($fields->TotalCostBasisRent, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"This is the total all-in cost basis of the project, including acquisition, closing costs, holding costs, rehab, origination and discount points and interest on the loan.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Total All-in Cost at end of Rehab -->
                                                                <div class="panel-field" id="RefiPercentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Refi % of Appraisal (ARV)</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "RefiPercent",
                                                                                'id'    => "RefiPercent",
                                                                                'value' => round($fields->RefiPercent),
                                                                                'class'	=> 'form-control number-field NumberOnly Required percentageField',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"This is the max % of the property value the lender will lend on.  This is commonly called the LTV ratio or Loan-to-Value.  The value is determined by an appraisal that the lender hires.",
                                                                                'maxlength' => 3,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Refi % of Appraisal (ARV) -->
                                                                <div class="panel-field" id="PermanentRateRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">New Mortgage Rate</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PermanentRate",
                                                                                'id'    => "PermanentRate",
                                                                                'value' => round($fields->PermanentRate),
                                                                                'class'	=> 'form-control number-field NumberOnly Required percentageField',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"Mortgage rate of the permanent loan you are refinancing into",
                                                                                'maxlength' => 3,
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- New Mortgage Rate -->
                                                                <div class="panel-field" id="AmortizationRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"># of Amortization Years / Interest Only?</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $options = array('Interest Only' => 'Interest Only', 5 => 5, 7 => 7, 10 => 10, 15 => 15, 20 => 20, 25 => 25, 30 => 30);
                                                                            $attrb = 'id="Amortization" class="form-control" data-popover="true" data-content="Is the permanent loan into which you are refinancing going to be Interest Only or Amortized?  If Amortized, please select the # of years (1-30)"'; ?>
                                                                            <?=form_dropdown('Amortization', $options, $fields->Amortization, $attrb)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- # of Amortization Years / Interest Only? -->
                                                                <div class="panel-field" id="RefiDiscountPointsRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Refi Discount Points and Misc Costs (%)</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "RefiDiscountPoints",
                                                                                'id'    => "RefiDiscountPoints",
                                                                                'value' => round($fields->RefiDiscountPoints),
                                                                                'class'	=> 'form-control number-field NumberOnly Required percentageField',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"Total closing costs when you refi, as a % of the loan balance",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Refi Discount Points and Misc Costs (%) -->
                                                                <div class="panel-field" id="OperatingIncomeRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">
                                                                            <button type="button" class="btn btn-default" data-target="#OperatingIncomeModal" data-toggle="modal">
                                                                                Click here to Enter Projected Operating Income
                                                                            </button>
                                                                        </label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingIncome",
                                                                                'id'    => "OperatingIncome",
                                                                                'value' => number_format($fields->OperatingIncome, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field Required',
                                                                                'readonly' => 'readonly',
                                                                                'data-popover'=>"true",
                                                                                "data-content"=>"Click on the button to be directed to a screen where you can enter projected rents (operating income)",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Click here to Enter Projected Operating Income -->
                                                                <div class="panel-field" id="OperatingExpensesRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">
                                                                            <button type="button" class="btn btn-default" data-target="#OperatingExpensesModal" data-toggle="modal">
                                                                                Click here to Enter Projected Operating Expenses
                                                                            </button>
                                                                        </label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "OperatingExpenses",
                                                                                'id'    => "OperatingExpenses",
                                                                                'value' => number_format($fields->OperatingExpenses, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field Required',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"Click on the button to be directed to a screen where you can enter projected operating expenses",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Click here to Enter Projected Operating Expenses -->
                                                                <div class="panel-field" id="NOIMonthlyRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Net Operating Income (monthly)</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "NOIMonthly",
                                                                                'id'    => "NOIMonthly",
                                                                                'value' => number_format($fields->NOIMonthly, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"This will show the Net Operating Income of your property in the first year",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Net Operating Income (monthly) -->
                                                                <div class="panel-field" id="NewPaymentRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">New Mtge Pmnt (monthly)</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "NewPayment",
                                                                                'id'    => "NewPayment",
                                                                                'value' => number_format($fields->NewPayment, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"This is the new mortgage payment after Refi (not including PMI or escrow)",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- New Mtge Pmnt (monthly) -->
                                                                <div class="panel-field" id="RefiAmountRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Refi Loan Amount</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "RefiAmount",
                                                                                'id'    => "RefiAmount",
                                                                                'value' => number_format($fields->RefiAmount, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"This will show the $ amount of your new loan",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Refi Loan Amount -->
                                                                <div class="panel-field" id="CashOutRefiRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Cash Out at Refi</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CashOutRefi",
                                                                                'id'    => "CashOutRefi",
                                                                                'value' => number_format($fields->CashOutRefi, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"This will indicate how much cash you are pulling out when refinancing.  Compare this to your original cash investment.  If this number is more, that means you are pulling out in excess of your original investment"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Cash Out at Refi -->
                                                                <div class="panel-field" id="RefiProfitRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Profit at Refi</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "RefiProfit",
                                                                                'id'    => "RefiProfit",
                                                                                'value' => number_format($fields->RefiProfit, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"This is the cash you are pulling out in excess of your cost basis in the property.  This is your profit.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                     </div>
                                                                </div><!-- Profit at Refi -->
                                                                <div class="panel-field" id="RefiROIRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>ROI on Downpayment/Cash Inv.</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "RefiROI",
                                                                                'id'    => "RefiROI",
                                                                                'value' => ($fields->RefiROI=='N/A' ? 'N/A' : round($fields->RefiROI)),
                                                                                'class'	=> 'form-control number-field percentageField',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"If you have your cash in on this deal for the acquisition/rehab costs, this will be your annualized ROI on your initial cash investment and then annualized.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- ROI on Downpayment/Cash Inv. -->
                                                                <div class="panel-field" id="CashTiedUPRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Original money tied up after refi</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CashTiedUP",
                                                                                'id'    => "CashTiedUP",
                                                                                'value' => number_format($fields->CashTiedUP, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"If after refinancing, you have any of your original invested cash still tied up, it will be calculated here.",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Original money tied up after refi -->
                                                                <div class="panel-field" id="EquityLeftRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Equity Left</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "EquityLeft",
                                                                                'id'    => "EquityLeft",
                                                                                'value' => number_format($fields->EquityLeft, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"How much equity you still have left in the property.  (ARV minus Loan Amount).",
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Equity Left -->
                                                                <div class="panel-field" id="CashflowMonthlyRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Cash Flow (Monthly, Pre-Tax)</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CashflowMonthly",
                                                                                'id'    => "CashflowMonthly",
                                                                                'value' => number_format($fields->CashflowMonthly, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"Monthly cashflow after all expenses including mortgage payment, taxes, insurance, and maintenance"
                                                                            ); ?>
                                                                            <div class="input-group">
                                                                                <div class="input-group-addon">$</div>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Cash Flow (Monthly, Pre-Tax) -->
                                                                <div class="panel-field" id="RefiROIAnnualRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8"><strong>Cash-on-Cash (Annual, Pre-Tax)</strong></label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "RefiROIAnnual",
                                                                                'id'    => "RefiROIAnnual",
                                                                                'value' => ($fields->RefiROIAnnual=='Infinite' ? 'Infinite' : round($fields->RefiROIAnnual)),
                                                                                'class'	=> 'form-control number-field percentageField',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"If your original cash investment is tied up in the property, this is your annualized Cash-on-Cash Return.  Of course , if you have pulled out your cash then this cash on cash return will be infinite.",
                                                                            ); ?>
                                                                            <div class="input-group">                                                                
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <hr />
                                                                </div><!-- Cash-on-Cash (Annual, Pre-Tax) -->                                                        
                                                                <h4>Additional Ratios applicable to Refi Scenario</h4><hr />
                                                                <div class="panel-field" id="DCRRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Property DCR (Given New Loan)</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "DCR",
                                                                                'id'    => "DCR",
                                                                                'value' => number_format($fields->DCR, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"Use the DCR as a check to ensure that a refi scenario is going to viable for your lender. This is the Debt Coverage Ratio of the new proposed loan.  If DCR is lower than 1.25, it probably won't work.",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Property DCR (Given New Loan) -->
                                                                <div class="panel-field" id="PaybackPeriodRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Payback Period</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "PaybackPeriod",
                                                                                'id'    => "PaybackPeriod",
                                                                                'value' => number_format($fields->PaybackPeriod, 2, '.', ''),
                                                                                'class'	=> 'form-control number-field',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"If you have your cash tied up in the deal, this will tell you the payback period in years to recoup that money pre-tax dollars."
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Payback Period -->
                                                                <div class="panel-field" id="CapRateARVRow">
                                                                    <div class="row">
                                                                        <label class="col-xs-8">Cap Rate of Property based on ARV</label>
                                                                        <div class="col-xs-4">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "CapRateARV",
                                                                                'id'    => "CapRateARV",
                                                                                'value' => round($fields->CapRateARV),
                                                                                'class'	=> 'form-control number-field percentageField',
                                                                                'readonly' => 'readonly',
                                                                                "data-popover"=>"true",
                                                                                "data-content"=>"This is another good sanity check.  This number will give you the  Cap Rate of your property based on the income/expenses and ARV.  Good sanity check for your income vs. ARV",
                                                                            ); ?>
                                                                            <div class="input-group">                                                                
                                                                                <?=form_input($field)?>
                                                                                <div class="input-group-addon">%</div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Cap Rate of Property based on ARV -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="50">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step6Button" data-rel="75">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="refi_analysis" />
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP (6) -->
                                                    
                                                    <!-- COMPARABLE SALES REPORT -->
                                                    <div class="full-section-box"><!-- STEP (7) -->
                                                        <div class="panel-field">
                                                            <h3>COMPARABLE SALES REPORT</h3><hr />
                                                        </div>                                                
                                                        <div class="assump-box-child">
        
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                                                                
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field topButtons">
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="62.50">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step7Button" data-rel="87.50">NEXT</button>
                                                                </div><!-- Prev/Print Buttons -->
                                                                
                                                                <?php foreach ( $sales as $k => $sale ) { ?>
                                                                    <?php if ( $k != 0 ) { ?><hr /><?php } ?>
                                                                    <input type="hidden" name="ReportID[]" value="<?=$sale->ReportID?>" />
                                                                    <div class="panel-field" id="SalesAddress<?=$k?>Row">
                                                                        <div class="row">
                                                                            <label class="col-xs-3">Address</label>
                                                                            <div class="col-xs-9">
                                                                                <?php 
                                                                                $field = array(
                                                                                    'name'  => "SalesAddress[{$k}]",
                                                                                    'id'    => "SalesAddress_{$k}",
                                                                                    'value' => $sale->SalesAddress,
                                                                                    'class'	=> 'form-control Required',
                                                                                ); ?>
                                                                                <?=form_input($field)?>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Address -->
                                                                    <div class="panel-field" id="BedsBaths<?=$k?>Row">
                                                                        <div class="row">
                                                                            <div class="col-xs-6">
                                                                                <div id="Beds<?=$k?>Row">
                                                                                    <div class="row">
                                                                                        <label class="col-xs-6">Beds</label>
                                                                                        <div class="col-xs-6">
                                                                                            <?php 
                                                                                            $field = array(
                                                                                                'name'  => "Beds[{$k}]",
                                                                                                'id'    => "Beds_{$k}",
                                                                                                'value' => $sale->Beds,
                                                                                                'class'	=> 'form-control NumberOnly Required',
                                                                                            ); ?>
                                                                                            <?=form_input($field)?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <div id="Baths<?=$k?>Row">
                                                                                    <div class="row">
                                                                                        <label class="col-xs-6">Baths</label>
                                                                                        <div class="col-xs-6">
                                                                                            <?php 
                                                                                            $field = array(
                                                                                                'name'  => "Baths[{$k}]",
                                                                                                'id'    => "Baths_{$k}",
                                                                                                'value' => $sale->Baths,
                                                                                                'class'	=> 'form-control NumberOnly Required',
                                                                                            ); ?>
                                                                                            <?=form_input($field)?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Beds Baths -->
                                                                    <div class="panel-field" id="SqFtDateSold<?=$k?>Row">
                                                                        <div class="row">
                                                                            <div class="col-xs-6">
                                                                                <div id="SqFt<?=$k?>Row">
                                                                                    <div class="row">
                                                                                        <label class="col-xs-6">Sq Ft</label>
                                                                                        <div class="col-xs-6">
                                                                                            <?php 
                                                                                            $field = array(
                                                                                                'name'  => "SqFt[{$k}]",
                                                                                                'id'    => "SqFt_{$k}",
                                                                                                'value' => $sale->SqFt,
                                                                                                'class'	=> 'form-control NumberOnly Required',
                                                                                            ); ?>
                                                                                            <?=form_input($field)?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <div id="DateSold<?=$k?>Row">
                                                                                    <div class="row">
                                                                                        <label class="col-xs-6">Date Sold</label>
                                                                                        <div class="col-xs-6">
                                                                                            <?php 
                                                                                            $field = array(
                                                                                                'name'  => "DateSold[{$k}]",
                                                                                                'id'    => "DateSold_{$k}",
                                                                                                'value' => $sale->DateSold,
                                                                                                'class'	=> 'form-control DateSoldField Required',
                                                                                                'data-provide' => 'datepicker',
                                                                                                'data-date-format' => 'mm-dd-yyyy',
																								'readonly' => 'readonly',
                                                                                            ); ?>
                                                                                            <?=form_input($field)?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Sq Ft Date Sold -->
                                                                    <div class="panel-field" id="SalesPriceNotes<?=$k?>Row">
                                                                        <div class="row">
                                                                            <div class="col-xs-6">
                                                                                <div id="SalesPrice<?=$k?>Row">
                                                                                    <div class="row">
                                                                                        <label class="col-xs-6">Sales Price</label>
                                                                                        <div class="col-xs-6">
                                                                                            <?php 
                                                                                            $field = array(
                                                                                                'name'  => "SalesPrice[{$k}]",
                                                                                                'id'    => "SalesPrice_{$k}",
                                                                                                'value' => $sale->SalesPrice,
                                                                                                'class'	=> 'form-control NumberOnly Required',
                                                                                            ); ?>
                                                                                            <div class="input-group">
                                                                                                <div class="input-group-addon">$</div>
                                                                                                <?=form_input($field)?>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                            <div class="col-xs-6">
                                                                                <div id="Notes<?=$k?>Row">
                                                                                    <div class="row">
                                                                                        <label class="col-xs-6">Notes</label>
                                                                                        <div class="col-xs-6">
                                                                                            <?php 
                                                                                            $field = array(
                                                                                                'name'  => "Notes[{$k}]",
                                                                                                'id'    => "Notes_{$k}",
                                                                                                'value' => $sale->Notes,
                                                                                                'class'	=> 'form-control Required',
                                                                                            ); ?>
                                                                                            <?=form_input($field)?>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div><!-- Sales Price Notes -->
                                                                <?php } ?>
                                                                
                                                                <div id="more_sales"></div>
                                                                
                                                                <div class="panel-field">
                                                                    <hr /><a id="last_sales" rel="<?=count($sales)?>"></a>
                                                                    <a id="add_new_sales" class="action-btn" title="Add New" data-placement="top" data-toggle="tooltip">
                                                                        <i class="fa fa-plus-circle"></i>
                                                                    </a>
                                                                    <a id="remove_sales" class="action-btn" data-toggle="tooltip" data-placement="top" title="Remove" style="display:none">
                                                                        <i class="fa fa-minus-circle"></i>
                                                                    </a>
                                                                </div><!-- Add New/Remove Buttons -->
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="62.50">BACK</button>
                                                                    <button type="button" class="btn btn-md btn-primary on_next_section Step7Button" data-rel="87.50">NEXT</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="comparable_reports" />
                                                                </div><!-- Prev/Print Buttons -->
                                                                
                                                            <?=form_close()?>
                                                        
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP (7) -->
                                                    
                                                    <!-- ADDITIONAL PICTURES -->
                                                    <div class="full-section-box"><!-- STEP (8) -->
                                                        <div class="panel-field">
                                                            <h3>PICTURES AND REPORT HEADLINES</h3><hr />
                                                        </div>
                                                        <div class="assump-box-child">
                                                        
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PictureForm'); ?>
                                                            <?=form_open_multipart(base_url('pages/ajax-image-upload' . '/' . $PropertyID . '/' . $CreatedOn . '/property_photos/rehab') . '/', $attributes)?>
                                                            	
                                                                <div class="panel-field topButtons">
                                                                    <img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide loading" />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="75">BACK</button>
                                                                    <button type="submit" name="upload_btn" class="btn btn-md btn-primary on_save_event Step8Button" data-rel="100">SAVE</button>
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                                <div class="panel-field row" id="PreviewRow">
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <div id="PreviewResults">
                                                                            	<input type="hidden" name="OldPhotos" id="OldPhotos" value="<?php echo $fields->Photos ? $fields->Photos : ''; ?>" />
                                                                                <?php if ( !empty($fields->Photos) ) { ?>
                                                                                    <?php $images = explode('|', $fields->Photos); ?>
                                                                                    <?php $captions = explode('||||', $fields->Captions); ?>
                                                                                    <?php $cover = explode('|', $fields->Cover); ?>
                                                                                    <?php for ( $i = 0; $i < count($images); $i++ ) { ?>
                                                                                        <div class="col-sm-3 image-upload">
                                                                                        	<div class="Check_Cover">
                                                                                                <input type="radio" name="SetCover" class="radio SetCover" <?php echo $cover[$i]=='on' ? 'checked' : ''; ?> />
                                                                                                <label>Cover Photo</label>
                                                                                                <input type="hidden" class="CoverImages" name="CoversImages[]" value="<?php echo $cover[$i]=='on' ? 'on' : 'off'; ?>" />
                                                                                            </div>
                                                                                            <a class="DeletePhotos" data-id="<?=$fields->PhotoID?>" data-photo="<?php echo $images[$i]; ?>" data-table="property_photos" onclick="DeletePhotos(this);"><i class="fa fa-times"></i></a>
                                                                                            <img alt="pic" id="Preview_Images_<?=$i?>" class="thumbnail" src="<?php echo $images[$i]; ?>" width="100%" height="110" style="margin-bottom:5px;" />
                                                                                            <textarea name="Captions[]" id="Captions_<?=$i?>" class="form-control" rows="1" style="margin-bottom:10px;"><?php echo isset($captions[$i]) ? $captions[$i] : ' '; ?></textarea>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                <?php } ?>
                                                                                <div id="added_preview_0" class="col-sm-3 image-upload">
                                                                                	<div class="Check_Cover">
                                                                                    	<input disabled type="radio" name="SetCover" class="radio SetCover" />
                                                                                        <label for="SetCover_0">Cover Photo</label>
                                                                                        <input disabled type="hidden" class="CoverImages" name="CoversImages[]" value="off" />
                                                                                    </div>
                                                                                	<img id="Preview_Images_0" class="thumbnail" src="<?=base_url('assets/images/preview.jpg')?>" width="100%" height="110" style="margin-bottom:5px;" />
                                                                                    <textarea disabled name="Captions[]" id="Caption_0" class="form-control" rows="1" style="margin-bottom:10px;"></textarea>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Preview -->
                                                                
                                                                <div class="panel-field" id="Images0Row">
                                                                    <div class="row">
                                                                        <div class="col-xs-12">
                                                                            <p><strong>If you want to change the images please upload, otherwise leave this field blank.</strong></p>
                                                                        </div>
                                                                        <label class="col-xs-6">Pictures</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Images[0]",
                                                                                'id'    => "Images_0",
                                                                                'class'	=> 'form-control images',
																				'onChange' => 'ValidateFileUpload(this.id)'
                                                                            ); ?>
                                                                            <?=form_upload($field)?>
                                                                            <font size="1">Allowed file types: GIF, JPG, JPEG, PNG, BMP</font>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Images -->
                                                                
                                                                <div id="more_image"></div>
        
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <div class="pull-left">
	                                                                    <a id="last_image" rel="<?=(!empty($fields->Photos) ? count($images) : 1)?>"></a>
    	                                                                <a id="add_new_image" class="action-btn" title="Add New" data-placement="top" data-toggle="tooltip">
        	                                                                <i class="fa fa-plus-circle"></i>
            	                                                        </a>
                	                                                    <a id="remove_image" class="action-btn" data-toggle="tooltip" data-placement="top" title="Remove" style="display:none">
  	                                                                      <i class="fa fa-minus-circle"></i>
    	                                                                </a>
                                                                    </div>
                                                                    <div class="clearfix"></div>
                                                                    <hr />
                                                                </div><!-- Next/Prev Buttons -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                            <?php $attributes = array('class' => 'form-horizontal', 'name' => 'PropertyForm'); ?>
                                                            <?=form_open(base_url('pages/edit-proposal/' . $PropertyID . '/proposals') . '/', $attributes)?>
                                                            
                                                                <input type="hidden" name="PropertyID" value="<?=$PropertyID?>" />
                                                                <input type="hidden" name="UserID" value="<?=$user->id?>" />
                                                                
                                                                <div class="panel-field" id="Headline1Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Headline</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "Headline",
                                                                                'id'    => "Headline",
                                                                                'value' => $fields->Headline,
                                                                                'class'	=> 'form-control',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Headline 1 -->
                                                                <div class="panel-field" id="SubHeadline1Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Sub Headline 1</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SubHeadline1",
                                                                                'id'    => "SubHeadline1",
                                                                                'value' => $fields->SubHeadline1,
                                                                                'class'	=> 'form-control',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Sub Headline 1 -->
                                                                <div class="panel-field" id="SubHeadline2Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Sub Headline 2</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SubHeadline2",
                                                                                'id'    => "SubHeadline2",
                                                                                'value' => $fields->SubHeadline2,
                                                                                'class'	=> 'form-control',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Sub Headline 2 -->
                                                                <div class="panel-field" id="SubHeadline3Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Sub Headline 3</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SubHeadline3",
                                                                                'id'    => "SubHeadline3",
                                                                                'value' => $fields->SubHeadline3,
                                                                                'class'	=> 'form-control',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Sub Headline 3 -->
                                                                <div class="panel-field" id="SubHeadline4Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Sub Headline 4</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SubHeadline4",
                                                                                'id'    => "SubHeadline4",
                                                                                'value' => $fields->SubHeadline4,
                                                                                'class'	=> 'form-control',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Sub Headline 4 -->
                                                                <div class="panel-field" id="SubHeadline5Row">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Sub Headline 5</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "SubHeadline5",
                                                                                'id'    => "SubHeadline5",
                                                                                'value' => $fields->SubHeadline5,
                                                                                'class'	=> 'form-control',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Sub Headline 5 -->
                                                                <div class="panel-field" id="UnitMixRow">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Unit Mix</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "UnitMix",
                                                                                'id'    => "UnitMix",
                                                                                'value' => $fields->UnitMix,
                                                                                'class'	=> 'form-control',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Unit Mix -->
                                                                <div class="panel-field" id="DescPropRow">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Desciption of Property</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "DescProp",
                                                                                'id'    => "DescProp",
                                                                                'value' => $fields->DescProp,
                                                                                'class'	=> 'form-control',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Desc Prop -->
                                                                <div class="panel-field" id="AreaNotesRow">
                                                                    <div class="row">                                                                
                                                                        <label class="col-xs-6">Notes on Area</label>
                                                                        <div class="col-xs-6">
                                                                            <?php 
                                                                            $field = array(
                                                                                'name'  => "AreaNotes",
                                                                                'id'    => "AreaNotes",
                                                                                'value' => $fields->AreaNotes,
                                                                                'class'	=> 'form-control',
                                                                                'data-popover'=>'true',
                                                                                'data-content'=>"",
                                                                            ); ?>
                                                                            <?=form_input($field)?>
                                                                        </div>
                                                                    </div>
                                                                </div><!-- Desc Prop -->
                                                                
                                                                <div class="panel-field text-right">
                                                                    <hr />
                                                                    <img src="<?=base_url('assets/images/loading.gif')?>" alt="loading" class="hide loading" />
                                                                    <button type="button" class="btn btn-md btn-default on_prev_section trigger-bar" data-rel="75">BACK</button>
                                                                    <button type="submit" name="upload_btn" class="btn btn-md btn-primary on_save_event Step8Button" data-rel="100">SAVE</button>
                                                                    <input type="hidden" name="CreatedOn" value="<?=$CreatedOn?>" />
                                                                    <input type="hidden" name="TableName" value="property_headlines" />
                                                                    <div class="clearfix"></div>
                                                                </div><!-- PRINT REPORT Button -->
                                                                
                                                            <?=form_close()?>
                                                            
                                                        </div>
                                                        <div class="clearfix"></div>
                                                    </div><!-- STEP (8) -->
                                                    
                                                    <div class="clearfix"></div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <footer>
                    <div class="row">
                        <div class="col-xs-6">
                            <p class="text-left">
                            	<a href="<?=base_url('pages/blog')?>/">Blog</a> | 
                                <a href="<?=base_url('pages/our-offer')?>/">Our Offer</a> | 
                                <a href="<?=base_url('pages/features')?>/">Features</a>
                            </p>
                        </div>
                        <div class="col-xs-6">
                            <p class="text-right">&copy; Copyright 2015. Realty Fund Connect.</p>
                        </div>
                    </div>
                </footer>
            </aside>
        </div>
    </div>
</section>

<?php $this->load->view('template/closing-cost-modal-edit'); ?>
<?php $this->load->view('template/holding-cost-modal-edit'); ?>
<?php $this->load->view('template/flip-analysis-modal-edit'); ?>
<?php $this->load->view('template/refi-analysis-modal-edit'); ?>
<?php $this->load->view('template/operating-income-modal-edit'); ?>
<?php $this->load->view('template/operating-expenses-modal-edit'); ?>