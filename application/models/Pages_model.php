<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
class Pages_model extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }
	
	// Start of check user exist
	public function check_user_exists($email) {
		$this->db->where("email", $email);
		$query = $this->db->get("users");
		if ( $query->num_rows() == 1 ) {
			return 1;
		} else {
			return 0;
		}
	}
	// End of the function
            
	// Start of linkedin register process
	public function do_linkedin($data, $exists = FALSE) {
		if ( $exists == FALSE ) {
			$this->db->set('register_on', 'NOW()', FALSE);
			$this->db->insert('users', $data);
		}
		$this->db->where("linkedin", $data['linkedin']);
		$this->db->where("email", $data['email']);
		$query = $this->db->get("users");
		if ( $query->num_rows() == 1 ) {
			foreach($query->result() as $rows) {
				//add all data to session
				$newdata = array(
					'user_id'  		=> $rows->id,
					'linkedin'	  	=> $rows->linkedin,
					'user_email'	=> $rows->email,
					'name'			=> $rows->fname,
					'logged_in'  	=> TRUE,
				);
			}
			$this->session->set_userdata($newdata);
			return true;
		} else {
			return 0;	
		}
		return false;
    }
	// End of the function
            
	// Start of email register process
	public function do_register($data, $exists = FALSE) {
		if ( $exists == FALSE ) {
			$this->db->set('register_on', 'NOW()', FALSE);
			$this->db->insert('users', $data);
		}
		$this->db->where("fname", $data['fname']);
		$this->db->where("email", $data['email']);
		$query = $this->db->get("users");
		if ( $query->num_rows() == 1 ) {
			return true;
		}
		return false;
    }
	// End of the function

	// Start of linkedin register process
	public function do_login($email, $password, $user_type) {
		$this->db->where("email", $email);
		$this->db->where("password", $password);
		$this->db->where("user_type", $user_type);
		$query = $this->db->get("users");
		if ( $query->num_rows() == 1 ) {
			foreach($query->result() as $rows) {
				//add all data to session
				$newdata = array(
					'user_id'  		=> $rows->id,
					'linkedin'	  	=> $rows->linkedin,
					'user_email'	=> $rows->email,
					'name'			=> $rows->fname,
					'logged_in'  	=> TRUE,
				);
			}
			$this->session->set_userdata($newdata);
			return 1;
		} else {
			return 0;
		}
    }
	// End of the function
	
	public function update_password($password, $email) {
		$data = array(
			'password' => sha1($password),
		);
		$this->db->where('email', $email);
		$this->db->update('users', $data);
	}
	
	// Start of usertype login check
	public function check_usertype($email = NULL, $user_id = NULL) {
		if ( $email ) {
			$this->db->where("email", $email);
		} elseif ( $user_id ) {
			$this->db->where("id", $user_id);
		}
		$query = $this->db->get("users");
		if ( $query->num_rows() == 1 ) {
			return $query->row()->user_type;
		} else {
			return 0;
		}
	}
	// Check of the function

	// Start of get user details
	public function get_user($user_id) {
		$this->db->where("id", $user_id);
		$query = $this->db->get("users");
		return $query->result();
	}
	// End of the function
	
	// Start of get user details
	public function get_user_by_email($email) {
		$this->db->where("email", $email);
		$query = $this->db->get("users");
		return $query->row();
	}
	// End of the function
            
	// Start of get public user details
	public function get_public_user($public_id) {
		$this->db->trans_start();
		$query = $this->db->query("SELECT * FROM `ci_users` AS `u`, `ci_profile` AS `p` WHERE `u`.`id` = $public_id AND `p`.`user_id` = `u`.`id`");
		return $query->result();
	}
	// End of the function
            
	// Start of check user profile exist
	public function check_user_profile_exists($user_id) {
		$this->db->where("user_id", $user_id);
		$query = $this->db->get("profile");
		if ( $query->num_rows() == 1 ) {
			return 1;
		} else {
			return 0;
		}
	}
	// End of the function
            
	// Start of check user portfolio exist
	public function check_user_portfolio_exists($user_id) {
		$this->db->where("user_id", $user_id);
		$query = $this->db->get("portfolio");
		if ( $query->num_rows() > 0 ) {
			return 1;
		} else {
			return 0;
		}
	}
	// End of the function
            
	// Start of check user public proposal exist
	public function check_user_proposal_exists($user_id) {
		$this->db->where("UserID", $user_id);
		$query = $this->db->get("property_information");
		if ( $query->num_rows() > 0 ) {
			return 1;
		} else {
			return 0;
		}
	}
	// End of the function
	
	// Start of check user public proposal exist
	public function check_user_offer_exists($user_id) {
		$this->db->where("UserID", $user_id);
		$query = $this->db->get("max_offer");
		if ( $query->num_rows() > 0 ) {
			return 1;
		} else {
			return 0;
		}
	}
	// End of the function
	
	// Start of check user public proposal exist
	public function check_user_rental_exists($user_id) {
		$this->db->where("UserID", $user_id);
		$query = $this->db->get("rental_property_information");
		if ( $query->num_rows() > 0 ) {
			return 1;
		} else {
			return 0;
		}
	}
	// End of the function
	
	// Start of check user public proposal exist
	public function check_user_analyser_exists($user_id) {
		$this->db->where("UserID", $user_id);
		$query = $this->db->get("analyser_property_details");
		if ( $query->num_rows() > 0 ) {
			return 1;
		} else {
			return 0;
		}
	}
	// End of the function
            
	// Start of profile_data process
    public function update_account_data($user_id) {
		$this->db->where("id", $user_id);
		$query = $this->db->get("users");
		if ( $query->num_rows() == 1 ) {
			if ( $this->input->post('password') != '' ) {
				$password = sha1($this->input->post('password'));
				$data = array(
					'fname' 		=> $this->input->post('fname'),
					'dob' 			=> $this->input->post('dob'),
					'phone' 		=> $this->input->post('phone') ? $this->input->post('phone') : 'Not updated',
					'password' 		=> $password,
					'street' 		=> $this->input->post('street'),
					'town' 			=> $this->input->post('town'),
					'state' 		=> $this->input->post('state'),
					'zip' 			=> $this->input->post('zip'),
					'country' 		=> $this->input->post('country'),
					'linkedin_url' 	=> $this->input->post('linkedin_url') ? $this->input->post('linkedin_url') : 'Not updated',
					'facebook_url' 	=> $this->input->post('facebook_url') ? $this->input->post('facebook_url') : 'Not updated',
					'twitter_url' 	=> $this->input->post('twitter_url') ? $this->input->post('twitter_url') : 'Not updated',
				);
			} elseif ( $this->input->post('password') == '') {
				$data = array(
					'fname' 		=> $this->input->post('fname'),
					'dob' 			=> $this->input->post('dob'),
					'phone' 		=> $this->input->post('phone') ? $this->input->post('phone') : 'Not updated',
					'street' 		=> $this->input->post('street'),
					'town' 			=> $this->input->post('town'),
					'state' 		=> $this->input->post('state'),
					'zip' 			=> $this->input->post('zip'),
					'country' 		=> $this->input->post('country'),
					'linkedin_url' 	=> $this->input->post('linkedin_url') ? $this->input->post('linkedin_url') : 'Not updated',
					'facebook_url' 	=> $this->input->post('facebook_url') ? $this->input->post('facebook_url') : 'Not updated',
					'twitter_url' 	=> $this->input->post('twitter_url') ? $this->input->post('twitter_url') : 'Not updated',
				);
			} elseif ( $this->input->post('password_linkedin') != '' ) {
				$password = 'linkedin';
				$data = array(
					'fname' 		=> $this->input->post('fname'),
					'dob' 			=> $this->input->post('dob'),
					'phone' 		=> $this->input->post('phone') ? $this->input->post('phone') : 'Not updated',
					'password' 		=> $password,
					'street' 		=> $this->input->post('street'),
					'town' 			=> $this->input->post('town'),
					'state' 		=> $this->input->post('state'),
					'zip' 			=> $this->input->post('zip'),
					'country' 		=> $this->input->post('country'),
					'linkedin_url' 	=> $this->input->post('linkedin_url') ? $this->input->post('linkedin_url') : 'Not updated',
					'facebook_url' 	=> $this->input->post('facebook_url') ? $this->input->post('facebook_url') : 'Not updated',
					'twitter_url' 	=> $this->input->post('twitter_url') ? $this->input->post('twitter_url') : 'Not updated',
				);
			} elseif ( $this->input->post('password_linkedin') == '' ) {
				$data = array(
					'fname' 		=> $this->input->post('fname'),
					'dob' 			=> $this->input->post('dob'),
					'phone' 		=> $this->input->post('phone') ? $this->input->post('phone') : 'Not updated',
					'street' 		=> $this->input->post('street'),
					'town' 			=> $this->input->post('town'),
					'state' 		=> $this->input->post('state'),
					'zip' 			=> $this->input->post('zip'),
					'country' 		=> $this->input->post('country'),
					'linkedin_url' 	=> $this->input->post('linkedin_url') ? $this->input->post('linkedin_url') : 'Not updated',
					'facebook_url' 	=> $this->input->post('facebook_url') ? $this->input->post('facebook_url') : 'Not updated',
					'twitter_url' 	=> $this->input->post('twitter_url') ? $this->input->post('twitter_url') : 'Not updated',
				);
			}
			
			$this->db->where('id', $user_id);
			$this->db->update('users', $data);
		}
    }
	// End of the function
            
	// Start of profile_data process
    public function update_profile_data($user_id, $user_type) {
		$this->db->where("user_id", $user_id);
		$query = $this->db->get("profile");
		if ( $query->num_rows() == 1 ) {
			$this->db->set('updated_on', 'NOW()', FALSE);
			if ( $user_type == 'investor' ) {
				$data = array(
					'sales_pitch' 		=> $this->input->post('sales_pitch'),
					'about_you' 		=> $this->input->post('about_you'),
					'about_investment' 	=> $this->input->post('about_investment'),
					'youtube_link' 		=> $this->input->post('youtube_link'),
				);
			} elseif ( $user_type == 'cash_lender' ) {
				$data = array(
					'sales_pitch' 		=> $this->input->post('sales_pitch'),
					'about_you' 		=> $this->input->post('about_you'),
					'about_investment' 	=> $this->input->post('about_investment'),
				);
			}
			$this->db->where('user_id', $user_id);
			$this->db->update('profile', $data); 
		} else {
			$this->db->set('updated_on', 'NOW()', FALSE);
			if ( $user_type == 'investor' ) {
				$data = array(
					'user_id'			=> $user_id,
					'sales_pitch' 		=> $this->input->post('sales_pitch'),
					'about_you' 		=> $this->input->post('about_you'),
					'about_investment' 	=> $this->input->post('about_investment'),
					'youtube_link' 		=> $this->input->post('youtube_link'),
				);
			} elseif ( $user_type == 'cash_lender' ) {
				$data = array(
					'user_id'			=> $user_id,
					'sales_pitch' 		=> $this->input->post('sales_pitch'),
					'about_you' 		=> $this->input->post('about_you'),
					'about_investment' 	=> $this->input->post('about_investment'),
				);
			}
			$this->db->insert('profile', $data);
		}
    }
	// End of the function
            
	// Start of profile_data process
    public function update_profile_picture($user_id, $pic_big, $pic_medium, $pic_small) {
		$this->db->where("id", $user_id);
		$query = $this->db->get("users");
		if ( $query->num_rows() == 1 ) {
			$data = array(
				'picture' 		 => $pic_big,
				'picture_medium' => $pic_medium,
				'picture_small'  => $pic_small,
            );
			$this->db->where('id', $user_id);
			$this->db->update('users', $data); 
		}
    }
	// End of the function
            
	// Start of get user profile details
	public function get_user_profile($user_id) {
		$this->db->where("user_id", $user_id);
		$query = $this->db->get("profile");
		return $query->result();
	}
	// End of the function
    
	
	
	
	// Start of getting portfolio by portfolio id
	public function get_portfolio($portfolio_id) {
		$this->db->where("id", $portfolio_id);
		$query = $this->db->get("portfolio");
		return $query->result();
    }
	// End of the function
	
	// Start of portfolio_data process
	public function add_portfolio_data($user_id, $portfolio_pic) {
		$this->db->set('created_on', 'NOW()', FALSE);
		$data = array(
			'user_id'		=> $user_id,
			'property_name' => $this->input->post('property_name'),
			'location' 		=> $this->input->post('property_add'),
			'youtube_link' 	=> $this->input->post('youtube_link'),
			'info_one' 		=> $this->input->post('info_one'),			
			'info_two'		=> $this->input->post('info_two'),
			'images'		=> $portfolio_pic,
		);
		$this->db->insert('portfolio', $data);
    }
	// End of the function
	
	// Start of update_portfolio_data process
	public function update_portfolio_data($portfolio_id, $new_portfolio_pic) {
		$data = array(
			'property_name' => $this->input->post('property_name'),
			'location' 		=> $this->input->post('property_add'),
			'youtube_link' 	=> $this->input->post('youtube_link'),
			'info_one' 		=> $this->input->post('info_one'),
			'info_two'		=> $this->input->post('info_two'),
			'images'		=> $new_portfolio_pic,
		);
		$this->db->where('id', $portfolio_id);
		$this->db->update('portfolio', $data); 
    }
	// End of update_portfolio_data process
    
	// Start of get portfolio list
	public function get_portfolio_lists($user_id) {
		$this->db->where("user_id", $user_id);
		$query = $this->db->get("portfolio");
		return $query->result();
    }
	// End of the function
	
	// Start of delete_portfolio process
	public function delete_portfolio($portfolio_id) {
		$this->db->where('id', $portfolio_id);
		$this->db->delete('portfolio');
	}
	// End of the function
	
	
	
	
	// Start of getting rental lists
	public function get_rental_lists($user_id) {
		$this->db->where("UserID", $user_id);
		$this->db->order_by("RentalPropertyID", "DESC");
		$query = $this->db->get("rental_property_information");
		return $query->result();
	}
	// End of the function
	
	// Start of delete rental calculation process
	public function delete_rental($rental_id) {
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_cash_outlay');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_financial_analysis');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_helpful_calculator');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_helpful_calculator1');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_holding_resale');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_operating_expenses');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_operating_income');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_price_assumptions');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_property_information');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_property_photos');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_advance');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_tax_assumption');
		
		$this->db->where('PropertyID', $rental_id);
		$this->db->delete('rental_headlines');
	}
	// End of the function
	
	// Start of getting max offers by offer id
	public function get_rental($rental_id) {
		$this->db->where("PropertyID", $rental_id);
		$query = $this->db->get("rental_property_information");
		return $query->result();
    }
	// End of the function
	
	
	
	
	// Start of getting proposal by public proposal id
	public function get_proposal($proposal_id) {
		$this->db->where("PropertyID", $proposal_id);
		$query = $this->db->get("property_information");
		return $query->result();
    }
	// End of the function
	
	// Start of getting proposal photos by proposal id
	public function get_proposal_photos($proposal_id) {
		$this->db->where("PropertyID", $proposal_id);
		$query = $this->db->get("property_photos");
		return $query->row();
    }
	// End of the function
	
	// Start of get public proposal list
	public function get_proposal_lists($user_id) {
		$this->db->where("UserID", $user_id);
		$this->db->order_by("InfoID", "DESC");
		$query = $this->db->get("property_information");
		return $query->result();
    }
	// End of the function
	
	// Start of delete_public_proposal process
	public function delete_proposal($proposal_id) {
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('property_information');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('personal_information');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('purchase_assumption');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('closing_costs');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('holding_costs');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('financing_assumption');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('flip_analysis');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('flip_budget');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('refi_analysis');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('refi_budget');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('operating_income');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('operating_expenses');
		
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('comparable_reports');
		
		$this->db->where('PropertyID', $proposal_id);
		$this->db->delete('property_photos');
	}
	// End of the function

	// Start of get single proposal info
	public function get_single_proposal($proposal_id) {
		$this->db->select("*, property_information.PropertyID AS `proposal_id`");
		$this->db->from("property_information");
		$this->db->join("users","property_information.UserID = users.id");
		$this->db->join("personal_information","personal_information.PropertyID = property_information.PropertyID");
		$this->db->where("property_information.PropertyID", $proposal_id);
		$query=$this->db->get();
		return $query->result();
	}
	// End of the function
	
	// Start of get single proposal info
	public function get_single_rental($proposal_id) {
		$this->db->select("*, rental_property_information.PropertyID AS `proposal_id`");
		$this->db->from("rental_property_information");
		$this->db->join("users", "rental_property_information.UserID = users.id");
		$this->db->where("rental_property_information.PropertyID", $proposal_id);
		$query=$this->db->get();
		return $query->result();
	}
	// End of the function
	
	// Start of get user details
	public function getUserDetails($user_id){
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('id',$user_id);
		$query = $this->db->get();
		$result = $query->row();
		return $result;
	}
	// End of the function
	
	// Start of get cashlender proposal
	public function get_cashlender_proposal($proposal_id) {
		$this->db->select('*');
		$this->db->from('property_information');
		$this->db->join('personal_information','property_information.PropertyID=personal_information.PropertyID');
		$this->db->join("property_photos", "property_photos.PropertyID=property_information.PropertyID");
		$this->db->join('users','property_information.UserID=users.id');
		$this->db->where("property_information.PropertyID",$proposal_id);
		$query = $this->db->get();
		return $query->row();
	}
	// End of the function
	
	
	
	
	
	// Start of getting max offers by offer id
	public function get_offer($offer_id) {
		$this->db->where("PropertyID", $offer_id);
		$query = $this->db->get("max_offer");
		return $query->result();
    }
	// End of the function
	
	// Start of get full max offer
	public function get_full_max_offer($offer_id) {
		$this->db->select('*');
		$this->db->from('max_offer');
		$this->db->join('offer_rate','max_offer.PropertyID=offer_rate.PropertyID');
		$this->db->where("max_offer.PropertyID", $offer_id);
		$query = $this->db->get();
		return $query->row();
	}
	// End of the function
	
	// Start of get public proposal list
	public function get_max_offer_lists($user_id) {
		$this->db->where("UserID", $user_id);
		$this->db->order_by("OfferID", "DESC");
		$query = $this->db->get("max_offer");
		return $query->result();
    }
	// End of the function
	
	// Start of Delete Max Offer process
	public function delete_offer($offer_id) {
		$this->db->where('PropertyID', $offer_id);
		$this->db->delete('max_offer');
		
		$this->db->where('PropertyID', $offer_id);
		$this->db->delete('offer_rate');
	}
	// End of the function
	
	
	
	// Start of getting max offers by offer id
	public function get_comparison($comp_id) {
		$this->db->where("PropertyID", $comp_id);
		$query = $this->db->get("analyser_property_details");
		return $query->result();
    }
	// End of the function
	
	// Start of get full max offer
	public function get_full_comparison($comp_id) {
		$this->db->select('analyser_property_details.*, analyser_extra_details.*, analyser_financing_assumption.*');
		$this->db->from('analyser_property_details');
		$this->db->join('analyser_extra_details',			'analyser_property_details.PropertyID=analyser_extra_details.PropertyID');
		$this->db->join('analyser_financing_assumption',	'analyser_property_details.PropertyID=analyser_financing_assumption.PropertyID');		
		$this->db->where("analyser_property_details.PropertyID", $comp_id);
		$query = $this->db->get();
		return $query->row();
	}
	// End of the function
	
	// Start of get comparison_list
	public function get_comparison_lists($user_id) {
		$this->db->select('analyser_property_details.*, analyser_extra_details.*');
		$this->db->from('analyser_property_details');
		$this->db->join('analyser_extra_details', 'analyser_property_details.PropertyID=analyser_extra_details.PropertyID');
		$this->db->where("analyser_property_details.UserID", $user_id);
		$this->db->order_by("analyser_property_details.DetailedID", "DESC");
		$query = $this->db->get();
		return $query->result();
    }
	// End of the function
	
	// Start of Delete comparison analyser process
	public function delete_comparison($comp_id) {
		$this->db->where('PropertyID', $comp_id);
		$this->db->delete('analyser_property_details');
		
		$this->db->where('PropertyID', $comp_id);
		$this->db->delete('analyser_financing_assumption');
		
		$this->db->where('PropertyID', $comp_id);
		$this->db->delete('analyser_extra_details');
	}
	// End of the function
	
     			
	
	

	// Start of set cashlender proposal view
	public function set_cashlender_proposal_view($investor_id, $cashlender_id, $proposal_id) {
		$this->db->set('visit_at', 'NOW()', FALSE);
		$data = array(
			'investor_id' 	=> $investor_id,
			'cashlender_id'	=> $cashlender_id,
			'proposal_id' 	=> $proposal_id,
			'download_link' => base_url('pages/download-cl-csv/' . $investor_id . '-' . $cashlender_id . '.csv') . '/',
			'downloaded'	=> 'no',
		);
		$this->db->insert('proposal_view', $data); 
	}
	// End of the function
            
	// Start of get cashlender proposal view
	public function get_cashlender_proposal_view($investor_id, $cashlender_id, $proposal_id) {
		$this->db->where("investor_id", $investor_id);
		$this->db->where("cashlender_id", $cashlender_id);
		$this->db->where("proposal_id", $proposal_id);
		$query = $this->db->get("proposal_view");
		if ( $query->num_rows() >= 1 ) {
			return 'viewed';
		} else {
			return 'notviewed';
		}
	}
	// End of the function
	
	// Start of get cashlender proposal views list
	public function get_cashlender_proposal_views_list() {
		$this->db->select("*");
		$this->db->from("proposal_view");
		$this->db->join("users", "users.id=proposal_view.cashlender_id");
		$this->db->order_by("proposal_view.id", 'desc');
		$this->db->group_by("proposal_view.cashlender_id");
		$query = $this->db->get();
		return $query->result();
	}
	// End of the function
	
	// Start of get cashlender proposal view
	public function investor_mail_scheduling() {
		$this->db->from("proposal_view");
		$this->db->group_by("investor_id");
		$query = $this->db->get();
		return $query->result();
	}
	// End of the function
	
	// Start of get cashlender visit count
	public function cashleander_visit_count($investor_id) {
		$this->db->select("COUNT(*) AS count");
		$this->db->where("investor_id", $investor_id);
		$this->db->where("downloaded", "no");
		$this->db->from("proposal_view");
		$query = $this->db->get();
		return $query->result();
	}
	// End of the function
	
	// Start of update visit flag after downloading the csv file
	public function update_downloaded_status($filename) {
		$fileurl = base_url('pages/download-cl-csv/' . $filename) . '/';

		$this->db->select("download_count");
    	$this->db->from("proposal_view");
    	$this->db->where('download_link', $fileurl);
    	$query = $this->db->get();
		$result = $query->row();

		$data = array(
			'downloaded' => 'yes',
			'download_count' => $result->download_count + 1,
		);
		$this->db->where('download_link', $fileurl);
		$this->db->update('proposal_view', $data); 
	}
	// End of the function
	
	// Start of proposal filter search results
	public function proposal_filter_results($InvestorLocation, $LoanValue, $PropertyLocation, $RepairValue) {
		$properties = array();
		
		$loan_value_exploded = explode(",", $LoanValue);
		$loan_min_value = $loan_value_exploded[0];
		$loan_max_value = $loan_value_exploded[1];

		$repair_value_exploded = explode(",", $RepairValue);
		$repair_min_value = $repair_value_exploded[0];
		$repair_max_value = $repair_value_exploded[1];
		
		$queryProp = $this->db->query("SELECT *, `ci_property_information`.`PropertyID` AS `proposal_id` FROM `ci_property_information` JOIN `ci_users` ON `ci_users`.`id` = `ci_property_information`.`UserID` JOIN `ci_flip_analysis` ON `ci_flip_analysis`.`UserID` = `ci_property_information`.`UserID` JOIN `ci_refi_analysis` ON `ci_refi_analysis`.`UserID` = `ci_property_information`.`UserID` WHERE `ci_property_information`.`Submitted` = 'Yes' AND (`ci_users`.`street` LIKE '%$InvestorLocation%' OR `ci_users`.`town` LIKE '%$InvestorLocation%' OR `ci_users`.`state` LIKE '%$InvestorLocation%' OR `ci_users`.`zip` LIKE '%$InvestorLocation%' OR `ci_users`.`country` LIKE '%$InvestorLocation%' OR `ci_property_information`.`PropertyStreetAddress` LIKE '%$PropertyLocation%' OR `ci_property_information`.`PropertyCityTown` LIKE '%$PropertyLocation%' OR `ci_property_information`.`PropertyStateProvince` LIKE '%$PropertyLocation%' OR `ci_property_information`.`PropertyZipCode` LIKE '%$PropertyLocation%' OR `ci_property_information`.`PropertyCountry` LIKE '%$PropertyLocation%' OR (`ci_flip_analysis`.`TotalCapitalNeededFlip` BETWEEN $loan_min_value AND $loan_max_value) OR (`ci_flip_analysis`.`ARVFlip` BETWEEN $repair_min_value AND $repair_max_value) OR (`ci_refi_analysis`.`TotalCapitalNeededRent` BETWEEN $loan_min_value AND $loan_max_value) OR (`ci_refi_analysis`.`ARVRent` BETWEEN $repair_min_value AND $repair_max_value))");
		$proposals = $queryProp->result();
		
		$queryRent = $this->db->query("SELECT *, `ci_rental_property_information`.`PropertyID` AS `proposal_id` FROM `ci_rental_property_information` JOIN `ci_users` ON `ci_users`.`id` = `ci_rental_property_information`.`UserID` WHERE `ci_rental_property_information`.`Submitted` = 'Yes' AND (`ci_users`.`street` LIKE '%$InvestorLocation%' OR `ci_users`.`town` LIKE '%$InvestorLocation%' OR `ci_users`.`state` LIKE '%$InvestorLocation%' OR `ci_users`.`zip` LIKE '%$InvestorLocation%' OR `ci_users`.`country` LIKE '%$InvestorLocation%' OR `ci_rental_property_information`.`PropertyStreetAddress` LIKE '%$PropertyLocation%' OR `ci_rental_property_information`.`PropertyCityTown` LIKE '%$PropertyLocation%' OR `ci_rental_property_information`.`PropertyStateProvince` LIKE '%$PropertyLocation%' OR `ci_rental_property_information`.`PropertyZipCode` LIKE '%$PropertyLocation%' OR `ci_rental_property_information`.`PropertyCountry` LIKE '%$PropertyLocation%')");
		$rentals = $queryRent->result();
		
		$properties = array_merge($proposals, $rentals);
		return $properties;
	}
	// End of the function
	
	// Start of get public proposal lists for cash lender
	public function get_proposals_lists_allusers() {
		$properties = array();
		
		$query = $this->db->select("users.*, property_information.*,refi_analysis.*,flip_analysis.*, property_information.PropertyID AS `proposal_id`")
						  ->from("property_information")
						  ->join("users", "users.id=property_information.UserID")
						  ->join("refi_analysis", "refi_analysis.PropertyID=property_information.PropertyID")
						  ->join("flip_analysis", "flip_analysis.PropertyID=property_information.PropertyID")
						  ->where("property_information.Submitted", 'Yes')
						  ->order_by("property_information.InfoID", "desc")
						  ->get();
		$proposals = $query->result();
		
		$query = $this->db->select("*, rental_property_information.PropertyID AS `proposal_id`")
						  ->from("rental_property_information")
						  ->join("users", "users.id=rental_property_information.UserID")
						  ->where("rental_property_information.Submitted", 'Yes')
						  ->order_by("rental_property_information.RentalPropertyID", "desc")
						  ->get();

		$rentals = $query->result();
		
		$properties = array_merge($proposals, $rentals);
		return $properties;
    }
    // End of get public proposal lists for cash lender
	
	
	
	
	// Start of Ajax function for saving data
	public function ajax_store($forms, $table, $prop_id) {
		$this->db->where("PropertyID", $prop_id);
		$this->db->where("CreatedOn", $forms['CreatedOn']);
		$query = $this->db->get($table);

		if ( $table == 'comparable_reports' ) {
			for ( $i = 0; $i < count($forms['SalesAddress']); $i++ ) {
				$data = array(
					'SalesAddress' 	=> $forms['SalesAddress'][$i],
					'Beds' 			=> $forms['Beds'][$i],
					'Baths' 		=> $forms['Baths'][$i],
					'SqFt' 			=> $forms['SqFt'][$i],
					'DateSold' 		=> $forms['DateSold'][$i],
					'SalesPrice' 	=> $forms['SalesPrice'][$i],
					'Notes' 		=> $forms['Notes'][$i],
				);
				
				if ( $query->num_rows() <= 0 ) {
					$data['PropertyID'] = $prop_id;
					$data['UserID'] = $forms['UserID'];
					$data['CreatedOn'] = $forms['CreatedOn'];
					$rs = $this->db->insert($table, $data);
				} else {
					if ( ($i + 1) <= $query->num_rows() ) {
						$this->db->where("ReportID", $forms['ReportID'][$i]);
						$this->db->where("PropertyID", $prop_id);
						$this->db->where("UserID", $forms['UserID']);
						$this->db->where("CreatedOn", $forms['CreatedOn']);
						$rs = $this->db->update($table, $data);
					} elseif ( ($i + 1) > $query->num_rows() ) {
						$data['PropertyID'] = $prop_id;
						$data['UserID'] = $forms['UserID'];
						$data['CreatedOn'] = $forms['CreatedOn'];
						$rs = $this->db->insert($table, $data);
					}
				}
			}
			
		} else {
			foreach ( $forms as $field => $value ) {
				$data[$field] = $value;
			}
			if ( $query->num_rows() > 0 ) {
				$this->db->where("PropertyID", $prop_id);
				$rs = $this->db->update($table, $data);
			} else {
				$data['PropertyID'] = $prop_id;
				$rs = $this->db->insert($table, $data);
			}
		}
		
		if ( $rs ) {
			return 'success';
		} else {
			return 'error';
		}
	}
	// End of the function
	
	// Start of  update property photos
	public function update_property_photos($TableName, $PropertyID, $UserID, $Photos, $Captions, $Covers, $CreatedOn) {
		$this->db->where("PropertyID", $PropertyID);
		$this->db->where("CreatedOn", $CreatedOn);
		$query = $this->db->get($TableName);
		
		if ( $query->num_rows() > 0 ) {
			if ( !empty($Photos) || $Photos != '' ) {
				$data['Photos'] = $Photos;
				$data['Captions'] = $Captions;
				$data['Cover'] = $Covers;
				$this->db->where("PropertyID", $PropertyID);
				$this->db->where("UserID", $UserID);
				$this->db->where("CreatedOn", $CreatedOn);
				$rs = $this->db->update($TableName, $data);
			}
		} else {
			$data['Photos'] = $Photos;
			$data['Captions'] = $Captions;
			$data['Cover'] = $Covers;
			$data['PropertyID']	= $PropertyID;
			$data['UserID'] = $UserID;
			$data['CreatedOn'] = $CreatedOn;
			$rs = $this->db->insert($TableName, $data);
		}
		return $this->db->insert_id();
	}
	// End of the function
	
	// Start of Flip budget insert
	public function FlipBudgetInsert($forms, $LumpSum, $Detailed, $prop_id, $user_id, $created) {
		$this->db->where("PropertyID", $prop_id);
		$this->db->where("CreatedOn", $created);
		$query = $this->db->get('flip_budget');
		
		$PreliWork = array(
			'Demo' => 'Demo',
			'Architectural' => 'Architectural fees',
			'MasterBuildingPermit' => 'Master Building Permit',
			'Plumbing' => '&nbsp;&nbsp;&nbsp;Plumbing',
			'Electrical' => '&nbsp;&nbsp;&nbsp;Electrical',
			'HVAC' => '&nbsp;&nbsp;&nbsp;HVAC',
			'Dumpster' => 'Dumpster',
			'Other' => 'Other',
			'WallFraming' => 'Wall Framing',
			'FloorFraming' => 'Floor Framing',
			'CeilingFraming' => 'Ceiling Framing',
			'InteriorElectrical' => 'Electrical',
			'InteriorPlumbing' => 'Plumbing',
			'InteriorHVAC' => 'HVAC',
			'Flooring' => 'Flooring',
			'Sheetrock' => 'Sheetrock',
			'Windows' => 'Windows',
			'InteriorDoors' => 'Interior Doors',
			'Trim' => 'Trim',
			'BathroomVanities' => 'Bathroom Vanities',
			'BathroomFixtures' => 'Bathroom Fixtures',
			'KitchenCabinets' => 'Kitchen Cabinets',
			'LaborInstallKitchen' => 'Labor to install Kitchen',
			'FloorCoverings1' => 'Floor Coverings 1',
			'FloorCoverings2' => 'Floor Coverings 2',
			'FloorCoverings3' => 'Floor Coverings 3',
			'InteriorPainting' => 'Interior Painting',
			'LightFixtures' => 'Light Fixtures',
			'OtherFixtures' => 'Other Fixtures',
			'Appliances' => 'Appliances',
			'OtherInterior' => 'Other Interior',
			'ExteriorTrim' => 'Exterior Trim',
			'ExteriorDoors' => 'Exterior Doors',
			'Porches' => 'Porches',
			'Siding' => 'Siding',
			'ExteriorPainting' => 'Exterior Painting',
			'Roof' => 'Roof',
			'GuttersDownspouts' => 'Gutters, Downspouts',
			'Fencing' => 'Fencing',
			'Landscaping' => 'Landscaping',
			'DrivewayConcrete' => 'Driveway, concrete work',
			'FoudationWork' => 'Foudation Work',
			'BrickPointingReplacement' => 'Brick Pointing, replacement',
			'OtherExterior' => 'Other Exterior',
			'Contingency' => 'Contingency',
			'GCFee' => 'GCFee',
			'Cleanup' => 'Cleanup',
			'OtherOther' => 'Others',
		);
		$column = array(
			'DetailsNotes',
			'SqFt',
			'Quantity',
			'Rate',
			'Bid1',
			'Bid2',
			'Bid3',
			'Budget',
			'MonthPaid',
		);
		$r = 0; $works = array();
		foreach ( $PreliWork as $key => $work ) {
			$data[$r]['PropertyID'] = $prop_id;
			$data[$r]['UserID'] = $user_id;
			$data[$r]['PreliminaryWork'] = $work;
			$data[$r]['FlipRehabBudgetMethod'] = $LumpSum['FlipRehabBudgetMethod'];
			$data[$r]['FlipRehabDrawSelection'] = $LumpSum['FlipRehabDrawSelection'];
			$data[$r]['LumpSumFlipBudget'] = $LumpSum['LumpSumFlipBudget'];

			for ( $i = 0; $i < count($column); $i++ ) {
				$data[$r][$column[$i]] = $forms[$column[$i] . $key];
			}
			
			$data[$r]['TotalBid1Total'] = $Detailed['TotalBid1Total'];
			$data[$r]['TotalBid2Total'] = $Detailed['TotalBid2Total'];
			$data[$r]['TotalBid3Total'] = $Detailed['TotalBid3Total'];
			$data[$r]['DetailedFlipBudget'] = $Detailed['DetailedFlipBudget'];
			$data[$r]['CreatedOn'] = $created;
			$r++;
		}
		if ( $query->num_rows() == 0 ) {
			for ( $j = 0; $j < count($data); $j++ ) {
				$rs = $this->db->insert('flip_budget', $data[$j]);
			}
		} else {
			for ( $j = 0; $j < count($data); $j++ ) {
				$this->db->where("PropertyID", $prop_id);
				$this->db->where("UserID", $user_id);
				$this->db->where("PreliminaryWork", $data[$j]['PreliminaryWork']);
				$this->db->where("CreatedOn", $created);
				$rs = $this->db->update('flip_budget', $data[$j]);
			}
		}
		
		if ( $rs ) {
			return 'success';
		} else {
			return 'error';
		}
	}
	// End of the function
	
	// Start of Refi budget insert
	public function RefiBudgetInsert($forms, $LumpSum, $Detailed, $prop_id, $user_id, $created) {
		$this->db->where("PropertyID", $prop_id);
		$this->db->where("CreatedOn", $created);
		$query = $this->db->get('refi_budget');
		
		$PreliWork = array(
			'Demo' => 'Demo',
			'Architectural' => 'Architectural fees',
			'MasterBuildingPermit' => 'Master Building Permit',
			'Plumbing' => '&nbsp;&nbsp;&nbsp;Plumbing',
			'Electrical' => '&nbsp;&nbsp;&nbsp;Electrical',
			'HVAC' => '&nbsp;&nbsp;&nbsp;HVAC',
			'Dumpster' => 'Dumpster',
			'Other' => 'Other',
			'WallFraming' => 'Wall Framing',
			'FloorFraming' => 'Floor Framing',
			'CeilingFraming' => 'Ceiling Framing',
			'InteriorElectrical' => 'Electrical',
			'InteriorPlumbing' => 'Plumbing',
			'InteriorHVAC' => 'HVAC',
			'Flooring' => 'Flooring',
			'Sheetrock' => 'Sheetrock',
			'Windows' => 'Windows',
			'InteriorDoors' => 'Interior Doors',
			'Trim' => 'Trim',
			'BathroomVanities' => 'Bathroom Vanities',
			'BathroomFixtures' => 'Bathroom Fixtures',
			'KitchenCabinets' => 'Kitchen Cabinets',
			'LaborInstallKitchen' => 'Labor to install Kitchen',
			'FloorCoverings1' => 'Floor Coverings 1',
			'FloorCoverings2' => 'Floor Coverings 2',
			'FloorCoverings3' => 'Floor Coverings 3',
			'InteriorPainting' => 'Interior Painting',
			'LightFixtures' => 'Light Fixtures',
			'OtherFixtures' => 'Other Fixtures',
			'Appliances' => 'Appliances',
			'OtherInterior' => 'Other Interior',
			'ExteriorTrim' => 'Exterior Trim',
			'ExteriorDoors' => 'Exterior Doors',
			'Porches' => 'Porches',
			'Siding' => 'Siding',
			'ExteriorPainting' => 'Exterior Painting',
			'Roof' => 'Roof',
			'GuttersDownspouts' => 'Gutters, Downspouts',
			'Fencing' => 'Fencing',
			'Landscaping' => 'Landscaping',
			'DrivewayConcrete' => 'Driveway, concrete work',
			'FoudationWork' => 'Foudation Work',
			'BrickPointingReplacement' => 'Brick Pointing, replacement',
			'OtherExterior' => 'Other Exterior',
			'Contingency' => 'Contingency',
			'GCFee' => 'GCFee',
			'Cleanup' => 'Cleanup',
			'OtherOther' => 'Others',
		);
		$column = array(
			'DetailsNotes',
			'SqFt',
			'Quantity',
			'Rate',
			'Bid1',
			'Bid2',
			'Bid3',
			'Budget',
			'MonthPaid',
		);
		$r = 0;
		foreach ( $PreliWork as $key => $work ) {
			$data[$r]['PropertyID'] = $prop_id;
			$data[$r]['UserID'] = $user_id;
			$data[$r]['PreliminaryWork'] = $work;
			$data[$r]['RefiRehabBudgetMethod'] = $LumpSum['RefiRehabBudgetMethod'];
			$data[$r]['RefiRehabDrawSelection'] = $LumpSum['RefiRehabDrawSelection'];
			$data[$r]['LumpSumRefiBudget'] = $LumpSum['LumpSumRefiBudget'];

			for ( $i = 0; $i < count($column); $i++ ) {
				$data[$r][$column[$i]] = $forms[$column[$i] . $key];
			}
			
			$data[$r]['TotalBid1Total'] = $Detailed['TotalBid1Total'];
			$data[$r]['TotalBid2Total'] = $Detailed['TotalBid2Total'];
			$data[$r]['TotalBid3Total'] = $Detailed['TotalBid3Total'];
			$data[$r]['DetailedRefiBudget'] = $Detailed['DetailedRefiBudget'];
			$data[$r]['CreatedOn'] = $created;
			$r++;
		}
		if ( $query->num_rows() == 0 ) {
			for ( $j = 0; $j < count($data); $j++ ) {
				$rs = $this->db->insert('refi_budget', $data[$j]);
			}
		} else {
			for ( $j = 0; $j < count($data); $j++ ) {
				$this->db->where("PropertyID", $prop_id);
				$this->db->where("UserID", $user_id);
				$this->db->where("PreliminaryWork", $data[$j]['PreliminaryWork']);
				$this->db->where("CreatedOn", $created);
				$rs = $this->db->update('refi_budget', $data[$j]);
			}
		}
		
		if ( $rs ) {
			return 'success';
		} else {
			return 'error';
		}
	}
	// End of the function
	
	// Start of max offer calculation report
    public function getOffersDetails($propertyId){
    	$this->db->select("*");
    	$this->db->from("max_offer");
    	$this->db->join("offer_rate", "offer_rate.PropertyID=max_offer.PropertyID");
    	$this->db->where('max_offer.PropertyID', $propertyId);
    	$query = $this->db->get();
    	return $query->row();
    }
	// End of the function
	
	
	// Start of max offer calculation report
    public function getAnalyserDetails($propertyId){
    	$this->db->select("*");
    	$this->db->from("analyser_property_details");
    	$this->db->join("analyser_financing_assumption", "analyser_financing_assumption.PropertyID=analyser_property_details.PropertyID");
		$this->db->join("analyser_extra_details", "analyser_extra_details.PropertyID=analyser_property_details.PropertyID");
    	$this->db->where('analyser_property_details.PropertyID', $propertyId);
    	$query = $this->db->get();
    	return $query->row();
    }
	// End of the function
	
	
	
	
	// Start of full expenses data for edit proposal page
	public function getFullOperatingExpenses($propertyId) {
		$this->db->select("operating_expenses.*");
    	$this->db->from("operating_expenses");
    	$this->db->where("operating_expenses.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->row();
	}
	// End of the function
	
	// Start of full income data for edit proposal page
	public function getFullOperatingIncome($propertyId) {
		$this->db->select("operating_income.*");
    	$this->db->from("operating_income");
    	$this->db->where("operating_income.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->row();
	}
	// End of the function
	
	// Start of full project data for edit proposal page
    public function getFullPropertyData($propertyId){
    	$this->db->select("property_information.*, personal_information.*, purchase_assumption.*, financing_assumption.*, flip_analysis.*, refi_analysis.*, closing_costs.*, holding_costs.*, property_headlines.*, property_photos.*");
    	$this->db->from("property_information");
		$this->db->join("personal_information", 	"property_information.PropertyID=personal_information.PropertyID");
		$this->db->join("purchase_assumption", 		"property_information.PropertyID=purchase_assumption.PropertyID");
		$this->db->join("financing_assumption", 	"property_information.PropertyID=financing_assumption.PropertyID");
    	$this->db->join("flip_analysis", 			"property_information.PropertyID=flip_analysis.PropertyID");
    	$this->db->join("refi_analysis", 			"property_information.PropertyID=refi_analysis.PropertyID");
		$this->db->join("closing_costs", 			"property_information.PropertyID=closing_costs.PropertyID");
		$this->db->join("holding_costs", 			"property_information.PropertyID=holding_costs.PropertyID");
		$this->db->join("property_headlines",		"property_information.PropertyID=property_headlines.PropertyID");
		$this->db->join("property_photos", 			"property_information.PropertyID=property_photos.PropertyID");
    	$this->db->where('property_information.PropertyID', $propertyId);
    	$query = $this->db->get();
    	return $query->row();
    }
	// End of the function
	
	// Start of project summary report
    public function getProjectSummary($propertyId){
    	$this->db->select("financing_assumption.*, purchase_assumption.*, flip_analysis.*, refi_analysis.*, property_information.*");
    	$this->db->from("purchase_assumption");
		$this->db->join("financing_assumption", "purchase_assumption.PropertyID=financing_assumption.PropertyID");
    	$this->db->join("flip_analysis", "purchase_assumption.PropertyID=flip_analysis.PropertyID");
    	$this->db->join("refi_analysis", "purchase_assumption.PropertyID=refi_analysis.PropertyID");
    	$this->db->join("property_information", "purchase_assumption.PropertyID=property_information.PropertyID");
    	$this->db->where('purchase_assumption.PropertyID', $propertyId);
    	$query = $this->db->get();
    	return $query->row();
    }
	// End of the function
	
	// Start of flip rehab budget report
	public function getFlipRehabBudget($propertyId) {
		$this->db->select("flip_budget.*, property_information.*");
    	$this->db->from("flip_budget");
		$this->db->join("property_information", "property_information.PropertyID=flip_budget.PropertyID");
    	$this->db->where("flip_budget.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->result();
	}
	// End of the function
	
	// Start of refi rehab budget report
	public function getRefiRehabBudget($propertyId) {
		$this->db->select("refi_budget.*, property_information.*");
    	$this->db->from("refi_budget");
		$this->db->join("property_information", "property_information.PropertyID=refi_budget.PropertyID");
    	$this->db->where("refi_budget.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->result();
	}
	// End of the function
	
	// Start of cover page report
	public function getCoverPage($propertyId) {
		$this->db->select("property_photos.*, personal_information.*, property_information.*, property_headlines.*");
    	$this->db->from("property_information");
    	$this->db->join("property_photos", "property_photos.PropertyID=property_information.PropertyID");
		$this->db->join("personal_information", "personal_information.PropertyID=property_information.PropertyID");
		$this->db->join("property_headlines", "property_headlines.PropertyID=property_information.PropertyID");
    	$this->db->where("property_information.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->row();
	}
	// End of the function
	
	// Start of flip marketing sheet report
	public function getFlipMarketingShaeet($propertyId) {
		$this->db->select("property_information.*, personal_information.*, purchase_assumption.*, flip_analysis.*, property_photos.*, financing_assumption.*");
    	$this->db->from("flip_analysis");
    	$this->db->join("property_information", "property_information.PropertyID=flip_analysis.PropertyID");
		$this->db->join("personal_information", "personal_information.PropertyID=flip_analysis.PropertyID");
    	$this->db->join("purchase_assumption", "purchase_assumption.PropertyID=flip_analysis.PropertyID");
		$this->db->join("property_photos", "property_photos.PropertyID=flip_analysis.PropertyID");
		$this->db->join("financing_assumption", "financing_assumption.PropertyID=flip_analysis.PropertyID");
    	$this->db->where("flip_analysis.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->row();
	}
	// End of the function
	
	// Start of refi marketing sheet report
	public function getRefiMarketingShaeet($propertyId) {
		$this->db->select("property_information.*, personal_information.*, purchase_assumption.*, refi_analysis.*, property_photos.*, financing_assumption.*");
    	$this->db->from("refi_analysis");
    	$this->db->join("property_information", "property_information.PropertyID=refi_analysis.PropertyID");
		$this->db->join("personal_information", "personal_information.PropertyID=refi_analysis.PropertyID");
    	$this->db->join("purchase_assumption", "purchase_assumption.PropertyID=refi_analysis.PropertyID");
		$this->db->join("property_photos", "property_photos.PropertyID=refi_analysis.PropertyID");
		$this->db->join("financing_assumption", "financing_assumption.PropertyID=refi_analysis.PropertyID");
    	$this->db->where("refi_analysis.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->row();
	}
	// End of the function
	
	// Start of flip cash flow
    public function getFlipCashFlowSummary($propertyId){
    	$this->db->select("property_information.*, personal_information.*, purchase_assumption.*, flip_analysis.*");
    	$this->db->from("flip_analysis");
    	$this->db->join("property_information", "property_information.PropertyID=flip_analysis.PropertyID");
		$this->db->join("personal_information", "personal_information.PropertyID=flip_analysis.PropertyID");
    	$this->db->join("purchase_assumption", "purchase_assumption.PropertyID=flip_analysis.PropertyID");
    	$this->db->where("flip_analysis.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->result();
    }
	// End of the function
	
	// Start of refi cash flow report
    public function getRefiCashFlowSummary($propertyId){
    	$this->db->select("property_information.*, personal_information.*, purchase_assumption.*, refi_analysis.*");
    	$this->db->from("refi_analysis");
    	$this->db->join("property_information", "property_information.PropertyID=refi_analysis.PropertyID");
		$this->db->join("personal_information", "personal_information.PropertyID=refi_analysis.PropertyID");
    	$this->db->join("purchase_assumption", "purchase_assumption.PropertyID=refi_analysis.PropertyID");
    	$this->db->where("refi_analysis.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->result();
    }
	// End of the function
	
	// Start of flip lender funding request report
	public function getFlipLenderFundingRequest($propertyId) {
		$this->db->select("financing_assumption.*, property_information.*, personal_information.*, purchase_assumption.*, flip_analysis.*, property_photos.*");
    	$this->db->from("flip_analysis");
    	$this->db->join("financing_assumption", "financing_assumption.PropertyID=flip_analysis.PropertyID");
		$this->db->join("property_information", "property_information.PropertyID=flip_analysis.PropertyID");
		$this->db->join("personal_information", "personal_information.PropertyID=flip_analysis.PropertyID");
    	$this->db->join("purchase_assumption", "purchase_assumption.PropertyID=flip_analysis.PropertyID");
		$this->db->join("property_photos", "property_photos.PropertyID=flip_analysis.PropertyID");
    	$this->db->where("flip_analysis.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->row();
	}
	// End of the function
	
	// Start of refi lender funding request report
	public function getRefiLenderFundingRequest($propertyId) {
		$this->db->select("financing_assumption.*, property_information.*, personal_information.*, purchase_assumption.*, refi_analysis.*, property_photos.*");
    	$this->db->from("refi_analysis");
    	$this->db->join("financing_assumption", "financing_assumption.PropertyID=refi_analysis.PropertyID");
		$this->db->join("property_information", "property_information.PropertyID=refi_analysis.PropertyID");
		$this->db->join("personal_information", "personal_information.PropertyID=refi_analysis.PropertyID");
    	$this->db->join("purchase_assumption", "purchase_assumption.PropertyID=refi_analysis.PropertyID");		
		$this->db->join("property_photos", "property_photos.PropertyID=refi_analysis.PropertyID");
    	$this->db->where("refi_analysis.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->row();
	}
	// End of the function
	
	// Start of flip lender cash flow report
	public function getFlipLenderCashFlow($propertyId) {
		$this->db->select("property_information.*, personal_information.*, purchase_assumption.*, financing_assumption.*, flip_analysis.*");
    	$this->db->from("flip_analysis");
    	$this->db->join("property_information", "property_information.PropertyID=flip_analysis.PropertyID");
		$this->db->join("personal_information", "personal_information.PropertyID=flip_analysis.PropertyID");
    	$this->db->join("purchase_assumption", "purchase_assumption.PropertyID=flip_analysis.PropertyID");
		$this->db->join("financing_assumption", "financing_assumption.PropertyID=flip_analysis.PropertyID");
    	$this->db->where("flip_analysis.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->result();
	}
	// End of the function
	
	// Start of refi lender cash flow report
	public function getRefiLenderCashFlow($propertyId) {
		$this->db->select("property_information.*, personal_information.*, purchase_assumption.*, financing_assumption.*, refi_analysis.*");
    	$this->db->from("refi_analysis");
    	$this->db->join("property_information", "property_information.PropertyID=refi_analysis.PropertyID");
		$this->db->join("personal_information", "personal_information.PropertyID=refi_analysis.PropertyID");
    	$this->db->join("purchase_assumption", "purchase_assumption.PropertyID=refi_analysis.PropertyID");
		$this->db->join("financing_assumption", "financing_assumption.PropertyID=refi_analysis.PropertyID");
    	$this->db->where("refi_analysis.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->result();
	}
	// End of the function
	
	// Start of additional pics report
	public function getAdditionalPics($propertyId) {
		$this->db->select("property_photos.*, property_information.PropertyStreetAddress, property_information.PropertyCityTown, property_information.PropertyStateProvince, property_information.PropertyZipCode, property_information.PropertyCountry");
    	$this->db->from("property_photos");
    	$this->db->join("property_information", "property_information.PropertyID=property_photos.PropertyID");
    	$this->db->where("property_photos.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->row();
	}
	// End of the function
	
	// Start of comparable sales report
	public function getComparableSales($propertyId) {
		$this->db->select("comparable_reports.*, personal_information.*, property_information.*");
    	$this->db->from("comparable_reports");
    	$this->db->join("personal_information", "personal_information.PropertyID=comparable_reports.PropertyID");
		$this->db->join("property_information", "property_information.PropertyID=comparable_reports.PropertyID");
    	$this->db->where("comparable_reports.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->result();
	}
	// End of get public proposal list
	
	
	// Start of full project data
    public function getFullProjectData($propertyId){
    	$this->db->select("purchase_assumption.*, financing_assumption.*, flip_analysis.*, refi_analysis.*, flip_budget.*, refi_budget.*");
    	$this->db->from("purchase_assumption");
		$this->db->join("financing_assumption", "purchase_assumption.PropertyID=financing_assumption.PropertyID");
    	$this->db->join("flip_analysis", "purchase_assumption.PropertyID=flip_analysis.PropertyID");
    	$this->db->join("refi_analysis", "purchase_assumption.PropertyID=refi_analysis.PropertyID");
		$this->db->join("flip_budget", "purchase_assumption.PropertyID=flip_budget.PropertyID");
		$this->db->join("refi_budget", "purchase_assumption.PropertyID=refi_budget.PropertyID");
    	$this->db->where("purchase_assumption.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->row();
    }
	// End of the function
	
	// Start of full flip budget
    public function getFullFlipBudget($propertyId){
    	$this->db->select("flip_budget.*,property_information.*");
    	$this->db->from("flip_budget");
		$this->db->join("property_information", 	"property_information.PropertyID=flip_budget.PropertyID");
    	$this->db->where("flip_budget.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->result();
    }
	// End of the function
	
	// Start of full refi budget
    public function getFullRefiBudget($propertyId){
    	$this->db->select("refi_budget.*,property_information.*");
    	$this->db->from("refi_budget");
		$this->db->join("property_information", 	"property_information.PropertyID=refi_budget.PropertyID");
    	$this->db->where("refi_budget.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->result();
    }
	// End of the function
	
	// Start of get paid user
	public function getUserpaid($user_id){
		$this->db->select('*');
		$this->db->where('id', $user_id);
		$query = $this->db->get('users');
		return $query->row();
	}
	// End of the function
	
	// Start of getPaymentupdate
	public function getPaymentupdate($user_id, $user_type, $user_firstname, $user_lastname, $payer_email, $receiver_email, $item_name, $subscr_id, $amount, $pay_date) {
		$data = array(
			'user_id'   		=> $user_id,
			'user_type'   		=> $user_type,
			'firstname'   		=> $user_firstname,
			'lastname'       	=> $user_lastname,
			'payer_email'      	=> $payer_email,
			'receiver_email' 	=> $receiver_email,
			'item_name'   		=> $item_name,
			'subscr_id'   		=> $subscr_id,
			'amount'   			=> $amount,
			'payment_date'		=> $pay_date,
		);
		$this->db->insert('payment', $data);
		$insert_id = $this->db->insert_id();
		
		$this->db->where("user_id", $user_id);
		$check = $this->db->get("payment");
		if ( $check->num_rows() == 2 ) {
			$this->db->select("*");
			$this->db->where("user_id", $user_id);
			$this->db->where("amount", '97.00');
			$query = $this->db->get("payment");
			
			$values = $query->row();
			$prevData = array(
				'user_id'   		  => $values->user_id,
				'firstname'   		  => $values->firstname,
				'lastname'       	  => $values->lastname,
				'subscription_id'	  => $values->subscr_id,
				'subscription_amount' => $values->amount,
				'subscription_date'	  => $values->payment_date,
			);
			$this->db->insert('previouspayment', $prevData);
			
			// delete previous payment data from payment table
			$this->db->where('user_id', $user_id);
			$this->db->where("amount", '97.00');
			$this->db->delete('payment');
		}
		return $insert_id;
	}
	// End of the function
	
	// Start of getPaymentDetails
	public function getPaymentDetails($user_id){
		$this->db->select("*");
		$this->db->where("user_id", $user_id);
		$this->db->order_by("id", "desc");
		$query = $this->db->get("payment");
		return $query->row();
	}
	// End of function
	
	// Start of getUserstatusChange
	public function getUserstatusChange($user_id, $paidtype) {
		$data = array('paidstatus' => '1', 'paidtype' => $paidtype);
		$this->db->where('id', $user_id);
		$this->db->update('users', $data);
	}
	// End of the function	
	
	// Start of get proposal completed
	public function get_proposal_completed($UserID){
		$this->db->select("*");
		$this->db->from("property_information");
		$this->db->where("UserID", $UserID);
		$this->db->order_by("PropertyID", "desc");
		$query = $this->db->get();
		return $query->result();
	}
	// End of the function
	
	// Start of get proposal completed
	public function get_rental_completed($UserID){
		$this->db->select("*");
		$this->db->from("rental_property_information");
		$this->db->where("UserID", $UserID);
		$this->db->order_by("PropertyID", "desc");
		$query = $this->db->get();
		return $query->result();
	}
	// End of the function
	
	// Start of get proposal completed
	public function get_offer_completed($UserID){
		$this->db->select("*");
		$this->db->from("max_offer");
		$this->db->where("UserID", $UserID);
		$this->db->order_by("PropertyID", "desc");
		$query = $this->db->get();
		return $query->result();
	}
	// End of the function
	
	// Start of get proposal completed
	public function get_comp_completed($UserID){
		$this->db->select("*");
		$this->db->from("analyser_property_details");
		$this->db->where("UserID", $UserID);
		$this->db->order_by("PropertyID", "desc");
		$query = $this->db->get();
		return $query->result();
	}
	// End of the function
	
	// Start of getting proposal photos by proposal id
	public function get_rental_photos($proposalId) {
		$this->db->where("PropertyID", $proposalId);
		$query = $this->db->get("rental_property_photos");
		return $query->row();
    }
	// End of the function
	
	//Start of Rental Single Property
	public function get_single_property($propertyId) {
		$this->db->select("rental_property_information.*, rental_operating_income.*, rental_operating_expenses.*, rental_price_assumptions.*, rental_holding_resale.*, rental_tax_assumption.*, rental_financial_analysis.*, rental_cash_outlay.*, rental_helpful_calculator.*, rental_helpful_calculator1.*, rental_property_photos.*, rental_headlines.*, rental_advance.*");
		$this->db->from("rental_property_information");
		$this->db->join("rental_operating_income", 		"rental_operating_income.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_operating_expenses", 	"rental_operating_expenses.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_price_assumptions", 	"rental_price_assumptions.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_holding_resale", 		"rental_holding_resale.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_tax_assumption", 		"rental_tax_assumption.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_financial_analysis", 	"rental_financial_analysis.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_cash_outlay", 			"rental_cash_outlay.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_helpful_calculator", 	"rental_helpful_calculator.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_helpful_calculator1", 	"rental_helpful_calculator1.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_property_photos", 		"rental_property_photos.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_headlines", 			"rental_headlines.PropertyID=rental_property_information.PropertyID");
		$this->db->join("rental_advance", 				"rental_advance.PropertyID=rental_property_information.PropertyID");
		$this->db->where("rental_property_information.PropertyID", $propertyId);
    	$query = $this->db->get();
    	return $query->row();
	}
	// End of the function
	
	public function submit_proposal_network($proposal_id, $user_id, $section) {
		if ( $section == 'proposals' ) {
			$table = 'property_information';
		} elseif ( $section == 'rentals' ) {
			$table = 'rental_property_information';
		}
		
		$this->db->where("PropertyID", $proposal_id);
		$this->db->where("UserID", $user_id);
		$query = $this->db->get($table);
		$value = $query->row();

		if ( $value->Submitted == 'No' ) {
			$data['Submitted'] = 'Yes';
		} else {
			$data['Submitted'] = 'No';
		}
		
		$this->db->where("PropertyID", $proposal_id);
		$this->db->where("UserID", $user_id);
		$this->db->update($table, $data);
		return $data['Submitted'];
		
	}
	// End of the function
	
	public function update_property_refiflip($property_id, $user_id, $status) {
		$data = array(
			'FlipOrRefi' => $status,
		);
		$this->db->where("PropertyID", $property_id);
		$this->db->where("UserID", $user_id);
		$this->db->update('property_information', $data);
		return $status;
	}
	// End of the function
	
	public function remove_photo($PhotoID, $Photo, $Table) {
		if ( $PhotoID ) {
			if ( $Table == 'property_photos' ) {
				$this->db->where("PhotoID", $PhotoID);
			} else {
				$this->db->where("RentalPhotoID", $PhotoID);
			}
			$query = $this->db->get($Table);
			$value = $query->row();
			
			$Photos 	= $value->Photos;
			$Captions 	= $value->Captions;
			$Covers		= $value->Cover;
			
			$PhotoArray = explode('|', $Photos);
			$CaptionArray = explode('||||', $Captions);
			$CoverArray = explode('|', $Covers);
			
			$PhotoKey = array_search($Photo, $PhotoArray);
			
			unset($PhotoArray[$PhotoKey]);
			unset($CaptionArray[$PhotoKey]);
			unset($CoverArray[$PhotoKey]);
			
			$PhotoString = implode('|', $PhotoArray);
			$CaptionString = implode('||||', $CaptionArray);
			$CoverString = implode('|', $CoverArray);
			
			$data = array(
				'Photos' => $PhotoString,
				'Captions' => $CaptionString,
				'Cover' => $CoverString,
			);
			if ( $Table == 'property_photos' ) {
				$this->db->where("PhotoID", $PhotoID);
			} else {
				$this->db->where("RentalPhotoID", $PhotoID);
			}
			$this->db->update($Table, $data);
		}
	}
	// End of the function
}

/* End of file pages_model.php */
/* Location: ./application/models/pages_model.php */