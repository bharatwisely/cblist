<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

define('FINANCIAL_ACCURACY', 1.0e-6);
define('FINANCIAL_MAX_ITERATIONS', 100);

function PMT($ir, $np, $pv, $fv, $type) {
	$pmt; $pvif;
	$fv || ($fv = 0);
	$type || ($type = 0);
	
	if ($ir === 0)
		return -($pv + $fv)/$np;
		
	$pvif = pow(1 + $ir, $np);
	$pmt = - $ir * $pv * ($pvif + $fv) / ($pvif - 1);
	
	if ($type === 1)
		$pmt /= (1 + $ir);
	
	return $pmt;
}

function IRR($values, $guess = 0.1)	{
	if (!is_array($values)) return null;
	
	$x1 = 0.0;
	$x2 = $guess;
	$f1 = NPV($x1, $values);
	$f2 = NPV($x2, $values);
	for ($i = 0; $i < 100; $i++) {
		if (($f1 * $f2) < 0.0) break;
		if (abs($f1) < abs($f2)) {
			$f1 = NPV($x1 += 1.6 * ($x1 - $x2), $values);
		} else {
			$f2 = NPV($x2 += 1.6 * ($x2 - $x1), $values);
		}
	}
	if (($f1 * $f2) > 0.0) return null;
	
	$f = NPV($x1, $values);
	if ($f < 0.0) {
		$rtb = $x1;
		$dx = $x2 - $x1;
	} else {
		$rtb = $x2;
		$dx = $x1 - $x2;
	}
	
	for ($i = 0;  $i < 100; $i++) {
		$dx *= 0.5;
		$x_mid = $rtb + $dx;
		$f_mid = NPV($x_mid, $values);
		if ($f_mid <= 0.0) $rtb = $x_mid;
		if ((abs($f_mid) < FINANCIAL_ACCURACY) || (abs($dx) < FINANCIAL_ACCURACY)) return $x_mid;
	}
	return null;
}
	
function NPV($rate, $values) {
	if (!is_array($values)) return null;
	
	$npv = 0.0;
	
	for ($i = 0; $i < count($values); $i++) {
		$npv += $values[$i] / pow(1 + $rate, $i + 1);
	}
	
	return (is_finite($npv) ? $npv: null);
}

function MIRR($values, $finance_rate, $reinvert_rate) {
	$n = count($values);
	for ($i = 0, $npv_pos = $npv_neg = 0; $i < $n; $i++) {
		$v = $values[$i];
		if ($v >= 0)
			$npv_pos += $v / pow(1.0 + $reinvert_rate, $i);
		else
			$npv_neg += $v / pow(1.0 + $finance_rate, $i);
	}
	
	if (($npv_neg == 0) || ($npv_pos == 0) || ($reinvert_rate <= -1))
		return null;
		
	$mirr = pow((-$npv_pos * pow(1 + $reinvert_rate, $n)) / ($npv_neg * (1 + $reinvert_rate)), (1.0 / ($n - 1))) - 1.0;
	return (is_finite($mirr) ? $mirr: null);
}

function RANK($value, $array, $order = 0) {
	if ($order) sort ($array); else rsort($array);
	array_unshift($array, $value+1); 
	$keys = array_keys($array, $value);
	if (count($keys) == 0) return NULL;
	return array_sum($keys) / count($keys);
}

function AVERAGE($array) {
    $count = count($array);
	$total = 0;
    foreach ($array as $value) {
        $total = $total + $value;
    }
    $average = ($total/$count);
    return round($average, 2);
}

function MEDIAN($array) {
    sort($array);
    $count = count($array);
    $middleval = floor(($count-1)/2);
    if($count % 2) {
        $median = $array[$middleval];
    } else {
        $low = $array[$middleval];
        $high = $array[$middleval+1];
        $median = (($low+$high)/2);
    }
    return $median;
}

function  country_select($name, $id, $class, $selected_country, $top_countries=array(), $all, $selection=NULL, $show_all=TRUE){
	$countries = config_item('country_list');
	$html = "<select name='{$name}' id='{$id}' class='{$class}'>";
	$selected = NULL;
	if ( in_array($selection, $top_countries) ) {
		$top_selection = $selection;
		$all_selection = NULL;
	} else {
		$top_selection = NULL;
		$all_selection = $selection;
	}
	if ( !empty($selected_country) && $selected_country != 'all' && $selected_country != 'select' ) {
		$html .= "<optgroup label='Selected Country'>";
		if ( $selected_country === $top_selection ) {
			$selected = "SELECTED";
		}
		$html .= "<option value='{$selected_country}'{$selected}>{$countries[$selected_country]}</option>";
		$selected = NULL;
		$html .= "</optgroup>";
	} else if ( $selected_country == 'all' ) {
		$html .= "<optgroup label='Selected Country'>";
		if ( $selected_country === $top_selection ) {
			$selected = "SELECTED";
		}
		$html .= "<option value='all'>All</option>";
		$selected = NULL;
		$html .= "</optgroup>";
	} else if ( $selected_country == 'select' ) {
		$html .= "<optgroup label='Selected Country'>";
		if ( $selected_country === $top_selection ) {
			$selected = "SELECTED";
		}
		$html .= "<option value='select'>Select</option>";
		$selected = NULL;
		$html .= "</optgroup>";
	}
	if ( !empty($all) && $all == 'all' && $selected_country != 'all' ) {
		$html .= "<option value='all'>All</option>";
		$selected = NULL;
	}
	if ( !empty($all) && $all == 'select' && $selected_country != 'select' ) {
		$html .= "<option value='select'>Select</option>";
		$selected = NULL;
	}
	if ( !empty($top_countries) ) {
		$html .= "<optgroup label='Default Countries'>";
		foreach ( $top_countries as $value ) {
			if ( array_key_exists($value, $countries) ) {
				if ( $value === $top_selection ) {
					$selected = "SELECTED";
				}
				$html .= "<option value='{$value}'{$selected}>{$countries[$value]}</option>";
				$selected = NULL;
			}
		}
		$html .= "</optgroup>";
	}
	
	if ( $show_all ) {
		$html .= "<optgroup label='All Countries'>";
		foreach ( $countries as $key => $country ) {
			if ( $key === $all_selection ) {
				$selected = "SELECTED";
			}
			$html .= "<option value='{$key}'{$selected}>{$country}</option>";
			$selected = NULL;
		}
		$html .= "</optgroup>";
	}
	
	$html .= "</select>";
	return $html;
}