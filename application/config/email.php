<?php defined('BASEPATH') OR exit('No direct script access allowed.');

$server = $_SERVER['HTTP_HOST'];
if ( $server == 'localhost' ) {
	
	define('SendFrom', 'info@cblist.com');
	
	$config['useragent']        = 'PHPMailer';
	$config['protocol']         = 'smtp';
	$config['mailpath']         = '/usr/sbin/sendmail';
	$config['smtp_host']        = 'smtp.gmail.com';
	$config['smtp_user']        = 'dev.surajit.dev@gmail.com';
	$config['smtp_pass']        = 'surajit123';
	$config['smtp_port']        = 465;
	$config['smtp_timeout']     = 5;
	$config['smtp_crypto']      = 'ssl';
	$config['smtp_debug']       = 0;
	$config['wordwrap']         = true;
	$config['wrapchars']        = 50;
	$config['mailtype']         = 'html';
	$config['charset']          = 'utf-8';
	$config['validate']         = true;
	$config['priority']         = 3;
	$config['crlf']             = "\r\n";
	$config['newline']          = "\r\n";
	$config['bcc_batch_mode']   = false;
	$config['bcc_batch_size']   = 200;
	$config['encoding']         = '8bit';
	
} else {
	
	define('SendFrom', 'info@soumitrapaul.com');
	
	$config['useragent']        = 'PHPMailer';
	$config['protocol']         = 'sendmail';
	$config['mailpath']         = '/usr/sbin/sendmail';
	$config['smtp_host']        = 'smtp.gmail.com';
	$config['smtp_user']        = 'dev.surajit.dev@gmail.com';
	$config['smtp_pass']        = 'surajit123';
	$config['smtp_port']        = 465;
	$config['smtp_timeout']     = 5;
	$config['smtp_crypto']      = 'ssl';
	$config['smtp_debug']       = 0;
	$config['wordwrap']         = true;
	$config['wrapchars']        = 50;
	$config['mailtype']         = 'html';
	$config['charset']          = 'utf-8';
	$config['validate']         = true;
	$config['priority']         = 3;
	$config['crlf']             = "\r\n";
	$config['newline']          = "\r\n";
	$config['bcc_batch_mode']   = false;
	$config['bcc_batch_size']   = 200;
	$config['encoding']         = '8bit';
	
}