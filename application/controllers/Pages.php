<?php if ( !defined('BASEPATH') ) exit('No direct script access allowed'); 

class Pages extends CI_Controller {
	
	// Start of Constructor
    public function __construct() {
        parent::__construct();

		$this->load->model('pages_model');
		
        $this->load->library('image_lib');
        $this->load->library('linkedin');
		$this->load->library('email');
		$this->load->library('dompdf_lib');
		$this->load->library('infusionsoft');
    }
	// End of the function
	
    // Start of Home Page
    public function index() {
		$data['user_id'] = $this->session->userdata('user_id');
        $data['page_title'] = 'Home Page >> CBLists';
        $this->load->view('template/header', $data);
        $this->load->view('view_home', $data);
        $this->load->view('template/footer', $data);
    }
    // End of the function
	
	
	
	// ================================================================== SIGNUP & LOGIN ==================================================================== //	
    // Start of Investors Sign Up Page
    public function investors_signup() {
        $this->logged_in();
        $data['page_title'] = 'Investors Sign Up >> CBLists';

        $data['errorL'] = ''; $data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
        if ($this->session->userdata('emailExists') == TRUE) {
            $data['errorL'] = "Your linkedin account email is already registered manually.<br />Try to use another linkedin account for registering.";
            $_SESSION['access_token'] = '';
            $_SESSION['expires_in'] = '';
            $_SESSION['expires_at'] = '';
            unset($_SESSION['access_token']);
            unset($_SESSION['expires_in']);
            unset($_SESSION['expires_at']);
        } elseif ($this->session->userdata('roleExists') == TRUE) {
            $data['errorL'] = "You are a cashlender, please login from <a href='" . base_url('pages/cashlenders-login') . "/'>cashlender login page</a>.";
            $_SESSION['access_token'] = '';
            $_SESSION['expires_in'] = '';
            $_SESSION['expires_at'] = '';
            unset($_SESSION['access_token']);
            unset($_SESSION['expires_in']);
            unset($_SESSION['expires_at']);
        }

        // Start of email registration script
        if ($this->input->post('email_submit')) {
            $this->form_validation->set_rules('username', 'Name', 'trim|required|min_length[4]|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
            if ($this->form_validation->run() == FALSE) {
                $data['error'] = 'Please correct the following errors.';
            } else {
                $data['reset'] = TRUE;
                if ($this->pages_model->check_user_exists($this->input->post('email')) == 0) {
                    // Auto generated random password
                    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
                    $password = substr(str_shuffle($chars), 0, 8);
					
                    //Email sending code
                    $message  = '<p>Hi ' . $this->input->post('username') . ',</p><br />';
                    $message .= '<p>Thank you for registering with us.</p>';
                    $message .= '<p>Below is your login credentials:</p>';
                    $message .= '<p>Username/Email: <strong>' . $this->input->post('email') . '</strong></p>';
                    $message .= '<p>Password: <strong>' . $password . '</strong></p>';
					$message .= '<p>Please keep this info safe and secure.</p>';
					$message .= '<p>You can change your password any time from your CBList account settings.</p><br />';
					$message .= '<p>Thank you,</p>';
					$message .= '<p>CBList team</p>';
					$message .= '<p><a href="' . base_url() . '">' . base_url() . '</a></p>';

                    $this->email->set_newline("\r\n");
                    $this->email->from(SendFrom, 'CBList Property');
                    $this->email->to($this->input->post('email'), $this->input->post('username'));
                    $this->email->subject('Registration verification');
                    $this->email->message($message);
                    if ($this->email->send()) {
                        $user_data = array(
                            'linkedin'			 => '',
                            'email'				 => $this->input->post('email'),
                            'phone'				 => 'Not updated',
                            'password'			 => sha1($password),
                            'fname'				 => $this->input->post('username'),
                            'dob'				 => 'Not updated',
                            'picture'			 => base_url('assets/images/default-pic.jpg'),
                            'picture_medium'	 => base_url('assets/images/default-pic.jpg'),
                            'picture_small'		 => base_url('assets/images/default-pic-small.jpg'),
                            'street'			 => 'Not updated',
							'town'				 => 'Not updated',
							'state'				 => 'Not updated',
							'zip'				 => 'Not updated',
                            'country'			 => 'Not updated',
                            'facebook_url'		 => 'Not updated',
                            'twitter_url'	 	 => 'Not updated',
                            'linkedin_url'	 	 => 'Not updated',
                            'recommenders'	 	 => 0,
                            'user_type'		 	 => 'investor',
							'user_experience'	 => 0,
							'profile_completion' => 'no',
							'paidstatus'		 => '0',
							'paidtype'			 => 'N',
                        );
                        $this->pages_model->do_register($user_data);
						// infusionsoft
						$name = $this->input->post('username');
						$infuData = array(
							'FirstName' => $name,
							'LastName'	=> '',
							'Email'		=> $this->input->post('email'),
						);
						$this->infusionsoft->InsertInfusion($infuData);
						// infusionsoft
                        $data['success'] = 'Visit your email to get your login credentials';
                        $data['reset'] = TRUE;
                    } else {
                        show_error($this->email->print_debugger());
                    }
                    //Email code ends here					
                } else {
                    $data['error'] = 'The email you entered is already registered.';
                }
            }
        }
        // End of email registration script

        $this->load->view('template/header', $data);
        $this->load->view('view_investors_signup', $data);
        $this->load->view('template/footer', $data);
        $this->session->unset_userdata(array('emailExists' => ''));
		$this->session->unset_userdata(array('roleExists' => ''));
        $this->session->unset_userdata(array('signup' => ''));
    }
    // End of the function
	
    // Start of Cash Lenders Sign Up Page
    public function cashlenders_signup() {
        $this->logged_in();
        $data['page_title'] = 'Cash Lenders Sign Up >> CBLists';

        $data['errorL'] = ''; $data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
        if ($this->session->userdata('emailExists') == TRUE) {
            $data['errorL'] = "Your linkedin account email is already registered manually.<br />Try to use another linkedin account for registering.";
            $_SESSION['access_token'] = '';
            $_SESSION['expires_in'] = '';
            $_SESSION['expires_at'] = '';
            unset($_SESSION['access_token']);
            unset($_SESSION['expires_in']);
            unset($_SESSION['expires_at']);
        } elseif ($this->session->userdata('roleExists') == TRUE) {
            $data['errorL'] = "You are an investor, please login from <a href='" . base_url('pages/investors-login') . "/'>investors login page</a>.";
            $_SESSION['access_token'] = '';
            $_SESSION['expires_in'] = '';
            $_SESSION['expires_at'] = '';
            unset($_SESSION['access_token']);
            unset($_SESSION['expires_in']);
            unset($_SESSION['expires_at']);
        }

        // Start of email registration script
        if ($this->input->post('email_submit')) {
            $this->form_validation->set_rules('username', 'Name', 'trim|required|min_length[4]|xss_clean');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|is_unique[users.email]');
            if ($this->form_validation->run() == FALSE) {
                $data['error'] = 'Please correct the following errors.';
            } else {
                $data['reset'] = TRUE;
                if ($this->pages_model->check_user_exists($this->input->post('email')) == 0) {
                    // Auto generated random password
                    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
                    $password = substr(str_shuffle($chars), 0, 8);
					
                    //Email sending code
					$message  = '<p>Hi ' . $this->input->post('username') . ',</p><br />';
                    $message .= '<p>Thank you for registering with us.</p>';
                    $message .= '<p>Below is your login credentials:</p>';
                    $message .= '<p>Username/Email: <strong>' . $this->input->post('email') . '</strong></p>';
                    $message .= '<p>Password: <strong>' . $password . '</strong></p>';
					$message .= '<p>Please keep this info safe and secure.</p>';
					$message .= '<p>You can change your password any time from your CBList account settings.</p><br />';
					$message .= '<p>Thank you,</p>';
					$message .= '<p>CBList team</p>';
					$message .= '<p><a href="' . base_url() . '">' . base_url() . '</a></p>';
					
                    $this->email->set_newline("\r\n");
                    $this->email->from(SendFrom, 'CBList Property');
                    $this->email->to($this->input->post('email'), $this->input->post('username'));
                    $this->email->subject('Registration verification');
                    $this->email->message($message);
                    if ($this->email->send()) {
                        $user_data = array(
                            'linkedin' 			 => '',
                            'email' 			 => $this->input->post('email'),
                            'phone' 			 => 'Not updated',
                            'password' 			 => sha1($password),
                            'fname' 			 => $this->input->post('username'),
                            'dob' 				 => 'Not updated',
                            'picture' 			 => base_url('assets/images/default-pic.jpg'),
                            'picture_medium'	 => base_url('assets/images/default-pic.jpg'),
                            'picture_small' 	 => base_url('assets/images/default-pic-small.jpg'),
                            'street'			 => 'Not updated',
							'town'				 => 'Not updated',
							'state'				 => 'Not updated',
							'zip'				 => 'Not updated',
                            'country'			 => 'Not updated',
                            'facebook_url' 		 => 'Not updated',
                            'twitter_url'		 => 'Not updated',
                            'linkedin_url'		 => 'Not updated',
                            'recommenders'		 => 0,
                            'user_type'	 		 => 'cash_lender',
							'user_experience'	 => 0,
							'profile_completion' => '',
							'paidstatus'		 => '1',
							'paidtype'			 => 'H',
                        );
                        $this->pages_model->do_register($user_data);
						// infusionsoft
						$name = $this->input->post('username');
						$infuData = array(
							'FirstName' => $name,
							'LastName'	=> '',
							'Email'		=> $this->input->post('email'),
						);
						$this->infusionsoft->InsertInfusion($infuData);
						// infusionsoft
                        $data['success'] = 'Visit your email to get your login credentials';
                        $data['reset'] = TRUE;
                    } else {
                        show_error($this->email->print_debugger());
                    }
                    //Email code ends here					
                } else {
                    $data['error'] = 'The email you entered is already registered.';
                }
            }
        }
        // End of email registration script

        $this->load->view('template/header', $data);
        $this->load->view('view_cashlenders_signup', $data);
        $this->load->view('template/footer', $data);
        $this->session->unset_userdata(array('emailExists' => ''));
		$this->session->unset_userdata(array('roleExists' => ''));
        $this->session->unset_userdata(array('signup' => ''));
    }
    // End of the function
	
    // Start of Investors Sign In Page
    public function investors_login() {
        $this->logged_in();
        $data['page_title'] = 'Investors Log in >> CBLists';

        $data['errorL'] = ''; $data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
        if ($this->session->userdata('emailExists') == TRUE) {
            $data['errorL'] = "Your linkedin account email is registered manually.<br />Please use below form to sign into your account.";
            $_SESSION['access_token'] = '';
            $_SESSION['expires_in'] = '';
            $_SESSION['expires_at'] = '';
            unset($_SESSION['access_token']);
            unset($_SESSION['expires_in']);
            unset($_SESSION['expires_at']);
        } elseif ($this->session->userdata('roleExists') == TRUE) {
            $data['errorL'] = "You are a cashlender, please login from <a href='" . base_url('pages/cashlenders-login') . "/'>cashlender login page</a>.";
            $_SESSION['access_token'] = '';
            $_SESSION['expires_in'] = '';
            $_SESSION['expires_at'] = '';
            unset($_SESSION['access_token']);
            unset($_SESSION['expires_in']);
            unset($_SESSION['expires_at']);
        }

        // Start of email login script
        if ($this->input->post('email_submit')) {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $data['error'] = 'Please correct the following errors.';
            } else {
                $data['reset'] = TRUE;
                $email = $this->input->post('email');
				$password = sha1($this->input->post('password'));
                if ($this->pages_model->do_login($email, $password, 'investor') == 1) {
					$user_id = $this->session->userdata('user_id');
					if ($this->pages_model->check_user_profile_exists($user_id) == 0) {
						redirect(base_url('pages/profile') . '/');
						return FALSE;
					} elseif ($this->pages_model->check_user_portfolio_exists($user_id) == 0) {
						redirect(base_url('pages/new-portfolio') . '/');
						return FALSE;
					} elseif ($this->pages_model->check_user_proposal_exists($user_id) == 0) {
						redirect(base_url('pages/proposal-writer') . '/');
						return FALSE;
					} else {
						redirect(base_url('pages/dashboard') . '/');
						return FALSE;
					}
                } else {
                    $data['error'] = 'The email or password you entered is incorrect.';
                }
            }
        }
        // End of email login script

        $this->load->view('template/header', $data);
        $this->load->view('view_investors_login', $data);
        $this->load->view('template/footer', $data);
        $this->session->unset_userdata(array('emailExists' => ''));
		$this->session->unset_userdata(array('roleExists' => ''));
        $this->session->unset_userdata(array('signin' => ''));
    }
    // End of the function
	
    // Start of Cash Lenders Sign In Page
    public function cashlenders_login() {
        $this->logged_in();
        $data['page_title'] = 'Cash Lenders Log in >> CBLists';

        $data['errorL'] = ''; $data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
        if ($this->session->userdata('emailExists') == TRUE) {
            $data['errorL'] = "Your linkedin account email is registered manually.<br />Please use below form to sign into your account.";
            $_SESSION['access_token'] = '';
            $_SESSION['expires_in'] = '';
            $_SESSION['expires_at'] = '';
            unset($_SESSION['access_token']);
            unset($_SESSION['expires_in']);
            unset($_SESSION['expires_at']);
        } elseif ($this->session->userdata('roleExists') == TRUE) {
            $data['errorL'] = "You are an investor, please login from <a href='" . base_url('pages/investors-login') . "/'>investors login page</a>.";
            $_SESSION['access_token'] = '';
            $_SESSION['expires_in'] = '';
            $_SESSION['expires_at'] = '';
            unset($_SESSION['access_token']);
            unset($_SESSION['expires_in']);
            unset($_SESSION['expires_at']);
        }

        // Start of email login script
        if ($this->input->post('email_submit')) {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $data['error'] = 'Please correct the following errors.';
            } else {
                $data['reset'] = TRUE;
                $email = $this->input->post('email');
                $password = sha1($this->input->post('password'));
                if ($this->pages_model->do_login($email, $password, 'cash_lender') == 1) {
					$user_id = $this->session->userdata('user_id');
					if ($this->pages_model->check_user_profile_exists($user_id) == 0) {
						redirect(base_url('pages/profile') . '/');
						return FALSE;
					} else {
						redirect(base_url('pages/investors-proposals') . '/');
						return FALSE;
					}
                } else {
                    $data['error'] = 'The email or password you entered is incorrect.';
                }
            }
        }
        // End of email login script

        $this->load->view('template/header', $data);
        $this->load->view('view_cashlenders_login', $data);
        $this->load->view('template/footer', $data);
        $this->session->unset_userdata(array('emailExists' => ''));
		$this->session->unset_userdata(array('roleExists' => ''));
        $this->session->unset_userdata(array('signin' => ''));
    }
    // End of the function
	
	// Start of forgot password
	public function forgot_password() {
		$this->logged_in();
        $data['page_title'] = 'Forgot Password >> CBLists';

        $data['errorL'] = ''; $data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
        if ($this->input->post('email_submit')) {
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            if ($this->form_validation->run() == FALSE) {
                $data['error'] = 'Please correct the following errors.';
            } else {
                $data['reset'] = TRUE;
                $email = $this->input->post('email');
                if ( $this->pages_model->check_user_exists($email) == 1 ) {
					$user = $this->pages_model->get_user_by_email($email);
					
					// Auto generated random password
                    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
                    $password = substr(str_shuffle($chars), 0, 8);

                    //Email sending code
                    $message  = '<p>Hi ' . $user->fname . ',</p><br />';
                    $message .= '<p>You can login with the following credentials:</p>';
                    $message .= '<p>Below is your new login credentials:</p>';
                    $message .= '<p>Username/Email: <strong>' . $email . '</strong></p>';
                    $message .= '<p>Password: <strong>' . $password . '</strong></p>';
					$message .= '<p>Please keep this info safe and secure.</p><br />';
					$message .= '<p>You can login from the below link:</p>';
					if ( $user->user_type == 'investor' ) {
						$message .= '<p><a href="' . base_url('pages/investors-login') . '/">' . base_url('pages/investors-login') . '/</a></p><br />';
					} elseif ( $user->user_type == 'cash_lender' ) {
						$message .= '<p><a href="' . base_url('pages/cashlenders-login') . '/">' . base_url('pages/cashlenders-login') . '/</a></p><br />';
					}
					$message .= '<p>Thank you,</p>';
					$message .= '<p>CBList team</p>';
					$message .= '<p><a href="' . base_url() . '">' . base_url() . '</a></p>';

                    $this->email->set_newline("\r\n");
                    $this->email->from(SendFrom, 'CBList Property');
                    $this->email->to($email, $user->fname);
                    $this->email->subject('New Password from CBList');
                    $this->email->message($message);
                    if ($this->email->send()) {
						$this->pages_model->update_password($password, $email);
                        $data['success'] = 'Please visit your email to get new login credentials';
                        $data['reset'] = TRUE;
                    } else {
                        show_error($this->email->print_debugger());
                    }
                    //Email code ends here	
                } else {
                    $data['error'] = 'Your email is not registered.';
                }
            }
        }

        $this->load->view('template/header', $data);
        $this->load->view('view_forgot_password', $data);
        $this->load->view('template/footer', $data);
	}
	// End of the function
	
	
	
	// ================================================================== PROFILE & ACCOUNTS ================================================================== //	
    // Start of Profile Page
    public function profile() {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $data['page_title'] = 'My Profile >> CBLists';
		
		$user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
        $data['error'] = '';  $data['success'] = ''; $data['image_error'] = '';
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->form_validation->set_rules('sales_pitch', 'Sales Pitch', 'trim|required|max_length[500]|xss_clean');
            $this->form_validation->set_rules('about_you', 'About You', 'trim|required|max_length[500]|xss_clean');
            $this->form_validation->set_rules('about_investment', 'Investment', 'trim|required|max_length[500]|xss_clean');

            if (isset($_FILES['picture']) && !empty($_FILES['picture']['name'])) {
				$file_name = $user_id . '_' . time() . rand(1, 988);
                $config['upload_path'] = FCPATH . 'assets/picture/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
                $config['max_size'] = '1024';
                $config['max_width'] = '0';
                $config['max_height'] = '0';
                $config['file_name'] = $file_name;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                
                if ($this->upload->do_upload('picture')) {
                    $upload_data = $this->upload->data();
                    $ext = pathinfo($upload_data['orig_name'], PATHINFO_EXTENSION);
                    $picture = base_url('assets/picture/' . $upload_data['orig_name']);

                    // Create 200x200 thumbnail
                    $crop_config_1['image_library'] = 'gd2';
                    $crop_config_1['source_image'] = $upload_data["full_path"];
                    $crop_config_1['new_image'] = $upload_data["file_path"] . $file_name . '-200x200.' . $ext;
                    $crop_config_1['quality'] = "100%";
                    $crop_config_1['maintain_ratio'] = TRUE;
                    $crop_config_1['create_thumb'] = TRUE;
                    $crop_config_1['thumb_marker'] = '';
                    $crop_config_1['width'] = 200;
                    $crop_config_1['height'] = 200;

                    $this->image_lib->initialize($crop_config_1);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $picture_medium = base_url('assets/picture/' . $file_name . '-200x200.' . $ext);

                    // Create 80x80 thumbnail
                    $crop_config_2['image_library'] = 'gd2';
                    $crop_config_2['source_image'] = $upload_data["full_path"];
                    $crop_config_2['new_image'] = $upload_data["file_path"] . $file_name . '-80x80.' . $ext;
                    $crop_config_2['quality'] = "100%";
                    $crop_config_2['maintain_ratio'] = TRUE;
                    $crop_config_2['create_thumb'] = TRUE;
                    $crop_config_2['thumb_marker'] = '';
                    $crop_config_2['width'] = 80;
                    $crop_config_2['height'] = 80;

                    $this->image_lib->initialize($crop_config_2);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $picture_small = base_url('assets/picture/' . $file_name . '-80x80.' . $ext);

                    $this->pages_model->update_profile_picture($user_id, $picture, $picture_medium, $picture_small);
                } else {
                    $data['image_error'] = $this->upload->display_errors();
                }
            }

            if ($this->form_validation->run() == FALSE || $data['image_error']) {
                $data['error'] = TRUE;
            } else {
                $data['success'] = 'Your profile is successfully updated.';
                $this->pages_model->update_profile_data($user_id, $user_details[0]->user_type);
            }
        }

        if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $profile_details = $this->pages_model->get_user_profile($user_id);
            $data['profile'] = $profile_details[0];
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
		if ( $user_details[0]->user_type == 'investor' ) {
			$profile_data = array(
				'SalesPitch'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->sales_pitch : '',
					'score'	=> 5,
				),
				'AboutYourself'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->about_you : '',
					'score'	=> 5,
				),
				'AboutInvestment'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->about_investment : '',
					'score'	=> 5,
				),
				'WorkVideo'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->youtube_link : '',
					'score'	=> 10,
				),
				'ProfilePic'	=> array(
					'name'	=> isset($user_details) ? strpos($user_details[0]->picture, 'default-pic.jpg') !== false ? '' : 'Completed' : '',
					'score'	=> 5,
				),
				'DOB'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->dob : '',
					'score'	=> 5,
				),
				'ContactNo'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->phone : '',
					'score'	=> 5,
				),
				'AddressCountry'	=> array(
					'name'	=> isset($user_details) ? ($user_details[0]->street != 'Not updated' && $user_details[0]->town != 'Not updated' && $user_details[0]->state != 'Not updated' && $user_details[0]->zip != 'Not updated' && $user_details[0]->country != 'Not updated') ? 'Completed' : '' : '',
					'score'	=> 5,
				),
				'LinkedinURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->linkedin_url : '',
					'score'	=> 5,
				),
				'FacebookURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->facebook_url : '',
					'score'	=> 5,
				),
				'TwitterURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->twitter_url : '',
					'score'	=> 5,
				),
				'Portfolio'	=> array(
					'name'	=> isset($user_details) ? $this->pages_model->check_user_portfolio_exists($user_id) : '',
					'score'	=> 20,
				),
				'Proposal'	=> array(
					'name'	=> isset($user_details) ? $this->pages_model->check_user_proposal_exists($user_id) : '',
					'score'	=> 20,
				),
			);
		} elseif ( $user_details[0]->user_type == 'cash_lender' ) {
			$profile_data = array(
				'SalesPitch'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->sales_pitch : '',
					'score'	=> 10,
				),
				'AboutYourself'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->about_you : '',
					'score'	=> 10,
				),
				'AboutInvestment'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->about_investment : '',
					'score'	=> 10,
				),
				'ProfilePic'	=> array(
					'name'	=> isset($user_details) ? strpos($user_details[0]->picture, 'default-pic.jpg') !== false ? '' : 'Completed' : '',
					'score'	=> 10,
				),
				'DOB'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->dob : '',
					'score'	=> 10,
				),
				'ContactNo'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->phone : '',
					'score'	=> 10,
				),
				'AddressCountry'	=> array(
					'name'	=> isset($user_details) ? ($user_details[0]->street != 'Not updated' && $user_details[0]->town != 'Not updated' && $user_details[0]->state != 'Not updated' && $user_details[0]->zip != 'Not updated' && $user_details[0]->country != 'Not updated') ? 'Completed' : '' : '',
					'score'	=> 10,
				),
				'LinkedinURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->linkedin_url : '',
					'score'	=> 10,
				),
				'FacebookURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->facebook_url : '',
					'score'	=> 10,
				),
				'TwitterURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->twitter_url : '',
					'score'	=> 10,
				),
			);
		}
		$data['completeness'] = $this->profile_completeness($profile_data);
		
		$user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];

        $this->load->view('template/header', $data);
        $this->load->view('view_profile', $data);
        $this->load->view('template/footer', $data);
    }
    // End of the function
	
    // Start of Public Profile Page
    public function profiles($public_id, $public_name) {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $public_id = $public_id;
        $public_user = $this->pages_model->get_public_user($public_id);
        if (empty($public_user) || !$public_name) {
            redirect(base_url('pages/profile') . '/');
            return FALSE;
        }
        $data['public_user'] = $public_user[0];
		$public_portfolio = $this->pages_model->get_portfolio_lists($public_id);
        $data['public_portfolio'] = $public_portfolio;
        $data['page_title'] = $public_user[0]->fname . ' >> CBLists';

        if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $profile_details = $this->pages_model->get_user_profile($user_id);
            $data['profile'] = $profile_details[0];
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];

        $this->load->view('template/header', $data);
        $this->load->view('view_public_profile', $data);
        $this->load->view('template/footer', $data);
    }
    // End of the function
	
    // Start of Account Page
    public function accounts() {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		$data['countries'] = config_item('country_list');
		
        $data['page_title'] = 'Account Settings >> CBLists';

        $data['error'] = ''; $data['success'] = ''; $data['image_error'] = ''; $data['linked_in_url_error'] = '';
        $data['facebook_url_error'] = ''; $data['twitter_url_error'] = ''; $data['phone_number_error'] = '';
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->form_validation->set_rules('fname', 'Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('dob', 'Date of Birth', 'trim|required|xss_clean');
            if ( $this->input->post('password') ) {
                $this->form_validation->set_rules('password', 'Password', 'trim|required|matches[passwordC]|xss_clean');
                $this->form_validation->set_rules('passwordC', 'Retype Password', 'trim|required|xss_clean');
            }
            $this->form_validation->set_rules('street', 'Street', 'trim|required|xss_clean');
			$this->form_validation->set_rules('town', 'Town', 'trim|required|xss_clean');
			$this->form_validation->set_rules('state', 'State', 'trim|required|xss_clean');
			$this->form_validation->set_rules('zip', 'ZIP', 'trim|required|xss_clean');
            $this->form_validation->set_rules('country', 'Country', 'trim|required|xss_clean');

            $linkedin_url = $this->input->post('linkedin_url') ? $this->input->post('linkedin_url') : 'Not updated';
            $facebook_url = $this->input->post('facebook_url') ? $this->input->post('facebook_url') : 'Not updated';
            $twitter_url = $this->input->post('twitter_url') ? $this->input->post('twitter_url') : 'Not updated';
            $phone_number = $this->input->post('phone') ? $this->input->post('phone') : 'Not updated';

            if (isset($_FILES['picture']) && !empty($_FILES['picture']['name'])) {
                $file_name = $user_id . '_' . time() . rand(1, 988);
                $config['upload_path'] = FCPATH . 'assets/picture/';
                $config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
                $config['max_size'] = '1024';
                $config['max_width'] = '0';
                $config['max_height'] = '0';
                $config['file_name'] = $file_name;
                $config['overwrite'] = TRUE;
                $this->upload->initialize($config);
                $this->load->library('upload', $config);
                
                if ($this->upload->do_upload('picture')) {
                    $upload_data = $this->upload->data();
                    $ext = pathinfo($upload_data['orig_name'], PATHINFO_EXTENSION);
                    $picture = base_url('assets/picture/' . $upload_data['orig_name']);

                    // Create 200x200 thumbnail
                    $crop_config_1['image_library'] = 'gd2';
                    $crop_config_1['source_image'] = $upload_data["full_path"];
                    $crop_config_1['new_image'] = $upload_data["file_path"] . $file_name . '-200x200.' . $ext;
                    $crop_config_1['quality'] = "100%";
                    $crop_config_1['maintain_ratio'] = TRUE;
                    $crop_config_1['create_thumb'] = TRUE;
                    $crop_config_1['thumb_marker'] = '';
                    $crop_config_1['width'] = 200;
                    $crop_config_1['height'] = 200;

                    $this->image_lib->initialize($crop_config_1);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $picture_medium = base_url('assets/picture/' . $file_name . '-200x200.' . $ext);

                    // Create 80x80 thumbnail
                    $crop_config_2['image_library'] = 'gd2';
                    $crop_config_2['source_image'] = $upload_data["full_path"];
                    $crop_config_2['new_image'] = $upload_data["file_path"] . $file_name . '-80x80.' . $ext;
                    $crop_config_2['quality'] = "100%";
                    $crop_config_2['maintain_ratio'] = TRUE;
                    $crop_config_2['create_thumb'] = TRUE;
                    $crop_config_2['thumb_marker'] = '';
                    $crop_config_2['width'] = 80;
                    $crop_config_2['height'] = 80;

                    $this->image_lib->initialize($crop_config_2);
                    $this->image_lib->resize();
                    $this->image_lib->clear();

                    $picture_small = base_url('assets/picture/' . $file_name . '-80x80.' . $ext);

                    $this->pages_model->update_profile_picture($user_id, $picture, $picture_medium, $picture_small);
                } else {
                    $data['image_error'] = $this->upload->display_errors();
                }
            }

            if ($this->form_validation->run() == FALSE || $data['image_error']) {
                $data['error'] = TRUE;
            } else {

                $linkedin_url_pattern = '/^(?!www\.)[A-Za-z0-9_-]+\.+[A-Za-z0-9.\/%&=\?_:;-]+$/';
                $facebook_url_pattern = '/^(?!www\.)[A-Za-z0-9_-]+\.+[A-Za-z0-9.\/%&=\?_:;-]+$/';
                $twitter_url_pattern  = '/^(?!www\.)[A-Za-z0-9_-]+\.+[A-Za-z0-9.\/%&=\?_:;-]+$/';
                $phone_number_pattern = '/^[789]\d{9}$/';
				if ( $linkedin_url != 'Not updated' && $linkedin_url != false ) {
	                if ( !preg_match($linkedin_url_pattern, $linkedin_url) || strpos($linkedin_url, 'linkedin.com') === false ) {
    	                $data['error'] = TRUE;
        	            $data['linked_in_url_error'] = TRUE;
            	        $data['linked_in_url_error'] = 'Please enter valid Linkedin profile link';
	                }
				}
				
				if ( $facebook_url != 'Not updated' && $facebook_url != false ) {
					if ( !preg_match($facebook_url_pattern, $facebook_url) || strpos($facebook_url, 'facebook.com') === false ) {
						$data['error'] = TRUE;
						$data['facebook_url_error'] = TRUE;
						$data['facebook_url_error'] = 'Please enter valid Facebook profile link';
					}
				}
				
				if ( $twitter_url != 'Not updated' && $twitter_url != false ) {
					if ( !preg_match($twitter_url_pattern, $twitter_url) || strpos($twitter_url, 'twitter.com') === false ) {
						$data['error'] = TRUE;
						$data['twitter_url_error'] = TRUE;
						$data['twitter_url_error'] = 'Please enter valid Twitter profile link';
					}
				}
				
				if ( $phone_number != 'Not updated' && $phone_number != false ) {
	                if (!preg_match($phone_number_pattern, $phone_number)) {
    	                $data['error'] = TRUE;
        	            $data['phone_number_error'] = TRUE;
            	        $data['phone_number_error'] = 'Please enter valid 10 digit Mobile number';
					}
                }
                
				if ( !$data['error'] ) {
                    $data['success'] = 'Your account is successfully updated.';
                    $this->pages_model->update_account_data($user_id);
                }
            }
        }

        if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $profile_details = $this->pages_model->get_user_profile($user_id);
            $data['profile'] = $profile_details[0];
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

		if ( $user_details[0]->user_type == 'investor' ) {
			$profile_data = array(
				'SalesPitch'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->sales_pitch : '',
					'score'	=> 5,
				),
				'AboutYourself'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->about_you : '',
					'score'	=> 5,
				),
				'AboutInvestment'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->about_investment : '',
					'score'	=> 5,
				),
				'WorkVideo'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->youtube_link : '',
					'score'	=> 10,
				),
				'ProfilePic'	=> array(
					'name'	=> isset($user_details) ? strpos($user_details[0]->picture, 'default-pic.jpg') !== false ? '' : 'Completed' : '',
					'score'	=> 5,
				),
				'DOB'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->dob : '',
					'score'	=> 5,
				),
				'ContactNo'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->phone : '',
					'score'	=> 5,
				),
				'AddressCountry'	=> array(
					'name'	=> isset($user_details) ? ($user_details[0]->street != 'Not updated' && $user_details[0]->town != 'Not updated' && $user_details[0]->state != 'Not updated' && $user_details[0]->zip != 'Not updated' && $user_details[0]->country != 'Not updated') ? 'Completed' : '' : '',
					'score'	=> 5,
				),
				'LinkedinURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->linkedin_url : '',
					'score'	=> 5,
				),
				'FacebookURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->facebook_url : '',
					'score'	=> 5,
				),
				'TwitterURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->twitter_url : '',
					'score'	=> 5,
				),
				'Portfolio'	=> array(
					'name'	=> isset($user_details) ? $this->pages_model->check_user_portfolio_exists($user_id) : '',
					'score'	=> 20,
				),
				'Proposal'	=> array(
					'name'	=> isset($user_details) ? $this->pages_model->check_user_proposal_exists($user_id) : '',
					'score'	=> 20,
				),
			);
		} elseif ( $user_details[0]->user_type == 'cash_lender' ) {
			$profile_data = array(
				'SalesPitch'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->sales_pitch : '',
					'score'	=> 10,
				),
				'AboutYourself'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->about_you : '',
					'score'	=> 10,
				),
				'AboutInvestment'	=> array(
					'name'	=> isset($profile_details) ? $profile_details[0]->about_investment : '',
					'score'	=> 10,
				),
				'ProfilePic'	=> array(
					'name'	=> isset($user_details) ? strpos($user_details[0]->picture, 'default-pic.jpg') !== false ? '' : 'Completed' : '',
					'score'	=> 10,
				),
				'DOB'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->dob : '',
					'score'	=> 10,
				),
				'ContactNo'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->phone : '',
					'score'	=> 10,
				),
				'AddressCountry'	=> array(
					'name'	=> isset($user_details) ? ($user_details[0]->street != 'Not updated' && $user_details[0]->town != 'Not updated' && $user_details[0]->state != 'Not updated' && $user_details[0]->zip != 'Not updated' && $user_details[0]->country != 'Not updated') ? 'Completed' : '' : '',
					'score'	=> 10,
				),
				'LinkedinURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->linkedin_url : '',
					'score'	=> 10,
				),
				'FacebookURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->facebook_url : '',
					'score'	=> 10,
				),
				'TwitterURL'	=> array(
					'name'	=> isset($user_details) ? $user_details[0]->twitter_url : '',
					'score'	=> 10,
				),
			);
		}
		$data['completeness'] = $this->profile_completeness($profile_data);
		
		$user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];

        $this->load->view('template/header', $data);
        $this->load->view('view_account', $data);
        $this->load->view('template/footer', $data);
    }
    // End of the function
	
	// Start of profile completeness function
	private function profile_completeness($data) {
		$maximumPoints  = 100;
		$hasCompleted = 0;
		foreach ( $data as $value ) {
			if ( $value['name'] != false && $value['name'] !== 'Not updated') {
				$hasCompleted += $value['score'];
			}
		}
		$percentage = ($hasCompleted * $maximumPoints) / 100;
		return $percentage;
	}
	// End of the function
	
	
	
	// ================================================================== INVESTOR PORTFOLIO MANAGER =========================================================== //	
    // Start of New Portfolio Page        
    public function new_portfolio() {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $data['page_title'] = 'Create Portfolio >> CBLists';

        $data['error'] = ''; $data['success'] = ''; $data['reset'] = ''; $data['image_error'] = '';
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->form_validation->set_rules('property_name', 'Property Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('property_add', 'Property location', 'trim|required|xss_clean');
            //$this->form_validation->set_rules('youtube_link', 'youtube link', 'trim|required|xss_clean');
            $this->form_validation->set_rules('info_one', 'info one', 'trim|required|min_length[32]|xss_clean');
            $this->form_validation->set_rules('info_two', 'info two', 'trim|xss_clean');
			
			$portfolio_pic = '';
			$files = $_FILES;
			$cpt = count($_FILES['portfolio_pic']['name']);
			for ($i = 0; $i < $cpt; $i++) {
				$_FILES['portfolio_pic']['name'] = $files['portfolio_pic']['name'][$i];
				$_FILES['portfolio_pic']['type'] = $files['portfolio_pic']['type'][$i];
				$_FILES['portfolio_pic']['tmp_name'] = $files['portfolio_pic']['tmp_name'][$i];
				$_FILES['portfolio_pic']['error'] = $files['portfolio_pic']['error'][$i];
				$_FILES['portfolio_pic']['size'] = $files['portfolio_pic']['size'][$i];

				$config['upload_path'] = FCPATH . 'assets/portfolio/';
				$config['file_name'] = $user_id . '_' . time() . rand(1, 988);
				$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
				$config['max_size'] = '2048';
				$config['max_width'] = '0';
				$config['max_height'] = '0';

				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				if ($this->upload->do_upload('portfolio_pic')) {
					$upload_data = $this->upload->data();
					$portfolio_pic .= base_url('assets/portfolio/' . $upload_data['orig_name']) . '|';
				}
			}
			
			if ($this->upload->display_errors() == '<p>You did not select a file to upload.</p>') {
				$data['image_error'] = '';
			} else {
				$data['image_error'] = $this->upload->display_errors();
			}
			
            if ($this->form_validation->run() == FALSE || $data['image_error']) {
                $data['error'] = TRUE;
            } else {
                $data['success'] = 'New portfolio is successfully created.';
                $data['reset'] = TRUE;
				$portfolio_pic = substr($portfolio_pic, 0, -1);
                $this->pages_model->add_portfolio_data($user_id, $portfolio_pic);
            }
        }

        if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];

        $this->load->view('template/header', $data);
        $this->load->view('view_new_portfolio', $data);
        $this->load->view('template/footer', $data);
    }
    // End of the function
	
	// Start of Investor Portfolio Page        
    public function investor_portfolio($id, $section) {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $portfolio_id = $id;
		$single = $this->pages_model->get_portfolio($portfolio_id);
     
        $data['page_title'] = 'Investor Portfolio >> CBLists';
		$data['profile'] = TRUE;
        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
        $data['single'] = $single[0];
		
		if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $this->load->view('template/header', $data);
        $this->load->view('view_investor_portfolio', $data);
        $this->load->view('template/footer', $data);
    }
    // End of the function
	
    // Start of Edit Portfolio Page        
    public function edit_portfolio($id, $section) {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $portfolio_id = $id;
        $single = $this->pages_model->get_portfolio($portfolio_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/portfolio') . '/');
            return FALSE;
        }

        $data['page_title'] = 'Edit Portfolio >> CBLists';

        $data['error'] = ''; $data['success'] = ''; $data['reset'] = ''; $data['image_error'] = '';
        if ($this->input->server('REQUEST_METHOD') === 'POST') {
            $this->form_validation->set_rules('property_name', 'Property Name', 'trim|required|xss_clean');
            $this->form_validation->set_rules('property_add', 'Property location', 'trim|required|xss_clean');
            //$this->form_validation->set_rules('youtube_link', 'youtube link', 'trim|required|xss_clean');
            $this->form_validation->set_rules('info_one', 'info one', 'trim|required|min_length[32]|xss_clean');
            $this->form_validation->set_rules('info_two', 'info two', 'trim|xss_clean');

			if ($this->input->post('all_pic')) {
				$all_pic = ltrim($this->input->post('all_pic'), '|');
				$all_pic = rtrim($all_pic, '|');
				$new_portfolio_pic = $all_pic . '|';
			} else {
				$new_portfolio_pic = '';
			}

			
			$files = $_FILES;
			$cpt = count($_FILES['portfolio_pic']['name']);
			for ($i = 0; $i < $cpt; $i++) {
				$_FILES['portfolio_pic']['name'] = $files['portfolio_pic']['name'][$i];
				$_FILES['portfolio_pic']['type'] = $files['portfolio_pic']['type'][$i];
				$_FILES['portfolio_pic']['tmp_name'] = $files['portfolio_pic']['tmp_name'][$i];
				$_FILES['portfolio_pic']['error'] = $files['portfolio_pic']['error'][$i];
				$_FILES['portfolio_pic']['size'] = $files['portfolio_pic']['size'][$i];

				$config['upload_path'] = FCPATH . 'assets/portfolio/';
				$config['file_name'] = $user_id . '_' . time() . rand(1, 988);
				$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
				$config['max_size'] = '2048';
				$config['max_width'] = '0';
				$config['max_height'] = '0';

				$this->upload->initialize($config);
				$this->load->library('upload', $config);
				if ($this->form_validation->run() == TRUE) {
					if ($this->upload->do_upload('portfolio_pic')) {
						$upload_data = $this->upload->data();
						$new_portfolio_pic .= base_url('assets/portfolio/' . $upload_data['orig_name']) . '|';
					}
				}
			}
			
			if ($this->upload->display_errors() == '<p>You did not select a file to upload.</p>') {
				$data['image_error'] = '';
			} else {
				$data['image_error'] = $this->upload->display_errors();
			}

            if ($this->form_validation->run() == FALSE || $data['image_error']) {
                $data['error'] = TRUE;
            } else {
                $this->session->set_userdata(array('portfolio_updated' => TRUE));
                $data['success'] = TRUE;
                $data['reset'] = TRUE;
                if ($this->input->post('del_pic')) {
                    $del_img = rtrim($this->input->post('del_pic'), '|');
                    $del_img = ltrim($del_img, '|');
                    $del_imgs = explode('|', $del_img);
                    foreach ($del_imgs as $dimg) {
                        $file_name = explode('/', $dimg);
                        $file_name = end($file_name);
                        unlink(FCPATH . 'assets/portfolio/' . $file_name);
                    }
                }
                $new_portfolio_pic = ltrim($new_portfolio_pic, '|');
                $new_portfolio_pic = rtrim($new_portfolio_pic, '|');
                $this->pages_model->update_portfolio_data($portfolio_id, $new_portfolio_pic);
                redirect(base_url('pages/edit-portfolio/' . $id . '/portfolio') . '/');
                return FALSE;
            }
        }

        if ($this->session->userdata('portfolio_updated') == TRUE) {
            $data['success'] = 'Portfolio is successfully updated.';
        }

        if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
        $data['single'] = $single[0];

        $this->load->view('template/header', $data);
        $this->load->view('view_edit_portfolio', $data);
        $this->load->view('template/footer', $data);
        $this->session->set_userdata(array('portfolio_updated' => FALSE));
    }
    // End of the function
	
    // Start of Delete Portfolio Page
    public function delete_portfolio($id, $section) {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $portfolio_id = $id;
        $single = $this->pages_model->get_portfolio($portfolio_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/portfolio') . '/');
            return FALSE;
        }
        $images = explode('|', $single[0]->images);
        foreach ($images as $img) {
            $file_name = explode('/', $img);
            $file_name = end($file_name);
            unlink(FCPATH . 'assets/portfolio/' . $user_id . '/' . $file_name);
        }
		$this->pages_model->delete_portfolio($portfolio_id);
        $this->session->set_userdata(array('portfolio_deleted' => TRUE));
        redirect(base_url('pages/edit-portfolio/' . $id . '/portfolio') . '/');
        return FALSE;
    }
    // End of the function
	
    // Start of New Portfolio Page        
    public function portfolio() {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $data['page_title'] = 'Portfolio >> CBLists';

        if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $portfolio_details = $this->pages_model->get_portfolio_lists($user_id);
            $data['portfolio'] = $portfolio_details;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];

        $this->load->view('template/header', $data);
        $this->load->view('view_portfolio', $data);
        $this->load->view('template/footer', $data);
        $this->session->set_userdata(array('portfolio_deleted' => ''));
    }
    // End of the function
	
	
	
	// ================================================================= INVESTOR PROPOSAL MANAGER ============================================================= //	
    // Start of Proposals Page        
    public function proposals() {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
		if ( $this->pages_model->check_usertype(NULL, $user_id) == 'investor' ) {
			if ($this->pages_model->check_user_profile_exists($user_id) == 0) {
				redirect(base_url('pages/profile') . '/');
				return FALSE;
			} elseif ($this->pages_model->check_user_portfolio_exists($user_id) == 0) {
				redirect(base_url('pages/portfolio') . '/');
				return FALSE;
			}
		}

        $data['page_title'] = 'Proposals >> CBLists';
		$data['countries'] = config_item('country_list');

        if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
		$offer_details = $this->pages_model->get_max_offer_lists($user_id);
        $data['offers'] = $offer_details;
		
        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];

        $this->load->view('template/header', $data);
        $this->load->view('view_proposals', $data);
        $this->load->view('template/footer', $data);
		$this->session->unset_userdata('proposals_deleted');
		$this->session->unset_userdata('proposal_submitted');
    }
    // End of the function
	
	// Start of Proposal from max offer
	public function offer_to_proposal($offer_id, $section) {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
		
        $single = $this->pages_model->get_offer($offer_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/proposals') . '/');
            return FALSE;
        }

        $data['page_title'] = 'Proposal from Max Offer >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		$data['PropertyID'] = $offer_id;
		$data['CreatedOn'] = date('Y-m-d H:i:s');
		
		$data['fields'] = $single[0];

		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_max_rehab', $data);
        $this->load->view('template/footer', $data);
	}
	// End of the function
	
	// Start of Proposal Writer function
	public function proposal_writer() {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $data['page_title'] = 'Proposal Writer >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		$data['PropertyID'] = time();
		$data['CreatedOn'] = date('Y-m-d H:i:s');

		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_rehab_valuator', $data);
        $this->load->view('template/footer', $data);
	}
	// End Proposal Writer function
	
	// Start of Proposal Writer function
	public function edit_proposal($id, $section) {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
		
		$proposal_id = $id;
        $single = $this->pages_model->get_proposal($proposal_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/proposals') . '/');
            return FALSE;
        }

        $data['page_title'] = 'Edit Proposal >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		$data['PropertyID'] = $proposal_id;
		$data['CreatedOn'] = $single[0]->CreatedOn;
		
		$data['fields'] = $this->pages_model->getFullPropertyData($proposal_id);
		$data['flip'] = $this->pages_model->getFullFlipBudget($proposal_id);
		$data['refi'] = $this->pages_model->getFullRefiBudget($proposal_id);
		$data['expense'] = $this->pages_model->getFullOperatingExpenses($proposal_id);
		$data['income'] = $this->pages_model->getFullOperatingIncome($proposal_id);
		$data['sales'] = $this->pages_model->getComparableSales($proposal_id);

		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_edit_rehab', $data);
        $this->load->view('template/footer', $data);
	}
	// End Proposal Writer function
	
	// Start of proposal submission to network
	public function submit_proposal($id, $section) {
		$this->not_logged_in();
            
        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
            
        $proposal_id = $id;
        $single = $this->pages_model->get_proposal($proposal_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/proposals') . '/');
            return FALSE;
        }

		$submit = $this->pages_model->submit_proposal_network($proposal_id, $user_id, $section);

        $this->session->set_userdata(array('proposal_submitted' => $submit));
        redirect(base_url('pages/proposals') . '/');
        return FALSE;		
	}
	// End of the function
	
	// Start of Delete proposal Page
    public function delete_proposal($id, $section) {
        $this->not_logged_in();
            
        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
            
        $proposal_id = $id;
        $single = $this->pages_model->get_proposal($proposal_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/proposals') . '/');
            return FALSE;
        }
		$photos = $this->pages_model->get_proposal_photos($proposal_id);
		if ( !empty($photos) ) {
			$images = explode('|', $photos->Photos);
			foreach ($images as $img) {
				$file_name = explode('/', $img);
				$file_name = end($file_name);
				unlink(FCPATH . 'assets/proposals/' . $file_name);
			}
		}
		$this->pages_model->delete_proposal($proposal_id);
        $this->session->set_userdata(array('proposals_deleted' => TRUE));
        redirect(base_url('pages/proposals') . '/');
        return FALSE;
    }
    // End of the function
	
	
	
	// ==================================================================== OFFER CALCULATOR MANAGER ============================================================ //
	// Start of Rehab Max Offers function
	public function max_offers() {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $data['page_title'] = 'Max Offers >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
		$offer_details = $this->pages_model->get_max_offer_lists($user_id);
        $data['offers'] = $offer_details;

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_max_offers', $data);
        $this->load->view('template/footer', $data);
		$this->session->unset_userdata('offer_deleted');
	}
	// End Max Offers function
	
	// Start of Offer Calculator function
	public function offer_calculator() {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $data['page_title'] = 'Offer Calculator >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		$data['PropertyID'] = time();
		
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_offer_calculator', $data);
        $this->load->view('template/footer', $data);
	}
	// End Offer Calculator function
	
	// Start of Edit max offer Page
    public function edit_offer($id, $section) {
        $this->not_logged_in();
            
        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
            
        $offer_id = $id;
        $single = $this->pages_model->get_offer($offer_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/max-offers') . '/');
            return FALSE;
        }
		
		$data['page_title'] = 'Offer Calculator >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		$data['max'] = $this->pages_model->get_full_max_offer($offer_id);
		
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_edit_offer', $data);
        $this->load->view('template/footer', $data);
    }
    // End of the function
	
	// Start of Delete max offer Page
    public function delete_offer($id, $section) {
        $this->not_logged_in();
            
        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
            
        $offer_id = $id;
        $single = $this->pages_model->get_offer($offer_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/max-offers') . '/');
            return FALSE;
        }
        $this->pages_model->delete_offer($offer_id);
        $this->session->set_userdata(array('offer_deleted' => TRUE));
        redirect(base_url('pages/max-offers') . '/');
        return FALSE;
    }
    // End of the function
	
	
	
	// ========================================================================== DASHBOARD PAGE ================================================================== //
    // Start of Dashboard Page
    public function dashboard() {
        $this->not_logged_in();

		$user_id = $this->session->userdata('user_id');
		if ( $this->pages_model->check_usertype(NULL, $user_id) == 'investor' ) {
			if ($this->pages_model->check_user_profile_exists($user_id) == 0) {
				redirect(base_url('pages/profile') . '/');
				return FALSE;
			} elseif ($this->pages_model->check_user_portfolio_exists($user_id) == 0) {
				redirect(base_url('pages/portfolio') . '/');
				return FALSE;
			} elseif ($this->pages_model->check_user_proposal_exists($user_id) == 0) {
				redirect(base_url('pages/proposal') . '/');
				return FALSE;
			}
		} else {
			redirect(base_url('pages/profile') . '/');
			return FALSE;
		}
		$data['page_title'] = 'Dashboard >> CBLists';
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
			$data['profile'] = TRUE;
		} else {
			$data['profile'] = FALSE;
		}
		if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
			$data['portfolio'] = TRUE;
		} else {
			$data['portfolio'] = FALSE;
		}
		
		if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
		$user_details = $this->pages_model->get_user($user_id);
		$data['user'] = $user_details[0];
		$data['proposal_completed'] = $this->pages_model->get_proposal_completed($user_id);
		$data['rental_completed'] = $this->pages_model->get_rental_completed($user_id);
		$data['offer_completed'] = $this->pages_model->get_offer_completed($user_id);
		$data['comp_completed'] = $this->pages_model->get_comp_completed($user_id);
		$this->load->view('template/header', $data);
		$this->load->view('view_dashboard', $data);
		$this->load->view('template/footer', $data);
	}
    // End of the function
	
	
	
	// =================================================================== CASHLENDER PROPOSAL LISTS ============================================================= //
	// Start of Investors Proposals Page
    public function investors_proposals() {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');

        $data['page_title'] = 'Investor Portfolio >> CBLists';
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
			$data['profile'] = TRUE;
		} else {
			$data['profile'] = FALSE;
		}
		if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
			$data['portfolio'] = TRUE;
		} else {
			$data['portfolio'] = FALSE;
		}
		
		if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
		$user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		if ( isset($_POST['filter_submit']) ) {
			$InvestorLocation = $this->input->post('InvestorLocation');
			$LoanValue = $this->input->post('LoanValue');
			$PropertyLocation = $this->input->post('PropertyLocation');
			$RepairValue = $this->input->post('RepairValue');
			
			$data['InvestorLocation'] = $InvestorLocation;
			$data['LoanValue'] = $LoanValue;
			$data['PropertyLocation'] = $PropertyLocation;
			$data['RepairValue'] = $RepairValue;
			
			$data['proposals'] = $this->pages_model->proposal_filter_results($InvestorLocation, $LoanValue, $PropertyLocation, $RepairValue);
		} else {
			$data['InvestorLocation'] = '';
			$data['LoanValue'] = '';
			$data['PropertyLocation'] = '';
			$data['RepairValue'] = '';
			
			$proposal_details = $this->pages_model->get_proposals_lists_allusers();
			$data['proposals'] = $proposal_details;
		}

        $this->load->view('template/header', $data);
        $this->load->view('view_investors_proposals', $data);
        $this->load->view('template/footer', $data);
	}
	// End of the function
	
	// Start of Public Proposal Page
	public function proposal($proposal_id, $proposal_title) {
		$this->not_logged_in();
		
		$enc_proposal_id=$proposal_id;
		$user_id = $this->session->userdata('user_id');
		$this->check_profile_update($user_id);
		
		$proposal_id = $proposal_id;
		$public_proposal = $this->pages_model->get_single_proposal($proposal_id);
		
		if (empty($public_proposal) || !$proposal_title) {
			redirect(base_url('pages/investors-proposals') . '/');
			return FALSE;
		}
		$data['single_proposal'] = $public_proposal[0];
		
		$data['page_title'] = $public_proposal[0]->PropertyName . ' >> CBLists';
		
		$user_details = $this->pages_model->get_user($user_id);
		$data['user'] = $user_details[0];
		
		if ( isset($_POST['accept_confirm']) ) {
			$investor_id = $public_proposal[0]->UserID;
			$proposal_view_status = $this->pages_model->get_cashlender_proposal_view($investor_id, $user_id, $proposal_id);
			if ( $proposal_view_status == 'notviewed') {
				$proposal_view_status = $this->pages_model->set_cashlender_proposal_view($investor_id, $user_id, $proposal_id);
				$list = array (
					array('Name: ', 		 $user_details[0]->fname),
					array('Email: ', 		 $user_details[0]->email),
					array('Street: ', 		 $user_details[0]->street),
					array('Town: ', 		 $user_details[0]->town),
					array('State: ', 		 $user_details[0]->state),
					array('ZIP: ', 		 	 $user_details[0]->zip),
					array('Country: ', 		 $user_details[0]->country),
					array('Phone: ', 		 $user_details[0]->phone),
					array('Property Name: ', $public_proposal[0]->PropertyName),
					array('', ''),
				);
				$fp = fopen(FCPATH . 'assets/csv/' . $investor_id . '-' . $user_id . '.csv', 'a');
				foreach ($list as $fields) {
					fputcsv($fp, $fields);
				}
				fclose($fp);
			}
		}
		
		if ( $this->pages_model->check_user_profile_exists($user_id) == 1 ) {
			$data['profile'] = TRUE;
		} else {
			$data['profile'] = FALSE;
		}
		
		if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
			$data['portfolio'] = TRUE;
		} else {
			$data['portfolio'] = FALSE;
		}
		
		if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
		$status_viewed = $this->pages_model->get_cashlender_proposal_view($public_proposal[0]->UserID, $user_id, $proposal_id);
		$users_data = $this->pages_model->getUserDetails($this->session->userdata('user_id'));
		if ( $status_viewed == "viewed" && $users_data->user_type == 'cash_lender' ){
			redirect(base_url('pages/print-report/' . $enc_proposal_id . '/PublicProposal'). '/');
		} else {
			$this->load->view('template/header', $data);
			$this->load->view('view_single_proposal', $data);
			$this->load->view('template/footer', $data);
		}
	}
	// End of the function
	
	// Start of Public Proposal Page
	public function rental($proposal_id, $proposal_title) {
		$this->not_logged_in();
		
		$enc_proposal_id = $proposal_id;
		$user_id = $this->session->userdata('user_id');
		$this->check_profile_update($user_id);
		
		$proposal_id = $proposal_id;
		$public_proposal = $this->pages_model->get_single_rental($proposal_id);
		
		if (empty($public_proposal) || !$proposal_title) {
			redirect(base_url('pages/investors-proposals') . '/');
			return FALSE;
		}
		$data['single_proposal'] = $public_proposal[0];
		
		$data['page_title'] = $public_proposal[0]->PropertyName . ' >> CBLists';
		
		$user_details = $this->pages_model->get_user($user_id);
		$data['user'] = $user_details[0];
		
		if ( isset($_POST['accept_confirm']) ) {
			$investor_id = $public_proposal[0]->UserID;
			$proposal_view_status = $this->pages_model->get_cashlender_proposal_view($investor_id, $user_id, $proposal_id);
			if ( $proposal_view_status == 'notviewed') {
				$proposal_view_status = $this->pages_model->set_cashlender_proposal_view($investor_id, $user_id, $proposal_id);
				$list = array (
					array('Name: ', 		$user_details[0]->fname),
					array('Email: ', 		$user_details[0]->email),
					array('Street: ', 		$user_details[0]->street),
					array('Town: ', 		$user_details[0]->town),
					array('State: ', 		$user_details[0]->state),
					array('ZIP: ', 		 	$user_details[0]->zip),
					array('Country: ', 		$user_details[0]->country),
					array('Phone: ', 		$user_details[0]->phone),
					array('Rental Name: ', 	$public_proposal[0]->PropertyName),
					array('', ''),
				);
				$fp = fopen(FCPATH . 'assets/csv/' . $investor_id . '-' . $user_id . '.csv', 'a');
				foreach ($list as $fields) {
					fputcsv($fp, $fields);
				}
				fclose($fp);
			}
		}
		
		if ( $this->pages_model->check_user_profile_exists($user_id) == 1 ) {
			$data['profile'] = TRUE;
		} else {
			$data['profile'] = FALSE;
		}
		
		if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
			$data['portfolio'] = TRUE;
		} else {
			$data['portfolio'] = FALSE;
		}
		
		if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
		$status_viewed = $this->pages_model->get_cashlender_proposal_view($public_proposal[0]->UserID, $user_id, $proposal_id);
		$users_data = $this->pages_model->getUserDetails($this->session->userdata('user_id'));
		if ( $status_viewed == "viewed" && $users_data->user_type == 'cash_lender' ){
			redirect(base_url('pages/print-report/' . $enc_proposal_id . '/PublicRental'). '/');
		} else {
			$this->load->view('template/header', $data);
			$this->load->view('view_single_rental', $data);
			$this->load->view('template/footer', $data);
		}
	}
	// End of the function
	
	
	
	// =================================================================== AJAX RELATED FUNCTIONS ============================================================== //
	// Start of Ajax function for Max Offer and Proposal Writer
	public function ajax_store() {
		error_reporting(0);
        $user_id = $this->session->userdata('user_id');
		
		$values = $_POST;
		$table = array_pop($values);
		$prop_id = array_shift($values);
		foreach ( $values as $field => $value ) {
			$forms[$field] = $value;
		}
		echo $this->pages_model->ajax_store($forms, $table, $prop_id);
	}
	// End of the function
	
	// Start of flip budget ajax
	public function flip_budget_ajax() {
		error_reporting(0);
        $user_id = $this->session->userdata('user_id');
		
		$values = $_POST;
		$created = array_pop($values);
		$prop_id = array_shift($values);
		foreach ( $values as $field => $value ) {
			$forms[$field] = $value;
		}
		$FlipRehabBudgetMethod 	= $forms['FlipRehabBudgetMethod']; 	unset($forms['FlipRehabBudgetMethod']);
		$FlipRehabDrawSelection = $forms['FlipRehabDrawSelection']; unset($forms['FlipRehabDrawSelection']);
		$LumpSumFlipBudget 		= $forms['LumpSumFlipBudget']; 		unset($forms['LumpSumFlipBudget']);
		$TotalBid1Total 		= $forms['TotalBid1Total'];			unset($forms['TotalBid1Total']);
		$TotalBid2Total 		= $forms['TotalBid2Total'];			unset($forms['TotalBid2Total']);
		$TotalBid3Total 		= $forms['TotalBid3Total'];			unset($forms['TotalBid3Total']);
		$DetailedFlipBudget 	= $forms['DetailedFlipBudget'];		unset($forms['DetailedFlipBudget']);
		
		$LumpSum = array(
			'FlipRehabBudgetMethod'  => $FlipRehabBudgetMethod,
			'FlipRehabDrawSelection' => $FlipRehabDrawSelection, 
			'LumpSumFlipBudget' 	 => $LumpSumFlipBudget
		);
		$Detailed = array(
			'TotalBid1Total' 	 => $TotalBid1Total, 
			'TotalBid2Total' 	 => $TotalBid2Total, 
			'TotalBid3Total' 	 => $TotalBid3Total, 
			'DetailedFlipBudget' => $DetailedFlipBudget
		);
		
		echo $this->pages_model->FlipBudgetInsert($forms, $LumpSum, $Detailed, $prop_id, $user_id, $created);
	}
	// End of the function
	
	// Start of refi budget ajax
	public function refi_budget_ajax() {
		error_reporting(0);
        $user_id = $this->session->userdata('user_id');
		
		$values = $_POST;
		$created = array_pop($values);
		$prop_id = array_shift($values);
		foreach ( $values as $field => $value ) {
			$forms[$field] = $value;
		}
		$RefiRehabBudgetMethod 	= $forms['RefiRehabBudgetMethod']; 	unset($forms['RefiRehabBudgetMethod']);
		$RefiRehabDrawSelection = $forms['RefiRehabDrawSelection']; unset($forms['RefiRehabDrawSelection']);
		$LumpSumRefiBudget 		= $forms['LumpSumRefiBudget']; 		unset($forms['LumpSumRefiBudget']);
		$TotalBid1Total 		= $forms['TotalBid1Total'];			unset($forms['TotalBid1Total']);
		$TotalBid2Total 		= $forms['TotalBid2Total'];			unset($forms['TotalBid2Total']);
		$TotalBid3Total 		= $forms['TotalBid3Total'];			unset($forms['TotalBid3Total']);
		$DetailedRefiBudget 	= $forms['DetailedRefiBudget'];		unset($forms['DetailedRefiBudget']);
		
		$LumpSum = array(
			'RefiRehabBudgetMethod'  => $RefiRehabBudgetMethod,
			'RefiRehabDrawSelection' => $RefiRehabDrawSelection, 
			'LumpSumRefiBudget' 	 => $LumpSumRefiBudget
		);
		$Detailed = array(
			'TotalBid1Total' 	 => $TotalBid1Total, 
			'TotalBid2Total' 	 => $TotalBid2Total, 
			'TotalBid3Total' 	 => $TotalBid3Total, 
			'DetailedRefiBudget' => $DetailedRefiBudget
		);
		
		echo $this->pages_model->RefiBudgetInsert($forms, $LumpSum, $Detailed, $prop_id, $user_id, $created);
	}
	// End of the function
	
	// Start Ajax Image Upload for Proposal Writer
	public function ajax_image_upload($PropertyID, $CreatedOn, $TableName, $Page) {
		error_reporting(0);
        $user_id = $this->session->userdata('user_id');

		$Photos = ''; 
		$Captions = '';
		$Covers = '';
		$CreatedOn = urldecode($CreatedOn);
		$OldPhotos = $_POST['OldPhotos'];
		foreach ( $_POST['Captions'] as $caption ) {
			if ( $caption == '' ) {
				$Captions .= '' . '||||';
			} else {
				$Captions .= $caption . '||||';
			}
		}
		$Captions = substr($Captions, 0, -4);
		foreach ( $_POST['CoversImages'] as $cover ) {
			if ( $cover == 'on' ) {
				$Covers .= $cover . '|';
			} else {
				$Covers .= 'off' . '|';
			}
		}
		$Covers = substr($Covers, 0, -1);
		if ( isset($_FILES['Images']) ) {
			foreach($_FILES['Images']['name'] as $key => $val) {
				if ( $Page == 'rehab' ) {
					$target_dir = FCPATH . 'assets/proposals/';
					$target_url = base_url() . 'assets/proposals/';
				} elseif ( $Page == 'rental' ) {
					$target_dir = FCPATH . 'assets/rental/';
					$target_url = base_url() . 'assets/rental/';
				}
				$ext = strtolower(pathinfo($_FILES['Images']['name'][$key], PATHINFO_EXTENSION));
				$name = $user_id . '_' . ($key + 1) * time() . rand(1, 988) . '.' . $ext;
				$target_file = $target_dir . $name;
				if ( move_uploaded_file($_FILES['Images']['tmp_name'][$key], $target_file)) {
					$Photos .= $target_url . $name . '|';
				}
			}
			if ( $Photos ) {
				$Photos = substr($Photos, 0, -1);
				$Photos = (!empty($OldPhotos) ? $OldPhotos  . '|' : '') . $Photos;
			} else {
				$Photos = (!empty($OldPhotos) ? $OldPhotos : '');
			}
			echo $this->pages_model->update_property_photos($TableName, $PropertyID, $user_id, $Photos, $Captions, $Covers, $CreatedOn);
		} else {
			$Photos = (!empty($OldPhotos) ? $OldPhotos : '');
			echo $this->pages_model->update_property_photos($TableName, $PropertyID, $user_id, $Photos, $Captions, $Covers, $CreatedOn);
		}
	}
	// End of the function
	
	
	
	// ========================================================== INVESTOR RELATED FUNCTIONS ============================================================== //
	// Start of Investors Leadbox Page
    public function leadbox() {
        $this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
		
		$cashlender_proposal_views = $this->pages_model->get_cashlender_proposal_views_list();
		$data['cashlender_proposal_views'] = $cashlender_proposal_views;
		
		$user_details = $this->pages_model->get_user($user_id);
		$data['user'] = $user_details[0];

        $data['page_title'] = 'Leadbox >> CBLists';

        if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

		$this->load->view('template/header', $data);
        $this->load->view('view_cashlender_views', $data);
        $this->load->view('template/footer', $data);
    }
    // End of the function
	
	// Start of Download files from server
	public function download_cl_csv($filename) {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
		$this->pages_model->update_downloaded_status($filename);
		
		header('HTTP/1.1 200 OK');
        header('Cache-Control: no-cache, must-revalidate');
        header("Pragma: no-cache");
        header("Expires: 0");
        header("Content-type: text/csv");
        header("Content-Disposition: attachment; filename=$filename");
        readfile('assets/csv/' . $filename);
        exit;
	}
	// End Download files from server
	
	// Start of Download files from server
	public function update_downloaded_csv() {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
		
		$filepath = $this->input->post('url');
		$this->pages_model->update_downloaded_status($filepath);
	}
	// End Download files from server
	
	// Start of update proposal for refi/flip
	public function update_refi_flip() {
		$this->not_logged_in();
		
        $user_id = $this->session->userdata('user_id');
		$property_id = $_POST['pid'];
		$status = $_POST['status'];
		echo $this->pages_model->update_property_refiflip($property_id, $user_id, $status);
	}
	
	// Start of leadbox cron mailing function
	public function leadbox_cron() {
		$investors = $this->pages_model->investor_mail_scheduling();
		
		// Email Configuration
		foreach ( $investors as $investor ) {
			$investor_data = $this->pages_model->get_user($investor->investor_id);
			$count = $this->pages_model->cashleander_visit_count($investor->investor_id);
			//Email sending code
			$message  = '<p>Hi ' . $investor_data[0]->fname . ',</p>';
			$message .= '<p>You have ' . $count[0]->count . ' new leads in your leadbox. Please click on the below link to visit your leadbox.</p>';
			$message .= '<p><a href="' . base_url('pages/leadbox') . '/">' . base_url('pages/leadbox') . '/</a></p>';
			$message .= '<p>The Team</p>';
			$message .= '<p>Arts Project</p>';
			$this->email->set_newline("\r\n");
			$this->email->from(SendFrom, 'CBList Property');
			$this->email->to($investor_data[0]->email, $investor_data[0]->fname);
			$this->email->subject($count[0]->count . ' new leads');
			$this->email->message($message);
			if ( $this->email->send() ) {
				echo 'Mail Sent!';
			}
		}
	}
	// End of the function
	
	
	
	// ============================================================ ACCOUNT RELATED FUNCTIONS ============================================================= //
    // Start of Check profile update
    private function check_profile_update($user_id) {
		if ( $this->pages_model->check_usertype(NULL, $user_id) == 'investor' ) {
			if ($this->pages_model->check_user_profile_exists($user_id) == 0) {
				redirect(base_url('pages/profile') . '/');
				return FALSE;
			}
		}
    }
	// End of the function
	
    // Start of not logged in script
    private function not_logged_in() {
        if (($this->session->userdata('logged_in') == FALSE)) {
            redirect(base_url());
            return FALSE;
        }
    }
    // End of the function
	
    // Start of logged in script
    private function logged_in() {
        if (($this->session->userdata('logged_in') == TRUE)) {
            redirect(base_url('pages/profile') . '/');
            return FALSE;
        }
    }
    // End of the function
	
    // Start of logout script
    public function logout() {
		$newdata = array(
            'user_id' => '',
            'linkedin' => '',
            'user_email' => '',
            'name' => '',
            'logged_in' => FALSE,
        );
        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        $_SESSION['access_token'] = '';
        $_SESSION['expires_in'] = '';
        $_SESSION['expires_at'] = '';
        unset($_SESSION['access_token']);
        unset($_SESSION['expires_in']);
        unset($_SESSION['expires_at']);
        redirect(base_url());
        return FALSE;
    }
    // End of the function
	
	
	
	// ================================================================ PROPOSAL RELATED FUNCTIONS ======================================================= //
	// Start of print report
    public function print_report($encodedId, $featuresEncoded) {
		$this->not_logged_in();
		
		$user_id = $this->session->userdata('user_id');
		$user_details = $this->pages_model->get_user($user_id);
		$data['user'] = $user_details[0];
		
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
        $id = $encodedId;
		$pdfData = '';
        $features = explode('-', urldecode($featuresEncoded));
		
		if ( in_array('MaxOffers', $features) ) {
			$data['page_title'] = "Max Offer Reports >> CBLists";
			$pdfData .= $this->load->view("template/maxoffer-header", $data, true);
		} elseif ( in_array('SingleOffer', $features) ) {
			$data['page_title'] = "Single Offer Reports >> CBLists";
			$pdfData .= $this->load->view('template/header', $data, true);
		} elseif ( in_array('SingleProposal', $features) ) {
			$data['page_title'] = "Single Proposal Reports >> CBLists";
			$pdfData .= $this->load->view('template/header', $data, true);
		} elseif ( in_array('PublicProposal', $features) ) {
			$data['page_title'] = "Public Proposal >> CBLists";
			$pdfData .= $this->load->view('template/header', $data, true);
		} elseif ( in_array('ProjectSummary', $features) || in_array('FlipRehabBudget', $features) || in_array('RefiRehabBudget', $features) || in_array('CoverPage', $features) || in_array('FlipMarketingSheet', $features) || in_array('RefiMarketingSheet', $features) || in_array('FlipCashFlow', $features) || in_array('RefiCashFlow', $features) || in_array('FlipFundingRequest', $features) || in_array('RefiFundingRequest', $features) || in_array('FlipCashFlowLender', $features) || in_array('RefiCashFlowLender', $features) || in_array('CompsReport', $features) || in_array('PicsPage', $features) ) {
			$data['page_title'] = "Proposal Reports >> CBLists";
			$pdfData .= $this->load->view("template/proposal-header", $data, true);
		} elseif ( in_array('SingleRental', $features) ) {
			$data['page_title'] = "Single Rental Reports >> CBLists";
			$pdfData .= $this->load->view('template/header', $data, true);
		} elseif ( in_array('PublicRental', $features) ) {
			$data['page_title'] = "Public Rental >> CBLists";
			$pdfData .= $this->load->view('template/header', $data, true);
		} elseif ( in_array('RentalCoverPage', $features) || in_array('SinglePagePropertySummery', $features) || in_array('RentalAdditionalPics', $features) || in_array('AnnualPropertyOperating', $features) || in_array('RentReport', $features) || in_array('OperatingExpensesReport', $features) || in_array('IncomeStatement', $features) || in_array('RentalCashFlowReport', $features) || in_array('DiscountCashFlowAnalysis', $features) || in_array('LenderSummary', $features) || in_array('RentalRefiAnalysis', $features) || in_array('FullTaxTreatment', $features) || in_array('PropertyValueEvolution', $features) || in_array('FinancialRatiosReport', $features) ) {
			$data['page_title'] = "Proposal Reports >> CBLists";
			$pdfData .= $this->load->view("template/rental-header", $data, true);
		} elseif ( in_array('Analyser', $features) ) {
			$data['page_title'] = "Comparisons Analyser Reports >> CBLists";
			$pdfData .= $this->load->view("template/analyser-header", $data, true);
		}
		
		foreach( $features as $feature ) {
			unset($data['print_data']);
			$data['print_data'][$feature] = $this->getPrintData($feature, $id);
			switch (strtolower($feature)) {
				case 'maxoffers':
					$pdfData .= $this->load->view("view_print_offer", $data, true);
					break; 
				case 'singleoffer':
					$pdfData .= $this->load->view("view_print_singleoffer", $data, true);
					break; 
				case 'singleproposal':  
					$pdfData .= $this->load->view("view_print_investor", $data, true);
					break;
				case 'publicproposal':
					$pdfData .= $this->load->view("view_print_cashlender", $data, true);
					break;
				case 'projectsummary':
				case 'fliprehabbudget':
				case 'refirehabbudget':
				case 'coverpage':
				case 'flipmarketingsheet':
				case 'refimarketingsheet':
				case 'flipcashflow':
				case 'reficashflow':
				case 'flipfundingrequest':
				case 'refifundingrequest':
				case 'flipcashflowlender':
				case 'reficashflowlender':
				case 'compsreport':
				case 'picspage':
					$pdfData .= $this->load->view("view_print_proposal", $data, true);
					break;
				case 'singlerental':  
					$pdfData .= $this->load->view("view_print_invrental", $data, true);
					break;
				case 'publicrental':
					$pdfData .= $this->load->view("view_print_cashrental", $data, true);
					break;
				case 'rentalcoverpage':
				case 'singlepagepropertysummery':
				case 'rentaladditionalpics':
				case 'annualpropertyoperating':
				case 'rentreport':
				case 'operatingexpensesreport':
				case 'incomestatement':
				case 'rentalcashflowreport':
				case 'discountcashflowanalysis':
				case 'lendersummary':
				case 'rentalrefianalysis':
				case 'fulltaxtreatment':
				case 'propertyvalueevolution':
				case 'financialratiosreport':
				case 'mortgageamortizationtables':
					$pdfData .= $this->load->view("view_print_rental", $data, true);
					break;
				case 'analyser': 
					$pdfData .= $this->load->view("view_print_analyser", $data, true);
					break;
			}
		}
		
		if ( in_array('MaxOffers', $features) ) {
			$pdfData .= $this->load->view("template/maxoffer-footer", $data, true);
		} elseif ( in_array('SingleOffer', $features) ) {
			$pdfData .= $this->load->view('template/footer', $data, true);
		} elseif ( in_array('SingleProposal', $features) ) {
			$pdfData .= $this->load->view('template/footer', $data, true);
		} elseif ( in_array('PublicProposal', $features) ) {
			$pdfData .= $this->load->view('template/footer', $data, true);
		} elseif ( in_array('ProjectSummary', $features) || in_array('FlipRehabBudget', $features) || in_array('RefiRehabBudget', $features) || in_array('CoverPage', $features) || in_array('FlipMarketingSheet', $features) || in_array('RefiMarketingSheet', $features) || in_array('FlipCashFlow', $features) || in_array('RefiCashFlow', $features) || in_array('FlipFundingRequest', $features) || in_array('RefiFundingRequest', $features) || in_array('FlipCashFlowLender', $features) || in_array('RefiCashFlowLender', $features) || in_array('CompsReport', $features) || in_array('PicsPage', $features) ) {
			$pdfData .= $this->load->view("template/proposal-footer", $data, true);
		} elseif ( in_array('SingleRental', $features) ) {
			$pdfData .= $this->load->view('template/footer', $data, true);
		} elseif ( in_array('PublicRental', $features) ) {
			$pdfData .= $this->load->view('template/footer', $data, true);
		} elseif ( in_array('RentalCoverPage', $features) || in_array('SinglePagePropertySummery', $features) || in_array('RentalAdditionalPics', $features) || in_array('AnnualPropertyOperating', $features) || in_array('RentReport', $features) || in_array('OperatingExpensesReport', $features) || in_array('IncomeStatement', $features) || in_array('RentalCashFlowReport', $features) || in_array('DiscountCashFlowAnalysis', $features) || in_array('LenderSummary', $features) || in_array('RentalRefiAnalysis', $features) || in_array('FullTaxTreatment', $features) || in_array('PropertyValueEvolution', $features) || in_array('FinancialRatiosReport', $features) ) {
			$pdfData .= $this->load->view("template/rental-footer", $data, true);
		} elseif ( in_array('Analyser', $features) ) {
			$pdfData .= $this->load->view("template/analyser-footer", $data, true);
		}
        echo $pdfData;
    }
	// End of the function
	
	// Start of print report
    public function download_report($encodedId, $featuresEncoded, $ajaxcall=false, $type=false) {
		$this->not_logged_in();
		
		$user_id = $this->session->userdata('user_id');
		$user_details = $this->pages_model->get_user($user_id);
		$data['user'] = $user_details[0];
		
        $id = $encodedId;
		$pdfData = '';
        $features = explode('-', urldecode($featuresEncoded));
		
		if ( in_array('MaxOffers', $features) ) {
			$data['page_title'] = "Max Offer Reports >> CBLists";
			$pdfData .= $this->load->view("template/maxoffer-pdf-header", $data, true);
		} elseif ( in_array('ProjectSummary', $features) || in_array('FlipRehabBudget', $features) || in_array('RefiRehabBudget', $features) || in_array('CoverPage', $features) || in_array('FlipMarketingSheet', $features) || in_array('RefiMarketingSheet', $features) || in_array('FlipCashFlow', $features) || in_array('RefiCashFlow', $features) || in_array('FlipFundingRequest', $features) || in_array('RefiFundingRequest', $features) || in_array('FlipCashFlowLender', $features) || in_array('RefiCashFlowLender', $features) || in_array('CompsReport', $features) || in_array('PicsPage', $features) ) {
			$data['page_title'] = "Proposal Reports >> CBLists";
			$pdfData .= $this->load->view("template/proposal-pdf-header", $data, true);
		} elseif ( in_array('RentalCoverPage', $features) || in_array('SinglePagePropertySummery', $features) || in_array('RentalAdditionalPics', $features) || in_array('AnnualPropertyOperating', $features) || in_array('RentReport', $features) || in_array('OperatingExpensesReport', $features) || in_array('IncomeStatement', $features) || in_array('RentalCashFlowReport', $features) || in_array('DiscountCashFlowAnalysis', $features) || in_array('LenderSummary', $features) || in_array('RentalRefiAnalysis', $features) || in_array('FullTaxTreatment', $features) || in_array('PropertyValueEvolution', $features) || in_array('FinancialRatiosReport', $features) ) {
			$data['page_title'] = "Proposal Reports >> CBLists";
			$pdfData .= $this->load->view("template/rental-pdf-header", $data, true);
		} elseif ( in_array('Analyser', $features) ) {
			$data['page_title'] = "Comparisons Analyser Reports >> CBLists";
			$pdfData .= $this->load->view("template/analyser-pdf-header", $data, true);
		}
		foreach( $features as $feature ) {
			unset($data['print_data']);
			$data['print_data'][$feature] = $this->getPrintData($feature, $id);
			switch (strtolower($feature)) {
				case 'maxoffers':
					$pdfData .= $this->load->view("view_pdf_offer", $data, true);
					break; 
				case 'projectsummary':
				case 'fliprehabbudget':
				case 'refirehabbudget':
				case 'coverpage':
				case 'flipmarketingsheet':
				case 'refimarketingsheet':
				case 'flipcashflow':
				case 'reficashflow':
				case 'flipfundingrequest':
				case 'refifundingrequest':
				case 'flipcashflowlender':
				case 'reficashflowlender':
				case 'compsreport':
				case 'picspage':
					$pdfData .= $this->load->view("view_pdf_proposal", $data, true);
					break;
				case 'rentalcoverpage':
				case 'singlepagepropertysummery':
				case 'rentaladditionalpics':
				case 'annualpropertyoperating':
				case 'rentreport':
				case 'operatingexpensesreport':
				case 'incomestatement':
				case 'rentalcashflowreport':
				case 'discountcashflowanalysis':
				case 'lendersummary':
				case 'rentalrefianalysis':
				case 'fulltaxtreatment':
				case 'propertyvalueevolution':
				case 'financialratiosreport':
				case 'mortgageamortizationtables':
					$pdfData .= $this->load->view("view_pdf_rental", $data, true);
					break;
				case 'analyser':
					$pdfData .= $this->load->view("view_pdf_analyser", $data, true);
					break; 
			}
		}
		if ( in_array('MaxOffers', $features) ) {
			$pdfData .= $this->load->view("template/maxoffer-pdf-footer", $data, true);
			$pdfname = 'MaxOffer';
		} elseif ( in_array('ProjectSummary', $features) || in_array('FlipRehabBudget', $features) || in_array('RefiRehabBudget', $features) || in_array('CoverPage', $features) || in_array('FlipMarketingSheet', $features) || in_array('RefiMarketingSheet', $features) || in_array('FlipCashFlow', $features) || in_array('RefiCashFlow', $features) || in_array('FlipFundingRequest', $features) || in_array('RefiFundingRequest', $features) || in_array('FlipCashFlowLender', $features) || in_array('RefiCashFlowLender', $features) || in_array('CompsReport', $features) || in_array('PicsPage', $features) ) {
			$pdfData .= $this->load->view("template/proposal-pdf-footer", $data, true);
			$pdfname = 'ProposalReport';
		} elseif ( in_array('RentalCoverPage', $features) || in_array('SinglePagePropertySummery', $features) || in_array('RentalAdditionalPics', $features) || in_array('AnnualPropertyOperating', $features) || in_array('RentReport', $features) || in_array('OperatingExpensesReport', $features) || in_array('IncomeStatement', $features) || in_array('RentalCashFlowReport', $features) || in_array('DiscountCashFlowAnalysis', $features) || in_array('LenderSummary', $features) || in_array('RentalRefiAnalysis', $features) || in_array('FullTaxTreatment', $features) || in_array('PropertyValueEvolution', $features) || in_array('FinancialRatiosReport', $features) ) {
			$pdfData .= $this->load->view("template/rental-pdf-footer", $data, true);
			$pdfname = 'RentalReport';
		} elseif ( in_array('Analyser', $features) ) {
			$pdfData .= $this->load->view("template/analyser-pdf-footer", $data, true);
			$pdfname = 'Analyser';
		}
		
		if ( $ajaxcall == 'save' ) {
			if ( $type == 'proposal' ) {
				$single = $this->pages_model->get_proposal($id);
			} elseif ( $type == 'rental' ) {
				$single = $this->pages_model->get_rental($id);
			}
			echo $this->save_pdf($pdfData, $pdfname, $id, $single[0]->PropertyName);
		} else {
			echo $this->print_pdf($this->generate_pdf($pdfData, $pdfname));
		}
    }
	// End of the function
 
	// Start of get print data 
    private function getPrintData($string, $id){
		$this->not_logged_in();
		
        $tableData = '';
        switch (strtolower($string)) {
            case 'maxoffers':
                $tableData = $this->pages_model->getOffersDetails($id);
                break;
			case 'singleoffer':
                $tableData = $this->pages_model->getOffersDetails($id);
                break;
            case 'projectsummary':
                $tableData 				 = $this->pages_model->getProjectSummary($id);
				$tableData->Property 	 = $this->pages_model->getFullProjectData($id);
				$tableData->FlipBudget 	 = $this->pages_model->getFullFlipBudget($id);
				$tableData->RefiBudget 	 = $this->pages_model->getFullRefiBudget($id);
                break;
			case 'fliprehabbudget':
				$tableData = $this->pages_model->getFlipRehabBudget($id);
				break;
			case 'refirehabbudget':
				$tableData = $this->pages_model->getRefiRehabBudget($id);
				break;
			case 'coverpage':
				$tableData = $this->pages_model->getCoverPage($id);
				break;
			case 'flipmarketingsheet':
				$tableData 				 = $this->pages_model->getFlipMarketingShaeet($id);
				$tableData->Property 	 = $this->pages_model->getFullProjectData($id);
				$tableData->FlipBudget 	 = $this->pages_model->getFullFlipBudget($id);
				break;
			case 'refimarketingsheet':
				$tableData 				 = $this->pages_model->getRefiMarketingShaeet($id);
				$tableData->Property 	 = $this->pages_model->getFullProjectData($id);
				$tableData->RefiBudget 	 = $this->pages_model->getFullRefiBudget($id);
				break;
			case 'flipcashflow':
				$tableData 				 = $this->pages_model->getFlipCashFlowSummary($id);
				$tableData['Property'] 	 = $this->pages_model->getFullProjectData($id);
				$tableData['FlipBudget'] = $this->pages_model->getFullFlipBudget($id);
				break;
			case 'reficashflow':
				$tableData 				 = $this->pages_model->getRefiCashFlowSummary($id);
				$tableData['Property'] 	 = $this->pages_model->getFullProjectData($id);
				$tableData['RefiBudget'] = $this->pages_model->getFullRefiBudget($id);
				break;
			case 'flipfundingrequest':
				$tableData 				 = $this->pages_model->getFlipLenderFundingRequest($id);
				$tableData->Property 	 = $this->pages_model->getFullProjectData($id);
				$tableData->FlipBudget 	 = $this->pages_model->getFullFlipBudget($id);
				break;
			case 'refifundingrequest':
				$tableData 				 = $this->pages_model->getRefiLenderFundingRequest($id);
				$tableData->Property 	 = $this->pages_model->getFullProjectData($id);
				$tableData->RefiBudget 	 = $this->pages_model->getFullRefiBudget($id);
				break;
			case 'flipcashflowlender':
				$tableData 				 = $this->pages_model->getFlipLenderCashFlow($id);
				$tableData['Property'] 	 = $this->pages_model->getFullProjectData($id);
				$tableData['FlipBudget'] = $this->pages_model->getFullFlipBudget($id);
				break;
			case 'reficashflowlender':
				$tableData 				 = $this->pages_model->getRefiLenderCashFlow($id);
				$tableData['Property'] 	 = $this->pages_model->getFullProjectData($id);
				$tableData['RefiBudget'] = $this->pages_model->getFullRefiBudget($id);
				break;
			case 'compsreport':
				$tableData = $this->pages_model->getComparableSales($id);
				break;
			case 'picspage':
				$tableData = $this->pages_model->getAdditionalPics($id);
				break;
			case 'singleproposal':
			case 'publicproposal':
				$tableData['CoverPage'] 						= $this->pages_model->get_cashlender_proposal($id);
				$tableData['FlipRehabBudget'] 					= $this->pages_model->getFlipRehabBudget($id);
				$tableData['RefiRehabBudget'] 					= $this->pages_model->getRefiRehabBudget($id);
				$tableData['ProjectSummary'] 					= $this->pages_model->getProjectSummary($id);
				$tableData['ProjectSummary']->Property 	 		= $this->pages_model->getFullProjectData($id);
				$tableData['ProjectSummary']->FlipBudget 		= $this->pages_model->getFullFlipBudget($id);
				$tableData['ProjectSummary']->RefiBudget 		= $this->pages_model->getFullRefiBudget($id);
				$tableData['FlipMarketingSheet'] 			 	= $this->pages_model->getFlipMarketingShaeet($id);
				$tableData['FlipMarketingSheet']->Property 	 	= $this->pages_model->getFullProjectData($id);
				$tableData['FlipMarketingSheet']->FlipBudget 	= $this->pages_model->getFullFlipBudget($id);
				$tableData['RefiMarketingSheet'] 			 	= $this->pages_model->getRefiMarketingShaeet($id);
				$tableData['RefiMarketingSheet']->Property 	 	= $this->pages_model->getFullProjectData($id);
				$tableData['RefiMarketingSheet']->RefiBudget 	= $this->pages_model->getFullRefiBudget($id);
				$tableData['FlipCashFlow'] 				 	 	= $this->pages_model->getFlipCashFlowSummary($id);
				$tableData['FlipCashFlow']['Property'] 	 	 	= $this->pages_model->getFullProjectData($id);
				$tableData['FlipCashFlow']['FlipBudget'] 	 	= $this->pages_model->getFullFlipBudget($id);
				$tableData['RefiCashFlow'] 				 	 	= $this->pages_model->getRefiCashFlowSummary($id);
				$tableData['RefiCashFlow']['Property'] 	 	 	= $this->pages_model->getFullProjectData($id);
				$tableData['RefiCashFlow']['RefiBudget'] 	 	= $this->pages_model->getFullRefiBudget($id);
				$tableData['FlipFundingRequest'] 			 	= $this->pages_model->getFlipLenderFundingRequest($id);
				$tableData['FlipFundingRequest']->Property 	 	= $this->pages_model->getFullProjectData($id);
				$tableData['FlipFundingRequest']->FlipBudget 	= $this->pages_model->getFullFlipBudget($id);
				$tableData['RefiFundingRequest'] 			 	= $this->pages_model->getRefiLenderFundingRequest($id);
				$tableData['RefiFundingRequest']->Property 	 	= $this->pages_model->getFullProjectData($id);
				$tableData['RefiFundingRequest']->RefiBudget 	= $this->pages_model->getFullRefiBudget($id);
				$tableData['FlipCashFlowLender'] 			   	= $this->pages_model->getFlipLenderCashFlow($id);
				$tableData['FlipCashFlowLender']['Property']   	= $this->pages_model->getFullProjectData($id);
				$tableData['FlipCashFlowLender']['FlipBudget'] 	= $this->pages_model->getFullFlipBudget($id);
				$tableData['RefiCashFlowLender'] 			   	= $this->pages_model->getRefiLenderCashFlow($id);
				$tableData['RefiCashFlowLender']['Property']   	= $this->pages_model->getFullProjectData($id);
				$tableData['RefiCashFlowLender']['RefiBudget'] 	= $this->pages_model->getFullRefiBudget($id);
				$tableData['CompsReport'] 						= $this->pages_model->getComparableSales($id);
				$tableData['PicsPage'] 							= $this->pages_model->getAdditionalPics($id);
				break;
			case 'rentalcoverpage':
			case 'singlepagepropertysummery':
			case 'rentaladditionalpics':
			case 'annualpropertyoperating':
			case 'rentreport':
			case 'operatingexpensesreport':
			case 'incomestatement':
			case 'rentalcashflowreport':
			case 'discountcashflowanalysis':
			case 'lendersummary':
			case 'rentalrefianalysis':
			case 'fulltaxtreatment':
			case 'propertyvalueevolution':
			case 'financialratiosreport':
			case 'singlerental':
			case 'publicrental':
				$tableData = $this->pages_model->get_single_property($id);
				break;
			case 'analyser':
                $tableData = $this->pages_model->getAnalyserDetails($id);
                break;
			default:
				$tableData = 'ERROR LOADING';
				break;
		}
		return $tableData;
	}
	// End of the function
	
	// Start of print pdf
    private function print_pdf($pdfData){
        $fileSize = count($pdfData);
        header("HTTP/1.1 200 OK");
        header("Pragma: public");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private", false);
        header("Content-type: application/pdf");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . $fileSize);
        return $pdfData;
    }
	// End of the function
	
	// Start of generate pdf
    private function generate_pdf($html_data, $feature) {
		$this->dompdf_lib->set_paper("A4", "potrait");
		$this->dompdf_lib->load_html($html_data);
		$this->dompdf_lib->render();
		$pdfdata = $this->dompdf_lib->stream($feature.".pdf", array('Attachment'=>0));
		return $pdfdata;
    }
	// End of the function
	
	// Start of generate pdf
    private function save_pdf($html_data, $feature, $proposalId, $propertyName) {
		$user_id = $this->session->userdata('user_id');
		$timeName = $feature . "-" . date('d-m-Y') . "-" . time() . ".pdf";
		$this->dompdf_lib->set_paper("A4", "potrait");
		$this->dompdf_lib->load_html($html_data);
		$this->dompdf_lib->render();
		$pdf = $this->dompdf_lib->output();
		$pdf_location = FCPATH . "assets/reports/" . $timeName;
		file_put_contents($pdf_location, $pdf);
		$data['pdf_name'] = $timeName;
		$data['pdf_url'] = base_url("assets/reports/" . $timeName);
		$data['pdf_path'] = $pdf_location;
		$data['proposalId'] = $proposalId;
		$data['propertyName'] = $propertyName;
		return json_encode($data);
    }
	// End of the function
	
	

	
	// =============================================================== PAYMENT RELATED FUNCTION =========================================================== //
	// Statrt of Check user to paid
	public function checkuserpaid($user_id, $echo = TRUE) {
		$this->not_logged_in();
		
		$user_status = $this->pages_model->getUserpaid($user_id);
		if ( $user_status->paidtype == 'H' && $user_status->paidstatus == 1 ) {
			if ( $echo ) echo 2; else return 2;
		} elseif ( $user_status->paidtype == 'M' && $user_status->paidstatus == 1 ) {
			if ( $echo ) echo 1; else return 1;
		} elseif ( $user_status->paidtype == 'N' && $user_status->paidstatus == 0 ) {
			if ( $echo ) echo 0; else return 0;
		}
	}
	// End of the function
	
	// Start of payment
	public function payment() {
		$this->not_logged_in();
		
		$data['page_title'] = 'Choose Plan >> CBLists';
		$user_id = $this->session->userdata('user_id');
		$PaidStatus = $this->checkuserpaid($user_id, FALSE);
		if ( $PaidStatus == 2 ) {
			$redirect = isset($_SERVER["HTTP_REFERER"]) ? $_SERVER["HTTP_REFERER"] : base_url();
			redirect($redirect);
			return FALSE;
		}
		$user_details = $this->pages_model->get_user($user_id);
		$data['user'] = $user_details[0];
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
			$data['profile'] = TRUE;
		} else {
			$data['profile'] = FALSE;
		}
		
		if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
			$data['portfolio'] = TRUE;
		} else {
			$data['portfolio'] = FALSE;
		}
		
		if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

		if ( isset($_POST['choose_amount']) != '' ) {
			$choose_amount = $_POST['choose_amount'];
			if ( $choose_amount == 97 ) {
				echo '<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top" class="cblist97">';
				echo '<input type="hidden" name="cmd" value="_s-xclick">';
				echo '<input type="hidden" name="hosted_button_id" value="QWL8Z99FW7AVE">';
				echo '<input type="hidden" name="custom" value="'.$user_id.'&investor">';
				echo '</form>';
			} else {
				echo '<form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" target="_top" class="cblist97">';
				echo '<input type="hidden" name="cmd" value="_s-xclick">';
				echo '<input type="hidden" name="hosted_button_id" value="UMQ4HFKCZ58BL">';
				echo '<input type="hidden" name="custom" value="'.$user_id.'&investor">';
				echo '</form>';
			}
		} else {
			$this->load->view('template/header', $data);
			$this->load->view('view_chooseplan', $data);
			$this->load->view('template/footer', $data);
		}
	}
	// End of the function
	
	// Start of paypal ipn
	public function Paypal_IPN() {
		if ( isset($_POST) ) {
			$pay_date='';
			$myFile = APPPATH . "views/PayPal_IPN.txt";
			$fh = fopen($myFile, 'w') or die("can't open file");
			$stringData = '';
			foreach ( $_REQUEST as $key => $val ) {
				if($key == "subscr_date" || $key=="payment_date"){
					$pay_date=$val;
				}
				$stringData .= $key . "=" . $val . "\n";
			}
			fwrite($fh, $stringData);
			fclose($fh);
			$amount = $_POST['mc_amount3'];
			$subscr_id = $_POST['subscr_id'];
			$user_det = explode("&",$_POST['custom']);
			$user_id = $user_det[0];
			$user_type = $user_det[1];
			$user_firstname = $_POST['first_name'];
			$user_lastname = $_POST['last_name'];
			$username = $_POST['first_name'].' '.$user_lastname;
			$receiver_email = $_POST['receiver_email'];
			$payer_email = $_POST['payer_email'];
			$item_name = $_POST['item_name'];
			$db_insert = $this->pages_model->getPaymentupdate($user_id,$user_type,$user_firstname,$user_lastname,$payer_email,$receiver_email,$item_name,$subscr_id,$amount,$pay_date);
			if ( $db_insert != '' || $db_insert != 0 ) {
				$message  = '<p>Hi '.$user_firstname.' '.$user_lastname.',</p><br />';
				$message .= '<p><strong>Your Payment is Successful .</strong></p>';
				$message .= '<p>Thank You for subscribe CBList with '.$amount.' as an '.$user_type.'.</p>';
				$message .= '<p>Your payment details given below :</p>';
				$message .= '<p>Subscription ID: <strong>' . $subscr_id . '</strong></p>';
				$message .= '<p>Subscription For: <strong>' . $item_name . '</strong>.</p>';
				$message .= '<p>Subscription Date: <strong>'.$pay_date.'</strong>.</p><br />';
				$message .= '<p>Thank you,</p>';
				$message .= '<p>CBList team</p>';
				$message .= '<p>If you have any questions, Please contact us on below link and we will get back to you as soon as possible</p>';
				$message .= '<p><a href="' . base_url() . '">' . base_url() . '</a></p>';
				
				$this->email->set_newline("\r\n");
				$this->email->from(SendFrom, 'CBList Property');
				$this->email->to($payer_email, $username);
				$this->email->subject('CBList Property Payment Successful');
				$this->email->message($message);
				if ( $this->email->send() ) {
					if ( $_POST['mc_amount3'] == 97.00 ) {
						$paidtype = 'M';
					} else {
						$paidtype = 'H';
					}
					$this->pages_model->getUserstatusChange($user_id, $paidtype);
					echo '<script>get_transaction_data();</script>';
				}
			}
		}
	}
	// End of the function
	
	// Start of payment sucessful
	public function payment_sucessful() {
		$this->not_logged_in();
		
		$data['page_title'] = 'Payment Status >> CBLists';
		$user_id = $this->session->userdata('user_id');
		$user_details = $this->pages_model->get_user($user_id);
		$data['user'] = $user_details[0];
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
			$data['profile'] = TRUE;
		} else {
			$data['profile'] = FALSE;
		}
		
		if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
			$data['portfolio'] = TRUE;
		} else {
			$data['portfolio'] = FALSE;
		}
		
		if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
		$this->load->view('template/header', $data);
		$this->load->view('view_paymentsuccess', $data);
		$this->load->view('template/footer', $data);
	}
	// End of the function
	
	// Start of payment status
	public function payment_status(){
		$this->not_logged_in();
		
		$user_id = $this->session->userdata('user_id');
		$user_details = $this->pages_model->get_user($user_id);
		$status = $user_details[0]->paidstatus;
		echo json_encode(array("status"=>$status));
	}
	// End of the function
	
	// Start of payment details
	public function payment_details(){
		$this->not_logged_in();
		
		$user_id = $this->session->userdata('user_id');
		$result = $this->pages_model->getPaymentDetails($user_id);
		echo json_encode($result);
	}
	// End of the function
	
	

	// ================================================================ QUICK PROPERTY ANALYSER ============================================================== //
	// Start of Proposal Comparison List
	public function comparisons() {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $data['page_title'] = 'Property Comparison >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
		$comparison = $this->pages_model->get_comparison_lists($user_id);
        $data['comparison'] = $comparison;

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_comparison_lists', $data);
        $this->load->view('template/footer', $data);
		$this->session->unset_userdata('comparison_deleted');
	}
	// End of the function
	
	// Start of Proposal Comparison Analyser
	public function comparison_analyser() {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $data['page_title'] = 'Quick Comparison Analyser >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		$data['PropertyID'] = time();
		$data['CreatedOn'] = date('Y-m-d H:i:s');
		
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_property_comparison', $data);
        $this->load->view('template/footer', $data);
	}
	// End of the function
	
	// Start of Edit max offer Page
    public function edit_comparison($id, $section) {
        $this->not_logged_in();
            
        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
            
        $analyser_id = $id;
        $single = $this->pages_model->get_comparison($analyser_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/comparisons') . '/');
            return FALSE;
        }
		
		$data['page_title'] = 'Comparison Analyser >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		$data['PropertyID'] = $analyser_id;
		$data['CreatedOn'] = $single[0]->CreatedOn;
		
		$data['fields'] = $this->pages_model->get_full_comparison($analyser_id);
		
		
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_edit_analyser', $data);
        $this->load->view('template/footer', $data);
    }
    // End of the function
	
	// Start of Delete Comparison Page
    public function delete_comparison($id, $section) {
        $this->not_logged_in();
            
        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
            
        $comp_id = $id;
        $single = $this->pages_model->get_comparison($comp_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/comparisons') . '/');
            return FALSE;
        }
        $this->pages_model->delete_comparison($comp_id);
        $this->session->set_userdata(array('comparison_deleted' => TRUE));
        redirect(base_url('pages/comparisons') . '/');
        return FALSE;
    }
    // End of the function
	
	
	
	// ========================================================= STATIC PAGES (ABOUT BLOG PROCING FEATURE ETC) ======================================================== //
	// Start of rental valuator
	public function rental_valuator() {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
		
		$data['countries'] = config_item('country_list');
        $data['page_title'] = 'Rental Property Evaluator >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }
		
		$rentals = $this->pages_model->get_rental_lists($user_id);
        $data['rentals'] = $rentals;

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_rental_lists', $data);
        $this->load->view('template/footer', $data);
		$this->session->unset_userdata('rental_deleted');
		$this->session->unset_userdata('rental_submitted');
	}
	// End of the function
	
	// Start of the new rental valuator
	public function rental_calculation() {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $data['page_title'] = 'Rental Rental Evaluator Calculation >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		$data['PropertyID'] = time();
		$data['CreatedOn'] = date('Y-m-d H:i:s');
		
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_rental_calculation', $data);
        $this->load->view('template/footer', $data);
	}
	// End of the function
	
	// Start of the new rental valuator
	public function edit_rental($id, $section) {
		$this->not_logged_in();

        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
		
		// Checking for Valid Propoerty ID else redirect to 404 page
		$proposal_id = $id;
        $single = $this->pages_model->get_rental($proposal_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/rental-valuator') . '/');
            return FALSE;
        }
		
        $data['page_title'] = 'Rental Property Evaluator Calculation >> CBLists';
		$data['error'] = ''; $data['success'] = ''; $data['reset'] = '';
		
		$data['PropertyID'] = $proposal_id;
		$data['CreatedOn'] = $single[0]->CreatedOn;
		
		$data['fields'] = $this->pages_model->get_single_property($proposal_id);
		
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
            $data['profile'] = TRUE;
        } else {
            $data['profile'] = FALSE;
        }

        if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
            $data['portfolio'] = TRUE;
        } else {
            $data['portfolio'] = FALSE;
        }

        if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
            $proposal_details = $this->pages_model->get_proposal_lists($user_id);
            $data['proposal'] = $proposal_details;
        } else {
            $data['proposal'] = FALSE;
        }
		
		if ($this->pages_model->check_user_offer_exists($user_id) == 1) {
            $offer_details = $this->pages_model->get_max_offer_lists($user_id);
            $data['offer'] = $offer_details;
        } else {
            $data['offer'] = FALSE;
        }
		
		if ($this->pages_model->check_user_rental_exists($user_id) == 1) {
            $rental_details = $this->pages_model->get_rental_lists($user_id);
            $data['rental'] = $rental_details;
        } else {
            $data['rental'] = FALSE;
        }
		
		if ($this->pages_model->check_user_analyser_exists($user_id) == 1) {
            $analyser_details = $this->pages_model->get_comparison_lists($user_id);
            $data['analyser'] = $analyser_details;
        } else {
            $data['analyser'] = FALSE;
        }

        $user_details = $this->pages_model->get_user($user_id);
        $data['user'] = $user_details[0];
		
		$this->load->view('template/header', $data);
        $this->load->view('view_rental_edit', $data);
        $this->load->view('template/footer', $data);
	}
	// End of the function
	
	// Start of proposal submission to network
	public function submit_rental($id, $section) {
		$this->not_logged_in();
            
        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);

        $rental_id = $id;
        $single = $this->pages_model->get_rental($rental_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/rental-valuator') . '/');
            return FALSE;
        }
		
		$submit = $this->pages_model->submit_proposal_network($rental_id, $user_id, $section);
		
        $this->session->set_userdata(array('rental_submitted' => $submit));
        redirect(base_url('pages/rental-valuator') . '/');
        return FALSE;		
	}
	// End of the function
	
	// Start of Delete Comparison Page
    public function delete_rental($id, $section) {
        $this->not_logged_in();
            
        $user_id = $this->session->userdata('user_id');
        $this->check_profile_update($user_id);
            
        $rental_id = $id;
        $single = $this->pages_model->get_rental($rental_id);
        if (empty($single) || !$section) {
            redirect(base_url('pages/rental-valuator') . '/');
            return FALSE;
        }
		$photos = $this->pages_model->get_rental_photos($rental_id);
        $images = explode('|', $photos->Photos);
        foreach ($images as $img) {
            $file_name = explode('/', $img);
            $file_name = end($file_name);
            unlink(FCPATH . 'assets/rental/' . $file_name);
        }
        $this->pages_model->delete_rental($rental_id);
        $this->session->set_userdata(array('rental_deleted' => TRUE));
        redirect(base_url('pages/rental-valuator') . '/');
        return FALSE;
    }
    // End of the function
	
	// Start of proposal email 
	public function email_proposal() {
		$this->not_logged_in();
		
		$user_id = $this->session->userdata('user_id');
		$user_details = $this->pages_model->get_user($user_id);
		
		$inputTo = $_POST['inputTo'];
		$inputSubject = $_POST['inputSubject'];
		$inputBody = $_POST['inputBody'];
		$attachments = $_POST['attachments'];
		
		//Email sending code
		$this->email->set_newline("\r\n");
		$this->email->from($user_details[0]->email, $user_details[0]->fname);
		$this->email->to($inputTo);
		$this->email->subject($inputSubject);
		$this->email->message($inputBody);
		foreach( $attachments as $attach ) {
			$this->email->attach($attach);
		}
		if ($this->email->send()) {
			foreach( $attachments as $attach ) {
				unlink($attach);
			}
			echo 'sent';
		} else {
			echo 'error';
		}
	}
	// End of the function
	
	// Start of the advance modal date column
	public function setpropertyClosingDate() {
		$time_html = '';
		$chq_date = str_replace("-","/", $_POST['PropertyClosingDate']);
		$closing_date  = date("Y-m-d", strtotime($chq_date)); 
		$time_html .='<th colspan="2"></th>';
		for ( $td = 0; $td <= 40; $td++ ) {
			$time_html .='<th>'.date('d-m-Y', strtotime("$closing_date + $td years")).'<input type="hidden" name="Year_'.$td.'" id="Year_'.$td.'" value="'.date('d-m-Y', strtotime("$closing_date + $td years")).'" /></th>';
		} 
		echo $time_html;
	}
	// End of the function
	
	// Start of the report ajax call function
	function report_refi_calc() {
		$PropertyID = $_POST['PropertyID'];
		$C9 = $_POST['FirstMortgageUsed'];
		$D9 = $_POST['SecondMortgageUsed'];
		$C10 = $_POST['FirstMortgageLoanType'];
		$D10 = $_POST['SecondMortgageLoanType'];
		$C11 = $_POST['FirstMortgageTerm'];
		$D11 = $_POST['SecondMortgageTerm'];
		$C12 = ($_POST['FirstMortgageLTV']/100);
		$D12 = ($_POST['SecondMortgageLTV']/100);
		$C13 = ($_POST['FirstMortgageRate']/100);
		$D13 = ($_POST['SecondMortgageRate']/100);
		$H9 = $_POST['RefiResale'];
		$H10 = ($_POST['TerminalCapRate']/100);
		$B23 = ($_POST['NewLoanAmount']/100);
		$B24 = ($_POST['ClosingCostNewRate']/100);
		
		$DATA = $this->pages_model->get_single_property($PropertyID);
		
		/** ------------------------------------------------------- D16 ------------------------------------------------------- **/
		$Incomes = array();
		for ( $i=1; $i<=12; $i++ ) {
			$PropertyAnnualRent = 'PropertyAnnualRent_' . $i;
			$PropertyAnnualGrowthRate = 'PropertyAnnualGrowthRate_' . $i;
			$IncomeRate = $DATA->$PropertyAnnualRent;
			for ( $n=1; $n<=$DATA->YearSale; $n++ ) {
				if ( $n == 1 ) {
					$IncomeRate = $IncomeRate;
				} else {
					if ( $DATA->RentGrowthOveride == "No" ) {
						$IncomeRate = $IncomeRate + (($IncomeRate * $DATA->$PropertyAnnualGrowthRate)/100);
					} else {
						$IncomeRate = $IncomeRate + (($IncomeRate * $DATA->RentGrowthRate)/100);
					}
				}
				$Incomes[$i][$n] = round($IncomeRate, 2);
			}
		}
		foreach ( $Incomes as $k => $SubInArray ) {
			foreach ( $SubInArray as $id => $value ) {
				if ( !isset($D16[$id]) ) {
					$D16[$id] = 0;
				}
				$D16[$id] += round($value, 2);
			}
		}
		/** ------------------------------------------------------- D16 ------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D22 ------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$OverrideVacancyRates = 'OverrideVacancyRates_Year_' . $i;
			$OverrideConcessions = 'OverrideConcessions_Year_' . $i;
			$D18[$i] = round($DATA->OverrideVacancyRates=="No" ? ($D16[$i]*$DATA->VacancyLossPercentage)/100 : ($D16[$i]*$DATA->$OverrideVacancyRates)/100, 2);
			$D19[$i] = round($DATA->OverrideConcessions=="No" ? ($D16[$i]*$DATA->ConcessionsParcentage)/100 : ($D16[$i]*$DATA->$OverrideConcessions)/100, 2);
			$D20[$i] = round(($D16[$i]*$DATA->ManagementFeePercentage)/100, 2);
			if ( $i == 1 ) {
				$D21[$i] = $DATA->OtherIncomeAnnual;
			} else {
				$D21[$i] = round($D21[$i-1] * (1+($DATA->OtherIncomeAnnualGrowthRate/100))*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1), 2);
			}
			$D22[$i] = $D16[$i]-($D18[$i]+$D19[$i]+$D20[$i])+$D21[$i];
		}
		/** ------------------------------------------------------- D22 ------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D46------------------------------------------------------- **/
		$ExpenseReservesAnnual = 'OperatingExpenseReservesAnnual';
		$ExpenseReservesGrowthRate = 'OperatingExpenseReservesGrowthRate';
		$ReservesRate = $DATA->$ExpenseReservesAnnual;
		for ( $n=1; $n<=$DATA->YearSale; $n++ ) {
			if ( $n == 1 ) {
				$ReservesRate = $ReservesRate;
			} else {
				if ( $DATA->RentGrowthOveride == "No" ) {
					$ReservesRate = $ReservesRate + (($ReservesRate * $DATA->$ExpenseReservesGrowthRate)/100);
				} else {
					$ReservesRate = $ReservesRate + (($ReservesRate * $DATA->RentGrowthRate)/100);
				}
			}
			$Reserves[$i][$n] = $ReservesRate;
		}
		foreach ( $Reserves as $k => $ReservesArray ) {
			foreach ( $ReservesArray as $id => $value ) {
				if ( !isset($D46[$id]) ) {
					$D46[$id] = 0;
				}
				$D46[$id] += round($value, 2);
			}
		}
		/** ------------------------------------------------------- D46------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D47 ------------------------------------------------------- **/
		$Expenses = array();
		for ( $i=1; $i<=21; $i++ ) {
			if ( $i <= 15 ) {
				$OperatingExpenseAnnual = 'OperatingExpenseAnnual_'.$i;
				$OperatingExpenseGrowthRate = 'OperatingExpenseGrowthRate_'.$i;
			} elseif ( $i == 16 ) {
				$OperatingExpenseAnnual = 'OperatingExpenseWaterSewerAnnual';
				$OperatingExpenseGrowthRate = 'OperatingExpenseWaterSewerGrowthRate';
			} elseif ( $i == 17 ) {
				$OperatingExpenseAnnual = 'OperatingExpenseElectricityAnnual';
				$OperatingExpenseGrowthRate = 'OperatingExpenseElectricityGrowthRate';
			} elseif ( $i == 18 ) {
				$OperatingExpenseAnnual = 'OperatingExpenseGasAnnual';
				$OperatingExpenseGrowthRate = 'OperatingExpenseGasGrowthRate';
			} elseif ( $i == 19 ) {
				$OperatingExpenseAnnual = 'OperatingExpenseFuelOilAnnual';
				$OperatingExpenseGrowthRate = 'OperatingExpenseFuelOilGrowthRate';
			} elseif ( $i == 20 ) {
				$OperatingExpenseAnnual = 'OperatingExpenseOtherUtilitiesAnnual';
				$OperatingExpenseGrowthRate = 'OperatingExpenseOtherUtilitiesGrowthRate';
			} elseif ( $i == 21 ) {
				$OperatingExpenseAnnual = 'OperatingExpenseReservesAnnual';
				$OperatingExpenseGrowthRate = 'OperatingExpenseReservesGrowthRate';
			}
			$ExpenseRate = $DATA->$OperatingExpenseAnnual;
			for ( $n=1; $n<=$DATA->YearSale; $n++ ) {
				if ( $n == 1 ) {
					$ExpenseRate = $ExpenseRate;
				} else {
					$ExpenseRate = $ExpenseRate * (1 + ($DATA->OpExpensesGrowthOverride=="No" ? ($DATA->$OperatingExpenseGrowthRate/100) : ($DATA->OpExpensesGrowthRate/100))) * (($DATA->YearSale+1==$n || $DATA->YearSale+1<$n) ? 0 : 1);
				}
				$Expenses[$i][$n] = $ExpenseRate;
			}
		}
		foreach ( $Expenses as $k => $SubExArray ) {
			foreach ( $SubExArray as $id => $value ) {
				if ( !isset($D47[$id]) ) {
					$D47[$id] = 0;
				}
				$D47[$id] += round($value, 2);
			}
		}
		/** ------------------------------------------------------- D47 ------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D49 ------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D49[$i] = $D22[$i]-$D47[$i];
		}
		/** ------------------------------------------------------- D49 ------------------------------------------------------- **/
		
		/** ----------------------------------------------------- D54/D55 ----------------------------------------------------- **/
		for ( $i=1; $i<=481; $i++ ) {
			if ( $i == 1 ) {
				$B7[$i] = round($DATA->FirstMortgageAmount, 2);
				$C7[$i] = round(($B7[$i] > 1 ? $DATA->FirstMortgagePayment : 0), 2);
				$D7[$i] = round((($DATA->FirstMortgageRate/100)/12)*$B7[$i], 2);
				$E7[$i] = round(-$C7[$i]-$D7[$i], 2);
			} else {
				$B7[$i] = round(max($B7[$i-1]-$E7[$i-1], 0), 2);
				$C7[$i] = round(($B7[$i] > 1 ? $DATA->FirstMortgagePayment : 0), 2);
				$D7[$i] = round((($DATA->FirstMortgageRate/100)/12)*$B7[$i], 2);
				$E7[$i] = round(-$C7[$i]-$D7[$i], 2);
			}
		}
		for ( $i=12; $i<=481; $i=$i+12 ) {
			$F18[$i] = $D7[$i-11]+$D7[$i-10]+$D7[$i-9]+$D7[$i-8]+$D7[$i-7]+$D7[$i-6]+$D7[$i-5]+$D7[$i-4]+$D7[$i-3]+$D7[$i-2]+$D7[$i-1]+$D7[$i];
			$G18[$i] = $E7[$i-11]+$E7[$i-10]+$E7[$i-9]+$E7[$i-8]+$E7[$i-7]+$E7[$i-6]+$E7[$i-5]+$E7[$i-4]+$E7[$i-3]+$E7[$i-2]+$E7[$i-1]+$E7[$i];
		}
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D54[$i] = round($G18[$i*12], 2);
			$D55[$i] = round($F18[$i*12], 2);
		}
		/** ----------------------------------------------------- D54/D55 ----------------------------------------------------- **/
		
		/** ----------------------------------------------------- D56/D57 ----------------------------------------------------- **/
		for ( $i=1; $i<=481; $i++ ) {
			if ( $i == 1 ) {
				$J7[$i] = round($DATA->SecondMortgageAmount, 2);
				$K7[$i] = round(($J7[$i] > 1 ? $DATA->SecondMortgagePayment : 0), 2);
				$L7[$i] = round((($DATA->SecondMortgageRate/100)/12)*$J7[$i], 2);
				$M7[$i] = round(-$K7[$i]-$L7[$i], 2);
			} else {
				$J7[$i] = round(max($J7[$i-1]-$M7[$i-1], 0), 2);
				$K7[$i] = round(($J7[$i] > 1 ? $DATA->SecondMortgagePayment : 0), 2);
				$L7[$i] = round((($DATA->SecondMortgageRate/100)/12)*$J7[$i], 2);
				$M7[$i] = round(-$K7[$i]-$L7[$i], 2);
			}
		}
		for ( $i=12; $i<=481; $i=$i+12 ) {
			$N18[$i] = $L7[$i-11]+$L7[$i-10]+$L7[$i-9]+$L7[$i-8]+$L7[$i-7]+$L7[$i-6]+$L7[$i-5]+$L7[$i-4]+$L7[$i-3]+$L7[$i-2]+$L7[$i-1]+$L7[$i];
			$O18[$i] = $M7[$i-11]+$M7[$i-10]+$M7[$i-9]+$M7[$i-8]+$M7[$i-7]+$M7[$i-6]+$M7[$i-5]+$M7[$i-4]+$M7[$i-3]+$M7[$i-2]+$M7[$i-1]+$M7[$i];
		}
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D56[$i] = round($O18[$i*12], 2);
			$D57[$i] = round($N18[$i*12], 2);
		}
		/** ----------------------------------------------------- D56/D57 ----------------------------------------------------- **/
		
		/** ------------------------------------------------------- D58 ------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D58[$i] = round($D54[$i]+$D55[$i]+$D56[$i]+$D57[$i], 2);
		}
		/** ------------------------------------------------------- D58 ------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D59 ------------------------------------------------------- **/
		$D59[0] = $DATA->TotalCashOutlay_0;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D59[$i] = round($D49[$i]-$D58[$i], 2);
		}
		/** ------------------------------------------------------- D59 ------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D61 ------------------------------------------------------- **/
		$D61[0] = ($DATA->YearSale > 30 ? IRR($D59, 0.05) : "N/A");
		/** ------------------------------------------------------- D61 ------------------------------------------------------- **/
		
		/** ----------------------------------------------------- D63/D65 ------------------------------------------------------- **/
		$D63[0] = $DATA->PurchasePrice*(1-($DATA->LandPercentage/100))+$DATA->InitialCapitalImprovements;
		$D65[0] = 0;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			if ( $i == 1 ) {
				$D65[$i] = round(min($D63[0]/$DATA->DepYears/12*11.5, $D63[0])*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1), 2);
			} else {
				$D65[$i] = round(min($D63[0]/$DATA->DepYears, $D63[$i-1])*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1), 2);
			}
			$D63[$i] = round(max(0, $D63[$i-1]-$D65[$i])*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1), 2);
		}
		/** ----------------------------------------------------- D63/D65 ------------------------------------------------------- **/
		
		/** ----------------------------------------------------- D64/D66 ------------------------------------------------------- **/
		$AssetBase1=0;$AssetBase2=0;$AssetBase3=0;$AssetBase4=0;$AssetBase5=0;$AssetBase6=0;$AssetBase7=0;$AssetBase8=0;$AssetBase9=0;$AssetBase10=0;
		$AssetBase11=0;$AssetBase12=0;$AssetBase13=0;$AssetBase14=0;$AssetBase15=0;$AssetBase16=0;$AssetBase17=0;$AssetBase18=0;$AssetBase19=0;$AssetBase20=0;
		$AssetBase21=0;$AssetBase22=0;$AssetBase23=0;$AssetBase24=0;$AssetBase25=0;$AssetBase26=0;$AssetBase27=0;$AssetBase28=0;$AssetBase29=0;$AssetBase30=0;
		for ( $i=1; $i<=60; $i++ ) {
			$AssetBaseArray1[$i]=0;$AssetBaseArray2[$i]=0; $AssetBaseArray3[$i]=0; $AssetBaseArray4[$i]=0; $AssetBaseArray5[$i]=0; 
			$AssetBaseArray6[$i]=0; $AssetBaseArray7[$i]=0; $AssetBaseArray8[$i]=0; $AssetBaseArray9[$i]=0; $AssetBaseArray10[$i]=0; 
			$AssetBaseArray11[$i]=0; $AssetBaseArray12[$i]=0; $AssetBaseArray13[$i]=0; $AssetBaseArray14[$i]=0; $AssetBaseArray15[$i]=0; 
			$AssetBaseArray16[$i]=0; $AssetBaseArray17[$i]=0; $AssetBaseArray18[$i]=0; $AssetBaseArray19[$i]=0; $AssetBaseArray20[$i]=0; 
			$AssetBaseArray21[$i]=0; $AssetBaseArray22[$i]=0; $AssetBaseArray23[$i]=0; $AssetBaseArray24[$i]=0; $AssetBaseArray25[$i]=0; 
			$AssetBaseArray26[$i]=0; $AssetBaseArray27[$i]=0; $AssetBaseArray28[$i]=0; $AssetBaseArray29[$i]=0; $AssetBaseArray30[$i]=0;
			$AssetDepArray1[$i]=0;$AssetDepArray2[$i]=0; $AssetDepArray3[$i]=0; $AssetDepArray4[$i]=0; $AssetDepArray5[$i]=0; 
			$AssetDepArray6[$i]=0; $AssetDepArray7[$i]=0; $AssetDepArray8[$i]=0; $AssetDepArray9[$i]=0; $AssetDepArray10[$i]=0; 
			$AssetDepArray11[$i]=0; $AssetDepArray12[$i]=0; $AssetDepArray13[$i]=0; $AssetDepArray14[$i]=0; $AssetDepArray15[$i]=0; 
			$AssetDepArray16[$i]=0; $AssetDepArray17[$i]=0; $AssetDepArray18[$i]=0; $AssetDepArray19[$i]=0; $AssetDepArray20[$i]=0; 
			$AssetDepArray21[$i]=0; $AssetDepArray22[$i]=0; $AssetDepArray23[$i]=0; $AssetDepArray24[$i]=0; $AssetDepArray25[$i]=0; 
			$AssetDepArray26[$i]=0; $AssetDepArray27[$i]=0; $AssetDepArray28[$i]=0; $AssetDepArray29[$i]=0; $AssetDepArray30[$i]=0;
		}
		$DeductBase1=0;$DeductBase2=0;$DeductBase3=0;$DeductBase4=0;$DeductBase5=0;$DeductBase6=0;$DeductBase7=0;$DeductBase8=0;$DeductBase9=0;$DeductBase10=0;
		$DeductBase11=0;$DeductBase12=0;$DeductBase13=0;$DeductBase14=0;$DeductBase15=0;$DeductBase16=0;$DeductBase17=0;$DeductBase18=0;$DeductBase19=0;$DeductBase20=0;
		$DeductBase21=0;$DeductBase22=0;$DeductBase23=0;$DeductBase24=0;$DeductBase25=0;$DeductBase26=0;$DeductBase27=0;$DeductBase28=0;$DeductBase29=0;$DeductBase30=0;
		$DepYears = $DATA->DepYears;
		$CIS1=$DATA->CapitalImprovementSchedule_1;$CIS2=$DATA->CapitalImprovementSchedule_2;$CIS3=$DATA->CapitalImprovementSchedule_3;
		$CIS4=$DATA->CapitalImprovementSchedule_4;$CIS5=$DATA->CapitalImprovementSchedule_5;$CIS6=$DATA->CapitalImprovementSchedule_6;
		$CIS7=$DATA->CapitalImprovementSchedule_7;$CIS8=$DATA->CapitalImprovementSchedule_8;$CIS9=$DATA->CapitalImprovementSchedule_9;
		$CIS10=$DATA->CapitalImprovementSchedule_10;$CIS11=$DATA->CapitalImprovementSchedule_11;$CIS12=$DATA->CapitalImprovementSchedule_12;
		$CIS13=$DATA->CapitalImprovementSchedule_13;$CIS14=$DATA->CapitalImprovementSchedule_14;$CIS15=$DATA->CapitalImprovementSchedule_15;
		$CIS16=$DATA->CapitalImprovementSchedule_16;$CIS17=$DATA->CapitalImprovementSchedule_17;$CIS18=$DATA->CapitalImprovementSchedule_18;
		$CIS19=$DATA->CapitalImprovementSchedule_19;$CIS20=$DATA->CapitalImprovementSchedule_20;$CIS21=$DATA->CapitalImprovementSchedule_21;
		$CIS22=$DATA->CapitalImprovementSchedule_22;$CIS23=$DATA->CapitalImprovementSchedule_23;$CIS24=$DATA->CapitalImprovementSchedule_24;
		$CIS25=$DATA->CapitalImprovementSchedule_25;$CIS26=$DATA->CapitalImprovementSchedule_26;$CIS27=$DATA->CapitalImprovementSchedule_27;
		$CIS28=$DATA->CapitalImprovementSchedule_28;$CIS29=$DATA->CapitalImprovementSchedule_29;$CIS30=$DATA->CapitalImprovementSchedule_30;
		for ( $i=1; $i<=30;$i++ ) {
			if ( $i == 1 ) {
				$AssetBase1=$CIS1;$AssetDep1=0; $AssetBase2=$CIS2;$AssetDep2=0; $AssetBase3=$CIS3;$AssetDep3=0;
				$AssetBase4=$CIS4;$AssetDep4=0; $AssetBase5=$CIS5;$AssetDep5=0; $AssetBase6=$CIS6;$AssetDep6=0;
				$AssetBase7=$CIS7;$AssetDep7=0; $AssetBase8=$CIS8;$AssetDep8=0; $AssetBase9=$CIS9;$AssetDep9=0;
				$AssetBase10=$CIS10;$AssetDep10=0; $AssetBase11=$CIS11;$AssetDep11=0; $AssetBase12=$CIS12;$AssetDep12=0;
				$AssetBase13=$CIS13;$AssetDep13=0; $AssetBase14=$CIS14;$AssetDep14=0; $AssetBase15=$CIS15;$AssetDep15=0;
				$AssetBase16=$CIS16;$AssetDep16=0; $AssetBase17=$CIS17;$AssetDep17=0; $AssetBase18=$CIS18;$AssetDep18=0;
				$AssetBase19=$CIS19;$AssetDep19=0; $AssetBase20=$CIS20;$AssetDep20=0; $AssetBase21=$CIS21;$AssetDep21=0;
				$AssetBase22=$CIS22;$AssetDep22=0; $AssetBase23=$CIS23;$AssetDep23=0; $AssetBase24=$CIS24;$AssetDep24=0;
				$AssetBase25=$CIS25;$AssetDep25=0; $AssetBase26=$CIS26;$AssetDep26=0; $AssetBase27=$CIS27;$AssetDep27=0;
				$AssetBase28=$CIS28;$AssetDep28=0; $AssetBase29=$CIS29;$AssetDep29=0; $AssetBase30=$CIS30;$AssetDep30=0;
			} else {
				$AssetBase1=$AssetBase1;$AssetDep1=min($CIS1/$DepYears, $AssetBase1); $AssetBase2=$AssetBase2;$AssetDep2=min($CIS2/$DepYears, $AssetBase2);
				$AssetBase3=$AssetBase3;$AssetDep3=min($CIS3/$DepYears, $AssetBase3); $AssetBase4=$AssetBase4;$AssetDep4=min($CIS4/$DepYears, $AssetBase4);
				$AssetBase5=$AssetBase5;$AssetDep5=min($CIS5/$DepYears, $AssetBase5); $AssetBase6=$AssetBase6;$AssetDep6=min($CIS6/$DepYears, $AssetBase6);
				$AssetBase7=$AssetBase7;$AssetDep7=min($CIS7/$DepYears, $AssetBase7); $AssetBase8=$AssetBase8;$AssetDep8=min($CIS8/$DepYears, $AssetBase8);
				$AssetBase9=$AssetBase9;$AssetDep9=min($CIS9/$DepYears, $AssetBase9); $AssetBase10=$AssetBase10;$AssetDep10=min($CIS10/$DepYears, $AssetBase10);
				$AssetBase11=$AssetBase11;$AssetDep11=min($CIS11/$DepYears, $AssetBase11); $AssetBase12=$AssetBase12;$AssetDep12=min($CIS12/$DepYears, $AssetBase12);
				$AssetBase13=$AssetBase13;$AssetDep13=min($CIS13/$DepYears, $AssetBase13); $AssetBase14=$AssetBase14;$AssetDep14=min($CIS14/$DepYears, $AssetBase14);
				$AssetBase15=$AssetBase15;$AssetDep15=min($CIS15/$DepYears, $AssetBase15); $AssetBase16=$AssetBase16;$AssetDep16=min($CIS16/$DepYears, $AssetBase16);
				$AssetBase17=$AssetBase17;$AssetDep17=min($CIS17/$DepYears, $AssetBase17); $AssetBase18=$AssetBase18;$AssetDep18=min($CIS18/$DepYears, $AssetBase18);
				$AssetBase19=$AssetBase19;$AssetDep19=min($CIS19/$DepYears, $AssetBase19); $AssetBase20=$AssetBase20;$AssetDep20=min($CIS20/$DepYears, $AssetBase20);
				$AssetBase21=$AssetBase21;$AssetDep21=min($CIS21/$DepYears, $AssetBase21); $AssetBase22=$AssetBase22;$AssetDep22=min($CIS22/$DepYears, $AssetBase22);
				$AssetBase23=$AssetBase23;$AssetDep23=min($CIS23/$DepYears, $AssetBase23); $AssetBase24=$AssetBase24;$AssetDep24=min($CIS24/$DepYears, $AssetBase24);
				$AssetBase25=$AssetBase25;$AssetDep25=min($CIS25/$DepYears, $AssetBase25); $AssetBase26=$AssetBase26;$AssetDep26=min($CIS26/$DepYears, $AssetBase26);
				$AssetBase27=$AssetBase27;$AssetDep27=min($CIS27/$DepYears, $AssetBase27); $AssetBase28=$AssetBase28;$AssetDep28=min($CIS28/$DepYears, $AssetBase28);
				$AssetBase29=$AssetBase29;$AssetDep29=min($CIS29/$DepYears, $AssetBase29); $AssetBase30=$AssetBase30;$AssetDep30=min($CIS30/$DepYears, $AssetBase30);
			}
			$AssetBase1=max(0, $AssetBase1-$AssetDep1);$AssetBaseArray1[$i]=$AssetBase1;
			$AssetBase2=max(0, $AssetBase2-$AssetDep2);$AssetBaseArray2[$i+1]=$AssetBase2;
			$AssetBase3=max(0, $AssetBase3-$AssetDep3);$AssetBaseArray3[$i+2]=$AssetBase3;
			$AssetBase4=max(0, $AssetBase4-$AssetDep4);$AssetBaseArray4[$i+3]=$AssetBase4;
			$AssetBase5=max(0, $AssetBase5-$AssetDep5);$AssetBaseArray5[$i+4]=$AssetBase5;
			$AssetBase6=max(0, $AssetBase6-$AssetDep6);$AssetBaseArray6[$i+5]=$AssetBase6;
			$AssetBase7=max(0, $AssetBase7-$AssetDep7);$AssetBaseArray7[$i+6]=$AssetBase7;
			$AssetBase8=max(0, $AssetBase8-$AssetDep8);$AssetBaseArray8[$i+7]=$AssetBase8;
			$AssetBase9=max(0, $AssetBase9-$AssetDep9);$AssetBaseArray9[$i+8]=$AssetBase9;
			$AssetBase10=max(0, $AssetBase10-$AssetDep10);$AssetBaseArray10[$i+9]=$AssetBase10;
			$AssetBase11=max(0, $AssetBase11-$AssetDep11);$AssetBaseArray11[$i+10]=$AssetBase11;
			$AssetBase12=max(0, $AssetBase12-$AssetDep12);$AssetBaseArray12[$i+11]=$AssetBase12;
			$AssetBase13=max(0, $AssetBase13-$AssetDep13);$AssetBaseArray13[$i+12]=$AssetBase13;
			$AssetBase14=max(0, $AssetBase14-$AssetDep14);$AssetBaseArray14[$i+13]=$AssetBase14;
			$AssetBase15=max(0, $AssetBase15-$AssetDep15);$AssetBaseArray15[$i+14]=$AssetBase15;
			$AssetBase16=max(0, $AssetBase16-$AssetDep16);$AssetBaseArray16[$i+15]=$AssetBase16;
			$AssetBase17=max(0, $AssetBase17-$AssetDep17);$AssetBaseArray17[$i+16]=$AssetBase17;
			$AssetBase18=max(0, $AssetBase18-$AssetDep18);$AssetBaseArray18[$i+17]=$AssetBase18;
			$AssetBase19=max(0, $AssetBase19-$AssetDep19);$AssetBaseArray19[$i+18]=$AssetBase19;
			$AssetBase20=max(0, $AssetBase20-$AssetDep20);$AssetBaseArray20[$i+19]=$AssetBase20;
			$AssetBase21=max(0, $AssetBase21-$AssetDep21);$AssetBaseArray21[$i+20]=$AssetBase21;
			$AssetBase22=max(0, $AssetBase22-$AssetDep22);$AssetBaseArray22[$i+21]=$AssetBase22;
			$AssetBase23=max(0, $AssetBase23-$AssetDep23);$AssetBaseArray23[$i+22]=$AssetBase23;
			$AssetBase24=max(0, $AssetBase24-$AssetDep24);$AssetBaseArray24[$i+23]=$AssetBase24;
			$AssetBase25=max(0, $AssetBase25-$AssetDep25);$AssetBaseArray25[$i+24]=$AssetBase25;
			$AssetBase26=max(0, $AssetBase26-$AssetDep26);$AssetBaseArray26[$i+25]=$AssetBase26;
			$AssetBase27=max(0, $AssetBase27-$AssetDep27);$AssetBaseArray27[$i+26]=$AssetBase27;
			$AssetBase28=max(0, $AssetBase28-$AssetDep28);$AssetBaseArray28[$i+27]=$AssetBase28;
			$AssetBase29=max(0, $AssetBase29-$AssetDep29);$AssetBaseArray29[$i+28]=$AssetBase29;
			$AssetBase30=max(0, $AssetBase30-$AssetDep30);$AssetBaseArray30[$i+29]=$AssetBase30;
			$AssetDepArray1[$i]=$AssetDep1; $AssetDepArray2[$i+1]=$AssetDep2; 
			$AssetDepArray3[$i+2]=$AssetDep3; $AssetDepArray4[$i+3]=$AssetDep4; 
			$AssetDepArray5[$i+4]=$AssetDep5; $AssetDepArray6[$i+5]=$AssetDep6; 
			$AssetDepArray7[$i+6]=$AssetDep7; $AssetDepArray8[$i+7]=$AssetDep8; 
			$AssetDepArray9[$i+8]=$AssetDep9; $AssetDepArray10[$i+9]=$AssetDep10; 
			$AssetDepArray11[$i+10]=$AssetDep11; $AssetDepArray12[$i+11]=$AssetDep12; 
			$AssetDepArray13[$i+12]=$AssetDep13; $AssetDepArray14[$i+13]=$AssetDep14; 
			$AssetDepArray15[$i+14]=$AssetDep15; $AssetDepArray16[$i+15]=$AssetDep16; 
			$AssetDepArray17[$i+16]=$AssetDep17; $AssetDepArray18[$i+17]=$AssetDep18; 
			$AssetDepArray19[$i+18]=$AssetDep19; $AssetDepArray20[$i+19]=$AssetDep20; 
			$AssetDepArray21[$i+20]=$AssetDep21; $AssetDepArray22[$i+21]=$AssetDep22; 
			$AssetDepArray23[$i+22]=$AssetDep23; $AssetDepArray24[$i+23]=$AssetDep24; 
			$AssetDepArray25[$i+24]=$AssetDep25; $AssetDepArray26[$i+25]=$AssetDep26; 
			$AssetDepArray27[$i+26]=$AssetDep27; $AssetDepArray28[$i+27]=$AssetDep28; 
			$AssetDepArray29[$i+28]=$AssetDep29; $AssetDepArray30[$i+29]=$AssetDep30; 
		}
		for ( $i=1; $i<=60; $i++ ) {
			if ( $i == 1 ) {
				$CBCI[$i] = $AssetBaseArray1[$i];
				$CD[$i] = $AssetDepArray1[$i];
			} else {
				$CBCI[$i] = $AssetBaseArray1[$i]+$AssetBaseArray2[$i]+$AssetBaseArray3[$i]+$AssetBaseArray4[$i]+$AssetBaseArray5[$i]+$AssetBaseArray6[$i]+$AssetBaseArray7[$i]+$AssetBaseArray8[$i]+$AssetBaseArray9[$i]+$AssetBaseArray10[$i]+$AssetBaseArray11[$i]+$AssetBaseArray12[$i]+$AssetBaseArray13[$i]+$AssetBaseArray14[$i]+$AssetBaseArray15[$i]+$AssetBaseArray16[$i]+$AssetBaseArray17[$i]+$AssetBaseArray18[$i]+$AssetBaseArray19[$i]+$AssetBaseArray20[$i]+$AssetBaseArray21[$i]+$AssetBaseArray22[$i]+$AssetBaseArray23[$i]+$AssetBaseArray24[$i]+$AssetBaseArray25[$i]+$AssetBaseArray26[$i]+$AssetBaseArray27[$i]+$AssetBaseArray28[$i]+$AssetBaseArray29[$i]+$AssetBaseArray30[$i];
				$CD[$i] = $AssetDepArray1[$i]+$AssetDepArray2[$i]+$AssetDepArray3[$i]+$AssetDepArray4[$i]+$AssetDepArray5[$i]+$AssetDepArray6[$i]+$AssetDepArray7[$i]+$AssetDepArray8[$i]+$AssetDepArray9[$i]+$AssetDepArray10[$i]+$AssetDepArray11[$i]+$AssetDepArray12[$i]+$AssetDepArray13[$i]+$AssetDepArray14[$i]+$AssetDepArray15[$i]+$AssetDepArray16[$i]+$AssetDepArray17[$i]+$AssetDepArray18[$i]+$AssetDepArray19[$i]+$AssetDepArray20[$i]+$AssetDepArray21[$i]+$AssetDepArray22[$i]+$AssetDepArray23[$i]+$AssetDepArray24[$i]+$AssetDepArray25[$i]+$AssetDepArray26[$i]+$AssetDepArray27[$i]+$AssetDepArray28[$i]+$AssetDepArray29[$i]+$AssetDepArray30[$i];
			}
		}
		$D66[0] = 0;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D64[$i] = $CBCI[$i];
			$D66[$i] = $CD[$i];
		}
		/** ----------------------------------------------------- D64/D66 ------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D67 --------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D67[$i] = $D49[$i]-$D55[$i]-$D57[$i]-$D65[$i]-$D66[$i];
		}
		/** ------------------------------------------------------- D67 --------------------------------------------------------- **/
		
		/** ------------------------------------------------------- C69 --------------------------------------------------------- **/
		$D69[0] = $DATA->Designation;
		/** ------------------------------------------------------- C69 --------------------------------------------------------- **/
		
		/** ------------------------------------------------------- C70 --------------------------------------------------------- **/
		$D70[0] = $Maxwriteoff = ($DATA->Designation=="Real Estate Professional" ? "Unlimited" : ($DATA->Designation=="Passive Participant" ? 0 : ($DATA->GAI<100000 ? 25000 : ($DATA->GAI>150000 ? 0 : (1-($DATA->GAI-100000)/50000)*25000))));
		/** ------------------------------------------------------- C70 --------------------------------------------------------- **/
		
		/** ------------------------------------------------------- C71 --------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D71[$i] = ($DATA->Designation=="Real Estate Professional" ? min($D67[$i], 0) : ($DATA->Designation=="Passive Participant" ? 0 : min(0, max($D67[$i], -$Maxwriteoff))));
		}
		/** ------------------------------------------------------- C71 --------------------------------------------------------- **/
		
		/** ------------------------------------------------------- C72 --------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D72[$i] = min(0, $D67[$i]-$D71[$i]);
		}
		/** ------------------------------------------------------- C72 --------------------------------------------------------- **/
		
		/** ----------------------------------------------------- D73/D74 --------------------------------------------------------- **/
		$D73[0] = 0;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D74[$i] = ($i==1 ? 0 : ($D67[$i]>0 ? min(-$D73[$i-1], $D67[$i]) : 0));
			$D73[$i] = ($DATA->YearSale==$i-1 ? $D73[$i-1]+$D72[$i]+$D74[$i]-$C94[$i] : $D73[$i-1]+$D72[$i]+$D74[$i]);
		}
		/** ----------------------------------------------------- D73/D74 --------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D76 ----------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D76[$i] = $D67[$i]-$D74[$i]-$D72[$i];
		}
		/** ------------------------------------------------------- D76 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D78 ----------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D78[$i] = round(($D76[$i]*$DATA->TaxBracket)/100, 2);
		}
		/** ------------------------------------------------------- D78 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D80 ----------------------------------------------------------- **/
		$D80[0] = $D59[0];
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D80[$i] = $D59[$i]-$D78[$i];
		}
		/** ------------------------------------------------------- D80 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D149 ---------------------------------------------------------- **/
		$D149[1] = ($DATA->OverrideInitialValue=="Yes" ? $DATA->InitialMarketValue-$DATA->PurchasePrice : $DATA->InitialCapitalImprovements*$DATA->FMVMultiplier);
		/** ------------------------------------------------------- D149 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D85 ----------------------------------------------------------- **/
		$D85[0] = $DATA->PurchasePrice+($DATA->Resale=="Assume Annual Appreciation%" ? $D149[1] : 0);
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
			$FairMarketValue = 'FairMarketValue_' . $i;
			$D85[$i] = round(($DATA->Resale=="Assume Annual Appreciation%" ? $D85[$i-1]*(1+($DATA->AppreciationRate/100))*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1)+$DATA->$CapitalImprovementSchedule*$DATA->$FairMarketValue : ($D49[$i]/$DATA->AppreciationRate)*100), 2);
		}
		/** ------------------------------------------------------- D85 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D51 ------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D51[$i] = $D49[$i]+($DATA->YearSale==$i ? $D85[$i] : 0);
		}
		/** ------------------------------------------------------- D51 ------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D86 ----------------------------------------------------------- **/
		$D86[0] = $DATA->FirstMortgageAmount;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D86[$i] = round($B7[$i*12+1]*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1));
		}
		/** ------------------------------------------------------- D86 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D87 ----------------------------------------------------------- **/
		$D87[0] = $DATA->SecondMortgageAmount;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D87[$i] = round($J7[$i*12+1]*(($DATA->YearSale+1==$i || $DATA->YearSale+1<$i) ? 0 :1));
		}
		/** ------------------------------------------------------- D87 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D88 ----------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D88[$i] = ($DATA->YearSale==$i ? ($DATA->CostOfSale/100)*$D85[$i] : 0);
		}
		/** ------------------------------------------------------- D88 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D89 ----------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D89[$i] = ($DATA->YearSale==$i ? $D85[$i]-($D86[$i]+$D87[$i]+$D88[$i]) : 0);
		}
		/** ------------------------------------------------------- D89 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D91 ----------------------------------------------------------- **/
		$D91[0] = $DATA->PurchasePrice+$DATA->InitialCapitalImprovements;
		$D65Total = $D65[0]; $D66Total = $D66[0];
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
			$D65Total = $D65Total + $D65[$i];
			$D66Total = $D66Total + $D66[$i];
			$D91[$i] = ($DATA->YearSale>=$i ? $D91[0]-($D65Total)+($i <= 30 ? $DATA->$CapitalImprovementSchedule : 0)-($D66Total) : 0);
		}
		/** ------------------------------------------------------- D91 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D93 ----------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D93[$i] = ($DATA->YearSale==$i ? $D85[$i]-$D91[$i]-$D88[$i] : 0);
		}
		/** ------------------------------------------------------- D93 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D94 ----------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D94[$i] = ($DATA->YearSale==$i ? $D73[$i] : 0);
		}
		/** ------------------------------------------------------- D94 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D95 ----------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D95[$i] = ($DATA->YearSale==$i ? max(0, $D93[$i]+$D94[$i]) : 0);
		}
		/** ------------------------------------------------------- D95 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D96 ----------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D96[$i] = round(($DATA->YearSale==$i ? $D95[$i]*$DATA->CapitalGainsPercentage/100 : 0), 2);
		}
		/** ------------------------------------------------------- D96 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D98 ----------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D98[$i] = ($DATA->YearSale==$i ? min(0, $D93[$i]+$D94[$i]) : 0);
		}
		/** ------------------------------------------------------- D98 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D129 ---------------------------------------------------------- **/
		$D129[0] = $DATA->InitialCapitalReserves_1;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D129[$i] = round(($DATA->YearSale>=$i ? $D129[$i-1]*(1+($DATA->InitialCapitalReserves_2/100))+$D46[$i] : 0), 2);
		}
		/** ------------------------------------------------------- D129 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D99 ----------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D99[$i] = ($DATA->YearSale==$i ? $D129[$i] : 0);
		}
		/** ------------------------------------------------------- D99 ----------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D100 ---------------------------------------------------------- **/
		$D100[0] = $D80[0];
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
			$D100[$i] = ($DATA->YearSale==$i ? $D59[$i]+$D89[$i]+$D99[$i] : $D59[$i]-($i <= 30 ? $DATA->$CapitalImprovementSchedule : 0));
		}
		/** ------------------------------------------------------- D100 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D101 ---------------------------------------------------------- **/
		$D101[0] = $D80[0];
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
			$D101[$i] = ($DATA->YearSale==$i ? $D80[$i]+$D89[$i]-$D96[$i]+$D99[$i] : $D80[$i]-($i <= 30 ? $DATA->$CapitalImprovementSchedule : 0));
		}
		/** ------------------------------------------------------- D101 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D102 ---------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D101Current[] = $D101[$i];
			$D102[$i] = ($i!=1 ? ($DATA->YearSale>=$i ? IRR($D101Current, 0.005)*100 : 0) : 0);
		}
		/** ------------------------------------------------------- D102 ---------------------------------------------------------- **/
		
		
		/** ------------------------------------------------------- D114 ---------------------------------------------------------- **/
		$D114[0] = $DATA->TotalCashOutlay_0;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$TotalCashOutlay = 'TotalCashOutlay_' . $i;
			$D114[$i] = ($DATA->YearSale>=$i ? $DATA->$TotalCashOutlay : 0);
		}
		/** ------------------------------------------------------- D114 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D116 ---------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D116[$i] = ($D114[$i]==0 ? "N/A" : ($DATA->YearSale>=$i ? round(-$D59[$i]/$D114[$i]*100, 2) : 0));
		}
		/** ------------------------------------------------------- D116 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D117 ---------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D117[$i] = ($D114[$i]==0 ? "N/A" : ($DATA->YearSale>=$i ? round(-$D80[$i]/$D114[$i]*100, 2) : 0));
		}
		/** ------------------------------------------------------- D117 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D118 ---------------------------------------------------------- **/
		$D59Total = 0;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D59Total = $D59Total + $D59[$i];
			$D118[$i] = ($D114[$i]==0 ? "N/A" : ($DATA->YearSale>=$i ? round((-$D59Total-($DATA->YearSale==$i ? $D89[$i] : 0))/$D114[$i]*100, 2) : 0));
		}
		/** ------------------------------------------------------- D118 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D119 ---------------------------------------------------------- **/
		$D101Total = 0;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D101Total = $D101Total + $D101[$i];
			$D119[$i] = ($D114[$i]==0 ? "N/A" : ($DATA->YearSale>=$i ? round(-($D101Total)/$D114[$i]*100, 2) : 0));
		}
		/** ------------------------------------------------------- D119 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D120 ---------------------------------------------------------- **/
		$D120[0] = $D86[0]+$D87[0];
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D120[$i] = ($DATA->YearSale>=$i ? $D86[$i]+$D87[$i] : 0);
		}
		/** ------------------------------------------------------- D120 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D121 ---------------------------------------------------------- **/
		$D121[0] = $D85[0]-$D120[0];
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D121[$i] = ($DATA->YearSale>=$i ? $D85[$i]-$D120[$i] : 0);
		}
		/** ------------------------------------------------------- D121 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D122 ---------------------------------------------------------- **/
		$D122[0] = ($DATA->YearSale>=0 ? $D121[0]+$D129[0] : 0);
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D122[$i] = ($DATA->YearSale>=$i ? $D121[$i]+$D129[$i] : 0);
		}
		/** ------------------------------------------------------- D122 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D123 ---------------------------------------------------------- **/
		$D123[0] = ($D85[0] == 0 ? 0 : $D120[0]/$D85[0]*100);
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D123[$i] = ($DATA->YearSale>=$i ? round(($D85[$i] == 0 ? 0 : $D120[$i]/$D85[$i]*100), 2) : 0);
		}
		/** ------------------------------------------------------- D123 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D124 ---------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D124[$i] = ($DATA->YearSale>=$i ? ($D58[$i]>0.01 ? round($D49[$i]/$D58[$i], 2) : "N/A") : 0);
		}
		/** ------------------------------------------------------- D124 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D125 ---------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D125[$i] = ($DATA->YearSale>=$i ? round($D59[$i]/$D122[$i]*100, 2) : 0);
		}
		/** ------------------------------------------------------- D125 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D126 ---------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D126[$i] = ($DATA->YearSale>=$i ? round($D80[$i]/$D122[$i]*100, 2) : 0);
		}
		/** ------------------------------------------------------- D126 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D127 ---------------------------------------------------------- **/
		$D127[0] = ($DATA->TotalLoanAmount>1 ? round($DATA->NetOperatingIncomeAnnual/$DATA->TotalLoanAmount*100, 2) : "N/A");
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D127[$i] = ($DATA->YearSale>=$i ? ($D120[$i]>0.01 ? round($D49[$i]/$D120[$i]*100, 2) : "N/A") : 0);
		}
		/** ------------------------------------------------------- D127 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D131 ---------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D131[$i] = ($DATA->YearSale>=$i ? round($D49[$i]/($DATA->PurchasePrice+$DATA->InitialCapitalImprovements)*100, 2) : 0);
		}
		/** ------------------------------------------------------- D131 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D134 ---------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D134[$i] = ($DATA->YearSale>=$i ? $D55[$i]+$D57[$i] : 0);
		}
		/** ------------------------------------------------------- D134 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D135 ---------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D135[$i] = ($DATA->YearSale>=$i ? $D54[$i]+$D56[$i] : 0);
		}
		/** ------------------------------------------------------- D135 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D138 ---------------------------------------------------------- **/
		$D138[0] = $D122[0];
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D138[$i] = ($DATA->YearSale>=$i ? round($D122[$i]) : 0);
		}
		/** ------------------------------------------------------- D138 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D139 ---------------------------------------------------------- **/
		$D139[0] = $DATA->DownPayment;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D139[$i] = ($DATA->YearSale>=$i ? $DATA->DownPayment : 0);
		}
		/** ------------------------------------------------------- D139 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D140 ---------------------------------------------------------- **/
		$D140[0] = $D129[0];
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D140[$i] = ($DATA->YearSale>=$i ? round($D129[$i]) : 0);
		}
		/** ------------------------------------------------------- D140 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D141 ---------------------------------------------------------- **/
		$D141[0] = $D85[0]-$DATA->PurchasePrice;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D141[$i] = ($DATA->YearSale>=$i ? $D85[$i]-$DATA->PurchasePrice : 0);
		}
		/** ------------------------------------------------------- D141 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D142 ---------------------------------------------------------- **/
		$D142[0] = -($D120[0]-$D120[0]);
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D142[$i] = ($DATA->YearSale>=$i ? -($D120[$i]-$D120[0]) : 0);
		}
		/** ------------------------------------------------------- D142 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D143 ---------------------------------------------------------- **/
		$D143[0] = $D138[0]-($D139[0]+$D140[0]+$D141[0]+$D142[0]);
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D143[$i] = ($DATA->YearSale>=$i ? $D138[$i]-($D139[$i]+$D140[$i]+$D141[$i]+$D142[$i]) : 0);
		}
		/** ------------------------------------------------------- D143 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D145 ---------------------------------------------------------- **/
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$D145[$i] = ($DATA->YearSale>=$i ? round(($D58[$i]+$D47[$i])/$D22[$i]*100, 2) : 0);
		}
		/** ------------------------------------------------------- D145 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D146 ---------------------------------------------------------- **/
		$D146[0] = -$DATA->TotalCashOutlay+$DATA->InitialCapitalReserves_1;
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
			$D146[$i] = ($DATA->YearSale>=$i ? $D146[$i-1]+$D100[$i]+($D129[$i]-$D129[$i-1])-($i <= 30 ? $DATA->$CapitalImprovementSchedule : 0)-($DATA->YearSale==$i ? $D99[$i] : 0) : 0);
		}
		/** ------------------------------------------------------- D146 ---------------------------------------------------------- **/
		
		/** ------------------------------------------------------- D147 ---------------------------------------------------------- **/
		$D147[0] = $D146[0];
		for ( $i=1; $i<=$DATA->YearSale; $i++ ) {
			$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
			$D147[$i] = ($DATA->YearSale>=$i ? $D147[$i-1]+$D101[$i]+($D129[$i]-$D129[$i-1])-($i <= 30 ? $DATA->$CapitalImprovementSchedule : 0)-($DATA->YearSale==$i ? $D99[$i] : 0) : 0);
		}
		/** ------------------------------------------------------- D147 ---------------------------------------------------------- **/ 
		
		$CapRate = round($DATA->NetOperatingIncomeAnnual/($DATA->PurchasePrice+$DATA->InitialCapitalImprovements+$DATA->FeeName)*100, 2); 		
		$GRM = round(($DATA->PurchasePrice+$DATA->InitialCapitalImprovements+$DATA->FeeName)/$DATA->GrossScheduledIncomeAnnual, 2); 		
		$SqFeet = round(($DATA->PurchasePrice+$DATA->InitialCapitalImprovements+$DATA->FeeName)/$DATA->PropertySquareFeet_Total, 2);		
		$DCR = round(($DATA->TotalLoanAmount==0 ? "N/A" : $DATA->NetOperatingIncomeAnnual/-($DATA->FirstMortgagePaymentTotal*12)), 2); 		
		$DebtYield = ($DATA->TotalLoanAmount>1 ? round($DATA->NetOperatingIncomeAnnual/$DATA->TotalLoanAmount*100, 2) : "N/A"); 		
		$PaybackPeriod = round($DATA->TotalCashOutlay/((($DATA->OperatingIncomeMonthlyTotal-$DATA->OperatingExpenseTotalMonthly)+$DATA->FirstMortgagePaymentTotal)*12), 2); 		
		$BreakEvenRatio = round(-(($DATA->FirstMortgagePaymentTotal*12)-$DATA->OperatingExpenseTotalAnnual)/$DATA->OperatingIncomeAnnualTotal*100, 2); 		
		$Unit = round(($DATA->PurchasePrice+$DATA->InitialCapitalImprovements+$DATA->FeeName)/$DATA->PropertyNoOfUnit_Total);		
		$SalePrice = $D85[$DATA->YearSale];		
		$PreTaxIRR = ($DATA->TotalCashOutlay==0 ? "N/A" : round(IRR($D100, 0.05)*100, 2));		
		$AfterTaxIRR = ($DATA->TotalCashOutlay==0 ? "N/A" : round(IRR($D101, 0.05)*100, 2));		
		$PreTaxCumCashCash = ($DATA->TotalCashOutlay==0 ? "N/A" : $D118[$DATA->YearSale]);		
		$CumCashCash = ($DATA->TotalCashOutlay==0 ? "N/A" : $D119[$DATA->YearSale]);		
		$InitialCashInvestment = -$DATA->TotalCashOutlay;		
		$CapitalImprovementOutlaysAfterYear = 0;
		for ( $i=1; $i<=30; $i++ ) {
			$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
			$CapitalImprovementOutlaysAfterYear += $DATA->$CapitalImprovementSchedule;
		}		
		$NewD80 = $D80;
		array_splice($NewD80, 0, 1);
		$CashReceivedOverHoldingPeriod = round(array_sum($NewD80));		
		$FirstMortgagePayoff = -$D86[$DATA->YearSale];		
		$SecondMortgagePayoff = -$D87[$DATA->YearSale];		
		$CostOfSale = round(-$D88[$DATA->YearSale]);		
		$CapitalGains = round(-$D96[$DATA->YearSale]);		
		$AfterTaxCash = round($SalePrice+$FirstMortgagePayoff+$SecondMortgagePayoff+$CostOfSale+$CapitalGains);
		
		$html = ''; 
		for ( $a = 0; $a < $DATA->YearSale; $a = $a + 10 ) {
			if ( ($a + 10) <= $DATA->YearSale && $a == 0 ) {
				$loop = 10;
			} elseif ( ($a + 10) <= $DATA->YearSale && $a == 10 ) {
				$loop = 20;
			} elseif ( ($a + 10) <= $DATA->YearSale && $a == 20 ) {
				$loop = 30;
			} else {
				$loop = $DATA->YearSale;
			}
			if ( $loop <= 10 ) {
				$span = 10;
			} elseif ( $loop <= 20 ) {
				$span = 20;
			} elseif ( $loop <= 30 ) {
				$span = 30;
			} else {
				$span = 40;
			}
			
			$html .= '<table class="table table-bordered">';
			
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<th width="17.5%">Year (End)<br><br></th>';
			$html .= '<th width="7.5%" class="text-center">&nbsp;</th>';
			$chq_date = str_replace("-", "/", $DATA->PurchaseDate);
			$closing_date  = date("Y-m-d", strtotime($chq_date));
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$html .= '<th width="7.5%" class="text-center">' . $i . '<br>'. date('d-m-Y', strtotime("$closing_date + $i years")) . '</th>';
				} else {
					$html .= '<th width="7.5%" class="text-center">&nbsp;</th>';
				}
			}
			$html .= '</tr>';
			$html .= '</thead>';
			
            $html .= '<tbody>';
            $html .= '<tr>';
            $html .= '<td>Existing Mortgage Balance 1</td>';
            $html .= '<td class="text-center">&nbsp;</td>';
            for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
	                $html .= '<td class="text-center">' . number_format($D86[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
            }
			$html .= '</tr>';
			
            $html .= '<tr>';
            $html .= '<td>Existing Mortgage Balance 2</td>';
            $html .= '<td class="text-center">&nbsp;</td>';
            for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
	                $html .= '<td class="text-center">' . number_format($D87[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
            }
            $html .= '</tr>';
			
            $html .= '<tr>';
            $html .= '<td>Net Operating Income (NOI)</td>';
            $html .= '<td class="text-center">&nbsp;</td>';
            for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
	                $html .= '<td class="text-center">' . number_format($D49[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
            }
			$html .= '</tr>';
			
            $html .= '<tr>';
			$html .= '<td>Projected Appraisal of Property</td>';
			$html .= '<td>&nbsp;</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$CapitalImprovementSchedule = 'CapitalImprovementSchedule_' . $i;
					$FairMarketValue = 'FairMarketValue_' . $i;
					$C22[$i] = ($DATA->YearSale>=$i ? ($H9=="Set Cap Rate" ? ($D49[$i]/$H10) : $D85[$i-1]*(1+$H10)+$DATA->$CapitalImprovementSchedule*$DATA->$FairMarketValue) : 0);
					$html .= '<td class="text-center">' . number_format($C22[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td>New Loan Amount</td>';
			$NewLoanRate = $C12*($C9=="Yes" ? 1 : 0)+$D12*($D9=="Yes" ? 1 : 0);
			$html .= '<td class="text-center">' . ($a==0 ? $NewLoanRate*100 . '%' : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;') . '</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$C23[$i] = ($DATA->YearSale>=$i ? $B23*$C22[$i] : 0);
					$html .= '<td class="text-center">' . number_format($C23[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td>Closing costs (% of new loan)</td>';
			$html .= '<td>&nbsp;</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$C24[$i] = ($DATA->YearSale>=$i ? $C23[$i]*$B24 : 0);
					$html .= '<td class="text-center">' . number_format($C24[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td>Cash Pulled out at Refi</td>';
			$html .= '<td class="text-center">&nbsp;</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$C25[$i] = ($DATA->YearSale>=$i ? $C23[$i]-($D86[$i]+$D87[$i])-$C24[$i] : 0);
					$html .= '<td class="text-center">' . number_format($C25[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td>Cash tied up in Property prior to Refi</td>';
			$html .= '<td class="text-center">&nbsp;</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$TotalCashOutlay = 'TotalCashOutlay_' . ($i-1);
					$C26[$i] = ($DATA->YearSale>=$i ? $DATA->$TotalCashOutlay : 0);
					$html .= '<td class="text-center">' . number_format($C26[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td>Net Cash Pulled out at Refi</td>';
			$html .= '<td class="text-center">&nbsp;</td>';
			$C27[0] = 0;
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$C27[$i] = ($DATA->YearSale>=$i ? $C25[$i]+$C26[$i] : 0);
					$html .= '<td class="text-center">' . number_format($C27[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td><strong>First year when you can pull out entire original Cash Investment</strong></td>';
			$html .= '<td class="text-center">&nbsp;</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$C28[$i] = ($DATA->YearSale>=$i ? ($C27[$i-1]>0 ? "&nbsp;" : ($C27[$i]>0 ? "IN YEAR " . $i : "&nbsp;")) : "&nbsp;");
					$html .= '<td class="text-center">' . $C28[$i] . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td>New Mortgage Payment 1 (Annual)</td>';
			$html .= '<td class="text-center">&nbsp;</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$C30[$i] = ($DATA->YearSale>=$i ? ($C10=="Fixed" ? PMT($C13/12, $C11*12, $C12*$C22[$i], 0, 0) : -$C12*$C22[$i]*$C13/12)*12 : 0);
					$html .= '<td class="text-center">' . number_format($C30[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td>New Mortgage Payment 2 (Annual)</td>';
			$html .= '<td class="text-center">&nbsp;</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$C31[$i] = ($DATA->YearSale>=$i ? ($D10=="Fixed" ? PMT($D13/12, $D11*12, $D12*$C23[$i], 0, 0) : -$D12*$C23[$i]*$D13/12)*($D9=="No" ? 0 : 1)*12 : 0);
					$html .= '<td class="text-center">' . number_format($C31[$i], 0) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td>DCR of new loan</td>';
			$html .= '<td class="text-center">&nbsp;</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$C32[$i] = ($DATA->YearSale>=$i ? ($C30[$i]+$C31[$i] == 0 ? 0 : -$D49[$i]/(($C30[$i]+$C31[$i]))) : 0);
					$html .= '<td class="text-center">' . number_format($C32[$i], 2) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td>New Monthly Cash Flow (after Refi)</td>';
			$html .= '<td class="text-center">&nbsp;</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$C33[$i] = ($DATA->YearSale>=$i ? ($D49[$i]+$C30[$i])/12 : 0);
					$html .= '<td class="text-center">' . number_format($C33[$i], 2) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '<tr>';
			$html .= '<td>Equity Left (after Refi)</td>';
			$html .= '<td class="text-center">&nbsp;</td>';
			for ( $i=$a+1; $i<=$span; $i++ ) {
				if ( $i <= $DATA->YearSale ) {
					$C34[$i] = ($DATA->YearSale>=$i ? $C22[$i]-$C23[$i] : 0);
					$html .= '<td class="text-center">' . number_format($C34[$i], 2) . '</td>';
				} else {
					$html .= '<td class="text-center">&nbsp;</td>';
				}
			}
			$html .= '</tr>';
			
			$html .= '</tbody>';
			$html .= '</table>';
		}
		$RefAnaPDF = array(
			'FirstMortgageUsed' 		=> $_POST['FirstMortgageUsed'],
			'SecondMortgageUsed' 		=> $_POST['SecondMortgageUsed'],
			'FirstMortgageLoanType' 	=> $_POST['FirstMortgageLoanType'],
			'SecondMortgageLoanType' 	=> $_POST['SecondMortgageLoanType'],
			'FirstMortgageTerm' 		=> $_POST['FirstMortgageTerm'],
			'SecondMortgageTerm' 		=> $_POST['SecondMortgageTerm'],
			'FirstMortgageLTV' 			=> $_POST['FirstMortgageLTV'],
			'SecondMortgageLTV' 		=> $_POST['SecondMortgageLTV'],
			'FirstMortgageRate' 		=> $_POST['FirstMortgageRate'],
			'SecondMortgageRate' 		=> $_POST['SecondMortgageRate'],
			'RefiResale' 				=> $_POST['RefiResale'],
			'TerminalCapRate' 			=> $_POST['TerminalCapRate'],
			'NewLoanAmount' 			=> $_POST['NewLoanAmount'],
			'ClosingCostNewRate' 		=> $_POST['ClosingCostNewRate'],			
			'RefinancingAnalysisHTML' 	=> $html
		);
		$this->session->set_userdata($RefAnaPDF);
		echo $html;
	}
	// End of the function
	
	// Start Delete Photo Scripts
	public function delete_photo() {
		$user_id = $this->session->userdata('user_id');
		
		$PhotoID 	= $_POST['PhotoID'];
		$Photo 		= $_POST['Photo'];
		$Table		= $_POST['Table'];

		echo $this->pages_model->remove_photo($PhotoID, $Photo, $Table);
		
	}
	// End of the function
	
	
	
	// ========================================================= STATIC PAGES (ABOUT BLOG PROCING FEATURE ETC) ======================================================== //
	// Start About us page
	public function aboutus() {
		$data['user_id'] = $this->session->userdata('user_id');
		$data['page_title'] = 'About Us >> CBLists';
        $this->load->view('template/header', $data);
        $this->load->view('view_aboutus', $data);
        $this->load->view('template/footer', $data);
	}
	// End of the function
	
	// Start About us page
	public function blog() {
		$data['user_id'] = $this->session->userdata('user_id');
		$data['page_title'] = 'Blog >> CBLists';
        $this->load->view('template/header', $data);
        $this->load->view('view_blog', $data);
        $this->load->view('template/footer', $data);
	}
	// End of the function
	
	// Start About us page
	public function features() {
		$data['user_id'] = $this->session->userdata('user_id');
		$data['page_title'] = 'Features >> CBLists';
        $this->load->view('template/header', $data);
        $this->load->view('view_features', $data);
        $this->load->view('template/footer', $data);
	}
	// End of the function
	
	// Start Our Offer page
	public function our_offer() {
		$data['user_id'] = $this->session->userdata('user_id');
		$data['page_title'] = 'Features >> CBLists';
        $this->load->view('template/header', $data);
        $this->load->view('view_ouroffer', $data);
        $this->load->view('template/footer', $data);
	}
	// End of the function
	
}

/* End of file pages.php */
/* Location: ./application/controllers/pages.php */