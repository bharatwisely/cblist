<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
class CustomError extends CI_Controller {
	
    public function __construct() {
        parent::__construct(); 
		
		$this->load->model('pages_model');
		$this->load->library('email');
    }

    public function Error404() {
		$data['user_id'] = $user_id = $this->session->userdata('user_id');
		$user_details = $this->pages_model->get_user($user_id);
		if ($this->pages_model->check_user_profile_exists($user_id) == 1) {
			$data['profile'] = TRUE;
		} else {
			$data['profile'] = FALSE;
		}
		if ($this->pages_model->check_user_portfolio_exists($user_id) == 1) {
			$data['portfolio'] = TRUE;
		} else {
			$data['portfolio'] = FALSE;
		}
		
		if ($this->pages_model->check_user_proposal_exists($user_id) == 1) {
			$data['proposal'] = TRUE;
		} else {
			$data['proposal'] = FALSE;
		}
		
		$data['success'] = ''; $data['error'] = '';
		if ( $this->input->post() ) {
			$issue = $this->input->post('message');
			if ( !empty($issue) ) {
				//Email sending code
				$message  = "<p>One of our visitor got a 404 page. Something went wrong there. He/She is contacting you for resolving this issue.</p>";
				$message .= "Visitor's message: " . $issue;
	
				$this->email->set_newline("\r\n");
				$this->email->from(SendFrom, 'CBList');
				$this->email->to('jitdxpert@gmail.com', 'CBList');
				$this->email->subject('CBList 404 Page information by visitor');
				$this->email->message($message);
				if ($this->email->send()) {
					$data['success'] = 'Thank you for inform us! We will resolved the problem soon';
				} else {
					$data['error'] = 'Something went wrong! Please try again.';
				}
			} else {
				$data['error'] = 'Please type your message first.';
			}
		}
		
		if ( $user_id ) {
			$data['user'] = $user_details[0];
		}
        $this->output->set_status_header('404'); 
        $data['page_title'] = '404 Page not found';
		$this->load->view('template/header', $data);
        $this->load->view('view_404', $data);
		$this->load->view('template/footer', $data);
    } 
}