<?php
require_once("dompdf_config.inc.php");

$html =
  '<html><body>'.
  '<img src="http://engagewise.com/images/EngageWise-Logo-White.png">'.
  '<p>Put your html here, or generate it with your favourite '.
  'templating system.</p>'.
  '</body></html>';

$dompdf = new DOMPDF();
$dompdf->load_html($html);
$dompdf->render();
$dompdf->stream("sample.pdf");

?>